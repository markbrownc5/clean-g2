/****** Object:  User [G2]    Script Date: 9/19/2018 10:41:03 AM ******/
CREATE USER [G2] FOR LOGIN [G2] WITH DEFAULT_SCHEMA=[G2]
GO

CREATE ROLE [G2AppUser]
GO

ALTER ROLE G2AppUser ADD MEMBER G2
GO
--CREATE USER [dbo] FOR LOGIN [g2dbo] WITH DEFAULT_SCHEMA=[dbo]
--GO

--sp_change_users_login @action = 'update_one', @UserNamePattern='dbo', @LoginName='g2db0'
--GO
--sp_changedbowner @loginame='g2dbo'
