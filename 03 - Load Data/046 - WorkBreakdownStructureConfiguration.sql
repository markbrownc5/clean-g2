BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @funding INT = G2.fnGetFundingConfigurationTypeId(), @cost INT = G2.fnGetCostObligationConfigurationTypeId(), @user INT = 614;
	DECLARE @project INT = G2.fnGetProjectSubSystemLevelId(1);

	INSERT INTO G2.WorkBreakdownStructureConfiguration (WorkBreakdownStructureId, ConfigurationTypeId, ConfigurationGroupId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT wbs.Id, ct.Id, NULL, @user, @user, @user, @user 
	FROM G2.WorkBreakdownStructure wbs
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = wbs.SubSystemLevelId
	CROSS JOIN G2.ConfigurationType ct
	WHERE ssl.Id = @project
	AND ct.Id IN (@funding, @cost)
	EXCEPT
	SELECT wbsc.WorkBreakdownStructureId, wbsc.ConfigurationTypeId, wbsc.ConfigurationGroupId, @user, @user, @user, @user
	FROM G2.WorkBreakdownStructureConfiguration wbsc;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH