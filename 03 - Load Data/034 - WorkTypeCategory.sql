BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 4,
    "SubSystemId": 1,
    "Name": "Management",
    "Description": "All management subfunctions."
}, {
    "Id": 5,
    "SubSystemId": 1,
    "Name": "Cross-Cutting",
    "Description": "All Cross-Cutting subfunctions."
}, {
    "Id": 6,
    "SubSystemId": 1,
    "Name": "Construction",
    "Description": "All Construction subfunctions."
}, {
    "Id": 7,
    "SubSystemId": 1,
    "Name": "Recapitalization",
    "Description": "All Recapitalization subfunctions."
}, {
    "Id": 8,
    "SubSystemId": 1,
    "Name": "Maintenance",
    "Description": "All Maintenance subfunctions."
}, {
    "Id": 9,
    "SubSystemId": 1,
    "Name": "Operations",
    "Description": "All Operations subfunctions."
}, {
    "Id": 22,
    "SubSystemId": 1,
    "Name": "Disposition",
    "Description": "All Disposition subfunctions."
}, {
    "Id": 23,
    "SubSystemId": 1,
    "Name": "AMP",
    "Description": "All AMP subfunctions."
}, {
    "Id": 24,
    "SubSystemId": 1,
    "Name": "SEMO",
    "Description": "All SEMO subfunctions."
}]';

	SET IDENTITY_INSERT G2.WorkTypeCategory ON;

	INSERT INTO G2.WorkTypeCategory (Id, SubSystemId, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.SubSystemId, oj.Name, oj.Description, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (Id INT '$.Id', SubSystemId INT '$.SubSystemId', Name NVARCHAR(50) '$.Name', Description NVARCHAR(100) '$.Description') oj
	EXCEPT
	SELECT wtc.Id, wtc.SubSystemId, wtc.Name, wtc.Description, @user, @user, @user, @user
	FROM G2.WorkTypeCategory wtc;
	
	SET IDENTITY_INSERT G2.WorkTypeCategory OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH