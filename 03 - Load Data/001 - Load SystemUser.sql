/*
SELECT pers.Id, pers.LoginName, pers.EmailAddress, pers.AIMSId, pers.FirstName, pers.MiddleName, pers.LastName, pers.Phone, pers.Fax, pers.G2UserInd, pers.FedInd, pers.ActiveInd, pers.MobileUserInd, pers.JobTitle
FROM [NN1STGDB03.ORNL.GOV].QA_Setup.G2.Person p
INNER JOIN G2.Security.Person pers ON p.Id = pers.Id
WHERE pers.ActiveInd = 1
FOR JSON PATH 
*/


BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @personData NVARCHAR(MAX) = '[{
    "Id": 207,
    "LoginName": "kve",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "2E3403EF-047F-4484-9AE3-15B4F2C08BE6",
    "FirstName": "Karl",
    "MiddleName": "I",
    "LastName": "Allen",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 208,
    "LoginName": "ouc",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "5805A32A-BB90-452D-822A-E2226461BD19",
    "FirstName": "Sara",
    "MiddleName": "N",
    "LastName": "O''Neal",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Software Engineer"
}, {
    "Id": 209,
    "LoginName": "i8l",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "553F8391-200B-44AE-8C77-04E3A9AA7396",
    "FirstName": "Christopher",
    "MiddleName": "G",
    "LastName": "Luttrell",
    "Phone": "865-694-5603",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Data Architect"
}, {
    "Id": 210,
    "LoginName": "i95",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "7E0A16C0-8A47-4D2A-A6CD-E2E4DA2B8719",
    "FirstName": "Chris",
    "MiddleName": "S",
    "LastName": "O''Neal",
    "Phone": "865-300-6836",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Sr Partner"
}, {
    "Id": 211,
    "LoginName": "t7o",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "79DDBE02-1D9A-4BF8-80D1-8C754B78EE58",
    "FirstName": "Catherine",
    "MiddleName": "P",
    "LastName": "Toth",
    "Phone": "865-381-1120 e7006",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false
}, {
    "Id": 283,
    "LoginName": "d77",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "76FE8D6A-CD4D-4645-8DC6-626E0CE900A5",
    "FirstName": "Diedre",
    "MiddleName": "D",
    "LastName": "Nickum",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Sr. Technical Analyst"
}, {
    "Id": 335,
    "LoginName": "cqu",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "218BF72C-1119-4C52-937E-CB2ECAA077FC",
    "FirstName": "Curtis",
    "MiddleName": "B",
    "LastName": "Dunn",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 357,
    "LoginName": "mfi",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "C1707D79-CF79-4667-9F10-8C146AE21BB8",
    "FirstName": "Michael",
    "MiddleName": "W",
    "LastName": "Strickland",
    "Phone": "865-241-5995",
    "Fax": "865-574-3900",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false
}, {
    "Id": 376,
    "LoginName": "kqy",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "68BB6561-7D88-49F8-BF88-BC003D9E6C6A",
    "FirstName": "Kimberly",
    "MiddleName": "K",
    "LastName": "Hobson",
    "Phone": "865-241-3291",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Project Manager"
}, {
    "Id": 411,
    "LoginName": "tjc",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "2EAA5F45-8253-416B-8C6C-567170BD5461",
    "FirstName": "Tammy",
    "MiddleName": "T",
    "LastName": "Claiborne",
    "Phone": "865-576-3267",
    "Fax": "865-574-8186",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false
}, {
    "Id": 454,
    "LoginName": "ogo",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "430471C0-8D2F-47BD-8D45-DD12E60E6235",
    "FirstName": "Greg",
    "MiddleName": "A",
    "LastName": "Ogle",
    "Phone": "865-241-6108",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Developer"
}, {
    "Id": 455,
    "LoginName": "fhm",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "4F7E88D1-A52F-445E-9F9D-192BDCB0BDC5",
    "FirstName": "Matthew",
    "MiddleName": "L",
    "LastName": "Morrell",
    "Phone": "865-241-6109",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Developer"
}, {
    "Id": 550,
    "LoginName": "ttclaiborne@gmail.com",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "AE50DB8E-A49D-4404-B751-36EE464C1DBA",
    "FirstName": "Test",
    "MiddleName": "T",
    "LastName": "Tammy",
    "Phone": "865-576-3267",
    "Fax": "865-574-8186",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false
}, {
    "Id": 581,
    "LoginName": "gi2",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "4A076B73-6755-44A1-A8AF-427619E81338",
    "FirstName": "Geffrey",
    "MiddleName": "A",
    "LastName": "Ivey",
    "Phone": "865-574-3779",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true
}, {
    "Id": 614,
    "LoginName": "bor",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "D5FAE87A-D183-43EB-AD4D-76718F0ADD15",
    "FirstName": "Mark",
    "MiddleName": "A",
    "LastName": "Brown",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 626,
    "LoginName": "gm9",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "566E130A-4406-4060-83AE-EFD115DF17EC",
    "FirstName": "Gary",
    "MiddleName": "L",
    "LastName": "Murray",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Inf. Man. Specialist\/Analyst Level 2"
}, {
    "Id": 643,
    "LoginName": "gm9@ornl.gov",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "05D0ADCA-64F2-4995-87AC-A169F52CBB95",
    "FirstName": "GaryTest",
    "MiddleName": "L",
    "LastName": "Murray",
    "Phone": "865-660-9799",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Inf. Man. Specialist\/Analyst Level 2"
}, {
    "Id": 653,
    "LoginName": "cni",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "8424B308-790A-471A-B5D5-A5A6ED10389D",
    "FirstName": "Carrie",
    "MiddleName": "L",
    "LastName": "Nix",
    "Phone": "865-241-5853",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": "Application Support and Data Analyst"
}, {
    "Id": 674,
    "LoginName": "xaa",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "19340F8B-A388-495B-B137-27BFD3CDCE97",
    "FirstName": "Andrew",
    "MiddleName": "J",
    "LastName": "Ayers",
    "Phone": "630-252-0752",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Principal Software Engineer"
}, {
    "Id": 723,
    "LoginName": "j43",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "739E72C8-AE72-46AB-BE9D-0B8010978805",
    "FirstName": "Jason",
    "MiddleName": "K",
    "LastName": "Frank",
    "Phone": "865-705-5432",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 733,
    "LoginName": "bq4",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "D111CE6B-9B12-4701-8C0C-AA8AF0814216",
    "FirstName": "Jocelyn",
    "MiddleName": "L",
    "LastName": "Borgers",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 734,
    "LoginName": "pk4",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "7A93A76C-BE4C-4160-A726-C89C7BFFA04A",
    "FirstName": "Priya",
    "LastName": "Kumar",
    "Phone": "865-381-1120 e7004",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist"
}, {
    "Id": 740,
    "LoginName": "h7j",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "A71F433B-D175-4B4A-9F56-C3BBD2624818",
    "FirstName": "Jeffrey",
    "MiddleName": "B",
    "LastName": "Herd",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information System Analyst Level 2"
}, {
    "Id": 870,
    "LoginName": "dqq",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "F10705A4-EA27-4BB3-BF34-AB29935F88C7",
    "FirstName": "David",
    "MiddleName": "G",
    "LastName": "Prenshaw",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1018,
    "LoginName": "u89",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "3686FFB9-00E6-4CE9-87BC-FD9CE3FA5579",
    "FirstName": "Adela",
    "MiddleName": "E",
    "LastName": "Smith",
    "Phone": "863-447-0840",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Senior Software Engineer 2"
}, {
    "Id": 1019,
    "LoginName": "hrm",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "2F859266-27B7-4DD5-BC85-158A92AD8991",
    "FirstName": "Michael",
    "MiddleName": "E",
    "LastName": "Holm",
    "Phone": "630-252-1392",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Web Developer"
}, {
    "Id": 1028,
    "LoginName": "pk4@ornl.gov",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "D7DF0E4F-7535-4DDD-95FF-493C6D104832",
    "FirstName": "PriyaTest",
    "LastName": "Kumar",
    "Phone": "865-381-1120 e7004",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist"
}, {
    "Id": 1080,
    "LoginName": "h7j@ornl.gov",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "B82F544C-D175-4B4A-9F56-C3BBD2624818",
    "FirstName": "JeffreyTest",
    "MiddleName": "B",
    "LastName": "Herd",
    "Phone": "865-381-1120 e7003",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information System Analyst Level 2"
}, {
    "Id": 1082,
    "LoginName": "azm",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "E42F96A3-1C9C-445B-8D01-A678D74711BD",
    "FirstName": "Aaron",
    "MiddleName": "T",
    "LastName": "Myers",
    "Phone": "865-241-4321",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Geospatial Systems Architect"
}, {
    "Id": 1094,
    "LoginName": "j48",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "17C06BFD-1B04-4DA2-953C-1AE4BA3702CA",
    "FirstName": "Jacqueline",
    "MiddleName": "M",
    "LastName": "Croft",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Business Manager"
}, {
    "Id": 1097,
    "LoginName": "d7d",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "146650C5-4073-4F1A-B4C8-9EF5EEFEB8C4",
    "FirstName": "Joshua",
    "MiddleName": "M",
    "LastName": "Dunn",
    "Phone": "865-576-8826",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": true,
    "JobTitle": ".NET and Mobile Application Developer"
}, {
    "Id": 1102,
    "LoginName": "g2b",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "C243C267-9FC1-4CCE-A7D2-4524092DEE40",
    "FirstName": "Jonathan",
    "MiddleName": "C",
    "LastName": "Gilbert",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer II"
}, {
    "Id": 1130,
    "LoginName": "J7A",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "C32D30F0-955A-4CD0-BA14-2CECEBA5B8B4",
    "FirstName": "Jay",
    "MiddleName": "K",
    "LastName": "Cabler",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Sp."
}, {
    "Id": 1210,
    "LoginName": "lr6",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "E1DC31BF-B51D-4F3E-A69A-7858F28984B5",
    "FirstName": "Ramon",
    "MiddleName": "M",
    "LastName": "Lee",
    "Phone": "865-360-7877",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "software engineer"
}, {
    "Id": 1237,
    "LoginName": "s29",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "7D81F742-A844-499B-8A56-995ADD1C40D1",
    "FirstName": "Stephen",
    "MiddleName": "P",
    "LastName": "Lamb",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "IT Analyst"
}, {
    "Id": 1280,
    "LoginName": "n2c",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "619C368F-5CB6-46F6-872D-D53ADB7CF39C",
    "FirstName": "Nathan",
    "MiddleName": "S",
    "LastName": "Coffey",
    "Phone": "865-241-5853",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1285,
    "LoginName": "k31",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "D3B29854-EB2E-4B45-BA7A-5F9ED6A38D7B",
    "FirstName": "Kacy",
    "MiddleName": "S",
    "LastName": "Schnake",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist"
}, {
    "Id": 1291,
    "LoginName": "b54",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "6F0EE56C-B7ED-4F57-8F4D-A590A8079E39",
    "FirstName": "Byron",
    "MiddleName": "J",
    "LastName": "Roland",
    "Phone": "931-248-2862",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1297,
    "LoginName": "w27",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "98B694F5-6CF3-4B11-BD2F-91AA9C1A5AEC",
    "FirstName": "William",
    "MiddleName": "S",
    "LastName": "Johnson",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist\/Analyst"
}, {
    "Id": 1426,
    "LoginName": "rji",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "8D8B3547-B3A2-4A0D-A23C-3FC877893916",
    "FirstName": "Jill",
    "MiddleName": "D",
    "LastName": "Rochat",
    "Phone": "865-257-5455",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1429,
    "LoginName": "tnp",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "C7520315-79C9-4CDE-94F6-77256732C3BA",
    "FirstName": "Taylor",
    "MiddleName": "C",
    "LastName": "Patterson",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist\/Analyst"
}, {
    "Id": 1492,
    "LoginName": "lt6",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "1631B395-C6D1-4CD8-B319-058D5813315D",
    "FirstName": "James",
    "MiddleName": "R",
    "LastName": "Lott",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist\/Analyst"
}, {
    "Id": 1493,
    "LoginName": "tnv",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "675DA111-99EA-473D-8AED-DD3CDFE8E4B6",
    "FirstName": "Trey",
    "MiddleName": "G",
    "LastName": "Neal",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist"
}, {
    "Id": 1505,
    "LoginName": "g91",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "3C817E7B-29F3-4A68-9E62-009CD0B8EFC6",
    "FirstName": "Gabriel",
    "MiddleName": "J",
    "LastName": "Hanas",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1553,
    "LoginName": "o7a",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "5933F14B-A51B-4502-AB46-1DF30A95595F",
    "FirstName": "Audrey",
    "MiddleName": "S",
    "LastName": "Jackson",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "QA Specialist III"
}, {
    "Id": 1554,
    "LoginName": "o9a",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "2A222E5F-19B8-48B1-A988-2A374CBC8D76",
    "FirstName": "Alton",
    "MiddleName": "B",
    "LastName": "Coalter",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "QA Analyst IV"
}, {
    "Id": 1722,
    "LoginName": "qbn",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "7130352E-DC89-4D84-9890-2D95DD225CAE",
    "FirstName": "Chad",
    "MiddleName": "M",
    "LastName": "Bloomfield",
    "Phone": "630-252-4697",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1725,
    "LoginName": "vgm",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "048AB0D3-5EC8-4A5D-A936-DC0D4CC93912",
    "FirstName": "Marina",
    "LastName": "Gombos",
    "Phone": "865-381-1128",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist\/Analyst"
}, {
    "Id": 1732,
    "LoginName": "qxa",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "DEE371CF-3570-4687-AD1D-64FF54CCF177",
    "FirstName": "Tony",
    "MiddleName": "D",
    "LastName": "Abston",
    "Phone": "865-694-5600",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1748,
    "LoginName": "qud",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "FB84F939-8C28-4D39-AD97-B49E5EF2F329",
    "FirstName": "Doug",
    "MiddleName": "A",
    "LastName": "Johnson",
    "Phone": "630-252-8667",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software Engineer"
}, {
    "Id": 1909,
    "LoginName": "1rd",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "8E95DF1D-7F68-4FDC-B908-D0226B17D1E7",
    "FirstName": "Rebekah",
    "MiddleName": "E",
    "LastName": "Diring",
    "Phone": "895-694-5600",
    "Fax": "865-690-4438",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Software UI\/UX Designer"
}, {
    "Id": 2095,
    "LoginName": "2dd",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "3A477D08-4861-47FC-B4F8-837EC4AD8C56",
    "FirstName": "David",
    "MiddleName": "K",
    "LastName": "Davis",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Information Management Specialist \/ Analyst"
}, {
    "Id": 2130,
    "LoginName": "s0z",
    "EmailAddress": "mark.brown@cadre5.com",
    "AIMSId": "E9D67BFD-1F71-4CDE-B1BA-A3308606DBB1",
    "FirstName": "Scott",
    "MiddleName": "A",
    "LastName": "Capps",
    "Phone": "865-381-1120",
    "G2UserInd": true,
    "FedInd": false,
    "ActiveInd": true,
    "MobileUserInd": false,
    "JobTitle": "Engineer"
}]';
	DECLARE @systemUserId INT = 214;
	SET IDENTITY_INSERT Security.Person ON;
	
	ALTER TABLE Security.Person DISABLE TRIGGER trgG2_NSSubscriber_Insert;
	ALTER TABLE Security.Person DISABLE TRIGGER trgG2_NSSubscriber_Update;
	ALTER TABLE Security.Person DISABLE TRIGGER trgG2_NSSubscriber_Delete;


	PRINT 'Insert SystemUser ...'
	INSERT INTO Security.Person (Id, LoginName, EmailAddress, AIMSId, FirstName, MiddleName, LastName, Phone, Fax, G2UserInd, FedInd, ActiveInd, MobileUserInd, JobTitle, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT @systemUserId, 'SystemUser', 'mark.brown@cadre5.com', NULL, 'System', NULL, 'User', NULL, NULL, 1, 0, 1, 0, NULL, @systemUserId, @systemUserId, @systemUserId, @systemUserId
	EXCEPT 
	SELECT p.Id, p.LoginName, p.EmailAddress, p.AIMSId, p.FirstName, p.MiddleName, p.LastName, p.Phone, p.Fax, p.G2UserInd, p.FedInd, p.ActiveInd, p.MobileUserInd, p.JobTitle, p.CreatedBy, p.CreatedAs, p.LastModifiedBy, p.LastModifiedAs 
	FROM Security.Person p;
	
	PRINT 'Inserting devs ...'
	INSERT INTO Security.Person (Id, LoginName, EmailAddress, AIMSId, FirstName, MiddleName, LastName, Phone, Fax, G2UserInd, FedInd, ActiveInd, MobileUserInd, JobTitle, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.LoginName, oj.EmailAddress, oj.AIMSId, oj.FirstName, oj.MiddleName, oj.LastName, oj.Phone, oj.Fax, oj.G2UserInd, oj.FedInd, oj.ActiveInd, oj.MobileUserInd, oj.JobTitle, @systemUserId, @systemUserId, @systemUserId, @systemUserId
	FROM OPENJSON(@personData) WITH (Id INT '$.Id', LoginName NVARCHAR(70) '$.LoginName', EmailAddress NVARCHAR(50) '$.EmailAddress', AIMSId NVARCHAR(128) '$.AIMSId', 
									 FirstName NVARCHAR(50) '$.FirstName', MiddleName NVARCHAR(50) '$.MiddleName', LastName NVARCHAR(50) '$.LastName', Phone NVARCHAR(50) '$.Phone', 
									 Fax NVARCHAR(50) '$.Fax', G2UserInd BIT '$.G2UserInd', FedInd BIT '$.FedInd', ActiveInd BIT '$.ActiveInd', MobileUserInd BIT '$.MobileUserInd', JobTitle NVARCHAR(150) '$.JobTitle')  oj
	EXCEPT
	SELECT p.Id, p.LoginName, p.EmailAddress, p.AIMSId, p.FirstName, p.MiddleName, p.LastName, p.Phone, p.Fax, p.G2UserInd, p.FedInd, p.ActiveInd, p.MobileUserInd, p.JobTitle, @systemUserId, @systemUserId, @systemUserId, @systemUserId
	FROM Security.Person p;

	ALTER TABLE Security.Person ENABLE TRIGGER trgG2_NSSubscriber_Insert;
	ALTER TABLE Security.Person ENABLE TRIGGER trgG2_NSSubscriber_Update;
	ALTER TABLE Security.Person ENABLE TRIGGER trgG2_NSSubscriber_Delete;

	SET IDENTITY_INSERT Security.Person OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH

--SELECT * 
--FROM [NN1STGDB03.ORNL.GOV].QA_Setup.G2.Person p
--INNER JOIN G2.Security.Person pers ON p.Id = pers.Id
--WHERE pers.ActiveInd = 1

