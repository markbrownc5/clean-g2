BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @crt NVARCHAR(MAX) = '[{
    "Id": 1,
    "ChangeRequestTypeGroupId": 1,
    "Name": "HQ AFP",
    "Description": "Financial Plan Change",
    "DisplayName": "HQ AFP",
    "SortOrder": 3,
    "FinancialTransactionInd": true,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 5,
    "ChangeRequestTypeGroupId": 2,
    "Name": "Status",
    "Description": "Project Schedule Status Updates",
    "DisplayName": "Performance Reporting",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 8,
    "ChangeRequestTypeGroupId": 10,
    "Name": "Lifecycle Budget",
    "Description": "Lifecycle Budget Changes",
    "DisplayName": "Lifecycle Budget",
    "SortOrder": 2,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 14,
    "ChangeRequestTypeGroupId": 10,
    "Name": "Historic Spend Plan",
    "Description": "Spending Plan Change in Real Time",
    "DisplayName": "Historic Spend Plan",
    "SortOrder": 3,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 15,
    "ChangeRequestTypeGroupId": 2,
    "Name": "Realtime Status",
    "Description": "Realtime Project Schedule Status Updates",
    "DisplayName": "Performance Reporting",
    "SortOrder": 2,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 17,
    "ChangeRequestTypeGroupId": 1,
    "Name": "No Funds Work Authorization",
    "Description": "No Funds Work Authorization",
    "DisplayName": "NFWA",
    "SortOrder": 1,
    "FinancialTransactionInd": true,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 18,
    "ChangeRequestTypeGroupId": 7,
    "Name": "New Appropriation",
    "Description": "Beginning of fiscal year congressional appropriation",
    "DisplayName": "New Appropriation",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 19,
    "ChangeRequestTypeGroupId": 7,
    "Name": "Supplemental Funding",
    "Description": "Supplemental appropriations received throughout the year",
    "DisplayName": "Supplemental Funding",
    "SortOrder": 2,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 20,
    "ChangeRequestTypeGroupId": 7,
    "Name": "Removed Funding",
    "Description": "Funding removed from a project by NA62",
    "DisplayName": "Removed Funding",
    "SortOrder": 3,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 21,
    "ChangeRequestTypeGroupId": 1,
    "Name": "Local AFP",
    "Description": "Follow on to Fin Plan request",
    "DisplayName": "Local AFP",
    "SortOrder": 2,
    "FinancialTransactionInd": true,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 22,
    "ChangeRequestTypeGroupId": 10,
    "Name": "Spend Plan",
    "Description": "Spend Plan Changes",
    "DisplayName": "Spend Plan",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 23,
    "ChangeRequestTypeGroupId": 10,
    "Name": "Baseline",
    "Description": "Schedule, Scope and Metric changes",
    "DisplayName": "Baseline",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 24,
    "ChangeRequestTypeGroupId": 11,
    "Name": "Cost",
    "Description": "Cost changes",
    "DisplayName": "Cost",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 25,
    "ChangeRequestTypeGroupId": 12,
    "Name": "Travel",
    "Description": "Travel Changes",
    "DisplayName": "Travel",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 26,
    "ChangeRequestTypeGroupId": 13,
    "Name": "Alarm Response Training",
    "Description": "Alarm Response Training Changes",
    "DisplayName": "ART",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 27,
    "ChangeRequestTypeGroupId": 13,
    "Name": "Personal Radiation Detector Training",
    "Description": "Personal Radiation Detector Training Changes",
    "DisplayName": "PRDT",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 28,
    "ChangeRequestTypeGroupId": 13,
    "Name": "Search and Secure",
    "Description": "Search and Secure Changes",
    "DisplayName": "SS",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 29,
    "ChangeRequestTypeGroupId": 13,
    "Name": "Table Top Exercise",
    "Description": "Table Top Exercise Changes",
    "DisplayName": "TTX",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 30,
    "ChangeRequestTypeGroupId": 13,
    "Name": "Table Top Exercise Coordination Visit",
    "Description": "Table Top Exercise Coordination Visit Changes",
    "DisplayName": "TTXCV",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 31,
    "ChangeRequestTypeGroupId": 14,
    "Name": "Indirect and Other Direct Cost",
    "Description": "Indirect and Other Direct Cost Changes",
    "DisplayName": "Indirect and Other Direct Cost",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 32,
    "ChangeRequestTypeGroupId": 15,
    "Name": "Budget Formulation Base Case",
    "Description": "Budget Formulation Base Case requests",
    "DisplayName": "Base Case",
    "Abbreviation": "BC",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 33,
    "ChangeRequestTypeGroupId": 15,
    "Name": "Budget Formulation Indirects",
    "Description": "Budget Formulation Indirect requests",
    "DisplayName": "Indirects",
    "Abbreviation": "IND",
    "SortOrder": 4,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 34,
    "ChangeRequestTypeGroupId": 15,
    "Name": "Budget Formulation Over Target",
    "Description": "Budget Formulation Over Target requests",
    "DisplayName": "Over Target",
    "Abbreviation": "OT",
    "SortOrder": 2,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 35,
    "ChangeRequestTypeGroupId": 16,
    "Name": "Year End Forecast",
    "Description": "Year End Forecast",
    "DisplayName": "Year End Forecast",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 36,
    "ChangeRequestTypeGroupId": 17,
    "Name": "Work Breakdown Structure Plan",
    "Description": "Work Breakdown Structure Plan Change",
    "DisplayName": "WBS Plan",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 37,
    "ChangeRequestTypeGroupId": 18,
    "Name": "Funding Requests",
    "Description": "Funding Requests",
    "DisplayName": "Funding Requests",
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 38,
    "ChangeRequestTypeGroupId": 15,
    "Name": "Budget Formulation Other Direct Funds",
    "Description": "Budget Formulation Other Direct Fund requests",
    "DisplayName": "Other Direct Funds",
    "Abbreviation": "ODF",
    "SortOrder": 3,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 39,
    "ChangeRequestTypeGroupId": 19,
    "Name": "Asset Management",
    "Description": "Asset Management Changes",
    "DisplayName": "Asset Management",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 40,
    "ChangeRequestTypeGroupId": 20,
    "Name": "Competitive Proposal",
    "Description": "Competitive Proposal Requests",
    "DisplayName": "Competitive",
    "SortOrder": 1,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 41,
    "ChangeRequestTypeGroupId": 20,
    "Name": "Directed Proposal",
    "Description": "Directed Proposal Requests",
    "DisplayName": "Directed",
    "SortOrder": 2,
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 42,
    "ChangeRequestTypeGroupId": 21,
    "Name": "Activity Funding Request",
    "Description": "Activity Funding Request",
    "DisplayName": "AFR",
    "Abbreviation": "AFR",
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}, {
    "Id": 43,
    "ChangeRequestTypeGroupId": 21,
    "Name": "Reconcile Activity Funding Request",
    "Description": "Reconcile Activity Funding Request",
    "DisplayName": "Reconcile AFR",
    "Abbreviation": "Recon AFR",
    "FinancialTransactionInd": false,
    "AllowOnlyOneChangeRequestPerPeriodInd": false
}]';

	SET IDENTITY_INSERT G2.ChangeRequestType ON;

	INSERT INTO G2.ChangeRequestType (Id, ChangeRequestTypeGroupId, Name, Description, DisplayName, Abbreviation, SortOrder, FinancialTransactionInd, AllowOnlyOneChangeRequestPerPeriodInd)
	SELECT c.Id, c.ChangeRequestTypeGroupId, c.Name, c.Description, c.DisplayName, c.Abbreviation, c.SortOrder, c.FinancialTransactionInd, c.AllowOnlyOneChangeRequestPerPeriodInd
	FROM
		OPENJSON(@crt)
			WITH (
				Id INT '$.Id',
				ChangeRequestTypeGroupId INT '$.ChangeRequestTypeGroupId',
				Name NVARCHAR(50) '$.Name',
				Description NVARCHAR(50) '$.Description',
				DisplayName NVARCHAR(50) '$.DisplayName',
				Abbreviation NVARCHAR(50) '$.Abbreviation',
				SortOrder INT '$.SortOrder',
				FinancialTransactionInd BIT '$.FinancialTransactionInd',
				AllowOnlyOneChangeRequestPerPeriodInd BIT '$.AllowOnlyOneChangeRequestPerPeriodInd'
			) c
	EXCEPT
	SELECT crt.Id, crt.ChangeRequestTypeGroupId, crt.Name, crt.Description, crt.DisplayName, crt.Abbreviation, crt.SortOrder, crt.FinancialTransactionInd, crt.AllowOnlyOneChangeRequestPerPeriodInd
	FROM G2.ChangeRequestType crt;

	SET IDENTITY_INSERT G2.ChangeRequestType OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH