BEGIN TRY

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRANSACTION;

	DECLARE @userId INT
		= 614,
		@pcv NVARCHAR(MAX) = '[{
    "Id": 1,
    "PreferenceChoiceId": 1,
    "Name": "On",
    "DisplayOrder": 1,
    "DeleteIfChosen": false
}, {
    "Id": 2,
    "PreferenceChoiceId": 1,
    "Name": "Off",
    "DisplayOrder": 2,
    "DeleteIfChosen": true
}, {
    "Id": 3,
    "PreferenceChoiceId": 2,
    "Name": "Opt-in",
    "DisplayOrder": 1,
    "DeleteIfChosen": false
}, {
    "Id": 4,
    "PreferenceChoiceId": 2,
    "Name": "Opt-out",
    "DisplayOrder": 2,
    "DeleteIfChosen": false
}, {
    "Id": 7,
    "PreferenceChoiceId": 5,
    "Name": "Alphabetical",
    "DisplayOrder": 1,
    "DeleteIfChosen": false
}, {
    "Id": 8,
    "PreferenceChoiceId": 5,
    "Name": "Organizational",
    "DisplayOrder": 2,
    "DeleteIfChosen": true
}, {
    "Id": 15,
    "PreferenceChoiceId": 7,
    "Name": "Show",
    "DisplayOrder": 1,
    "DeleteIfChosen": false
}, {
    "Id": 16,
    "PreferenceChoiceId": 7,
    "Name": "Hide",
    "DisplayOrder": 2,
    "DeleteIfChosen": false
}, {
    "Id": 5,
    "PreferenceChoiceId": 4,
    "Name": "FL",
    "DisplayOrder": 1,
    "DeleteIfChosen": false
}]' ;


	SET IDENTITY_INSERT G2.PreferenceChoiceValue ON;

	INSERT INTO G2.PreferenceChoiceValue (Id, PreferenceChoiceId, Name, DisplayOrder, DeleteIfChosen, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT p.Id, p.PreferenceChoiceId, p.Name, p.DisplayOrder, p.DeleteIfChosen, @userId, @userId, @userId, @userId
	FROM
		OPENJSON(@pcv)
			WITH (
				Id INT '$.Id',
				PreferenceChoiceId INT '$.PreferenceChoiceId',
				Name VARCHAR(50) '$.Name',
				DisplayOrder INT '$.DisplayOrder',
				DeleteIfChosen BIT '$.DeleteIfChosen'
			) p
	EXCEPT
	SELECT pcv.Id, pcv.PreferenceChoiceId, pcv.Name, pcv.DisplayOrder, pcv.DeleteIfChosen, @userId, @userId, @userId, @userId
	FROM G2.PreferenceChoiceValue pcv;


	SET IDENTITY_INSERT G2.PreferenceChoiceValue OFF;

	COMMIT TRANSACTION;

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END;

	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid), ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;

END CATCH;