BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Budget"
}, {
    "Id": 2,
    "Name": "Funding"
}, {
    "Id": 3,
    "Name": "Cost\/Obligation"
}, {
    "Id": 4,
    "Name": "Schedule"
}, {
    "Id": 5,
    "Name": "Indirect Cost"
}, {
    "Id": 6,
    "Name": "Other Direct Cost"
}, {
    "Id": 7,
    "Name": "Budget Formulation"
}, {
    "Id": 8,
    "Name": "Schedule Display Minimum"
}, {
    "Id": 9,
    "Name": "SpendPlan"
}, {
    "Id": 10,
    "Name": "Budget Formulation Group"
}, {
    "Id": 11,
    "Name": "IP Linkable"
}]';

	SET IDENTITY_INSERT G2.ConfigurationType ON;

	INSERT INTO G2.ConfigurationType (Id, Name, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.Name, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (Id INT '$.Id', Name NVARCHAR(25) '$.Name') oj
	EXCEPT
	SELECT ct.Id, ct.Name, @user, @user, @user, @user
	FROM G2.ConfigurationType ct;

	SET IDENTITY_INSERT G2.ConfigurationType OFF;
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH