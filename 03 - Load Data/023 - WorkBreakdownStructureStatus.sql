BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @wbsStatus NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Active",
    "Description": "An active work item."
}, {
    "Id": 2,
    "Name": "Inactive",
    "Description": "An inactive work item allows no new change requests or permission assignments of any kind."
}, {
    "Id": 3,
    "Name": "New",
    "Description": "A newly entered work item to be approved through the baseline process."
}, {
    "Id": 4,
    "Name": "Voided",
    "Description": "A work item voided prior to being sent though the baseline process."
}, {
    "Id": 5,
    "Name": "Rejected",
    "Description": "A work item rejected as part of the baseline process."
}, {
    "Id": 6,
    "Name": "Deleted",
    "Description": "A work item that has been deleted."
}, {
    "Id": 7,
    "Name": "Cancelled",
    "Description": "A work item that has been cancelled."
}]', @wbsStatusState NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Active",
    "Description": "Statuses that are currently active."
}, {
    "Id": 2,
    "Name": "Inactive",
    "Description": "Statuses that are not active."
}, {
    "Id": 3,
    "Name": "Active and Cancelled",
    "Description": "Active and cancelled statuses."
}, {
    "Id": 4,
    "Name": "Open Request",
    "Description": "Open request statuses."
}, {
    "Id": 5,
    "Name": "Active and Open Request",
    "Description": "Active and open request statuses."
}]', @wbsStatusStateXref NVARCHAR(MAX) = '[{
    "Id": 1,
    "WorkBreakdownStructureStatusId": 1,
    "WorkBreakdownStructureStatusStateId": 1
}, {
    "Id": 2,
    "WorkBreakdownStructureStatusId": 1,
    "WorkBreakdownStructureStatusStateId": 3
}, {
    "Id": 3,
    "WorkBreakdownStructureStatusId": 1,
    "WorkBreakdownStructureStatusStateId": 5
}, {
    "Id": 4,
    "WorkBreakdownStructureStatusId": 2,
    "WorkBreakdownStructureStatusStateId": 2
}, {
    "Id": 5,
    "WorkBreakdownStructureStatusId": 3,
    "WorkBreakdownStructureStatusStateId": 4
}, {
    "Id": 6,
    "WorkBreakdownStructureStatusId": 3,
    "WorkBreakdownStructureStatusStateId": 5
}, {
    "Id": 7,
    "WorkBreakdownStructureStatusId": 4,
    "WorkBreakdownStructureStatusStateId": 2
}, {
    "Id": 8,
    "WorkBreakdownStructureStatusId": 5,
    "WorkBreakdownStructureStatusStateId": 2
}, {
    "Id": 9,
    "WorkBreakdownStructureStatusId": 6,
    "WorkBreakdownStructureStatusStateId": 2
}, {
    "Id": 10,
    "WorkBreakdownStructureStatusId": 7,
    "WorkBreakdownStructureStatusStateId": 2
}, {
    "Id": 11,
    "WorkBreakdownStructureStatusId": 7,
    "WorkBreakdownStructureStatusStateId": 3
}]';

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatus ON;

	INSERT INTO G2.WorkBreakdownStructureStatus (Id, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT wbss.Id, wbss.Name, wbss.Description, @userId, @userId, @userId, @userId
	FROM OPENJSON(@wbsStatus) WITH (Id INT '$.Id', Name VARCHAR(25) '$.Name', Description VARCHAR(200) '$.Description') wbss 
	EXCEPT
	SELECT wbss.Id, wbss.Name, wbss.Description, @userId, @userId, @userId, @userId
	FROM G2.WorkBreakdownStructureStatus wbss

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatus OFF;

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatusState ON;

	INSERT INTO G2.WorkBreakdownStructureStatusState (Id, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT wbss.Id, wbss.Name, wbss.Description, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@wbsStatusState) WITH (Id INT '$.Id', Name VARCHAR(25) '$.Name', Description VARCHAR(100) '$.Description') wbss
	EXCEPT
	SELECT wbss.Id, wbss.Name, wbss.Description, @userId, @userId, @userId, @userId 
	FROM G2.WorkBreakdownStructureStatusState wbss

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatusState OFF;

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatusStateXref ON;

	INSERT INTO G2.WorkBreakdownStructureStatusStateXref (Id, WorkBreakdownStructureStatusId, WorkBreakdownStructureStatusStateId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT wbssx.Id, wbssx.WorkBreakdownStructureStatusId, wbssx.WorkBreakdownStructureStatusStateId, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@wbsStatusStateXref) WITH (Id INT '$.Id', WorkBreakdownStructureStatusId INT '$.WorkBreakdownStructureStatusId', WorkBreakdownStructureStatusStateId INT '$.WorkBreakdownStructureStatusStateId') wbssx
	EXCEPT
	SELECT wbssx.Id, wbssx.WorkBreakdownStructureStatusId, wbssx.WorkBreakdownStructureStatusStateId, @userId, @userId, @userId, @userId 
	FROM G2.WorkBreakdownStructureStatusStateXref wbssx

	SET IDENTITY_INSERT G2.WorkBreakdownStructureStatusStateXref OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH