BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @ot NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Program"
}, {
    "Id": 2,
    "Name": "HQ"
}, {
    "Id": 3,
    "Name": "Front Office"
}, {
    "Id": 4,
    "Name": "Functional Office"
}, {
    "Id": 5,
    "Name": "Field Office"
}, {
    "Id": 6,
    "Name": "Reporting Entity"
}, {
    "Id": 7,
    "Name": "Sub-Office"
}, {
    "Id": 8,
    "Name": "Office"
}, {
    "Id": 9,
    "Name": "NNSA Site"
}, {
    "Id": 10,
    "Name": "NNSA Contractor Site"
}, {
    "Id": 11,
    "Name": "SMP Site"
}]';

	SET IDENTITY_INSERT Contact.OrganizationType ON;

	INSERT INTO Contact.OrganizationType (Id, Name, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT ot.Id, ot.Name, @userId, @userId, @userId, @userId
	FROM OPENJSON(@ot) WITH (Id INT '$.Id', Name VARCHAR(50) '$.Name') ot
	EXCEPT
	SELECT ot.Id, ot.Name, @userId, @userId, @userId, @userId
	FROM Contact.OrganizationType ot;

	SET IDENTITY_INSERT Contact.OrganizationType OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH