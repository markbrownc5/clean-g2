BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @generalUserRoleId INT = Security.fnGetGeneralUserRoleId(), @programAdminRoleId INT = Security.fnGetProgramAdministratorRoleId();

	INSERT INTO Security.PersonRole (SubSystemId, PersonId, RoleId, WorkBreakdownStructureId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT ss.Id, p.Id, r.Id, 1, @userId, @userId, @userId, @userId 
	FROM Security.Person p
	CROSS JOIN G2.SubSystem ss
	CROSS JOIN Security.Role r
	WHERE r.Id IN (@generalUserRoleId, @programAdminRoleId)
	EXCEPT
	SELECT persR.SubSystemId, persR.PersonId, persR.RoleId, persR.WorkBreakdownStructureId, @userId, @userId, @userId, @userId
	FROM Security.PersonRole persR;

	INSERT INTO G2.PersonPreferenceChoice (PersonId, PreferenceChoiceId, PreferenceChoiceValueId)
	SELECT p.Id, pc.Id, pcv.Id
	FROM Security.Person p
	CROSS JOIN G2.PreferenceChoice pc
	CROSS JOIN G2.PreferenceChoiceValue pcv
	WHERE pc.Id IN (4)
	AND pcv.Name = 'FL'
	EXCEPT
	SELECT ppc.PersonId, ppc.PreferenceChoiceId, ppc.PreferenceChoiceValueId 
	FROM G2.PersonPreferenceChoice ppc;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH