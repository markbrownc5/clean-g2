BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @prt NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Employee",
    "Description": "Employee"
}, {
    "Id": 2,
    "Name": "Internal Organization",
    "Description": "Internal Organization"
}, {
    "Id": 3,
    "Name": "Traveler",
    "Description": "Person entered for Travel"
}, {
    "Id": 4,
    "Name": "Traveler Organization",
    "Description": "External Organization entered for Travel Request"
}, {
    "Id": 5,
    "Name": "Parent Organization Group",
    "Description": "Organization Grouping Entity"
}, {
    "Id": 6,
    "Name": "Performer",
    "Description": "Organization performing work"
}, {
    "Id": 7,
    "Name": "Performer Group",
    "Description": "Grouping of performers"
}, {
    "Id": 8,
    "Name": "Allottee",
    "Description": "Funding organizations grouping performers"
}, {
    "Id": 9,
    "Name": "Lead Lab",
    "Description": "A Performer overseeing a given task"
}, {
    "Id": 10,
    "Name": "Event Contact",
    "Description": "A contact for events in Event Manager"
}, {
    "Id": 11,
    "Name": "Budget Performer",
    "Description": "Organization assigned budget"
}, {
    "Id": 12,
    "Name": "Org Subsystem Map",
    "Description": "Mapping Top Level Org to a Subsystem"
}, {
    "Id": 13,
    "Name": "Budget Formulation Performer",
    "Description": "A performer that can plan budgets."
}, {
    "Id": 14,
    "Name": "Indirect and Other Direct Cost Performer",
    "Description": "A performer that can collect other costs"
}, {
    "Id": 15,
    "Name": "Infrastructure Planning Site",
    "Description": "Sites for Infrastructure Planning Project"
}, {
    "Id": 16,
    "Name": "SMP Performer",
    "Description": "Sites that collect SMPs"
}, {
    "Id": 17,
    "Name": "Annex1 Lab",
    "Description": "A Performer mapping for use in Annex1 validation"
}]';

	SET IDENTITY_INSERT Contact.PartyRoleType ON;
	
	ALTER TABLE Contact.PartyRoleType DISABLE TRIGGER trgPartyRoleType_Insert_Auto_Create_FN;

	INSERT INTO Contact.PartyRoleType (Id, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT prt.Id, prt.Name, prt.Description, @userId, @userId, @userId, @userId
	FROM OPENJSON(@prt) WITH (Id INT '$.Id', Name VARCHAR(50) '$.Name', Description VARCHAR(50) '$.Description') prt
	EXCEPT 
	SELECT prt.Id, prt.Name, prt.Description, @userId, @userId, @userId, @userId 
	FROM Contact.PartyRoleType prt
	
	ALTER TABLE Contact.PartyRoleType ENABLE TRIGGER trgPartyRoleType_Insert_Auto_Create_FN;

	SET IDENTITY_INSERT Contact.PartyRoleType OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH