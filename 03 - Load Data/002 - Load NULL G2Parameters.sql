BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @systemUserId INT = 614;

	
	DECLARE @paramData NVARCHAR(MAX) = '
	[{
		"ParmKey": "Excel Export\/Import Password",
		"Value": "L!ght_Gone_2?!?",
		"Datatype": "nvarchar",
		"Description": "The password used to protect the excel export\/import spreadsheets."
	}, {
		"ParmKey": "Scheduled Maintenance Lockout Threshhold",
		"Value": "15",
		"Datatype": "int",
		"Description": "The number of minutes prior to a scheduled G2 maintenance in which users begin to get locked out."
	}, {
		"ParmKey": "System User",
		"Value": "214",
		"Datatype": "int",
		"Description": "User for System Created Records"
	}, {
		"ParmKey": "NSAT User",
		"Value": "1074",
		"Datatype": "int",
		"Description": "Used for actions on behalf of the NSAT System at Y-12."
	}, {
		"ParmKey": "AIMS User",
		"Value": "613",
		"Datatype": "int",
		"Description": "Used for actions on behalf of the AIMS System."
	}, {
		"ParmKey": "NA21 SubSystem Split Date",
		"Value": "Nov  5 2015 11:14PM",
		"Datatype": "datetime",
		"Description": "Represents the time at the end of the NA-21 GMS\/MMM split."
	}, {
		"ParmKey": "Current Release",
		"Value": "7.5",
		"Datatype": "nvarchar",
		"Description": "Current Release of DB code"
	}, {
		"ParmKey": "Notification Services Enabled",
		"Value": "0",
		"Datatype": "bit",
		"Description": "If this is 1, notifications are enabled; if this is 0 notification services aren''t enabled.  Actions that would typically communication with notification services won''t attempt to do so."
	}]';

	INSERT INTO G2.G2Parameters (SubSystemId, ParmKey, Value, Datatype, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.SubSystemId, oj.ParmKey, oj.Value, oj.DataType, oj.Description, @systemUserId, @systemUserId, @systemUserId, @systemUserId
	FROM OPENJSON(@paramData) WITH (SubSystemId INT '$.SubSystemId', ParmKey VARCHAR(50) '$.ParmKey', Value VARCHAR(200) '$.Value', DataType NVARCHAR(50) '$.Datatype', Description NVARCHAR(256) '$.Description') oj
	EXCEPT 
	SELECT gp.SubSystemId, gp.ParmKey, gp.Value, gp.Datatype, gp.Description, @systemUserId, @systemUserId, @systemUserId, @systemUserId
	FROM G2.G2Parameters gp;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH