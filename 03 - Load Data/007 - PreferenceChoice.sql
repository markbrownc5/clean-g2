BEGIN TRY

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRANSACTION;

	DECLARE @userId INT
		= 614,
		@pc NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Enable Proxy",
    "AllowMultipleValues": false,
    "DisplayText": "Text to display in UI"
}, {
    "Id": 2,
    "Name": "Receive Joule Milestone Complete Notification",
    "AllowMultipleValues": false,
    "DisplayText": "Joule Milestone Completion"
}, {
    "Id": 3,
    "Name": "Project WBS Sort",
    "AllowMultipleValues": false,
    "DisplayText": "Sort projects by WBS"
}, {
    "Id": 4,
    "Name": "Default Sub-System",
    "AllowMultipleValues": false,
    "DisplayText": "Chosen Default Sub-System for Login"
}, {
    "Id": 5,
    "Name": "Address Book Default View",
    "AllowMultipleValues": false,
    "DisplayText": "Chosen Default View for Address Book"
}, {
    "Id": 6,
    "Name": "Funding Request Advanced BNR Editor",
    "AllowMultipleValues": false,
    "DisplayText": "Funding Request Advanced BNR Editor"
}, {
    "Id": 7,
    "Name": "GIS Zoom Tool Tip",
    "AllowMultipleValues": false,
    "DisplayText": "Show GIS Zoom Tool Tip"
}]' ;

	SET IDENTITY_INSERT G2.PreferenceChoice ON;

	INSERT INTO G2.PreferenceChoice (Id, Name, AllowMultipleValues, DisplayText, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT p.Id, p.Name, p.AllowMultipleValues, p.DisplayText, @userId, @userId, @userId, @userId
	FROM
		OPENJSON(@pc)
			WITH (Id INT '$.Id',
				Name VARCHAR(50) '$.Name',
				AllowMultipleValues BIT '$.AllowMultipleValues',
				DisplayText VARCHAR(2000) '$.DisplayText'
			) p
	EXCEPT
	SELECT pc.Id, pc.Name, pc.AllowMultipleValues, pc.DisplayText, @userId, @userId, @userId, @userId
	FROM G2.PreferenceChoice pc;

	SET IDENTITY_INSERT G2.PreferenceChoice OFF;

	COMMIT TRANSACTION;

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END;

	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid), ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;

END CATCH;