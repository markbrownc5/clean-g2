BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 7,
    "SubSystemId": 1,
    "Name": "Management",
    "DisplayOrder": 9
}, {
    "Id": 8,
    "SubSystemId": 1,
    "Name": "Cross-Cutting",
    "DisplayOrder": 8
}, {
    "Id": 9,
    "SubSystemId": 1,
    "Name": "Construction",
    "DisplayOrder": 6
}, {
    "Id": 10,
    "SubSystemId": 1,
    "Name": "Recapitalization",
    "DisplayOrder": 3
}, {
    "Id": 11,
    "SubSystemId": 1,
    "Name": "Maintenance",
    "DisplayOrder": 2
}, {
    "Id": 12,
    "SubSystemId": 1,
    "Name": "Operations",
    "DisplayOrder": 1
}, {
    "Id": 26,
    "SubSystemId": 1,
    "Name": "Disposition",
    "DisplayOrder": 4
}, {
    "Id": 27,
    "SubSystemId": 1,
    "Name": "AMP",
    "DisplayOrder": 5
}, {
    "Id": 28,
    "SubSystemId": 1,
    "Name": "SEMO",
    "DisplayOrder": 7
}]';

	SET IDENTITY_INSERT G2.WorkTypeGroup ON;

	INSERT INTO G2.WorkTypeGroup (Id, SubSystemId, Name, DisplayOrder, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.SubSystemId, oj.Name, oj.DisplayOrder, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (Id INT '$.Id', SubSystemId INT '$.SubSystemId', Name NVARCHAR(50) '$.Name', DisplayOrder INT '$.DisplayOrder') oj
	EXCEPT
	SELECT wtg.Id, wtg.SubSystemId, wtg.Name, wtg.DisplayOrder, @user, @user, @user, @user
	FROM G2.WorkTypeGroup wtg

	SET IDENTITY_INSERT G2.WorkTypeGroup OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH