BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @bnr NVARCHAR(MAX) = '[{
    "Id": 76,
    "ShortName": "DP0901250",
    "InformalCode": "0901250",
    "ProgramValue": "2220660",
    "SubSystemId": 1,
    "Name": "R&D Facilities",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 77,
    "ShortName": "DP0901300",
    "InformalCode": "0901300",
    "ProgramValue": "2220661",
    "SubSystemId": 1,
    "Name": "Facilities",
    "Description": "Includes costs to operate and maintain",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 78,
    "ShortName": "DP0901350",
    "InformalCode": "0901350",
    "ProgramValue": "2220662",
    "SubSystemId": 1,
    "Name": "Institutional Site Support",
    "Description": "Includes costs for other nuclear weapons complex activities such as those associated with maintaining facilities in a standby status or decontaminating and decommissioning facilities; DNFSB activities for materials such as inactive actinides; Departmental taxes; and other unforeseen issues that affect site operations.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 79,
    "ShortName": "DP0901360",
    "InformalCode": "0901360",
    "ProgramValue": "2221717",
    "SubSystemId": 1,
    "Name": "TA-18 Early Move Initiative",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 80,
    "ShortName": "DP0901370",
    "InformalCode": "0901370",
    "ProgramValue": "2222149",
    "SubSystemId": 1,
    "Name": "Kansas City Plant - Operations of Facilities",
    "Description": "Funds the base operations at multiple facilities across the nuclear security enterprise. These costs include facility operations; utilities, including steam, gas and electric distribution; leases; safety bases development, implementation and maintenance; program management; waste management; ES&H including radiation, and industrial and high explosives safety.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 81,
    "ShortName": "DP0901371",
    "InformalCode": "0901371",
    "ProgramValue": "2222149",
    "SubSystemId": 1,
    "Name": "Kansas City Plant",
    "Description": "Includes costs to operate and maintain the Kansas City Plant (KCP) in a state of readiness, prepared to execute programmatic tasks identified in Campaigns and DSW. Operations of Facilities include costs, but not limited to, facilities management, maintenance, utilities, ES&H, capital equipment, General Plant Projects (GPP), and expense-funded projects.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 82,
    "ShortName": "DP0901372",
    "InformalCode": "0901372",
    "ProgramValue": "2222478",
    "SubSystemId": 1,
    "Name": "Kansas City Responsive Infrastructure, Manfacturing Sourcing (KCRIMS)",
    "Description": "Includes costs associated with the Kansas City Responsive Infrastructure, Manufacturing and Sourcing (KCRIMS) project. Specifically, but not limited to, support new facility, costs to relocate the Kansas City Plant (KCP) from the Banister Facility, stand-up and lease the Kansas City National Security Campus, and disposition of the Bannister Facility.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 83,
    "ShortName": "DP0901374",
    "InformalCode": "0901374",
    "ProgramValue": "2222151",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Common Use Stores",
    "Description": "Includes the change in inventory cost balances of stores inventories, which are materials, supplies, and parts on hand that are normally used or consumed in operations, maintenance and general use. Included are building and road materials and supplies, hardware and small tools, chemicals and compounds, metals and metal alloys, electrical materials and supplies, custodial materials and supplies, medical materials and supplies, office material and supplies, fuels and lubricants, clothing, motor vehicle accessories and repair parts, heavy mobile equipment accessories and repair parts, laboratory supplies, miscellaneous materials and supplies, returnable containers, Yttrium, stores work in process, and scrap.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 84,
    "ShortName": "DP090137M",
    "InformalCode": "090137M",
    "ProgramValue": "2222358",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 85,
    "ShortName": "DP0901380",
    "InformalCode": "0901380",
    "ProgramValue": "2222161",
    "SubSystemId": 1,
    "Name": "Lawrence Livermore National Laboratory - Operations of Facilities",
    "Description": "Funds the base operations at multiple facilities across the nuclear security enterprise. These costs include facility operations; utilities, including steam, gas and electric distribution; leases; safety bases development, implementation and maintenance; program management; waste management; ES&H including radiation, and industrial and high explosives safety.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 86,
    "ShortName": "DP0901381",
    "InformalCode": "0901381",
    "ProgramValue": "2222161",
    "SubSystemId": 1,
    "Name": "Lawrence Livermore National Laboratory",
    "Description": "Includes costs for fixed operational costs and keeps the facilities and capabilities in a safe, secure, reliable, and \"ready for operations\" state of readiness. Activities include, but are not limited to newly generated waste, building and building system maintenance; utilities; maintenance of programmatic equipment; ES&H; actions to address safety issues, and implementation of rules, such as the Safety Basis Rule 10 CFR830, Nuclear Safety Management.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 87,
    "ShortName": "DP090138M",
    "InformalCode": "090138M",
    "ProgramValue": "2222359",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 88,
    "ShortName": "DP0901390",
    "InformalCode": "0901390",
    "ProgramValue": "2222173",
    "SubSystemId": 1,
    "Name": "Los Alamos National Laboratory",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 89,
    "ShortName": "DP0901391",
    "InformalCode": "0901391",
    "ProgramValue": "2222173",
    "SubSystemId": 1,
    "Name": "Los Alamos National Laboratory",
    "Description": "Includes costs for implementation of the technologies and methods necessary to make construction, operation, and maintenance of Defense Programs (DP) facilities safe, secure, compliant, and cost effective. Activities also include infrasturcture support and TA-18 Early Move activities to complete special nuclear material shipments.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 90,
    "ShortName": "DP0901392",
    "InformalCode": "0901392",
    "ProgramValue": "2222852",
    "SubSystemId": 1,
    "Name": "Newly Generated TRU Waste Streams",
    "Description": "Funding will be used on programs and projects dealing with newly generated TRU waste streams (waste generated after 1999). Funding will be utilized to include On-going Solid Waste Facility Operations, GPP Construction and Start-up Capabilities for Waste Disposition and Disposition of DP Waste.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 91,
    "ShortName": "DP0901394",
    "InformalCode": "0901394",
    "ProgramValue": "2222175",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Common Use Stores",
    "Description": "Includes the change in inventory cost balances of stores inventories, which are materials, supplies, and parts on hand that are normally used or consumed in operations, maintenance and general use. Included are building and road materials and supplies, hardware and small tools, chemicals and compounds, metals and metal alloys, electrical materials and supplies, custodial materials and supplies, medical materials and supplies, office material and supplies, fuels and lubricants, clothing, motor vehicle accessories and repair parts, heavy mobile equipment accessories and repair parts, laboratory supplies, miscellaneous materials and supplies, returnable containers, Yttrium, stores work in process, and scrap.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 92,
    "ShortName": "DP0901397",
    "InformalCode": "0901397",
    "ProgramValue": "2222178",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Change In Collateral Funds and Other Deposits",
    "Description": "Includes those amounts of DOE securities and cash held in the custody of other persons or entities in accordance with contractual provisions and trust agreements. Such funds consist primarily of insurance funds, benefits and annuity funds, pension funds, special contract funds, excess premium payments, and service deposits.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 93,
    "ShortName": "DP090139B",
    "InformalCode": "090139B",
    "ProgramValue": "2222179",
    "SubSystemId": 1,
    "Name": "M&O Contractors at HQs - Salaries\/Benefits",
    "Description": "Includes costs for salaries\/benefits for facility contractor employees (as defined in DOE N 350.5) who are temporarily assigned to the Washington, D.C. area and perform tasks for a Headquarters organization.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 94,
    "ShortName": "DP090139M",
    "InformalCode": "090139M",
    "ProgramValue": "2222361",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 95,
    "ShortName": "DP0901400",
    "InformalCode": "0901400",
    "ProgramValue": "2222185",
    "SubSystemId": 1,
    "Name": "Nevada Test Site",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 96,
    "ShortName": "DP0901401",
    "InformalCode": "0901401",
    "ProgramValue": "2222185",
    "SubSystemId": 1,
    "Name": "Nevada Test Site",
    "Description": "Includes costs for fixed operational costs and keeps the facilities and capabilities in a safe, secure, reliable, and \"ready for operations\" state of readiness. Provides essential physical and operational infrastructure to nine unique and specialized facilities to handle and test special nuclear material, and are designated RTBF mission critical.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 97,
    "ShortName": "DP090140M",
    "InformalCode": "090140M",
    "ProgramValue": "2222362",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 98,
    "ShortName": "DP0901410",
    "InformalCode": "0901410",
    "ProgramValue": "2222197",
    "SubSystemId": 1,
    "Name": "Pantex Plant",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 99,
    "ShortName": "DP0901411",
    "InformalCode": "0901411",
    "ProgramValue": "2222197",
    "SubSystemId": 1,
    "Name": "Pantex Plant",
    "Description": "Includes costs for facility management and support, and costs associated with facilities and their ability to function effectively, such as plant and maintenance engineering, facility utilization analysis, modification and upgrade analysis, facilities planning and condition determinations, and the rental of buildings and land.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 100,
    "ShortName": "DP0901414",
    "InformalCode": "0901414",
    "ProgramValue": "2222199",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Common Use Stores",
    "Description": "Includes the change in inventory cost balances of stores inventories, which are materials, supplies, and parts on hand that are normally used or consumed in operations, maintenance and general use. Included are building and road materials and supplies, hardware and small tools, chemicals and compounds, metals and metal alloys, electrical materials and supplies, custodial materials and supplies, medical materials and supplies, office material and supplies, fuels and lubricants, clothing, motor vehicle accessories and repair parts, heavy mobile equipment accessories and repair parts, laboratory supplies, miscellaneous materials and supplies, returnable containers, Yttrium, stores work in process, and scrap.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 101,
    "ShortName": "DP090141K",
    "InformalCode": "090141K",
    "ProgramValue": "2222392",
    "SubSystemId": 1,
    "Name": "Supporting Activities",
    "Description": "Includes costs for activities associated with the storage of surplus plutonium and preparation for shipments.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 102,
    "ShortName": "DP090141M",
    "InformalCode": "090141M",
    "ProgramValue": "2222363",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 103,
    "ShortName": "DP0901420",
    "InformalCode": "0901420",
    "ProgramValue": "2222209",
    "SubSystemId": 1,
    "Name": "Sandia National Laboratories",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 104,
    "ShortName": "DP0901421",
    "InformalCode": "0901421",
    "ProgramValue": "2222209",
    "SubSystemId": 1,
    "Name": "Sandia National Laboratories",
    "Description": "Includes fixed operational costs to keep the facilities and capabilities in a safe, secure, reliable, and \"ready for operations\" state of readiness.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 105,
    "ShortName": "DP090142B",
    "InformalCode": "0090142B",
    "ProgramValue": "2222215",
    "SubSystemId": 1,
    "Name": "M&O Contractors at HQs - Salaries\/Benefits",
    "Description": "Includes costs for salaries\/benefits facility contractor employees (as defined in DOE N 350.5) who are temporarily assigned to the Washington, D.C. area and perform tasks for a Headquarters organization.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 106,
    "ShortName": "DP0901430",
    "InformalCode": "0901430",
    "ProgramValue": "2222221",
    "SubSystemId": 1,
    "Name": "Savannah River Site",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 107,
    "ShortName": "DP0901431",
    "InformalCode": "0901431",
    "ProgramValue": "2222221",
    "SubSystemId": 1,
    "Name": "Savannah River Site",
    "Description": "Includes costs for facilities management and support activities that maintain the facilities and infrastructure in a state of readiness for mission operations. Preventive, predictive, and corrective maintenance of process and infrastructure equipment and facilities is performed.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 108,
    "ShortName": "DP0901433",
    "InformalCode": "0901433",
    "ProgramValue": "2222222",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Special Process Spares",
    "Description": "Includes the Change In Inventory cost balances of those process spare parts obtainable only by special manufacture that are unique to the DOE program and that are used to replace parts of retirement units. Excludes all spare parts for non-process equipment such as automotive, heavy mobile, construction, hospital and medical, office, railroad, shop, aircraft, security, etc.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 109,
    "ShortName": "DP090143K",
    "InformalCode": "090143K",
    "ProgramValue": "2222365",
    "SubSystemId": 1,
    "Name": "PDCF OPC",
    "Description": "Includes costs for activities associated with the evaluation, testing, and demonstration of technologies to convert pit materials to oxide, a form suitable for disposition as MOX fuel. This includes costs associated with other project costs (OPC) associated with the PDC project during CD-0 through CD-4.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 110,
    "ShortName": "DP090143M",
    "InformalCode": "090143M",
    "ProgramValue": "2222366",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 111,
    "ShortName": "DP0901440",
    "InformalCode": "0901440",
    "ProgramValue": "2222233",
    "SubSystemId": 1,
    "Name": "Y-12 National Security Complex",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 112,
    "ShortName": "DP0901441",
    "InformalCode": "0901441",
    "ProgramValue": "2222233",
    "SubSystemId": 1,
    "Name": "Y-12 National Security Complex",
    "Description": "Includes costs for operation and maintenance of mission-essential facilities in a state of readiness, in which each facility is operationally ready to execute programmatic tasks within multiple Defense Programs mission elements.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 113,
    "ShortName": "DP0901444",
    "InformalCode": "0901444",
    "ProgramValue": "2222235",
    "SubSystemId": 1,
    "Name": "Change In Inventories - Common Use Stores",
    "Description": "Includes the change in inventory cost balances of stores inventories, which are materials, supplies, and parts on hand that are normally used or consumed in operations, maintenance and general use. Included are building and road materials and supplies, hardware and small tools, chemicals and compounds, metals and metal alloys, electrical materials and supplies, custodial materials and supplies, medical materials and supplies, office material and supplies, fuels and lubricants, clothing, motor vehicle accessories and repair parts, heavy mobile equipment accessories and repair parts, laboratory supplies, miscellaneous materials and supplies, returnable containers, Yttrium, stores work in process, and scrap.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 114,
    "ShortName": "DP090144M",
    "InformalCode": "090144M",
    "ProgramValue": "2222367",
    "SubSystemId": 1,
    "Name": "Direct Maintenance",
    "Description": "Funds direct maintenance activities required to sustain property, plant, and equipment in a condition suitable for it to be used for its designated purpose. Direct maintenance activities include, but are not limited to Preventive Maintenance, Predictive Maintenance, Corrective Maintenance, Maintenance Management and General Maintenance. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 115,
    "ShortName": "DP0901450",
    "InformalCode": "0901450",
    "ProgramValue": "2222245",
    "SubSystemId": 1,
    "Name": "Institutional Site Support",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 116,
    "ShortName": "DP0901451",
    "InformalCode": "0901451",
    "ProgramValue": "2222245",
    "SubSystemId": 1,
    "Name": "Institutional Site Support",
    "Description": "Includes costs to support corporate activities across the nuclear material complex including: re-packaging and disposition of inactive actinide materials, program management and performance monitoring, occurrence reporting systems, quality assurance working groups, systems engineering, program risk management, enterprise modeling, independent and internal technical reviews and assessments. This also includes projects focused on facility downsizing, equipment modernization, reduction of operation maintainence cost and programmatic risk reduction.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 117,
    "ShortName": "DP0901814",
    "InformalCode": "0901814",
    "ProgramValue": "2720013",
    "SubSystemId": 1,
    "Name": "Special Reactor Materials",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 118,
    "ShortName": "DP0901815",
    "InformalCode": "0901815",
    "ProgramValue": "2720014",
    "SubSystemId": 1,
    "Name": "Other Special Materials",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 119,
    "ShortName": "DP0902000",
    "InformalCode": "0902000",
    "ProgramValue": "2720016",
    "SubSystemId": 1,
    "Name": "Program Readiness",
    "Description": "Includes costs for activities that support more than one facility, campaign, or DSW activity and are essential to achieving each program''s objectives. The activities may vary from site to site due to the inherent differences in site activities and organizational structure.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 120,
    "ShortName": "DP0902010",
    "InformalCode": "0902010",
    "ProgramValue": "2220663",
    "SubSystemId": 1,
    "Name": "Program Readiness",
    "Description": "Supports selected activities that support more than one facility, Campaign, or Directed Stockpile Work activity, and are essential to achieving the objectives of the Stockpile Stewardship Program. Ongoing activities include: NTS activities and pulsed power science and other technical support.",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 121,
    "ShortName": "DP0902070",
    "InformalCode": "0902070",
    "ProgramValue": "2222407",
    "SubSystemId": 1,
    "Name": "Joint US\/French Collaboration on Nuclear Criticality Safety",
    "Description": "Includes costs associated with the refurbishment and operation of a criticality safety facility at Valduc Centre, France.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 122,
    "ShortName": "DP0902090",
    "InformalCode": "0902090",
    "ProgramValue": "2220667",
    "SubSystemId": 1,
    "Name": "DOE Nuclear Criticality Safety Program",
    "Description": "Includes costs associated with implementation of the Department?s Nuclear Criticality Safety Program (NCSP). These costs support the six main NCSP elements of Analytical Methods, Information Preservation and Dissemination, Integral Experiments, International Criticality Safety Benchmark Evaluation Project, Nuclear Data, and Training and Education.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 123,
    "ShortName": "DP0905000",
    "InformalCode": "0905000",
    "ProgramValue": "2220677",
    "SubSystemId": 1,
    "Name": "Containers",
    "Description": "Includes costs incurred in providing adequate quantities of containers to support the nuclear weapons mission. Activities include research and development, design, recertification and maintenance, off-site transportation certification of component containers in accordance with federal regulations, off-site transportation authorization of non-certifiable nuclear materials transportation configuration; test and evaluation, production\/procurement, fielding and maintenance, and decontamination and disposal to provide adequate quantities of containers to support the nuclear weapons mission (transportation and storage).",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 124,
    "ShortName": "DP0905020",
    "InformalCode": "0905020",
    "SubSystemId": 1,
    "Name": "RECERTIFICATION AND MAINTENANCE",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 125,
    "ShortName": "DP0905040",
    "InformalCode": "0905040",
    "ProgramValue": "2221699",
    "SubSystemId": 1,
    "Name": "Containers",
    "Description": "Includes costs incurred in providing adequate quantities of containers to support the nuclear weapons mission. Activities include research and development, design, recertification and maintenance, off-site transportation certification of component containers in accordance with federal regulations, off-site transportation authorization of non-certifiable nuclear materials transportation configuration; test and evaluation, production\/procurement, fielding and maintenance, and decontamination and disposal to provide adequate quantities of containers to support the nuclear weapons mission (transportation and storage).",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 126,
    "ShortName": "DP0907000",
    "InformalCode": "0907000",
    "ProgramValue": "2222892",
    "SubSystemId": 1,
    "Name": "Maintenance and Repair of Facilities",
    "Description": "Funds the direct maintenance activities at NNSA sites across the nuclear security enterprise. These costs include completing prioritized annual surveillances and preventative maintenance of the vital systems, structures, and components at mission essential facilities. Funding also includes activities associated with corrective maintenance and predictive maintenance. Provides funds for unplanned or unforeseen events as corrective maintenance activities, which includes roof repairs. Provides for upkeep of all vital safety systems in both nuclear and non-nuclear facilities essential for national security missions. This does not include indirect maintenance funding, line item maintenance funding or maintenance associated with direct project scope.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 127,
    "ShortName": "DP0908000",
    "InformalCode": "0908000",
    "ProgramValue": "2222893",
    "SubSystemId": 1,
    "Name": "Recapitalization",
    "Description": "Provides funds for needed investments in obsolete\/aging facilities and infrastructure to improve its condition. Costs are associated with Capital Equipment, General Plant Projects, Expense Funded Projects, Other Project Costs and disposition activities. Funding also includes Capabilities Based Investments (CBI) activities, which provides targeted, strategic investments for life-extension and modernization of enduring requirements needed to sustain Defense Program capabilities. CBIs will provide funding to implement projects across the nuclear security enterprise.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 128,
    "ShortName": "DP0908010",
    "InformalCode": "0908010",
    "ProgramValue": "2222905",
    "SubSystemId": 1,
    "Name": "Recapitalization",
    "Description": "Provides funds for needed investments in obsolete\/aging facilities and infrastructure to improve its condition. Costs are associated with Capital Equipment, General Plant Projects, Expense Funded Projects, Other Project Costs and disposition activities.",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 129,
    "ShortName": "DP1000000",
    "InformalCode": "1000000",
    "ProgramValue": "2720078",
    "SubSystemId": 1,
    "Name": "FIRP",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 130,
    "ShortName": "DP1001000",
    "InformalCode": "1001000",
    "ProgramValue": "2220684",
    "SubSystemId": 1,
    "Name": "Recapitalization",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 131,
    "ShortName": "DP1002000",
    "InformalCode": "1002000",
    "ProgramValue": "2220685",
    "SubSystemId": 1,
    "Name": "Facility Disposition",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 132,
    "ShortName": "DP1003000",
    "InformalCode": "1003000",
    "ProgramValue": "2220686",
    "SubSystemId": 1,
    "Name": "Infrstructure Planning",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 133,
    "ShortName": "DP5000000",
    "InformalCode": "5000000",
    "ProgramValue": "2221952",
    "SubSystemId": 1,
    "Name": "Environmental Projects and Operations",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 134,
    "ShortName": "DP5001000",
    "InformalCode": "5001000",
    "ProgramValue": "2221952",
    "SubSystemId": 1,
    "Name": "Long-Term Stewardship",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 135,
    "ShortName": "DP5501000",
    "InformalCode": "5501000",
    "ProgramValue": "2222381",
    "SubSystemId": 1,
    "Name": "Environmental Projects and Operations",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 136,
    "ShortName": "DP5503000",
    "InformalCode": "5503000",
    "SubSystemId": 1,
    "Name": "Energy Modernization and Investment Program",
    "AppropriationStartFY": 2014,
    "ActiveInd": false
}, {
    "Id": 137,
    "ShortName": "DP5503010",
    "InformalCode": "5503010",
    "ProgramValue": "2222433",
    "SubSystemId": 1,
    "Name": "Energy Planning",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 138,
    "ShortName": "DP5503020",
    "InformalCode": "5503020",
    "ProgramValue": "2222434",
    "SubSystemId": 1,
    "Name": "Energy Modernization",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 145,
    "ShortName": "DP0920200",
    "InformalCode": "0920200",
    "ProgramValue": "2223007",
    "SubSystemId": 1,
    "Name": "Production Facilities Construction",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 167,
    "ShortName": "DP5502020",
    "InformalCode": "5502020",
    "ProgramValue": "2222383",
    "SubSystemId": 1,
    "Name": "Excess\/Surplus Materials",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 168,
    "ShortName": "DP5502040",
    "InformalCode": "5502040",
    "ProgramValue": "2222508",
    "SubSystemId": 1,
    "Name": "Nuclear Materials Management and Safeguards System (NMMSS)",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 169,
    "ShortName": "DP5502050",
    "InformalCode": "5502050",
    "ProgramValue": "2223032",
    "SubSystemId": 1,
    "Name": "Integrated Projects",
    "AppropriationStartFY": 2015,
    "ActiveInd": true
}, {
    "Id": 170,
    "ShortName": "DP5502010",
    "InformalCode": "5502010",
    "ProgramValue": "2222382",
    "SubSystemId": 1,
    "Name": "Program Support",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 171,
    "ShortName": "DP5502030",
    "InformalCode": "5502030",
    "SubSystemId": 1,
    "Name": "De-Inventory",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 198,
    "ShortName": "DP0909010",
    "InformalCode": "0909010",
    "ProgramValue": "2223112",
    "SubSystemId": 1,
    "Name": "Nuclear Criticality Safety Program",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 199,
    "ShortName": "DP0909020",
    "InformalCode": "0909020",
    "ProgramValue": "2223113",
    "SubSystemId": 1,
    "Name": "Nuclear Safety Research and Development",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 200,
    "ShortName": "DP0909030",
    "InformalCode": "0909030",
    "ProgramValue": "2223114",
    "SubSystemId": 1,
    "Name": "Packaging",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 201,
    "ShortName": "DP0909040",
    "InformalCode": "0909040",
    "ProgramValue": "2223115",
    "SubSystemId": 1,
    "Name": "Long Term Stewardship",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 202,
    "ShortName": "MB0505031",
    "InformalCode": "0505031",
    "ProgramValue": "2223130",
    "SubSystemId": 1,
    "Name": "Program Support",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 203,
    "ShortName": "MB0505032",
    "InformalCode": "0505032",
    "ProgramValue": "2223131",
    "SubSystemId": 1,
    "Name": "Excess\/Surplus Materials",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 204,
    "ShortName": "MB0505033",
    "InformalCode": "0505033",
    "ProgramValue": "2223132",
    "SubSystemId": 1,
    "Name": "De-Inventory",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 205,
    "ShortName": "MB0505034",
    "InformalCode": "0505034",
    "ProgramValue": "2223133",
    "SubSystemId": 1,
    "Name": "Nuclear Materials Management and Safeguards System (NMMSS)",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 206,
    "ShortName": "MB0505035",
    "InformalCode": "0505035",
    "ProgramValue": "2223134",
    "SubSystemId": 1,
    "Name": "Integrated Projects",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 214,
    "ShortName": "MB0505030",
    "InformalCode": "0505030",
    "ProgramValue": "2223126",
    "SubSystemId": 1,
    "Name": "Nuclear Materials Integration",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 215,
    "ShortName": "DP0901402",
    "InformalCode": "0901402",
    "ProgramValue": "2223136",
    "SubSystemId": 1,
    "Name": "Change in Inventories - Common Use Stores",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 216,
    "ShortName": "DP0920000",
    "InformalCode": "0920000",
    "ProgramValue": "2220442",
    "SubSystemId": 1,
    "Name": "Construction",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}, {
    "Id": 217,
    "ShortName": "DP0920100",
    "InformalCode": "0920100",
    "ProgramValue": "2223006",
    "SubSystemId": 1,
    "Name": "R&D Facilities Construction",
    "AppropriationStartFY": 2015,
    "ActiveInd": true
}, {
    "Id": 219,
    "ShortName": "DP0901422",
    "InformalCode": "0901422",
    "ProgramValue": "2223139",
    "SubSystemId": 1,
    "Name": "Change in Inventories - Common Use Stores",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 231,
    "ShortName": "DP0909000",
    "InformalCode": "0909000",
    "ProgramValue": "2223112",
    "SubSystemId": 1,
    "Name": "Safety and Environmental Operations",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 233,
    "ShortName": "DP0908030",
    "InformalCode": "0908030",
    "ProgramValue": "2223150",
    "SubSystemId": 1,
    "Name": "Bannister Federal Complex Disposition",
    "AppropriationStartFY": 2017,
    "ActiveInd": true
}, {
    "Id": 238,
    "ShortName": "DP0910000",
    "InformalCode": "0910000",
    "ProgramValue": "2720075",
    "SubSystemId": 1,
    "Name": "Operations of Facilities",
    "AppropriationStartFY": 2017,
    "ActiveInd": true
}, {
    "Id": 309,
    "ShortName": "39DP09200PRN17D63000",
    "InformalCode": "0017630",
    "ProgramValue": "2223149",
    "SubSystemId": 1,
    "Name": "17-D-630, Expand Electrical Distribution System, LLNL",
    "AppropriationStartFY": 2017,
    "ActiveInd": false
}, {
    "Id": 310,
    "ShortName": "39DP09020PRN16D51500",
    "InformalCode": "0016515",
    "ProgramValue": "2223110",
    "SubSystemId": 1,
    "Name": "16-D-515, Albuquerque Complex Project",
    "AppropriationStartFY": 2016,
    "ActiveInd": false
}, {
    "Id": 322,
    "ShortName": "39DP09020PRN16D62100",
    "InformalCode": "0016621",
    "ProgramValue": "2223111",
    "SubSystemId": 1,
    "Name": "16-D-621, TA-3 Substation Replacement, LANL",
    "AppropriationStartFY": 2017,
    "ActiveInd": false
}, {
    "Id": 323,
    "ShortName": "39DP09200PRN15D61300",
    "InformalCode": "0015613",
    "ProgramValue": "2223006",
    "SubSystemId": 1,
    "Name": "15-D-613, Emergency Operations Center, Y-12",
    "AppropriationStartFY": 2015,
    "ActiveInd": false
}, {
    "Id": 324,
    "ShortName": "39DP09200PRN16D51500",
    "InformalCode": "16D515",
    "ProgramValue": "2223057",
    "SubSystemId": 1,
    "Name": "16-D-515, Albuquerque Complex Project",
    "AppropriationStartFY": 2016,
    "ActiveInd": true
}, {
    "Id": 325,
    "ShortName": "39DP09200PRN16D62100",
    "InformalCode": "16D621",
    "ProgramValue": "2223067",
    "SubSystemId": 1,
    "Name": "16-D-621, TA-3 Substation replacement, LANL",
    "AppropriationStartFY": 2016,
    "ActiveInd": false
}, {
    "Id": 330,
    "ShortName": "DP0905050",
    "InformalCode": "0905050",
    "ProgramValue": "2221724",
    "SubSystemId": 1,
    "Name": "TA-18 Early Move Initiaitive",
    "AppropriationStartFY": 2014,
    "ActiveInd": true
}]';

	SET IDENTITY_INSERT Financial.BNR ON;
	
	INSERT INTO Financial.BNR (Id, ShortName, InformalCode, ProgramValue, ParentId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.ShortName, oj.InformalCode, oj.ProgramValue, oj.ParentId, @userId, @userId, @userId, @userId
	FROM OPENJSON(@bnr) WITH (Id INT '$.Id', ShortName NVARCHAR(25) '$.ShortName', InformalCode NVARCHAR(20) '$.InformalCode', 
							  ProgramValue NVARCHAR(7), ParentId INT '$.ParentId') oj
	EXCEPT
	SELECT b.Id, b.ShortName, b.InformalCode, b.ProgramValue, b.ParentId, @userId, @userId, @userId, @userId
	FROM Financial.BNR b;

	SET IDENTITY_INSERT Financial.BNR OFF;

	INSERT INTO Financial.SubSystemBNR (SubSystemId, BNRId, Name, Description, AbbreviatedName, AppropriationStartFY, AppropriationEndFY, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.SubSystemId, oj.Id, oj.Name, oj.Description, oj.AbbreviatedName, oj.AppropriationStartFY, oj.AppropriationEndFY, oj.ActiveInd, @userId, @userId, @userId, @userId
	FROM OPENJSON(@bnr) WITH (Id INT '$.Id', SubSystemId INT '$.SubSystemId', Name NVARCHAR(100) '$.Name', Description NVARCHAR(1000) '$.Description', AbbreviatedName NVARCHAR(25) '$.AbbreviationName',
							  AppropriationStartFY INT '$.AppropriationStartFY', AppropriationEndFY INT '$.AppropriationEndFY', ActiveInd BIT '$.ActiveInd') oj
	EXCEPT
	SELECT ssb.SubSystemId, ssb.BNRId, ssb.Name, ssb.Description, ssb.AbbreviatedName, ssb.AppropriationStartFY, ssb.AppropriationEndFY, ssb.ActiveInd, @userId, @userId, @userId, @userId
	FROM Financial.SubSystemBNR ssb

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH