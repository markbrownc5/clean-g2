BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @systemUserId INT = G2.fnGetSystemUser();

	INSERT INTO G2.PersonPreferenceChoice (PersonId, PreferenceChoiceId, PreferenceChoiceValueId)
	SELECT p.Id, pc.Id, pcv.Id
	FROM Security.Person p 
	CROSS JOIN G2.PreferenceChoice pc 
	INNER JOIN G2.PreferenceChoiceValue pcv ON pc.Id = pcv.PreferenceChoiceId
	WHERE p.Id NOT IN (@systemUserId)
	AND pc.Name IN ('Enable Proxy')
	AND pcv.Name = 'On'
	UNION 
	SELECT p.Id, pc.Id, pcv.Id 
	FROM Security.Person p 
	CROSS JOIN G2.PreferenceChoice pc 
	INNER JOIN G2.PreferenceChoiceValue pcv ON pc.Id = pcv.PreferenceChoiceId
	WHERE p.Id NOT IN (@systemUserId)
	AND pc.Name IN ('Default Sub-System')
	AND pcv.Name = 'FL'
	EXCEPT
	SELECT ppc.PersonId, ppc.PreferenceChoiceId, ppc.PreferenceChoiceValueId
	FROM G2.PersonPreferenceChoice ppc

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH