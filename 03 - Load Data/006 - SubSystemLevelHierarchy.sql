BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @userId INT = 614, @hierarchy NVARCHAR(MAX) = '[{
    "ChildLabel": "Office"
}, {
    "ParentLabel": "Portfolio",
    "ChildLabel": "Portfolio"
}, {
    "ParentLabel": "Sub-Program",
    "ChildLabel": "Portfolio"
}, {
    "ParentLabel": "Program",
    "ChildLabel": "Portfolio"
}, {
    "ParentLabel": "Office",
    "ChildLabel": "Program"
}, {
    "ParentLabel": "Program",
    "ChildLabel": "Program"
}, {
    "ParentLabel": "Portfolio",
    "ChildLabel": "Project"
}, {
    "ParentLabel": "Sub-Program",
    "ChildLabel": "Project"
}, {
    "ParentLabel": "Project",
    "ChildLabel": "Project"
}, {
    "ParentLabel": "Program",
    "ChildLabel": "Project"
}, {
    "ParentLabel": "Program",
    "ChildLabel": "Sub-Program"
}, {
    "ParentLabel": "Project",
    "ChildLabel": "Task"
}]';
	
	INSERT INTO G2.SubSystemLevelHierarchy (ParentSubSystemLevelId, ChildSubSystemLevelId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT sslParent.Id, sslChild.Id, @userId, @userId, @userId, @userId
	FROM OPENJSON(@hierarchy) WITH (ParentLabel NVARCHAR(50) '$.ParentLabel', ChildLabel NVARCHAR(50) '$.ChildLabel') h
	LEFT OUTER JOIN G2.SubSystemLevel sslParent ON sslParent.Label = h.ParentLabel
	INNER JOIN G2.SubSystemLevel sslChild ON sslChild.Label = h.ChildLabel
	EXCEPT
	SELECT sslh.ParentSubSystemLevelId, sslh.ChildSubSystemLevelId, @userId, @userId, @userId, @userId
	FROM G2.SubSystemLevelHierarchy sslh;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH