BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @crtssx NVARCHAR(MAX) = '[{
    "Id": 1,
    "ChangeRequestDetailStatusId": 2,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 2,
    "ChangeRequestDetailStatusId": 3,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 3,
    "ChangeRequestDetailStatusId": 4,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 4,
    "ChangeRequestDetailStatusId": 7,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 5,
    "ChangeRequestDetailStatusId": 1,
    "ChangeRequestDetailStatusStateId": 2
}, {
    "Id": 6,
    "ChangeRequestDetailStatusId": 5,
    "ChangeRequestDetailStatusStateId": 2
}, {
    "Id": 7,
    "ChangeRequestDetailStatusId": 6,
    "ChangeRequestDetailStatusStateId": 2
}, {
    "Id": 8,
    "ChangeRequestDetailStatusId": 6,
    "ChangeRequestDetailStatusStateId": 3
}, {
    "Id": 9,
    "ChangeRequestDetailStatusId": 2,
    "ChangeRequestDetailStatusStateId": 4
}, {
    "Id": 10,
    "ChangeRequestDetailStatusId": 8,
    "ChangeRequestDetailStatusStateId": 2
}, {
    "Id": 11,
    "ChangeRequestDetailStatusId": 1,
    "ChangeRequestDetailStatusStateId": 5
}, {
    "Id": 12,
    "ChangeRequestDetailStatusId": 5,
    "ChangeRequestDetailStatusStateId": 5
}, {
    "Id": 13,
    "ChangeRequestDetailStatusId": 8,
    "ChangeRequestDetailStatusStateId": 5
}, {
    "Id": 14,
    "ChangeRequestDetailStatusId": 9,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 15,
    "ChangeRequestDetailStatusId": 10,
    "ChangeRequestDetailStatusStateId": 2
}, {
    "Id": 16,
    "ChangeRequestDetailStatusId": 10,
    "ChangeRequestDetailStatusStateId": 5
}, {
    "Id": 17,
    "ChangeRequestDetailStatusId": 11,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 18,
    "ChangeRequestDetailStatusId": 7,
    "ChangeRequestDetailStatusStateId": 4
}, {
    "Id": 19,
    "ChangeRequestDetailStatusId": 12,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 20,
    "ChangeRequestDetailStatusId": 13,
    "ChangeRequestDetailStatusStateId": 1
}, {
    "Id": 21,
    "ChangeRequestDetailStatusId": 5,
    "ChangeRequestDetailStatusStateId": 6
}]';

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatusStateXref ON;

	INSERT INTO G2.ChangeRequestDetailStatusStateXref (Id, ChangeRequestDetailStatusId, ChangeRequestDetailStatusStateId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT crtssx.Id, crtssx.ChangeRequestDetailStatusId, crtssx.ChangeRequestDetailStatusStateId, @userId, @userId, @userId, @userId
	FROM OPENJSON(@crtssx) WITH (Id INT '$.Id', ChangeRequestDetailStatusId INT '$.ChangeRequestDetailStatusId', ChangeRequestDetailStatusStateId INT '$.ChangeRequestDetailStatusStateId') crtssx
	EXCEPT
	SELECT crdssx.Id, crdssx.ChangeRequestDetailStatusId, crdssx.ChangeRequestDetailStatusStateId, @userId, @userId, @userId, @userId
	FROM G2.ChangeRequestDetailStatusStateXref crdssx

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatusStateXref OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH