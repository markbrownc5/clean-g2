BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "SubSystemId": 1,
    "TransactionTypeId": 1,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 2,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 3,
    "SortOrder": 1,
    "ActiveInd": false
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 6,
    "SortOrder": 3,
    "ActiveInd": false
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 10,
    "SortOrder": 2,
    "ActiveInd": false
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 11,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 16,
    "SortOrder": 19,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 17,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 19,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 32,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 26,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 33,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 27,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 21,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 28,
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 18,
    "SortOrder": 9,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 25,
    "SortOrder": 10,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 31,
    "SortOrder": 11,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 29,
    "SortOrder": 12,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 23,
    "SortOrder": 14,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 30,
    "SortOrder": 15,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 24,
    "SortOrder": 16,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 22,
    "SortOrder": 17,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 20,
    "SortOrder": 18,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 51,
    "SortOrder": 8,
    "ActiveInd": true
}, {
    "SubSystemId": 1,
    "TransactionTypeId": 52,
    "SortOrder": 13,
    "ActiveInd": true
}]';

	INSERT INTO Financial.SubSystemTransactionType (SubSystemId, TransactionTypeId, SortOrder, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.SubSystemId, oj.TransactionTypeId, oj.SortOrder, oj.ActiveInd, @user, @user, @user, @user
	FROM OPENJSON(@json) WITH (SubSystemId INT '$.SubSystemId', TransactionTypeId INT '$.TransactionTypeId', SortOrder INT '$.SortOrder', ActiveInd BIT '$.ActiveInd') oj
	EXCEPT
	SELECT sstt.SubSystemId, sstt.TransactionTypeId, sstt.SortOrder, sstt.ActiveInd, @user, @user, @user, @user
	FROM Financial.SubSystemTransactionType sstt

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH