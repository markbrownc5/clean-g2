BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @prt NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Organization Member",
    "Description": "Organization Member"
}, {
    "Id": 2,
    "Name": "Organization Rollup",
    "Description": "Organization Rollup"
}, {
    "Id": 3,
    "Name": "Performer Group Rollup",
    "Description": "Performer to Performer Group Rollup"
}, {
    "Id": 4,
    "Name": "Performer Allottee Rollup",
    "Description": "Performer to Allottee Rollup"
}, {
    "Id": 5,
    "Name": "Lead Lab Rollup",
    "Description": "From Service Center Contract to Lead Lab"
}, {
    "Id": 6,
    "Name": "Annex1 Lab Rollup",
    "Description": "From Service Center Contract or Lab alias to Actual Performer"
}]';

	SET IDENTITY_INSERT Contact.PartyRelationshipType ON;

	INSERT INTO Contact.PartyRelationshipType (Id, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT p.Id, p.Name, p.Description, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@prt) WITH (Id INT '$.Id', Name VARCHAR(100) '$.Name', Description VARCHAR(100) '$.Description') p
	EXCEPT
	SELECT prt.Id, prt.Name, prt.Description, @userId, @userId, @userId, @userId 
	FROM Contact.PartyRelationshipType prt;

	SET IDENTITY_INSERT Contact.PartyRelationshipType OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH