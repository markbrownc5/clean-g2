BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @userId INT = 614, @activeStatus INT = G2.fnGetActiveWorkBreakdownStructureStatusId(),
	@wbsList NVARCHAR(MAX) = '
	[
	{
	"Id": 1,
	"SubSystemLevel": "Office",
	"SortableWBS": "0001",
	"FullWBS": "1",
	"Name": "Fantasy Land"
	},
		{
			"Id": 2,
			"SubSystemLevel": "Program",
			"SortableWBS": "0001.0001",
			"FullWBS": "1.1",
			"Name": "Sci Fi",
			"ParentId": 1
		},
		{
			"Id": 3,
			"SubSystemLevel": "Program",
			"SortableWBS": "0001.0002",
			"FullWBS": "1.2",
			"Name": "Magical",
			"ParentId": 1
		},
		{
			"Id": 4,
			"SubSystemLevel": "Program",
			"SortableWBS": "0001.0001.0001",
			"FullWBS": "1.1.1",
			"Name": "Star Wars",
			"ParentId": 2
		},
		{
			"Id": 5,
			"SubSystemLevel": "Program",
			"SortableWBS": "0001.0002.0001",
			"FullWBS": "1.2.1",
			"Name": "Wizarding World",
			"ParentId": 3
		},
		{
			"Id": 6,
			"SubSystemLevel": "Program",
			"SortableWBS": "0001.0002.0002",
			"FullWBS": "1.2.2",
			"Name": "Final Fantasy",
			"ParentId": 3
		},
			{
				"Id": 7,
				"SubSystemLevel": "Sub-Program",
				"SortableWBS": "0001.0001.0001.0001",
				"FullWBS": "1.1.1.1",
				"Name": "Empire",
				"ParentId": 4
			},
			{
				"Id": 8,
				"SubSystemLevel": "Sub-Program",
				"SortableWBS": "0001.0001.0001.0002",
				"FullWBS": "1.1.1.2",
				"Name": "Rebellion",
				"ParentId": 4
			},
				{
					"Id": 9,
					"SubSystemLevel": "Portfolio",
					"SortableWBS": "0001.0001.0001.0001.0001",
					"FullWBS": "1.1.1.1.1",
					"Name": "Death Star",
					"ParentId": 7
				},
					{
						"Id": 10,
						"SubSystemLevel": "Project",
						"SortableWBS": "0001.0001.0001.0001.0001.0001",
						"FullWBS": "1.1.1.1.1.1",
						"Name": "Execution",
						"ParentId": 9
					},
					{
						"Id": 11,
						"SubSystemLevel": "Project",
						"SortableWBS": "0001.0001.0001.0001.0001.0002",
						"FullWBS": "1.1.1.1.1.2",
						"Name": "Maintenance",
						"ParentId": 9
					},
				{
					"Id": 13,
					"SubSystemLevel": "Portfolio",
					"SortableWBS": "0001.0001.0001.0002.0001",
					"FullWBS": "1.1.1.2.1",
					"Name": "Hoth",
					"ParentId": 8
				},
					{
						"Id": 14,
						"SubSystemLevel": "Project",
						"SortableWBS": "0001.0001.0001.0002.0001.0001",
						"FullWBS": "1.1.1.2.1.1",
						"Name": "Supply",
						"ParentId": 13
					},
					{
						"Id": 15,
						"SubSystemLevel": "Project",
						"SortableWBS": "0001.0001.0001.0002.0001.0002",
						"FullWBS": "1.1.1.2.1.2",
						"Name": "Construction",
						"ParentId": 13
					}
	]'

	SET IDENTITY_INSERT G2.WorkBreakdownStructure ON;

	INSERT INTO G2.WorkBreakdownStructure (Id, ParentId, SubSystemId, SubSystemLevelId, FullWBS, SortableWBS, Name, CompletedInd, StatusId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT w.Id, w.ParentId, 1, ssl.Id, w.FullWBS, w.SortableWBS, w.Name, CAST(0 AS BIT), @activeStatus, @userId, @userId, @userId, @userId
	FROM OPENJSON(@wbsList) WITH (Id INT '$.Id', ParentId INT '$.ParentId', SubSystemLevel NVARCHAR(100) '$.SubSystemLevel', SortableWBS NVARCHAR(100) '$.SortableWBS', FullWBS NVARCHAR(100) '$.FullWBS',
								  Name NVARCHAR(400) '$.Name') w
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Label = w.SubSystemLevel
	EXCEPT
	SELECT wbs.Id, wbs.ParentId, wbs.SubSystemId, wbs.SubSystemLevelId, wbs.FullWBS, wbs.SortableWBS, wbs.Name, wbs.CompletedInd, wbs.StatusId, @userId, @userId, @userId, @userId
	FROM G2.WorkBreakdownStructure wbs

	SET IDENTITY_INSERT G2.WorkBreakdownStructure OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH