BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @ft NVARCHAR(MAX) = '[{
    "Id": 14,
    "FundValue": "900",
    "ShortName": "TC",
    "DisplayName": "900 (TC)",
    "InternationalInd": false
}, {
    "Id": 15,
    "FundValue": "904",
    "ShortName": "FH",
    "DisplayName": "904 (FH)",
    "InternationalInd": false
}, {
    "Id": 16,
    "FundValue": "905",
    "ShortName": "FJ",
    "DisplayName": "905 (FJ)",
    "InternationalInd": false
}, {
    "Id": 17,
    "FundValue": "914",
    "ShortName": "XP",
    "DisplayName": "914 (XP)",
    "InternationalInd": false
}, {
    "Id": 18,
    "FundValue": "1050",
    "ShortName": "TF",
    "DisplayName": "1050 (TF)",
    "InternationalInd": false
}, {
    "Id": 19,
    "FundValue": "4000",
    "ShortName": "4A",
    "DisplayName": "4000 (4A)",
    "InternationalInd": false
}, {
    "Id": 28,
    "FundValue": "5350",
    "ShortName": "CN",
    "DisplayName": "5350 (CN)",
    "InternationalInd": false
}]', @ssft NVARCHAR(MAX) = '[{
    "Id": 14,
    "SubSystemId": 1,
    "FundTypeId": 14,
    "Name": "Weapons Activities",
    "ActiveInd": true,
    "PrimaryInd": true
}, {
    "Id": 15,
    "SubSystemId": 1,
    "FundTypeId": 15,
    "Name": "Weapons Activities",
    "ActiveInd": false,
    "PrimaryInd": false
}, {
    "Id": 16,
    "SubSystemId": 1,
    "FundTypeId": 16,
    "Name": "Weapons Activities",
    "ActiveInd": false,
    "PrimaryInd": false
}, {
    "Id": 17,
    "SubSystemId": 1,
    "FundTypeId": 17,
    "Name": "Weapons Activities",
    "ActiveInd": true,
    "PrimaryInd": false
}, {
    "Id": 18,
    "SubSystemId": 1,
    "FundTypeId": 18,
    "Name": "Materials Production and Other Defense Programs",
    "ActiveInd": true,
    "PrimaryInd": false
}, {
    "Id": 19,
    "SubSystemId": 1,
    "FundTypeId": 19,
    "Name": "Trust Funds - Advances for Co-Sponsored Projects - Operating Expenses",
    "ActiveInd": false,
    "PrimaryInd": false
}, {
    "Id": 67,
    "SubSystemId": 1,
    "FundTypeId": 28,
    "Name": "Nuclear Energy (NE) Primary Fund (P.L. 110-161 FY08)",
    "ActiveInd": true,
    "PrimaryInd": false
}]';
	
	SET IDENTITY_INSERT Financial.FundType ON;

	INSERT INTO Financial.FundType (Id, FundValue, ShortName, InternationalInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.FundValue, oj.ShortName, oj.InternationalInd, @userId, @userId, @userId, @userId
	FROM OPENJSON(@ft) WITH (Id INT '$.Id', FundValue NVARCHAR(5) '$.FundValue', ShortName NVARCHAR(25) '$.ShortName', DisplayName NVARCHAR(33) '$.DisplayName', InternationalInd BIT '$.InternationalInd') oj
	EXCEPT
	SELECT ft.Id, ft.FundValue, ft.ShortName, ft.InternationalInd, @userId, @userId, @userId, @userId
	FROM Financial.FundType ft;

	INSERT INTO Financial.SubSystemFundType (SubSystemId, FundTypeId, Name, ActiveInd, PrimaryInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT ssft.SubSystemId, ssft.FundTypeId, ssft.Name, ssft.ActiveInd, ssft.PrimaryInd, @userId, @userId, @userId, @userId
	FROM OPENJSON(@ssft) WITH (SubSystemId INT '$.SubSystemId', FundTypeId INT '$.FundTypeId', Name NVARCHAR(100) '$.Name', ActiveInd BIT '$.ActiveInd', PrimaryInd BIT '$.PrimaryInd') ssft
	EXCEPT
	SELECT ssft.SubSystemId, ssft.FundTypeId, ssft.Name, ssft.ActiveInd, ssft.PrimaryInd, @userId, @userId, @userId, @userId
	FROM Financial.SubSystemFundType ssft

	SET IDENTITY_INSERT Financial.FundType OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH