BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @roles NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Program Administrator",
    "Description": "Users with administrative functions in the program.",
    "Abbreviation": "PROG ADMIN",
    "ActiveInd": true
}, {
    "Id": 2,
    "Name": "General User",
    "Description": "Any user of the system.",
    "Abbreviation": "GEN USER",
    "ActiveInd": true
}, {
    "Id": 3,
    "Name": "Budget Officer",
    "Description": "Responsible for reviewing all budget changes and for accepting them into the system once they have received final approval.",
    "Abbreviation": "BO",
    "ActiveInd": true
}, {
    "Id": 4,
    "Name": "Country Officer",
    "Description": "Responsible for day-to-day management of projects within their country. Responsible for collecting and approving project status updates for their projects. Responsible for reviewing and approving change requests (budget, schedule and scope) made for projects in their country.",
    "Abbreviation": "CO",
    "ActiveInd": false
}, {
    "Id": 5,
    "Name": "Executive",
    "Description": "Responsible for reviewing all change requests (budget, schedule and scope) for the program.",
    "Abbreviation": "EXEC",
    "ActiveInd": true
}, {
    "Id": 6,
    "Name": "Lab Lead",
    "Description": "DELETE? do we have any way to define these?",
    "Abbreviation": "LAB LEAD",
    "ActiveInd": false
}, {
    "Id": 7,
    "Name": "Office Director",
    "Description": "Responsible for reviewing all change requests  (budget, schedule and scope) for their office.",
    "Abbreviation": "OD",
    "ActiveInd": true
}, {
    "Id": 8,
    "Name": "HQ Project Controls",
    "Description": "Responsible for ensuring that all change requests components and status updates are completed appropriately. Responsible for accepting status updates and schedule & scope change requests into the system.",
    "Abbreviation": "HQPC",
    "ActiveInd": true
}, {
    "Id": 9,
    "Name": "Project Manager",
    "PluralName": "Project Managers",
    "Description": "Responsible for approving status updates and all change requests for their projects. This would typically be the Country Officer where projects are tied to a country. However, for cross-cutting projects, another person may be assigned.",
    "Abbreviation": "PM",
    "ActiveInd": true
}, {
    "Id": 10,
    "Name": "Technical Director",
    "Description": "Responsible for reviewing all change requests for projects within their subfunction.",
    "Abbreviation": "TD",
    "ActiveInd": true
}, {
    "Id": 11,
    "Name": "Project Manager Designee",
    "PluralName": "Project Manager Designees",
    "Description": "Alternate Person Responsible for approving status updates and all change requests for their projects.",
    "Abbreviation": "PMD",
    "ActiveInd": true
}, {
    "Id": 13,
    "Name": "Lab Budget POC",
    "Description": "Lab Budget POC responsible for reviewing Fin Plan changes.",
    "Abbreviation": "LB POC",
    "ActiveInd": true
}, {
    "Id": 15,
    "Name": "AADA",
    "Description": "Associate Assistant Deputy Administrator",
    "Abbreviation": "AADA",
    "ActiveInd": true
}, {
    "Id": 16,
    "Name": "Inventory Read",
    "Description": "Allowed to view inventory data.",
    "Abbreviation": "INV READ",
    "ActiveInd": true
}, {
    "Id": 17,
    "Name": "Inventory Edit",
    "Description": "Allowed to edit inventory data.",
    "Abbreviation": "INV EDIT",
    "ActiveInd": true
}, {
    "Id": 18,
    "Name": "SPA Read",
    "Description": "Allowed to view SPA data.",
    "Abbreviation": "SPA READ",
    "ActiveInd": true
}, {
    "Id": 19,
    "Name": "SPA Team Member",
    "Description": "Allowed to edit SPA data.",
    "Abbreviation": "STM",
    "ActiveInd": true
}, {
    "Id": 20,
    "Name": "Protect Technical Lead (International)",
    "Description": "Responsible for approving deviations in SPA.",
    "Abbreviation": "PTL-I",
    "ActiveInd": true
}, {
    "Id": 21,
    "Name": "SPA Team Lead",
    "Description": "SPA Review and Submittal.",
    "Abbreviation": "STL",
    "ActiveInd": true
}, {
    "Id": 22,
    "Name": "Region Officer",
    "Description": "SPA Review and Submittal.",
    "Abbreviation": "RO",
    "ActiveInd": true
}, {
    "Id": 23,
    "Name": "HQ Executive Officer",
    "Description": "Grants access to the GIS Analysis Dashboard and the related exports.",
    "Abbreviation": "HQ Exec",
    "ActiveInd": true
}, {
    "Id": 24,
    "Name": "Lab Management POC",
    "Description": "Lab Management POC",
    "Abbreviation": "LM POC",
    "ActiveInd": true
}, {
    "Id": 25,
    "Name": "Inventory Administrator",
    "Description": "Administrator for Inventory and SPA",
    "Abbreviation": "INV ADMIN",
    "ActiveInd": true
}, {
    "Id": 26,
    "Name": "Contracts Specialist",
    "Description": "Contracts Specialist for SPA",
    "Abbreviation": "CTR SPC",
    "ActiveInd": true
}, {
    "Id": 27,
    "Name": "Inventory QA",
    "Description": "Responsible for Inventory Data Quality",
    "Abbreviation": "INV QA",
    "ActiveInd": true
}, {
    "Id": 28,
    "Name": "IDD Coordinator",
    "Description": "Responsible for IDD coordination",
    "Abbreviation": "IDD COORD",
    "ActiveInd": true
}, {
    "Id": 29,
    "Name": "Country Team Lead",
    "Description": "SPA review and submittal.",
    "Abbreviation": "CTL",
    "ActiveInd": false
}, {
    "Id": 30,
    "Name": "Lab SPA Lead (International)",
    "Description": "SPA review and submittal.",
    "Abbreviation": "LSL-I",
    "ActiveInd": false
}, {
    "Id": 31,
    "Name": "ART Coordinator",
    "Description": "Alarm Response Training (ART) coordination.",
    "Abbreviation": "ART COORD",
    "ActiveInd": true
}, {
    "Id": 32,
    "Name": "Lab SPA Lead (Domestic)",
    "Description": "SPA Review and submittal",
    "Abbreviation": "LSL-D",
    "ActiveInd": false
}, {
    "Id": 33,
    "Name": "Portfolio Officer",
    "Description": "Responsible for day-to-day management of projects within their portfolio. Responsible for reviewing and approving change requests (budget, schedule and scope) made for projects in their portfolio.",
    "Abbreviation": "PO",
    "ActiveInd": true
}, {
    "Id": 34,
    "Name": "TTX Coordinator",
    "Description": "Table Top Exercise (TTX) Coordinator",
    "Abbreviation": "TTX COORD",
    "ActiveInd": true
}, {
    "Id": 36,
    "Name": "Deputy Office Director",
    "Description": "Allowed to review all change requests (budget, schedule and scope) for their office. Shares same responsibilities as Office Director",
    "Abbreviation": "DOD",
    "ActiveInd": true
}, {
    "Id": 37,
    "Name": "Deputy Technical Director",
    "Description": "Responsible for reviewing all change requests for projects within their subfunction. Shares same responsibilities as Technical Director",
    "Abbreviation": "DTD",
    "ActiveInd": true
}, {
    "Id": 38,
    "Name": "RMS Coordinator",
    "Description": "Responsible for RMS installation coordination.",
    "Abbreviation": "RMS COORD",
    "ActiveInd": true
}, {
    "Id": 39,
    "Name": "Search & Secure Administrator",
    "Description": "Administrator for Search & Secure Module",
    "Abbreviation": "SAS ADMIN",
    "ActiveInd": true
}, {
    "Id": 40,
    "Name": "Search & Secure User",
    "Description": "User of Search & Secure Module",
    "Abbreviation": "SAS USER",
    "ActiveInd": false
}, {
    "Id": 41,
    "Name": "Protect Technical Lead (Domestic)",
    "Description": "Responsible for approving deviations in SPA.",
    "Abbreviation": "PTL-D",
    "ActiveInd": true
}, {
    "Id": 42,
    "Name": "GIS Analyst",
    "Description": "GIS Analyst",
    "Abbreviation": "GISA",
    "ActiveInd": true
}, {
    "Id": 45,
    "Name": "HQ Reporting",
    "Description": "Provides HQ\/Executive report access to users who do not have access via another role.",
    "Abbreviation": "HQRPT",
    "ActiveInd": true
}, {
    "Id": 46,
    "Name": "Forum: User",
    "Description": "Gives users visibility of the G2 Forums Link but does not control access to the forums",
    "Abbreviation": "GTRI FORUM",
    "ActiveInd": true
}, {
    "Id": 47,
    "Name": "Address Book Admin",
    "Description": "Users responsible for managing the address book.",
    "Abbreviation": "AB ADMIN",
    "ActiveInd": true
}, {
    "Id": 48,
    "Name": "System Administrator",
    "Description": "Users with administrative functions in the system.",
    "Abbreviation": "SYS ADMIN",
    "ActiveInd": true
}, {
    "Id": 49,
    "Name": "Field Office Program Direction POC",
    "Description": "Field Office Program Direction POC",
    "Abbreviation": "FOPD POC",
    "ActiveInd": true
}, {
    "Id": 50,
    "Name": "HQ Program Direction POC",
    "Description": "HQ Program Direction POC",
    "Abbreviation": "HQPD POC",
    "ActiveInd": true
}, {
    "Id": 51,
    "Name": "Field Office Manager",
    "Description": "Field Office Manager",
    "Abbreviation": "FOM",
    "ActiveInd": true
}, {
    "Id": 52,
    "Name": "Deputy Field Office Manager",
    "Description": "Deputy Field Office Manager",
    "Abbreviation": "DFOM",
    "ActiveInd": true
}, {
    "Id": 53,
    "Name": "Functional Office Manager",
    "Description": "Functional Office Manager",
    "Abbreviation": "OD",
    "ActiveInd": true
}, {
    "Id": 54,
    "Name": "Deputy Functional Office Manager",
    "Description": "Deputy Functional Office Manager",
    "Abbreviation": "DOD",
    "ActiveInd": true
}, {
    "Id": 55,
    "Name": "Associate Administrator",
    "Description": "Associate Administrator",
    "Abbreviation": "AA",
    "ActiveInd": true
}, {
    "Id": 56,
    "Name": "Deputy Associate Administrator",
    "Description": "Deputy Associate Administrator",
    "Abbreviation": "DAA",
    "ActiveInd": true
}, {
    "Id": 57,
    "Name": "Program Executive Officer",
    "Description": "Program Executive Officer",
    "Abbreviation": "PEO",
    "ActiveInd": true
}, {
    "Id": 58,
    "Name": "Office Program Direction POC",
    "Description": "Office Program Direction POC",
    "Abbreviation": "OPD POC",
    "ActiveInd": true
}, {
    "Id": 59,
    "Name": "Lab SPA Lead",
    "Description": "SPA review and submittal",
    "Abbreviation": "LSL",
    "ActiveInd": true
}, {
    "Id": 60,
    "Name": "Associate Principal Deputy Administrator",
    "Description": "Associate Principal Deputy Administrator",
    "Abbreviation": "APDA",
    "ActiveInd": true
}, {
    "Id": 61,
    "Name": "Assistant Manager",
    "Description": "Assistant Manager",
    "Abbreviation": "AM",
    "ActiveInd": true
}, {
    "Id": 62,
    "Name": "NSAT System",
    "Description": "Used for actions performed by the external NSAT System at Y-12.",
    "Abbreviation": "NSAT",
    "ActiveInd": true
}, {
    "Id": 63,
    "Name": "Travel: Approver (Primary)",
    "Description": "Travel: Approver (Primary)",
    "Abbreviation": "TRVL APP1",
    "ActiveInd": true
}, {
    "Id": 64,
    "Name": "Travel: Approver (Secondary)",
    "Description": "Travel: Approver (Secondary)",
    "Abbreviation": "TRVL APP2",
    "ActiveInd": true
}, {
    "Id": 65,
    "Name": "Travel: Admin",
    "Description": "Travel: Admin",
    "Abbreviation": "TRVL ADMIN",
    "ActiveInd": true
}, {
    "Id": 66,
    "Name": "Travel: POC",
    "Description": "Travel: POC",
    "Abbreviation": "TRVL POC",
    "ActiveInd": true
}, {
    "Id": 67,
    "Name": "Travel: Executive Approver (Primary)",
    "Description": "Travel: Executive Approver (Primary)",
    "Abbreviation": "EXC TRVL APP1",
    "ActiveInd": true
}, {
    "Id": 68,
    "Name": "Travel: Executive Approver (Secondary)",
    "Description": "Travel: Executive Approver (Secondary)",
    "Abbreviation": "EXC TRVL APP2",
    "ActiveInd": true
}, {
    "Id": 69,
    "Name": "Travel: Staff",
    "Description": "Travel: Staff",
    "Abbreviation": "TRVLR",
    "ActiveInd": true
}, {
    "Id": 70,
    "Name": "FORT: User",
    "Description": "Gives users visibility of the FORT Link but does not control access to the application",
    "Abbreviation": "FORT",
    "ActiveInd": true
}, {
    "Id": 71,
    "Name": "QA Staff",
    "Description": "Members of QA staff",
    "Abbreviation": "QA",
    "ActiveInd": true
}, {
    "Id": 72,
    "Name": "Developer",
    "Description": "Members of development team",
    "Abbreviation": "DEV",
    "ActiveInd": true
}, {
    "Id": 73,
    "Name": "FinPlan: Site Financial Stakeholder",
    "Description": "FinPlan: Site Financial Stakeholder",
    "Abbreviation": "SITE STK",
    "ActiveInd": true
}, {
    "Id": 74,
    "Name": "FinPlan: Site Financial Approver (1)",
    "Description": "FinPlan: Site Financial Approver (1)",
    "Abbreviation": "SITE APP1",
    "ActiveInd": true
}, {
    "Id": 75,
    "Name": "FinPlan: Site Financial Approver (2)",
    "Description": "FinPlan: Site Financial Approver (2)",
    "Abbreviation": "SITE APP2",
    "ActiveInd": true
}, {
    "Id": 76,
    "Name": "FinPlan: Site Financial Approver (3)",
    "Description": "FinPlan: Site Financial Approver (3)",
    "Abbreviation": "SITE APP3",
    "ActiveInd": true
}, {
    "Id": 77,
    "Name": "FinPlan: Site Financial Approver (4)",
    "Description": "FinPlan: Site Financial Approver (4)",
    "Abbreviation": "SITE APP4",
    "ActiveInd": true
}, {
    "Id": 78,
    "Name": "FinPlan: Site Financial Approver (5)",
    "Description": "FinPlan: Site Financial Approver (5)",
    "Abbreviation": "SITE APP5",
    "ActiveInd": true
}, {
    "Id": 79,
    "Name": "FinPlan: Project Stakeholder",
    "Description": "FinPlan: Project Stakeholder",
    "Abbreviation": "PROJ STK",
    "ActiveInd": true
}, {
    "Id": 80,
    "Name": "FinPlan: Project Manager (Primary)",
    "Description": "FinPlan: Project Manager (Primary)",
    "Abbreviation": "PROJ APP1",
    "ActiveInd": true
}, {
    "Id": 81,
    "Name": "FinPlan: Project Manager (Secondary)",
    "Description": "FinPlan: Project Manager (Secondary)",
    "Abbreviation": "PROJ APP2",
    "ActiveInd": true
}, {
    "Id": 82,
    "Name": "FinPlan: Portfolio Manager (Primary)",
    "Description": "FinPlan: Portfolio Manager (Primary)",
    "Abbreviation": "PORT APP1",
    "ActiveInd": true
}, {
    "Id": 83,
    "Name": "FinPlan: Portfolio Manager (Secondary)",
    "Description": "FinPlan: Portfolio Manager (Secondary)",
    "Abbreviation": "PORT APP2",
    "ActiveInd": true
}, {
    "Id": 84,
    "Name": "FinPlan: Sub-Program Manager (Primary)",
    "Description": "FinPlan: Sub-Program Manager (Primary)",
    "Abbreviation": "SBPROG APP1",
    "ActiveInd": true
}, {
    "Id": 85,
    "Name": "FinPlan: Sub-Program Manager (Secondary)",
    "Description": "FinPlan: Sub-Program Manager (Secondary)",
    "Abbreviation": "SBPROG APP2",
    "ActiveInd": true
}, {
    "Id": 86,
    "Name": "FinPlan: Program Manager (Primary)",
    "Description": "FinPlan: Program Manager (Primary)",
    "Abbreviation": "PROG APP1",
    "ActiveInd": true
}, {
    "Id": 87,
    "Name": "FinPlan: Program Manager (Secondary)",
    "Description": "FinPlan: Program Manager (Secondary)",
    "Abbreviation": "PROG APP2",
    "ActiveInd": true
}, {
    "Id": 88,
    "Name": "FinPlan: Executive Manager (Primary)",
    "Description": "FinPlan: Executive Manager (Primary)",
    "Abbreviation": "EXC APP1",
    "ActiveInd": true
}, {
    "Id": 89,
    "Name": "FinPlan: Executive Manager (Secondary)",
    "Description": "FinPlan: Executive Manager (Secondary)",
    "Abbreviation": "EXC APP2",
    "ActiveInd": true
}, {
    "Id": 90,
    "Name": "FinPlan: HQ Financial Officer (Primary)",
    "Description": "FinPlan: HQ Financial Officer (Primary)",
    "Abbreviation": "HQ APP1",
    "ActiveInd": true
}, {
    "Id": 91,
    "Name": "FinPlan: HQ Financial Officer (Secondary)",
    "Description": "FinPlan: HQ Financial Officer (Secondary)",
    "Abbreviation": "HQ APP2",
    "ActiveInd": true
}, {
    "Id": 92,
    "Name": "FinPlan: Admin",
    "Description": "FinPlan: Admin",
    "Abbreviation": "FIN ADMIN",
    "ActiveInd": true
}, {
    "Id": 93,
    "Name": "Costs: Site Financial Officer (Primary)",
    "Description": "Costs: Site Financial Officer (Primary)",
    "Abbreviation": "SITE APP1",
    "ActiveInd": true
}, {
    "Id": 94,
    "Name": "Costs: Site Financial Officer (Secondary)",
    "Description": "Costs: Site Financial Officer (Secondary)",
    "Abbreviation": "SITE APP2",
    "ActiveInd": true
}, {
    "Id": 95,
    "Name": "Costs: HQ Financial Officer (Primary)",
    "Description": "Costs: HQ Financial Officer (Primary)",
    "Abbreviation": "HQ APP1",
    "ActiveInd": true
}, {
    "Id": 96,
    "Name": "Costs: HQ Financial Officer (Secondary)",
    "Description": "Costs: HQ Financial Officer (Secondary)",
    "Abbreviation": "HQ APP2",
    "ActiveInd": true
}, {
    "Id": 97,
    "Name": "Costs: Admin",
    "Description": "Costs: Admin",
    "Abbreviation": "COST ADMIN",
    "ActiveInd": true
}, {
    "Id": 98,
    "Name": "Funding Adjustment: User",
    "Description": "Funding Adjustment: User",
    "Abbreviation": "FUND ADJ USER",
    "ActiveInd": true
}, {
    "Id": 99,
    "Name": "GIS: NNSA Infrastructure Layers",
    "Description": "GIS: NNSA Infrastructure Layers",
    "Abbreviation": "NNSA INFSTR",
    "ActiveInd": true
}, {
    "Id": 100,
    "Name": "Schedule: Project Manager (Primary)",
    "PluralName": "Schedule: Project Managers (Primary)",
    "Description": "Schedule: Project Manager (Primary)",
    "Abbreviation": "PROJ APP1",
    "ActiveInd": true
}, {
    "Id": 101,
    "Name": "Schedule: Project Manager (Secondary)",
    "PluralName": "Schedule: Project Managers (Secondary)",
    "Description": "Schedule: Project Manager (Secondary)",
    "Abbreviation": "PROJ APP2",
    "ActiveInd": true
}, {
    "Id": 102,
    "Name": "Schedule: Project Stakeholder",
    "Description": "Schedule: Project Stakeholder",
    "Abbreviation": "PROJ STK",
    "ActiveInd": true
}, {
    "Id": 103,
    "Name": "Schedule: Portfolio Manager (Primary)",
    "Description": "Schedule: Portfolio Manager (Primary)",
    "Abbreviation": "PORT APP1",
    "ActiveInd": true
}, {
    "Id": 104,
    "Name": "Schedule: Portfolio Manager (Secondary)",
    "Description": "Schedule: Portfolio Manager (Secondary)",
    "Abbreviation": "PORT APP2",
    "ActiveInd": true
}, {
    "Id": 105,
    "Name": "Schedule: Sub-Program Manager (Primary)",
    "Description": "Schedule: Sub-Program Manager (Primary)",
    "Abbreviation": "SBPROG APP1",
    "ActiveInd": true
}, {
    "Id": 106,
    "Name": "Schedule: Sub-Program Manager (Secondary)",
    "Description": "Schedule: Sub-Program Manager (Secondary)",
    "Abbreviation": "SBPROG APP2",
    "ActiveInd": true
}, {
    "Id": 107,
    "Name": "Schedule: Program Manager (Primary)",
    "Description": "Schedule: Program Manager (Primary)",
    "Abbreviation": "PROG APP1",
    "ActiveInd": true
}, {
    "Id": 108,
    "Name": "Schedule: Program Manager (Secondary)",
    "Description": "Schedule: Program Manager (Secondary)",
    "Abbreviation": "PROG APP2",
    "ActiveInd": true
}, {
    "Id": 109,
    "Name": "Schedule: Executive (Primary)",
    "Description": "Schedule: Executive (Primary)",
    "Abbreviation": "EXC APP1",
    "ActiveInd": true
}, {
    "Id": 110,
    "Name": "Schedule: Executive (Secondary)",
    "Description": "Schedule: Executive (Secondary)",
    "Abbreviation": "EXC APP2",
    "ActiveInd": true
}, {
    "Id": 111,
    "Name": "Schedule: Admin",
    "Description": "Schedule: Admin",
    "Abbreviation": "SCH ADMIN",
    "ActiveInd": true
}, {
    "Id": 112,
    "Name": "BUILDER: User",
    "Description": "BUILDER: User",
    "Abbreviation": "BLDR USR",
    "ActiveInd": true
}, {
    "Id": 113,
    "Name": "Asset: Site Stakeholder",
    "Description": "Asset: Site Stakeholder",
    "Abbreviation": "ASSET SITE STK",
    "ActiveInd": true
}, {
    "Id": 114,
    "Name": "Planning: Project Planning",
    "Description": "Planning: Project Planning",
    "Abbreviation": "PROJ PLAN",
    "ActiveInd": true
}, {
    "Id": 115,
    "Name": "Travel: Senior Approver (Primary)",
    "Description": "Travel: Senior Approver (Primary)",
    "Abbreviation": "SR TRVL APP1",
    "ActiveInd": true
}, {
    "Id": 116,
    "Name": "Travel: Senior Approver (Secondary)",
    "Description": "Travel: Senior Approver (Secondary)",
    "Abbreviation": "SR TRVL APP2",
    "ActiveInd": true
}, {
    "Id": 117,
    "Name": "FinPlan: Office Director (Primary)",
    "Description": "FinPlan: Office Director (Primary)",
    "Abbreviation": "OFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 118,
    "Name": "FinPlan: Office Director (Secondary)",
    "Description": "FinPlan: Office Director (Secondary)",
    "Abbreviation": "OFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 119,
    "Name": "FinPlan: Sub-Office Director (Primary)",
    "Description": "FinPlan: Sub-Office Director (Primary)",
    "Abbreviation": "SBOFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 120,
    "Name": "FinPlan: Sub-Office Director (Secondary)",
    "Description": "FinPlan: Sub-Office Director (Secondary)",
    "Abbreviation": "SBOFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 121,
    "Name": "Schedule: Office Director (Primary)",
    "Description": "Schedule: Office Director (Primary)",
    "Abbreviation": "OFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 122,
    "Name": "Schedule: Office Director (Secondary)",
    "Description": "Schedule: Office Director (Secondary)",
    "Abbreviation": "OFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 123,
    "Name": "Schedule: Sub-Office Director (Primary)",
    "Description": "Schedule: Sub-Office Director (Primary)",
    "Abbreviation": "SBOFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 124,
    "Name": "Schedule: Sub-Office Director (Secondary)",
    "Description": "Schedule: Sub-Office Director (Secondary)",
    "Abbreviation": "SBOFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 125,
    "Name": "SPA: Team Member",
    "Description": "SPA: Team Member",
    "Abbreviation": "SPA TM MBR",
    "ActiveInd": true
}, {
    "Id": 126,
    "Name": "SPA: Team Lead",
    "Description": "SPA: Team Lead",
    "Abbreviation": "SPA TM LD",
    "ActiveInd": true
}, {
    "Id": 127,
    "Name": "SPA: Lab Lead",
    "Description": "SPA: Lab Lead",
    "Abbreviation": "SPA LB LD",
    "ActiveInd": true
}, {
    "Id": 128,
    "Name": "SPA: Portfolio Manager (Primary)",
    "Description": "SPA: Portfolio Manager (Primary)",
    "Abbreviation": "SPA PM-P",
    "ActiveInd": true
}, {
    "Id": 129,
    "Name": "SPA: Portfolio Manager (Secondary)",
    "Description": "SPA: Portfolio Manager (Secondary)",
    "Abbreviation": "SPA PM-S",
    "ActiveInd": true
}, {
    "Id": 130,
    "Name": "SPA: Sub-Program Manager (Primary)",
    "Description": "SPA: Sub-Program Manager (Primary)",
    "Abbreviation": "SPA SPM-P",
    "ActiveInd": true
}, {
    "Id": 131,
    "Name": "SPA: Sub-Program Manager (Secondary)",
    "Description": "SPA: Sub-Program Manager (Secondary)",
    "Abbreviation": "SPA SPM-S",
    "ActiveInd": true
}, {
    "Id": 132,
    "Name": "SPA: Protect Technical Lead",
    "Description": "SPA: Protect Technical Lead",
    "Abbreviation": "SPA PTL",
    "ActiveInd": true
}, {
    "Id": 133,
    "Name": "SPA: Administrator",
    "Description": "SPA: Administrator",
    "Abbreviation": "SPA ADM",
    "ActiveInd": true
}, {
    "Id": 134,
    "Name": "SPA: NRC License Administrator (Primary)",
    "Description": "SPA: NRC License Administrator (Primary)",
    "Abbreviation": "SPA NRC LA-P",
    "ActiveInd": true
}, {
    "Id": 135,
    "Name": "SPA: NRC License Administrator (Secondary)",
    "Description": "SPA: NRC License Administrator (Secondary)",
    "Abbreviation": "SPA NRC LA-S",
    "ActiveInd": true
}, {
    "Id": 136,
    "Name": "SPA: IDD Coordinator (Primary)",
    "Description": "SPA: IDD Coordinator (Primary)",
    "Abbreviation": "SPA IDD CRD-P",
    "ActiveInd": true
}, {
    "Id": 137,
    "Name": "SPA: IDD Coordinator (Secondary)",
    "Description": "SPA: IDD Coordinator (Secondary)",
    "Abbreviation": "SPA IDD CRD-S",
    "ActiveInd": true
}, {
    "Id": 138,
    "Name": "SPA: Cost Sharing Admin (Primary)",
    "Description": "SPA: Cost Sharing Admin (Primary)",
    "Abbreviation": "SPA CSA-P",
    "ActiveInd": true
}, {
    "Id": 139,
    "Name": "SPA: Cost Sharing Admin (Secondary)",
    "Description": "SPA: Cost Sharing Admin (Secondary)",
    "Abbreviation": "SPA CSA-S",
    "ActiveInd": true
}, {
    "Id": 140,
    "Name": "SPA: Inventory QA",
    "Description": "SPA: Inventory QA",
    "Abbreviation": "SPA INV QA",
    "ActiveInd": true
}, {
    "Id": 141,
    "Name": "Inventory: Administrator",
    "Description": "Inventory: Administrator",
    "Abbreviation": "INV ADM",
    "ActiveInd": true
}, {
    "Id": 142,
    "Name": "SPA: RMS Coordinator",
    "Description": "SPA: RMS Coordinator",
    "Abbreviation": "RMS COORD",
    "ActiveInd": true
}, {
    "Id": 143,
    "Name": "EM: ART Coordinator",
    "Description": "EM: ART Coordinator",
    "Abbreviation": "EM ART",
    "ActiveInd": true
}, {
    "Id": 144,
    "Name": "EM: TTX Coordinator",
    "Description": "EM: TTX Coordinator",
    "Abbreviation": "EM TTX",
    "ActiveInd": true
}, {
    "Id": 145,
    "Name": "EM: Protect Technical Lead",
    "Description": "EM: Protect Technical Lead",
    "Abbreviation": "EM PTL",
    "ActiveInd": true
}, {
    "Id": 146,
    "Name": "EM: Office Director (Primary)",
    "Description": "EM: Office Director (Primary)",
    "Abbreviation": "EM OD1",
    "ActiveInd": true
}, {
    "Id": 147,
    "Name": "EM: Office Director (Secondary)",
    "Description": "EM: Office Director (Secondary)",
    "Abbreviation": "EM OD2",
    "ActiveInd": true
}, {
    "Id": 148,
    "Name": "EM: Portfolio Manager (Primary)",
    "Description": "EM: Portfolio Manager (Primary)",
    "Abbreviation": "EM PM1",
    "ActiveInd": true
}, {
    "Id": 149,
    "Name": "EM: Portfolio Manager (Secondary)",
    "Description": "EM: Portfolio Manager (Secondary)",
    "Abbreviation": "EM PM2",
    "ActiveInd": true
}, {
    "Id": 150,
    "Name": "SS: HQ Financial Officer (Primary)",
    "Description": "SS: HQ Financial Officer (Primary)",
    "Abbreviation": "SS HQFO1",
    "ActiveInd": true
}, {
    "Id": 151,
    "Name": "SS: HQ Financial Officer (Secondary)",
    "Description": "SS: HQ Financial Officer (Secondary)",
    "Abbreviation": "SS HQFO1",
    "ActiveInd": true
}, {
    "Id": 152,
    "Name": "SS: Office Director (Primary)",
    "Description": "SS: Office Director (Primary)",
    "Abbreviation": "SS OD1",
    "ActiveInd": true
}, {
    "Id": 153,
    "Name": "SS: Office Director (Secondary)",
    "Description": "SS: Office Director (Secondary)",
    "Abbreviation": "SS OD2",
    "ActiveInd": true
}, {
    "Id": 154,
    "Name": "SS: Administrator",
    "Description": "SS: Administrator",
    "Abbreviation": "SS ADM",
    "ActiveInd": true
}, {
    "Id": 155,
    "Name": "SS: Sub Program Manager (Primary)",
    "Description": "SS: Sub Program Manager (Primary)",
    "Abbreviation": "SS SPM1",
    "ActiveInd": true
}, {
    "Id": 156,
    "Name": "SS: Sub Program Manager (Secondary)",
    "Description": "SS: Sub Program Manager (Secondary)",
    "Abbreviation": "SS SPM2",
    "ActiveInd": true
}, {
    "Id": 157,
    "Name": "SS: Portfolio Manager (Primary)",
    "Description": "SS: Portfolio Manager (Primary)",
    "Abbreviation": "SS PM1",
    "ActiveInd": true
}, {
    "Id": 158,
    "Name": "SS: Portfolio Manager (Secondary)",
    "Description": "SS: Portfolio Manager (Secondary)",
    "Abbreviation": "SS PM2",
    "ActiveInd": true
}, {
    "Id": 159,
    "Name": "General: System Stakeholder",
    "Description": "Read-only Report role",
    "Abbreviation": "SYS STK",
    "ActiveInd": true
}, {
    "Id": 160,
    "Name": "SPA: Inventory Layers",
    "Description": "SPA: Inventory Layers for GIS",
    "Abbreviation": "SPA Inv Layers",
    "ActiveInd": true
}, {
    "Id": 161,
    "Name": "Planning: Site Stakeholder",
    "Description": "Planning: Site Stakeholder",
    "Abbreviation": "PLAN SITE STK",
    "ActiveInd": true
}, {
    "Id": 162,
    "Name": "Planning: Site Modifier",
    "Description": "Planning: Site Modifier",
    "Abbreviation": "PLAN SITE",
    "ActiveInd": true
}, {
    "Id": 163,
    "Name": "Planning: HQ",
    "Description": "Planning: HQ",
    "Abbreviation": "PLAN HQ",
    "ActiveInd": true
}, {
    "Id": 164,
    "Name": "Planning: Admin",
    "Description": "Planning: Admin",
    "Abbreviation": "PLAN ADMIN",
    "ActiveInd": true
}, {
    "Id": 165,
    "Name": "FIMS: Admin",
    "Description": "FIMS: Admin",
    "Abbreviation": "FIMS ADMIN",
    "ActiveInd": true
}, {
    "Id": 166,
    "Name": "Schedule: Project Staff",
    "PluralName": "Schedule: Project Staff",
    "Description": "Schedule: Project Staff",
    "Abbreviation": "PROJ STAFF",
    "ActiveInd": true
}, {
    "Id": 167,
    "Name": "YEF: HQ Financial Officer",
    "Description": "YEF: HQ Financial Officer",
    "Abbreviation": "YEF HQFO",
    "ActiveInd": true
}, {
    "Id": 168,
    "Name": "YEF: Site Financial Officer",
    "Description": "YEF: Site Financial Officer",
    "Abbreviation": "YEF SFO ",
    "ActiveInd": true
}, {
    "Id": 169,
    "Name": "YEF: Sub Program Manager",
    "Description": "YEF: Sub-Program Manager",
    "Abbreviation": "YEF SPM",
    "ActiveInd": true
}, {
    "Id": 170,
    "Name": "GIS: SPA\/Inventory Layers",
    "PluralName": "GIS: SPA\/Inventory Layers",
    "Description": "GIS: SPA\/Inventory Layers",
    "Abbreviation": "GIS SPA INV",
    "ActiveInd": true
}, {
    "Id": 171,
    "Name": "GIS: Radtrax\/Thadias Layers",
    "PluralName": "GIS: Radtrax\/Thadias Layers",
    "Description": "GIS: Radtrax\/Thadias Layers",
    "Abbreviation": "GIS RAD THAD",
    "ActiveInd": true
}, {
    "Id": 172,
    "Name": "Inventory: Portfolio Manager (Primary)",
    "Description": "Inventory: Portfolio Manager (Primary)",
    "Abbreviation": "INV PM-P",
    "ActiveInd": true
}, {
    "Id": 173,
    "Name": "Inventory: Portfolio Manager (Secondary)",
    "Description": "Inventory: Portfolio Manager (Secondary)",
    "Abbreviation": "INV PM-S",
    "ActiveInd": true
}, {
    "Id": 174,
    "Name": "Inventory: Project Manager (Primary)",
    "Description": "Inventory: Project Manager (Primary)",
    "Abbreviation": "INV PJM-P",
    "ActiveInd": true
}, {
    "Id": 175,
    "Name": "Inventory: Project Manager (Secondary)",
    "Description": "Inventory: Project Manager (Secondary)",
    "Abbreviation": "INV PJM-S",
    "ActiveInd": true
}, {
    "Id": 176,
    "Name": "Planning: Project Planning Manager",
    "Description": "Planning: Project Planning Manager",
    "Abbreviation": "PLAN PM",
    "ActiveInd": true
}, {
    "Id": 177,
    "Name": "Planning: Project Enterprise Manager",
    "Description": "Planning: Project Enterprise Manager",
    "Abbreviation": "PLAN EM",
    "ActiveInd": true
}, {
    "Id": 178,
    "Name": "SMP: Staff",
    "Description": "SMP: Staff",
    "Abbreviation": "SMP STAFF",
    "ActiveInd": true
}, {
    "Id": 179,
    "Name": "SMP: Manager",
    "Description": "SMP: Manager",
    "Abbreviation": "SMP MGR",
    "ActiveInd": true
}, {
    "Id": 180,
    "Name": "SMP: Site Stakeholder",
    "Description": "SMP: Site Stakeholder",
    "Abbreviation": "SMP STK",
    "ActiveInd": true
}, {
    "Id": 181,
    "Name": "SMP: HQ Stakeholder",
    "Description": "SMP: HQ Stakeholder",
    "Abbreviation": "SMP HQ STK",
    "ActiveInd": true
}, {
    "Id": 182,
    "Name": "FinPlan: Sub-Office Manager (Primary)",
    "Description": "FinPlan: Sub-Office Manager (Primary)",
    "Abbreviation": "SUBOFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 183,
    "Name": "FinPlan: Sub-Office Manager (Secondary)",
    "Description": "FinPlan: Sub-Office Manager (Secondary)",
    "Abbreviation": "SUBOFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 184,
    "Name": "Schedule: Sub-Office Manager (Primary)",
    "Description": "Schedule: Sub-Office Manager (Primary)",
    "Abbreviation": "SUBOFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 185,
    "Name": "Schedule: Sub-Office Manager (Secondary)",
    "Description": "Schedule: Sub-Office Manager (Secondary)",
    "Abbreviation": "SUBOFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 186,
    "Name": "Funding Request: User",
    "Description": "Funding Request: User",
    "Abbreviation": "PFR USER",
    "ActiveInd": true
}, {
    "Id": 187,
    "Name": "FinPlan: Team Lead (Primary)",
    "PluralName": "FinPlan: Team Leads (Primary)",
    "Description": "The FinPlan approver at the team level",
    "Abbreviation": "TEAM APP1",
    "ActiveInd": true
}, {
    "Id": 188,
    "Name": "Asset: Admin",
    "Description": "Asset: Admin",
    "Abbreviation": "ASSET ADMIN",
    "ActiveInd": true
}, {
    "Id": 189,
    "Name": "Asset: Executive Reviewer",
    "Description": "Asset: Executive Reviewer",
    "Abbreviation": "ASSET EXEC APP",
    "ActiveInd": true
}, {
    "Id": 190,
    "Name": "Asset: HQ Reviewer",
    "Description": "Asset: HQ Reviewer",
    "Abbreviation": "ASSET HQ APP",
    "ActiveInd": true
}, {
    "Id": 191,
    "Name": "Asset: Site Reviewer",
    "Description": "Asset: Site Reviewer",
    "Abbreviation": "ASSET SITE APP",
    "ActiveInd": true
}, {
    "Id": 192,
    "Name": "Asset: Site Modifier",
    "Description": "Asset: Site Modifier",
    "Abbreviation": "ASSET SITE",
    "ActiveInd": true
}, {
    "Id": 193,
    "Name": "Asset: HQ Stakeholder",
    "Description": "Asset: HQ Stakeholder",
    "Abbreviation": "ASSET HQ STK",
    "ActiveInd": true
}, {
    "Id": 194,
    "Name": "YEF: Team Lead",
    "Description": "YEF: Team Lead",
    "Abbreviation": "YEF TM LD",
    "ActiveInd": true
}, {
    "Id": 195,
    "Name": "FinPlan: Sub-Portfolio Manager (Primary)",
    "Description": "FinPlan: Sub-Portfolio Manager (Primary)",
    "Abbreviation": "SBPORT APP1",
    "ActiveInd": true
}, {
    "Id": 196,
    "Name": "FinPlan: Sub-Portfolio Manager (Secondary)",
    "Description": "FinPlan: Sub-Portfolio Manager (Secondary)",
    "Abbreviation": "SBPORT APP2",
    "ActiveInd": true
}, {
    "Id": 197,
    "Name": "Schedule: Sub-Portfolio Manager (Primary)",
    "Description": "Schedule: Sub-Portfolio Manager (Primary)",
    "Abbreviation": "SBPORT APP1",
    "ActiveInd": true
}, {
    "Id": 198,
    "Name": "Schedule: Sub-Portfolio Manager (Secondary)",
    "Description": "Schedule: Sub-Portfolio Manager (Secondary)",
    "Abbreviation": "SBPORT APP2",
    "ActiveInd": true
}, {
    "Id": 199,
    "Name": "G2 Help Desk",
    "Description": "G2 Help Desk",
    "Abbreviation": "G2-HD",
    "ActiveInd": true
}, {
    "Id": 200,
    "Name": "Planning: Solicitation Modifier",
    "Description": "Planning: Solicitation Modifier",
    "Abbreviation": "PLAN SM",
    "ActiveInd": true
}, {
    "Id": 201,
    "Name": "Planning: Team Lead",
    "Description": "Planning: Team Lead",
    "Abbreviation": "PLAN TL",
    "ActiveInd": true
}, {
    "Id": 202,
    "Name": "Planning: Office Director",
    "Description": "Planning: Office Director",
    "Abbreviation": "PLAN OD",
    "ActiveInd": true
}, {
    "Id": 203,
    "Name": "Planning: HQ Reviewer",
    "Description": "Planning: HQ Reviewer",
    "Abbreviation": "PLAN HQ REV",
    "ActiveInd": true
}, {
    "Id": 204,
    "Name": "Funding Request: Project Manager (Primary)",
    "Description": "Funding Request: Project Manager (Primary)",
    "Abbreviation": "FR PROJ APP1",
    "ActiveInd": true
}, {
    "Id": 205,
    "Name": "Funding Request: Project Manager (Secondary)",
    "Description": "Funding Request: Project Manager (Secondary)",
    "Abbreviation": "FR PROJ APP2",
    "ActiveInd": true
}, {
    "Id": 206,
    "Name": "Funding Request: Portfolio Manager (Primary)",
    "Description": "Funding Request: Portfolio Manager (Primary)",
    "Abbreviation": "FR PORT APP1",
    "ActiveInd": true
}, {
    "Id": 207,
    "Name": "Funding Request: Portfolio Manager (Secondary)",
    "Description": "Funding Request: Portfolio Manager (Secondary)",
    "Abbreviation": "FR PORT APP2",
    "ActiveInd": true
}, {
    "Id": 208,
    "Name": "Funding Request: Sub-Program Manager (Primary)",
    "Description": "Funding Request: Sub-Program Manager (Primary)",
    "Abbreviation": "FR SBPROG APP1",
    "ActiveInd": true
}, {
    "Id": 209,
    "Name": "Funding Request: Sub-Program Manager (Secondary)",
    "Description": "Funding Request: Sub-Program Manager (Secondary)",
    "Abbreviation": "FR SBPROG APP2",
    "ActiveInd": true
}, {
    "Id": 210,
    "Name": "Funding Request: Office Director (Primary)",
    "Description": "Funding Request: Office Director (Primary)",
    "Abbreviation": "FR OFFICE APP1",
    "ActiveInd": true
}, {
    "Id": 211,
    "Name": "Funding Request: Office Director (Secondary)",
    "Description": "Funding Request: Office Director (Secondary)",
    "Abbreviation": "FR OFFICE APP2",
    "ActiveInd": true
}, {
    "Id": 212,
    "Name": "Funding Request: HQ Financial Officer (Primary)",
    "Description": "Funding Request: HQ Financial Officer (Primary)",
    "Abbreviation": "FR HQ APP1",
    "ActiveInd": true
}, {
    "Id": 213,
    "Name": "Funding Request: HQ Financial Officer (Secondary)",
    "Description": "Funding Request: HQ Financial Officer (Secondary)",
    "Abbreviation": "FR HQ APP2",
    "ActiveInd": true
}, {
    "Id": 214,
    "Name": "Activity: Site Financial Officer",
    "Description": "Activity: Site Financial Officer",
    "Abbreviation": "AF SFO",
    "ActiveInd": true
}, {
    "Id": 215,
    "Name": "Activity: HQ Financial Officer",
    "Description": "Activity: HQ Financial Officer",
    "Abbreviation": "AF HQFO",
    "ActiveInd": true
}, {
    "Id": 216,
    "Name": "Activity: Project Manager",
    "Description": "Activity: Project Manager",
    "Abbreviation": "AF PROJ",
    "ActiveInd": true
}, {
    "Id": 217,
    "Name": "Activity: Portfolio Manager",
    "Description": "Activity: Portfolio Manager",
    "Abbreviation": "AF PORT",
    "ActiveInd": true
}, {
    "Id": 218,
    "Name": "Activity: Sub-Program Manager",
    "Description": "Activity: Sub-Program Manager",
    "Abbreviation": "AF SBPROG",
    "ActiveInd": true
}, {
    "Id": 219,
    "Name": "Activity: Office Director",
    "Description": "Activity: Office Director",
    "Abbreviation": "AF OFFICE",
    "ActiveInd": true
}, {
    "Id": 220,
    "Name": "WFORM: User",
    "Description": "Allows access to WFORM app via link from inside G2 app",
    "Abbreviation": "WFORM",
    "ActiveInd": true
}]';

	SET IDENTITY_INSERT Security.Role ON;

	INSERT INTO Security.Role (Id, Name, PluralName, Description, Abbreviation, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT r.Id, r.Name, r.PluralName, r.Description, r.Abbreviation, r.ActiveInd, @userId, @userId, @userId, @userId
	FROM OPENJSON(@roles) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', PluralName NVARCHAR(60) '$.PluralName', Description NVARCHAR(500) '$.Description', 
		Abbreviation NVARCHAR(15) '$.Abbreviation', ActiveInd BIT '$.ActiveInd') r
	EXCEPT
	SELECT r.Id, r.Name, r.PluralName, r.Description, r.Abbreviation, r.ActiveInd, @userId, @userId, @userId, @userId
	FROM Security.Role r;

	SET IDENTITY_INSERT Security.Role OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH