BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @userId INT = 614;
	
	SET IDENTITY_INSERT G2.SubSystem ON;

	INSERT INTO G2.SubSystem (Id, Name, ShortName, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT 1, 'Fantasy Land', 'FL', 1, @userId, @userId, @userId, @userId
	EXCEPT 
	SELECT ss.Id, ss.Name, ss.ShortName, ss.ActiveInd, @userId, @userId, @userId, @userId
	FROM G2.SubSystem ss;

	SET IDENTITY_INSERT G2.SubSystem OFF;
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH