BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 1,
    "ShortName": "OBL",
    "Name": "Obligation",
    "TransactionTypeGroupId": 1
}, {
    "Id": 2,
    "ShortName": "COM",
    "Name": "Unburdened Commitment",
    "TransactionTypeGroupId": 2
}, {
    "Id": 3,
    "ShortName": "LLB",
    "Name": "Lab Labor",
    "TransactionTypeGroupId": 3
}, {
    "Id": 4,
    "ShortName": "LTV",
    "Name": "Lab Travel",
    "TransactionTypeGroupId": 3
}, {
    "Id": 5,
    "ShortName": "LEQ",
    "Name": "Lab Equipment",
    "TransactionTypeGroupId": 3
}, {
    "Id": 6,
    "ShortName": "LCF",
    "Name": "Lab Contract Fees",
    "TransactionTypeGroupId": 3
}, {
    "Id": 7,
    "ShortName": "FST",
    "Name": "FSU Travel",
    "TransactionTypeGroupId": 3
}, {
    "Id": 8,
    "ShortName": "FSF",
    "Name": "FSU Services\/Equipment - from FSU",
    "TransactionTypeGroupId": 3
}, {
    "Id": 9,
    "ShortName": "FSU",
    "Name": "FSU Services\/Equipment  -from US\/Europe",
    "TransactionTypeGroupId": 3
}, {
    "Id": 10,
    "ShortName": "PSC",
    "Name": "Private Sector\/Non-Lab",
    "TransactionTypeGroupId": 3
}, {
    "Id": 11,
    "ShortName": "ADL",
    "Name": "Commitment Overhead",
    "TransactionTypeGroupId": 4
}, {
    "Id": 12,
    "ShortName": "STR",
    "Name": "STARS-Allowed Indirect Commitment",
    "TransactionTypeGroupId": 4
}, {
    "Id": 13,
    "ShortName": "BA ",
    "Name": "Funding",
    "TransactionTypeGroupId": 1
}, {
    "Id": 14,
    "ShortName": "COS",
    "Name": "Cost",
    "TransactionTypeGroupId": 3
}, {
    "Id": 15,
    "ShortName": "OBL",
    "Name": "Obligation",
    "TransactionTypeGroupId": 2
}, {
    "Id": 16,
    "ShortName": "OTH",
    "Name": "Other Costs",
    "TransactionTypeGroupId": 3
}, {
    "Id": 17,
    "ShortName": "IOC",
    "Name": "Total Cost",
    "TransactionTypeGroupId": 5
}, {
    "Id": 18,
    "ShortName": "AUG",
    "Name": "Staff Augmentation",
    "TransactionTypeGroupId": 3
}, {
    "Id": 19,
    "ShortName": "CRA",
    "Name": "Craft Labor",
    "TransactionTypeGroupId": 3
}, {
    "Id": 20,
    "ShortName": "DRD",
    "Name": "Directed research & development (LDRD, PDRD, SDRD)",
    "TransactionTypeGroupId": 3
}, {
    "Id": 21,
    "ShortName": "EQT",
    "Name": "Equipment and Tooling",
    "TransactionTypeGroupId": 3
}, {
    "Id": 22,
    "ShortName": "FEE",
    "Name": "Fee",
    "TransactionTypeGroupId": 3
}, {
    "Id": 23,
    "ShortName": "GNA",
    "Name": "General and Administrative (G&A)",
    "TransactionTypeGroupId": 3
}, {
    "Id": 24,
    "ShortName": "GRT",
    "Name": "New Mexico Gross Receipts Tax (GRT)",
    "TransactionTypeGroupId": 3
}, {
    "Id": 25,
    "ShortName": "ISC",
    "Name": "Recovery of Indirect Service Center Costs",
    "TransactionTypeGroupId": 3
}, {
    "Id": 26,
    "ShortName": "MGA",
    "Name": "Managerial & Administrative Labor",
    "TransactionTypeGroupId": 3
}, {
    "Id": 27,
    "ShortName": "MSU",
    "Name": "Materials and Supplies",
    "TransactionTypeGroupId": 3
}, {
    "Id": 28,
    "ShortName": "PSL",
    "Name": "Subcontracts",
    "TransactionTypeGroupId": 3
}, {
    "Id": 29,
    "ShortName": "PSU",
    "Name": "Program Office Support",
    "TransactionTypeGroupId": 3
}, {
    "Id": 30,
    "ShortName": "SGS",
    "Name": "Safeguard and Security adder (S&S)",
    "TransactionTypeGroupId": 3
}, {
    "Id": 31,
    "ShortName": "SSU",
    "Name": "Site Support",
    "TransactionTypeGroupId": 3
}, {
    "Id": 32,
    "ShortName": "TES",
    "Name": "Technical, Engineering, and Scientific Labor",
    "TransactionTypeGroupId": 3
}, {
    "Id": 33,
    "ShortName": "TVL",
    "Name": "Travel",
    "TransactionTypeGroupId": 3
}, {
    "Id": 34,
    "ShortName": "COM",
    "Name": "Total Commitments",
    "TransactionTypeGroupId": 2
}, {
    "Id": 35,
    "ShortName": "USC",
    "Name": "US Costs",
    "TransactionTypeGroupId": 3
}, {
    "Id": 36,
    "ShortName": "LCF",
    "Name": "US Overheads on Partner Costs",
    "TransactionTypeGroupId": 3
}, {
    "Id": 37,
    "ShortName": "PSC",
    "Name": "Partner Costs",
    "TransactionTypeGroupId": 3
}, {
    "Id": 38,
    "ShortName": "USL",
    "Name": "US Labor for Russia",
    "TransactionTypeGroupId": 3
}, {
    "Id": 39,
    "ShortName": "UST",
    "Name": "US Travel for Russia",
    "TransactionTypeGroupId": 3
}, {
    "Id": 40,
    "ShortName": "USE",
    "Name": "US Equipment for Russia",
    "TransactionTypeGroupId": 3
}, {
    "Id": 41,
    "ShortName": "USF",
    "Name": "US Overhead on Non-US Contracts for Russia",
    "TransactionTypeGroupId": 3
}, {
    "Id": 42,
    "ShortName": "INC",
    "Name": "In Country Cost (Non FSU)",
    "TransactionTypeGroupId": 3
}, {
    "Id": 43,
    "ShortName": "NIS",
    "Name": "US Costs for Non-Russia\/Non - NIS",
    "TransactionTypeGroupId": 3
}, {
    "Id": 44,
    "ShortName": "COM",
    "Name": "Commitments",
    "TransactionTypeGroupId": 2
}, {
    "Id": 45,
    "ShortName": "EQT",
    "Name": "Equipment",
    "TransactionTypeGroupId": 3
}, {
    "Id": 46,
    "ShortName": "LLB",
    "Name": "Labor",
    "TransactionTypeGroupId": 3
}, {
    "Id": 47,
    "ShortName": "MSU",
    "Name": "Materials ",
    "TransactionTypeGroupId": 3
}, {
    "Id": 48,
    "ShortName": "OTH",
    "Name": "Other",
    "TransactionTypeGroupId": 3
}, {
    "Id": 49,
    "ShortName": "OVH",
    "Name": "Overhead",
    "TransactionTypeGroupId": 3
}, {
    "Id": 50,
    "ShortName": "ADL",
    "Name": "Additional Commitments",
    "TransactionTypeGroupId": 4
}, {
    "Id": 51,
    "ShortName": "SCI",
    "Name": "Subcontracts - International",
    "TransactionTypeGroupId": 3
}, {
    "Id": 52,
    "ShortName": "UFS",
    "Name": "Infrastructure Support",
    "TransactionTypeGroupId": 3
}]'; 
	
	SET IDENTITY_INSERT Financial.TransactionType ON;

	INSERT INTO Financial.TransactionType (Id, ShortName, Name, TransactionTypeGroupId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.ShortName, oj.Name, oj.TransactionTypeGroupId, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (Id INT '$.Id', ShortName CHAR(3) '$.ShortName', Name NVARCHAR(50) '$.Name', TransactionTypeGroupId INT '$.TransactionTypeGroupId') oj
	EXCEPT
	SELECT tt.Id, tt.ShortName, tt.Name, tt.TransactionTypeGroupId, @user, @user, @user, @user
	FROM Financial.TransactionType tt;

	SET IDENTITY_INSERT Financial.TransactionType OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH