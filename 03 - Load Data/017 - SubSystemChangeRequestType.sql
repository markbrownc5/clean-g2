BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @sscrt NVARCHAR(MAX) = '[{
    "Id": 26,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 1
}, {
    "Id": 33,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 8
}, {
    "Id": 34,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 15
}, {
    "Id": 27,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 17
}, {
    "Id": 28,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 18
}, {
    "Id": 29,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 19
}, {
    "Id": 30,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 20
}, {
    "Id": 35,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 22
}, {
    "Id": 36,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 23
}, {
    "Id": 32,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 24
}, {
    "Id": 37,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 25
}, {
    "Id": 79,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 31
}, {
    "Id": 80,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 32
}, {
    "Id": 81,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 33
}, {
    "Id": 82,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 34
}, {
    "Id": 84,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 36
}, {
    "Id": 86,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 38
}, {
    "Id": 94,
    "SubSystemId": 1,
    "ChangeRequestTypeId": 39
}]';

	INSERT INTO G2.SubSystemChangeRequestType (SubSystemId, ChangeRequestTypeId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT sscrt.SubSystemId, sscrt.ChangeRequestTypeId, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@sscrt) WITH (SubSystemId INT '$.SubSystemId', ChangeRequestTypeId INT '$.ChangeRequestTypeId') sscrt
	EXCEPT
	SELECT sscrt.SubSystemId, sscrt.ChangeRequestTypeId, @userId, @userId, @userId, @userId
	FROM G2.SubSystemChangeRequestType sscrt;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH