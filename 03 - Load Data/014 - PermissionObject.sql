BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @po NVARCHAR(MAX) = '[{
    "Id": 1,
    "HID": "\/1\/",
    "Name": "FinPlan Edit",
    "Description": "Page used to edit a FinPlan change request.",
    "ActiveInd": true
}, {
    "Id": 2,
    "HID": "\/2\/",
    "Name": "FinPlan Initiate",
    "Description": "Page used to initiate a FinPlan change request.",
    "ActiveInd": true
}, {
    "Id": 3,
    "HID": "\/3\/",
    "Name": "FinPlan Review",
    "Description": "Page used during the FinPlan review process.",
    "ActiveInd": true
}, {
    "Id": 5,
    "HID": "\/5\/",
    "Name": "FinPlan Admin",
    "Description": "Page used to for FinPlan administration.",
    "ActiveInd": true
}, {
    "Id": 12,
    "HID": "\/12\/",
    "Name": "Baseline Change Request",
    "Description": "Page used to edit a Baseline change request.",
    "ActiveInd": true
}, {
    "Id": 14,
    "HID": "\/14\/",
    "Name": "Baseline Review",
    "Description": "Page used during the Baseline review process.",
    "ActiveInd": true
}, {
    "Id": 15,
    "HID": "\/15\/",
    "Name": "Performance Reporting",
    "Description": "Process used to status projects.",
    "ActiveInd": true
}, {
    "Id": 16,
    "ParentId": 15,
    "HID": "\/15\/1\/",
    "Name": "Performance Reporting Change Request",
    "Description": "Page used to status projects.",
    "ActiveInd": true
}, {
    "Id": 17,
    "ParentId": 15,
    "HID": "\/15\/2\/",
    "Name": "Performance Reporting Review",
    "Description": "Page used to review performance reports.",
    "ActiveInd": true
}, {
    "Id": 18,
    "ParentId": 15,
    "HID": "\/15\/3\/",
    "Name": "Performance Reporting Import",
    "Description": "Page used to import performance reports.",
    "ActiveInd": true
}, {
    "Id": 19,
    "HID": "\/16\/",
    "Name": "Funding Adjustments",
    "Description": "Process used to make funding adjustments.",
    "ActiveInd": true
}, {
    "Id": 20,
    "HID": "\/17\/",
    "Name": "Spend Plan Admin",
    "Description": "Pages used to make manage spend plans.",
    "ActiveInd": false
}, {
    "Id": 21,
    "HID": "\/18\/",
    "Name": "Transaction Browse",
    "Description": "Page used to browse financial transactions.",
    "ActiveInd": true
}, {
    "Id": 22,
    "HID": "\/19\/",
    "Name": "Resolution Management",
    "Description": "Page used to view website at a different resolution.",
    "ActiveInd": false
}, {
    "Id": 23,
    "HID": "\/20\/",
    "Name": "Scheduled Maintenance",
    "Description": "Page used to schedule maintenance.",
    "ActiveInd": true
}, {
    "Id": 24,
    "HID": "\/21\/",
    "Name": "Project Admin",
    "Description": "Page used to make changes to projects.",
    "ActiveInd": false
}, {
    "Id": 25,
    "HID": "\/22\/",
    "Name": "Period Admin",
    "Description": "Page used to manage periods.",
    "ActiveInd": false
}, {
    "Id": 26,
    "HID": "\/23\/",
    "Name": "Joule Target Admin",
    "Description": "Page used to manage joule targets.",
    "ActiveInd": true
}, {
    "Id": 27,
    "HID": "\/24\/",
    "Name": "Cache Admin",
    "Description": "Pages used to clear application cache.",
    "ActiveInd": true
}, {
    "Id": 28,
    "HID": "\/25\/",
    "Name": "System Usage",
    "Description": "Page used to view system usage.",
    "ActiveInd": true
}, {
    "Id": 29,
    "HID": "\/26\/",
    "Name": "Announcement Admin",
    "Description": "Pages used to manage announcements.",
    "ActiveInd": true
}, {
    "Id": 30,
    "HID": "\/27\/",
    "Name": "Performer Admin",
    "Description": "Page used to manage performers.",
    "ActiveInd": false
}, {
    "Id": 31,
    "HID": "\/28\/",
    "Name": "Map Settings Admin",
    "Description": "Page used to manage map settings.",
    "ActiveInd": false
}, {
    "Id": 32,
    "HID": "\/29\/",
    "Name": "Person Admin",
    "Description": "Pages used to manage users'' proxies and roles.",
    "ActiveInd": true
}, {
    "Id": 33,
    "HID": "\/30\/",
    "Name": "Region Admin",
    "Description": "Pages used to manage regions and states.",
    "ActiveInd": false
}, {
    "Id": 34,
    "HID": "\/31\/",
    "Name": "Top Projects Admin",
    "Description": "Page used to manage top projects.",
    "ActiveInd": false
}, {
    "Id": 35,
    "HID": "\/32\/",
    "Name": "Media\/RSS Admin",
    "Description": "Pages used to manage media and RSS feeds.",
    "ActiveInd": false
}, {
    "Id": 36,
    "HID": "\/33\/",
    "Name": "Lifecycle Budget Admin",
    "Description": "Page used to update lifecycle budget.",
    "ActiveInd": false
}, {
    "Id": 37,
    "HID": "\/34\/",
    "Name": "Notification Admin",
    "Description": "Page used to trigger notifications.",
    "ActiveInd": false
}, {
    "Id": 38,
    "HID": "\/35\/",
    "Name": "Inventory Main",
    "Description": "Page used to navigate Inventory and SPA.",
    "ActiveInd": true
}, {
    "Id": 39,
    "ParentId": 38,
    "HID": "\/35\/1\/",
    "Name": "Inventory Site",
    "Description": "Page used to maintain site inventory.",
    "ActiveInd": true
}, {
    "Id": 40,
    "ParentId": 39,
    "HID": "\/35\/1\/1\/",
    "Name": "Inventory Building",
    "Description": "Page used to maintain building inventory.",
    "ActiveInd": true
}, {
    "Id": 41,
    "ParentId": 40,
    "HID": "\/35\/1\/1\/1\/",
    "Name": "Inventory Room",
    "Description": "Page used to maintain room inventory.",
    "ActiveInd": true
}, {
    "Id": 42,
    "ParentId": 41,
    "HID": "\/35\/1\/1\/1\/1\/",
    "Name": "Inventory Source",
    "Description": "Page used to maintain source inventory.",
    "ActiveInd": true
}, {
    "Id": 43,
    "HID": "\/145\/",
    "Name": "SPA Main",
    "Description": "Page used to maintain Site Protection Assessment (SPA).",
    "ActiveInd": true
}, {
    "Id": 44,
    "HID": "\/36\/",
    "Name": "IDD Summary",
    "Description": "Page used to maintain IDD information.",
    "ActiveInd": true
}, {
    "Id": 45,
    "HID": "\/37\/",
    "Name": "Announcements",
    "Description": "Page used to display system announcements.",
    "ActiveInd": true
}, {
    "Id": 46,
    "HID": "\/38\/",
    "Name": "Dashboard",
    "Description": "G2 dashboard page.",
    "ActiveInd": false
}, {
    "Id": 47,
    "HID": "\/39\/",
    "Name": "Inbox",
    "Description": "Page used to display items awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 48,
    "HID": "\/40\/",
    "Name": "Preferences",
    "Description": "Page used to manage a user''s preferences.",
    "ActiveInd": true
}, {
    "Id": 49,
    "HID": "\/41\/",
    "Name": "WBS Dashboard",
    "Description": "Page used to browse and view project details.",
    "ActiveInd": true
}, {
    "Id": 50,
    "HID": "\/42\/",
    "Name": "Media",
    "Description": "Page used to view featured links\/videos and RSS feeds.",
    "ActiveInd": false
}, {
    "Id": 51,
    "HID": "\/43\/",
    "Name": "MS Project Export",
    "Description": "Pages used to export data to Microsoft Project.",
    "ActiveInd": false
}, {
    "Id": 52,
    "HID": "\/44\/",
    "Name": "Reports",
    "Description": "Pages used view and run reports.",
    "ActiveInd": true
}, {
    "Id": 53,
    "HID": "\/45\/",
    "Name": "Baseline Browse",
    "Description": "Page used to view current and historic Baseline change requests.",
    "ActiveInd": true
}, {
    "Id": 54,
    "HID": "\/46\/",
    "Name": "Planning",
    "Description": "Pages used to facilitate the planning process.",
    "ActiveInd": false
}, {
    "Id": 55,
    "HID": "\/47\/",
    "Name": "PWP Map Admin",
    "Description": "Pages used to generate maps used in the PWP report.",
    "ActiveInd": false
}, {
    "Id": 56,
    "HID": "\/48\/",
    "Name": "Preferences Project Manager Designee Tab",
    "Description": "Tab used to manage Project Manager Designees.",
    "ActiveInd": false
}, {
    "Id": 57,
    "HID": "\/49\/",
    "Name": "Preferences Notifications Tab",
    "Description": "Tab used to manage notifications.",
    "ActiveInd": true
}, {
    "Id": 59,
    "HID": "\/51\/",
    "Name": "FinPlan Inbox",
    "Description": "Grid used to display FinPlans awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 60,
    "HID": "\/52\/",
    "Name": "Funding Adjustments Inbox",
    "Description": "Grid used to display Funding Adjustments awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 62,
    "HID": "\/54\/",
    "Name": "Baseline Inbox",
    "Description": "Grid used to display Baselines awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 63,
    "HID": "\/55\/",
    "Name": "Performance Reporting Inbox",
    "Description": "Grid used to display Performance Reports awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 64,
    "HID": "\/56\/",
    "Name": "SPA Inbox",
    "Description": "Grid used to display SPAs awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 65,
    "HID": "\/57\/",
    "Name": "Event Manager Inbox",
    "Description": "Grid used to display events awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 66,
    "ParentId": 15,
    "HID": "\/15\/4\/",
    "Name": "Performance Reporting Project Review",
    "Description": "Grid used to review performance reporting projects.",
    "ActiveInd": false
}, {
    "Id": 67,
    "HID": "\/65\/",
    "Name": "Performance Reporting Variance Review",
    "Description": "Grid used to review performance reporting variances.",
    "ActiveInd": false
}, {
    "Id": 68,
    "HID": "\/58\/",
    "Name": "Baseline Import",
    "Description": "Page used for importing baseline changes.",
    "ActiveInd": true
}, {
    "Id": 70,
    "HID": "\/60\/",
    "Name": "Planning Target Export\/Import",
    "Description": "Page used for importing planning target export\/import.",
    "ActiveInd": true
}, {
    "Id": 72,
    "HID": "\/62\/",
    "Name": "Report Permission Admin",
    "Description": "Pages used to manage report permissions.",
    "ActiveInd": true
}, {
    "Id": 73,
    "HID": "\/63\/",
    "Name": "Event Manager",
    "Description": "Pages used for event manager.",
    "ActiveInd": true
}, {
    "Id": 74,
    "ParentId": 38,
    "HID": "\/35\/3\/",
    "Name": "Inventory Export",
    "Description": "Page used to export inventory data.",
    "ActiveInd": true
}, {
    "Id": 75,
    "ParentId": 38,
    "HID": "\/35\/4\/",
    "Name": "Inventory POC",
    "Description": "Page used to maintain inventory points of contact.",
    "ActiveInd": true
}, {
    "Id": 76,
    "ParentId": 38,
    "HID": "\/35\/5\/",
    "Name": "Inventory File Viewer",
    "Description": "Page used to view inventory file uploads.",
    "ActiveInd": true
}, {
    "Id": 77,
    "HID": "\/64\/",
    "Name": "Minimum Joule Completion",
    "Description": "Page used to edit the minimum joule completion date.",
    "ActiveInd": true
}, {
    "Id": 78,
    "HID": "\/66\/",
    "Name": "Baseline Admin",
    "Description": "Page used for Baseline administration",
    "ActiveInd": true
}, {
    "Id": 79,
    "HID": "\/67\/",
    "Name": "NRC License Admin",
    "Description": "Page used for NRC License administration.",
    "ActiveInd": true
}, {
    "Id": 80,
    "ParentId": 40,
    "HID": "\/68\/",
    "Name": "Building Attributes",
    "Description": "Grid used to manage building attributes.",
    "ActiveInd": true
}, {
    "Id": 81,
    "HID": "\/69\/",
    "Name": "Funding Profile",
    "Description": "Page used to display a funding profile for a project and period.",
    "ActiveInd": true
}, {
    "Id": 82,
    "HID": "\/70\/",
    "Name": "Cost Entry",
    "Description": "Page used to enter \/ import \/ review \/ browse cost data.",
    "ActiveInd": true
}, {
    "Id": 83,
    "HID": "\/71\/",
    "Name": "STARS Import",
    "Description": "Page used to import STARS cost data.",
    "ActiveInd": true
}, {
    "Id": 84,
    "HID": "\/72\/",
    "Name": "Cost Admin",
    "Description": "Page used to manage the cost process.",
    "ActiveInd": true
}, {
    "Id": 85,
    "HID": "\/76\/",
    "Name": "Performer Cost Data",
    "Description": "Performer cost data.",
    "ActiveInd": true
}, {
    "Id": 86,
    "HID": "\/74\/",
    "Name": "Performer Cost Notes",
    "Description": "Performer cost notes.",
    "ActiveInd": true
}, {
    "Id": 87,
    "HID": "\/75\/",
    "Name": "B&R \/ Fund Type Admin",
    "Description": "Page used to manage B&R and Fund Types",
    "ActiveInd": true
}, {
    "Id": 88,
    "HID": "\/77\/",
    "Name": "Search and Secure",
    "Description": "Pages used for search and secure.",
    "ActiveInd": true
}, {
    "Id": 89,
    "ParentId": 40,
    "HID": "\/35\/1\/1\/2\/",
    "Name": "Building In-Scope Indicator",
    "Description": "Checkbox to determine if building is in scope",
    "ActiveInd": true
}, {
    "Id": 90,
    "HID": "\/78\/",
    "Name": "GIS Analysis Dashboard",
    "Description": "GIS analsysis dashboard page.",
    "ActiveInd": true
}, {
    "Id": 91,
    "HID": "\/80\/",
    "Name": "Assessment Visit Admin",
    "Description": "Page used for Assessment Visit Administration.",
    "ActiveInd": true
}, {
    "Id": 92,
    "HID": "\/79\/",
    "Name": "Forum Link",
    "Description": "Forum Link",
    "ActiveInd": true
}, {
    "Id": 93,
    "HID": "\/81\/",
    "Name": "Assume Forecast Project Management",
    "Description": "Permission to see page that allows PM to alter Assume Forecast attribute for their projects",
    "ActiveInd": false
}, {
    "Id": 94,
    "HID": "\/82\/",
    "Name": "Address Book",
    "Description": "Page used to view address book \/ contacts.",
    "ActiveInd": true
}, {
    "Id": 95,
    "HID": "\/83\/",
    "Name": "SharePoint My Sites",
    "Description": "SharePoint My Sites link.",
    "ActiveInd": false
}, {
    "Id": 96,
    "HID": "\/84\/",
    "Name": "SharePoint Finance",
    "Description": "SharePoint Finane link.",
    "ActiveInd": false
}, {
    "Id": 97,
    "HID": "\/85\/",
    "Name": "SharePoint Reference",
    "Description": "SharePoint Reference link.",
    "ActiveInd": false
}, {
    "Id": 98,
    "HID": "\/86\/",
    "Name": "SharePoint Sustainability",
    "Description": "SharePoint Sustainability link.",
    "ActiveInd": false
}, {
    "Id": 99,
    "HID": "\/87\/",
    "Name": "SharePoint PWPs",
    "Description": "SharePoint PWP link.",
    "ActiveInd": false
}, {
    "Id": 100,
    "HID": "\/88\/",
    "Name": "Help Documentation",
    "Description": "G2 help documentation.",
    "ActiveInd": true
}, {
    "Id": 101,
    "HID": "\/89\/",
    "Name": "Process Calendar",
    "Description": "G2 process calendar.",
    "ActiveInd": true
}, {
    "Id": 102,
    "HID": "\/90\/",
    "Name": "SharePoint Calendar",
    "Description": "SharePoint Calendar link.",
    "ActiveInd": false
}, {
    "Id": 103,
    "HID": "\/96\/",
    "Name": "Travel Funds Inbox",
    "Description": "Grid used to display Travel Funds items awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 104,
    "HID": "\/95\/",
    "Name": "Travel",
    "Description": "Page used for travel management",
    "ActiveInd": true
}, {
    "Id": 105,
    "HID": "\/91\/",
    "Name": "Preferences Bookmark Tab",
    "Description": "Tab used to manage bookmarks",
    "ActiveInd": false
}, {
    "Id": 106,
    "HID": "\/92\/",
    "Name": "Preferences Permissions Tab",
    "Description": "Tab used to manage permissions",
    "ActiveInd": true
}, {
    "Id": 107,
    "HID": "\/93\/",
    "Name": "Preferences Proxy Tab",
    "Description": "Tab used to manage proxies",
    "ActiveInd": true
}, {
    "Id": 108,
    "HID": "\/94\/",
    "Name": "Preferences General Tab",
    "Description": "Tab used to manage general preferences",
    "ActiveInd": true
}, {
    "Id": 109,
    "HID": "\/97\/",
    "Name": "SharePoint",
    "Description": "SharePoint links.",
    "ActiveInd": true
}, {
    "Id": 110,
    "HID": "\/98\/",
    "Name": "NA-50 PD SharePoint System Reference",
    "Description": "NA-50 PD SharePoint System Reference link.",
    "ActiveInd": false
}, {
    "Id": 111,
    "HID": "\/99\/",
    "Name": "NA-50 PD SharePoint Program Documents",
    "Description": "NA-50 PD SharePoint Program Documents link.",
    "ActiveInd": false
}, {
    "Id": 112,
    "HID": "\/100\/",
    "Name": "NA-50 PD SharePoint Process Descriptions",
    "Description": "NA-50 PD SharePoint Process Descriptions link.",
    "ActiveInd": false
}, {
    "Id": 113,
    "HID": "\/101\/",
    "Name": "Site Layer",
    "Description": "GIS Mapping layer for sites",
    "ActiveInd": true
}, {
    "Id": 114,
    "HID": "\/102\/",
    "Name": "Task Layer",
    "Description": "GIS Mapping layer for tasks",
    "ActiveInd": true
}, {
    "Id": 115,
    "HID": "\/103\/",
    "Name": "Building Layer",
    "Description": "GIS Mapping layer for buildings",
    "ActiveInd": true
}, {
    "Id": 116,
    "HID": "\/104\/",
    "Name": "FORT Menu Link",
    "Description": "FORT Menu Link",
    "ActiveInd": true
}, {
    "Id": 117,
    "HID": "\/105\/",
    "Name": "Application Downloads",
    "Description": "Page used for Application Downloads",
    "ActiveInd": true
}, {
    "Id": 118,
    "HID": "\/106\/",
    "Name": "Offline Forms App",
    "Description": "Offline Forms App Link",
    "ActiveInd": true
}, {
    "Id": 119,
    "HID": "\/107\/",
    "Name": "Desktop Notification App",
    "Description": "Desktop Notification App Link",
    "ActiveInd": true
}, {
    "Id": 120,
    "HID": "\/108\/",
    "Name": "NA-50 PD SharePoint Mission Alignment Menu",
    "Description": "NA-50 PD SharePoint Mission Alignment Menu link.",
    "ActiveInd": false
}, {
    "Id": 121,
    "HID": "\/109\/",
    "Name": "Impersonate",
    "Description": "Page used to impersonate users (QA and DEV only!)",
    "ActiveInd": true
}, {
    "Id": 122,
    "HID": "\/110\/",
    "Name": "Cost Sharing Admin Menu Link",
    "Description": "Cost Sharing Admin Menu Link",
    "ActiveInd": true
}, {
    "Id": 123,
    "HID": "\/111\/",
    "Name": "Mission Layer",
    "Description": "GIS Mapping FIMS layer for mission",
    "ActiveInd": true
}, {
    "Id": 124,
    "HID": "\/112\/",
    "Name": "Usage\/Utilization Layer",
    "Description": "GIS Mapping FIMS layer for usage and utilization",
    "ActiveInd": true
}, {
    "Id": 125,
    "HID": "\/113\/",
    "Name": "Structural Parameters Layer",
    "Description": "GIS Mapping FIMS layer for structural parameters",
    "ActiveInd": true
}, {
    "Id": 126,
    "HID": "\/114\/",
    "Name": "Cost\/Value Layer",
    "Description": "GIS Mapping FIMS layer for cost\/value",
    "ActiveInd": true
}, {
    "Id": 127,
    "HID": "\/115\/",
    "Name": "Condition Layer",
    "Description": "GIS Mapping FIMS layer for condition",
    "ActiveInd": true
}, {
    "Id": 128,
    "HID": "\/116\/",
    "Name": "Roads Layer",
    "Description": "GIS Mapping Site Background layer for roads",
    "ActiveInd": true
}, {
    "Id": 129,
    "HID": "\/117\/",
    "Name": "Parking Areas Layer",
    "Description": "GIS Mapping Site Background layer for parking areas",
    "ActiveInd": true
}, {
    "Id": 130,
    "HID": "\/118\/",
    "Name": "Site Complex Layer",
    "Description": "GIS Mapping Site Background layer for site complex",
    "ActiveInd": true
}, {
    "Id": 131,
    "HID": "\/119\/",
    "Name": "Site Areas Layer",
    "Description": "GIS Mapping Site Background layer for site areas",
    "ActiveInd": true
}, {
    "Id": 132,
    "HID": "\/120\/",
    "Name": "Site Boundaries Layer",
    "Description": "GIS Mapping Site Background layer for site boundaries",
    "ActiveInd": true
}, {
    "Id": 133,
    "HID": "\/121\/",
    "Name": "RADTRAX Layer",
    "Description": "GIS Mapping Layer for RADTRAX",
    "ActiveInd": true
}, {
    "Id": 134,
    "HID": "\/122\/",
    "Name": "THADIAS Layer",
    "Description": "GIS Mapping Layer for THADIAS",
    "ActiveInd": true
}, {
    "Id": 135,
    "HID": "\/123\/",
    "Name": "Performer Distribution Import",
    "Description": "Page used for importing performer distribution data.",
    "ActiveInd": true
}, {
    "Id": 136,
    "HID": "\/124\/",
    "Name": "Performer Distribution Admin",
    "Description": "Page used for enabling the performer distribution import.",
    "ActiveInd": true
}, {
    "Id": 137,
    "HID": "\/125\/",
    "Name": "NA-50 PE SharePoint My Sites",
    "Description": "NA-50 PE SharePoint My Sites link.",
    "ActiveInd": false
}, {
    "Id": 138,
    "HID": "\/126\/",
    "Name": "BUILDER Menu Link",
    "Description": "Link to the BUILDER application",
    "ActiveInd": true
}, {
    "Id": 139,
    "HID": "\/127\/",
    "Name": "MDI",
    "Description": "Pages used to manage Mission Dependency Indexing",
    "ActiveInd": true
}, {
    "Id": 140,
    "HID": "\/128\/",
    "Name": "Project Planning Menu Link",
    "Description": "Link to Planning application",
    "ActiveInd": true
}, {
    "Id": 141,
    "HID": "\/129\/",
    "Name": "WBS Dashboard Description",
    "Description": "WBS description.",
    "ActiveInd": true
}, {
    "Id": 142,
    "HID": "\/130\/",
    "Name": "WBS Dashboard Attributes",
    "Description": "WBS attributes.",
    "ActiveInd": true
}, {
    "Id": 143,
    "HID": "\/131\/",
    "Name": "WBS Dashboard Tasks",
    "Description": "WBS tasks.",
    "ActiveInd": true
}, {
    "Id": 144,
    "HID": "\/132\/",
    "Name": "WBS Dashboard Lifecycle Budget",
    "Description": "WBS lifecycle budget.",
    "ActiveInd": true
}, {
    "Id": 145,
    "HID": "\/133\/",
    "Name": "WBS Dashboard Funding by Reporting Entity",
    "Description": "WBS funding by reporting entity.",
    "ActiveInd": true
}, {
    "Id": 146,
    "HID": "\/134\/",
    "Name": "WBS Dashboard Funding by B&R",
    "Description": "WBS funding by B&R.",
    "ActiveInd": true
}, {
    "Id": 147,
    "HID": "\/135\/",
    "Name": "WBS Dashboard Roles",
    "Description": "WBS roles.",
    "ActiveInd": true
}, {
    "Id": 148,
    "HID": "\/136\/",
    "Name": "WBS Dashboard Details",
    "Description": "WBS details including description, attributes, name, and FIMS assets.",
    "ActiveInd": true
}, {
    "Id": 149,
    "HID": "\/137\/",
    "Name": "NA-50 PE SharePoint System Reference",
    "Description": "NA-50 PE SharePoint System Reference link.",
    "ActiveInd": false
}, {
    "Id": 150,
    "HID": "\/138\/",
    "Name": "NA-50 PE SharePoint Program Documents",
    "Description": "NA-50 PE SharePoint Program Documents link.",
    "ActiveInd": false
}, {
    "Id": 151,
    "HID": "\/139\/",
    "Name": "NA-50 PE SharePoint Process Descriptions",
    "Description": "NA-50 PE SharePoint Process Descriptions link.",
    "ActiveInd": false
}, {
    "Id": 152,
    "HID": "\/140\/",
    "Name": "NA-50 PE SharePoint Mission Alignment",
    "Description": "NA-50 PE SharePoint Mission Alignment link.",
    "ActiveInd": false
}, {
    "Id": 153,
    "HID": "\/141\/",
    "Name": "NA-23 SharePoint My Sites",
    "Description": "NA-23 SharePoint My Sites link",
    "ActiveInd": false
}, {
    "Id": 154,
    "HID": "\/142\/",
    "Name": "NA-23 SharePoint Reference",
    "Description": "NA-23 SharePoint Reference link",
    "ActiveInd": false
}, {
    "Id": 155,
    "HID": "\/143\/",
    "Name": "NA-23 SharePoint Sustainability",
    "Description": "NA-23 SharePoint Sustainability link",
    "ActiveInd": false
}, {
    "Id": 156,
    "HID": "\/144\/",
    "Name": "NA-193 SharePoint My Sites",
    "Description": "NA-193 SharePoint My Sites link",
    "ActiveInd": false
}, {
    "Id": 157,
    "HID": "\/146\/",
    "Name": "Project Layer",
    "Description": "GIS Mapping Layer for projects",
    "ActiveInd": true
}, {
    "Id": 158,
    "HID": "\/147\/",
    "Name": "Performer Layer",
    "Description": "GIS Mapping Layer for performers",
    "ActiveInd": true
}, {
    "Id": 159,
    "HID": "\/151\/",
    "Name": "Budget Formulation Review",
    "Description": "Page used to review Budget Formulation requests.",
    "ActiveInd": true
}, {
    "Id": 160,
    "HID": "\/152\/",
    "Name": "Budget Formulation Edit",
    "Description": "Page used to edit Budget Formulation requests.",
    "ActiveInd": true
}, {
    "Id": 161,
    "HID": "\/153\/",
    "Name": "Budget Formulation Rank",
    "Description": "Page used to rank Budget Formulation requests.",
    "ActiveInd": true
}, {
    "Id": 162,
    "HID": "\/154\/",
    "Name": "Planning Admin",
    "Description": "Page used for Budget Formulation administration.",
    "ActiveInd": true
}, {
    "Id": 163,
    "HID": "\/148\/",
    "Name": "WBS Dashboard FIMS Assets",
    "Description": "WBS FIMS assets.",
    "ActiveInd": true
}, {
    "Id": 164,
    "HID": "\/149\/",
    "Name": "Contracts Manager",
    "Description": "Mapping for the STARS.ContractWorkBreakdownStructureReportingEntityBNRXref table",
    "ActiveInd": true
}, {
    "Id": 165,
    "HID": "\/150\/",
    "Name": "Financial Dashboard",
    "Description": "Page used to show key information related to Financials.",
    "ActiveInd": true
}, {
    "Id": 166,
    "HID": "\/155\/",
    "Name": "Budget Formulation Analysis",
    "Description": "Page used to analyze Budget Formulation scenarios.",
    "ActiveInd": true
}, {
    "Id": 167,
    "HID": "\/156\/",
    "Name": "Budget Formulation Scenario Edit",
    "Description": "Page used to create\/edit Budget Formulation scenarios.",
    "ActiveInd": true
}, {
    "Id": 168,
    "HID": "\/157\/",
    "Name": "Year End Forecasting",
    "Description": "Page used to manage Year End Forecasting data.",
    "ActiveInd": true
}, {
    "Id": 169,
    "HID": "\/158\/",
    "Name": "Year End Forecasting Admin",
    "Description": "Page used to manage Year End Forecasting process.",
    "ActiveInd": true
}, {
    "Id": 170,
    "HID": "\/159\/",
    "Name": "NLSA Inbox",
    "Description": "Grid used to display NLSAs awaiting action from the user",
    "ActiveInd": true
}, {
    "Id": 171,
    "HID": "\/160\/",
    "Name": "Environment Name Banner",
    "Description": "Banner used to display environment name",
    "ActiveInd": true
}, {
    "Id": 172,
    "HID": "\/161\/",
    "Name": "Database Connection Details Banner",
    "Description": "Banner used to display database connection details",
    "ActiveInd": true
}, {
    "Id": 173,
    "HID": "\/162\/",
    "Name": "Elements of Cost Export\/Import",
    "Description": "Page used to export and import the Elements of Cost Export\/Import report",
    "ActiveInd": true
}, {
    "Id": 174,
    "HID": "\/163\/",
    "Name": "Infrastructure Planning Project List",
    "Description": "Page used to view project plans.",
    "ActiveInd": true
}, {
    "Id": 175,
    "HID": "\/164\/",
    "Name": "Infrastructure Planning Enterprise List",
    "Description": "Page used to view a master list of project plans.",
    "ActiveInd": true
}, {
    "Id": 176,
    "HID": "\/165\/",
    "Name": "Infrastructure Planning Project Portfolio",
    "Description": "Page used to manage Project Portfolios.",
    "ActiveInd": true
}, {
    "Id": 177,
    "HID": "\/166\/",
    "Name": "Infrastructure Planning Scenario",
    "Description": "Page used to view and manage Planning Scenarios.",
    "ActiveInd": true
}, {
    "Id": 178,
    "HID": "\/167\/",
    "Name": "Infrastructure Planning Admin",
    "Description": "Page used for Infrastructure Planning administration.",
    "ActiveInd": false
}, {
    "Id": 179,
    "HID": "\/168\/",
    "Name": "Infrastructure Planning Project Score Import",
    "Description": "Page used to import project scores.",
    "ActiveInd": true
}, {
    "Id": 180,
    "HID": "\/169\/",
    "Name": "Program Management",
    "Description": "Page used to view WBS data.",
    "ActiveInd": true
}, {
    "Id": 181,
    "HID": "\/170\/",
    "Name": "Fire Protection",
    "Description": "Fire Protection",
    "ActiveInd": true
}, {
    "Id": 182,
    "HID": "\/171\/",
    "Name": "Funding Requests",
    "Description": "Page used to view funding requests formerly made through IMPACT",
    "ActiveInd": true
}, {
    "Id": 183,
    "HID": "\/172\/",
    "Name": "SharePoint PWP Upload Location",
    "Description": "SharePoint PWP Upload Location",
    "ActiveInd": true
}, {
    "Id": 184,
    "HID": "\/173\/",
    "Name": "Budget Formulation Comment",
    "Description": "Comments field for Budget Formulation Requests",
    "ActiveInd": true
}, {
    "Id": 185,
    "HID": "\/174\/",
    "Name": "Budget Formulation Federal Funding",
    "Description": "Federal Funding fields for Budget Formulation Requests",
    "ActiveInd": true
}, {
    "Id": 186,
    "HID": "\/175\/",
    "Name": "Country Filter",
    "Description": "WorkBreakdownStructure Dashboard Filter",
    "ActiveInd": true
}, {
    "Id": 187,
    "HID": "\/176\/",
    "Name": "Person Search",
    "Description": "Page used to search for users.",
    "ActiveInd": true
}, {
    "Id": 188,
    "HID": "\/177\/",
    "Name": "Base Site URL",
    "Description": "Base Site URL",
    "ActiveInd": true
}, {
    "Id": 189,
    "HID": "\/180\/",
    "Name": "Travel Admin",
    "Description": "Page used to edit budget for travel",
    "ActiveInd": true
}, {
    "Id": 190,
    "HID": "\/178\/",
    "Name": "Asset Management Inbox",
    "Description": "Grid used to display Asset Management requests awaiting action from the user.",
    "ActiveInd": true
}, {
    "Id": 191,
    "HID": "\/179\/",
    "Name": "Asset Management Review",
    "Description": "Page used during the Asset Management review process.",
    "ActiveInd": true
}, {
    "Id": 192,
    "HID": "\/182\/",
    "Name": "Proposal",
    "Description": "All pages in the proposal module",
    "ActiveInd": true
}, {
    "Id": 193,
    "HID": "\/181\/",
    "Name": "Chief Finance Office Reporting Entity Admin",
    "Description": "Page used to add and edit Chief Finance Office Reporting Entities",
    "ActiveInd": true
}, {
    "Id": 194,
    "HID": "\/183\/",
    "Name": "Task Admin",
    "Description": "Page used to manage tasks.",
    "ActiveInd": true
}, {
    "Id": 195,
    "HID": "\/184\/",
    "Name": "InfrastructurePlanning Export\/Import",
    "Description": "Page used to import\/export Infrastructure Planning project data that is currently in process",
    "ActiveInd": true
}, {
    "Id": 196,
    "HID": "\/187\/",
    "Name": "Activity Funding",
    "Description": "Page used to view activity funding requests",
    "ActiveInd": true
}, {
    "Id": 197,
    "HID": "\/186\/",
    "Name": "Activity Funding Admin",
    "Description": "Page used for admin tasks associated with activity funding requests",
    "ActiveInd": true
}, {
    "Id": 198,
    "HID": "\/185\/",
    "Name": "WFORM Link",
    "Description": "WFORM external Link",
    "ActiveInd": true
}, {
    "Id": 200,
    "HID": "\/188\/",
    "Name": "SharePoint Inventory Assets Location",
    "Description": "SharePoint Inventory Assets Location",
    "ActiveInd": true
}]';

	SET IDENTITY_INSERT Security.PermissionObject ON;

	INSERT INTO Security.PermissionObject (Id, ParentId, HID, Name, Description, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT po.Id, po.ParentId, po.HID, po.Name, po.Description, po.ActiveInd, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@po) WITH (Id INT '$.Id', ParentId INT '$.ParentId', HID NVARCHAR(4000) '$.HID', Name NVARCHAR(50) '$.Name', Description NVARCHAR(2000) '$.Description', ActiveInd BIT '$.ActiveInd') po
	EXCEPT
	SELECT po.Id, po.ParentId, po.HID.ToString(), po.Name, po.Description, po.ActiveInd, @userId, @userId, @userId, @userId 
	FROM Security.PermissionObject po

	SET IDENTITY_INSERT Security.PermissionObject OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH