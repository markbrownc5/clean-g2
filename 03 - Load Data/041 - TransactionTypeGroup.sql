BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Funding"
}, {
    "Id": 2,
    "Name": "Commitment"
}, {
    "Id": 3,
    "Name": "Cost"
}, {
    "Id": 4,
    "Name": "Additional Commitment"
}, {
    "Id": 5,
    "Name": "Indirect and Other Direct Cost"
}]';
	
	SET IDENTITY_INSERT Financial.TransactionTypeGroup ON;
	
	INSERT INTO Financial.TransactionTypeGroup (Id, Name, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.Name, @user, @user, @user, @user
	FROM OPENJSON(@json) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name') oj
	EXCEPT
	SELECT ttg.Id, ttg.Name, @user, @user, @user, @user
	FROM Financial.TransactionTypeGroup ttg;

	SET IDENTITY_INSERT Financial.TransactionTypeGroup OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH