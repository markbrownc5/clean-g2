BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @perm NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Inventory Read",
    "Description": "Read-only permission on inventory data.",
    "ActiveInd": true
}, {
    "Id": 2,
    "Name": "Inventory Edit",
    "Description": "Edit permission on inventory data.",
    "ActiveInd": true
}, {
    "Id": 3,
    "Name": "SPA Read",
    "Description": "Read-only permission on SPA data.",
    "ActiveInd": true
}, {
    "Id": 4,
    "Name": "SPA Edit",
    "Description": "Edit permission on SPA data.",
    "ActiveInd": true
}, {
    "Id": 5,
    "Name": "SPA Submit",
    "Description": "Permission to submit, approve, or reject a SPA.",
    "ActiveInd": true
}, {
    "Id": 6,
    "Name": "IDD Summary Read",
    "Description": "Read-only permission on IDD summary.",
    "ActiveInd": true
}, {
    "Id": 7,
    "Name": "IDD Summary Edit",
    "Description": "Edit permission on IDD summary.",
    "ActiveInd": true
}, {
    "Id": 8,
    "Name": "ART Read",
    "Description": "Read-only permission on Alarm Response Training",
    "ActiveInd": false
}, {
    "Id": 9,
    "Name": "ART Edit",
    "Description": "Edit permission on Alarm Response Training",
    "ActiveInd": false
}, {
    "Id": 10,
    "Name": "Event Read",
    "Description": "Read only permission to Calendar Events.",
    "ActiveInd": true
}, {
    "Id": 11,
    "Name": "Event Edit",
    "Description": "Create, Edit permission for Calendar Events.",
    "ActiveInd": true
}, {
    "Id": 12,
    "Name": "Queue Entities",
    "Description": "Queue enties for participation in the event.",
    "ActiveInd": true
}, {
    "Id": 13,
    "Name": "Approve Entity",
    "Description": "Approve entities for participation",
    "ActiveInd": true
}, {
    "Id": 14,
    "Name": "Approve",
    "Description": "Approve",
    "ActiveInd": true
}, {
    "Id": 15,
    "Name": "Concur",
    "Description": "Ability to Concur",
    "ActiveInd": true
}, {
    "Id": 16,
    "Name": "Courtesy Concur",
    "Description": "Courtesy Concur",
    "ActiveInd": true
}, {
    "Id": 17,
    "Name": "Create",
    "Description": "Create",
    "ActiveInd": true
}, {
    "Id": 18,
    "Name": "Delete",
    "Description": "Delete",
    "ActiveInd": true
}, {
    "Id": 19,
    "Name": "Edit",
    "Description": "Ability to Edit",
    "ActiveInd": true
}, {
    "Id": 20,
    "Name": "Mass Approve",
    "Description": "Mass Approve",
    "ActiveInd": true
}, {
    "Id": 21,
    "Name": "Mass Process",
    "Description": "Mass Process",
    "ActiveInd": true
}, {
    "Id": 22,
    "Name": "Mass Submit",
    "Description": "Mass Submit",
    "ActiveInd": true
}, {
    "Id": 23,
    "Name": "Process",
    "Description": "Process",
    "ActiveInd": true
}, {
    "Id": 24,
    "Name": "Recall",
    "Description": "Recall",
    "ActiveInd": true
}, {
    "Id": 25,
    "Name": "Reject",
    "Description": "Reject",
    "ActiveInd": true
}, {
    "Id": 26,
    "Name": "View Report",
    "Description": "Ability to View Report",
    "ActiveInd": true
}, {
    "Id": 27,
    "Name": "Submit",
    "Description": "Submit",
    "ActiveInd": true
}, {
    "Id": 28,
    "Name": "View",
    "Description": "Ability to view project",
    "ActiveInd": true
}, {
    "Id": 29,
    "Name": "Void",
    "Description": "Void",
    "ActiveInd": true
}, {
    "Id": 31,
    "Name": "Mark\/Unmark Loan Memo",
    "Description": "Mark\/Unmark Loan Memo",
    "ActiveInd": true
}, {
    "Id": 33,
    "Name": "Advance Workflow Level",
    "Description": "Advance Workflow Level",
    "ActiveInd": true
}, {
    "Id": 34,
    "Name": "Close Process",
    "Description": "Close Process",
    "ActiveInd": true
}, {
    "Id": 35,
    "Name": "Open Process",
    "Description": "Open Process",
    "ActiveInd": true
}, {
    "Id": 36,
    "Name": "Print Loan Memo",
    "Description": "Print Loan Memo",
    "ActiveInd": true
}, {
    "Id": 39,
    "Name": "View All Requests",
    "Description": "View All Requests",
    "ActiveInd": true
}, {
    "Id": 40,
    "Name": "Claim",
    "Description": "Claim",
    "ActiveInd": true
}, {
    "Id": 41,
    "Name": "Remove Custom Spend Plan",
    "Description": "Remove Custom Spend Plan",
    "ActiveInd": true
}, {
    "Id": 42,
    "Name": "Show in Inbox",
    "Description": "Show in Inbox",
    "ActiveInd": true
}, {
    "Id": 43,
    "Name": "Responsible Roles Per Workflow",
    "Description": "Responsible Roles Per Workflow",
    "ActiveInd": true
}, {
    "Id": 44,
    "Name": "Variance Review",
    "Description": "Variance Review",
    "ActiveInd": true
}, {
    "Id": 46,
    "Name": "Assign Administrator",
    "Description": "Assign Administrator",
    "ActiveInd": true
}, {
    "Id": 47,
    "Name": "Assign Budget Officer",
    "Description": "Assign Budget Officer",
    "ActiveInd": true
}, {
    "Id": 48,
    "Name": "Override CreatedAs Check",
    "Description": "Override CreatedAs business rule check",
    "ActiveInd": true
}, {
    "Id": 49,
    "Name": "Send E-mail",
    "Description": "Send E-mail",
    "ActiveInd": true
}, {
    "Id": 50,
    "Name": "Show Recall Message",
    "Description": "Determines whether or not a user should see a message indicating a change request has been recalled.",
    "ActiveInd": true
}, {
    "Id": 51,
    "Name": "Pass Back",
    "Description": "Send a change request to a previous level.",
    "ActiveInd": true
}, {
    "Id": 52,
    "Name": "Receive E-mail",
    "Description": "Receive E-mail",
    "ActiveInd": true
}, {
    "Id": 53,
    "Name": "Classify Funding",
    "Description": "Ability to Classify Funding Transactions",
    "ActiveInd": true
}, {
    "Id": 54,
    "Name": "Advance HQ AFP Period",
    "Description": "Advance HQ AFP Period",
    "ActiveInd": true
}, {
    "Id": 55,
    "Name": "Enable\/Disable Financial Transaction Types",
    "Description": "Enable\/Disable Financial Transaction Types",
    "ActiveInd": true
}, {
    "Id": 56,
    "Name": "Mass Void",
    "Description": "Mass Void",
    "ActiveInd": true
}, {
    "Id": 57,
    "Name": "Mass Reject",
    "Description": "Mass Reject",
    "ActiveInd": true
}, {
    "Id": 58,
    "Name": "Purview",
    "Description": "Purview",
    "ActiveInd": true
}, {
    "Id": 59,
    "Name": "Import",
    "Description": "Import data",
    "ActiveInd": true
}, {
    "Id": 60,
    "Name": "Transfer",
    "Description": "Transfer data",
    "ActiveInd": true
}, {
    "Id": 61,
    "Name": "Lock \/ Unlock",
    "Description": "Lock \/ unlock data",
    "ActiveInd": true
}, {
    "Id": 62,
    "Name": "Close Cost Period",
    "Description": "Close Cost Period",
    "ActiveInd": true
}, {
    "Id": 63,
    "Name": "Cancel",
    "Description": "Cancel",
    "ActiveInd": true
}, {
    "Id": 64,
    "Name": "Invite",
    "Description": "Invite Event Participants",
    "ActiveInd": true
}, {
    "Id": 65,
    "Name": "Allow Loan Memo",
    "Description": "Allow Loan Memo",
    "ActiveInd": true
}, {
    "Id": 66,
    "Name": "Receive Report Subscription",
    "Description": "Ability to Receive Subscriptions for Report",
    "ActiveInd": true
}, {
    "Id": 67,
    "Name": "Copy on E-mail",
    "Description": "Copy on E-mail",
    "ActiveInd": true
}, {
    "Id": 68,
    "Name": "Unactualize",
    "Description": "Unactualize Project Milestone",
    "ActiveInd": true
}, {
    "Id": 69,
    "Name": "Edit GIS Bookmark",
    "Description": "Allows a user to edit a GIS bookmark",
    "ActiveInd": true
}, {
    "Id": 70,
    "Name": "Edit Global GIS Bookmark",
    "Description": "Allows a user to edit a global GIS bookmark",
    "ActiveInd": true
}, {
    "Id": 71,
    "Name": "Edit Processed Request",
    "Description": "Ability to edit processed requests.",
    "ActiveInd": true
}, {
    "Id": 72,
    "Name": "Tag",
    "Description": "Ability to create and edit tags.",
    "ActiveInd": true
}, {
    "Id": 73,
    "Name": "Rank",
    "Description": "Rank",
    "ActiveInd": true
}, {
    "Id": 74,
    "Name": "HQ Rank",
    "Description": "HQ Rank",
    "ActiveInd": true
}, {
    "Id": 75,
    "Name": "Set Default",
    "Description": "Set Default",
    "ActiveInd": true
}, {
    "Id": 76,
    "Name": "Super Proxy",
    "Description": "Determines whether a user can proxy as any other user, regardless of the Proxy table",
    "ActiveInd": true
}, {
    "Id": 77,
    "Name": "Remove Scope",
    "Description": "Remove Scope",
    "ActiveInd": true
}, {
    "Id": 78,
    "Name": "Assign Planning: Project Planning Manager",
    "Description": "Can assign a Planning: Project Planning Manager to a Planning Scenario",
    "ActiveInd": true
}, {
    "Id": 79,
    "Name": "Edit HQ Comment",
    "Description": "Edit HQ Comment",
    "ActiveInd": true
}, {
    "Id": 80,
    "Name": "View HQ Comment",
    "Description": "View HQ Comment",
    "ActiveInd": true
}]';

	SET IDENTITY_INSERT Security.Permission ON;

	INSERT INTO Security.Permission (Id, Name, Description, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT p.Id, p.Name, p.Description, p.ActiveInd, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@perm) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', Description VARCHAR(2000) '$.Description', ActiveInd BIT '$.ActiveInd') p
	EXCEPT
	SELECT p.Id, p.Name, p.Description, p.ActiveInd, @userId, @userId, @userId, @userId 
	FROM Security.Permission p;

	SET IDENTITY_INSERT Security.Permission OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH