BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;

	DECLARE @user INT = 614;

	INSERT INTO G2.WorkBreakdownStructureWorkTypeXref (WorkBreakdownStructureId, WorkTypeId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT wbs.Id, wt.Id, @user, @user, @user, @user 
	FROM G2.WorkBreakdownStructure wbs
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = wbs.SubSystemLevelId
	CROSS JOIN G2.WorkType wt 
	WHERE ssl.Label = 'Project'
	AND wt.Name = 'Construction: Execution'
	EXCEPT
	SELECT wbswtx.WorkBreakdownStructureId, wbswtx.WorkTypeId, @user, @user, @user, @user
	FROM G2.WorkBreakdownStructureWorkTypeXref wbswtx
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH