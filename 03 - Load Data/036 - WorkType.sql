BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 14,
    "Name": "Capital Equipment: MIE",
    "LongName": "Capital Equipment: Major Item of Equipment (MIE)",
    "ShortName": "Capital Equipment: MIE",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 3,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 15,
    "Name": "Capital Equipment: Other",
    "LongName": "Capital Equipment: Other Equipment",
    "ShortName": "Capital Equipment: Other",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 4,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 16,
    "Name": "Construction: Other",
    "LongName": "Construction: Other",
    "ShortName": "Construction: Other",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 6,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 17,
    "Name": "Cross-Cutting",
    "LongName": "Cross-Cutting Projects",
    "ShortName": "Cross-Cutting",
    "WorkTypeGroupId": 8,
    "WorkTypeCategoryId": 5,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 18,
    "Name": "Disposition",
    "LongName": "Disposition",
    "ShortName": "Disposition",
    "WorkTypeGroupId": 26,
    "WorkTypeCategoryId": 22,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 19,
    "Name": "Long Term Stewardship",
    "LongName": "Long Term Stewardship (LTS)",
    "ShortName": "LTS",
    "WorkTypeGroupId": 28,
    "WorkTypeCategoryId": 24,
    "DisplayOrder": 3,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 20,
    "Name": "Maintenance: Other",
    "LongName": "Maintenance: Other",
    "ShortName": "Maint: Other",
    "WorkTypeGroupId": 11,
    "WorkTypeCategoryId": 8,
    "DisplayOrder": 4,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 21,
    "Name": "Management Projects",
    "LongName": "Management Projects",
    "ShortName": "Management Projects",
    "WorkTypeGroupId": 7,
    "WorkTypeCategoryId": 4,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 22,
    "Name": "Minor Construction: Other",
    "LongName": "Minor Construction: Other",
    "ShortName": "Minor Construc: Other",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 2,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 23,
    "Name": "Nuc Safety",
    "LongName": "Nuclear Safety",
    "ShortName": "Nuc Safety",
    "WorkTypeGroupId": 28,
    "WorkTypeCategoryId": 24,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 24,
    "Name": "Ops of Facilities",
    "LongName": "Operations of Facilities",
    "ShortName": "Ops of Facilities",
    "WorkTypeGroupId": 12,
    "WorkTypeCategoryId": 9,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 25,
    "Name": "Packaging",
    "LongName": "Packaging",
    "ShortName": "Packaging",
    "WorkTypeGroupId": 28,
    "WorkTypeCategoryId": 24,
    "DisplayOrder": 2,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 26,
    "Name": "Recap: Equipment and Capabilities",
    "LongName": "Recap: Equipment and Capabilities",
    "ShortName": "Recap: Equip. and Cap.",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 5,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 27,
    "Name": "Recap: Other",
    "LongName": "Recap: Other",
    "ShortName": "Recap: Other",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 7,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 28,
    "Name": "Recap: Replace\/Refurb",
    "LongName": "Recap: Replacement\/Refurbishment",
    "ShortName": "Recap: Replace\/Refurb",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 6,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 29,
    "Name": "Construction: OPC",
    "LongName": "Construction: OPC",
    "ShortName": "Construction: OPC",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 4,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 30,
    "Name": "Recap: OPC",
    "LongName": "Recap: Other Project Costs",
    "ShortName": "Recap: OPC",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 8,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 89,
    "Name": "AMP",
    "LongName": "Asset Management Program",
    "ShortName": "AMP",
    "WorkTypeGroupId": 27,
    "WorkTypeCategoryId": 23,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 90,
    "Name": "Construction: Design",
    "LongName": "Construction: Design",
    "ShortName": "Construction: Design",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 91,
    "Name": "Construction: Execution",
    "LongName": "Construction: Execution",
    "ShortName": "Construction: Execution",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 2,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 92,
    "Name": "Construction: Contingency",
    "LongName": "Construction: Contingency",
    "ShortName": "Construction: Contingency",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 3,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 93,
    "Name": "Construction: Cost Savings",
    "LongName": "Construction: Cost Savings",
    "ShortName": "Construction: Cost Sav.",
    "WorkTypeGroupId": 9,
    "WorkTypeCategoryId": 6,
    "DisplayOrder": 5,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 94,
    "Name": "NMI",
    "LongName": "Nuclear Materials Integration",
    "ShortName": "NMI",
    "WorkTypeGroupId": 28,
    "WorkTypeCategoryId": 24,
    "DisplayOrder": 4,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 96,
    "Name": "Maintenance: Corrective",
    "LongName": "Maintenance: Corrective",
    "ShortName": "Maint: Corrective",
    "WorkTypeGroupId": 11,
    "WorkTypeCategoryId": 8,
    "DisplayOrder": 3,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 97,
    "Name": "Maintenance: Management",
    "LongName": "Maintenance: Management",
    "ShortName": "Maint: Management",
    "WorkTypeGroupId": 11,
    "WorkTypeCategoryId": 8,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 98,
    "Name": "Maintenance: Preventative",
    "LongName": "Maintenance: Preventative",
    "ShortName": "Maint: Preventative",
    "WorkTypeGroupId": 11,
    "WorkTypeCategoryId": 8,
    "DisplayOrder": 2,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 99,
    "Name": "Minor Construction: New Footprint",
    "LongName": "Minor Construction: New Footprint",
    "ShortName": "Minor Construc: New Foot.",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 1,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 105,
    "Name": "Indirect Disposition",
    "LongName": "Indirect Disposition",
    "ShortName": "Indirect Disposition",
    "WorkTypeGroupId": 26,
    "WorkTypeCategoryId": 22,
    "DisplayOrder": 2,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}, {
    "Id": 106,
    "Name": "Indirect Recap",
    "LongName": "Indirect Recap",
    "ShortName": "Indirect Recap",
    "WorkTypeGroupId": 10,
    "WorkTypeCategoryId": 7,
    "DisplayOrder": 9,
    "DashboardFilterInd": true,
    "AddNewTaskInd": true,
    "ChangeSiteInd": false,
    "ChangeBuildingInd": false,
    "ChangeDeviceInd": false
}]';

	SET IDENTITY_INSERT G2.WorkType ON;

	INSERT INTO G2.WorkType (Id, Name, LongName, ShortName, WorkTypeGroupId, WorkTypeCategoryId, DisplayOrder, DashboardFilterInd, AddNewTaskInd, ChangeSiteInd,
		ChangeBuildingInd, ChangeDeviceInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.Name, oj.LongName, oj.ShortName, oj.WorkTypeGroupId, oj.WorkTypeCategoryId, oj.DisplayOrder,
		   oj.DashboardFilterInd, oj.AddNewTaskInd, oj.ChangeSiteInd, oj.ChangeBuildingInd, oj.ChangeDeviceInd, 
		   @user, @user, @user, @user
	FROM OPENJSON(@json) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', LongName NVARCHAR(100) '$.LongName', ShortName NVARCHAR(25) '$.ShortName',
		WorkTypeGroupId INT '$.WorkTypeGroupId', WorkTypeCategoryId INT '$.WorkTypeCategoryId', DisplayOrder INT '$.DisplayOrder', 
		DashboardFilterInd BIT '$.DashboardFilterInd', AddNewTaskInd BIT '$.AddNewTaskInd', ChangeSiteInd BIT '$.ChangeSiteInd',
		ChangeBuildingInd BIT '$.ChangeBuildingInd', ChangeDeviceInd BIT '$.ChangeDeviceInd') oj
	EXCEPT
	SELECT wt.Id, wt.Name, wt.LongName, wt.ShortName, wt.WorkTypeGroupId, wt.WorkTypeCategoryId, wt.DisplayOrder, wt.DashboardFilterInd,
		   wt.AddNewTaskInd, wt.ChangeSiteInd, wt.ChangeBuildingInd, wt.ChangeDeviceInd, @user, @user, @user, @user 
	FROM G2.WorkType wt;

	SET IDENTITY_INSERT G2.WorkType OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH