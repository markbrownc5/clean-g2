BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Non-M&O Operating",
    "NAMBObjectId": 1,
    "NAMBObjectClass": 25200
}, {
    "Id": 2,
    "Name": "M&O Operating",
    "NAMBObjectId": 2,
    "NAMBObjectClass": 25400
}, {
    "Id": 3,
    "Name": "MIE",
    "NAMBObjectId": 3,
    "NAMBObjectClass": 31001
}, {
    "Id": 4,
    "Name": "GPE",
    "NAMBObjectId": 4,
    "NAMBObjectClass": 31002
}, {
    "Id": 5,
    "Name": "EQU",
    "NAMBObjectId": 5,
    "NAMBObjectClass": 31003
}, {
    "Id": 6,
    "Name": "GPP",
    "NAMBObjectId": 6,
    "NAMBObjectClass": 32002
}, {
    "Id": 7,
    "Name": "Line Item",
    "NAMBObjectId": 7,
    "NAMBObjectClass": 32001
}, {
    "Id": 8,
    "Name": "Grants, Subsidies, and Contributions",
    "NAMBObjectId": 9,
    "NAMBObjectClass": 41000
}]';

	SET IDENTITY_INSERT NAMB.FundingClassification ON;

	INSERT INTO NAMB.FundingClassification (Id, Name, NAMBObjectId, NAMBObjectClass, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.Name, oj.NAMBObjectId, oj.NAMBObjectClass, @user, @user, @user, @user
	FROM OPENJSON(@json) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', NAMBObjectId INT '$.NAMBObjectId', NAMBObjectClass INT '$.NAMBObjectClass') oj
	EXCEPT
	SELECT fc.Id,
		   fc.Name,
		   fc.NAMBObjectId,
		   fc.NAMBObjectClass, @user, @user, @user, @user
	FROM NAMB.FundingClassification fc;

	SET IDENTITY_INSERT NAMB.FundingClassification OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH