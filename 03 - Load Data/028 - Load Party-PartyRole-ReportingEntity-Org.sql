BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @party NVARCHAR(MAX) = '[{
    "Id": 14,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 15,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 16,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 17,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 18,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 19,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 20,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1285,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1286,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1287,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1288,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1289,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1290,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1291,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1292,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1293,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1295,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1297,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1298,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1299,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1300,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1301,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1303,
    "DeletedInd": false,
    "pr": [{
        "prtId": 8
    }]
}, {
    "Id": 1305,
    "DeletedInd": false,
    "pr": [{
        "prtId": 7
    }, {
        "prtId": 13
    }]
}, {
    "Id": 1306,
    "DeletedInd": false,
    "pr": [{
        "prtId": 7
    }]
}, {
    "Id": 1310,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1311,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 8
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1312,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1315,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1316,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1318,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1319,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1320,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1321,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1324,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1326,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1327,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1332,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1334,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1335,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1336,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1338,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1340,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }, {
        "prtId": 13
    }, {
        "prtId": 14
    }, {
        "prtId": 15
    }, {
        "prtId": 16
    }]
}, {
    "Id": 1365,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1367,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1382,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1385,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 8
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1387,
    "DeletedInd": false,
    "pr": [{
        "prtId": 11
    }]
}, {
    "Id": 1393,
    "DeletedInd": false,
    "pr": [{
        "prtId": 12
    }]
}, {
    "Id": 1398,
    "DeletedInd": false,
    "pr": [{
        "prtId": 12
    }]
}, {
    "Id": 1457,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1464,
    "DeletedInd": false,
    "pr": [{
        "prtId": 6
    }, {
        "prtId": 7
    }, {
        "prtId": 9
    }, {
        "prtId": 11
    }]
}, {
    "Id": 1505,
    "DeletedInd": false,
    "pr": [{
        "prtId": 9
    }, {
        "prtId": 15
    }]
}, {
    "Id": 1511,
    "DeletedInd": false,
    "pr": [{
        "prtId": 15
    }]
}, {
    "Id": 1,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 2,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 3,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 4,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 5,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 6,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 7,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 8,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 9,
    "DeletedInd": true,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 10,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 11,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 12,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 13,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 14,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 15,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 16,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 17,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 18,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 19,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 20,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 21,
    "DeletedInd": true,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1193,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1194,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1195,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1196,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1197,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1198,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1200,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1201,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1202,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1203,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1204,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1205,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1206,
    "DeletedInd": false,
    "pr": [{
        "prtId": 4
    }]
}, {
    "Id": 1207,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1208,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1209,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1210,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1211,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1212,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1213,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1223,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1224,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1225,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1226,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1227,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1228,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1229,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1230,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1231,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1232,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1233,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1234,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1235,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1236,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1237,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1238,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1239,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1240,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1241,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1242,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1243,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1244,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1245,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1246,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1247,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1248,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1249,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1250,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1251,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1252,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1253,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1254,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1255,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1256,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1257,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1258,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1259,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1260,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1261,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1262,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1263,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1264,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1265,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1266,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1267,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1268,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1269,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1270,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1271,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1272,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1273,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1274,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1275,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1276,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1342,
    "DeletedInd": true,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1343,
    "DeletedInd": true,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1344,
    "DeletedInd": true,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1393,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1394,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1395,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1396,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1397,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1398,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1399,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1400,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1401,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1402,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1403,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1404,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1440,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1441,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1442,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1443,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1444,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1445,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1446,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1455,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1574,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1575,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}, {
    "Id": 1579,
    "DeletedInd": false,
    "pr": [{
        "prtId": 2
    }]
}]', @orgs NVARCHAR(MAX) = '[{
    "PartyId": 1,
    "OrganizationBreakdownStructure": "NA-00",
    "Name": "INFRASTRUCTURE AND OPERATIONS",
    "OrganizationHierarchyId": "\/1\/",
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 2,
    "OrganizationBreakdownStructure": "NA-00.1",
    "Name": "PROGRAM EXECUTIVE OFFICE",
    "OrganizationHierarchyId": "\/1\/1\/",
    "ParentId": 1,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 3,
    "OrganizationBreakdownStructure": "NA-00.1.1",
    "Name": "EMPLOYEE INVOLVEMENT",
    "OrganizationHierarchyId": "\/1\/1\/1\/",
    "ParentId": 2,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 4,
    "OrganizationBreakdownStructure": "NA-00.1.2",
    "Name": "OFFICE OF CAPABILITIES ASSESSMENT",
    "OrganizationHierarchyId": "\/1\/1\/2\/",
    "ParentId": 2,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 5,
    "OrganizationBreakdownStructure": "NA-00.1.3",
    "Name": "OFFICE OF INNOVATIVE SOLUTIONS",
    "OrganizationHierarchyId": "\/1\/1\/3\/",
    "ParentId": 2,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 6,
    "OrganizationBreakdownStructure": "NA-00.2",
    "Name": "FIELD COORDINATION OFFICE",
    "OrganizationHierarchyId": "\/1\/2\/",
    "ParentId": 1,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 7,
    "OrganizationBreakdownStructure": "NA-00-10",
    "Name": "OFFICE OF ENVIRONMENT, SAFETY AND HEALTH",
    "OrganizationHierarchyId": "\/1\/3\/",
    "ParentId": 1,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 8,
    "OrganizationBreakdownStructure": "NA-00-20",
    "Name": "OFFICE OF INFRASTRUCTURE AND CAPITAL PLANNING",
    "OrganizationHierarchyId": "\/1\/4\/",
    "ParentId": 1,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 9,
    "OrganizationBreakdownStructure": "NA-00-30",
    "Name": "OFFICE OF SECURITY OPERATIONS",
    "OrganizationHierarchyId": "\/1\/5\/",
    "ParentId": 1,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 10,
    "OrganizationBreakdownStructure": "NA-00-40",
    "Name": "OFFICE OF PACKAGING AND TRANSPORTATION",
    "OrganizationHierarchyId": "\/1\/6\/",
    "ParentId": 1,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 11,
    "OrganizationBreakdownStructure": "NA-00-50",
    "Name": "OFFICE OF INFRASTRUCTURE RESOURCE MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/7\/",
    "ParentId": 1,
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "PartyId": 12,
    "OrganizationBreakdownStructure": "NA-00-60",
    "Name": "OFFICE OF SUSTAINABILITY",
    "OrganizationHierarchyId": "\/1\/8\/",
    "ParentId": 1,
    "SortOrder": 8,
    "ActiveInd": true
}, {
    "PartyId": 13,
    "OrganizationBreakdownStructure": "NA-00-70",
    "Name": "OFFICE OF QUALITY MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/9\/",
    "ParentId": 1,
    "SortOrder": 9,
    "ActiveInd": true
}, {
    "PartyId": 14,
    "OrganizationBreakdownStructure": "NA-KC",
    "Name": "KANSAS CITY FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/10\/",
    "ParentId": 1,
    "SortOrder": 16,
    "ActiveInd": true
}, {
    "PartyId": 15,
    "OrganizationBreakdownStructure": "NA-LA",
    "Name": "LOS ALAMOS FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/11\/",
    "ParentId": 1,
    "SortOrder": 13,
    "ActiveInd": true
}, {
    "PartyId": 16,
    "OrganizationBreakdownStructure": "NA-LL",
    "Name": "LIVERMORE FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/12\/",
    "ParentId": 1,
    "SortOrder": 11,
    "ActiveInd": true
}, {
    "PartyId": 17,
    "OrganizationBreakdownStructure": "NA-NV",
    "Name": "NEVADA FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/13\/",
    "ParentId": 1,
    "SortOrder": 12,
    "ActiveInd": true
}, {
    "PartyId": 18,
    "OrganizationBreakdownStructure": "NA-NPO",
    "Name": "NNSA PRODUCTION OFFICE",
    "OrganizationHierarchyId": "\/1\/14\/",
    "ParentId": 1,
    "SortOrder": 15,
    "ActiveInd": true
}, {
    "PartyId": 19,
    "OrganizationBreakdownStructure": "NA-SV",
    "Name": "SAVANNAH RIVER FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/15\/",
    "ParentId": 1,
    "SortOrder": 17,
    "ActiveInd": true
}, {
    "PartyId": 20,
    "OrganizationBreakdownStructure": "NA-SN",
    "Name": "SANDIA FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/16\/",
    "ParentId": 1,
    "SortOrder": 14,
    "ActiveInd": true
}, {
    "PartyId": 21,
    "OrganizationBreakdownStructure": "NA-21 GTRI",
    "Name": "OFFICE OF GLOBAL THREAT REDUCTION INITIATIVE",
    "OrganizationHierarchyId": "\/2\/",
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 1193,
    "OrganizationBreakdownStructure": "NA-1",
    "Name": "NNSA FRONT OFFICE",
    "OrganizationHierarchyId": "\/3\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1194,
    "OrganizationBreakdownStructure": "NA-10",
    "Name": "DEFENSE PROGRAMS",
    "OrganizationHierarchyId": "\/4\/",
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1195,
    "OrganizationBreakdownStructure": "NA-20",
    "Name": "DEFENSE NUCLEAR NONPROLIFERATION",
    "OrganizationHierarchyId": "\/5\/",
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1196,
    "OrganizationBreakdownStructure": "NA-30",
    "Name": "NAVAL REACTORS",
    "OrganizationHierarchyId": "\/6\/",
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 1197,
    "OrganizationBreakdownStructure": "NA-40",
    "Name": "EMERGENCY OPERATIONS",
    "OrganizationHierarchyId": "\/7\/",
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "PartyId": 1198,
    "OrganizationBreakdownStructure": "NA-70",
    "Name": "DEFENSE NUCLEAR SECURITY",
    "OrganizationHierarchyId": "\/8\/",
    "SortOrder": 8,
    "ActiveInd": true
}, {
    "PartyId": 1200,
    "OrganizationBreakdownStructure": "NA-MB",
    "Name": "MANAGEMENT & BUDGET",
    "OrganizationHierarchyId": "\/10\/",
    "SortOrder": 10,
    "ActiveInd": true
}, {
    "PartyId": 1201,
    "OrganizationBreakdownStructure": "NA-IM",
    "Name": "INFORMATION MANAGEMENT",
    "OrganizationHierarchyId": "\/11\/",
    "SortOrder": 11,
    "ActiveInd": true
}, {
    "PartyId": 1202,
    "OrganizationBreakdownStructure": "NA-SH",
    "Name": "SAFETY & HEALTH",
    "OrganizationHierarchyId": "\/12\/",
    "SortOrder": 12,
    "ActiveInd": true
}, {
    "PartyId": 1203,
    "OrganizationBreakdownStructure": "NA-EA",
    "Name": "EXTERNAL AFFAIRS",
    "OrganizationHierarchyId": "\/13\/",
    "SortOrder": 13,
    "ActiveInd": true
}, {
    "PartyId": 1204,
    "OrganizationBreakdownStructure": "NA-GC",
    "Name": "OFFICE OF GENERAL COUNSEL",
    "OrganizationHierarchyId": "\/14\/",
    "SortOrder": 14,
    "ActiveInd": true
}, {
    "PartyId": 1205,
    "OrganizationBreakdownStructure": "NA-APM",
    "Name": "ACQUISITIONS & PROJECT MANAGEMENT",
    "OrganizationHierarchyId": "\/15\/",
    "SortOrder": 15,
    "ActiveInd": true
}, {
    "PartyId": 1206,
    "OrganizationBreakdownStructure": "OTHER",
    "Name": "INCLUDE ORGANIZATION IN TRIP JUSTIFICATION",
    "OrganizationHierarchyId": "\/16\/",
    "SortOrder": 16,
    "ActiveInd": true
}, {
    "PartyId": 1207,
    "OrganizationBreakdownStructure": "NA-NPO.01",
    "Name": "MANAGER''S OFFICE",
    "OrganizationHierarchyId": "\/1\/14\/1\/",
    "ParentId": 18,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1208,
    "OrganizationBreakdownStructure": "NA-NPO.10",
    "Name": "NUCLEAR SAFETY & ENGINEERING",
    "OrganizationHierarchyId": "\/1\/14\/2\/",
    "ParentId": 18,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1209,
    "OrganizationBreakdownStructure": "NA-NPO.20",
    "Name": "SAFEGUARDS & SECURITY",
    "OrganizationHierarchyId": "\/1\/14\/3\/",
    "ParentId": 18,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1210,
    "OrganizationBreakdownStructure": "NA-NPO.30",
    "Name": "OPERATIONS MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/14\/4\/",
    "ParentId": 18,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1211,
    "OrganizationBreakdownStructure": "NA-NPO.50",
    "Name": "BUSINESS & CONTRACT MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/14\/5\/",
    "ParentId": 18,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 1212,
    "OrganizationBreakdownStructure": "NA-NPO.60",
    "Name": "ENVIRONMENT,SAFETY,HEALTH & QUALITY",
    "OrganizationHierarchyId": "\/1\/14\/6\/",
    "ParentId": 18,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 1213,
    "OrganizationBreakdownStructure": "NA-NPO.70",
    "Name": "PROGRAMS & PROJECTS",
    "OrganizationHierarchyId": "\/1\/14\/7\/",
    "ParentId": 18,
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "PartyId": 1223,
    "OrganizationBreakdownStructure": "NA-LL.SSO",
    "Name": "SECURITY, SAFETY, AND OPERATIONS",
    "OrganizationHierarchyId": "\/1\/12\/1\/",
    "ParentId": 16,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1224,
    "OrganizationBreakdownStructure": "NA-LL.SSO.FO",
    "Name": "FACILITY OPERATIONS",
    "OrganizationHierarchyId": "\/1\/12\/1\/1\/",
    "ParentId": 1223,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1225,
    "OrganizationBreakdownStructure": "NA-LL.SSO.ESH",
    "Name": "ENVIRONMENT, SAFETY & HEALTH",
    "OrganizationHierarchyId": "\/1\/12\/1\/2\/",
    "ParentId": 1223,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1226,
    "OrganizationBreakdownStructure": "NA-LL.SSO.SI",
    "Name": "SUSTAINABILITY AND INFRASTRUCTURE",
    "OrganizationHierarchyId": "\/1\/12\/1\/3\/",
    "ParentId": 1223,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1227,
    "OrganizationBreakdownStructure": "NA-LL.SSO.SS",
    "Name": "SAFEGUARDS & SECURITY",
    "OrganizationHierarchyId": "\/1\/12\/1\/4\/",
    "ParentId": 1223,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1228,
    "OrganizationBreakdownStructure": "NA-LL.PB",
    "Name": "PROGRAMS AND BUSINESS",
    "OrganizationHierarchyId": "\/1\/12\/2\/",
    "ParentId": 16,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1229,
    "OrganizationBreakdownStructure": "NA-LL.PB.CB",
    "Name": "CONTRACTS & BUSINESS",
    "OrganizationHierarchyId": "\/1\/12\/2\/1\/",
    "ParentId": 1228,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1230,
    "OrganizationBreakdownStructure": "NA-LL.PB.IM",
    "Name": "INTERAGENCY MISSIONS",
    "OrganizationHierarchyId": "\/1\/12\/2\/2\/",
    "ParentId": 1228,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1231,
    "OrganizationBreakdownStructure": "NA-LL.PB.DP",
    "Name": "DEFENSE PROGRAMS",
    "OrganizationHierarchyId": "\/1\/12\/2\/3\/",
    "ParentId": 1228,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1232,
    "OrganizationBreakdownStructure": "NA-LA.QAS",
    "Name": "QUALITY ASSURANCE STAFF",
    "OrganizationHierarchyId": "\/1\/11\/1\/",
    "ParentId": 15,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1233,
    "OrganizationBreakdownStructure": "NA-LA.SOCS",
    "Name": "SITE OFFICE COUNSEL STAFF",
    "OrganizationHierarchyId": "\/1\/11\/2\/",
    "ParentId": 15,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1234,
    "OrganizationBreakdownStructure": "NA-LA.NSM",
    "Name": "NATIONAL SECURITY MISSIONS",
    "OrganizationHierarchyId": "\/1\/11\/3\/",
    "ParentId": 15,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1235,
    "OrganizationBreakdownStructure": "NA-LA.NSM.PIT",
    "Name": "PROGRAM INTEGRATION TEAM",
    "OrganizationHierarchyId": "\/1\/11\/3\/1\/",
    "ParentId": 1234,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1236,
    "OrganizationBreakdownStructure": "NA-LA.NSM.LPT",
    "Name": "LANDLORD PROGRAM TEAM",
    "OrganizationHierarchyId": "\/1\/11\/3\/2\/",
    "ParentId": 1234,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1237,
    "OrganizationBreakdownStructure": "NA-LA.NSM.MPST",
    "Name": "PROJECT MANAGEMENT SUPPORT TEAM",
    "OrganizationHierarchyId": "\/1\/11\/3\/3\/",
    "ParentId": 1234,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1238,
    "OrganizationBreakdownStructure": "NA-LA.EPO",
    "Name": "ENVIRONMENT PROJECTS OFFICE",
    "OrganizationHierarchyId": "\/1\/11\/4\/",
    "ParentId": 15,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1239,
    "OrganizationBreakdownStructure": "NA-LA.EPO.WMT",
    "Name": "WASTE MANAGEMENT TEAM",
    "OrganizationHierarchyId": "\/1\/11\/4\/1\/",
    "ParentId": 1238,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1240,
    "OrganizationBreakdownStructure": "NA-LA.EPO.BTS",
    "Name": "BUSINESS & TECHNICAL SERVICES",
    "OrganizationHierarchyId": "\/1\/11\/4\/2\/",
    "ParentId": 1238,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1241,
    "OrganizationBreakdownStructure": "NA-LA.EPO.SG",
    "Name": "SOIL AND GROUNDWATER",
    "OrganizationHierarchyId": "\/1\/11\/4\/3\/",
    "ParentId": 1238,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1242,
    "OrganizationBreakdownStructure": "NA-LA.SS",
    "Name": "SAFEGUARDS & SECURITY",
    "OrganizationHierarchyId": "\/1\/11\/5\/",
    "ParentId": 15,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 1243,
    "OrganizationBreakdownStructure": "NA-LA.SS.CST",
    "Name": "CYBER SECURITY TEAM",
    "OrganizationHierarchyId": "\/1\/11\/5\/1\/",
    "ParentId": 1242,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1244,
    "OrganizationBreakdownStructure": "NA-LA.SS.SOT",
    "Name": "SECURITY OPERATIONS TEAM",
    "OrganizationHierarchyId": "\/1\/11\/5\/2\/",
    "ParentId": 1242,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1245,
    "OrganizationBreakdownStructure": "NA-LA.SO",
    "Name": "SAFETY OPERATIONS",
    "OrganizationHierarchyId": "\/1\/11\/6\/",
    "ParentId": 15,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 1246,
    "OrganizationBreakdownStructure": "NA-LA.SO.SBT",
    "Name": "SAFETY BASIS TEAM",
    "OrganizationHierarchyId": "\/1\/11\/6\/1\/",
    "ParentId": 1245,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1247,
    "OrganizationBreakdownStructure": "NA-LA.FO",
    "Name": "FIELD OPERATIONS",
    "OrganizationHierarchyId": "\/1\/11\/7\/",
    "ParentId": 15,
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "PartyId": 1248,
    "OrganizationBreakdownStructure": "NA-LA.FO.FRT",
    "Name": "FACILITY REPRESENTATIVE TEAM",
    "OrganizationHierarchyId": "\/1\/11\/7\/1\/",
    "ParentId": 1247,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1249,
    "OrganizationBreakdownStructure": "NA-LA.FO.SET",
    "Name": "SAFETY ENGINEERING TEAM",
    "OrganizationHierarchyId": "\/1\/11\/7\/2\/",
    "ParentId": 1247,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1250,
    "OrganizationBreakdownStructure": "NA-LA.BA",
    "Name": "BUSINESS ADMINISTRATION",
    "OrganizationHierarchyId": "\/1\/11\/8\/",
    "ParentId": 15,
    "SortOrder": 8,
    "ActiveInd": true
}, {
    "PartyId": 1251,
    "OrganizationBreakdownStructure": "NA-LA.BA.CAS",
    "Name": "CONTRACT ADMINISTRATION STAFF",
    "OrganizationHierarchyId": "\/1\/11\/8\/1\/",
    "ParentId": 1250,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1252,
    "OrganizationBreakdownStructure": "NA-SN.AP",
    "Name": "ADMIN POOL",
    "OrganizationHierarchyId": "\/1\/16\/1\/",
    "ParentId": 20,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1253,
    "OrganizationBreakdownStructure": "NA-SN.PA",
    "Name": "PERFORMANCE ASSURANCE",
    "OrganizationHierarchyId": "\/1\/16\/2\/",
    "ParentId": 20,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1254,
    "OrganizationBreakdownStructure": "NA-SN.CABM",
    "Name": "CONTRACT ADMINISTRATION AND BUSINESS MANAGEMNET",
    "OrganizationHierarchyId": "\/1\/16\/3\/",
    "ParentId": 20,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1255,
    "OrganizationBreakdownStructure": "NA-SN.EN",
    "Name": "ENGINEERING",
    "OrganizationHierarchyId": "\/1\/16\/4\/",
    "ParentId": 20,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1256,
    "OrganizationBreakdownStructure": "NA-SN.OP",
    "Name": "OPERATIONS",
    "OrganizationHierarchyId": "\/1\/16\/5\/",
    "ParentId": 20,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 1257,
    "OrganizationBreakdownStructure": "NA-SN.PM",
    "Name": "PROGRAMS",
    "OrganizationHierarchyId": "\/1\/16\/6\/",
    "ParentId": 20,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 1258,
    "OrganizationBreakdownStructure": "NA-SN.SS",
    "Name": "SAFEGUARDS & SECURITY",
    "OrganizationHierarchyId": "\/1\/16\/7\/",
    "ParentId": 20,
    "SortOrder": 7,
    "ActiveInd": true
}, {
    "PartyId": 1259,
    "OrganizationBreakdownStructure": "NA-KC.OSM",
    "Name": "OFFICE OF STOCKPILE MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/10\/1\/",
    "ParentId": 14,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1260,
    "OrganizationBreakdownStructure": "NA-KC.OSM.PM",
    "Name": "PROGRAM MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/10\/1\/1\/",
    "ParentId": 1259,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1261,
    "OrganizationBreakdownStructure": "NA-KC.OSM.WQA",
    "Name": "WEAPONS QUALITY ASSURANCE",
    "OrganizationHierarchyId": "\/1\/10\/1\/2\/",
    "ParentId": 1259,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1262,
    "OrganizationBreakdownStructure": "NA-KC.OO",
    "Name": "OFFICE OF OPERATIONS",
    "OrganizationHierarchyId": "\/1\/10\/2\/",
    "ParentId": 14,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1263,
    "OrganizationBreakdownStructure": "NA-KC.OO.AM",
    "Name": "KCRIMS - ASSET MANGEMENT",
    "OrganizationHierarchyId": "\/1\/10\/2\/1\/",
    "ParentId": 1262,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1264,
    "OrganizationBreakdownStructure": "NA-KC.OO.TO",
    "Name": "TECHNICAL OPERATIONS",
    "OrganizationHierarchyId": "\/1\/10\/2\/2\/",
    "ParentId": 1262,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1265,
    "OrganizationBreakdownStructure": "NA-KC.OBSO",
    "Name": "OFFICE OF BUSINESS & SECURITY OPERATIONS",
    "OrganizationHierarchyId": "\/1\/10\/3\/",
    "ParentId": 14,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1266,
    "OrganizationBreakdownStructure": "NA-KC.OBSO.BM",
    "Name": "BUSINESS MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/10\/3\/1\/",
    "ParentId": 1265,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1267,
    "OrganizationBreakdownStructure": "NA-KC.OBSO.SIS",
    "Name": "SECURITY & INFORMATION SYSTEMS",
    "OrganizationHierarchyId": "\/1\/10\/3\/2\/",
    "ParentId": 1265,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1268,
    "OrganizationBreakdownStructure": "NA-SV.MA",
    "Name": "MISSION ASSURANCE",
    "OrganizationHierarchyId": "\/1\/15\/1\/",
    "ParentId": 19,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1269,
    "OrganizationBreakdownStructure": "NA-SV.CABM",
    "Name": "CONTRACT ADMINISTRATION AND BUSINESS MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/15\/2\/",
    "ParentId": 19,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1270,
    "OrganizationBreakdownStructure": "NA-SV.FP",
    "Name": "FACILITIES AND PROJECTS",
    "OrganizationHierarchyId": "\/1\/15\/3\/",
    "ParentId": 19,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1271,
    "OrganizationBreakdownStructure": "NA-NV.BCM",
    "Name": "BUSINESS AND CONTRACT MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/13\/1\/",
    "ParentId": 17,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1272,
    "OrganizationBreakdownStructure": "NA-NV.NS",
    "Name": "NATIONAL SECURITY",
    "OrganizationHierarchyId": "\/1\/13\/2\/",
    "ParentId": 17,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1273,
    "OrganizationBreakdownStructure": "NA-NV.SS",
    "Name": "SAFETY AND SECURITY",
    "OrganizationHierarchyId": "\/1\/13\/3\/",
    "ParentId": 17,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1274,
    "OrganizationBreakdownStructure": "NA-NV.SO",
    "Name": "SITE OPERATIONS",
    "OrganizationHierarchyId": "\/1\/13\/4\/",
    "ParentId": 17,
    "SortOrder": 4,
    "ActiveInd": true
}, {
    "PartyId": 1275,
    "OrganizationBreakdownStructure": "NA-NV.EM",
    "Name": "ENVIRONMENTAL MANAGEMENT",
    "OrganizationHierarchyId": "\/1\/13\/5\/",
    "ParentId": 17,
    "SortOrder": 5,
    "ActiveInd": true
}, {
    "PartyId": 1276,
    "OrganizationBreakdownStructure": "NA-NV.DOEEM",
    "Name": "DOE\/ENVIRONMENTAL MANAGEMENT (NNSA SMALL SITES)",
    "OrganizationHierarchyId": "\/1\/13\/6\/",
    "ParentId": 17,
    "SortOrder": 6,
    "ActiveInd": true
}, {
    "PartyId": 1342,
    "OrganizationBreakdownStructure": "NA-21.1 GTRI",
    "Name": "Office of North and South American Threat Reduction",
    "OrganizationHierarchyId": "\/2\/1\/",
    "ParentId": 21,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1343,
    "OrganizationBreakdownStructure": "NA-21.2 GTRI",
    "Name": "Office of European and African Threat Reduction",
    "OrganizationHierarchyId": "\/2\/2\/",
    "ParentId": 21,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1344,
    "OrganizationBreakdownStructure": "NA-21.3 GTRI",
    "Name": "Office of Former Soviet Union (FSU) and Asian Threat Reduction",
    "OrganizationHierarchyId": "\/2\/3\/",
    "ParentId": 21,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1393,
    "OrganizationBreakdownStructure": "NA-50",
    "Name": "Office of Safety, Infrastructure & Operations",
    "OrganizationHierarchyId": "\/91\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1394,
    "OrganizationBreakdownStructure": "NA-51",
    "Name": "Safety",
    "OrganizationHierarchyId": "\/91\/1\/",
    "ParentId": 1393,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1395,
    "OrganizationBreakdownStructure": "NA-511",
    "Name": "Office of the Chief of Defense Nuclear Safety",
    "OrganizationHierarchyId": "\/91\/1\/1\/",
    "ParentId": 1394,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1396,
    "OrganizationBreakdownStructure": "NA-512",
    "Name": "Office of Nuclear Safety Services",
    "OrganizationHierarchyId": "\/91\/1\/2\/",
    "ParentId": 1394,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1397,
    "OrganizationBreakdownStructure": "NA-513",
    "Name": "Office of Worker Safety & Health Services",
    "OrganizationHierarchyId": "\/91\/1\/3\/",
    "ParentId": 1394,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1398,
    "OrganizationBreakdownStructure": "NA-52",
    "Name": "Infrastructure",
    "OrganizationHierarchyId": "\/91\/2\/",
    "ParentId": 1393,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1399,
    "OrganizationBreakdownStructure": "NA-521",
    "Name": "Office of Infrastructure Planning & Analysis",
    "OrganizationHierarchyId": "\/91\/2\/1\/",
    "ParentId": 1398,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1400,
    "OrganizationBreakdownStructure": "NA-522",
    "Name": "Office of Infrastructure Operations and Modernization",
    "OrganizationHierarchyId": "\/91\/2\/2\/",
    "ParentId": 1398,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1401,
    "OrganizationBreakdownStructure": "NA-53",
    "Name": "Enterprise Stewardship",
    "OrganizationHierarchyId": "\/91\/3\/",
    "ParentId": 1393,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1402,
    "OrganizationBreakdownStructure": "NA-531",
    "Name": "Office of Packaging & Transportation",
    "OrganizationHierarchyId": "\/91\/3\/1\/",
    "ParentId": 1401,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1403,
    "OrganizationBreakdownStructure": "NA-532",
    "Name": "Office of Nuclear Materials Integration",
    "OrganizationHierarchyId": "\/91\/3\/2\/",
    "ParentId": 1401,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1404,
    "OrganizationBreakdownStructure": "NA-533",
    "Name": "Office of Environment & Sustainability",
    "OrganizationHierarchyId": "\/91\/3\/3\/",
    "ParentId": 1401,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1440,
    "OrganizationBreakdownStructure": "NA-21",
    "Name": "Office of Global Material Security",
    "OrganizationHierarchyId": "\/92\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1441,
    "OrganizationBreakdownStructure": "NA-21.2",
    "Name": "Office of Radiological Security",
    "OrganizationHierarchyId": "\/92\/1\/",
    "ParentId": 1440,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1442,
    "OrganizationBreakdownStructure": "NA-21.2.1",
    "Name": "Office of Radiological Security - Domestic",
    "OrganizationHierarchyId": "\/92\/1\/1\/",
    "ParentId": 1441,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1443,
    "OrganizationBreakdownStructure": "NA-21.2.2",
    "Name": "Office of Radiological Security - International",
    "OrganizationHierarchyId": "\/92\/1\/2\/",
    "ParentId": 1441,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1444,
    "OrganizationBreakdownStructure": "NA-23",
    "Name": "Office of Material Management and Minimization",
    "OrganizationHierarchyId": "\/93\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1445,
    "OrganizationBreakdownStructure": "NA-23.1",
    "Name": "Office of Convert",
    "OrganizationHierarchyId": "\/93\/1\/",
    "ParentId": 1444,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1446,
    "OrganizationBreakdownStructure": "NA-23.2",
    "Name": "Office of Remove",
    "OrganizationHierarchyId": "\/93\/2\/",
    "ParentId": 1444,
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1455,
    "OrganizationBreakdownStructure": "NA-23.3",
    "Name": "Office of Dispose",
    "OrganizationHierarchyId": "\/93\/3\/",
    "ParentId": 1444,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1574,
    "OrganizationBreakdownStructure": "NA-24",
    "Name": "Nonproliferation and Arms Control",
    "OrganizationHierarchyId": "\/142\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1575,
    "OrganizationBreakdownStructure": "NA-523",
    "Name": "Program Management Office (PMO)",
    "OrganizationHierarchyId": "\/91\/2\/3\/",
    "ParentId": 1398,
    "SortOrder": 3,
    "ActiveInd": true
}, {
    "PartyId": 1579,
    "OrganizationBreakdownStructure": "NA-80",
    "Name": "Office of Counterterrorism and Counterproliferation",
    "OrganizationHierarchyId": "\/145\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 14,
    "OrganizationBreakdownStructure": "NA-KC",
    "Name": "KANSAS CITY FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/10\/",
    "ParentId": 1,
    "SortOrder": 16,
    "ActiveInd": true
}, {
    "PartyId": 15,
    "OrganizationBreakdownStructure": "NA-LA",
    "Name": "LOS ALAMOS FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/11\/",
    "ParentId": 1,
    "SortOrder": 13,
    "ActiveInd": true
}, {
    "PartyId": 16,
    "OrganizationBreakdownStructure": "NA-LL",
    "Name": "LIVERMORE FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/12\/",
    "ParentId": 1,
    "SortOrder": 11,
    "ActiveInd": true
}, {
    "PartyId": 17,
    "OrganizationBreakdownStructure": "NA-NV",
    "Name": "NEVADA FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/13\/",
    "ParentId": 1,
    "SortOrder": 12,
    "ActiveInd": true
}, {
    "PartyId": 18,
    "OrganizationBreakdownStructure": "NA-NPO",
    "Name": "NNSA PRODUCTION OFFICE",
    "OrganizationHierarchyId": "\/1\/14\/",
    "ParentId": 1,
    "SortOrder": 15,
    "ActiveInd": true
}, {
    "PartyId": 19,
    "OrganizationBreakdownStructure": "NA-SV",
    "Name": "SAVANNAH RIVER FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/15\/",
    "ParentId": 1,
    "SortOrder": 17,
    "ActiveInd": true
}, {
    "PartyId": 20,
    "OrganizationBreakdownStructure": "NA-SN",
    "Name": "SANDIA FIELD OFFICE",
    "OrganizationHierarchyId": "\/1\/16\/",
    "ParentId": 1,
    "SortOrder": 14,
    "ActiveInd": true
}, {
    "PartyId": 1285,
    "OrganizationBreakdownStructure": "BP",
    "Name": "Bonneville Power Administration",
    "OrganizationHierarchyId": "\/18\/",
    "SortOrder": 18,
    "ActiveInd": true
}, {
    "PartyId": 1286,
    "OrganizationBreakdownStructure": "CC",
    "Name": "Consolidated Business Center",
    "OrganizationHierarchyId": "\/19\/",
    "SortOrder": 19,
    "ActiveInd": true
}, {
    "PartyId": 1287,
    "OrganizationBreakdownStructure": "CH",
    "Name": "Chicago Operations Office",
    "OrganizationHierarchyId": "\/20\/",
    "SortOrder": 20,
    "ActiveInd": true
}, {
    "PartyId": 1288,
    "OrganizationBreakdownStructure": "CR",
    "Name": "Chief Financial Office",
    "OrganizationHierarchyId": "\/21\/",
    "SortOrder": 21,
    "ActiveInd": true
}, {
    "PartyId": 1289,
    "OrganizationBreakdownStructure": "ER",
    "Name": "Federal Energy Regulatory Commission",
    "OrganizationHierarchyId": "\/22\/",
    "SortOrder": 22,
    "ActiveInd": true
}, {
    "PartyId": 1290,
    "OrganizationBreakdownStructure": "FT",
    "Name": "National Energy Technology Lab",
    "OrganizationHierarchyId": "\/23\/",
    "SortOrder": 23,
    "ActiveInd": true
}, {
    "PartyId": 1291,
    "OrganizationBreakdownStructure": "ID",
    "Name": "Idaho Operations Office",
    "OrganizationHierarchyId": "\/24\/",
    "SortOrder": 24,
    "ActiveInd": true
}, {
    "PartyId": 1292,
    "OrganizationBreakdownStructure": "NS",
    "Name": "NNSA Office of Field Financial Management",
    "OrganizationHierarchyId": "\/25\/",
    "SortOrder": 25,
    "ActiveInd": true
}, {
    "PartyId": 1293,
    "OrganizationBreakdownStructure": "OR",
    "Name": "Oak Ridge Operations Office",
    "OrganizationHierarchyId": "\/26\/",
    "SortOrder": 26,
    "ActiveInd": true
}, {
    "PartyId": 1295,
    "OrganizationBreakdownStructure": "PN",
    "Name": "Naval Reactors Lab Field Office",
    "OrganizationHierarchyId": "\/28\/",
    "SortOrder": 28,
    "ActiveInd": true
}, {
    "PartyId": 1297,
    "OrganizationBreakdownStructure": "RL",
    "Name": "Hanford Site",
    "OrganizationHierarchyId": "\/30\/",
    "SortOrder": 30,
    "ActiveInd": true
}, {
    "PartyId": 1298,
    "OrganizationBreakdownStructure": "RP",
    "Name": "Western Area Power Administration",
    "OrganizationHierarchyId": "\/31\/",
    "SortOrder": 31,
    "ActiveInd": true
}, {
    "PartyId": 1299,
    "OrganizationBreakdownStructure": "SE",
    "Name": "Southeastern Power Administration",
    "OrganizationHierarchyId": "\/32\/",
    "SortOrder": 32,
    "ActiveInd": true
}, {
    "PartyId": 1300,
    "OrganizationBreakdownStructure": "SP",
    "Name": "Strategic Petroleum Reserves Office",
    "OrganizationHierarchyId": "\/33\/",
    "SortOrder": 33,
    "ActiveInd": true
}, {
    "PartyId": 1301,
    "OrganizationBreakdownStructure": "SR",
    "Name": "Savannah River Operations Office",
    "OrganizationHierarchyId": "\/34\/",
    "SortOrder": 34,
    "ActiveInd": true
}, {
    "PartyId": 1303,
    "OrganizationBreakdownStructure": "SW",
    "Name": "Southwestern Power Administration",
    "OrganizationHierarchyId": "\/36\/",
    "SortOrder": 36,
    "ActiveInd": true
}, {
    "PartyId": 1305,
    "OrganizationBreakdownStructure": "HQ",
    "Name": "NNSA-HQ",
    "OrganizationHierarchyId": "\/38\/",
    "SortOrder": 38,
    "ActiveInd": true
}, {
    "PartyId": 1306,
    "OrganizationBreakdownStructure": "NS",
    "Name": "NNSA Albuquerque Office",
    "OrganizationHierarchyId": "\/39\/",
    "SortOrder": 39,
    "ActiveInd": true
}, {
    "PartyId": 1310,
    "OrganizationBreakdownStructure": "ANL",
    "Name": "Argonne National Laboratory",
    "OrganizationHierarchyId": "\/43\/",
    "SortOrder": 43,
    "ActiveInd": true
}, {
    "PartyId": 1311,
    "OrganizationBreakdownStructure": "BNL",
    "Name": "Brookhaven National Laboratory",
    "OrganizationHierarchyId": "\/44\/",
    "SortOrder": 44,
    "ActiveInd": true
}, {
    "PartyId": 1312,
    "OrganizationBreakdownStructure": "CBC",
    "Name": "Consolidated Business Center",
    "OrganizationHierarchyId": "\/45\/",
    "SortOrder": 45,
    "ActiveInd": true
}, {
    "PartyId": 1315,
    "OrganizationBreakdownStructure": "HQ-E",
    "Name": "NNSA-HQ Execution",
    "OrganizationHierarchyId": "\/48\/",
    "SortOrder": 48,
    "ActiveInd": true
}, {
    "PartyId": 1316,
    "OrganizationBreakdownStructure": "HQ-H",
    "Name": "NNSA-HQ Hold",
    "OrganizationHierarchyId": "\/49\/",
    "SortOrder": 49,
    "ActiveInd": true
}, {
    "PartyId": 1318,
    "OrganizationBreakdownStructure": "INL",
    "Name": "Idaho National Laboratory",
    "OrganizationHierarchyId": "\/51\/",
    "SortOrder": 51,
    "ActiveInd": true
}, {
    "PartyId": 1319,
    "OrganizationBreakdownStructure": "KC",
    "Name": "Kansas City National Security Campus",
    "OrganizationHierarchyId": "\/52\/",
    "SortOrder": 52,
    "ActiveInd": true
}, {
    "PartyId": 1320,
    "OrganizationBreakdownStructure": "LANL",
    "Name": "Los Alamos National Laboratory",
    "OrganizationHierarchyId": "\/53\/",
    "SortOrder": 53,
    "ActiveInd": true
}, {
    "PartyId": 1321,
    "OrganizationBreakdownStructure": "LLNL",
    "Name": "Lawrence Livermore National Laboratory",
    "OrganizationHierarchyId": "\/54\/",
    "SortOrder": 54,
    "ActiveInd": true
}, {
    "PartyId": 1324,
    "OrganizationBreakdownStructure": "NNSS",
    "Name": "Nevada National Security Site",
    "OrganizationHierarchyId": "\/57\/",
    "SortOrder": 57,
    "ActiveInd": true
}, {
    "PartyId": 1326,
    "OrganizationBreakdownStructure": "NS-E",
    "Name": "NNSA Albuquerque Office Execution",
    "OrganizationHierarchyId": "\/59\/",
    "SortOrder": 59,
    "ActiveInd": true
}, {
    "PartyId": 1327,
    "OrganizationBreakdownStructure": "NS-H",
    "Name": "NNSA Albuquerque Office Hold",
    "OrganizationHierarchyId": "\/60\/",
    "SortOrder": 60,
    "ActiveInd": true
}, {
    "PartyId": 1332,
    "OrganizationBreakdownStructure": "ORNL",
    "Name": "Oak Ridge National Laboratory",
    "OrganizationHierarchyId": "\/65\/",
    "SortOrder": 65,
    "ActiveInd": true
}, {
    "PartyId": 1334,
    "OrganizationBreakdownStructure": "PX",
    "Name": "Pantex Plant",
    "OrganizationHierarchyId": "\/67\/",
    "SortOrder": 67,
    "ActiveInd": true
}, {
    "PartyId": 1335,
    "OrganizationBreakdownStructure": "PNNL",
    "Name": "Pacific Northwest National Laboratory",
    "OrganizationHierarchyId": "\/68\/",
    "SortOrder": 68,
    "ActiveInd": true
}, {
    "PartyId": 1336,
    "OrganizationBreakdownStructure": "SNL",
    "Name": "Sandia National Laboratory",
    "OrganizationHierarchyId": "\/69\/",
    "SortOrder": 69,
    "ActiveInd": true
}, {
    "PartyId": 1338,
    "OrganizationBreakdownStructure": "SRNS",
    "Name": "Savannah River Nuclear Solutions",
    "OrganizationHierarchyId": "\/71\/",
    "SortOrder": 71,
    "ActiveInd": true
}, {
    "PartyId": 1340,
    "OrganizationBreakdownStructure": "Y-12",
    "Name": "Y-12 National Security Complex",
    "OrganizationHierarchyId": "\/73\/",
    "SortOrder": 73,
    "ActiveInd": true
}, {
    "PartyId": 1365,
    "OrganizationBreakdownStructure": "NETL",
    "Name": "National Energy Technology Laboratory",
    "OrganizationHierarchyId": "\/83\/",
    "SortOrder": 99999,
    "ActiveInd": true
}, {
    "PartyId": 1367,
    "OrganizationBreakdownStructure": "WIPP",
    "Name": "Waste Isolation Pilot Plant",
    "OrganizationHierarchyId": "\/85\/",
    "SortOrder": 99999,
    "ActiveInd": true
}, {
    "PartyId": 1382,
    "OrganizationBreakdownStructure": "BMPC",
    "Name": "Bechtel Marine Propulsion Corporations",
    "OrganizationHierarchyId": "\/86\/",
    "SortOrder": 99999,
    "ActiveInd": true
}, {
    "PartyId": 1385,
    "OrganizationBreakdownStructure": "LBNL",
    "Name": "Lawrence Berkley National Laboratory",
    "OrganizationHierarchyId": "\/89\/",
    "SortOrder": 99999,
    "ActiveInd": true
}, {
    "PartyId": 1387,
    "OrganizationBreakdownStructure": "Unassigned",
    "Name": "Unassigned",
    "OrganizationHierarchyId": "\/90\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1393,
    "OrganizationBreakdownStructure": "NA-50",
    "Name": "Office of Safety, Infrastructure & Operations",
    "OrganizationHierarchyId": "\/91\/",
    "SortOrder": 1,
    "ActiveInd": true
}, {
    "PartyId": 1398,
    "OrganizationBreakdownStructure": "NA-52",
    "Name": "Infrastructure",
    "OrganizationHierarchyId": "\/91\/2\/",
    "ParentId": 1393,
    "SortOrder": 2,
    "ActiveInd": true
}, {
    "PartyId": 1457,
    "OrganizationBreakdownStructure": "ORISE",
    "Name": "Oak Ridge Institute for Science and Education",
    "OrganizationHierarchyId": "\/94\/",
    "SortOrder": 99,
    "ActiveInd": true
}, {
    "PartyId": 1464,
    "OrganizationBreakdownStructure": "NRLFO",
    "Name": "Naval Reactors Laboratory Field Office",
    "OrganizationHierarchyId": "\/100\/",
    "SortOrder": 99,
    "ActiveInd": true
}, {
    "PartyId": 1505,
    "OrganizationBreakdownStructure": "OST",
    "Name": "Office of Secure Transportation",
    "OrganizationHierarchyId": "\/102\/",
    "SortOrder": 99,
    "ActiveInd": true
}, {
    "PartyId": 1511,
    "OrganizationBreakdownStructure": "ABQ",
    "Name": "Albuquerque Complex",
    "OrganizationHierarchyId": "\/103\/",
    "SortOrder": 999,
    "ActiveInd": true
}]', @reops NVARCHAR(MAX) = '[{
    "OrganizationPartyId": 14,
    "FormalCode": "NA-KC",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 15,
    "FormalCode": "NA-LA",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 16,
    "FormalCode": "NA-LL",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 17,
    "FormalCode": "NA-NV",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 18,
    "FormalCode": "NA-NPO",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 19,
    "FormalCode": "NA-SV",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 20,
    "FormalCode": "NA-SN",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1297,
    "FormalCode": "RL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1305,
    "FormalCode": "HQ",
    "CostInd": false,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1310,
    "FormalCode": "ANL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1311,
    "FormalCode": "BNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1312,
    "FormalCode": "CBC",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1315,
    "FormalCode": "HQE",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1316,
    "FormalCode": "HQH",
    "DefaultFundingClassificationId": 1,
    "CostInd": false,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1318,
    "FormalCode": "INL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1319,
    "FormalCode": "KC",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1320,
    "FormalCode": "LANL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1321,
    "FormalCode": "LLNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1324,
    "FormalCode": "NNSS",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1326,
    "FormalCode": "NS-E",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1327,
    "FormalCode": "NS-H",
    "DefaultFundingClassificationId": 1,
    "CostInd": false,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1332,
    "FormalCode": "ORNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1334,
    "FormalCode": "PX",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1335,
    "FormalCode": "PNNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1336,
    "FormalCode": "SNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1338,
    "FormalCode": "SRNS",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1340,
    "FormalCode": "Y-12",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1365,
    "FormalCode": "NETL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1367,
    "FormalCode": "WIPP",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1382,
    "FormalCode": "BMPC",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1385,
    "FormalCode": "LBNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1387,
    "FormalCode": "Unassigned",
    "CostInd": false,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1457,
    "FormalCode": "ORISE",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1464,
    "FormalCode": "NRLFO",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1505,
    "FormalCode": "OST",
    "DefaultFundingClassificationId": 1,
    "CostInd": false,
    "UnobligatedInd": true
}]';

	SET IDENTITY_INSERT Contact.Party ON;

	INSERT INTO Contact.Party (Id, DeletedInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.DeletedInd, @userId, @userId, @userId, @userId
	FROM OPENJSON(@party) WITH (Id INT '$.Id', DeletedInd BIT '$.DeletedInd') oj
	EXCEPT
	SELECT pty.Id, pty.DeletedInd, @userId, @userId, @userId, @userId
	FROM Contact.Party pty;

	SET IDENTITY_INSERT Contact.Party OFF;

	INSERT INTO Contact.PartyRole (SubSystemId, PartyId, PartyRoleTypeId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT 1, oj.Id, prt.prtId, @userId, @userId, @userId, @userId
	FROM OPENJSON(@party) WITH (Id INT '$.Id', pr NVARCHAR(MAX) '$.pr' AS JSON) oj
	CROSS APPLY OPENJSON(oj.pr) WITH (prtId INT '$.prtId') prt
	EXCEPT
	SELECT pr.SubSystemId, pr.PartyId, pr.PartyRoleTypeId, @userId, @userId, @userId, @userId
	FROM Contact.PartyRole pr;

	INSERT INTO Contact.Organization (PartyId, OrganizationBreakdownStructure, Name, OrganizationHierarchyId, ParentId, SortOrder, ActiveInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.PartyId, oj.OrganizationBreakdownStructure, oj.Name, oj.OrganizationHierarchyId, oj.ParentId, oj.SortOrder, oj.ActiveInd, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@orgs) WITH (PartyId INT '$.PartyId', OrganizationBreakdownStructure VARCHAR(50) '$.OrganizationBreakdownStructure', Name VARCHAR(100) '$.Name', OrganizationHierarchyId VARCHAR(2000) '$.OrganizationHierarchyId', ParentId INT '$.ParentId', SortOrder INT '$.SortOrder', ActiveInd BIT '$.ActiveInd') oj
	LEFT OUTER JOIN Contact.Organization o ON o.ParentId = oj.ParentId
	WHERE o.PartyId IS NULL
	EXCEPT 
	SELECT oj.PartyId, oj.OrganizationBreakdownStructure, oj.Name, oj.OrganizationHierarchyId, oj.ParentId, oj.SortOrder, oj.ActiveInd, @userId, @userId, @userId, @userId 
	FROM Contact.Organization oj;
		
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH