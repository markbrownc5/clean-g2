BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @crdss NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Active",
    "Description": "Statuses that are currently active."
}, {
    "Id": 2,
    "Name": "Final",
    "Description": "Statuses that are not active."
}, {
    "Id": 3,
    "Name": "Processed",
    "Description": "Statuses that have reached the final state and are processed into the system."
}, {
    "Id": 4,
    "Name": "Recallable",
    "Description": "Statuses that have the potential of being recalled to a prior workflow level."
}, {
    "Id": 5,
    "Name": "Ignore",
    "Description": "Statuses that shouldn''t be included in active\/processed queries."
}, {
    "Id": 6,
    "Name": "Rejected",
    "Description": "Statuses that have been rejected."
}]';

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatusState ON;

	INSERT INTO G2.ChangeRequestDetailStatusState (Id, Name, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT crdss.Id, crdss.Name, crdss.Description, @userId, @userId, @userId, @userId 
	FROM OPENJSON(@crdss) WITH (Id INT '$.Id', Name VARCHAR(25) '$.Name', Description VARCHAR(100) '$.Description') crdss
	EXCEPT
	SELECT crdss.Id, crdss.Name, crdss.Description, @userId, @userId, @userId, @userId 
	FROM G2.ChangeRequestDetailStatusState crdss

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatusState OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH