
BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "OrganizationPartyId": 1365,
    "FormalCode": "NETL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1367,
    "FormalCode": "WIPP",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 14,
    "FormalCode": "NA-KC",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 15,
    "FormalCode": "NA-LA",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 16,
    "FormalCode": "NA-LL",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 17,
    "FormalCode": "NA-NV",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 18,
    "FormalCode": "NA-NPO",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 19,
    "FormalCode": "NA-SV",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 20,
    "FormalCode": "NA-SN",
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1310,
    "FormalCode": "ANL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1311,
    "FormalCode": "BNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1312,
    "FormalCode": "CBC",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1315,
    "FormalCode": "HQE",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1318,
    "FormalCode": "INL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1319,
    "FormalCode": "KC",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1320,
    "FormalCode": "LANL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1321,
    "FormalCode": "LLNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1324,
    "FormalCode": "NNSS",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1326,
    "FormalCode": "NS-E",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1332,
    "FormalCode": "ORNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1334,
    "FormalCode": "PX",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1335,
    "FormalCode": "PNNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1336,
    "FormalCode": "SNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1338,
    "FormalCode": "SRNS",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1340,
    "FormalCode": "Y-12",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1382,
    "FormalCode": "BMPC",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1385,
    "FormalCode": "LBNL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1297,
    "FormalCode": "RL",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1316,
    "FormalCode": "HQH",
    "DefaultFundingClassificationId": 1,
    "CostInd": false,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1457,
    "FormalCode": "ORISE",
    "DefaultFundingClassificationId": 2,
    "CostInd": true,
    "UnobligatedInd": false
}, {
    "OrganizationPartyId": 1464,
    "FormalCode": "NRLFO",
    "DefaultFundingClassificationId": 1,
    "CostInd": true,
    "UnobligatedInd": true
}, {
    "OrganizationPartyId": 1327,
    "FormalCode": "NS-H",
    "DefaultFundingClassificationId": 1,
    "CostInd": false,
    "UnobligatedInd": true
}]';
	
	INSERT INTO Contact.ReportingEntity (OrganizationPartyId, FormalCode, DefaultFundingClassificationId, CostInd, UnobligatedInd, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.OrganizationPartyId, oj.FormalCode, oj.DefaultFundingClassificationId, oj.CostInd, oj.UnobligatedInd, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (OrganizationPartyId INT '$.OrganizationPartyId', FormalCode NVARCHAR(50) '$.FormalCode', DefaultFundingClassificationId INT '$.DefaultFundingClassificationId', CostInd BIT '$.CostInd', UnobligatedInd BIT '$.UnobligatedInd') oj
	EXCEPT
    SELECT re.OrganizationPartyId,
		   re.FormalCode,
		   re.DefaultFundingClassificationId,
		   re.CostInd,
		   re.UnobligatedInd, @user, @user, @user, @user
	FROM Contact.ReportingEntity re
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH