BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	DECLARE @user INT = 614
	DECLARE @fundingTransactionTypeId INT = Financial.fnGetFundingTransactionTypeId(1), @per INT = (SELECT per.Id FROM G2.Period per WHERE per.FiscalYear = 2019 AND per.FiscalPeriod = 1);
	DECLARE @perStartDt DATETIME = (SELECT per.BeginDt FROM G2.Period per WHERE per.Id = @per)

	INSERT INTO Financial.Account (ReportingEntityOrganizationPartyId, WorkBreakdownStructureId, FundTypeId, BNRId, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT re.OrganizationPartyId, wbs.id, ft.id, b.Id, @user, @user, @user, @user
	FROM G2.WorkBreakdownStructure wbs
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = wbs.SubSystemLevelId
	CROSS JOIN Financial.FundType ft
	CROSS JOIN Contact.ReportingEntity re
	CROSS JOIN Financial.BNR b 
	INNER JOIN Financial.SubSystemFundTypeBNRXref ssftbx ON wbs.SubSystemId = ssftbx.SubSystemId AND ssftbx.FundTypeId = ft.Id AND ssftbx.BNRId = b.Id
	WHERE ssl.Label = 'Project' 
	AND wbs.Name = 'Construction'
	AND ft.ShortName = 'TC'
	AND re.FormalCode = 'ORNL'
	AND b.ShortName = 'DP0920100'
	EXCEPT
	SELECT a.ReportingEntityOrganizationPartyId,
		   a.WorkBreakdownStructureId,
		   a.FundTypeId,
		   a.BNRId,
		   @user, @user, @user, @user
	FROM Financial.Account a;

	INSERT INTO Financial.AccountTransaction (AccountId, PeriodId, TransactionTypeId, TransactionDt, EffectiveDt, Amount, Note, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT a.Id, @per, @fundingTransactionTypeId, @perStartDt, @perStartDt, 50000.00, NULL, @user, @user, @user, @user	 
	FROM Financial.Account a
	WHERE a.Id = 1
	EXCEPT
	SELECT at.AccountId,
		   at.PeriodId,
		   at.TransactionTypeId,
		   at.TransactionDt,
		   at.EffectiveDt,
		   at.Amount,
		   at.Note,
		   @user, @user, @user, @user	
	FROM Financial.AccountTransaction at;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH