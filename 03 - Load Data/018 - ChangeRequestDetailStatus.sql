BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @crs NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "Not Applicable",
    "Description": "Not Applicable, nothing to load."
}, {
    "Id": 2,
    "Name": "Needs Attention",
    "Description": "New Item in persons work queue that has not been worked on by them yet"
}, {
    "Id": 3,
    "Name": "In Process",
    "Description": "Item in a persons work queue that they have made changes to, but not completed."
}, {
    "Id": 4,
    "Name": "Approved",
    "Description": "Change Request Approved but not loaded into G2 main tables yet."
}, {
    "Id": 5,
    "Name": "Rejected",
    "Description": "Rejected, will not be loaded into G2."
}, {
    "Id": 6,
    "Name": "Processed",
    "Description": "Change request loaded into the G2 main tables."
}, {
    "Id": 7,
    "Name": "Submitted",
    "Description": "All parts submitted and ready for advancement"
}, {
    "Id": 8,
    "Name": "Voided",
    "Description": "Voided, will not be loaded into G2."
}, {
    "Id": 9,
    "Name": "Passed Back",
    "Description": "Change request passed back for revisions."
}, {
    "Id": 10,
    "Name": "Deleted",
    "Description": "Change request deleted."
}, {
    "Id": 11,
    "Name": "Insufficient Funds",
    "Description": "Change request has insufficient funds."
}, {
    "Id": 12,
    "Name": "Finalized",
    "Description": "Change request has been finalized."
}, {
    "Id": 13,
    "Name": "Not Providing",
    "Description": "Data will not be provided for this Change Request"
}]';

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatus ON;

	INSERT INTO G2.ChangeRequestDetailStatus (Id, Name, Description)
	SELECT c.Id, c.Name, c.Description
	FROM OPENJSON(@crs) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', Description NVARCHAR(255) '$.Description') c
	EXCEPT 
	SELECT crds.Id, crds.Name, crds.Description
	FROM G2.ChangeRequestDetailStatus crds

	SET IDENTITY_INSERT G2.ChangeRequestDetailStatus OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH