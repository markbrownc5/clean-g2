BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 87,
    "SubSystemId": 1,
    "ParmKey": "Current FY",
    "Value": "2019",
    "Datatype": "int",
    "Description": "Current Fiscal Year"
}, {
    "Id": 88,
    "SubSystemId": 1,
    "ParmKey": "HQ AFP Creation Status",
    "Value": "Closed",
    "Datatype": "nvarchar",
    "Description": "Whether HQ AFP creation is \"Open\" or \"Closed\""
}, {
    "Id": 89,
    "SubSystemId": 1,
    "ParmKey": "Current Funding Thru Period",
    "Value": "291",
    "Datatype": "int",
    "Description": "Latest period for which finplan changes were processed"
}, {
    "Id": 90,
    "SubSystemId": 1,
    "ParmKey": "Current HQ AFP Period",
    "Value": "292",
    "Datatype": "int",
    "Description": "Current HQ AFP Period"
}, {
    "Id": 92,
    "SubSystemId": 1,
    "ParmKey": "Current NFWA Period",
    "Value": "292",
    "Datatype": "int",
    "Description": "The earliest period for which a NFWA can be created"
}, {
    "Id": 95,
    "SubSystemId": 1,
    "ParmKey": "Minimum HQ AFP Period",
    "Value": "291",
    "Datatype": "int",
    "Description": "The minimum allowed period for which HQ AFPs can be processed."
}, {
    "Id": 97,
    "SubSystemId": 1,
    "ParmKey": "Minimum NFWA Period",
    "Value": "292",
    "Datatype": "int",
    "Description": "The minimum allowed period for which NFWAs can be processed."
}, {
    "Id": 98,
    "SubSystemId": 1,
    "ParmKey": "NFWA Creation Status",
    "Value": "Closed",
    "Datatype": "nvarchar",
    "Description": "Whether NFWA creation is \"Open\" or \"Closed\""
}, {
    "Id": 99,
    "SubSystemId": 1,
    "ParmKey": "NFWA Period Advancement Day",
    "Value": "1",
    "Datatype": "int",
    "Description": "Day on which the NFWA period should be advanced."
}, {
    "Id": 102,
    "SubSystemId": 1,
    "ParmKey": "Cost Period Advancement Day",
    "Value": "1",
    "Datatype": "int",
    "Description": "Day on which the cost periods should attempted to be advanced."
}, {
    "Id": 103,
    "SubSystemId": 1,
    "ParmKey": "Cost Submission Late Day",
    "Value": "11",
    "Datatype": "int",
    "Description": "Costs are late on the first weekday on or after this date of the month."
}, {
    "Id": 104,
    "SubSystemId": 1,
    "ParmKey": "Current Cost Period",
    "Value": "291",
    "Datatype": "int",
    "Description": "Current period for entering costs"
}, {
    "Id": 105,
    "SubSystemId": 1,
    "ParmKey": "Current Cost Thru Period",
    "Value": "291",
    "Datatype": "int",
    "Description": "Latest period for which costs have been processed"
}, {
    "Id": 107,
    "SubSystemId": 1,
    "ParmKey": "Annual Planning Fiscal Year",
    "Value": "2019",
    "Datatype": "int",
    "Description": "Fiscal year for annual planning"
}, {
    "Id": 108,
    "SubSystemId": 1,
    "ParmKey": "Baseline Creation Status",
    "Value": "Open",
    "Datatype": "nvarchar",
    "Description": "Whether baseline change request creation in \"Open\" or \"Closed\""
}, {
    "Id": 109,
    "SubSystemId": 1,
    "ParmKey": "Baseline Period Advancement Day",
    "Value": "1",
    "Datatype": "int",
    "Description": "Day on which the baseline periods should be advanced."
}, {
    "Id": 110,
    "SubSystemId": 1,
    "ParmKey": "Current Baseline Period",
    "Value": "292",
    "Datatype": "int",
    "Description": "Current period for baseline changes"
}, {
    "Id": 111,
    "SubSystemId": 1,
    "ParmKey": "Current Baseline Thru Period",
    "Value": "291",
    "Datatype": "int",
    "Description": "Latest period for which baseline changes were processed"
}, {
    "Id": 112,
    "SubSystemId": 1,
    "ParmKey": "Current Performance Period",
    "Value": "293",
    "Datatype": "int",
    "Description": "Current Status Period"
}, {
    "Id": 113,
    "SubSystemId": 1,
    "ParmKey": "Current Performance Thru Period",
    "Value": "292",
    "Datatype": "int",
    "Description": "Latest period for which performance reports were processed"
}, {
    "Id": 114,
    "SubSystemId": 1,
    "ParmKey": "Performance Allowed Joule Completion Cutoff",
    "Value": "10",
    "Datatype": "int",
    "Description": "The number of business days until the current minimum joule completion date for protect projects is updated."
}, {
    "Id": 115,
    "SubSystemId": 1,
    "ParmKey": "Performance Minimum Allowed Joule Completion Date",
    "Value": "Nov  1 2018 12:00AM",
    "Datatype": "datetime",
    "Description": "The minimum allowed joule completion date."
}, {
    "Id": 116,
    "SubSystemId": 1,
    "ParmKey": "Performer Distribution Import Status",
    "Value": "Closed",
    "Datatype": "nvarchar",
    "Description": "Whether the Performer Distribution import is \"Open\" or \"Closed\""
}, {
    "Id": 117,
    "SubSystemId": 1,
    "ParmKey": "ScheduleVarianceMilestoneWeightLimit",
    "Value": "20",
    "Datatype": "int",
    "Description": "Schedule Variance Milestone Weight Limit"
}, {
    "Id": 123,
    "SubSystemId": 1,
    "ParmKey": "Minimum Funding Adjustment Period",
    "Value": "1041",
    "Datatype": "int",
    "Description": "The minimum allowed period for which funding adjustments can be processed."
}, {
    "Id": 222,
    "SubSystemId": 1,
    "ParmKey": "Minimum Appropriation Start FY",
    "Value": "2014",
    "Datatype": "int",
    "Description": "The minimum fiscal year in which a B&R can start."
}, {
    "Id": 231,
    "SubSystemId": 1,
    "ParmKey": "Budget Formulation Process Status",
    "Value": "Closed",
    "Datatype": "nvarchar",
    "Description": "Whether the budget formulation process is \"Open\" or \"Closed\""
}, {
    "Id": 232,
    "SubSystemId": 1,
    "ParmKey": "Budget Formulation Planning Year",
    "Value": "2020",
    "Datatype": "int",
    "Description": "Fiscal year for budget formulation planning; the FY that planning should begin in"
}, {
    "Id": 240,
    "SubSystemId": 1,
    "ParmKey": "Security Label",
    "Value": "Protect as UCNI\/OUO Pending Review",
    "Datatype": "nvarchar",
    "Description": "Text to display of UCNI\/OUO information that is pending review."
}, {
    "Id": 256,
    "SubSystemId": 1,
    "ParmKey": "Performance Allowed Joule Completion Cutoff Type",
    "Value": "calendar",
    "Datatype": "nvarchar",
    "Description": "Valid values are ''calendar'' and ''working''.  This field determines whether we advance the performance period based on business days or calendar days."
}, {
    "Id": 264,
    "SubSystemId": 1,
    "ParmKey": "Default Commitment Transaction Type Id",
    "Value": "2",
    "Datatype": "int",
    "Description": "The default commitment transaction type to be used in STARS contract mappings"
}, {
    "Id": 265,
    "SubSystemId": 1,
    "ParmKey": "Default Cost Transaction Type Id",
    "Value": "28",
    "Datatype": "int",
    "Description": "The default cost transaction type to be used in STARS contract mappings"
}, {
    "Id": 296,
    "SubSystemId": 1,
    "ParmKey": "Minimum Financial CR Report Parameter Start Date",
    "Value": "2014-12-01 00:00:00.000",
    "Datatype": "datetime",
    "Description": "The effective minimum start date for the change request reports to use to limit the FY and FP parameter drop down lists."
}]';

	INSERT INTO G2.G2Parameters (SubSystemId, ParmKey, Value, Datatype, Description, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.SubSystemId, oj.ParmKey, oj.Value, oj.Datatype, oj.Description, @user, @user, @user, @user
	FROM OPENJSON(@json) WITH (Id INT '$.Id', SubSystemId INT '$.SubSystemId', ParmKey NVARCHAR(50) '$.ParmKey', Value NVARCHAR(200) '$.Value', Datatype NVARCHAR(50) '$.Datatype', Description NVARCHAR(256) '$.Description') oj
	EXCEPT
	SELECT gp.SubSystemId,
		   gp.ParmKey,
		   gp.Value,
		   gp.Datatype,
		   gp.Description,
		   @user, @user, @user, @user
	FROM G2.G2Parameters gp
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH