BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @user INT = 614, @json NVARCHAR(MAX) = '[{
    "Id": 1,
    "DisplayName": "DP0901000",
    "Description": "Parent- CCL - DP0901000 - Operations of Facilities",
    "STARSCode": "C000902"
}, {
    "Id": 2,
    "DisplayName": "DP0902000",
    "Description": "Parent- CCL - DP0902000 - Program Readiness",
    "STARSCode": "C000903"
}, {
    "Id": 3,
    "DisplayName": "DP1000000",
    "Description": "Parent- CCL - DP1000000 - Facilities and Infrastructure Recapitalization Program",
    "STARSCode": "C000904"
}, {
    "Id": 4,
    "DisplayName": "NN4000000",
    "Description": "Parent- CCL - NN4000000 - Nonproliferation and International Security",
    "STARSCode": "C001056"
}, {
    "Id": 5,
    "DisplayName": "NN5000000",
    "Description": "Parent- CCL - NN5000000 - International Nuclear Materials Protection and Cooperation",
    "STARSCode": "C001058"
}, {
    "Id": 6,
    "DisplayName": "PS0000000",
    "Description": "Parent- CCL - PS0000000 - Program Direction - National Nuclear Security Administration - Office of the Administrator",
    "STARSCode": "C001075"
}, {
    "Id": 7,
    "DisplayName": "DP0905000",
    "Description": "Parent- CCL - DP0905000 - Containers",
    "STARSCode": "C001136"
}, {
    "Id": 8,
    "DisplayName": "NN9000000",
    "Description": "Parent - CCL - NN9000000 - Global Threat Reduction Initiative",
    "STARSCode": "C002080"
}, {
    "Id": 9,
    "DisplayName": "DP5000000",
    "Description": "Parent - CCL - DP5000000 - Environmental Projects and Operations",
    "STARSCode": "C002384"
}, {
    "Id": 10,
    "DisplayName": "DP0901370",
    "Description": "Parent - CCL - DP0901370, Operations of Facilities - Kansas City Plant",
    "STARSCode": "C002492"
}, {
    "Id": 11,
    "DisplayName": "DP0901380",
    "Description": "Parent - CCL - DP0901380, Operations of Facilities - Lawrence Livermore National Laboratory",
    "STARSCode": "C002493"
}, {
    "Id": 12,
    "DisplayName": "DP0901390",
    "Description": "Parent - CCL - DP0901390, Operations of Facilities - Los Alamos National Laboratory",
    "STARSCode": "C002494"
}, {
    "Id": 13,
    "DisplayName": "DP0901400",
    "Description": "Parent - CCL - DP0901400, Operations of Facilities - Nevada Test Site",
    "STARSCode": "C002495"
}, {
    "Id": 14,
    "DisplayName": "DP0901410",
    "Description": "Parent - CCL - DP0901410, Operations of Facilities - Pantex Plant",
    "STARSCode": "C002496"
}, {
    "Id": 15,
    "DisplayName": "DP0901420",
    "Description": "Parent - CCL - DP0901420, Operations of Facilities - Sandia National Laboratories",
    "STARSCode": "C002497"
}, {
    "Id": 16,
    "DisplayName": "DP0901430",
    "Description": "Parent - CCL - DP0901430, Operations of Facilities - Savannah River Site",
    "STARSCode": "C002498"
}, {
    "Id": 17,
    "DisplayName": "DP0901440",
    "Description": "Parent - CCL - DP0901440, Operations of Facilities - Y-12 National Security Complex",
    "STARSCode": "C002499"
}, {
    "Id": 18,
    "DisplayName": "DP0901450",
    "Description": "Parent - CCL - DP0901450 - Operations of Facilities - Institutional Site Support",
    "STARSCode": "C002500"
}, {
    "Id": 19,
    "DisplayName": "DP5501000",
    "Description": "Parent - CCL - DP5501000, Environmental Projects and Operations",
    "STARSCode": "C002836"
}, {
    "Id": 20,
    "DisplayName": "DP5503000",
    "Description": "Parent - CCL - DP5503000, Energy Modernization and Investment",
    "STARSCode": "C002838"
}, {
    "Id": 21,
    "DisplayName": "NN9300000",
    "Description": "Parent - CCL - NN9300000, Domestic Radiological Material Removal and Protection",
    "STARSCode": "C002936"
}, {
    "Id": 22,
    "DisplayName": "NN9200000",
    "Description": "Parent - CCL - NN9200000, International Nuclear and Radiological Material Removal and Protection",
    "STARSCode": "C002937"
}, {
    "Id": 23,
    "DisplayName": "NN9100000",
    "Description": "Parent - CCL - NN9100000, Highly Enriched Uranium (HEU) Reactor Conversion",
    "STARSCode": "C002938"
}, {
    "Id": 24,
    "DisplayName": "DP0907000",
    "Description": "Parent - CCL - DP0907000, Maintenance and Repair of Facilities",
    "STARSCode": "C002939"
}, {
    "Id": 25,
    "DisplayName": "DP0908000",
    "Description": "Parent - CCL - DP0908000, Recapitalization",
    "STARSCode": "C002940"
}, {
    "Id": 26,
    "DisplayName": "EY0801100",
    "STARSCode": "NOTREAL01"
}, {
    "Id": 27,
    "DisplayName": "EZ0801100",
    "STARSCode": "NOTREAL02"
}, {
    "Id": 28,
    "DisplayName": "NN8000000",
    "Description": "Parent- CCL - NN8000000",
    "STARSCode": "NOTREAL03"
}, {
    "Id": 29,
    "DisplayName": "NNNA25GRR",
    "STARSCode": "NOTREAL04"
}, {
    "Id": 30,
    "DisplayName": "DP0920",
    "Description": "Production Facilities Construction",
    "STARSCode": "C002995"
}, {
    "Id": 31,
    "DisplayName": "NN6000000",
    "Description": "NN6000000 - U.S. Surplus Fissile Materials Disposition",
    "STARSCode": "C001059"
}, {
    "Id": 32,
    "DisplayName": "NN6100000",
    "Description": "NN6100000 - Russian Surplus Fissile Materials Disposition",
    "STARSCode": "C001063"
}, {
    "Id": 33,
    "DisplayName": "DP5502000",
    "Description": "Nuclear Materials Integration",
    "STARSCode": "2222382"
}, {
    "Id": 34,
    "DisplayName": "DN3001000",
    "Description": "Conversion",
    "STARSCode": "C003039"
}, {
    "Id": 35,
    "DisplayName": "DN3002000",
    "Description": "Nuclear Material Removal",
    "STARSCode": "C003040"
}, {
    "Id": 36,
    "DisplayName": "DN3003000",
    "Description": "Material Disposition",
    "STARSCode": "C003041"
}, {
    "Id": 37,
    "DisplayName": "DP0909000",
    "Description": "Parent - CCL - DP0909000, Safety & Environmental Operations",
    "STARSCode": "C002955"
}, {
    "Id": 38,
    "DisplayName": "DN1000000",
    "Description": "Parent- CCL - Global Material Security",
    "STARSCode": "BogusDN1"
}, {
    "Id": 39,
    "DisplayName": "MB0505000",
    "Description": "Parent - CCL - MB0505000, Strategic Materials Sustainment",
    "STARSCode": "C003055"
}, {
    "Id": 40,
    "DisplayName": "DN1001000",
    "Description": "Parent- CCL - INS",
    "STARSCode": "BogusDN1001"
}, {
    "Id": 41,
    "DisplayName": "DN1002000",
    "Description": "Parent- CCL - ORS",
    "STARSCode": "BogusDN1002"
}, {
    "Id": 42,
    "DisplayName": "DN1003000",
    "Description": "Parent- CCL - NSDD",
    "STARSCode": "BogusDN1003"
}, {
    "Id": 43,
    "DisplayName": "DN1004000",
    "Description": "Parent- CCL - International Contributions",
    "STARSCode": "BogusDN1004"
}, {
    "Id": 44,
    "DisplayName": "DP0910000",
    "Description": "Operations of Facilities",
    "STARSCode": "C003090"
}, {
    "Id": 45,
    "DisplayName": "NN3100000",
    "Description": "Parent- CCL - NN3100000 - HEU Transparency Implementation",
    "STARSCode": "C001054"
}, {
    "Id": 46,
    "DisplayName": "NN4100000",
    "Description": "Parent- CCL - NN4100000 - Global Initiative for Proliferation Prevention",
    "STARSCode": "C001057"
}, {
    "Id": 47,
    "DisplayName": "DN4000000",
    "Description": "Parent - CCL - DN4000000, Nonproliferation and Arms Control",
    "STARSCode": "C003042"
}, {
    "Id": 48,
    "DisplayName": "DN1005000",
    "Description": "Parent- CCL - ORS",
    "STARSCode": "BogusDN1005"
}, {
    "Id": 49,
    "DisplayName": "DN1006000",
    "Description": "Parent- CCL - ORS",
    "STARSCode": "BogusDN1006"
}, {
    "Id": 50,
    "DisplayName": "DP4000000",
    "Description": "Nuclear Counterterrorism Incident Response",
    "STARSCode": "C000925"
}, {
    "Id": 51,
    "DisplayName": "NN3000000",
    "Description": "International Nuclear Safety and Cooperation",
    "STARSCode": "C001053"
}, {
    "Id": 52,
    "DisplayName": "CT0000000",
    "Description": "Nuclear Counterterrorism and Incident Response Program",
    "STARSCode": "C003007"
}, {
    "Id": 53,
    "DisplayName": "DN3004000",
    "Description": "Laboratory and Partnership Support",
    "STARSCode": "C003121"
}]';

	SET IDENTITY_INSERT Financial.ControlPoint ON;

	INSERT INTO Financial.ControlPoint (Id, DisplayName, Description, STARSCode, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT oj.Id, oj.DisplayName, oj.Description, oj.STARSCode, @user, @user, @user, @user 
	FROM OPENJSON(@json) WITH (Id INT '$.Id', DisplayName NVARCHAR(9) '$.DisplayName', Description NVARCHAR(200) '$.Description', STARSCode NVARCHAR(25) '$.STARSCode') oj
	EXCEPT
	SELECT cp.Id, cp.DisplayName, cp.Description, cp.STARSCode, @user, @user, @user, @user
	FROM Financial.ControlPoint cp;
	
	SET IDENTITY_INSERT Financial.ControlPoint OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH