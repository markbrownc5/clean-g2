BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @levels NVARCHAR(MAX) = '[{
    "Id": 11,
    "SubSystemId": 1,
    "Label": "Office"
}, {
    "Id": 12,
    "SubSystemId": 1,
    "Label": "Portfolio"
}, {
    "Id": 13,
    "SubSystemId": 1,
    "Label": "Program"
}, {
    "Id": 14,
    "SubSystemId": 1,
    "Label": "Project"
}, {
    "Id": 15,
    "SubSystemId": 1,
    "Label": "Sub-Program"
}, {
    "Id": 18,
    "SubSystemId": 1,
    "Label": "Task"
}]';
	
	SET IDENTITY_INSERT G2.SubSystemLevel ON;

	INSERT INTO G2.SubSystemLevel (Id, SubSystemId, Label, CreatedBy, CreatedAs, LastModifiedBy, LastModifiedAs)
	SELECT l.Id, l.SubSystemId, l.Label, @userId, @userId, @userId, @userId
	FROM OPENJSON(@levels) WITH (Id INT '$.Id', SubSystemId INT '$.SubSystemId', Label NVARCHAR(50) '$.Label') l
	EXCEPT 
	SELECT ssl.Id, ssl.SubSystemId, ssl.Label, @userId, @userId, @userId, @userId
	FROM G2.SubSystemLevel ssl;

	SET IDENTITY_INSERT G2.SubSystemLevel OFF;

	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH