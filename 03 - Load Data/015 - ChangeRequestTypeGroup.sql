BEGIN TRY
		
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRANSACTION;
	
	DECLARE @userId INT = 614, @crtg NVARCHAR(MAX) = '[{
    "Id": 1,
    "Name": "FinPlan",
    "Description": "Financial Baseline Changes(AFP)"
}, {
    "Id": 2,
    "Name": "Project Status Changes",
    "Description": "Project Status Changes"
}, {
    "Id": 7,
    "Name": "Funding Adjustment",
    "Description": "One-sided funding adjustments"
}, {
    "Id": 9,
    "Name": "Spend Plan",
    "Description": "Spend Plan Changes"
}, {
    "Id": 10,
    "Name": "Baseline",
    "Description": "Schedule, Scope, Metric and Out-Year changes"
}, {
    "Id": 11,
    "Name": "Cost",
    "Description": "Direct Costs"
}, {
    "Id": 12,
    "Name": "Travel",
    "Description": "Travel Changes"
}, {
    "Id": 13,
    "Name": "Training",
    "Description": "Training Changes"
}, {
    "Id": 14,
    "Name": "Indirect and Other Direct Cost",
    "Description": "Indirect and Other Direct Costs"
}, {
    "Id": 15,
    "Name": "Budget Formulation",
    "Description": "Budget Formulation requests"
}, {
    "Id": 16,
    "Name": "Year End Forecast",
    "Description": "Year End Forecast"
}, {
    "Id": 17,
    "Name": "Work Breakdown Structure",
    "Description": "Work Breakdown Structure Changes"
}, {
    "Id": 18,
    "Name": "Funding Requests",
    "Description": "Funding Requests"
}, {
    "Id": 19,
    "Name": "Asset Management",
    "Description": "Asset Management Changes"
}, {
    "Id": 20,
    "Name": "Proposal",
    "Description": "Proposal Changes"
}, {
    "Id": 21,
    "Name": "Activity Funding Requests",
    "Description": "Activity Funding Requests"
}]';

	SET IDENTITY_INSERT G2.ChangeRequestTypeGroup ON;

	INSERT INTO G2.ChangeRequestTypeGroup (Id, Name, Description)
	SELECT c.Id, c.Name, c.Description 
	FROM OPENJSON(@crtg) WITH (Id INT '$.Id', Name NVARCHAR(50) '$.Name', Description NVARCHAR(50) '$.Description') c
	EXCEPT
	SELECT crtg.Id, crtg.Name, crtg.Description 
	FROM G2.ChangeRequestTypeGroup crtg;

	SET IDENTITY_INSERT G2.ChangeRequestTypeGroup OFF;
	
	COMMIT TRANSACTION;
	
END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END
	
	DECLARE @ProcName NVARCHAR(100);
	SELECT @ProcName = COALESCE(OBJECT_NAME(@@procid),ERROR_PROCEDURE());
	EXEC Tools.LogError @ProcName;
	
END CATCH