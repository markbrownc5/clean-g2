/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 2:57:55 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating types'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueStringTable] AS TABLE
(
[Val] [varchar] (900) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
PRIMARY KEY CLUSTERED  ([Val])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIntTable] AS TABLE
(
[Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIntIntTable] AS TABLE
(
[IntVal1] [int] NOT NULL,
[IntVal2] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([IntVal1], [IntVal2])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIntIntBitTable] AS TABLE
(
[IntVal1] [int] NOT NULL,
[IntVal2] [int] NOT NULL,
[BitVal] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([IntVal1], [IntVal2], [BitVal])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdUniqueIntTable] AS TABLE
(
[Id] [int] NOT NULL,
[IntVal] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id], [IntVal])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdTimestampTable] AS TABLE
(
[Id] [int] NOT NULL,
[Timestamp] [binary] (8) NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdTable] AS TABLE
(
[Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdIntTable] AS TABLE
(
[Id] [int] NOT NULL,
[IntVal] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdIntIntTable] AS TABLE
(
[Id] [int] NOT NULL,
[IntVal1] [int] NOT NULL,
[IntVal2] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdDecimalRowVersionTable] AS TABLE
(
[Id] [int] NOT NULL,
[DecimalVal] [decimal] (18, 2) NOT NULL,
[RowVersion] [binary] (8) NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdDateTimeTable] AS TABLE
(
[Id] [int] NOT NULL,
[DateTimeVal] [datetime] NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericUniqueIdBitTable] AS TABLE
(
[Id] [int] NOT NULL,
[BitVal] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericStringTable] AS TABLE
(
[Val] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericIntTable] AS TABLE
(
[IntVal] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericIntStringTable] AS TABLE
(
[Id] [int] NOT NULL,
[Val] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericIntIntTable] AS TABLE
(
[IntVal1] [int] NULL,
[IntVal2] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericIdIntStringTable] AS TABLE
(
[Id] [int] NOT NULL,
[IntVal] [int] NOT NULL,
[StrVal] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[GenericIdDateTimeBitTable] AS TABLE
(
[Id] [int] NOT NULL,
[DateTimeVal] [datetime] NOT NULL,
[BitVal] [bit] NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericIdDateTimeBitTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericIdDateTimeBitTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericIdIntStringTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericIdIntStringTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericIntIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericIntIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericIntStringTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericIntStringTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericStringTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericStringTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdBitTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdBitTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdDateTimeTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdDateTimeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdDecimalRowVersionTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdDecimalRowVersionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdIntIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdIntIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdTimestampTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdTimestampTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIdUniqueIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIdUniqueIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIntIntBitTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIntIntBitTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIntIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIntIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueIntTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueIntTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[GenericUniqueStringTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[GenericUniqueStringTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
