/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 5:54:53 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating full text catalogs'
GO
CREATE FULLTEXT CATALOG [G2]
WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [Proposal].[Request] KEY INDEX [PK_Request] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE FULLTEXT INDEX ON [Proposal].[Solicitation] KEY INDEX [PK_Solicitation] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [Proposal].[Request] ADD ([Name] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[Request] ADD ([Summary] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[Solicitation] ADD ([Name] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[Request] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[Solicitation] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
