/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 5:59:18 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusActiveStatesAndProcessedStates]'
GO

CREATE VIEW [G2].[vwChangeRequestDetailStatusActiveStatesAndProcessedStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id IN (1, 3) -- (Active, Processed)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusActiveStatesAndProcessedStates] on [G2].[vwChangeRequestDetailStatusActiveStatesAndProcessedStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusActiveStatesAndProcessedStates] ON [G2].[vwChangeRequestDetailStatusActiveStatesAndProcessedStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusActiveStates]'
GO


-- =============================================================
-- Author:      Chad Gilbert
-- Created:     09/8/2014
-- Release:     5.3
-- Description: Returns active request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusActiveStates]
WITH SCHEMABINDING
AS
	SELECT wbss.Id AS WorkBreakdownStructureStatusId, wbss.Name AS WorkBreakdownStructureStatusName
	FROM G2.WorkBreakdownStructureStatus wbss
	INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
	INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
	WHERE wbsss.Id = 1; -- G2.fnGetActiveWorkBreakdownStructureStatusStateId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusActiveStates] on [G2].[vwWorkBreakdownStructureStatusActiveStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusActiveStates] ON [G2].[vwWorkBreakdownStructureStatusActiveStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureConfigurationPivot]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     01/08/14
-- Release:     4.6
-- Description: Indexed view that pivots WorkBreakdownStructureConfiguration data
--
-- mm/dd/yy  Name      Release  Description
-- 09/17/14  s.oneal   5.3      Added HasAtLeastOneConfigInd column
-- 11/23/15  s.oneal   6.4      Modified to include configuration types for Indirect Cost, Other Direct Cost, Budget Formulation
--                              and renamed "HasAtLeastOneConfigInd" to "HasAtLeastOneLeafLevelConfigInd"
-- 02/06/17  j.borgers 7.5		Added SpendPlanInd
-- 10/04/17  j.borgers 8.3      Added BudgetFormulationGroupInd
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureConfigurationPivot]
WITH SCHEMABINDING
AS

	SELECT WorkBreakdownStructureId, [1] AS BudgetInd, [2] AS FundingInd, [3] AS CostObligationInd, [4] AS ScheduleInd,
		[5] AS IndirectCostInd, [6] AS OtherDirectCostInd, [7] AS BudgetFormulationInd, [9] AS SpendPlanInd, [10] AS BudgetFormulationGroupInd,
		[1] | [2] | [3] | [4] AS HasAtLeastOneLeafLevelConfigInd
	FROM (
		SELECT wbs.Id AS WorkBreakdownStructureId, wbsc.ConfigurationTypeId AS Config, wbsc.ConfigurationTypeId AS Config2
		FROM G2.WorkBreakdownStructure wbs
		LEFT OUTER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbs.Id = wbsc.WorkBreakdownStructureId
	) X
	PIVOT(COUNT(Config2) FOR Config IN ([1], [2], [3], [4], [5], [6], [7], [9], [10])) AS P
	-- G2.fnGetBudgetConfigurationTypeId(), G2.fnGetFundingConfigurationTypeId(), G2.fnGetCostObligationConfigurationTypeId(), G2.fnGetScheduleConfigurationTypeId(),
	-- G2.fnGetIndirectCostConfigurationTypeId(), G2.fnGetOtherDirectCostConfigurationTypeId(), G2.fnGetBudgetFormulationConfigurationTypeId(), G2.fnGetSpendPlanConfigurationTypeId(), G2.fnGetBudgetFormulationGroupConfigurationTypeId()


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[vwReportingEntityPerformerGroupXref]'
GO

CREATE VIEW [Contact].[vwReportingEntityPerformerGroupXref]
WITH SCHEMABINDING
AS
	SELECT ppr.SubSystemId, ppr.PartyId AS ReportingEntityOrganizationPartyId, apr.PartyId AS PerformerGroupOrganizationPartyId
	FROM Contact.PartyRole ppr 
	INNER JOIN Contact.PartyRelationship pr ON ppr.Id = pr.FromPartyRoleId 
	INNER JOIN Contact.PartyRole apr ON pr.ToPartyRoleId = apr.Id 
	WHERE ppr.PartyRoleTypeId = 6 -- Contact.fnGetPerformerPartyRoleTypeId() 
	AND pr.PartyRelationshipTypeId = 3 --Contact.fnGetPerformerGroupRollupPartyRelationshipTypeId()
	AND apr.PartyRoleTypeId = 7 --Contact.fnGetPerformerGroupPartyRoleTypeId() 
	AND pr.ThruDate IS NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusProcessedStates]'
GO

CREATE VIEW [G2].[vwChangeRequestDetailStatusProcessedStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id = 3 --Processed


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusProcessedStates] on [G2].[vwChangeRequestDetailStatusProcessedStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusProcessedStates] ON [G2].[vwChangeRequestDetailStatusProcessedStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusActiveStates]'
GO
CREATE VIEW [G2].[vwChangeRequestDetailStatusActiveStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id = 1 --Active


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusActiveStates] on [G2].[vwChangeRequestDetailStatusActiveStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusActiveStates] ON [G2].[vwChangeRequestDetailStatusActiveStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusActiveAndOpenRequestStates]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Returns active and open request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusActiveAndOpenRequestStates]
WITH SCHEMABINDING
AS
	SELECT wbss.Id AS WorkBreakdownStructureStatusId, wbss.Name AS WorkBreakdownStructureStatusName
	FROM G2.WorkBreakdownStructureStatus wbss
	INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
	INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
	WHERE wbsss.Id = 5; -- G2.fnGetActiveAndOpenRequestWorkBreakdownStructureStatusStateId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusActiveAndOpenRequestStates] on [G2].[vwWorkBreakdownStructureStatusActiveAndOpenRequestStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusActiveAndOpenRequestStates] ON [G2].[vwWorkBreakdownStructureStatusActiveAndOpenRequestStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[vwMaterialAtSite]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/19/2010
-- Release:     1.18
-- Description: A denormalized view of Inventory data including 
--              calculated values for radioactive decay.
--
-- mm/dd/yy  Name        Release  Description
-- 02/21/13  CGL         4.0      Removing ProjectId and MilestoneTemplateId from ProjectMilestone table
-- 08/01/13  Strickland  4.3      Update to use SPA Revision instead of VersionId
--                                Bug fix: SPA.SPAMainGeneral was always joining on spg.Revision = 1 instead of spg.Revision = spa.Revision 
-- 10/09/14  Strickland  5.3      Update to use G2.WorkBreakdownStructure tables
-- 08/10/15  Strickland  6.3      Update with new function calls to get Ids
-- 11/10/15  Strickland  6.4      Update to include Resourcing Method Indicator and Resourcing Method name
-- 12/17/15  Strickland  6.4      Update to limit tasks to NA21 GMS subsystem and update milestone IDs for Contract Awarded and Shipment Complete
-- 01/19/16  Strickland  6.5      Update to use the NA21 GMS MilestoneTemplateCategoryId for Implementation
-- 08/05/16  Strickland  7.2      Update to include Inventory.Source.InScopeInd when determining OSRPStatusId
-- 10/24/17  Strickland  8.3      US8741 Update for new OSRP Status options
-- 12/22/17  Strickland  8.4      US9255 Add OUTER APPLY to determine if a device is an IDD Candidate or Factory Hardened
-- 01/04/18  Strickland  8.4      US9421 Add IDDCandidateStatusOverrideId
-- 01/16/18  n.coffey    8.4      Removed index hints that were not intended to be pushed to production
-- 01/16/18  Strickland  8.4      Exclude deleted sites and rooms
-- 01/18/18  n.coffey    8.4      DE8033 fixed unhandled exception with serial numbers larger than int data type can handle
-- 02/13/18  Strickland  8.5      US9307 Add NumberOfDevices
-- 04/24/18  Strickland  9.0      US9894 Consider Reduce - Remove milestones when determining disposition (OSRP and non-OSRP)
-- 05/01/18  Strickland  9.0      DE8250 Coalesce r.DeletedInd in where clause
-- 05/23/18  Strickland  9.0      DE8400 Only domestic sites should be considered for OSRP status
-- 06/21/18  Strickland  9.1      US10225 Add EnergyRange, EnergyUnitName, InstallDt, SupportedInstallInd
-- =============================================================
CREATE VIEW [Inventory].[vwMaterialAtSite]
AS

-- Get the non-storage/disposition/manufacture/distribution type buildings which can have 
-- only a single type per source
SELECT 
    c.Id CountryId, c.Name CountryName,
    s.Id SiteId, s.Name SiteName,
    b.Id BuildingId, b.Name BuildingName, 
    MIN(JouleMetric.FinishDt) JouleMetricFinishDt, 
    COALESCE(r.Id, -1) RoomId, COALESCE(r.Name, '') RoomName,
    COALESCE(rt.Id, -1) RoomTypeId,
    COALESCE(src.Id, -1) SourceId,
    COALESCE(m.Id, -1) MaterialTypeId, COALESCE(m.Name, '') MaterialTypeName,
    COALESCE(st.Id, -1) SourceTypeId, st.Name SourceTypeName,
    COALESCE(fn.Id, -1) FissileNuclideId, fn.Name FissileNuclideName,
    COALESCE(dt.Id, -1) DeviceTypeId, dt.Name DeviceTypeName,
    COALESCE(dst.Id, -1) DeviceSubTypeId, dst.Name DeviceSubTypeName,
    COALESCE(v.Id, -1) VendorId, v.Name VendorName, src.VendorOther, 
    COALESCE(dmm.Id, -1) DeviceMakeModelId, dmm.Name DeviceMakeModelName,
    src.DeviceMakeModelOther, src.DeviceSerialNumber,
    COALESCE(src.NumberOfSources, 0) NumberOfSources,
    COALESCE(src.NumberOfDevices, 0) NumberOfDevices,
    COALESCE(st.CalculateActivityInd, CAST(0 AS BIT)) CalculateActivityInd,
    
    -- Reported Activity and Reported Activity Date
    CASE WHEN m.Id = 1 THEN
        COALESCE(src.OriginalActivity, 0)
        -- If OSRP Shipment Completed milestone complete then multiply activity by 0 else 1
        * CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Reduce - Removal Rad Source Device Removed milestone completed then multiply activity by 0 else 1
        * CASE WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Dispositioned them multiply activity by 0
        * CASE WHEN src.DispositionInd = 1 THEN 0 ELSE 1 END
    ELSE
        0
    END RadReportedActivity,
    CASE WHEN m.Id = 1 THEN
        src.OriginalActivityDate --COALESCE(src.OriginalActivityDate, CONVERT(DATETIME, '1/1/1753'))
    ELSE
        0
    END RadReportedActivityDate,
    
    -- Calculated Activity based on SPA start date
    CASE WHEN m.Id = 1 THEN
        -- Need to calculate activity if we know source type (isotope),
        -- reported activity, and SPA date
        Inventory.fnGetCurrentActivity(COALESCE(src.OriginalActivity, 0), 
            COALESCE(src.OriginalActivityDate, COALESCE(spg.EventDt, CURRENT_TIMESTAMP)), 
            COALESCE(spg.EventDt, CURRENT_TIMESTAMP), 
            COALESCE(st.HalfLifeDays, 0))
        -- If OSRP Shipment Completed milestone complete then multiply activity by 0 else 1
        * CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Reduce - Removal Rad Source Device Removed milestone completed then multiply activity by 0 else 1
        * CASE WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Dispositioned them multiply activity by 0
        * CASE WHEN src.DispositionInd = 1 THEN 0 ELSE 1 END
    ELSE
        0
    END RadSPAActivity,
    COALESCE(spg.EventDt, CURRENT_TIMESTAMP) RadSPAActivityDate,
    
    -- Calculated Activity based on current date
    CASE WHEN m.Id = 1 THEN
        -- Need to calculate activity if we know source type (isotope),
        -- reported activity, and reported activity date
        Inventory.fnGetCurrentActivity(COALESCE(src.OriginalActivity, 0), 
            COALESCE(src.OriginalActivityDate, CURRENT_TIMESTAMP), 
            CURRENT_TIMESTAMP, 
            COALESCE(st.HalfLifeDays, 0))
        -- If OSRP Shipment Completed milestone complete then multiply activity by 0 else 1
        * CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Reduce - Removal Rad Source Device Removed milestone completed then multiply activity by 0 else 1
        * CASE WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Dispositioned them multiply activity by 0
        * CASE WHEN src.DispositionInd = 1 THEN 0 ELSE 1 END
    ELSE
        0
    END RadCurrentActivity,
    CURRENT_TIMESTAMP RadCurrentActivityDate,
    
    -- Recovered activity based on OSRP Date
    CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 
        -- Need to calculate activity if we know source type (isotope),
        -- reported activity, and recovered date
        Inventory.fnGetCurrentActivity(COALESCE(src.OriginalActivity, 0), 
            COALESCE(src.OriginalActivityDate, CURRENT_TIMESTAMP), 
            CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN OSRP_ShipmentCompleted.FinishDt ELSE ReduceRemove_RadSourceDeviceRemoved.FinishDt END, 
            COALESCE(st.HalfLifeDays, 0))
        -- If Dispositioned them multiply activity by 0
        * CASE WHEN src.DispositionInd = 1 THEN 0 ELSE 1 END
    ELSE 
        0
    END RadRecoveredActivity,
    CASE 
        WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN OSRP_ShipmentCompleted.FinishDt 
        WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN ReduceRemove_RadSourceDeviceRemoved.FinishDt
        ELSE NULL
    END RadRecoveredActivityDate, 
    
    -- Disposition activity based on Disposition Date
    CASE WHEN src.DispositionInd = 1 THEN 
        -- Need to calculate activity if we know source type (isotope),
        -- reported activity, and disposition date
        Inventory.fnGetCurrentActivity(COALESCE(src.OriginalActivity, 0), 
            COALESCE(src.OriginalActivityDate, CURRENT_TIMESTAMP), 
            COALESCE(src.DispositionDt, CURRENT_TIMESTAMP), 
            COALESCE(st.HalfLifeDays, 0))
        -- If OSRP Shipment Completed milestone complete then multiply activity by 0 else 1
        * CASE WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
        -- If Reduce - Removal Rad Source Device Removed milestone completed then multiply activity by 0 else 1
        * CASE WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 0 ELSE 1 END
    ELSE 
        0
    END RadDispositionActivity,
    src.DispositionDt RadDispositionActivityDate, 
    
    CASE WHEN m.Id = 2 THEN
        src.Amount
    ELSE
        0
    END NucFissileAmount,
    COALESCE(dt.UseLicensedMaximumInd, CONVERT(BIT, 0)) UseLicensedMaximumInd,
    COALESCE(src.LicensedMaximum, 0) LicensedMaximum,
    COALESCE(src.OperatingPower, 0) OperatingPower,
    COALESCE(src.FissileEnrichment, 0) FissileEnrichment,
    os.Id OperationalStatusId, os.Name OperationalStatusName,
    dt.ResourcingMethodInd, rm.Name ResourcingMethodName,
    src.DOEOwnedInd,
    
    osrp.Id OSRPStatusId,
    osrp.Name OSRPStatusName,
    osrp.Description OSRPStatusDescription,
        CASE
            -- Status is "Recovered"
            WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN OSRP_ShipmentCompleted.FinishDt
            WHEN c.Id = 1 AND COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN ReduceRemove_RadSourceDeviceRemoved.FinishDt
            ELSE NULL
        END OSRPStatusDate,
    
    -- IDD Fields
    CAST(CASE WHEN IDDCandidateDeviceTypes.DeviceTypeId IS NULL THEN 0 ELSE 1 END AS BIT) ShowIDDSectionInd,    
    dmmConfig.IDDCandidateInd, 
    dmmConfig.FactoryHardenedInd,
    src.IDDCandidateStatusOverrideId, icso.Name IDDCandidateStatusOverrideName,
    COALESCE(src.IDDStatusId, -1) IDDStatusId, idds.Name IDDStatusName,
    COALESCE(src.IDDContractId, -1) IDDContractId, iddc.Name IDDContractName,
    COALESCE(src.IDDTaskTypeId, -1) IDDTaskTypeId, iddtt.Name IDDTaskTypeName,    
    src.IDDInstallDt, src.DeviceAnchoredInd,
    src.IDDNoteId,
    
    -- 7/26/12, Strickland, 3.4, Add Disposition fields
    src.DispositionInd,
    src.DispositionDt,
    COALESCE(dpt.Id, -1) DispositionTypeId, dpt.Name DispositionTypeName,
    src.DispositionNoteId,
    
    nrc.Id NRCLicenseId,
    nrc.LicenseNo NRCLicenseNo,
    COALESCE(spa.Id, -1) SPAMainId,
    COALESCE(spa.SPAStatusId, -1) SPAStatusId,
    COALESCE(src.IsCompleteInd, CAST(0 AS BIT)) IsCompleteInd,
    COALESCE(sms.IsIncludedInd, CAST(0 AS BIT)) IsIncludedInSPAInd,
    b.InScopeInd BuildingInScopeInd,
    CAST(0 AS BIT) BuildingPlanningTaskInd,
    CASE WHEN RMSInstalled.MaxExists > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END RMSPreExistsInd,
    CASE WHEN RMSInstalled.MaxCompleted > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END RMSInstalledInd,
    -- US10225 Alt Tech Fields
    src.EnergyRange, 
    eu.Name EnergyUnitName, 
    src.InstallDt,
    src.SupportedInstallInd
FROM G2.Site s 
INNER JOIN G2.Country c ON c.Id = s.CountryId
INNER JOIN G2.Building b ON b.SiteId = s.Id
LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id AND r.DeletedInd = 0
LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
LEFT JOIN Inventory.MaterialType m ON m.Id = src.MaterialTypeId
LEFT JOIN Inventory.SourceType st ON st.Id = src.SourceTypeId
LEFT JOIN Inventory.FissileNuclide fn ON fn.Id = src.FissileNuclideId
LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
LEFT JOIN Inventory.DeviceSubType dst ON dst.Id = src.DeviceSubTypeId
LEFT JOIN Inventory.Vendor v ON v.Id = src.VendorId
LEFT JOIN Inventory.DeviceMakeModel dmm ON dmm.Id = src.DeviceMakeModelId
LEFT JOIN Inventory.OperationalStatus os ON os.Id = src.OperationalStatusId
LEFT JOIN Inventory.NRCLicense nrc ON nrc.Id = src.NRCLicenseId
LEFT JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
LEFT JOIN SPA.SPAMainGeneral spg ON spg.SPAMainId = spa.Id AND spg.Revision = spa.Revision
LEFT JOIN SPA.SPAMainSection sms ON sms.SPAMainId = spa.Id AND sms.RefId = src.Id  AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
-- Joule Metric Milestone
LEFT JOIN 
(
    SELECT tb.BuildingId, pm.FinishDt, pm.CompletedInd
    FROM G2.TaskBuilding tb
    INNER JOIN G2.Task t ON t.Id = tb.TaskId
    INNER JOIN G2.ProjectMilestone pm ON pm.TaskId = t.Id
        AND pm.JouleMetricInd = 1
    INNER JOIN G2.MilestoneTemplate mt ON mt.Id = t.MilestoneTemplateId
        AND mt.MilestoneTemplateCategoryId = 11
) JouleMetric ON JouleMetric.BuildingId = b.Id
LEFT JOIN G2.TaskDevice td 
    INNER JOIN G2.Task t ON t.Id = td.TaskId
    INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsTask.Id = t.WorkBreakdownStructureId AND wbsTask.SubSystemId = G2.fnGetNA21GMSSubSystemId()
    INNER JOIN G2.vwWorkBreakdownStructureStatusActiveStates wbsActiveStates ON wbsActiveStates.WorkBreakdownStructureStatusId = wbsTask.StatusId
    LEFT JOIN G2.ProjectMilestone OSRP_ContractAwarded ON OSRP_ContractAwarded.TaskId = td.TaskId
        AND OSRP_ContractAwarded.MilestoneId = 601 -- OSRP: Contract Awarded/Award Contract milestone IDs
    LEFT JOIN G2.ProjectMilestone OSRP_ShipmentCompleted ON OSRP_ShipmentCompleted.TaskId = td.TaskId
        AND OSRP_ShipmentCompleted.MilestoneId = 639 -- OSRP: Shipment Completed milestone ID
    LEFT JOIN G2.ProjectMilestone ReduceRemove_AwardContract ON ReduceRemove_AwardContract.TaskId = t.Id
        AND ReduceRemove_AwardContract.MilestoneId = 595 -- Reduce - Remove: Award Contract Milestone
    LEFT JOIN G2.ProjectMilestone ReduceRemove_RadSourceDeviceRemoved ON ReduceRemove_RadSourceDeviceRemoved.TaskId = t.Id
        AND ReduceRemove_RadSourceDeviceRemoved.MilestoneId = 1278
    ON td.SourceId = src.Id
LEFT JOIN Inventory.OSRPStatus osrp ON osrp.Id = 
    CASE WHEN c.Id = 1 THEN
        CASE
            -- Status is "Recovered"
            WHEN COALESCE(OSRP_ShipmentCompleted.CompletedInd, 0) = 1 THEN 2 -- fnGetOSRPStatusIdRecovered()
            WHEN COALESCE(ReduceRemove_RadSourceDeviceRemoved.CompletedInd, 0) = 1 THEN 2 -- fnGetOSRPStatusIdRecovered()
            -- Status is "Under Contract"
            WHEN COALESCE(OSRP_ContractAwarded.CompletedInd, 0) = 1 THEN 4 -- fnGetOSRPStatusIdUnderContract()
            WHEN COALESCE(ReduceRemove_AwardContract.CompletedInd, 0) = 1 THEN 4 -- fnGetOSRPStatusIdUnderContract()
            -- Status is "Registered"
            WHEN COALESCE(td.Id, -1) > 0 THEN 1 -- fnGetOSRPStatusIdRegistered()
            -- Status is "Registered without task"
            WHEN COALESCE(src.InScopeInd, 0) = 1 THEN 
                CASE COALESCE(src.SiteSourceUsageId, 1)
                    WHEN 1 THEN 5 -- fnGetOSRPStatusIdRegisteredWithoutATaskInUse()
                    WHEN 2 THEN 6 -- fnGetOSRPStatusIdRegisteredWithoutATaskExcess()
                    ELSE 3 -- -- fnGetOSRPStatusIdNone()
                END
            -- Else status is "None"
            ELSE 3 -- fnGetOSRPStatusIdNone()
        END
        -- Else status is "None"
        ELSE 3 -- fnGetOSRPStatusIdNone()
    END
LEFT JOIN SPA.IDDStatus idds ON idds.Id = src.IDDStatusId
LEFT JOIN SPA.IDDContract iddc ON iddc.Id = src.IDDContractId
LEFT JOIN SPA.IDDTaskType iddtt ON iddtt.Id = src.IDDTaskTypeId
LEFT JOIN Inventory.DispositionType dpt ON dpt.Id = src.DispositionTypeId
LEFT JOIN Inventory.RoomType rt ON rt.Id = r.RoomTypeId
LEFT JOIN 
(
    SELECT SPAMainId, RefId,
        MAX(CAST(COALESCE(ExistsInd, 0) AS INT)) MaxExists, 
        MAX(CAST(COALESCE(CompletedInd, 0) AS INT)) MaxCompleted
    FROM SPA.SecurityActionAnswer 
    WHERE EntityTypeId = 8 -- fnGetEntityTypeIdRoom()
    AND SecurityActionId = 19 -- fnGetSecurityactionIdRMS
    GROUP BY SPAMainId, RefId
) RMSInstalled ON RMSInstalled.SPAMainId = spa.Id AND RMSInstalled.RefId = r.Id
LEFT JOIN Inventory.ResourcingMethod rm ON rm.Id = src.ResourcingMethodId
LEFT JOIN SPA.IDDCandidateStatusOverride icso ON icso.Id = src.IDDCandidateStatusOverrideId
LEFT OUTER JOIN 
(
    SELECT dtdmmx.DeviceTypeId
    FROM Inventory.DeviceMakeModelSourceTypeConfig dmmstc
    INNER JOIN Inventory.DeviceMakeModel dmm ON dmm.Id = dmmstc.DeviceMakeModelId
    INNER JOIN Inventory.DeviceTypeDeviceMakeModelXref dtdmmx ON dtdmmx.DeviceMakeModelId = dmm.Id
    GROUP BY dtdmmx.DeviceTypeId
) IDDCandidateDeviceTypes ON IDDCandidateDeviceTypes.DeviceTypeId = dt.Id
OUTER APPLY 
(
    SELECT 
        dmmstc.FactoryHardenedInd, dmmstc.IDDCandidateInd, dmmstc.IDDSurveyRequiredInd
    FROM Inventory.DeviceMakeModelSourceTypeConfig dmmstc
    WHERE dmmstc.DeviceMakeModelId = src.DeviceMakeModelId
    AND dmmstc.SourceTypeId = src.SourceTypeId
--2147483647 is max value for an integer data type
    AND CASE WHEN ISNUMERIC(src.DeviceSerialNumber) = 1 AND TRY_PARSE(src.DeviceSerialNumber AS INTEGER) IS NOT NULL THEN TRY_PARSE(src.DeviceSerialNumber AS INTEGER) 
			 WHEN ISNUMERIC(src.DeviceSerialNumber) = 1 AND TRY_PARSE(src.DeviceSerialNumber AS INTEGER) IS NULL THEN 2147483647 
		ELSE -1 END BETWEEN COALESCE(dmmstc.SerialNumberLower, 0) AND COALESCE(dmmstc.SerialNumberUpper, 2147483647)
    AND (dmmstc.SerialNumberLower IS NOT NULL OR dmmstc.SerialNumberUpper IS NOT NULL )

    UNION
    
	SELECT 
        dmmstc.FactoryHardenedInd, dmmstc.IDDCandidateInd, dmmstc.IDDSurveyRequiredInd
    FROM Inventory.DeviceMakeModelSourceTypeConfig dmmstc
    WHERE dmmstc.DeviceMakeModelId = src.DeviceMakeModelId
    AND dmmstc.SourceTypeId = src.SourceTypeId
    AND dmmstc.SerialNumberLower IS NULL
    AND dmmstc.SerialNumberUpper IS NULL
) dmmConfig
LEFT JOIN Inventory.EnergyUnit eu ON eu.Id = src.EnergyUnitId

WHERE s.DeletedInd = 0
AND b.DeletedInd = 0
AND COALESCE(r.DeletedInd, 0) = 0

GROUP BY 
    c.Id, c.Name,
    s.Id, s.Name,
    b.Id, b.Name, 
    COALESCE(r.Id, -1), COALESCE(r.Name, ''),
    rt.Id,
    src.Id,
    m.Id, m.Name,
    st.Id, st.Name,
    fn.Id, fn.Name,
    dt.Id, dt.Name,
    dst.Id, dst.Name,
    v.Id, v.Name, src.VendorOther, 
    dmm.Id, dmm.Name,
    src.DeviceMakeModelOther, src.DeviceSerialNumber,
    src.NumberOfSources, 
    src.NumberOfDevices,
    st.CalculateActivityInd,     
    src.OriginalActivity,
    src.OriginalActivityDate,
    spg.EventDt, 
    st.HalfLifeDays,
    src.Amount,
    dt.UseLicensedMaximumInd, 
    src.LicensedMaximum, 
    src.OperatingPower, 
    src.FissileEnrichment, 
    os.Id, os.Name,
    dt.ResourcingMethodInd, rm.Name,
    src.DOEOwnedInd,
    td.Id,
    OSRP_ContractAwarded.CompletedInd,
    OSRP_ShipmentCompleted.CompletedInd,
    OSRP_ShipmentCompleted.FinishDt,
    ReduceRemove_RadSourceDeviceRemoved.CompletedInd,
    ReduceRemove_RadSourceDeviceRemoved.FinishDt,
    osrp.Id, osrp.Name, osrp.Description,
    dmmConfig.IDDCandidateInd, dmmConfig.FactoryHardenedInd,
    IDDCandidateDeviceTypes.DeviceTypeId,
    src.IDDCandidateStatusOverrideId,
    icso.Name,
    src.IDDStatusId, idds.Name,
    src.IDDContractId, iddc.Name,
    src.IDDTaskTypeId, iddtt.Name,    
    src.IDDInstallDt, src.DeviceAnchoredInd,
    src.IDDInstallDt, src.DeviceAnchoredInd, src.IDDNoteId,
    nrc.Id,
    nrc.LicenseNo,
    spa.Id,
    spa.SPAStatusId, 
    src.IsCompleteInd,
    sms.IsIncludedInd,
    src.DispositionInd,
    src.DispositionDt,
    dpt.Id, dpt.Name,
    src.DispositionNoteId,
    b.InScopeInd,
    RMSInstalled.MaxExists,
    RMSInstalled.MaxCompleted,
    src.EnergyRange, 
    eu.Name,
    src.InstallDt,
    src.SupportedInstallInd;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialAttractivenessLevel_Radiological]'
GO





-- =============================================================
-- Author:      Mike Strickland
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yyyy   Name        Release  Description
-- 06/16/2014   Strickland  5.1      Update to exclude dispositioned sources in calculations
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================================
CREATE VIEW [SPA].[vwMaterialAttractivenessLevel_Radiological]
AS

SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.RoomId, sq1.RoomName,
	sq1.SPAMainId,
	sq1.TotalRadSPAActivity,
	malrad.Id MaterialAttractivenessLevelId, malrad.[Name] MaterialAttractivenessLevelName
FROM (
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName,
		spa.Id SPAMainId,
		--SUM(CASE WHEN vw.UseLicensedMaximumInd = 1 THEN vw.LicensedMaximum ELSE CASE WHEN vw.CalculateActivityInd = 1 THEN vw.RadSPAActivity ELSE vw.RadReportedActivity END END) TotalRadSPAActivity
		CASE WHEN vw.OSRPStatusId = 2 OR vw.DispositionInd = 1 THEN
			-- Don't count curie amounts if device is recovered (OSRPStatusId = 2) or dispositioned
			0
		ELSE
			SUM(CASE WHEN vw.UseLicensedMaximumInd = 1 OR vw.CalculateActivityInd = 0 THEN vw.LicensedMaximum ELSE vw.RadSPAActivity END) 
		END TotalRadSPAActivity
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
		--AND sms.IsIncludedInd = 1
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	WHERE vw.MaterialTypeId = 1
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName, vw.OSRPStatusId, vw.DispositionInd,
		spa.Id
) sq1

LEFT JOIN SPA.MaterialAttractivenessLevelSourceTypeAmounts malx ON sq1.TotalRadSPAActivity >= malx.MinAmount 
	AND sq1.TotalRadSPAActivity < malx.MaxAmount
LEFT JOIN SPA.MaterialAttractivenessLevel malrad ON malrad.Id = malx.MaterialAttractivenessLevelId









GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialAttractivenessLevel_Nuclear]'
GO





-- =============================================================
-- Author:      Mike Strickland
--
-- mm/dd/yyyy  Name        Release  Description
-- 08/10/2015   Strickland  6.3     Update with new function calls to get Ids
-- =============================================================
CREATE VIEW [SPA].[vwMaterialAttractivenessLevel_Nuclear]
AS
-- First query determines material attractiveness level for HEU-235, U-233, Pu-238, Pu-239, Pu-240, and Pu-241
-- by joining to SPA.MaterialAttractivenessLevelFissileNuclideXref
SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.RoomId, sq1.RoomName,
	sq1.SPAMainId,
	sq1.FissileNuclideId,
	sq1.TotalNucFissileAmount,
	sq1.TotalOperatingPower,
	malnuc.Id MaterialAttractivenessLevelId, malnuc.[Name] MaterialAttractivenessLevelName
FROM (
	-- Subquery 1 (sq1) determines the total amount of each nuclide
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName,
		spa.Id SPAMainId,
		vw.FissileNuclideId,
		SUM(vw.NucFissileAmount) TotalNucFissileAmount,
		SUM(vw.OperatingPower) TotalOperatingPower
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	--INNER JOIN Inventory.FissileNuclide fn ON fn.Id = vw.FissileNuclideId
	WHERE vw.MaterialTypeId = 2
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName,
		spa.Id,
		vw.FissileNuclideId
) sq1
	
INNER JOIN SPA.MaterialAttractivenessLevelFissileNuclideXref malx ON malx.FissileNuclideId = sq1.FissileNuclideId 
	AND COALESCE(malx.MinAmount, 0) <= sq1.TotalNucFissileAmount 
	AND COALESCE(malx.MaxAmount, 999999) > sq1.TotalNucFissileAmount
INNER JOIN SPA.MaterialAttractivenessLevel malnuc ON malnuc.Id = malx.MaterialAttractivenessLevelId

UNION

-- Second query determines material attractiveness level by Operating Power for LEU-235 at Research Reactors
-- by joining to SPA.MaterialAttractivenessLevelReactorPower
SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.RoomId, sq1.RoomName,
	sq1.SPAMainId,
	sq1.FissileNuclideId,
	sq1.TotalNucFissileAmount,
	sq1.TotalOperatingPower,
	malnuc.Id MaterialAttractivenessLevelId, malnuc.[Name] MaterialAttractivenessLevelName
FROM (
	-- Subquery 1 (sq1) determines the total amount of each nuclide
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName,
		spa.Id SPAMainId,
		vw.FissileNuclideId,
		SUM(vw.NucFissileAmount) TotalNucFissileAmount,
		SUM(vw.OperatingPower) TotalOperatingPower
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	--INNER JOIN Inventory.FissileNuclide fn ON fn.Id = vw.FissileNuclideId
	WHERE vw.MaterialTypeId = 2
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		vw.RoomId, vw.RoomName,
		spa.Id,
		vw.FissileNuclideId
) sq1
	
INNER JOIN SPA.MaterialAttractivenessLevelReactorPower malx ON malx.FissileNuclideId = sq1.FissileNuclideId 
	AND COALESCE(malx.MinAmount, 0) <= sq1.TotalOperatingPower
	AND COALESCE(malx.MaxAmount, 999999) > sq1.TotalOperatingPower
INNER JOIN SPA.MaterialAttractivenessLevel malnuc ON malnuc.Id = malx.MaterialAttractivenessLevelId





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialAttractivenessLevel]'
GO
CREATE VIEW [SPA].[vwMaterialAttractivenessLevel]
AS
SELECT sq2.SPAMainId,
	sq2.CountryName,
	sq2.SiteId, sq2.SiteName,
	sq2.BuildingId, sq2.BuildingName,
	sq2.RoomId, sq2.RoomName,
	mal.Id MaterialAttractivenessLevelId, sq2.MaterialAttractivenessLevelName
FROM (
	-- Get the overall material prioritization
	SELECT sq1.SPAMainId,
		sq1.CountryName,
		sq1.SiteId, sq1.SiteName,
		sq1.BuildingId, sq1.BuildingName,
		sq1.RoomId, sq1.RoomName,
		MIN(sq1.MaterialAttractivenessLevelName) MaterialAttractivenessLevelName
	FROM (
		-- Get radiological material prioritization 
		SELECT SPAMainId,
			CountryName,
			SiteId, SiteName,
			BuildingId, BuildingName,
			RoomId, RoomName,
			MaterialAttractivenessLevelId, MaterialAttractivenessLevelName
		FROM SPA.vwMaterialAttractivenessLevel_Radiological
			
		UNION 

		-- Get nuclear material prioritization 
		SELECT SPAMainId,
			CountryName,
			SiteId, SiteName,
			BuildingId, BuildingName,
			RoomId, RoomName,
			MaterialAttractivenessLevelId, MaterialAttractivenessLevelName
		FROM SPA.vwMaterialAttractivenessLevel_Nuclear
	) sq1

	GROUP BY sq1.CountryName,
		sq1.SiteId, sq1.SiteName,
		sq1.BuildingId, sq1.BuildingName,
		sq1.RoomId, sq1.RoomName,
		sq1.SPAMainId
) sq2
INNER JOIN SPA.MaterialAttractivenessLevel mal ON mal.[Name] = sq2.MaterialAttractivenessLevelName
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[PerformerGroup]'
GO


CREATE VIEW [Contact].[PerformerGroup]
WITH SCHEMABINDING
AS
SELECT o.PartyId AS OrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, o.ActiveInd
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
WHERE pr.PartyRoleTypeId = 7 -- Contact.fnGetPerformerGroupPartyRoleTypeId()
AND pty.DeletedInd = 0


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_PerformerGroup] on [Contact].[PerformerGroup]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_PerformerGroup] ON [Contact].[PerformerGroup] ([SubSystemId], [OrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[Performer]'
GO

CREATE VIEW [Contact].[Performer]
WITH SCHEMABINDING
AS
SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
WHERE pr.PartyRoleTypeId = 6 -- Contact.fnGetPerformerPartyRoleTypeId()
AND pty.DeletedInd = 0

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_Performer] on [Contact].[Performer]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_Performer] ON [Contact].[Performer] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ui_Performer_SubSystemId_DisplayName] on [Contact].[Performer]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [ui_Performer_SubSystemId_DisplayName] ON [Contact].[Performer] ([SubSystemId], [DisplayName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwScheduleSecondaryReportingRolePersonList]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/14/14
-- Release:     5.3
-- Description: Retrieves a concatenated list of people
--              with a secondary responsibility for schedule reporting.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [G2].[vwScheduleSecondaryReportingRolePersonList]
AS
	WITH SecondaryReportingRolePersons
	AS
	(
		SELECT rm.SubsystemId, rm.ConfigurationTypeId, pr.WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Report.PrimarySecondaryRoleMapping rm
		INNER JOIN Security.PersonRole pr ON rm.SubSystemId = pr.SubSystemId AND rm.SecondaryRoleId = pr.RoleId
		INNER JOIN Security.Person p ON pr.Personid = p.Id
		WHERE rm.ConfigurationTypeId = G2.fnGetScheduleConfigurationTypeId() 
	),
	SecondaryReportingRolePersonList
	AS
	(
		SELECT srr.SubsystemId, srr.WorkBreakdownStructureId, (SELECT p2.Name + ', ' FROM SecondaryReportingRolePersons p2 WHERE srr.WorkBreakdownStructureId = p2.WorkBreakdownStructureId FOR XML PATH('')) AS SecondaryReportingRolePeople
		FROM SecondaryReportingRolePersons srr
		GROUP BY srr.SubSystemId, srr.WorkBreakdownStructureId
	)
	SELECT srr.SubSystemId, srr.WorkBreakdownStructureId, LEFT(srr.SecondaryReportingRolePeople, (LEN(srr.SecondaryReportingRolePeople) - 1)) AS SecondaryReportingRolePeople
	FROM SecondaryReportingRolePersonList srr;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwCountryWorkBreakdownStructure]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     9/10/2015
-- Release:     6.3
-- Description: A view linking countries to Work Breakdown Structure 
--              for SPA.
--
-- mm/dd/yy  Name        Release  Description
-- 10/07/16  Strickland  7.3      Added support for new SubSystem Level Sub Office
-- =============================================================
CREATE VIEW [SPA].[vwCountryWorkBreakdownStructure] 
AS

	SELECT 
	    COALESCE(sub_system.Id, -1) SubSystemId, 
        COALESCE(sub_system.Name, '') SubSystemName, 
        COALESCE(sub_system.ShortName, '') SubSystemShortName,
        COALESCE(wbs_program.Id, -1) ProgramWorkBreakdownStructureId,
		COALESCE(wbs_program.Name, '') ProgramWorkBreakdownStructureName,
		COALESCE(wbs_office.Id, -1) OfficeWorkBreakdownStructureId,
		COALESCE(wbs_office.Name, '') OfficeWorkBreakdownStructureName,
		COALESCE(wbs_sub_office.Id, -1) SubOfficeWorkBreakdownStructureId,
		COALESCE(wbs_sub_office.Name, '') SubOfficeWorkBreakdownStructureName,
		COALESCE(wbs_sub_program.Id, -1) SubProgramWorkBreakdownStructureId,
		COALESCE(wbs_sub_program.Name, '') SubProgramWorkBreakdownStructureName,
		COALESCE(wbs_portfolio_international.Id, -1) PortfolioWorkBreakdownStructureId,
		COALESCE(wbs_portfolio_international.Name, '') PortfolioWorkBreakdownStructureName,
		COALESCE(c.Id, -1) CountryId,
		COALESCE(c.[Name], '') CountryName
	FROM G2.Country c 
	INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx 
		ON wbscx.CountryId = c.Id
    -- Sub-Program assuming a one-to-one match between G2.Country and G2.WorkBreakdownStructure via G2.WorkBreakdownStructureCountryXref
    INNER JOIN G2.WorkBreakdownStructure wbs_sub_program
		ON wbs_sub_program.Id = wbscx.WorkBreakdownStructureId
        AND wbs_sub_program.SubSystemLevelId = G2.fnGetSubProgramSubSystemLevelId(5)
    --------------------------------------------------------------------------------------------
    -- Sub Office assuming Sub Office is the direct parent of Sub-Program
	INNER JOIN G2.WorkBreakdownStructure wbs_sub_office 
		ON wbs_sub_office.Id = wbs_sub_program.ParentId 
        AND wbs_sub_office.SubSystemLevelId = G2.fnGetSubOfficeSubSystemLevelId(5)
    --------------------------------------------------------------------------------------------
    -- Office assuming Office is the direct parent of Sub Office
	INNER JOIN G2.WorkBreakdownStructure wbs_office 
		ON wbs_office.Id = wbs_sub_office.ParentId 
        AND wbs_office.SubSystemLevelId = G2.fnGetOfficeSubSystemLevelId(5)
    -- Program
    INNER JOIN G2.WorkBreakdownStructure wbs_program
        ON wbs_program.Id = wbs_office.ParentId
        AND wbs_program.SubSystemLevelId = G2.fnGetProgramSubSystemLevelId(5)
    -- Sub-system
    INNER JOIN G2.SubSystem sub_system
        ON sub_system.Id = wbs_sub_program.SubSystemId
    -- Portfolio (international) assuming that the portfolio is the direct descendant of the sub-program 
    LEFT JOIN G2.WorkBreakdownStructure wbs_portfolio_international
        INNER JOIN G2.WorkBreakdownStructure wbs_project_international
            ON wbs_project_international.ParentId = wbs_portfolio_international.Id
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbs_worktype_xref
			ON wbs_worktype_xref.WorkBreakdownStructureId = wbs_project_international.Id
		INNER JOIN G2.WorkType wt 
			ON wt.Id = wbs_worktype_xref.WorkTypeId
			AND wt.WorkTypeGroupId = 15 -- GMS Protect Work Type Group
        ON wbs_portfolio_international.ParentId = wbs_sub_program.Id
        AND wbs_portfolio_international.SubSystemLevelId = G2.fnGetPortfolioSubSystemLevelId(5)
    GROUP BY 
        sub_system.Id, sub_system.Name, sub_system.ShortName,
        wbs_program.Id, wbs_program.Name, 
		wbs_office.Id, wbs_office.Name,
		wbs_sub_office.Id, wbs_sub_office.Name,
		wbs_sub_program.Id, wbs_sub_program.Name, 
        wbs_portfolio_international.Id, wbs_portfolio_international.Name, 
		c.Id, c.[Name];


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[LeadLab]'
GO

CREATE VIEW [Contact].[LeadLab]
WITH SCHEMABINDING
AS
	SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode
	FROM Contact.ReportingEntity re
	INNER JOIN Contact.Organization o ON re.OrganizationPartyId = o.PartyId
	INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId AND pr.PartyRoleTypeId = 9 -- Contact.fnGetLeadLabPartyRoleTypeId()
	WHERE o.ActiveInd = 1

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_LeadLab] on [Contact].[LeadLab]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_LeadLab] ON [Contact].[LeadLab] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusInactiveStates]'
GO



-- =============================================================
-- Author:      Chad Gilbert
-- Created:     09/8/2014
-- Release:     5.3
-- Description: Returns inactive request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusInactiveStates]
WITH SCHEMABINDING
AS
	SELECT wbss.Id AS WorkBreakdownStructureStatusId, wbss.Name AS WorkBreakdownStructureStatusName
	FROM G2.WorkBreakdownStructureStatus wbss
	INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
	INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
	WHERE wbsss.Id = 2; -- G2.fnGetInactiveWorkBreakdownStructureStatusStateId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusInactiveStates] on [G2].[vwWorkBreakdownStructureStatusInactiveStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusInactiveStates] ON [G2].[vwWorkBreakdownStructureStatusInactiveStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[vwPersonRoleReportingEntity]'
GO




-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2013
-- Release:     4.?
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 02/26/14  j.borgers 5.0      Modified to work with PDFM changes Luttrell said that this was ok
-- 07/16/14  j.borgers 5.2      Added Costs: Admin, and all four HQ Financial Officers to ON clause
-- 12/02/15  s.oneal   6.4      Modified to include System Admin, Planning: Admin, Planning: HQ roles
-- =============================================================
CREATE VIEW [Security].[vwPersonRoleReportingEntity]
WITH SCHEMABINDING
AS
	SELECT DISTINCT persR.PersonId, persR.RoleId, persR.SubSystemId, re.OrganizationPartyId AS ReportingEntityOrganizationPartyId
	FROM Security.PersonRole persR 
	LEFT OUTER JOIN Security.PersonRoleOrganizationBreakdownStructureDetail persROBSD ON persR.Id = persROBSD.PersonRoleId 
	INNER JOIN Contact.ReportingEntity re ON (re.OrganizationPartyId = persROBSD.OrganizationPartyId OR (persROBSD.Id IS NULL AND persR.RoleId IN (1, 3, 48, 90, 91, 95, 96, 97, 163, 164))) 
	INNER JOIN Contact.PartyRole ptr ON persR.SubSystemId = ptr.SubSystemId AND re.OrganizationPartyId = ptr.PartyId 




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[BudgetFormulationPerformer]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2015
-- Release:     6.4
-- Description: Retrieves the list of valid performers to be
--   used in Budget Formulation.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [Contact].[BudgetFormulationPerformer]
WITH SCHEMABINDING
AS
	SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd
	FROM Contact.Organization o
	INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
	INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
	INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
	WHERE pr.PartyRoleTypeId = 13 -- Contact.fnGetBudgetFormulationPerformerPartyRoleTypeId()
	AND pty.DeletedInd = 0

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_BudgetFormulationPerformer] on [Contact].[BudgetFormulationPerformer]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_BudgetFormulationPerformer] ON [Contact].[BudgetFormulationPerformer] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialPrioritizationAtSiteByBuildingAndSPA_Radiological]'
GO





-- =============================================================
-- Author:      Mike Strickland
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yyyy   Name        Release  Description
-- 08/01/2013   Strickland  4.3      Update to use SPA Revision instead of VersionId
--                                   Bug fix: SPA.SPAMainGeneral was always joining on spg.Revision = 1 instead of spg.Revision = spa.Revision which could change the SiteSecurityConditionScore
-- 09/12/2013   Strickland  4.4      Remove filter on SPA.SPAMainSection.IsIncludedInd = 1
--                                   Add filter on Inventory.vwMaterialAtSite.BuildingInScope = 1
-- 06/16/2014   Strickland  5.1      Update to exclude dispositioned and OSRP recovered sources in calculations
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================================
CREATE VIEW [SPA].[vwMaterialPrioritizationAtSiteByBuildingAndSPA_Radiological]
AS

SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.CountryThreatLevelId, sq1.CountryThreatLevelName, sq1.CountryThreatLevelScore,
	sq1.SiteSecurityConditionId, sq1.SiteSecurityConditionName, sq1.SiteSecurityConditionScore,
	sq1.UASITierId, sq1.UASITierName, sq1.UASITierScore, sq1.StrategicAssetScore, 
	sq1.SPAMainId,
	sq1.TotalRadSPAActivity,
	malrad.Id MaterialAttractivenessLevelId, malrad.[Name] MaterialAttractivenessLevelName,
	sq1.UnassessedSourceInd
FROM (
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id CountryThreatLevelId, ctl.[Name] CountryThreatLevelName, ctl.Score CountryThreatLevelScore,
		ssc.Id SiteSecurityConditionId, ssc.[Name] SiteSecurityConditionName, ssc.Score SiteSecurityConditionScore,
		uasi.Id UASITierId, uasi.[Name] UASITierName, uasi.Score UASITierScore,
		b.NearStrategicAssetInd StrategicAssetScore,
		spa.Id SPAMainId,
		--SUM(CASE WHEN vw.CalculateActivityInd = 1 THEN vw.RadSPAActivity ELSE vw.RadReportedActivity END) TotalRadSPAActivity
		--SUM(CASE WHEN vw.UseLicensedMaximumInd = 1 THEN vw.LicensedMaximum ELSE CASE WHEN vw.CalculateActivityInd = 1 THEN vw.RadSPAActivity ELSE vw.RadReportedActivity END END) TotalRadSPAActivity
		SUM(CASE WHEN vw.UseLicensedMaximumInd = 1 OR vw.CalculateActivityInd = 0 THEN vw.LicensedMaximum ELSE vw.RadSPAActivity END) TotalRadSPAActivity,
		CAST(CASE WHEN MIN(CAST(sms.IsIncludedInd AS INT)) = 0 THEN 1 ELSE 0 END AS BIT) UnassessedSourceInd
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	INNER JOIN SPA.SPAMainGeneral spg ON spg.SPAMainId = spa.Id	
		AND spg.Revision = spa.Revision
	LEFT JOIN SPA.CountryThreatRating ctl ON ctl.Id = COALESCE(c.CountryThreatRatingId, 2)
	LEFT JOIN Inventory.UrbanAreaCountyXref uacx ON uacx.CountyId = b.CountyId
	LEFT JOIN Inventory.UrbanArea ua ON ua.Id = uacx.UrbanAreaId
	LEFT JOIN Inventory.UASITier uasi ON uasi.Id = COALESCE(ua.UASITierId, 3)
	LEFT JOIN SPA.SiteSecurityCondition ssc ON ssc.Id = COALESCE(spg.SiteSecurityConditionId, 1)
	WHERE vw.MaterialTypeId = 1
	AND vw.BuildingInScopeInd = 1
	AND vw.DispositionInd = 0 -- Not dispositioned
	AND vw.OSRPStatusId <> 2 -- OSRP Recovered
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id, ctl.[Name], ctl.Score,
		ssc.Id, ssc.[Name], ssc.Score, 
		uasi.Id, uasi.[Name], uasi.Score, 
		b.NearStrategicAssetInd,
		spa.Id
) sq1

LEFT JOIN SPA.MaterialAttractivenessLevelSourceTypeAmounts malx ON sq1.TotalRadSPAActivity >= malx.MinAmount 
	AND sq1.TotalRadSPAActivity < malx.MaxAmount
LEFT JOIN SPA.MaterialAttractivenessLevel malrad ON malrad.Id = malx.MaterialAttractivenessLevelId










GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialPrioritizationAtSiteByBuildingAndSPA_Nuclear]'
GO





-- =============================================================
-- Author:      Mike Strickland
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yyyy   Name        Release  Description
-- 08/01/2013   Strickland  4.3      Update to use SPA Revision instead of VersionId
--                                   Bug fix: SPA.SPAMainGeneral was always joining on spg.Revision = 1 instead of spg.Revision = spa.Revision which could change the SiteSecurityConditionScore
-- 09/12/2013   Strickland  4.4      Remove filter on SPA.SPAMainSection.IsIncludedInd = 1
--                                   Add filter on Inventory.vwMaterialAtSite.BuildingInScope = 1
--                                   Add UnassessedSourceInd to note if calculation includes unassessed sources
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================================
CREATE VIEW [SPA].[vwMaterialPrioritizationAtSiteByBuildingAndSPA_Nuclear]
AS
-- First query determines material attractiveness level for HEU-235, U-233, Pu-238, Pu-239, Pu-240, and Pu-241
-- by joining to SPA.MaterialAttractivenessLevelFissileNuclideXref
SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.CountryThreatLevelId, sq1.CountryThreatLevelName, sq1.CountryThreatLevelScore,
	sq1.SiteSecurityConditionId, sq1.SiteSecurityConditionName, sq1.SiteSecurityConditionScore,
	sq1.UASITierId, sq1.UASITierName, sq1.UASITierScore, sq1.StrategicAssetScore,
	sq1.SPAMainId,
	sq1.TotalNucFissileAmount,
	sq1.TotalOperatingPower,
	malnuc.Id MaterialAttractivenessLevelId, malnuc.[Name] MaterialAttractivenessLevelName,
	sq1.UnassessedSourceInd
FROM (
	-- Subquery 1 (sq1) determines the total amount of each nuclide
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id CountryThreatLevelId, ctl.[Name] CountryThreatLevelName, ctl.Score CountryThreatLevelScore,
		ssc.Id SiteSecurityConditionId, ssc.[Name] SiteSecurityConditionName, ssc.Score SiteSecurityConditionScore,
		uasi.Id UASITierId, uasi.[Name] UASITierName, uasi.Score UASITierScore,
		b.NearStrategicAssetInd StrategicAssetScore,
		spa.Id SPAMainId,
		vw.FissileNuclideId,
		SUM(vw.NucFissileAmount) TotalNucFissileAmount,
		SUM(vw.OperatingPower) TotalOperatingPower,
		CAST(CASE WHEN MIN(CAST(sms.IsIncludedInd AS INT)) = 0 THEN 1 ELSE 0 END AS BIT) UnassessedSourceInd
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	INNER JOIN SPA.SPAMainGeneral spg ON spg.SPAMainId = spa.Id	
		AND spg.Revision = spa.Revision
	INNER JOIN Inventory.FissileNuclide fn ON fn.Id = vw.FissileNuclideId
	LEFT JOIN SPA.CountryThreatRating ctl ON ctl.Id = COALESCE(c.CountryThreatRatingId, 2)
	LEFT JOIN Inventory.UrbanAreaCountyXref uacx ON uacx.CountyId = b.CountyId
	LEFT JOIN Inventory.UrbanArea ua ON ua.Id = uacx.UrbanAreaId
	LEFT JOIN Inventory.UASITier uasi ON uasi.Id = COALESCE(ua.UASITierId, 3)
	LEFT JOIN SPA.SiteSecurityCondition ssc ON ssc.Id = COALESCE(spg.SiteSecurityConditionId, 1)
	WHERE vw.MaterialTypeId = 2
	AND vw.BuildingInScopeInd = 1
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id, ctl.[Name], ctl.Score,
		ssc.Id, ssc.[Name], ssc.Score, 
		uasi.Id, uasi.[Name], uasi.Score, 
		b.NearStrategicAssetInd, 
		spa.Id,
		vw.FissileNuclideId
		--, vw.FissileEnrichment, vw.NucFissileAmount
) sq1
	
INNER JOIN SPA.MaterialAttractivenessLevelFissileNuclideXref malx ON malx.FissileNuclideId = sq1.FissileNuclideId 
	AND COALESCE(malx.MinAmount, 0) <= sq1.TotalNucFissileAmount 
	AND COALESCE(malx.MaxAmount, 999999) > sq1.TotalNucFissileAmount
INNER JOIN SPA.MaterialAttractivenessLevel malnuc ON malnuc.Id = malx.MaterialAttractivenessLevelId

UNION

-- Second query determines material attractiveness level by Operating Power for LEU-235 at Research Reactors
-- by joining to SPA.MaterialAttractivenessLevelReactorPower
SELECT sq1.CountryId, sq1.CountryName,
	sq1.SiteId, sq1.SiteName,
	sq1.BuildingId, sq1.BuildingName,
	sq1.CountryThreatLevelId, sq1.CountryThreatLevelName, sq1.CountryThreatLevelScore,
	sq1.SiteSecurityConditionId, sq1.SiteSecurityConditionName, sq1.SiteSecurityConditionScore,
	sq1.UASITierId, sq1.UASITierName, sq1.UASITierScore, sq1.StrategicAssetScore,
	sq1.SPAMainId,
	sq1.TotalNucFissileAmount,
	sq1.TotalOperatingPower,
	malnuc.Id MaterialAttractivenessLevelId, malnuc.[Name] MaterialAttractivenessLevelName,
	sq1.UnassessedSourceInd
FROM (
	-- Subquery 1 (sq1) determines the total amount of each nuclide
	SELECT c.Id CountryId, c.[Name] CountryName,
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id CountryThreatLevelId, ctl.[Name] CountryThreatLevelName, ctl.Score CountryThreatLevelScore,
		ssc.Id SiteSecurityConditionId, ssc.[Name] SiteSecurityConditionName, ssc.Score SiteSecurityConditionScore,
		uasi.Id UASITierId, uasi.[Name] UASITierName, uasi.Score UASITierScore,
		b.NearStrategicAssetInd StrategicAssetScore,
		spa.Id SPAMainId,
		vw.FissileNuclideId,
		SUM(vw.NucFissileAmount) TotalNucFissileAmount,
		SUM(vw.OperatingPower) TotalOperatingPower,
		CAST(CASE WHEN MIN(CAST(sms.IsIncludedInd AS INT)) = 0 THEN 1 ELSE 0 END AS BIT) UnassessedSourceInd
	FROM Inventory.vwMaterialAtSite vw
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = vw.SPAMainId
		AND sms.RefId = vw.SourceId 
		AND sms.SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	INNER JOIN G2.Site s on s.Id = vw.SiteId
	INNER JOIN G2.Country c ON c.Id = s.CountryId
	INNER JOIN G2.Building b ON b.Id = vw.BuildingId
	INNER JOIN SPA.SPAMain spa ON spa.SiteId = s.Id
	INNER JOIN SPA.SPAMainGeneral spg ON spg.SPAMainId = spa.Id	
		AND spg.Revision = spa.Revision
	INNER JOIN Inventory.FissileNuclide fn ON fn.Id = vw.FissileNuclideId
	LEFT JOIN SPA.CountryThreatRating ctl ON ctl.Id = COALESCE(c.CountryThreatRatingId, 2)
	LEFT JOIN Inventory.UrbanAreaCountyXref uacx ON uacx.CountyId = b.CountyId
	LEFT JOIN Inventory.UrbanArea ua ON ua.Id = uacx.UrbanAreaId
	LEFT JOIN Inventory.UASITier uasi ON uasi.Id = COALESCE(ua.UASITierId, 3)
	LEFT JOIN SPA.SiteSecurityCondition ssc ON ssc.Id = COALESCE(spg.SiteSecurityConditionId, 1)
	WHERE vw.MaterialTypeId = 2
	AND vw.BuildingInScopeInd = 1
	GROUP BY c.Id, c.[Name],
		vw.SiteId, vw.SiteName,
		vw.BuildingId, vw.BuildingName,
		ctl.Id, ctl.[Name], ctl.Score,
		ssc.Id, ssc.[Name], ssc.Score, 
		uasi.Id, uasi.[Name], uasi.Score, 
		b.NearStrategicAssetInd, 
		spa.Id,
		vw.FissileNuclideId
		--, vw.FissileEnrichment, vw.NucFissileAmount
) sq1
	
INNER JOIN SPA.MaterialAttractivenessLevelReactorPower malx ON malx.FissileNuclideId = sq1.FissileNuclideId 
	AND COALESCE(malx.MinAmount, 0) <= sq1.TotalOperatingPower 
	AND COALESCE(malx.MaxAmount, 999999) > sq1.TotalOperatingPower
INNER JOIN SPA.MaterialAttractivenessLevel malnuc ON malnuc.Id = malx.MaterialAttractivenessLevelId





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialPrioritization]'
GO

CREATE VIEW [SPA].[vwMaterialPrioritization]
AS
SELECT sq2.SPAMainId,
	sq2.CountryName,
	sq2.SiteId, sq2.SiteName,
	sq2.BuildingId, sq2.BuildingName,
	mal.Id MaterialAttractivenessLevelId, sq2.MaterialAttractivenessLevel,
	sq2.CountryThreatLevelName, sq2.CountryThreatLevelScore,
	sq2.SiteSecurityConditionName, sq2.SiteSecurityConditionScore,
--	sq2.UASITierName, sq2.UASITierScore,
--	sq2.StrategicAssetScore,
	sap.[Name] LocationPriorityName, sq2.LocationPriorityScore,
	sq2.OtherFactorsScore,
	g.[Name] GTRIPriority,
	sq2.UnassessedSourceInd
FROM (
	-- Get the overall material prioritization
	SELECT sq1.SPAMainId,
		sq1.CountryName,
		sq1.SiteId, sq1.SiteName,
		sq1.BuildingId, sq1.BuildingName,
		MIN(sq1.MaterialAttractivenessLevelName) MaterialAttractivenessLevel,
		sq1.CountryThreatLevelName, sq1.CountryThreatLevelScore,
		sq1.SiteSecurityConditionName, sq1.SiteSecurityConditionScore,
--		sq1.UASITierName, sq1.UASITierScore,
--		sq1.StrategicAssetScore,
		sq1.UASITierScore + sq1.StrategicAssetScore LocationPriorityScore,
		sq1.CountryThreatLevelScore + sq1.SiteSecurityConditionScore + sq1.UASITierScore + sq1.StrategicAssetScore OtherFactorsScore,
		sq1.UnassessedSourceInd
	FROM (
		-- Get radiological material prioritization 
		SELECT SPAMainId,
			CountryName,
			SiteId, SiteName,
			BuildingId, BuildingName,
			MaterialAttractivenessLevelId, MaterialAttractivenessLevelName,
			CountryThreatLevelName, CountryThreatLevelScore,
			SiteSecurityConditionName, SiteSecurityConditionScore,
			UASITierName, UASITierScore,
			StrategicAssetScore,
			UnassessedSourceInd
		FROM SPA.vwMaterialPrioritizationAtSiteByBuildingAndSPA_Radiological
			
		UNION 

		-- Get nuclear material prioritization 
		SELECT SPAMainId,
			CountryName,
			SiteId, SiteName,
			BuildingId, BuildingName,
			MaterialAttractivenessLevelId, MaterialAttractivenessLevelName,
			CountryThreatLevelName, CountryThreatLevelScore,
			SiteSecurityConditionName, SiteSecurityConditionScore,
			UASITierName, UASITierScore,
			StrategicAssetScore,
			UnassessedSourceInd
		FROM SPA.vwMaterialPrioritizationAtSiteByBuildingAndSPA_Nuclear
	) sq1

	GROUP BY sq1.CountryName,
		sq1.SiteId, sq1.SiteName,
		sq1.BuildingId, sq1.BuildingName,
		sq1.CountryThreatLevelName, sq1.CountryThreatLevelScore,
		sq1.SiteSecurityConditionName, sq1.SiteSecurityConditionScore,
		sq1.UASITierName, sq1.UASITierScore,
		sq1.StrategicAssetScore,
		sq1.SPAMainId,
		sq1.UnassessedSourceInd
) sq2
INNER JOIN SPA.MaterialAttractivenessLevel mal ON mal.[Name] = sq2.MaterialAttractivenessLevel
INNER JOIN SPA.GTRIPriorityMaterialAttractivenessLevelXref x ON x.MaterialAttractivenessLevelId = mal.Id 
	AND sq2.OtherFactorsScore BETWEEN x.MinScore AND x.MaxScore
INNER JOIN SPA.GTRIPriority g ON g.Id = x.GTRIPriorityId
INNER JOIN SPA.LocationPriority sap ON sap.Score = sq2.LocationPriorityScore

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialAttractivenessLevelByBuilding]'
GO
CREATE VIEW [SPA].[vwMaterialAttractivenessLevelByBuilding]
AS
SELECT SPAMainId, CountryName, SiteId, SiteName, BuildingId, BuildingName,
	MIN(MaterialAttractivenessLevelId) MaterialAttractivenessLevelId, 
	MIN(MaterialAttractivenessLevel) MaterialAttractivenessLevelName
-- This data needs to be taken from vwMaterialPrioritization instead of vwMaterialAttractivenessLevel.
-- vwMaterialPrioritization will get the correct MAL rolled up to the building level.
-- vwMaterialAttractivenessLevel would only get the highest room level.
FROM SPA.vwMaterialPrioritization
GROUP BY SPAMainId, CountryName, SiteId, SiteName, BuildingId, BuildingName
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[vwAsset]'
GO
-- =============================================================
-- Author:      G. A. Ivey
-- Created:     03/16/2016
-- Release:     7.0
-- Description: Asset view for use with the MDI module. 
--
-- mm/dd/yy  Name      Release  Description
-- 04/04/16  m.morrell 7.0      Updated to include the new Contamination Category and remove the join to the FIMS.FIMSViewNNSAG2 table
-- 04/07/16  j.borgers 7.0      Added deleted assets that have MDI data
-- 04/08/16  j.borgers 7.0      Added Coalesce to SiteAbbreviation so it is less likely to be empty
-- 05/18/16	 n.harris  7.1		Added Score column from MDI.AssetDetail to view
-- 06/01/16  m.morrell 7.1      Added the FIMS FacilityConditionIndex 
-- 06/02/16  m.morrell 7.1      Added the Asset Operating Status Code and Description
-- 06/22/16  j.borgers 7.1      Modified what comprised "MDI data" - AssetDetail, AssetCapability, or SupportingAsset 
-- 08/11/16  m.morrell 7.2      Added YearAcquired and RepairNeeds
-- 09/27/16  j.borgers 7.3      Changed OperatingStatus of a deleted Asset to 'Archived' and the score to NULL
-- 12/08/16  j.borgers 7.4		Updated columns to match changes in FIMS
-- 01/26/16  a.smith   7.5      Added BuildingConditionIndex to view
-- 02/17/17  DNC       7.5      Added BuildingPerformanceIndex to view
-- 06/06/17  EGP       8.1      Source BuildingConditionIndex and BuildingPerformanceIndex from new table
-- 06/20/17  Marshall  8.1      Renamed BuildingDescription to Description
-- 06/23/17  DNC       8.1      Add PropertyProgramOwner and Annual O&M (sourced from FIMSConsolidatedDetails)
-- 06/23/17  s.oneal   8.1      Adding AssetGroupId
-- 06/26/17  j.borgers 8.1      Adding columns ExcessDate and SupportingAssetAbsentInd and updating what deleted assets are shown to include new data
-- 06/29/17  DNC       8.1      Remove AnnualO&M (calculated else from snapshot records)
-- 08/08/17  a.smith   8.2      Added FIMS.FIMSConsolidated.ExcessDate to the view and upated index
-- 08/09/17  s.oneal   8.2      Removed ReviewCompleteInd
-- 08/10/17  egp       8.2      Add [Est Cleanup + Disp Cost]
-- 08/14/17  s.oneal   8.2      Adding G2 organization columns (for the G2 Site)
-- 09/11/17  s.oneal   8.2      Fixed issue in COALESCE where there was a data type mismatch
-- 03/02/18  g.ogle    8.5      Adding OperatingCost and AnticipatedDispositionMethod from FIMSConsolidatedDetails
-- 03/06/18  g.ogle    8.5      Adding CantCurrentlyBeDisposed from FIMSConsolidatedDetails
-- 04/09/18  d.abston  9.0      Modified PropertyType to use G2 table and added Ownership from FIMS
-- 04/10/18  j.borgers 9.0      Modified YearBuilt to pull from AssetDetails instead of FIMS
-- 04/24/18  d.abston  9.0      Modified to pull ExcessDate and FIMSExcessDate from G2 instead of FIMS
-- 04/26/18  j.borgers 9.0      Renamed AssetAttachmentXref to ImageXref
-- 06/25/18  j.borgers 9.1      Removed unneeded columns and changed operating status to not pull from FIMSConsolidatedDetails and added ArchivedInd
-- 06/29/18  d.abston  9.1      Removed unnecessary columns and added calculation for ExcessInd
-- =============================================================
CREATE VIEW [MDI].[vwAsset]
AS
	/*NOTE: If columns get added or removed the index on FIMSConsolidatedDetails needs to be updated*/

	SELECT a.Id AS AssetId, a.RealPropertyUniqueIdentifier, fcd.PropertyId, fcd.PropertyName, fcd.AlternateName,
		fcd.MissionDependentProgram, fcd.MissionDependency, fcd.MissionUniqueFacility, fcd.ReplacementPlantValue,  
		fcd.HazardCategory1, fcd.AnnualActualMaintenance, fcd.AnnualRequiredMaintenance, 
		fcd.DeferredMaintenance, fcd.OverallAssetCondition, bcd.BuildingConditionIndex, bcd.BuildingPerformanceIndex, 
		fcd.YearAcquired, fcd.AssetPercentUtilized AS PercentUtilization, 
		fcd.EstDispositionYr, fcd.AnticipatedDispositionMethod, fcd.CantCurrentlyBeDisposed, 
		fcd.RepairNeeds, fcd.FacilityConditionIndex, 
		CAST(CASE WHEN CAST(COALESCE(ed.ExcessToDOEDate, '12/31/9999 23:59:59.997') AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) THEN 1 ELSE 0 END AS BIT) AS ExcessInd, 
		fcd.[EstCleanupDispCost] AS EstCleanupDispCost, fcd.OperatingCost
	FROM G2.Asset a
	INNER JOIN FIMS.FIMSConsolidatedDetails fcd ON a.RealPropertyUniqueIdentifier = fcd.RealPropertyUniqueIdentifier	
	INNER JOIN MDI.AssetDetail ad ON a.Id = ad.AssetId
	LEFT OUTER JOIN AssetManagement.ExcessDetail ed ON ed.AssetId = a.Id
	LEFT OUTER JOIN Builder.BuilderConsolidatedDetails bcd ON bcd.RealPropertyUniqueIdentifier = a.RealPropertyUniqueIdentifier
	WHERE ad.ArchivedInd = 0

	UNION

	SELECT a.Id AS AssetId, a.RealPropertyUniqueIdentifier, fcd.PropertyId, fcd.PropertyName, fcd.AlternateName,  
		fcd.MissionDependentProgram, fcd.MissionDependency, fcd.MissionUniqueFacility, fcd.ReplacementPlantValue, 
		fcd.HazardCategory1, fcd.AnnualActualMaintenance, fcd.AnnualRequiredMaintenance, 
		fcd.DeferredMaintenance, fcd.OverallAssetCondition, bcd.BuildingConditionIndex, bcd.BuildingPerformanceIndex, 
		fcd.YearAcquired, fcd.AssetPercentUtilized AS PercentUtilization, 
		fcd.EstDispositionYr, fcd.AnticipatedDispositionMethod, fcd.CantCurrentlyBeDisposed, 
		fcd.RepairNeeds, fcd.FacilityConditionIndex, 
		CAST(CASE WHEN CAST(COALESCE(ed.ExcessToDOEDate, '12/31/9999 23:59:59.997') AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) THEN 1 ELSE 0 END AS BIT) AS ExcessInd, 
		fcd.[EstCleanupDispCost] AS EstCleanupDispCost, fcd.OperatingCost
	FROM G2.Asset a
	INNER JOIN FIMS.FIMSConsolidatedDetails fcd ON a.RealPropertyUniqueIdentifier = fcd.RealPropertyUniqueIdentifier
	INNER JOIN MDI.AssetDetail ad ON a.Id = ad.AssetId
	LEFT OUTER JOIN AssetManagement.ExcessDetail ed ON ed.AssetId = a.Id
	LEFT OUTER JOIN MDI.AssetAlias aa ON a.Id = aa.AssetId
	LEFT OUTER JOIN MDI.AssetContact aco ON a.Id = aco.AssetId
	LEFT OUTER JOIN AssetManagement.ImageXref ix ON a.Id = ix.AssetId
	LEFT OUTER JOIN MDI.AssetCapability ac ON a.Id = ac.AssetId
	LEFT OUTER JOIN MDI.SupportingAsset sa ON a.Id = sa.AssetId
	LEFT OUTER JOIN MDI.AssetDetailQuestionnaireAnswer adqa ON a.Id = adqa.AssetId
	LEFT OUTER JOIN Builder.BuilderConsolidatedDetails bcd ON bcd.RealPropertyUniqueIdentifier = a.RealPropertyUniqueIdentifier
	WHERE ad.ArchivedInd = 1
	AND COALESCE(ad.Description, CAST(COALESCE(ed.Id, aa.Id, aco.Id, ac.Id, sa.Id, ix.Id, adqa.Id) AS VARCHAR(800))) IS NOT NULL;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusIgnoreStates]'
GO

CREATE VIEW [G2].[vwChangeRequestDetailStatusIgnoreStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id = 5 --Ignore

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusIgnoreStates] on [G2].[vwChangeRequestDetailStatusIgnoreStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusIgnoreStates] ON [G2].[vwChangeRequestDetailStatusIgnoreStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SearchAndSecure].[vwPortfolioManagerList]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     10/23/2015
-- Description: Retrieves a concatenated list of portfolio managers.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE VIEW [SearchAndSecure].[vwPortfolioManagerList]
AS
    ---------------------------------------------------------------------------------------------------
    -- Get SS: Portfolio Manager (Primary/Secondary) by WBS.
    ---------------------------------------------------------------------------------------------------
    WITH PortfolioManagerByWBS_CTE AS (
        SELECT DISTINCT pr.SubSystemId, wbscx.CountryId, p.FirstName + ' ' + p.LastName AS Name
        FROM Security.PersonRole pr 
        INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.WorkBreakdownStructureId = pr.WorkBreakdownStructureId
        INNER JOIN Security.Person p ON p.Id = pr.PersonId
        WHERE pr.RoleId IN(157, 158) -- Security.fnGetSSPortfolioManagerPrimaryRoleId(), Security.fnGetSSPortfolioManagerSecondaryRoleId()
    ),
    ---------------------------------------------------------------------------------------------------
    -- Get SS: Portfolio Manager (Primary/Secondary) by Country.
    ---------------------------------------------------------------------------------------------------
    PortfolioManagersByCountry_CTE AS (
        SELECT DISTINCT pmbw.SubSystemId, pmbw.CountryId, (SELECT DISTINCT pmbw2.Name + ', ' FROM PortfolioManagerByWBS_CTE pmbw2 WHERE pmbw.CountryId = pmbw2.CountryId ORDER BY pmbw2.Name + ', ' FOR XML PATH('')) AS PortfolioManagers
        FROM PortfolioManagerByWBS_CTE pmbw
        GROUP BY pmbw.SubSystemId, pmbw.CountryId
    )
    ---------------------------------------------------------------------------------------------------
    -- Trim trailing comma from PortfolioManagers. 
    ---------------------------------------------------------------------------------------------------
    SELECT pmbc.SubSystemId, pmbc.CountryId, LEFT(pmbc.PortfolioManagers, (LEN(pmbc.PortfolioManagers) - 1)) AS PortfolioManagers
    FROM PortfolioManagersByCountry_CTE pmbc;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwSubSystemWorkflowActionRequired]'
GO


CREATE VIEW [G2].[vwSubSystemWorkflowActionRequired]
WITH SCHEMABINDING
AS
	SELECT permR.SubSystemId, permR.ChangeRequestTypeId, crt.ChangeRequestTypeGroupId, crt.Name AS ChangeRequestTypeName,
		permR.WorkflowLevel, permR.PermissionId, p.Name AS PermissionName, COUNT_BIG(*) AS CountBig
	FROM Security.PermissionRole permR
	INNER JOIN Security.Permission p ON permR.PermissionId = p.Id
	INNER JOIN G2.ChangeRequestType crt ON permR.ChangeRequestTypeId = crt.Id
	WHERE permR.PermissionId IN (27, 23, 15, 21) -- Security.fnGetSubmitPermissionId(), Security.fnGetConcurPermissionId(), Security.fnGetProcessPermissionId(), Security.fnGetMassProcessPermissionId()
	AND permR.WorkflowLevel IS NOT NULL
	GROUP BY permR.SubSystemId, permR.ChangeRequestTypeId, crt.ChangeRequestTypeGroupId, crt.Name, permR.WorkflowLevel, permR.PermissionId, p.Name;




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwSubSystemWorkflowActionRequired_SubSystemId_ChangeRequestTypeId_WorkflowLevel_PermissionId] on [G2].[vwSubSystemWorkflowActionRequired]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwSubSystemWorkflowActionRequired_SubSystemId_ChangeRequestTypeId_WorkflowLevel_PermissionId] ON [G2].[vwSubSystemWorkflowActionRequired] ([SubSystemId], [ChangeRequestTypeId], [WorkflowLevel], [PermissionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusActiveAndCancelledStates]'
GO


-- =============================================================
-- Author:      Chad Gilbert
-- Created:     09/8/2014
-- Release:     5.3
-- Description: Returns active and cancelled request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusActiveAndCancelledStates]
WITH SCHEMABINDING
AS
	SELECT wbss.Id AS WorkBreakdownStructureStatusId, wbss.Name AS WorkBreakdownStructureStatusName
	FROM G2.WorkBreakdownStructureStatus wbss
	INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
	INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
	WHERE wbsss.Id = 3; -- G2.fnGetActiveAndCancelledWorkBreakdownStructureStatusStateId


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusActiveAndCancelledStates] on [G2].[vwWorkBreakdownStructureStatusActiveAndCancelledStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusActiveAndCancelledStates] ON [G2].[vwWorkBreakdownStructureStatusActiveAndCancelledStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwMaterialAttractivenessLevelBySite]'
GO
CREATE VIEW [SPA].[vwMaterialAttractivenessLevelBySite]
AS
SELECT SPAMainId, CountryName, SiteId, SiteName, 
	MIN(MaterialAttractivenessLevelId) MaterialAttractivenessLevelId, 
	MIN(MaterialAttractivenessLevelName) MaterialAttractivenessLevelName
FROM SPA.vwMaterialAttractivenessLevelByBuilding
GROUP BY SPAMainId, CountryName, SiteId, SiteName
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[OrganizationTypeClassification]'
GO
CREATE VIEW [Contact].[OrganizationTypeClassification]
WITH SCHEMABINDING
AS
	SELECT child.PartyId AS OrganizationPartyId, child.OrganizationBreakdownStructure, ootx.OrganizationTypeId, ot.Name
	FROM Contact.OrganizationOrganizationTypeXref ootx
	INNER JOIN Contact.OrganizationType ot ON ootx.OrganizationTypeId = ot.Id
	INNER JOIN Contact.Organization o ON ootx.OrganizationPartyId = o.PartyId
	INNER JOIN Contact.Organization child ON child.OrganizationHierarchyId.IsDescendantOf(o.OrganizationHierarchyId) = 1;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwProjectManagerList]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     
-- Description: Retrieves a concatenated list of project
--   managers for each project.
--
-- mm/dd/yy  Name      Release  Description
-- 01/07/12  CGL       3.0      Changing PersonRoleWBS to the Security schema
-- 01/11/12  s.oneal   3.0      Modified to use the new security model
-- 06/05/14  j.borgers 5.2      Modified to use new tables
-- 09/19/14  s.oneal   5.3      Modified to use schedule data model; included
--                              NA-00 PE Schedule: Project Manager role
-- 09/22/15  a.smith   6.3      Modified to include equivalent roles for NA-21 GMS, NA-23, NA-50, & NA-193.
--                              Added SubSystemId as output column.
-- 10/27/15  s.oneal   6.3      Modified to remove duplicates from list of users with roles
-- =============================================================
CREATE VIEW [G2].[vwProjectManagerList]
AS
	WITH ProjectManagers
	AS
	(
		SELECT DISTINCT w.SubSystemId, w.Id AS WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Security.PersonRole pr
		INNER JOIN G2.WorkBreakdownStructure w ON pr.WorkBreakdownStructureId = w.Id
		INNER JOIN Security.Person p ON pr.PersonId = p.Id
		WHERE pr.RoleId IN (9, 100, 80) -- Security.fnGetProjectManagerRoleId(), Security.fnGetScheduleProjectManagerPrimaryRoleId(), FinPlan: Project Manager (Primary)
	),
	CombinedProjectManagers
	AS
	(
		SELECT pm.SubSystemId, pm.WorkBreakdownStructureId,
			(SELECT p.Name + ', ' FROM ProjectManagers p WHERE pm.WorkBreakdownStructureId = p.WorkBreakdownStructureId FOR XML PATH('')) AS ProjectManagers
		FROM ProjectManagers pm
		GROUP BY pm.SubSystemId, pm.WorkBreakdownStructureId
	)
	SELECT pm.SubSystemId, pm.WorkBreakdownStructureId, LEFT(pm.ProjectManagers, (LEN(pm.ProjectManagers) - 1)) AS ProjectManagers
	FROM CombinedProjectManagers pm;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[Annex1Lab]'
GO
/****** Object:  View [Contact].[Annex1Lab]    Script Date: 11/17/2016 2:04:55 PM ******/
CREATE VIEW [Contact].[Annex1Lab]
WITH SCHEMABINDING
AS
	SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, o.ActiveInd, pr.Id PartyRoleId
	FROM Contact.ReportingEntity re
	INNER JOIN Contact.Organization o ON re.OrganizationPartyId = o.PartyId
	INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId AND pr.PartyRoleTypeId = 17; -- Contact.fnGetAnnex1LabPartyRoleTypeId()
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_Annex1Lab] on [Contact].[Annex1Lab]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_Annex1Lab] ON [Contact].[Annex1Lab] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ui_Annex1Lab_SubSystemId_DisplayName] on [Contact].[Annex1Lab]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [ui_Annex1Lab_SubSystemId_DisplayName] ON [Contact].[Annex1Lab] ([SubSystemId], [DisplayName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[vwReportingEntityAndAnnex1Lab]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     06/15/2018
-- Release:     9.1
-- Description: Displays list of Reporting Entities unioned with Annex1 Labs
--
-- mm/dd/yy  Name     Release  Description
-- 07/11/18  n.coffey  9.1     Added ParentReportingEntityOrganizationPartyId
-- =============================================================
CREATE VIEW [Activity].[vwReportingEntityAndAnnex1Lab]
AS

	SELECT p.ReportingEntityOrganizationPartyId, p.SubSystemId, p.DisplayName, p.Name, p.FormalCode PerformerCode, p.FormalCode GroupCode, p.ActiveInd, NULL AS ParentReportingEntityOrganizationPartyId
	FROM Contact.Performer p

	UNION 

	SELECT A1L.ReportingEntityOrganizationPartyId, A1L.SubSystemId, A1L.DisplayName, A1L.Name, A1L.FormalCode, P.FormalCode, A1L.ActiveInd, P.ReportingEntityOrganizationPartyId AS ParentReportingEntityOrganizationPartyId
	FROM Contact.Annex1Lab A1L
	INNER JOIN Contact.PartyRelationship PRel ON PRel.FromPartyRoleId = A1L.PartyRoleId AND PRel.PartyRelationshipTypeId = 6 --Contact.fnGetAnnex1LabRollupPartyRelationshipTypeId()
	INNER JOIN Contact.PartyRole PRToPerformer ON PRToPerformer.Id = PRel.ToPartyRoleId
	INNER JOIN Contact.Performer P ON P.ReportingEntityOrganizationPartyId = PRToPerformer.PartyId AND P.SubSystemId = A1L.SubSystemId
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwMilestoneTemplatePlanningPackages]'
GO
CREATE VIEW [G2].[vwMilestoneTemplatePlanningPackages]
----WITH SCHEMABINDING
AS
	SELECT DISTINCT mt.Id AS MilestoneTemplateId, mt.Name AS MilestoneTemplateName
	FROM G2.MilestoneTemplate mt
	INNER JOIN G2.MilestoneTemplateDetail mtd ON mt.Id = mtd.MilestoneTemplateId
	WHERE mtd.ScopePlanningMilestoneInd = 1
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusFinalStates]'
GO

CREATE VIEW [G2].[vwChangeRequestDetailStatusFinalStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id = 2 --Closed


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusFinalStates] on [G2].[vwChangeRequestDetailStatusFinalStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusFinalStates] ON [G2].[vwChangeRequestDetailStatusFinalStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusActiveStatesAndRejectedStates]'
GO
CREATE VIEW [G2].[vwChangeRequestDetailStatusActiveStatesAndRejectedStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id IN (1, 6) -- (Active, Rejected)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusActiveStatesAndRejectedStates] on [G2].[vwChangeRequestDetailStatusActiveStatesAndRejectedStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusActiveStatesAndRejectedStates] ON [G2].[vwChangeRequestDetailStatusActiveStatesAndRejectedStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[vwAssetOperatingStatus]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     6/26/2018
-- Release:     9.1
-- Description: Returns the Operation Status ClassificationTypeId based 
--				on the assets values and another based on the requested values.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [AssetManagement].[vwAssetOperatingStatus]
AS

	SELECT a.Id AS AssetId,
		CASE 
			WHEN ad.ArchivedInd = 1 AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 15 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetArchivedClassificationTypeId()
			WHEN adqa.Id IS NOT NULL AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 14 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetHistoricUnableToDemolishClassificationTypeId()
			WHEN CAST(COALESCE(ed.ExcessToDOEDate, '12/31/9999 23:59:59.997') AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 13 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetExcessToDOEClassificationTypeId()
			WHEN (CAST(COALESCE(ed.ExcessToSiteDate, '12/31/9999 23:59:59.997') AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) OR (ad.PropertyTypeId = 2 AND fcd.[Status] = 'Inactive Land')) THEN 12 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetExcessToSiteClassificationTypeId()
			WHEN CAST(COALESCE(ed.StartRunToRetirementDate, '12/31/9999 23:59:59.997') AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 11 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetActiveDescisionMadeToRetireClassificationTypeId()
			ELSE 10 --AssetManagement.fnGetActiveFullOperationsClassificationTypeId()
		END AS ClassificationTypeId,
		CASE
			WHEN COALESCE(dr.ArchivedInd, ad.ArchivedInd) = 1 AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 15 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetArchivedClassificationTypeId()
			WHEN qar.Id IS NOT NULL AND ansR.Id IS NOT NULL AND qar.DeletedInd = 0 AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 14 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetHistoricUnableToDemolishClassificationTypeId()
			WHEN qar.Id IS NULL AND adqa.Id IS NOT NULL AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 14 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetHistoricUnableToDemolishClassificationTypeId()
			WHEN CAST(CASE 
						WHEN edr.Id IS NOT NULL THEN COALESCE(edr.ExcessToDOEDate, '12/31/9999 23:59:59.997') ELSE COALESCE(ed.ExcessToDOEDate, '12/31/9999 23:59:59.997') 
					  END AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 13 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetExcessToDOEClassificationTypeId()
			WHEN (CAST(CASE 
						WHEN edr.Id IS NOT NULL THEN COALESCE(edr.ExcessToSiteDate, '12/31/9999 23:59:59.997') ELSE COALESCE(ed.ExcessToSiteDate, '12/31/9999 23:59:59.997') 
					  END AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) OR (ad.PropertyTypeId = 2 AND fcd.[Status] = 'Inactive Land')) THEN 12 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetExcessToSiteClassificationTypeId()
			WHEN CAST(CASE 
						WHEN edr.Id IS NOT NULL THEN COALESCE(edr.StartRunToRetirementDate, '12/31/9999 23:59:59.997') ELSE COALESCE(ed.StartRunToRetirementDate, '12/31/9999 23:59:59.997') 
					  END AS DATE) < CAST(CURRENT_TIMESTAMP AS DATE) AND COALESCE(ad.PropertyTypeId, -1) <> 2 THEN 11 --G2.fnGetLandPropertyTypeId(), AssetManagement.fnGetActiveDescisionMadeToRetireClassificationTypeId()
			ELSE 10 --AssetManagement.fnGetActiveFullOperationsClassificationTypeId()
		END AS ClassificationTypeRequestId
	FROM G2.Asset a
	INNER JOIN FIMS.FIMSConsolidatedDetails fcd ON fcd.RealPropertyUniqueIdentifier = a.RealPropertyUniqueIdentifier
	INNER JOIN MDI.AssetDetail ad ON ad.AssetId = a.Id
	LEFT OUTER JOIN AssetManagement.ExcessDetail ed ON ed.AssetId = a.Id
	LEFT OUTER JOIN MDI.AssetDetailQuestionnaireAnswer adqa 
		INNER JOIN Inquiry.QuestionnaireQuestion qq ON qq.Id = adqa.QuestionnaireQuestionId
		INNER JOIN Inquiry.Question q ON q.Id = qq.QuestionId AND q.[Text] = 'Can it be demolished?'
		INNER JOIN Inquiry.Answer ans ON ans.Id = adqa.AnswerId AND ans.[Text] = 'No'
	ON adqa.AssetId = a.Id
	LEFT OUTER JOIN AssetManagement.Request r 
		INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = r.ChangeRequestDetailId
		INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH (NOEXPAND) ON acrs.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
	ON r.AssetId = a.Id
	LEFT OUTER JOIN AssetManagement.DetailRequest dr ON r.Id = dr.RequestId
	LEFT OUTER JOIN AssetManagement.ExcessDetailRequest edr ON edr.RequestId = r.Id
	LEFT OUTER JOIN AssetManagement.QuestionnaireAnswerRequest qar 
		INNER JOIN Inquiry.QuestionnaireQuestion qqR ON qqR.Id = qar.QuestionnaireQuestionId
		INNER JOIN Inquiry.Question qR ON qR.Id = qqR.QuestionId AND qR.[Text] = 'Can it be demolished?'
	ON qar.RequestId = r.Id
	LEFT OUTER JOIN Inquiry.Answer ansR ON ansR.Id = qar.AnswerId AND ansR.[Text] = 'No'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwSPAMainSectionFileUploads]'
GO

	CREATE VIEW [SPA].[vwSPAMainSectionFileUploads]
	AS
	 
	SELECT sms.Id, sms.SPAMainId, sms.SPASectionTypeId, sms.EntityTypeId, sms.RefId, sms.IsIncludedInd, sms.IsCompleteInd,
		f.EntityTypeId FileUploadEntityTypeId, f.RefId FileUploadRefId,
		f.Id FileUploadId, f.Title FileUploadTitle, f.NameUnique FileUploadNameUnique,
		ft.Id FileUploadTypeId, ft.[Name] FileUploadTypeIdName,
		f.Title + ' (' + ft.[Name] + ')' Label,
		n.Note 
	FROM SPA.SPAMainSection sms
	INNER JOIN Inventory.FileUpload f ON f.Id = sms.RefId
		AND sms.SPASectionTypeId IN (2,6,9,19,20)
	INNER JOIN Inventory.FileUploadType ft ON ft.Id = f.FileUploadTypeId
	LEFT JOIN Inventory.Note n ON n.Id = f.NoteId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[BudgetPerformer]'
GO

CREATE VIEW [Contact].[BudgetPerformer]
WITH SCHEMABINDING
AS
SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
WHERE pr.PartyRoleTypeId = 11 -- Contact.fnGetBudgetPerformerPartyRoleTypeId()
AND pty.DeletedInd = 0

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_BudgetPerformer] on [Contact].[BudgetPerformer]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_BudgetPerformer] ON [Contact].[BudgetPerformer] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[vwRadProtectImplementationTasks]'
GO




CREATE VIEW [Inventory].[vwRadProtectImplementationTasks]
AS
SELECT 
	s.Id SiteId, s.Name SiteName,
	b.Id BuildingId, b.Name BuildingName,
	wbsProject.Id ProjectId, wbsProject.Name ProjectName,
	--psf.Id ProjectSubFunctionId, psf.LongName ProjectSubFunctionName,
	--t.Id TaskId, t.Name TaskName,
	wt.Id WorkTypeId, wt.LongName WorkTypeName,
	wbsTask.Id TaskId, wbsTask.Name TaskName,
	mt.Id MilestoneTemplateId, mt.Name MilestoneTemplateName,
	mtc.Id MilestoneTemplateCategoryId, mtc.Name MilestoneTemplateCategoryName,
	--COALESCE(pmConductSiteAssessment.CompletedInd, CAST(0 AS BIT)) ConductSiteAssessmentCompletedInd,
	COALESCE(pmConductSiteAssessment.CompletedInd, COALESCE(pmAwardContract.CompletedInd, CAST(0 AS BIT))) ConductSiteAssessmentCompletedInd,
	COALESCE(pmAwardContract.CompletedInd, CAST(0 AS BIT)) AwardContractCompletedInd,
	COALESCE(pmBuildingSecure.CompletedInd, CAST(0 AS BIT)) BuildingSecureCompletedInd
FROM G2.Site s 
INNER JOIN G2.Building b ON b.SiteId = s.Id
	AND b.DeletedInd = 0
INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
INNER JOIN G2.Task t ON t.Id = tb.TaskId
--	AND t.DeletedInd = 0 -- Not deleted
--	AND t.CancelledInd = 0 -- Not cancelled
INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsTask.Id = t.WorkBreakdownStructureId
INNER JOIN G2.vwWorkBreakdownStructureStatusActiveStates wbsActiveStates ON wbsActiveStates.WorkBreakdownStructureStatusId = wbsTask.StatusId

--INNER JOIN G2.Schedule p ON p.Id = t.ProjectId
--INNER JOIN G2.WorkType psf ON psf.Id = p.ProjectSubfunctionId
INNER JOIN G2.WorkBreakdownStructure wbsProject ON wbsProject.Id = wbsTask.ParentId
INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbsWorkTypeXref ON wbsWorkTypeXref.WorkBreakdownStructureId = wbsProject.Id
INNER JOIN G2.WorkType wt ON wt.Id = wbsWorkTypeXref.WorkTypeId
	AND wt.Id IN (33,34,35,36,38) -- Protect work types

INNER JOIN G2.MilestoneTemplate mt ON mt.Id = t.MilestoneTemplateId
INNER JOIN G2.MilestoneTemplateCategory mtc ON mtc.Id = mt.MilestoneTemplateCategoryId
LEFT JOIN G2.ProjectMilestone pmConductSiteAssessment ON pmConductSiteAssessment.TaskId = t.Id
	AND pmConductSiteAssessment.MilestoneId = 599
LEFT JOIN G2.ProjectMilestone pmAwardContract ON pmAwardContract.TaskId = t.Id
	AND pmAwardContract.MilestoneId = 595
LEFT JOIN G2.ProjectMilestone pmBuildingSecure ON pmBuildingSecure.TaskId = t.Id
	AND pmBuildingSecure.MilestoneId = 596
WHERE s.DeletedInd = 0
AND mtc.Id = 11 -- Implementation
GROUP BY
	s.Id, s.Name,
	b.Id, b.Name,
	wbsProject.Id, wbsProject.Name,
	wt.Id, wt.LongName,
	wbsTask.Id, wbsTask.Name,
	mt.Id, mt.Name,
	mtc.Id, mtc.Name,
	pmConductSiteAssessment.CompletedInd,
	pmAwardContract.CompletedInd,
	pmAwardContract.CompletedInd,
	pmBuildingSecure.CompletedInd




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwSearchWBS]'
GO
--WBS view and indexes
CREATE VIEW [G2].[vwSearchWBS]
WITH SCHEMABINDING  
AS 
	SELECT wbs.Id, wbs.FullWBS, wbs.[Name]
	FROM G2.WorkBreakdownStructure wbs
	INNER JOIN G2.SubSystem ss ON wbs.SubSystemId = ss.Id
	WHERE ss.ActiveInd = 1;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_vwSearchWBS] on [G2].[vwSearchWBS]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_vwSearchWBS] ON [G2].[vwSearchWBS] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[vwActiveAccountsByFiscalYear]'
GO

CREATE VIEW [Financial].[vwActiveAccountsByFiscalYear]
WITH SCHEMABINDING
AS
	SELECT a.Id AS AccountId, COALESCE(at.FiscalYear, priorYear.FiscalYear) AS FiscalYear, COALESCE(at.MinimumFiscalPeriod, priorYear.MinimumFiscalPeriod) AS MinimumFiscalPeriod
	FROM Financial.Account a
	CROSS APPLY (
		SELECT at.AccountId, MIN(atPer.FiscalYear) AS StartingFiscalYear
		FROM Financial.AccountTransaction at
		INNER JOIN G2.Period atPer ON at.PeriodId = atPer.Id
		WHERE at.AccountId = a.Id
		GROUP BY at.AccountId
	) startingFiscalYear
	INNER JOIN G2.FiscalYear fiscalYears ON fiscalYears.FiscalYear >= startingFiscalYear.StartingFiscalYear 
	OUTER APPLY 
	(
		SELECT at.AccountId, p.FiscalYear, MIN(p.FiscalPeriod) AS MinimumFiscalPeriod
		FROM Financial.AccountTransaction at 
		INNER JOIN G2.Period p ON at.PeriodId = p.Id
		WHERE a.Id = at.AccountId AND p.FiscalYear = fiscalYears.FiscalYear
		GROUP BY at.AccountId, p.FiscalYear
	) at
	OUTER APPLY 
	(
		SELECT ptft.AccountId, ptft.FiscalYear AS FiscalYear, 1 AS MinimumFiscalPeriod 
		FROM Financial.ProjectTranFYTotal ptft 
		WHERE ptft.AccountId = a.Id AND ptft.FiscalYear = fiscalYears.FiscalYear
		GROUP BY ptft.AccountId, ptft.FiscalYear
		HAVING SUM(ptft.PriorFyCarryOver) <> 0
	) priorYear
	WHERE (at.AccountId IS NOT NULL OR priorYear.AccountId IS NOT NULL)
	GROUP BY a.Id, COALESCE(at.FiscalYear, priorYear.FiscalYear), COALESCE(at.MinimumFiscalPeriod, priorYear.MinimumFiscalPeriod)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwCongressionalJouleWorkTypeMetrics]'
GO

/**************INSERT SCRIPT HERE****************/
	CREATE VIEW [G2].[vwCongressionalJouleWorkTypeMetrics]
	WITH SCHEMABINDING
	AS
		SELECT r.Id AS RollupId, r.Label AS RollupLabel, r.SubSystemId, WorkTypeId, MetricId
		FROM G2.Rollup r
		INNER JOIN G2.RollupWorkTypeXref rp ON r.Id = rp.RollupId	
		INNER JOIN G2.MetricRollup mr ON r.Id = mr.RollupId
		WHERE r.Type = 'Congressional Joule';

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwCongressionalJouleWorkTypeMetrics] on [G2].[vwCongressionalJouleWorkTypeMetrics]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwCongressionalJouleWorkTypeMetrics] ON [G2].[vwCongressionalJouleWorkTypeMetrics] ([SubSystemId], [WorkTypeId], [MetricId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[vwSearchSolicitationAttachment]'
GO
--Solicitation-attachment view and indexes
CREATE VIEW [Proposal].[vwSearchSolicitationAttachment]
WITH SCHEMABINDING  
AS
	--sol attachemnt
	SELECT a.Id, a.[Name], COUNT_BIG(*) AS [RowCount]
	FROM Proposal.Solicitation s 
	INNER JOIN Proposal.SolicitationAttachmentXref x ON s.Id = x.SolicitationId
	INNER JOIN G2.Attachment a ON x.AttachmentId = a.Id
	GROUP BY a.Id, a.[Name];
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_vwSearchSolicitationAttachment] on [Proposal].[vwSearchSolicitationAttachment]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_vwSearchSolicitationAttachment] ON [Proposal].[vwSearchSolicitationAttachment] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[vwSMPPerformer]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/15/2016
-- Release:     7.4
-- Description: Retrieves the list of valid performers to be
--   used in SMP.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [Contact].[vwSMPPerformer]
WITH SCHEMABINDING
AS
	SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd
	FROM Contact.Organization o
	INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
	INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
	INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
	WHERE pr.PartyRoleTypeId = 16 -- Contact.fnGetSMPPerformerPartyRoleTypeId()
	AND pty.DeletedInd = 0;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_vwSMPPerformer] on [Contact].[vwSMPPerformer]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_vwSMPPerformer] ON [Contact].[vwSMPPerformer] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwSubProgramManagerList]'
GO
-- =============================================================
-- Author:      Mike Strickland/Nevada Williford
-- Created:     3/29/2016
-- Description: Retrieves a concatenated list of SubProgram managers.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE VIEW [SPA].[vwSubProgramManagerList]
AS
    ---------------------------------------------------------------------------------------------------
    -- Get SPA: SubProgram Manager (Primary) by WBS.
    ---------------------------------------------------------------------------------------------------
    WITH SubProgramManagerByWBS_CTE AS (
        SELECT DISTINCT pr.SubSystemId, wbscx.CountryId, p.FirstName + ' ' + p.LastName AS Name
        FROM Security.PersonRole pr 
        INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.WorkBreakdownStructureId = pr.WorkBreakdownStructureId
        INNER JOIN Security.Person p ON p.Id = pr.PersonId
        WHERE pr.RoleId IN (130) 
    ),
    ---------------------------------------------------------------------------------------------------
    -- Get SPA: SubProgram Manager (Primary) by Country.
    ---------------------------------------------------------------------------------------------------
    SubProgramManagersByCountry_CTE AS (
        SELECT DISTINCT spmbw.SubSystemId, spmbw.CountryId, (SELECT DISTINCT pmbw2.Name + ', ' FROM SubProgramManagerByWBS_CTE pmbw2 WHERE spmbw.CountryId = pmbw2.CountryId ORDER BY pmbw2.Name + ', ' FOR XML PATH('')) AS SubProgramManagers
        FROM SubProgramManagerByWBS_CTE spmbw
        GROUP BY spmbw.SubSystemId, spmbw.CountryId
    )
    ---------------------------------------------------------------------------------------------------
    -- Trim trailing comma from SubProgramManagers. 
    ---------------------------------------------------------------------------------------------------
    SELECT spmbyc.SubSystemId, spmbyc.CountryId, LEFT(spmbyc.SubProgramManagers, (LEN(spmbyc.SubProgramManagers) - 1)) AS SubProgramManagers
    FROM SubProgramManagersByCountry_CTE spmbyc;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[vwAssessmentHierarchy]'
GO


-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/12/2016
-- Release:     7.3
-- Description: Created to eliminate heirarchyId so it can be used by NS over linked servers
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [Assessment].[vwAssessmentHierarchy]
AS
    SELECT ah.Id, ah.SubSystemId, ah.ParentId, ah.AssessmentHierarchyLevelId, ah.Reference, ah.Text, ah.LeafInd, ah.CreatedDt, ah.CreatedBy, ah.CreatedAs, ah.LastModifiedDt, ah.LastModifiedBy, ah.LastModifiedAs
	FROM Assessment.AssessmentHierarchy ah;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwPortfolioManagerList]'
GO
-- =============================================================
-- Author:      Mike Strickland/Nevada Williford
-- Created:     3/29/2016
-- Description: Retrieves a concatenated list of portfolio managers.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE VIEW [SPA].[vwPortfolioManagerList]
AS
    ---------------------------------------------------------------------------------------------------
    -- Get SPA: Portfolio Manager (Primary) by WBS.
    ---------------------------------------------------------------------------------------------------
    WITH PortfolioManagerByWBS_CTE AS (
        SELECT DISTINCT pr.SubSystemId, wbscx.CountryId, p.FirstName + ' ' + p.LastName AS Name
        FROM Security.PersonRole pr 
        INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.WorkBreakdownStructureId = pr.WorkBreakdownStructureId
        INNER JOIN Security.Person p ON p.Id = pr.PersonId
        WHERE pr.RoleId IN (128) 
    ),
    ---------------------------------------------------------------------------------------------------
    -- Get SPA: Portfolio Manager (Primary) by Country.
    ---------------------------------------------------------------------------------------------------
    PortfolioManagersByCountry_CTE AS (
        SELECT DISTINCT pmbw.SubSystemId, pmbw.CountryId, (SELECT DISTINCT pmbw2.Name + ', ' FROM PortfolioManagerByWBS_CTE pmbw2 WHERE pmbw.CountryId = pmbw2.CountryId ORDER BY pmbw2.Name + ', ' FOR XML PATH('')) AS PortfolioManagers
        FROM PortfolioManagerByWBS_CTE pmbw
        GROUP BY pmbw.SubSystemId, pmbw.CountryId
    )
    ---------------------------------------------------------------------------------------------------
    -- Trim trailing comma from PortfolioManagers. 
    ---------------------------------------------------------------------------------------------------
    SELECT pmbc.SubSystemId, pmbc.CountryId, LEFT(pmbc.PortfolioManagers, (LEN(pmbc.PortfolioManagers) - 1)) AS PortfolioManagers
    FROM PortfolioManagersByCountry_CTE pmbc;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwProposedProjectPlanningScenarioDataRebuild]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     12/13/2017
-- Release:     8.4
-- Description: Returns Proposed Project Planning Scenario data for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [Geo].[vwProposedProjectPlanningScenarioDataRebuild]
AS
    SELECT wbscpd.WorkBreakdownStructureChangeId, ps.Name AS PlanningScenario 
    FROM G2.WorkBreakdownStructureChange wbsc
    INNER JOIN G2.WorkBreakdownStructureChangePlanDetail wbscpd ON wbscpd.WorkBreakdownStructureChangeId = wbsc.Id
    INNER JOIN G2.ChangeRequestDetail crd ON wbsc.ChangeRequestDetailId = crd.Id
    INNER JOIN G2.ChangeRequestDetailStatus crds ON crd.ChangeRequestDetailStatusId = crds.Id
    INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
    INNER JOIN G2.vwChangeRequestDetailStatusActiveStatesAndProcessedStates vcrdsas WITH (NOEXPAND) ON vcrdsas.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
    INNER JOIN G2.FundingProgram nfpi ON wbscpd.FundingProgramId = nfpi.Id AND cr.SubSystemId = nfpi.SubSystemId
    INNER JOIN G2.WorkBreakdownStructureChangePlanningScenarioRank wbscpsr ON wbscpd.Id = wbscpsr.WorkBreakdownStructureChangePlanDetailId
    INNER JOIN G2.PlanningScenarioRankGroup psrg ON wbscpsr.PlanningScenarioRankGroupId = psrg.Id
    INNER JOIN G2.PlanningScenario ps ON psrg.PlanningScenarioId = ps.Id
    WHERE crd.ChangeRequestTypeId = G2.fnGetWorkBreakdownStructurePlanChangeRequestTypeId()
    AND cr.SubSystemId = G2.fnGetNA50PESubSystemId()
    AND nfpi.ShortName = 'NA-50'
    GROUP BY wbscpd.WorkBreakdownStructureChangeId, ps.Name;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[vwSearchRequestAttachment]'
GO
--Request-attachment view and indexes
CREATE VIEW [Proposal].[vwSearchRequestAttachment]
WITH SCHEMABINDING  
AS
	--req attachment
	SELECT a.Id, a.[Name], COUNT_BIG(*) AS [RowCount]
	FROM Proposal.Request r
	INNER JOIN Proposal.RequestAttachmentXref x ON r.Id = x.RequestId
	INNER JOIN G2.Attachment a ON x.AttachmentId = a.Id
	GROUP BY a.Id, a.[Name];
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_vwSearchRequestAttachment] on [Proposal].[vwSearchRequestAttachment]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_vwSearchRequestAttachment] ON [Proposal].[vwSearchRequestAttachment] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwTaskDataRebuild]'
GO


-- =============================================================
-- Author:      Mark Brown
-- Created:     06/04/2013
-- Release:     4.2
-- Description: Returns Task date for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- 03/05/14  j.frank  5.0      Updated some names and inner queries to support changing "country officer" to "portfolio officer".
-- 08/11/14  DNC      5.2      Smart Rename appeared to have been used to switch to LabId to ReportingEntityOrganizationPartyId; the join on the
--                             G2.Lab table was joining Lab.Id to Task.ReportingEntityOrganizationPartyId.  This was been corrected, but note
--                             that the G2.Lab table will need to be replaced with Contact.Performer and LabId removed entirely and replaced
--                             with ReportingEntityOrganizationPartyId in the next release.  This change will ripple through the views, webservices,
--                             geo tables, exports and GIS Analysis Dashboard.
-- 11/07/14  m.brown    5.3    Updated to accommodate schema changes in 5.3
-- 09/28/15  CGL        6.3    Added SubSystemId to support migration/split
-- 10/14/15  CGL        6.3    Modified for latest column inclusion decissions and renames
-- 10/19/15  CGL        6.3    Refactored the Portfolio and Project Manager fields to use the same logic as the Project Layer
-- 10/29/15  CGL        6.3    Changed from Performer view to LeadLab view
-- 10/30/15  CGL        6.3    Switched back to Performer for now due to truncation error in rebuild and mapping push
-- 11/11/15  CGL        6.3    Switched back to LeadLab view, Modified for new PM Date Columns, include Country from WBSXref table, changed TaskStatus to use normal WBS Status values
-- 11/12/15  CGL        6.3    Modified where the TaskStatus values are calculated from to avoid the data descrepancy issues Jeff saw in US4254
-- 11/18/16  DNC        7.4    Add PrimarySite per US6695
-- 05/17/17  DGP        8.0    Using Name instead of LongName for WorkType
-- =============================================================
CREATE VIEW [Geo].[vwTaskDataRebuild]
AS
SELECT	t.Id, taskWBS.SubSystemId,
		Geometry::STGeomFromText('POINT(' + CONVERT(VARCHAR(20), COALESCE(b.Longitude, taskSite.Longitude, 0)) + ' '
									+ CONVERT(VARCHAR(20), COALESCE(b.Latitude, taskSite.Latitude, 0)) + '  )', 4326) AS Geometry,
		CAST(CASE	WHEN COALESCE(b.Longitude, taskSite.Longitude, 0) = 0
							AND COALESCE(b.Latitude, taskSite.Latitude, 0) = 0 THEN 0
					ELSE 1
				END AS BIT) AS GeometryValid, WBSG.PrimaryGroupSortableWBS, WBSG.PrimaryGroupFullWBS,
		WBSG.PrimaryGroupId, WBSG.PrimaryGroupName, WBSG.SecondaryGroupSortableWBS, WBSG.SecondaryGroupFullWBS,
		WBSG.SecondaryGroupId, WBSG.SecondaryGroupName, c.Id AS CountryId, c.LongName AS CountryName, r.Id AS RegionId,
		r.Name AS RegionName, st.Id AS StateId, st.Name AS StateName, wt.Id AS WorkTypeId, CAST(wt.Name AS NVARCHAR(100)) AS WorkType,
		WTG.Name AS WorkTypeGroup, tat.Id AS TaskAttributeId, tat.Name AS TaskAttribute, mtc.Id AS TaskCategoryId,
		mtc.Name AS TaskCategoryName, mt.Id AS MilestoneTemplateId, mt.Name AS MilestoneTemplateName,
		CASE	WHEN taskWBS.CompletedInd = 1 THEN 'Complete'
				WHEN Sch.StartDt > CURRENT_TIMESTAMP THEN 'Not Started'
				ELSE 'In Progress'
		END AS TaskStatus, taskWBS.Name AS Name, PMMetrics.BaselineMetricMilestoneDt, PMMetrics.BaselineStartDt,
		PMMetrics.BaselineEndDt, PMMetrics.ForecastMetricMilestoneDt, PMMetrics.ForecastStartDt, PMMetrics.ForecastEndDt,
		projWBS.Name AS ProjectName, projWBS.FullWBS AS ProjectWBS, metrics.MetricAmount AS MetricAmount,
		LL.ReportingEntityOrganizationPartyId AS LeadPerformerId, LL.FormalCode AS LeadPerformer,
		PortfolioManagerList.PortfolioManagerIdList, PortfolioManagerList.PortfolioManagerList,
		ProjectManagerList.ProjectManagerIdList, ProjectManagerList.ProjectManagerList, CAST(wbsa.Value AS VARCHAR(MAX)) PrimarySite
FROM	G2.Task t
INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
INNER JOIN G2.Schedule Sch ON Sch.WorkBreakdownStructureId = taskWBS.Id
INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwac ON vwac.WorkBreakdownStructureStatusId = taskWBS.StatusId
LEFT OUTER JOIN G2.TaskAttribute ta
INNER JOIN G2.TaskAttributeType tat ON ta.TaskAttributeTypeId = tat.Id ON ta.TaskId = t.Id
INNER JOIN G2.MilestoneTemplate mt ON t.MilestoneTemplateId = mt.Id
LEFT OUTER JOIN G2.MilestoneTemplateCategory mtc ON mt.MilestoneTemplateCategoryId = mtc.Id
OUTER APPLY (
				SELECT	MAX(CASE	WHEN PMM.Id IS NOT NULL THEN PM.FinishDt
									ELSE NULL
							END) ForecastMetricMilestoneDt,
						MIN(PM.FinishDt) ForecastStartDt,
						MAX(PM.FinishDt) ForecastEndDt,
						MAX(CASE	WHEN PMM.Id IS NOT NULL THEN PM.BaselineFinishDt
									ELSE NULL
							END) BaselineMetricMilestoneDt,
						MIN(PM.BaselineFinishDt) BaselineStartDt,
						MAX(PM.BaselineFinishDt) BaselineEndDt
				FROM	G2.ProjectMilestone PM
				LEFT OUTER JOIN G2.ProjectMilestoneMetric PMM ON PMM.ProjectMilestoneId = PM.Id
				WHERE	t.Id = PM.TaskId
						AND PM.FinishDt <> CONVERT(DATETIME, '1/1/1900')
						AND PM.BaselineFinishDt <> CONVERT(DATETIME, '1/1/1900')
			) PMMetrics
INNER JOIN G2.WorkBreakdownStructure projWBS ON taskWBS.ParentId = projWBS.Id
INNER JOIN G2.WorkBreakdownStructureGroups WBSG ON WBSG.WorkBreakdownStructureId = projWBS.Id
INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkBreakdownStructureId = projWBS.Id
INNER JOIN G2.WorkType wt ON wt.Id = wbswtx.WorkTypeId
INNER JOIN G2.WorkTypeGroup WTG ON WTG.Id = wt.WorkTypeGroupId
LEFT OUTER JOIN Contact.LeadLab LL ON t.LeadReportingEntityOrganizationPartyId = LL.ReportingEntityOrganizationPartyId
										AND LL.SubSystemId = taskWBS.SubSystemId
LEFT OUTER JOIN G2.TaskDevice td
INNER JOIN Inventory.Source s ON td.SourceId = s.Id
INNER JOIN Inventory.Room room ON s.RoomId = room.Id ON td.TaskId = t.Id
LEFT OUTER JOIN G2.WorkBreakdownStructureAttribute wbsa
				ON projWBS.Id = wbsa.WorkBreakdownStructureId AND wbsa.WorkBreakdownStructureAttributeTypeId = 31 --G2.fnGetPrimarySiteWorkBreakdownStructureAttributeTypeId()
LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
LEFT OUTER JOIN G2.Building b ON COALESCE(tb.BuildingId, room.BuildingId) = b.Id
LEFT OUTER JOIN G2.Site buildingSite ON b.SiteId = buildingSite.Id
LEFT OUTER JOIN G2.TaskSite ts ON t.Id = ts.TaskId
LEFT OUTER JOIN G2.Site taskSite ON ts.SiteId = taskSite.Id
LEFT OUTER JOIN G2.WorkBreakdownStructureCountryXref WBSCX ON WBSCX.WorkBreakdownStructureId = projWBS.Id
LEFT OUTER JOIN G2.Country c ON COALESCE(buildingSite.CountryId, taskSite.CountryId, WBSCX.CountryId) = c.Id
LEFT OUTER JOIN G2.State st ON COALESCE(buildingSite.StateId, taskSite.StateId) = st.Id
LEFT OUTER JOIN Inventory.RegionStateXRef rsxr ON st.Id = rsxr.StateId
LEFT OUTER JOIN Inventory.Region r ON rsxr.RegionId = r.Id
LEFT OUTER JOIN (
					SELECT	t.Id AS TaskId, MIN(CAST(pm.CompletedInd AS INT)) AS MinCompletedInd,
							MAX(CAST(pm.CompletedInd AS INT)) AS MaxCompletedInd
					FROM	G2.Task t
					INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
					GROUP BY t.Id
				) upgradeStatusQuery ON upgradeStatusQuery.TaskId = t.Id
LEFT OUTER JOIN (
					SELECT	t.Id, SUM(pmm.ForecastValue) AS MetricAmount
					FROM	G2.Task t
					INNER JOIN G2.WorkBreakdownStructure taskWBS ON t.WorkBreakdownStructureId = taskWBS.Id
					INNER JOIN G2.WorkBreakdownStructure projWBS ON projWBS.Id = taskWBS.ParentId
					INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkBreakdownStructureId = projWBS.Id
					INNER JOIN G2.vwCongressionalJouleWorkTypeMetrics vcjpsm ON vcjpsm.WorkTypeId = wbswtx.WorkTypeId
					INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
															AND pm.JouleMetricInd = 1
					INNER JOIN G2.ProjectMilestoneMetric pmm ON pm.Id = pmm.ProjectMilestoneId
																AND pmm.MetricId = vcjpsm.MetricId
					GROUP BY t.Id
				) metrics ON t.Id = metrics.Id
OUTER APPLY (
				SELECT	SUBSTRING((
									SELECT	', ' + PortfolioManager
									FROM	(
												SELECT    DISTINCT
														wbsInt.Id AS WBSId,
														pers.FirstName + ' ' + pers.LastName AS PortfolioManager
												FROM	G2.WorkBreakdownStructure wbsInt
												INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
																										AND persR.WorkBreakdownStructureId = wbsInt.Id
												INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
												WHERE	persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
												
											) s
									WHERE	projWBS.Id = s.WBSId
									FOR
									XML	PATH('')
									), 3, 2000) AS PortfolioManagerList,
						SUBSTRING((
									SELECT	', ' + PortfolioManagerId
									FROM	(
												SELECT    DISTINCT
														wbsInt.Id AS WBSId,
														CAST(pers.Id AS VARCHAR) AS PortfolioManagerId
												FROM	G2.WorkBreakdownStructure wbsInt
												INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
																										AND persR.WorkBreakdownStructureId = wbsInt.Id
												INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
												WHERE	persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
												
											) s
									WHERE	projWBS.Id = s.WBSId
									FOR
									XML	PATH('')
									), 3, 2000) AS PortfolioManagerIdList
			) PortfolioManagerList
OUTER APPLY (
				SELECT	SUBSTRING((
									SELECT	', ' + ProjectManager
									FROM	(
												SELECT	wbsInt.Id AS WBSId,
														pers.FirstName + ' ' + pers.LastName AS ProjectManager
												FROM	G2.WorkBreakdownStructure wbsInt
												INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
																						AND persR.WorkBreakdownStructureId = wbsInt.Id
												INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
												WHERE	persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
												
											) s
									WHERE	projWBS.Id = s.WBSId
									FOR
									XML	PATH('')
									), 3, 2000) AS ProjectManagerList,
						SUBSTRING((
									SELECT	', ' + ProjectManagerId
									FROM	(
												SELECT	wbsInt.Id AS WBSId,
														CAST(pers.Id AS VARCHAR) AS ProjectManagerId
												FROM	G2.WorkBreakdownStructure wbsInt
												INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
																						AND persR.WorkBreakdownStructureId = wbsInt.Id
												INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
												WHERE	persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
												
											) s
									WHERE	projWBS.Id = s.WBSId
									FOR
									XML	PATH('')
									), 3, 2000) AS ProjectManagerIdList
			) ProjectManagerList;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[vwInfrastructurePlanningSite]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     6/1/2016
-- Release:     7.1
-- Description: View defining Infrastructure Planning Sites
--
-- mm/dd/yy  Name      Release  Description
-- 06/08/16  j.borgers 7.1      Added SubSystemId to output
-- =============================================================
CREATE VIEW [Contact].[vwInfrastructurePlanningSite]
WITH SCHEMABINDING
AS
	SELECT pr.SubSystemId, o.PartyId, o.OrganizationBreakdownStructure, o.Name, o.ParentId, o.SortOrder, pr.Id PartyRoleId, pr.PartyRoleTypeId
	FROM Contact.Organization o
	INNER JOIN Contact.Party p ON o.PartyId = p.Id
	INNER JOIN Contact.PartyRole pr ON pr.PartyId = o.PartyId
	WHERE pr.PartyRoleTypeId = 15 --Contact.fnGetInfrastructurePlanningSitePartyRoleTypeId()
	AND p.DeletedInd = 0

WITH CHECK OPTION;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwProposedProjectDataRebuild]'
GO

-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     2/16/2017
-- Release:     7.5
-- Description: Returns Proposed Project data for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- 03/01/17  DNC       7.5     Add RealPropertyUniqueIdentifier as CSV List
-- 03/07/17  DNC       7.5     Subtract 1 from StartDate calcs so that 
--                             they are in correct FiscalYear
-- 05/08/07  DGP       8.0     Casting WorkType Name to NVARCHAR(100)
-- 12/13/17  j.borgers 8.4     Removed PlanningScenario and reformatted
-- 03/13/18  d.nickum  8.5     Change date casts from DATE to DATETIME
-- =============================================================
CREATE VIEW [Geo].[vwProposedProjectDataRebuild]
AS
    SELECT wtg.SubSystemId, wbsc.Id AS WorkBreakdownStructureChangeId, wbsc.Name AS WBSName, 'Not Assigned' AS FullWBS,
        NULL AS SortableWBS, 'Proposed' AS Status, crds.Name AS PlanningStatus, 
        CAST(wt.Name AS NVARCHAR(100)) AS WorkType, wtg.Name AS WorkTypeGroup, NULL AS Country, NULL AS State, NULL AS PrimaryGroupSortableWBS,
        'Not Assigned' AS PrimaryGroupFullWBS, NULL AS PrimaryGroupId, wbspg.Name AS PrimaryGroupName,
        NULL AS SecondaryGroupSortableWBS, NULL AS SecondaryGroupFullWBS, NULL AS SecondaryGroupId,
        'Infrastructure Planning Module' AS SecondaryGroupName, G2.fnGetCurrentFiscalYear (G2.fnGetNA50PESubSystemId()) AS FiscalYear, bt.TotalProjectCost AS Budget,
        0.00 AS Carryover, 0.00 AS Funding, 0.00 AS TotalFunding, 0.00 AS Costs, 0.00 AS Commitments,
        0.00 AS AvailableFunding, NULL AS PortfolioManagerIdList, '' AS PortfolioManagerList,
        NULL AS ProjectManagerIdList, '' AS ProjectManagerList, CAST(NULL AS DATETIME) AS ForecastMetricMilestoneDt,
        CAST('10/1/' + CAST(wbscpd.TimeFrame - 1 AS VARCHAR(4)) AS DATETIME) AS ForecastStartDt,
        CAST('9/30/' + CAST(wbscpd.EstimatedCompletionFY AS VARCHAR(4)) AS DATETIME) AS ForecastEndDt,
        CAST(NULL AS DATETIME) AS BaselineMetricMilestoneDt,
        CAST('10/1/' + CAST(wbscpd.TimeFrame - 1 AS VARCHAR(4)) AS DATETIME) AS BaselineStartDt,
        CAST('9/30/' + CAST(wbscpd.EstimatedCompletionFY AS VARCHAR(4)) AS DATETIME) AS BaselineEndDt, NULL AS MetricAmount,
        vips.OrganizationBreakdownStructure AS PrimarySite, RealPropertyUniqueIdentifierList.RealPropertyUniqueIdentifier
    FROM G2.WorkBreakdownStructureChange wbsc
    INNER JOIN G2.WorkBreakdownStructureChangePlanDetail wbscpd ON wbscpd.WorkBreakdownStructureChangeId = wbsc.Id
    INNER JOIN G2.ChangeRequestDetail crd ON wbsc.ChangeRequestDetailId = crd.Id
    INNER JOIN G2.ChangeRequestDetailStatus crds ON crd.ChangeRequestDetailStatusId = crds.Id
    INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
    INNER JOIN G2.vwChangeRequestDetailStatusActiveStatesAndProcessedStates vcrdsas WITH (NOEXPAND) ON vcrdsas.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
    INNER JOIN G2.FundingProgram nfpi ON wbscpd.FundingProgramId = nfpi.Id AND cr.SubSystemId = nfpi.SubSystemId
    INNER JOIN G2.InfrastructurePlanningTimeframe ipt ON wbscpd.TimeFrame BETWEEN COALESCE(ipt.StartOffset + G2.fnGetInfrastructurePlanningCurrentBudgetYear(G2.fnGetNA50PESubSystemId()), 0)
        AND COALESCE(ipt.EndOffset + G2.fnGetInfrastructurePlanningCurrentBudgetYear(G2.fnGetNA50PESubSystemId()), wbscpd.TimeFrame)
    INNER JOIN G2.WorkBreakdownStructureChangeWorkTypeXref wbscwtx ON wbscwtx.WorkBreakdownStructureChangeId = wbsc.Id
    INNER JOIN G2.WorkType wt ON wt.Id = wbscwtx.WorkTypeId
    INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
    INNER JOIN Contact.vwInfrastructurePlanningSite vips ON wbscpd.OrganizationPartyId = vips.PartyId AND wtg.SubSystemId = vips.SubSystemId
    LEFT OUTER JOIN (
	    SELECT awbspc.Id, SUM(wbscbd.Amount) AS TotalDesignCost, SUM(wbscbc.Amount) AS TotalContingencyCost,
	        SUM(wbscbe.Amount) AS TotalExecutionCost, SUM(wbscbo.Amount) AS TotalOtherProjectCost,
	        SUM(COALESCE(wbscbd.Amount, 0)) + SUM(COALESCE(wbscbc.Amount, 0)) + SUM(COALESCE(wbscbe.Amount, 0)) + SUM(COALESCE(wbscbo.Amount, 0)) AS TotalProjectCost
	    FROM (
		    SELECT wbsc.Id, wbsc.Name, crds.Name AS Status
		    FROM G2.WorkBreakdownStructureChange wbsc
		    INNER JOIN G2.WorkBreakdownStructureChangePlanDetail wbscpd ON wbscpd.WorkBreakdownStructureChangeId = wbsc.Id
		    INNER JOIN G2.ChangeRequestDetail crd ON wbsc.ChangeRequestDetailId = crd.Id
		    INNER JOIN G2.ChangeRequestDetailStatus crds ON crd.ChangeRequestDetailStatusId = crds.Id
		    INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
		    INNER JOIN G2.vwChangeRequestDetailStatusActiveStatesAndProcessedStates vcrdsas WITH (NOEXPAND) ON vcrdsas.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
		    WHERE crd.ChangeRequestTypeId = G2.fnGetWorkBreakdownStructurePlanChangeRequestTypeId()
		    AND cr.SubSystemId = G2.fnGetNA50PESubSystemId()
	    ) awbspc
	    INNER JOIN G2.WorkBreakdownStructureChangePlanDetail wbscpd ON wbscpd.WorkBreakdownStructureChangeId = awbspc.Id
	    LEFT OUTER JOIN G2.FiscalYear fy ON fy.FiscalYear BETWEEN wbscpd.TimeFrame AND wbscpd.EstimatedCompletionFY
	    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeBudget wbscbd ON awbspc.Id = wbscbd.WorkBreakdownStructureChangeId
																	    AND wbscbd.BudgetTypeId = G2.fnGetDesignBudgetTypeId()
																	    AND wbscbd.FiscalYear = fy.FiscalYear
	    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeBudget wbscbc ON awbspc.Id = wbscbc.WorkBreakdownStructureChangeId
																	    AND wbscbc.BudgetTypeId = G2.fnGetContingencyBudgetTypeId()
																	    AND wbscbc.FiscalYear = fy.FiscalYear
	    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeBudget wbscbe ON awbspc.Id = wbscbe.WorkBreakdownStructureChangeId
																	    AND wbscbe.BudgetTypeId = G2.fnGetExecutionBudgetTypeId()
																	    AND wbscbe.FiscalYear = fy.FiscalYear
	    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeBudget wbscbo ON awbspc.Id = wbscbo.WorkBreakdownStructureChangeId
																	    AND wbscbo.BudgetTypeId = G2.fnGetOtherProjectBudgetTypeId()
																	    AND wbscbo.FiscalYear = fy.FiscalYear
	    GROUP BY awbspc.Id
    ) bt ON bt.Id = wbsc.Id
    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeQuestionDetail wbscqd ON wbscqd.WorkBreakdownStructureChangeId = wbsc.Id
    LEFT OUTER JOIN G2.WorkBreakdownStructurePlanStatus wbsps ON wbscqd.WorkBreakdownStructurePlanStatusId = wbsps.Id
    LEFT OUTER JOIN G2.WorkBreakdownStructureChangeGroup wbscg ON wbsc.Id = wbscg.WorkBreakdownStructureChangeId
    LEFT OUTER JOIN G2.WorkBreakdownStructurePlanGroup wbspg ON wbscg.WorkBreakdownStructurePlanGroupId = wbspg.Id
    OUTER APPLY ( 
	    SELECT SUBSTRING(
            ( 
                SELECT  ', ' + RealPropertyUniqueIdentifierId
                FROM ( 
                    SELECT DISTINCT wbsRPUID.Id AS WBSCId, CAST(ast.RealPropertyUniqueIdentifier AS VARCHAR) AS RealPropertyUniqueIdentifierId
                    FROM G2.WorkBreakdownStructureChange wbsRPUID
                    INNER JOIN G2.WorkBreakdownStructureChangeAsset AS wax ON wbsRPUID.Id = wax.WorkBreakdownStructureChangeId
                    INNER JOIN G2.Asset ast ON wax.AssetId = ast.Id
                ) s
                WHERE wbsc.Id = s.WBSCId
                FOR XML PATH('')
            ), 3, 2000
        ) AS RealPropertyUniqueIdentifier
    ) RealPropertyUniqueIdentifierList
    WHERE crd.ChangeRequestTypeId = G2.fnGetWorkBreakdownStructurePlanChangeRequestTypeId()
    AND cr.SubSystemId = G2.fnGetNA50PESubSystemId()
    AND nfpi.ShortName = 'NA-50';


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwProjectDataRebuild]'
GO



-- =============================================================
-- Author:      Christopher Luttrell (CORP\chris.luttrell)
-- Created:     10/01/2015
-- Release:     6.3
-- Description: Returns Project data for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- 10/6/15   CGL      6.3      Changed the Status value to be Schedule based to match the WBS Dashboard
-- 10/12/15  CGL      6.3      Refactored to remove financial Budgets and Performers so there is 1 row per Project with pertinent financial numbers
-- 10/14/15  CGL      6.3      Refactored to add PortfolioManager and all 4 fields for Primary and Secondary Groups (Id, Name, Full and Sortable WBS)
-- 11/23/15  s.oneal  6.4      Renamed "HasAtLeastOneConfigInd" to "HasAtLeastOneLeafLevelConfigInd"
-- 01/06/15  CGL      6.4      fixed bug DE4188 had LT instead of GT in calc for Project Status
-- 05/04/16  CGL      7.0      Fixed logic flaw joining to Schedule
-- 05/19/16  CGL      7.1      Added Baseline and Forecaset MetricMilestone, Start and End dates.
-- 11/18/16  DNC      7.4      Add PrimarySite per US6695
-- 02/17/17  DNC      7.5      Add PlanningStatus, Planning Scenario and Budget
-- 03/01/17  DNC      7.5      Add RealPropertyUniqueIdentifier as CSV List
-- 03/02/17  DNC      7.5      Changed '' to NULL for PlanningScenario
--                             Cast as NVARCHAR(50)
-- 05/08/17  DGP      8.0      Using Name instead of LongName for WorkType
-- =============================================================
CREATE VIEW [Geo].[vwProjectDataRebuild]
AS

    SELECT  WBS.SubSystemId, WBSG.WorkBreakdownStructureId, WBS.Name WBSName, WBS.FullWBS, WBS.SortableWBS,
            WBSS.Name Status, 'Processed/Budgeted' AS PlanningStatus, CAST(NULL AS NVARCHAR(50)) AS PlanningScenario, 
			CAST(WT.Name AS NVARCHAR(100)) WorkType, WTG.Name WorkTypeGroup, C.LongName Country, s.Name State, 
			WBSG.PrimaryGroupSortableWBS, WBSG.PrimaryGroupFullWBS, WBSG.PrimaryGroupId, WBSG.PrimaryGroupName, 
            WBSG.SecondaryGroupSortableWBS, WBSG.SecondaryGroupFullWBS, WBSG.SecondaryGroupId, WBSG.SecondaryGroupName, 
			FiscalYear.FiscalYear, budgetTotal.Budget,
            fundingProfile.Carryover, fundingProfile.Funding, 
			COALESCE(fundingProfile.Carryover,0) + COALESCE(fundingProfile.Funding,0) TotalFunding, 
			fundingProfile.Costs, fundingProfile.Commitments,
			COALESCE(fundingProfile.Carryover,0) + COALESCE(fundingProfile.Funding,0) - COALESCE(fundingProfile.Costs,0) - COALESCE(fundingProfile.Commitments,0) AvailableFunding, 
            PortfolioManagerList.PortfolioManagerIdList, PortfolioManagerList.PortfolioManagerList,
            ProjectManagerList.ProjectManagerIdList, ProjectManagerList.ProjectManagerList,
			PMMetrics.ForecastMetricMilestoneDt, PMMetrics.ForecastStartDt, PMMetrics.ForecastEndDt,
			PMMetrics.BaselineMetricMilestoneDt, PMMetrics.BaselineStartDt, PMMetrics.BaselineEndDt,
			PMMetrics.MetricAmount, CAST(wbsa.Value AS VARCHAR(MAX)) PrimarySite,
			RealPropertyUniqueIdentifierList.RealPropertyUniqueIdentifier
    FROM    G2.WorkBreakdownStructure WBS
    CROSS APPLY ( SELECT G2.fnGetCurrentFiscalYear (WBS.SubSystemId) FiscalYear
                ) FiscalYear
    INNER JOIN G2.SubSystemLevel SSL ON SSL.Id = WBS.SubSystemLevelId
                                        AND SSL.Label = 'Project'
    CROSS APPLY ( SELECT    CASE WHEN WBS.CompletedInd = 1 THEN 'Completed'
                                 WHEN MIN(Sch.StartDt) > CURRENT_TIMESTAMP THEN 'Not Started'
                                 ELSE 'In Progress'
                            END Name
                  FROM      G2.Schedule Sch
                  INNER JOIN G2.WorkBreakdownStructure WBSInternal ON Sch.WorkBreakdownStructureId = WBS.Id
                  WHERE     WBSInternal.SubSystemId = WBS.SubSystemId
                            AND WBSInternal.FullWBS LIKE WBS.FullWBS + '.%'
							OR WBSInternal.Id = WBS.Id
                ) WBSS
    INNER JOIN G2.vwWorkBreakdownStructureConfigurationPivot VWBSCP ON VWBSCP.WorkBreakdownStructureId = WBS.Id
                                                                       AND VWBSCP.HasAtLeastOneLeafLevelConfigInd = 1
    INNER JOIN G2.WorkBreakdownStructureGroups WBSG ON WBSG.WorkBreakdownStructureId = WBS.Id
    INNER JOIN G2.WorkBreakdownStructureWorkTypeXref WBSWTX ON WBSWTX.WorkBreakdownStructureId = WBS.Id
    INNER JOIN G2.WorkType WT ON WT.Id = WBSWTX.WorkTypeId
    INNER JOIN G2.WorkTypeGroup WTG ON WTG.Id = WT.WorkTypeGroupId
	LEFT OUTER JOIN G2.WorkBreakdownStructureAttribute wbsa
				ON WBS.Id = wbsa.WorkBreakdownStructureId AND wbsa.WorkBreakdownStructureAttributeTypeId = 31 --G2.fnGetPrimarySiteWorkBreakdownStructureAttributeTypeId()
	LEFT OUTER JOIN (
						SELECT	lb.WorkBreakdownStructureId, SUM(lb.Amount) AS Budget
						FROM	Financial.LifecycleBudget lb
						GROUP BY lb.WorkBreakdownStructureId
					) budgetTotal ON WBS.Id = budgetTotal.WorkBreakdownStructureId 
    LEFT OUTER JOIN G2.WorkBreakdownStructureCountryXref WBSCX
    INNER JOIN G2.Country C ON C.Id = WBSCX.CountryId ON WBSCX.WorkBreakdownStructureId = WBS.Id
    LEFT OUTER JOIN G2.WorkBreakdownStructureStateXref WBSSX
    INNER JOIN G2.State s ON s.Id = WBSSX.StateId ON WBSSX.WorkBreakdownStructureId = WBS.Id
    OUTER APPLY ( SELECT    a.WorkBreakdownStructureId,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 1 AND p.FiscalYear < FY.FiscalYear THEN at.Amount --Financial.fnGetFundingTransactionTypeGroupId()
                                        WHEN tt.TransactionTypeGroupId = 3 AND p.FiscalYear < FY.FiscalYear THEN -at.Amount --Financial.fnGetCostTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Carryover,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 1 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetFundingTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Funding,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 2 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetCommitmentTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Commitments,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 3 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetCostTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Costs
                      FROM      Financial.Account a
					  CROSS JOIN G2.FiscalYear FY
                      INNER JOIN Financial.AccountTransaction at ON at.AccountId = a.Id
                      INNER JOIN Financial.TransactionType tt ON tt.Id = at.TransactionTypeId
                      INNER JOIN G2.Period p ON p.Id = at.PeriodId
                      WHERE FY.FiscalYear = FiscalYear.FiscalYear
					  AND a.WorkBreakdownStructureId = WBS.Id
					  GROUP BY  a.WorkBreakdownStructureId
                    ) fundingProfile 
    OUTER APPLY ( SELECT    SUBSTRING(( SELECT  ', ' + PortfolioManager
                                        FROM    ( SELECT    DISTINCT wbsInt.Id AS WBSId ,
                                                            pers.FirstName + ' ' + pers.LastName AS PortfolioManager
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS PortfolioManagerList ,
                            SUBSTRING(( SELECT  ', ' + PortfolioManagerId
                                        FROM    ( SELECT    DISTINCT wbsInt.Id AS WBSId ,
                                                            CAST(pers.Id AS VARCHAR) AS PortfolioManagerId
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS PortfolioManagerIdList
                ) PortfolioManagerList
    OUTER APPLY ( SELECT    SUBSTRING(( SELECT  ', ' + ProjectManager
                                        FROM    ( SELECT    wbsInt.Id AS WBSId ,
                                                            pers.FirstName + ' ' + pers.LastName AS ProjectManager
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS ProjectManagerList ,
                            SUBSTRING(( SELECT  ', ' + ProjectManagerId
                                        FROM    ( SELECT    wbsInt.Id AS WBSId ,
                                                            CAST(pers.Id AS VARCHAR) AS ProjectManagerId
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS ProjectManagerIdList
                ) ProjectManagerList
	OUTER APPLY (SELECT 1 AS HasMetrics, MAX(CASE WHEN PMM.Id IS NOT NULL THEN PM.FinishDt ELSE NULL END) ForecastMetricMilestoneDt, MIN(PM.FinishDt) ForecastStartDt, MAX(PM.FinishDt) ForecastEndDt, 
						MAX(CASE WHEN PMM.Id IS NOT NULL THEN PM.BaselineFinishDt ELSE NULL END) BaselineMetricMilestoneDt, MIN(PM.BaselineFinishDt) BaselineStartDt, MAX(PM.BaselineFinishDt) BaselineEndDt, SUM(PMM.ForecastValue) MetricAmount
				 FROM G2.ProjectMilestone PM
				 INNER JOIN G2.Task T ON T.Id = PM.TaskId
				 INNER JOIN G2.WorkBreakdownStructure WBSTask ON WBSTask.Id = T.WorkBreakdownStructureId
				 LEFT OUTER JOIN G2.ProjectMilestoneMetric PMM ON PMM.ProjectMilestoneId = PM.Id
				 WHERE WBSTask.ParentId = WBS.Id
				 AND PM.FinishDt <> CONVERT(DATETIME, '1/1/1900')
				 AND PM.BaselineFinishDt <> CONVERT(DATETIME, '1/1/1900')
				 HAVING MIN(PM.FinishDt) IS NOT NULL
				 ) PMMetrics
    OUTER APPLY ( 
					SELECT SUBSTRING(( SELECT  ', ' + RealPropertyUniqueIdentifierId
                                        FROM    ( SELECT    DISTINCT wbsRPUID.Id AS WBSId ,
                                                            CAST(ast.RealPropertyUniqueIdentifier AS VARCHAR) AS RealPropertyUniqueIdentifierId
                                                  FROM      G2.WorkBreakdownStructure wbsRPUID
                                                  INNER JOIN G2.WorkBreakdownStructureAssetXref AS wax ON wbsRPUID.Id = wax.WorkBreakdownStructureId
                                                  INNER JOIN G2.Asset ast ON wax.AssetId = ast.Id
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS RealPropertyUniqueIdentifier
                ) RealPropertyUniqueIdentifierList
				 ;
    --WHERE   WBS.SubSystemId <> 1;











GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[CombinedPerformer]'
GO

CREATE VIEW [Contact].[CombinedPerformer]
WITH SCHEMABINDING
AS
SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd, COUNT_BIG(*) cnt
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
INNER JOIN Contact.PartyRoleType PRT ON PRT.Id = pr.PartyRoleTypeId
WHERE pty.DeletedInd = 0
AND pr.PartyRoleTypeId IN (6, 9, 11) -- Contact.fnGetBudgetPerformerPartyRoleTypeId(), Contact.fnGetLeadLabPartyRoleTypeId(), Contact.fnGetPerformerPartyRoleTypeId()
GROUP BY o.PartyId, pr.SubSystemId, o.OrganizationBreakdownStructure, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_CombinedPerformer] on [Contact].[CombinedPerformer]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_CombinedPerformer] ON [Contact].[CombinedPerformer] ([SubSystemId], [ReportingEntityOrganizationPartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwPerformerDataRebuild]'
GO

-- =============================================================
-- Author:      Christopher Luttrell (CORP\chris.luttrell)
-- Created:     11/13/2015
-- Release:     6.4
-- Description: Returns Perfomer/Project Financial data for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- 11/23/15  s.oneal  6.4      Renamed "HasAtLeastOneConfigInd" to "HasAtLeastOneConfigInd"
-- 01/06/15  CGL      6.4      fixed bug DE4188 had LT instead of GT in calc for Project Status
-- 01/07/15  CGL      6.4      fixed bug DE4145 changed to not rely on current financial data alone, so budget only will be picked up
-- 01/13/15  CGL      6.5      Changed to use new Combined Performer to include Lead Labs
-- 05/03/16  CGL      7.0      Added check against SpendPlanHeader to not throw out records that have SP but no other financials
-- 05/04/16  CGL      7.0      Fixed logic flaw joining to Schedule
-- 05/04/16  CGL      7.0      After discussing with Mike Holm and Ty we are going to return all records so the right combinations can be returned for graphs
-- 05/05/16  CGL      7.0      Fix for DE4855, 4883 and 4912, getting the right WBS/Performer combos to show up the right way and backed out the return all records above
-- 05/09/18  m.brown  9.0      Updated to use the new spend plan schema changes.  The spend plan query checks for 
-- =============================================================
CREATE VIEW [Geo].[vwPerformerDataRebuild] 
AS
    SELECT WBS.SubSystemId, CP.ReportingEntityOrganizationPartyId, CP.FormalCode Performer, WBSG.WorkBreakdownStructureId, WBS.Name WBSName, WBS.FullWBS, WBS.SortableWBS,
            WBSS.Name Status, WT.LongName WorkType, WTG.Name WorkTypeGroup, C.LongName Country, s.Name State, 
			WBSG.PrimaryGroupSortableWBS, WBSG.PrimaryGroupFullWBS, WBSG.PrimaryGroupId, WBSG.PrimaryGroupName, 
            WBSG.SecondaryGroupSortableWBS, WBSG.SecondaryGroupFullWBS, WBSG.SecondaryGroupId, WBSG.SecondaryGroupName, 
			FiscalYear.FiscalYear, LCB.Budget,
            fundingProfile.Carryover, fundingProfile.Funding, 
			COALESCE(fundingProfile.Carryover,0) + COALESCE(fundingProfile.Funding,0) TotalFunding, 
			fundingProfile.Costs, fundingProfile.Commitments,
			COALESCE(fundingProfile.Carryover,0) + COALESCE(fundingProfile.Funding,0) - COALESCE(fundingProfile.Costs,0) - COALESCE(fundingProfile.Commitments,0) AvailableFunding, 
            PortfolioManagerList.PortfolioManagerIdList, PortfolioManagerList.PortfolioManagerList,
            ProjectManagerList.ProjectManagerIdList, ProjectManagerList.ProjectManagerList,
			PMMetrics.ForecastMetricMilestoneDt, PMMetrics.ForecastStartDt, PMMetrics.ForecastEndDt,
			PMMetrics.BaselineMetricMilestoneDt, PMMetrics.BaselineStartDt, PMMetrics.BaselineEndDt,
			PMMetrics.MetricAmount
    FROM    G2.WorkBreakdownStructure WBS
    INNER JOIN G2.SubSystemLevel SSL ON SSL.Id = WBS.SubSystemLevelId
                                        AND SSL.Label = 'Project'
    CROSS APPLY ( 
		SELECT G2.fnGetCurrentFiscalYear (WBS.SubSystemId) FiscalYear   
	) FiscalYear
    CROSS APPLY ( SELECT    CASE WHEN WBS.CompletedInd = 1 THEN 'Completed'
                                 WHEN MIN(Sch.StartDt) > CURRENT_TIMESTAMP THEN 'Not Started'
                                 ELSE 'In Progress'
                            END Name
                  FROM      G2.Schedule Sch
                  INNER JOIN G2.WorkBreakdownStructure WBSInternal ON Sch.WorkBreakdownStructureId = WBS.Id
                  WHERE     WBSInternal.SubSystemId = WBS.SubSystemId
                            AND WBSInternal.FullWBS LIKE WBS.FullWBS + '.%'
							OR WBSInternal.Id = WBS.Id
                ) WBSS
    INNER JOIN G2.vwWorkBreakdownStructureConfigurationPivot VWBSCP ON VWBSCP.WorkBreakdownStructureId = WBS.Id
                                                                       AND VWBSCP.HasAtLeastOneLeafLevelConfigInd = 1
    INNER JOIN G2.WorkBreakdownStructureGroups WBSG ON WBSG.WorkBreakdownStructureId = WBS.Id
    INNER JOIN G2.WorkBreakdownStructureWorkTypeXref WBSWTX ON WBSWTX.WorkBreakdownStructureId = WBS.Id
    INNER JOIN G2.WorkType WT ON WT.Id = WBSWTX.WorkTypeId
    INNER JOIN G2.WorkTypeGroup WTG ON WTG.Id = WT.WorkTypeGroupId
    LEFT OUTER JOIN G2.WorkBreakdownStructureCountryXref WBSCX
    INNER JOIN G2.Country C ON C.Id = WBSCX.CountryId ON WBSCX.WorkBreakdownStructureId = WBS.Id
    LEFT OUTER JOIN G2.WorkBreakdownStructureStateXref WBSSX
    INNER JOIN G2.State s ON s.Id = WBSSX.StateId ON WBSSX.WorkBreakdownStructureId = WBS.Id
    INNER JOIN Contact.CombinedPerformer CP ON CP.SubSystemId = WBS.SubSystemId
    OUTER APPLY ( SELECT    1 AS HasTransactions, a.WorkBreakdownStructureId, a.ReportingEntityOrganizationPartyId, RE.FormalCode,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 1 AND p.FiscalYear < FY.FiscalYear THEN at.Amount --Financial.fnGetFundingTransactionTypeGroupId()
                                        WHEN tt.TransactionTypeGroupId = 3 AND p.FiscalYear < FY.FiscalYear THEN -at.Amount --Financial.fnGetCostTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Carryover,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 1 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetFundingTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Funding,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 2 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetCommitmentTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Commitments,
                            SUM(CASE WHEN tt.TransactionTypeGroupId = 3 AND p.FiscalYear = FY.FiscalYear THEN at.Amount --Financial.fnGetCostTransactionTypeGroupId()
                                        ELSE 0
                                END) AS Costs
                      FROM      Financial.Account a
					  INNER JOIN Contact.ReportingEntity RE ON RE.OrganizationPartyId = a.ReportingEntityOrganizationPartyId
					  CROSS JOIN G2.FiscalYear FY
                      INNER JOIN Financial.AccountTransaction at ON at.AccountId = a.Id
                      INNER JOIN Financial.TransactionType tt ON tt.Id = at.TransactionTypeId
                      INNER JOIN G2.Period p ON p.Id = at.PeriodId
                      WHERE FY.FiscalYear = FiscalYear.FiscalYear
					  AND a.WorkBreakdownStructureId = WBS.Id
					  AND a.ReportingEntityOrganizationPartyId = CP.ReportingEntityOrganizationPartyId
					  GROUP BY  a.WorkBreakdownStructureId, a.ReportingEntityOrganizationPartyId, RE.FormalCode
                    ) fundingProfile 
    OUTER APPLY ( SELECT 1 AS HasLB, SUM(CASE WHEN LB.FiscalYear = CheckFY.FiscalYear THEN LB.Amount ELSE 0 END) Budget FROM Financial.LifecycleBudget LB CROSS JOIN G2.FiscalYear CheckFY WHERE CheckFY.FiscalYear = FiscalYear.FiscalYear AND LB.WorkBreakdownStructureId = WBS.Id AND LB.ReportingEntityOrganizationPartyId = CP.ReportingEntityOrganizationPartyId HAVING SUM(CASE WHEN LB.FiscalYear = CheckFY.FiscalYear THEN LB.Amount ELSE 0 END) IS NOT NULL) LCB
    OUTER APPLY ( SELECT    SUBSTRING(( SELECT  ', ' + PortfolioManager
                                        FROM    ( SELECT    DISTINCT wbsInt.Id AS WBSId ,
                                                            pers.FirstName + ' ' + pers.LastName AS PortfolioManager
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS PortfolioManagerList ,
                            SUBSTRING(( SELECT  ', ' + PortfolioManagerId
                                        FROM    ( SELECT    DISTINCT wbsInt.Id AS WBSId ,
                                                            CAST(pers.Id AS VARCHAR) AS PortfolioManagerId
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 103--Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS PortfolioManagerIdList
                ) PortfolioManagerList
    OUTER APPLY ( SELECT    SUBSTRING(( SELECT  ', ' + ProjectManager
                                        FROM    ( SELECT    wbsInt.Id AS WBSId ,
                                                            pers.FirstName + ' ' + pers.LastName AS ProjectManager
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS ProjectManagerList ,
                            SUBSTRING(( SELECT  ', ' + ProjectManagerId
                                        FROM    ( SELECT    wbsInt.Id AS WBSId ,
                                                            CAST(pers.Id AS VARCHAR) AS ProjectManagerId
                                                  FROM      G2.WorkBreakdownStructure wbsInt
                                                  INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbsInt.SubSystemId
                                                                                          AND persR.WorkBreakdownStructureId = wbsInt.Id
                                                  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
                                                  WHERE     persR.RoleId = 100--Security.fnGetScheduleProjectManagerPrimaryRoleId()
                                                ) s
                                        WHERE   WBS.Id = s.WBSId
                                      FOR
                                        XML PATH('')
                                      ), 3, 2000) AS ProjectManagerIdList
                ) ProjectManagerList
	OUTER APPLY (SELECT 1 AS HasMetrics, MAX(CASE WHEN PMM.Id IS NOT NULL THEN PM.FinishDt ELSE NULL END) ForecastMetricMilestoneDt, MIN(PM.FinishDt) ForecastStartDt, MAX(PM.FinishDt) ForecastEndDt, 
						MAX(CASE WHEN PMM.Id IS NOT NULL THEN PM.BaselineFinishDt ELSE NULL END) BaselineMetricMilestoneDt, MIN(PM.BaselineFinishDt) BaselineStartDt, MAX(PM.BaselineFinishDt) BaselineEndDt, SUM(PMM.ForecastValue) MetricAmount
				 FROM G2.ProjectMilestone PM
				 INNER JOIN G2.Task T ON T.Id = PM.TaskId
				 INNER JOIN G2.WorkBreakdownStructure WBSTask ON WBSTask.Id = T.WorkBreakdownStructureId
				 LEFT OUTER JOIN G2.ProjectMilestoneMetric PMM ON PMM.ProjectMilestoneId = PM.Id
				 WHERE WBSTask.ParentId = WBS.Id
				 AND T.LeadReportingEntityOrganizationPartyId = CP.ReportingEntityOrganizationPartyId
				 AND PM.FinishDt <> CONVERT(DATETIME, '1/1/1900')
				 AND PM.BaselineFinishDt <> CONVERT(DATETIME, '1/1/1900')
				 HAVING MIN(PM.FinishDt) IS NOT NULL
				 ) PMMetrics
	OUTER APPLY (
		SELECT 1 AS HasSpendPlan 
		FROM Financial.SpendPlanHeader SPH 
		INNER JOIN Financial.SpendPlanDetail spd ON spd.SpendPlanHeaderId = SPH.Id
		INNER JOIN G2.Period p ON p.Id = spd.PeriodId
		WHERE SPH.WorkBreakdownStructureId = WBS.Id 
			AND SPH.ReportingEntityOrganizationPartyId = CP.ReportingEntityOrganizationPartyId 
			AND p.FiscalYear = FiscalYear.FiscalYear
	) SP
	WHERE WBS.SubSystemId > 1  -- Excluding the old GTRI SSID=1 records since we don't use that anymore to cut down the data size.
	AND COALESCE(LCB.HasLB, SP.HasSpendPlan, fundingProfile.HasTransactions, PMMetrics.HasMetrics) = 1;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwSiteDataRebuild]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     05/09/2013
-- Release:     4.1 
-- Description: Returns site date for the ANL map page
--
-- mm/dd/yy  Name       Release  Description
-- 05/22/13  m.brown    4.2    Updated SPAStatusSymbolization rules
-- 05/22/13  m.brown    4.2    Updated to include sites even if they don't have SPAs
-- 05/22/13  m.brown    4.2    Modified to show minimum date only if the milestone isn't completed; if the milestone is
--                             completed then we use the minimum completed date
-- 07/10/13  m.brown    4.2    Modified to not ONLY consider completed milestones when looking at milestones at the site.
-- 07/25/13  m.brown    4.3    No longer displays deleted sites
-- 09/11/13  m.brown    4.4    Removed SPAVersion and adjusted rules for SPAStatusSymbolization
-- 09/12/13  m.brown    4.4    Fixed join to SPAMainGeneral to use revision instead of SPAVersionId
-- 10/30/13  strickland 4.5    Updated to join SPA.SPAMainGeneralSPATeamXref to SPA.SPAMainGeneral in order to get SPA.SPAMainGeneralSPATeamXref.SPATeamId where SPA.SPAMainGeneralSPATeamXref.PrimaryInd = 1
-- 03/05/14  j.frank    5.0    Updated some field names and inner queries to support changing "country officer" to "portfolio officer".
-- 04/23/14  m.brown    5.0    The last site visit date for the building was considering non-actualized milestones incorrectly.
-- 06/17/14  m.brown    5.1    LastSiteVisit was always returning the building value irrespective of which was greater (building or site)
--                             The LastSiteVisit column now correctly returns the site when the site's last visit date is greater than the building's
-- 11/07/14  m.brown    5.3    Updated to accommodate schema changes in 5.3
-- 10/14/15  CGL        6.3    Modified for latest column inclusion decissions and renames
-- 08/30/18  m.holm     9.2    Replaced/Renamed logic for SPAStatusSymbolization.
-- =============================================================
CREATE VIEW [Geo].[vwSiteDataRebuild]
AS
	SELECT s.Id AS Id,
		geometry::STGeomFromText('POINT(' + CONVERT(VARCHAR(20), s.Longitude) + ' ' + CONVERT(VARCHAR(20), s.Latitude) + '  )', 4326) AS [Geometry],
		CAST(CASE WHEN s.Latitude = 0 AND s.Longitude = 0 THEN 0 ELSE 1 END AS BIT) AS GeometryValid,
		PortfolioManagerList.PortfolioManagerIdList,
		PortfolioManagerList.PortfolioManagerList,
		o.Id AS OfficeId,
		o.[Name] AS OfficeName,
		c.Id AS CountryId,
		c.LongName AS CountryName,
		r.[Name] AS RegionName,
		st.[Name] AS StateName,
		s.City AS City,
		s.Address1 AS PrimaryStreetAddress,
		s.Address2 AS SecondaryStreetAddress,
		s.[Name],
		sm.Revision AS SPAVersionRevision,
		ss.[Name] AS SPAStatusName,
		COALESCE((
			SELECT ms.[Name]
			FROM G2.TaskSite ts
			INNER JOIN G2.Task t ON t.Id = ts.TaskId
			INNER JOIN G2.ProjectMilestone pm
			INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
			WHERE ts.SiteId = s.Id
			AND ms.Id = 1300
			AND pm.CompletedInd = 1 --G2.fnGetSiteLevelSupportConcludedMilestoneId()
		), (
			SELECT ms.[Name]
			FROM G2.TaskSite ts
			INNER JOIN G2.Task t ON t.Id = ts.TaskId
			INNER JOIN G2.ProjectMilestone pm
			INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
			WHERE ts.SiteId = s.Id
			AND ms.Id = 1301
			AND pm.CompletedInd = 1 --G2.fnGetSiteLevelTransitionMilestoneId()
		), (
			SELECT CASE WHEN buildingStatusAssessed.AssessedCount < buildingStatusTasks.TaskCount
						AND buildingStatusAwarded.AwardedCount = 0
						AND buildingStatusSecure.securedCount = 0 THEN 'Not assessed'
					   WHEN buildingStatusAssessed.AssessedCount = buildingStatusTasks.TaskCount
					   AND buildingStatusAwarded.AwardedCount = 0
					   AND buildingStatusSecure.securedCount = 0 THEN 'Assessed'
					   WHEN buildingStatusAwarded.AwardedCount > 0
					   AND buildingStatusSecure.securedCount < buildingStatusTasks.TaskCount THEN 'In progress'
					   WHEN buildingStatusSecure.securedCount = buildingStatusTasks.TaskCount THEN 'Complete'
				   END
		),	'Other'
		) AS SiteStatusSymbolization,
		spaTeam.[Name] AS SPATeamName,
		CASE WHEN COALESCE(initialAssessmentMilestone.InitialAssessmentComplete, 0) = 1 THEN
				 initialAssessmentMilestone.CompletedInitialAssessmentDate
			ELSE initialAssessmentMilestone.InitialAssessmentDate
		END AS InitialAssessmentDate,
		initialAssessmentMilestone.InitialAssessmentComplete,
		CASE WHEN COALESCE(contractAwardMilestone.ContractAwardedComplete, 0) = 1 THEN contractAwardMilestone.CompletedContractAwardedDate
			ELSE contractAwardMilestone.ContractAwardedDate
		END AS ContractAwardedDate,
		contractAwardMilestone.ContractAwardedComplete,
		CASE WHEN COALESCE(assuranceVisitMilestone.AssuranceVisitComplete, 0) = 1 THEN assuranceVisitMilestone.CompletedAssuranceVisitDate
			ELSE assuranceVisitMilestone.AssuranceVisitDate
		END AS AssuranceVisitDate,
		assuranceVisitMilestone.AssuranceVisitComplete,
		CASE WHEN COALESCE(buildingSecureMilestone.BuildingSecureComplete, 0) = 1 THEN buildingSecureMilestone.CompletedBuildingSecureDate
			ELSE buildingSecureMilestone.BuildingSecureDate
		END AS BuildingSecureDate,
		buildingSecureMilestone.BuildingSecureComplete,
		CASE WHEN (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
				  AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			 )
			 OR (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
				AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			 ) THEN
				 CASE WHEN COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate, '19000101') > COALESCE(
																														  buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate,
																														  '19000101'
																													  ) THEN
						  siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
					 ELSE buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
				 END
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0 THEN
				buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1 THEN
				siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
		END AS SustainabilityAssessmentDate,
		CASE WHEN (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
				  AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			 )
			 OR (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
				AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			 ) THEN
				 CASE WHEN COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate, '19000101') > COALESCE(
																														  buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate,
																														  '19000101'
																													  ) THEN
						  siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
					 ELSE buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
				 END
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0 THEN
				buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1 THEN
				siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
		END AS SustainabilityAssessmentComplete,
		CASE WHEN COALESCE(lastBuildingVisitMilestone.LastVisitDate, '19000101') > COALESCE(lastSiteVisitMilestone.LastVisitDate, '19000101') THEN
				 lastBuildingVisitMilestone.LastVisitDate
			ELSE lastSiteVisitMilestone.LastVisitDate
		END AS LastSiteVisitDate
	FROM G2.Site s
	LEFT OUTER JOIN G2.[State] st ON s.StateId = st.Id
	LEFT OUTER JOIN Inventory.RegionStateXRef rsxr ON st.Id = rsxr.StateId
	LEFT OUTER JOIN Inventory.Region r ON rsxr.RegionId = r.Id
	INNER JOIN G2.Country c ON s.CountryId = c.Id
	CROSS APPLY (
		SELECT office.Id
		FROM G2.WorkBreakdownStructure portfolio
		INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.WorkBreakdownStructureId = portfolio.Id
		INNER JOIN G2.WorkBreakdownStructure office ON portfolio.ParentId = office.Id
		WHERE portfolio.SubSystemLevelId = 3 -- portfolio
		AND wbscx.CountryId = c.Id
		GROUP BY office.Id
	) countryOffice
	INNER JOIN G2.WorkBreakdownStructure o ON o.Id = countryOffice.Id
	LEFT OUTER JOIN SPA.SPAMain sm ON s.Id = sm.SiteId
	LEFT OUTER JOIN SPA.SPAStatus ss ON sm.SPAStatusId = ss.Id
	LEFT OUTER JOIN SPA.SPAMainGeneral smg ON sm.Id = smg.SPAMainId
										   AND sm.Revision = smg.Revision
	LEFT OUTER JOIN SPA.SPAMainGeneralSPATeamXref smgstx ON smgstx.SPAMainGeneralId = smg.Id
														 AND smgstx.PrimaryInd = 1 -- Get the primary SPA Team
	LEFT OUTER JOIN SPA.SPATeam spaTeam ON smgstx.SPATeamId = spaTeam.Id
	OUTER APPLY (
		SELECT b.SiteId,
			MIN(CAST(CASE WHEN b.InScopeInd = 0 THEN 1 ELSE sms.IsIncludedInd END AS INT)) AS AllInScopeBuildingsIncludedInSPA,
			MIN(bes.SPAVersionId) AS MinSPAVersionId,
			MIN(bes.SPAVersionId) AS MaxSPAVersionId
		FROM G2.Building b
		LEFT OUTER JOIN SPA.SPAMainSection sms ON sms.RefId = b.Id
		LEFT OUTER JOIN SPA.BuildingExtensionSPA bes ON bes.BuildingId = b.Id
		WHERE sms.EntityTypeId = 7 -- Building
		AND sms.SPASectionTypeId = 5 -- Building Information
		AND b.SiteId = s.Id
		AND b.InScopeInd = 1
		GROUP BY b.SiteId
	) buildingInfo
	LEFT OUTER JOIN (
		SELECT SiteId,
			SUBSTRING((
						  SELECT ', ' + PortfolioManager
						  FROM (
							  SELECT s.Id AS SiteId, pers.FirstName + ' ' + pers.LastName AS PortfolioManager
							  FROM G2.[Site] s
							  INNER JOIN G2.Country c ON c.Id = s.CountryId
							  INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.CountryId = c.Id
							  INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbscx.WorkBreakdownStructureId
																	   AND wbs.SubSystemLevelId = 3 -- portfolio
							  INNER JOIN Security.PersonRole persR ON persR.SubSystemId = wbs.SubSystemId
																   AND persR.WorkBreakdownStructureId = wbs.Id
							  INNER JOIN Security.Person pers ON pers.Id = persR.PersonId
							  WHERE persR.RoleId = 103 --Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
						  ) s
						  WHERE s.SiteId = x.SiteId
						  FOR XML PATH('')
					  ), 3, 2000
			) AS PortfolioManagerList,
			SUBSTRING((
						  SELECT ', ' + PortfolioManagerId
						  FROM (
							  SELECT s.Id AS SiteId, CAST(pers.Id AS VARCHAR) AS PortfolioManagerId
							  FROM G2.[Site] s
							  INNER JOIN G2.Country c ON c.Id = s.CountryId
							  INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.CountryId = c.Id
							  INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbscx.WorkBreakdownStructureId
																	   AND wbs.SubSystemLevelId = 3 -- portfolio
							  INNER JOIN [Security].PersonRole persR ON persR.SubSystemId = wbs.SubSystemId
																	 AND persR.WorkBreakdownStructureId = wbs.Id
							  INNER JOIN [Security].Person pers ON pers.Id = persR.PersonId
							  WHERE persR.RoleId = 103 --Security.fnGetSchedulePortfolioManagerPrimaryRoleId()
						  ) s
						  WHERE s.SiteId = x.SiteId
						  FOR XML PATH('')
					  ), 3, 2000
			) AS PortfolioManagerIdList
		FROM (SELECT s.Id AS SiteId FROM G2.[Site] s) x
		GROUP BY x.SiteId
	) PortfolioManagerList ON PortfolioManagerList.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId,
			MIN(pm.FinishDt) AS InitialAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedInitialAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS InitialAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId = 223
		GROUP BY b.SiteId
	) initialAssessmentMilestone ON initialAssessmentMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId,
			MIN(pm.FinishDt) AS ContractAwardedDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedContractAwardedDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS ContractAwardedComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId = 226
		GROUP BY b.SiteId
	) contractAwardMilestone ON contractAwardMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId,
			MIN(pm.FinishDt) AS AssuranceVisitDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedAssuranceVisitDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS AssuranceVisitComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId = 228
		GROUP BY b.SiteId
	) assuranceVisitMilestone ON assuranceVisitMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId,
			MIN(pm.FinishDt) AS BuildingSecureDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedBuildingSecureDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS BuildingSecureComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId = 51
		GROUP BY b.SiteId
	) buildingSecureMilestone ON buildingSecureMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId,
			MIN(pm.FinishDt) AS SustainabilityAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedSustainabilityAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS SustainabilityAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId = 558
		GROUP BY b.SiteId
	) buildingSustainabilityAssessmentMilestone ON buildingSustainabilityAssessmentMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT ts.SiteId,
			MIN(pm.FinishDt) AS SustainabilityAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedSustainabilityAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS SustainabilityAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.TaskSite ts ON t.Id = ts.TaskId
		INNER JOIN G2.Site s ON ts.SiteId = s.Id
		WHERE pm.MilestoneId = 558
		GROUP BY ts.SiteId
	) siteSustainabilityAssessmentMilestone ON siteSustainabilityAssessmentMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId, MAX(pm.FinishDt) AS LastVisitDate
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		LEFT OUTER JOIN G2.TaskDevice td
		INNER JOIN Inventory.Source src ON td.SourceId = src.Id
		INNER JOIN Inventory.Room r ON r.Id = src.RoomId ON t.Id = td.TaskId
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON COALESCE(tb.BuildingId, r.BuildingId) = b.Id
		INNER JOIN G2.Site s ON b.SiteId = s.Id
		WHERE pm.MilestoneId IN (223, 228, 558)
		AND pm.CompletedInd = 1
		GROUP BY b.SiteId
	) lastBuildingVisitMilestone ON lastBuildingVisitMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT ts.SiteId, MAX(pm.FinishDt) AS LastVisitDate
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.TaskSite ts ON ts.TaskId = t.Id
		INNER JOIN G2.Site s ON ts.SiteId = s.Id
		WHERE pm.MilestoneId IN (223, 228, 558)
		AND pm.CompletedInd = 1
		GROUP BY ts.SiteId
	) lastSiteVisitMilestone ON lastSiteVisitMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId, COUNT(DISTINCT (t.Id)) AS TaskCount
		FROM G2.Building b
		INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
		INNER JOIN G2.Task t ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.ProjectMilestone pm
		INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
		INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
		LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
		WHERE mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
		AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId()
		GROUP BY b.SiteId
	) buildingStatusTasks ON buildingStatusTasks.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.SiteId, COUNT(ms.Id) AS AssessedCount
		FROM G2.Building b
		INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
		INNER JOIN G2.Task t ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.ProjectMilestone pm
		INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
		INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
		LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
		WHERE ms.Id = 599 --G2.fnGetConductSiteAssessmentMilestoneId()
		AND pm.CompletedInd = 1
		AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
		AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId()
		GROUP BY b.SiteId, pm.CompletedInd
		UNION ALL --UNION BACK ON TABLE WITH NOT EXISTS
		SELECT 0, 0 AS AssessedCount
		WHERE NOT EXISTS (
			SELECT as2.SiteId
			FROM (
				SELECT b.SiteId AS SiteId, ms.Id AS MilestoneId
				FROM G2.Building b
				INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
				INNER JOIN G2.Task t ON t.Id = tb.TaskId
				LEFT OUTER JOIN G2.ProjectMilestone pm
				INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
				INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
				LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
				WHERE ms.Id = 599 -- G2.fnGetConductSiteAssessmentMilestoneId()
				AND pm.CompletedInd = 1
				AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
				AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId() 
			) as2
		)
	) buildingStatusAssessed ON buildingStatusAssessed.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT awarded.SiteId, COUNT(awarded.MilestoneId) AS AwardedCount
		FROM (
			SELECT b.SiteId, ms.Id AS MilestoneId
			FROM G2.Building b
			INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
			INNER JOIN G2.Task t ON t.Id = tb.TaskId
			LEFT OUTER JOIN G2.ProjectMilestone pm
			INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
			INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
			LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
			WHERE ms.Id = 595 --G2.fnGetAwardContractMilestoneId()
			AND pm.CompletedInd = 1
			AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
			AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId() 
		) awarded
		GROUP BY awarded.SiteId
		UNION ALL --UNION BACK ON TABLE WITH NOT EXISTS
		SELECT 0, 0 AS AwardedCount
		WHERE NOT EXISTS (
			SELECT aw2.SiteId
			FROM (
				SELECT b.SiteId AS SiteId, ms.Id AS MilestoneId
				FROM G2.Building b
				INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
				INNER JOIN G2.Task t ON t.Id = tb.TaskId
				LEFT OUTER JOIN G2.ProjectMilestone pm
				INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
				INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
				LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
				WHERE ms.Id = 595 -- G2.fnGetAwardContractMilestoneId()
				AND pm.CompletedInd = 1
				AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
				AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId()
			) aw2
		)
	) buildingStatusAwarded ON buildingStatusAwarded.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT secured.SiteId, COUNT(secured.MilestoneId) AS securedCount
		FROM (
			SELECT b.SiteId, ms.Id AS MilestoneId
			FROM G2.Building b
			INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
			INNER JOIN G2.Task t ON t.Id = tb.TaskId
			LEFT OUTER JOIN G2.ProjectMilestone pm
			INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
			INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
			LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
			WHERE ms.Id = 596 --G2.fnGetBuildingSecureMilestoneId(), 
			AND pm.CompletedInd = 1
			AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) -- G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
			AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId() 
		) secured
		GROUP BY secured.SiteId
		UNION ALL --UNION BACK ON TABLE WITH NOT EXISTS
		SELECT 0, 0 AS securedCount
		WHERE NOT EXISTS (
			SELECT s2.SiteId
			FROM (
				SELECT b.SiteId AS SiteId, ms.Id AS MilestoneId
				FROM G2.Building b
				INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
				INNER JOIN G2.Task t ON t.Id = tb.TaskId
				LEFT OUTER JOIN G2.ProjectMilestone pm
				INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
				INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
				LEFT OUTER JOIN G2.MilestoneTemplate mst ON mst.Id = t.MilestoneTemplateId
				WHERE ms.Id = 596 --G2.fnGetBuildingSecureMilestoneId()
				AND pm.CompletedInd = 1
				AND mst.MilestoneTemplateCategoryId NOT IN (10, 15) --  G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA21GMSSubSystemId()), G2.fnGetFollowOnMilestoneTemplateCategoryId(G2.fnGetNA23SubSystemId())
				AND ss.Id = 5 -- G2.fnGetNA21GMSSubSystemId() 
			) s2
		)
	) buildingStatusSecure ON buildingStatusSecure.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT DISTINCT (b.SiteId), ss.Id
		FROM G2.Building b
		INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
		INNER JOIN G2.Task t ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.ProjectMilestone pm
		INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
		INNER JOIN G2.SubSystem ss ON ss.Id = ms.SubSystemId
	) subsystem ON subsystem.SiteId = s.Id
	WHERE s.[Name] NOT LIKE '%No Site Assigned)'
	AND s.DeletedInd = 0
	AND subsystem.Id IN (5, 6); --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwBuildingDataRebuild]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     06/04/2013
-- Release:     4.2
-- Description: Returns building date for the ANL map page
--
-- mm/dd/yy  Name       Release  Description
-- 07/10/13  m.brown    4.2      Modified to not ONLY consider completed milestones when looking at milestones at the building.
-- 07/15/13  m.brown    4.2      Changed Upgrade Status to say "Completed" instead of "Upgrade Complete" 
-- 07/25/13  m.brown    4.3      No longer displays deleted buildings
-- 08/02/13  s.oneal    4.3      Adding SiteId and SiteName to list of returned columns
-- 09/11/13  m.brown    4.4      Included SPAVersion on building now instead of site; SPAStatus added
-- 03/05/14  j.frank    5.0      Updated some field names and inner queries to support changing "country officer" to "portfolio officer".
-- 04/23/14  m.brown    5.0      The last site visit date for the building was considering non-actualized milestones incorrectly.
-- 06/17/14  m.brown    5.1      LastSiteVisit was always returning the building value irrespective of which was greater (building or site)
--                               The LastSiteVisit column now correctly returns the site when the site's last visit date is greater than the building's
-- 11/07/14  m.brown    5.3      Updated to accommodate schema changes in 5.3
-- 10/14/15  CGL        6.3      Modified for latest column inclusion decissions and renames
-- 04/06/16  CGL        7.0      Removed the where clause to exclude "no site assigned" sites, prod support US5227
-- 08/30/18  d.johnson  9.2      Rewrite to fix subsystem split for NA21/NA23 and remove unneeded fields
-- 09/24/18  d.johnson  9.2      Default inventory not tied to a WBS to subsystem 5
-- =============================================================
CREATE VIEW [Geo].[vwBuildingDataRebuild]
AS
	SELECT b.Id AS Id,
		[geometry]::STGeomFromText('POINT(' + CONVERT(VARCHAR(20), b.Longitude) + ' ' + CONVERT(VARCHAR(20), b.Latitude) + '  )', 4326) AS [Geometry],
		CAST(CASE WHEN b.Latitude = 0 AND b.Longitude = 0 THEN 0 ELSE 1 END AS BIT) AS GeometryValid,
		b.[Name],
		buildingSecureMilestone.BuildingSecureComplete,
		CASE WHEN COALESCE(buildingSecureMilestone.BuildingSecureComplete, 0) = 1 THEN buildingSecureMilestone.CompletedBuildingSecureDate
			ELSE buildingSecureMilestone.BuildingSecureDate
		END AS BuildingSecureDate,
		bt.Id AS BuildingTypeId,
		bt.[Name] AS BuildingTypeName,
		b.City,
		s.CountryId,
		c.LongName AS CountryName,
		sourceInfo.CurieCount AS CurieCount,
		initialAssessmentMilestone.InitialAssessmentComplete,
		CASE WHEN COALESCE(initialAssessmentMilestone.InitialAssessmentComplete, 0) = 1 THEN
				 initialAssessmentMilestone.CompletedInitialAssessmentDate
			ELSE initialAssessmentMilestone.InitialAssessmentDate
		END AS InitialAssessmentDate,
		CASE WHEN sourceInfo.NucSources > 0
			 AND sourceInfo.RadSources > 0 THEN 'Both'
			WHEN sourceInfo.NucSources > 0 THEN 'Nuclear'
			WHEN sourceInfo.RadSources > 0 THEN 'Radiological'
		END AS MaterialTypeName,
		sourceInfo.NucMaterialAmount AS NucMaterialAmount,
		nucMaterialList.NucMaterialList AS NucMaterialType,
		sourceInfo.SourceCount AS SourcesCount,
		b.Address1 AS PrimaryStreetAddress,
		r.[Name] AS RegionName,
		b.Address2 AS SecondaryStreetAddress,
		s.Id AS SiteId,
		s.[Name] AS SiteName,
		st.[Name] AS StateName,
		CASE WHEN (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
				  AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			 )
			 OR (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
				AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			 ) THEN
				 CASE WHEN COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate, '19000101') > COALESCE(
																														  buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate,
																														  '19000101'
																													  ) THEN
						  siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
					 ELSE buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
				 END
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0 THEN
				buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1 THEN
				siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete
		END AS SustainabilityAssessmentComplete,
		CASE WHEN (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
				  AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			 )
			 OR (
				 COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
				AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			 ) THEN
				 CASE WHEN COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate, '19000101') > COALESCE(
																														  buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate,
																														  '19000101'
																													  ) THEN
						  siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
					 ELSE buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
				 END
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0 THEN
				buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
			WHEN COALESCE(buildingSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 0
			AND COALESCE(siteSustainabilityAssessmentMilestone.SustainabilityAssessmentComplete, 0) = 1 THEN
				siteSustainabilityAssessmentMilestone.SustainabilityAssessmentDate
		END AS SustainabilityAssessmentDate,
		CASE WHEN upgradeStatusQuery.MinCompletedInd = 1 THEN 'Complete'
			WHEN upgradeStatusQuery.MaxCompletedInd = 0 THEN 'Not Started'
			WHEN upgradeStatusQuery.MinCompletedInd = 0
			AND upgradeStatusQuery.MaxCompletedInd = 1 THEN 'In Progress'
			WHEN b.InScopeInd = 1
			AND upgradeStatusQuery.MinCompletedInd IS NULL THEN 'Unplanned'
			ELSE NULL
		END AS UpgradeStatus,
		COALESCE(subsystem.Id, 5) AS SubSystemId --G2.fnGetNA21GMSSubSystemId()
	FROM G2.Building b
	INNER JOIN G2.[Site] s ON b.SiteId = s.Id
	INNER JOIN G2.BuildingType bt ON b.BuildingTypeId = bt.Id
	INNER JOIN G2.Country c ON s.CountryId = c.Id
	LEFT OUTER JOIN G2.[State] st ON s.StateId = st.Id
	LEFT OUTER JOIN Inventory.RegionStateXRef rsxr ON st.Id = rsxr.StateId
	LEFT OUTER JOIN Inventory.Region r ON rsxr.RegionId = r.Id
	LEFT OUTER JOIN (
		SELECT b.Id AS BuildingId,
			MIN(pm.FinishDt) AS InitialAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedInitialAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS InitialAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.Building b ON tb.BuildingId = b.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND m.[Name] = 'Conduct Site Assessment'
		GROUP BY b.Id
	) initialAssessmentMilestone ON initialAssessmentMilestone.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT b.Id AS BuildingId,
			MIN(pm.FinishDt) AS BuildingSecureDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedBuildingSecureDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS BuildingSecureComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.Building b ON tb.BuildingId = b.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND m.[Name] = 'Building Secure'
		GROUP BY b.Id
	) buildingSecureMilestone ON buildingSecureMilestone.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT b.Id AS BuildingId,
			MIN(pm.FinishDt) AS SustainabilityAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedSustainabilityAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS SustainabilityAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.Building b ON tb.BuildingId = b.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND m.[Name] = 'Sustainability Assessment'
		GROUP BY b.Id
	) buildingSustainabilityAssessmentMilestone ON buildingSustainabilityAssessmentMilestone.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT ts.SiteId,
			MIN(pm.FinishDt) AS SustainabilityAssessmentDate,
			MIN(CASE WHEN pm.CompletedInd = 1 THEN pm.FinishDt END) AS CompletedSustainabilityAssessmentDate,
			CAST(MAX(CAST(pm.CompletedInd AS INT)) AS BIT) AS SustainabilityAssessmentComplete
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndCancelledStates vwacs ON vwacs.WorkBreakdownStructureStatusId = taskWBS.StatusId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		INNER JOIN G2.TaskSite ts ON t.Id = ts.TaskId
		INNER JOIN G2.[Site] s ON ts.SiteId = s.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND m.[Name] = 'Sustainability Assessment'
		GROUP BY ts.SiteId
	) siteSustainabilityAssessmentMilestone ON siteSustainabilityAssessmentMilestone.SiteId = s.Id
	LEFT OUTER JOIN (
		SELECT b.Id AS BuildingId, MIN(CAST(pm.CompletedInd AS INT)) AS MinCompletedInd, MAX(CAST(pm.CompletedInd AS INT)) AS MaxCompletedInd
		FROM G2.Task t
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.Building b ON tb.BuildingId = b.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND m.[Name] = 'Building Secure'
		GROUP BY b.Id
	) upgradeStatusQuery ON upgradeStatusQuery.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT b.Id AS BuildingId, MIN(CAST(pm.CompletedInd AS INT)) AS MinCompletedInd, MAX(CAST(pm.CompletedInd AS INT)) AS MaxCompletedInd
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure taskWBS ON taskWBS.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.WorkBreakdownStructure projWBS ON projWBS.Id = taskWBS.ParentId
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkBreakdownStructureId = projWBS.Id
		INNER JOIN G2.WorkType wt ON wt.Id = wbswtx.WorkTypeId
		INNER JOIN G2.WorkTypeGroup wtg ON wtg.Id = wt.WorkTypeGroupId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
		LEFT OUTER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.Building b ON tb.BuildingId = b.Id
		WHERE m.SubSystemId IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
		AND wtg.[Name] = 'Protect'
		GROUP BY b.Id
	) protectUpgradeStatusQuery ON protectUpgradeStatusQuery.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT vmas.BuildingId,
			SUM(CASE WHEN COALESCE(vmas.RadReportedActivity, 0) > COALESCE(vmas.LicensedMaximum, 0) THEN vmas.RadReportedActivity ELSE
vmas			.LicensedMaximum END
			) AS CurieCount,
			COUNT(CASE WHEN vmas.SourceId > 1 THEN 1 ELSE 0 END) AS SourceCount,
			SUM(CAST(COALESCE(vmas.DispositionInd, 0) AS INT)) AS SourcesDispositioned,
			SUM(vmas.NucFissileAmount) AS NucMaterialAmount,
			SUM(CASE WHEN vmas.IDDStatusId = 1 THEN 1 ELSE 0 END) AS IDDFieldHardenedDevices,
			SUM(CASE WHEN vmas.IDDStatusId = 3 THEN 1 ELSE 0 END) AS IDDFactoryHardenedDevices,
			MIN(vmas.IDDInstallDt) AS MinIDDInstallDt,
			SUM(CASE WHEN vmas.MaterialTypeId = 1 THEN 1 ELSE 0 END) AS RadSources,
			SUM(CASE WHEN vmas.MaterialTypeId = 2 THEN 1 ELSE 0 END) AS NucSources,
			SUM(CAST(COALESCE(vmas.RMSInstalledInd, 0) AS INT)) AS RMSInstalledCount
		FROM Inventory.vwMaterialAtSite vmas
		GROUP BY vmas.BuildingId
	) sourceInfo ON sourceInfo.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT BuildingId,
			SUBSTRING((
						  SELECT ', ' + FissileNuclideName
						  FROM (SELECT vmas.BuildingId, vmas.FissileNuclideName FROM Inventory.vwMaterialAtSite vmas WHERE vmas.MaterialTypeId = 2) b
						  WHERE b.BuildingId = x.BuildingId
						  FOR XML PATH('')
					  ), 3, 2000
			) AS NucMaterialList
		FROM (SELECT b.Id AS BuildingId FROM G2.Building b) x
		GROUP BY x.BuildingId
	) nucMaterialList ON nucMaterialList.BuildingId = b.Id
	LEFT OUTER JOIN (
		SELECT DISTINCT (b.SiteId), ss.Id
		FROM G2.Building b
		INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
		INNER JOIN G2.Task t ON t.Id = tb.TaskId
		LEFT OUTER JOIN G2.ProjectMilestone pm
		INNER JOIN G2.Milestone ms ON ms.Id = pm.MilestoneId ON pm.TaskId = t.Id
		INNER JOIN G2.SubSystem ss ON ss.Id IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
	) subsystem ON subsystem.SiteId = s.Id
	WHERE b.DeletedInd = 0
	AND COALESCE(subsystem.Id, 5) IN (5, 6) --G2.fnGetNA21GMSSubSystemId(), G2.fnGetNA23SubSystemId()
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[vwSiteMilestones]'
GO

---------------------------------------------------------------------------------------------------
-- Author:      Nevada Williford
-- Created:     2013-03-05
-- Description:	Return the first and last dates for the conduct site assessment, award contract, and 
--              building secured milestones for each site. Also returns indicator fields for whether 
--              some buildings have completed the milestone or all buildings have completed the milestone. 

-- mm/dd/yy  Name     Release  Description
-- 11/13/15  JNW      6.3.1    Replaced the MilestoneId and MilestoneTemplateCategoryId values with the 
--                             new ones tied to GMS SubSystem. 
---------------------------------------------------------------------------------------------------
CREATE VIEW [Calendar].[vwSiteMilestones]
AS
SELECT 
    SiteId,
    MIN(m.FirstAssessmentCompletedDate)  AS FirstAssessmentCompletedDate,
    MAX(m.LastAssessmentCompletedDate)   AS LastAssessmentCompletedDate,
    MIN(m.FirstContractAwardedDate)      AS FirstContractAwardedDate,
    MAX(m.LastContractAwardedDate)       AS LastContractAwardedDate,
    MIN(m.FirstBuildingSecuredDate)      AS FirstBuildingSecuredDate,
    MAX(m.LastBuildingSecuredDate)       AS LastBuildingSecuredDate,
    MAX(m.SomeAssessmentsCompletedFlag)  AS SomeAssessmentsCompletedFlag,
    MAX(m.AllAssessmentsCompletedFlag)   AS AllAssessmentsCompletedFlag,
    MAX(m.SomeContractsAwardedFlag)      AS SomeContractsAwardedFlag,
    MAX(m.AllContractsAwardedFlag)       AS AllContractsAwardedFlag,
    MAX(m.SomeBuildingsSecuredFlag)      AS SomeBuildingsSecuredFlag,
    MAX(m.AllBuildingsSecuredFlag)       AS AllBuildingsSecuredFlag
FROM
    (
        -- G2.fnGetBuildingSecureMilestoneId(), G2.fnGetConductSiteAssessmentMilestoneId(), G2.fnGetAwardContractMilestoneId()
        SELECT 
            s.Id AS SiteId,
            CASE WHEN pm.MilestoneId = 599  THEN MIN(pm.FinishDt)                                       END AS FirstAssessmentCompletedDate, 
            CASE WHEN pm.MilestoneId = 599  THEN MAX(pm.FinishDt)                                       END AS LastAssessmentCompletedDate, 
            CASE WHEN pm.MilestoneId = 595  THEN MIN(pm.FinishDt)                                       END AS FirstContractAwardedDate, 
            CASE WHEN pm.MilestoneId = 595  THEN MAX(pm.FinishDt)                                       END AS LastContractAwardedDate, 
            CASE WHEN pm.MilestoneId = 596   THEN MIN(pm.FinishDt)                                       END AS FirstBuildingSecuredDate,
            CASE WHEN pm.MilestoneId = 596   THEN MAX(pm.FinishDt)                                       END AS LastBuildingSecuredDate,
            CASE WHEN pm.MilestoneId = 599  THEN MAX(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS SomeAssessmentsCompletedFlag, 
            CASE WHEN pm.MilestoneId = 599  THEN MIN(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS AllAssessmentsCompletedFlag, 
            CASE WHEN pm.MilestoneId = 595  THEN MAX(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS SomeContractsAwardedFlag, 
            CASE WHEN pm.MilestoneId = 595  THEN MIN(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS AllContractsAwardedFlag, 
            CASE WHEN pm.MilestoneId = 596   THEN MAX(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS SomeBuildingsSecuredFlag,
            CASE WHEN pm.MilestoneId = 596   THEN MIN(CONVERT(INT,pm.CompletedInd)) ELSE CONVERT(INT, 0) END AS AllBuildingsSecuredFlag
        FROM 
            G2.[Site] s
            LEFT JOIN G2.Building b 
                ON b.SiteId = s.Id
            LEFT JOIN G2.TaskBuilding tb 
                ON tb.BuildingId = b.Id
            LEFT JOIN G2.Task t 
                ON t.Id = tb.TaskId
            LEFT JOIN g2.MilestoneTemplate mt 
                ON mt.Id = t.MilestoneTemplateId
            LEFT JOIN g2.MilestoneTemplateCategory mtc 
                ON mtc.Id = mt.MilestoneTemplateCategoryId
            INNER JOIN G2.ProjectMilestone pm 
                ON pm.TaskId = t.Id
        WHERE 
            pm.MilestoneId IN( 596,599,595) -- G2.fnGetBuildingSecureMilestoneId(), G2.fnGetConductSiteAssessmentMilestoneId(), G2.fnGetAwardContractMilestoneId()
            AND mtc.Id = 11 -- G2.fnGetImplementationMilestoneTemplateCategoryId()
        GROUP BY 
            s.Id, 
            pm.MilestoneId
    ) m
GROUP BY
    SiteId 



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[Site]'
GO

CREATE VIEW [Contact].[Site] AS SELECT * FROM Contact.Facility F WHERE F.FacilityTypeId = 1;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[Building]'
GO

CREATE VIEW [Contact].[Building] AS SELECT * FROM Contact.Facility F WHERE F.FacilityTypeId = 2;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[vwSearchReportingEntity]'
GO
--Lab view and indexes
CREATE VIEW [Proposal].[vwSearchReportingEntity]
WITH SCHEMABINDING 
AS
	SELECT o.PartyId, o.OrganizationBreakdownStructure, COUNT_BIG(*) AS [RowCount]
	FROM Contact.Organization o 
	INNER JOIN Proposal.RequestReportingEntityXref x ON o.PartyId = x.ReportingEntityOrganizationPartyId
	INNER JOIN Proposal.Request r ON x.RequestId = r.Id
	GROUP BY o.PartyId, o.OrganizationBreakdownStructure;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_vwSearchReportingEntity] on [Proposal].[vwSearchReportingEntity]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_vwSearchReportingEntity] ON [Proposal].[vwSearchReportingEntity] ([PartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[vwOrganization]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     5/20/2015
-- Release:     6.0 
-- Description: This view selects directly from the organization table 
--              without hierarchy id because CLR types don't work across
--              linked servers
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [Notification].[vwOrganization] WITH SCHEMABINDING
AS
SELECT o.PartyId, o.OrganizationBreakdownStructure, o.Name, o.ParentId, o.SortOrder, o.ActiveInd, o.CreatedDt, o.CreatedBy,
       o.CreatedAs, o.LastModifiedDt, o.LastModifiedBy, o.LastModifiedAs 
FROM Contact.Organization o;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ui_vwOrganization] on [Notification].[vwOrganization]'
GO
CREATE UNIQUE CLUSTERED INDEX [ui_vwOrganization] ON [Notification].[vwOrganization] ([PartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[Room]'
GO

CREATE VIEW [Contact].[Room] AS SELECT * FROM Contact.Facility F WHERE F.FacilityTypeId = 3;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[vwHistoricalProjectMilestoneChange]'
GO


-- =============================================================
-- Author:      Mark Brown
-- Created:     6/11/12
-- Release:     3.3
-- Description: Created to replace the vwHistoricalProjectMilestoneBaselineChange
--
-- mm/dd/yy  Name      Release  Description
-- 06/20/12  s.oneal   3.3      Removed the Id column from results
-- 05/14/14  j.borgers 5.1      Added SequenceNumber
-- =============================================================
CREATE VIEW [Baseline].[vwHistoricalProjectMilestoneChange]
AS
	SELECT  TaskChangeId
		  , ProjectMilestoneId
		  , MilestoneId
		  , SequenceNumber
		  , BaselineFinishDt
		  , CompletedInd
		  , CAST(NULL AS datetime) AS DeletedDt
	FROM    Baseline.ProjectMilestoneChange pmc

	UNION

	SELECT  TaskChangeId
		  , ProjectMilestoneId
		  , MilestoneId
		  , SequenceNumber
		  , BaselineFinishDt
		  , CompletedInd
		  , DeletedDt
	FROM    Audit.DeletedProjectMilestoneChange pmc



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[vwReportingEntityAllotteeXref]'
GO

CREATE VIEW [Contact].[vwReportingEntityAllotteeXref]
WITH SCHEMABINDING
AS
	SELECT ppr.SubSystemId, ppr.PartyId AS ReportingEntityOrganizationPartyId, apr.PartyId AS AllotteeOrganizationPartyId
	FROM Contact.PartyRole ppr
	INNER JOIN Contact.PartyRelationship pr ON ppr.Id = pr.FromPartyRoleId
	INNER JOIN Contact.PartyRole apr ON pr.ToPartyRoleId = apr.Id
	WHERE ppr.PartyRoleTypeId = 6 -- Contact.fnGetPerformerPartyRoleTypeId()
	AND pr.PartyRelationshipTypeId = 4 -- Contact.fnGetPerformerAllotteeRollupPartyRelationshipTypeId()
	AND apr.PartyRoleTypeId = 8 -- Contact.fnGetAllotteePartyRoleTypeId()
	AND pr.ThruDate IS NULL


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[vwSolicitationStatusActiveStates]'
GO
CREATE VIEW [Proposal].[vwSolicitationStatusActiveStates]
WITH SCHEMABINDING
AS
    SELECT ss.Id AS SolicitationStatusId, ss.Name AS SolicitationStatusName
    FROM Proposal.SolicitationStatus ss
    WHERE ss.Id IN (1, 2); --Proposal.fnGetOpenSolicitationStatusId and Proposal.fnGetClosedSolicitationStatusId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusActiveAndInactiveStates]'
GO


-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/09/2014
-- Release:     5.3
-- Description: Returns active and inactive request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusActiveAndInactiveStates]
WITH SCHEMABINDING
AS
    SELECT wbss.Id AS WorkBreakdownStructureStatusId,
           wbss.Name AS WorkBreakdownStructureStatusName
    FROM G2.WorkBreakdownStructureStatus wbss
    INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
    INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
    WHERE wbsss.Id IN (1, 2);   --   G2.fnGetActiveWorkBreakdownStructureStatusStateId, G2.fnGetInactiveWorkBreakdownStructureStatusStateId

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusActiveAndInactiveStates] on [G2].[vwWorkBreakdownStructureStatusActiveAndInactiveStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusActiveAndInactiveStates] ON [G2].[vwWorkBreakdownStructureStatusActiveAndInactiveStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwTaskAttributeTypeMetricXref]'
GO

-- =============================================================
-- Author:      Chris Luttrell
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwTaskAttributeTypeMetricXref]
WITH SCHEMABINDING
AS
	SELECT sfmt.WorkTypeId, mt.Id AS MilestoneTemplateId, mt.DefaultTaskAttributeTypeId, mtdm.MetricId, mtd.MilestoneId, mtd.SequenceNumber
	FROM G2.MilestoneTemplate mt
	INNER JOIN G2.WorkTypeMilestoneTemplateXref sfmt ON mt.Id = sfmt.MilestoneTemplateId
	INNER JOIN G2.MilestoneTemplateDetail mtd ON mt.Id = mtd.MilestoneTemplateId
	INNER JOIN G2.MilestoneTemplateDetailMetric mtdm ON mtd.Id = mtdm.MilestoneTemplateDetailId
	WHERE mt.DefaultTaskAttributeTypeId IS NOT NULL;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwPortfolioOfficerList]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/11/2014
-- Description: Retrieves a concatenated list of portfolio
--   officers.
--
-- mm/dd/yy  Name      Release  Description
-- 09/30/14  s.oneal   5.3      Modified to work the the schedule data model changes and new
--                              security data model
-- 11/25/14  j.borgers 5.3      Took out hardcoded database name
-- 05/19/15  CG:       6.0      Adding in SubsystemId and ordering PO List
-- 09/22/15  a.smith   6.3      Modified to include equivalent roles for NA-21 GMS, NA-23, NA-50, & NA-193.
-- 10/21/15  b.roland  6.3      Fixed issue with duplicate names when user had multiple roles
-- =============================================================
CREATE VIEW [G2].[vwPortfolioOfficerList]
AS
    WITH PortfolioOfficers
	AS
	(
		SELECT DISTINCT persR.SubSystemId, persR.WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Security.PersonRole persR
		INNER JOIN Security.Person p ON persR.PersonId = p.Id
		WHERE persR.RoleId IN (33, 82, 83, 103, 104) -- Security.fnGetPortfolioOfficerRoleId, FinPlan: Portfolio Officer (Primary)/(Secondary); Schedule: Portfolio Officer (Primary)/(Secondary)
	),
	PortfolioOfficerList
	AS
	(
        SELECT p.SubSystemId, p.WorkBreakdownStructureId, (SELECT p2.Name + ', ' FROM PortfolioOfficers p2 WHERE p.WorkBreakdownStructureId = p2.WorkBreakdownStructureId ORDER BY p2.Name FOR XML PATH('')) AS PortfolioOfficers
        FROM PortfolioOfficers p
        GROUP BY p.SubSystemId, p.WorkBreakdownStructureId
    )
	SELECT p.SubSystemId, p.WorkBreakdownStructureId, wbscx.CountryId, LEFT(p.PortfolioOfficers, (LEN(p.PortfolioOfficers) - 1)) AS PortfolioOfficers
    FROM PortfolioOfficerList p
	LEFT OUTER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON p.WorkBreakdownStructureId = wbscx.WorkBreakdownStructureId;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Performance].[vwHistoricalProjectMilestoneMetricStatus]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     10/17/2014
-- Release:     5.3
-- Description: Combines the the base and audit version of the
--   ProjectMilestoneMetricStatusTable
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [Performance].[vwHistoricalProjectMilestoneMetricStatus]
AS
	SELECT Id, ChangeRequestDetailId, ProjectMilestoneId, MetricId, ForecastValue, CAST(NULL AS datetime) AS DeletedDt
	FROM G2.ProjectMilestoneMetricStatus

	UNION

	SELECT Id, ChangeRequestDetailId, AuditProjectMilestoneId, MetricId, ForecastValue, DeletedDt
	FROM Audit.DeletedProjectMilestoneMetricStatus
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Performance].[vwHistoricalProjectMilestoneStatus]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     10/17/2014
-- Release:     5.3
-- Description: Combines the the base and audit version of the
--   ProjectMilestoneStatusTable
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [Performance].[vwHistoricalProjectMilestoneStatus]
AS
	SELECT Id, ChangeRequestDetailId, ProjectMilestoneId, FinishDt, CompletedInd, CAST(NULL AS datetime) AS DeletedDt
	FROM G2.ProjectMilestoneStatus

	UNION

	SELECT Id, ChangeRequestDetailId, AuditProjectMilestoneId, FinishDt, CompletedInd, DeletedDt
	FROM Audit.DeletedProjectMilestoneStatus
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwJouleMetrics]'
GO
-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     6/29/2007
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- 02/21/13  CGL      4.0      Removing ProjectId and MilestoneTemplateId from ProjectMilestone table
-- 09/19/14  s.oneal  5.3      Modified to return TaskId
-- 09/29/14  s.oneal  5.3      Added back in EquivalentXYZ columns
-- =============================================================
CREATE VIEW [G2].[vwJouleMetrics]
AS
	SELECT tw.ParentId AS WorkBreakdownStructureId, pm.TaskId, pm.CompletedInd,
		CONVERT(bit, CASE WHEN s.StartDt > CURRENT_TIMESTAMP THEN 1 ELSE 0 END) AS FutureInd, 
		m.EquivalentType, m.EquivalentFactor,
		m.Unit, pmm.ForecastValue AS MetricValue, m.Name AS MetricName, m.Id AS MetricId,
		CONVERT(numeric(18,3), pmm.ForecastValue / m.EquivalentFactor) AS EquivalencyValue,
		CONVERT(nvarchar, CONVERT(numeric(18,3), pmm.ForecastValue / m.EquivalentFactor)) + ' ' + m.EquivalentType AS EquivalencyText,
		prd2.Id AS BaselinePeriodId, prd.Id AS PeriodId, pm.FinishDt, pm.BaselineFinishDt
	FROM G2.ProjectMilestoneMetric pmm
	INNER JOIN G2.Metric m ON pmm.MetricId = m.Id
	INNER JOIN G2.ProjectMilestone pm ON pmm.ProjectMilestoneId = pm.Id
	INNER JOIN G2.Task t ON pm.TaskId = t.Id
	INNER JOIN G2.WorkBreakdownStructure tw ON t.WorkBreakdownStructureId = tw.Id
	INNER JOIN G2.Schedule s ON tw.ParentId = s.WorkBreakdownStructureId
	INNER JOIN G2.Period prd ON pm.FinishDt BETWEEN prd.BeginDt AND prd.EndDt
	INNER JOIN G2.Period prd2 ON pm.BaselineFinishDt BETWEEN prd2.BeginDt AND prd2.EndDt
	WHERE pm.JouleMetricInd = 1;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwActiveFootNotes]'
GO

-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     ?
-- Release:     ?
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- 02/21/13  CGL      4.0      Removing ProjectId and MilestoneTemplateId from ProjectMilestone table
-- 09/15/14  s.oneal  5.3      Modified to work with multi sub-system schedule data model changes
-- =============================================================
CREATE VIEW [G2].[vwActiveFootNotes]
AS
	WITH ActiveFootnotes_CTE
	AS
	(
		SELECT DISTINCT w.ParentId AS ProjectWorkBreakdownStructureId, mtf.DisplayChars
		FROM G2.MilestoneTemplateFootNote mtf
		INNER JOIN G2.MilestoneTemplate mt ON mtf.MilestoneTemplateId = mt.Id
		INNER JOIN G2.Task t ON t.MilestoneTemplateId = mt.Id
		INNER JOIN G2.WorkBreakdownStructure w ON t.WorkBreakdownStructureId = w.Id
		WHERE mtf.ActiveInd = 1
	),
	CombinedFootnotes_CTE
	AS
	(
		SELECT af.ProjectWorkBreakdownStructureId, (SELECT af2.DisplayChars + ',' FROM ActiveFootnotes_CTE af2 WHERE af.ProjectWorkBreakdownStructureId = af2.ProjectWorkBreakdownStructureId FOR XML PATH('')) AS FootNotes
		FROM ActiveFootnotes_CTE af
		GROUP BY af.ProjectWorkBreakdownStructureId
	)
	SELECT f.ProjectWorkBreakdownStructureId, LEFT(f.FootNotes, (LEN(f.FootNotes) - 1)) AS FootNotes
	FROM CombinedFootnotes_CTE f
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwMilestoneMaxDisplayOrder]'
GO


-- =============================================================
-- Author:      Chris Luttrell/Diedre Cantrell
-- Created:     4/9/2010
-- Description: This View is used to determine MaxDisplayOrder
-- for Milestones across ProjectSubFunctions.  Contains extra
-- fields for development

-- mm/dd/yy  Name      Release  Description
-- 09/10/10  DNC                Add MilestoneTemplateCategory info
-- 09/30/14  j.borgers 5.3      Modified to work with schedule data structure changes
-- =============================================================
CREATE VIEW [G2].[vwMilestoneMaxDisplayOrder]
AS
	SELECT TOP 1000 wt.WorkTypeGroupId,
		wtg.Name AS WorkTypeGroup,
		wt.WorkTypeCategoryId,
		wtc.Name AS WorkTypeCategory,
		wtmtx.WorkTypeId,
		wt.LongName AS WorkTypeName,
		wt.DisplayOrder AS WorkTypeDisplayOrder,
		wt.DashboardFilterInd,
		mtc.Id AS MilestoneTemplateCategoryId,
		mtc.Name AS MilestoneTemplateCategory,
		mt.MilestoneTemplateTimeFrameId,
		mttf.Name AS MilestoneTemplateTimeFrame,
		CASE mt.MilestoneTemplateTimeFrameId 
			WHEN  2 THEN 1
			WHEN  3 THEN 2
			WHEN  1 THEN 3
		END AS RealTimeFrame,
		mt.Id AS MilestoneTemplateId,
		mt.Name AS MilestoneTemplateName,
		mtd.MilestoneId,
		mtd.SequenceNumber,
		m.Name AS MilestoneName,
		mtd.DisplayOrder,
		MIN(mtd.DisplayOrder) OVER (PARTITION BY wt.Id, m.Id) AS MinDispOrd,
		MAX(mtd.DisplayOrder) OVER (PARTITION BY wt.Id, m.Id) AS MaxDispOrd,
		mt.FacilityRequiredInd,
		mtd.MilestoneWeight,
		mtd.JouleMetricInd,
		mtd.InternalMetricInd
	FROM G2.WorkTypeMilestoneTemplateXref wtmtx
	INNER JOIN G2.WorkType wt ON wtmtx.WorkTypeId = wt.Id
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	INNER JOIN G2.WorkTypeCategory wtc ON wt.WorkTypeCategoryId = wtc.Id
	INNER JOIN G2.MilestoneTemplate mt ON wtmtx.MilestoneTemplateId = mt.Id
	INNER JOIN G2.MilestoneTemplateTimeFrame mttf ON mt.MilestoneTemplateTimeFrameId = mttf.Id
	INNER JOIN G2.MilestoneTemplateDetail mtd ON mt.Id = mtd.MilestoneTemplateId
	INNER JOIN G2.Milestone M ON mtd.MilestoneId = M.Id
	LEFT OUTER JOIN G2.MilestoneTemplateCategory mtc ON mt.MilestoneTemplateCategoryId = mtc.Id
	ORDER BY wt.WorkTypeGroupId,wt.DisplayOrder, MaxDispOrd, RealTimeFrame
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[vwOperatingStatusClassificationTypeIdNotScorable]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     6/22/16
-- Release:     7.1
-- Description: List of Statuses that aren't scorable
--
-- mm/dd/yy  Name      Release  Description
-- 12/08/16  j.borgers 7.4		Changed from Status Description to Status
-- 06/27/18  j.borgers 9.1      Renamed and modified to use ClassificationTypeId's 
-- 07/23/18  j.borgers 9.1      Modifed so the view is indexable
-- =============================================================
CREATE VIEW [AssetManagement].[vwOperatingStatusClassificationTypeIdNotScorable]
WITH SCHEMABINDING
AS
	SELECT ct.Id AS ClassificationTypeId
	FROM AssetManagement.ClassificationType ct
	WHERE ct.Id IN (12, 13, 15); --AssetManagement.fnGetExcessToSiteClassificationTypeId(), AssetManagement.fnGetExcessToDOEClassificationTypeId(), AssetManagement.fnGetArchivedClassificationTypeId()
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[vwSiteWorkBreakdownStructure]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     9/10/2015
-- Release:     6.3
-- Description: A view linking sites to Work Breakdown Structure 
--              for SPA.
--
-- mm/dd/yy  Name        Release  Description
-- 10/07/16  Strickland  7.3      Added support for new SubSystem Level Sub Office
-- =============================================================
CREATE VIEW [SPA].[vwSiteWorkBreakdownStructure] 
AS
		
	SELECT 
	    COALESCE(sub_system.Id, -1) SubSystemId, 
        COALESCE(sub_system.Name, '') SubSystemName, 
        COALESCE(sub_system.ShortName, '') SubSystemShortName,
        COALESCE(wbs_program.Id, -1) ProgramWorkBreakdownStructureId,
		COALESCE(wbs_program.Name, '') ProgramWorkBreakdownStructureName,
		COALESCE(wbs_office.Id, -1) OfficeWorkBreakdownStructureId,
		COALESCE(wbs_office.Name, '') OfficeWorkBreakdownStructureName,
		COALESCE(wbs_sub_office.Id, -1) SubOfficeWorkBreakdownStructureId,
		COALESCE(wbs_sub_office.Name, '') SubOfficeWorkBreakdownStructureName,
		COALESCE(wbs_sub_program.Id, -1) SubProgramWorkBreakdownStructureId,
		COALESCE(wbs_sub_program.Name, '') SubProgramWorkBreakdownStructureName,
		COALESCE(CASE WHEN s.CountryId = 1 THEN wbs_portfolio_domestic.Id ELSE wbs_portfolio_international.Id END, -1) PortfolioWorkBreakdownStructureId,
		COALESCE(CASE WHEN s.CountryId = 1 THEN wbs_portfolio_domestic.Name ELSE wbs_portfolio_international.Name END, '') PortfolioWorkBreakdownStructureName,
		COALESCE(s.Id, -1) SiteId,
		COALESCE(s.[Name], '') SiteName
	FROM G2.[Site] s
	INNER JOIN G2.Country c
        ON s.CountryId = c.Id
	INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx 
		ON wbscx.CountryId = c.Id
    -- Sub-Program assuming a one-to-one match between G2.Country and G2.WorkBreakdownStructure via G2.WorkBreakdownStructureCountryXref
    INNER JOIN G2.WorkBreakdownStructure wbs_sub_program
		ON wbs_sub_program.Id = wbscx.WorkBreakdownStructureId
        AND wbs_sub_program.SubSystemLevelId = G2.fnGetSubProgramSubSystemLevelId(5)
    --------------------------------------------------------------------------------------------
    -- Sub Office assuming Sub Office is the direct parent of Sub-Program
	INNER JOIN G2.WorkBreakdownStructure wbs_sub_office 
		ON wbs_sub_office.Id = wbs_sub_program.ParentId 
        AND wbs_sub_office.SubSystemLevelId = G2.fnGetSubOfficeSubSystemLevelId(5)
    --------------------------------------------------------------------------------------------
    -- Office assuming Office is the direct parent of Sub Office
	INNER JOIN G2.WorkBreakdownStructure wbs_office 
		ON wbs_office.Id = wbs_sub_office.ParentId 
        AND wbs_office.SubSystemLevelId = G2.fnGetOfficeSubSystemLevelId(5)
    -- Program assuming Program is the direct parent of Office
    INNER JOIN G2.WorkBreakdownStructure wbs_program
        ON wbs_program.Id = wbs_office.ParentId
        AND wbs_program.SubSystemLevelId = G2.fnGetProgramSubSystemLevelId(5)
    -- Sub-system
    INNER JOIN G2.SubSystem sub_system
        ON sub_system.Id = wbs_sub_program.SubSystemId
    -- Portfolio (international) assuming that the portfolio is the direct descendant of the sub-program 
    LEFT JOIN G2.WorkBreakdownStructure wbs_portfolio_international
        INNER JOIN G2.WorkBreakdownStructure wbs_project_international
            ON wbs_project_international.ParentId = wbs_portfolio_international.Id
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbs_worktype_xref
			ON wbs_worktype_xref.WorkBreakdownStructureId = wbs_project_international.Id
		INNER JOIN G2.WorkType wt 
			ON wt.Id = wbs_worktype_xref.WorkTypeId
			AND wt.WorkTypeGroupId = 15 -- GMS Protect Work Type Group
        ON wbs_portfolio_international.ParentId = wbs_sub_program.Id
        AND wbs_portfolio_international.SubSystemLevelId = G2.fnGetPortfolioSubSystemLevelId(5)
    -- Portfolio (domestic) assuming that the portfolio is the parent of the state-level projects
    LEFT JOIN G2.WorkBreakdownStructureStateXref wbssx
        INNER JOIN G2.WorkBreakdownStructure wbs_project
            ON wbs_project.Id = wbssx.WorkBreakdownStructureId
            AND wbs_project.SubSystemLevelId = G2.fnGetProjectSubSystemLevelId(5)
        INNER JOIN G2.WorkBreakdownStructure wbs_portfolio_domestic
            ON wbs_portfolio_domestic.Id = wbs_project.ParentId
            AND wbs_portfolio_domestic.SubSystemLevelId = G2.fnGetPortfolioSubSystemLevelId(5)
        INNER JOIN G2.State st 
            ON st.Id = wbssx.StateId
        ON wbssx.StateId = s.StateId
    GROUP BY 
        sub_system.Id, sub_system.Name, sub_system.ShortName,
        wbs_program.Id, wbs_program.Name, 
		wbs_office.Id, wbs_office.Name,
		wbs_sub_office.Id, wbs_sub_office.Name,
		wbs_sub_program.Id, wbs_sub_program.Name, 
		s.CountryId, 
        wbs_portfolio_domestic.Id, wbs_portfolio_domestic.Name, 
        wbs_portfolio_international.Id, wbs_portfolio_international.Name, 
		s.Id, s.[Name];


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Geo].[vwPerformerLocation]'
GO

-- =============================================================
-- Author:      Christopher Luttrell (CORP\chris.luttrell)
-- Created:     11/16/2015
-- Release:     6.4
-- Description: Returns Perfomer MapPoint data for the ANL map page
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [Geo].[vwPerformerLocation]
AS
	SELECT	RE.OrganizationPartyId ReportingEntityOrganizationPartyId, PMP.Latitude, PMP.Longitude
	FROM	Contact.PartyMapPoint PMP
	INNER JOIN Contact.ReportingEntity RE ON PMP.PartyId = RE.OrganizationPartyId;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[InternalOrganization]'
GO
-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     9/24/13
-- Release:     4.4
-- Description: View defining Internal Organizations
--
-- mm/dd/yy  Name      Release  Description
-- 09/28/15  j.borgers 6.3      Added check for DeletedInd
-- =============================================================
CREATE VIEW [Contact].[InternalOrganization] 
WITH SCHEMABINDING
AS
SELECT  O.PartyId,
        O.OrganizationBreakdownStructure,
        O.Name,
        O.ParentId,
        O.SortOrder,
        PR.Id PartyRoleId,
        PR.PartyRoleTypeId
FROM Contact.Organization O
INNER JOIN Contact.Party pty ON O.PartyId = pty.Id
INNER JOIN Contact.PartyRole PR ON PR.PartyId = O.PartyId
WHERE PR.PartyRoleTypeId = 2
AND pty.DeletedInd = 0
WITH CHECK OPTION;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [PK_InternalOrganization] on [Contact].[InternalOrganization]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_InternalOrganization] ON [Contact].[InternalOrganization] ([PartyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ui_InternalOrganization_OrganizationBreakDownStructure] on [Contact].[InternalOrganization]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [ui_InternalOrganization_OrganizationBreakDownStructure] ON [Contact].[InternalOrganization] ([OrganizationBreakdownStructure]) INCLUDE ([Name], [ParentId], [PartyId], [PartyRoleId], [PartyRoleTypeId], [SortOrder])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[IndirectAndOtherDirectCostPerformer]'
GO


CREATE VIEW [Contact].[IndirectAndOtherDirectCostPerformer]
WITH SCHEMABINDING
AS
SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, re.FormalCode, re.UnobligatedInd, re.CostInd, o.ActiveInd
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.ReportingEntity re ON o.PartyId = re.OrganizationPartyId
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
WHERE pr.PartyRoleTypeId = 14  --Contact.fnGetIndirectAndOtherDirectCostPerformerPartyRoleTypeId()
AND pty.DeletedInd = 0


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[SubSystemOrganizationXref]'
GO



-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     9/13/2013
-- Release:     4.4
-- Description: Cross Reference between current Program table and new Organization structure
--
-- mm/dd/yy  Name      Release  Description
-- 01/30/14  j.borgers 5.0      Renamed and modified to use SubSystem instead of Program
-- =============================================================
CREATE VIEW [Contact].[SubSystemOrganizationXref] AS
WITH SubSystemOrgCTE AS 
(
SELECT SS.Id SubSystemId, O.PartyId OrganizationPartyId
FROM G2.SubSystem SS
INNER JOIN Contact.Organization O INNER JOIN Contact.PartyRole PR ON PR.PartyId = O.PartyId INNER JOIN Contact.PartyRoleType PRT ON PRT.Id = PR.PartyRoleTypeId AND PRT.Name = 'Org Subsystem Map' ON PR.SubSystemId = SS.Id
UNION ALL
SELECT C.SubSystemId, O.PartyId
FROM SubSystemOrgCTE  C
INNER JOIN Contact.Organization O ON O.ParentId = C.OrganizationPartyId
)
SELECT * FROM SubSystemOrgCTE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[vwHistoricalProjectMilestoneMetricChange]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     6/11/12
-- Release:     3.3
-- Description: Created to replace G2.vwHistoricalProjectMilestoneMetricBaselineChange
--
-- mm/dd/yy  Name      Release  Description
-- 06/20/12  s.oneal   3.3      Removed the Id column from results
-- 05/14/14  j.borgers 5.1      Added SequenceNumber
-- =============================================================
CREATE VIEW [Baseline].[vwHistoricalProjectMilestoneMetricChange]
AS
	SELECT  TaskChangeId
	      , ProjectMilestoneMetricId
	      , MilestoneId
		  , SequenceNumber
	      , MetricId
	      , BaselineValue
	      , CAST(NULL AS datetime) AS DeletedDt
	FROM Baseline.ProjectMilestoneMetricChange pmmc
	
	UNION 
	
	SELECT  TaskChangeId
	      , ProjectMilestoneMetricId
	      , MilestoneId
		  , SequenceNumber
	      , MetricId
	      , BaselineValue
	      , DeletedDt 
	FROM Audit.DeletedProjectMilestoneMetricChange pmmc



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[vwHistoricalProjectMilestoneMetricPlanChange]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     06/11/12
-- Release:     3.3
-- Description: Created to replace G2.vwHistoricalProjectMilestoneMetricPlanBaselineChange
--
-- mm/dd/yy  Name     Release  Description
-- 06/20/12  s.oneal  3.3      Removed the Id column from results
-- =============================================================
CREATE VIEW [Baseline].[vwHistoricalProjectMilestoneMetricPlanChange]
AS
	SELECT  TaskChangeId
	      , ProjectMilestoneMetricId
	      , MetricId
	      , PeriodId
	      , BaselineValue
	      , CAST(NULL AS datetime) AS DeletedDt	
	FROM Baseline.ProjectMilestoneMetricPlanChange pmmpc
	
	UNION
	
	SELECT  TaskChangeId
	      , ProjectMilestoneMetricId
	      , MetricId
	      , PeriodId
	      , BaselineValue
	      , DeletedDt
	FROM Audit.DeletedProjectMilestoneMetricPlanChange pmmpc
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[vwSiteWorkBreakdownStructure]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     04/08/2016
-- Release:     7.0
-- Description: A view linking sites to Work Breakdown Structure 
--              for Inventory.
--
-- mm/dd/yyyy  Name        Release  Description
-- =============================================================
CREATE VIEW [Inventory].[vwSiteWorkBreakdownStructure] 
AS
    WITH cteSiteWbs AS (
        -- Get the sites that can be tied to WBS
        SELECT 
            COALESCE(sub_system.Id, -1) SubSystemId, 
            COALESCE(sub_system.Name, '') SubSystemName, 
            COALESCE(sub_system.ShortName, '') SubSystemShortName,
            COALESCE(wbs_office.Id, -1) OfficeWorkBreakdownStructureId,
            COALESCE(wbs_office.Name, '') OfficeWorkBreakdownStructureName,
            COALESCE(wbsg.SecondaryGroupId, -1) SubOfficeWorkBreakdownStructureId,
            COALESCE(wbsg.SecondaryGroupName, '') SubOfficeWorkBreakdownStructureName,
            COALESCE(wbsg.PrimaryGroupId, -1) PortfolioWorkBreakdownStructureId,
            COALESCE(wbsg.PrimaryGroupName, '') PortfolioWorkBreakdownStructureName,
            COALESCE(wbs_project.Id, -1) ProjectWorkBreakdownStructureId,
            COALESCE(wbs_project.Name, '') ProjectWorkBreakdownStructureName,
            COALESCE(s.Id, -1) SiteId,
            COALESCE(s.[Name], '') SiteName
        FROM G2.[Site] s
        INNER JOIN G2.Building b ON b.SiteId = s.Id
        INNER JOIN G2.TaskBuilding tb ON tb.BuildingId = b.Id
        INNER JOIN G2.Task t ON t.Id = tb.TaskId
        INNER JOIN G2.WorkBreakdownStructure wbs_task ON wbs_task.Id = t.WorkBreakdownStructureId
        INNER JOIN G2.WorkBreakdownStructure wbs_project ON wbs_project.Id = wbs_task.ParentId
        INNER JOIN G2.WorkBreakdownStructureGroups wbsg ON wbsg.WorkBreakdownStructureId = wbs_project.Id
        INNER JOIN G2.WorkBreakdownStructure wbs_sub_office ON wbs_sub_office.Id = wbsg.SecondaryGroupId
        INNER JOIN G2.WorkBreakdownStructure wbs_office ON wbs_office.Id = wbs_sub_office.ParentId
        INNER JOIN G2.SubSystem sub_system ON sub_system.Id = wbs_task.SubSystemId
        WHERE sub_system.Id = 6 -- G2.fnGetNA23SubSystemId()
        GROUP BY sub_system.Id, sub_system.Name, sub_system.ShortName, wbs_office.Id, wbs_office.Name, 
            wbs_sub_office.Id, wbs_sub_office.Name, wbs_project.Id, wbs_project.Name,
            wbsg.SecondaryGroupId, wbsg.SecondaryGroupName, wbsg.PrimaryGroupId, wbsg.PrimaryGroupName, 
            s.Id, s.Name
    ), cteSiteSubSystem AS (
        -- Get the remaining sites that can't really be tied to WBS and tie to the subsystem and office WBS
        SELECT
            COALESCE(sub_system.Id, -1) SubSystemId, 
            COALESCE(sub_system.Name, '') SubSystemName, 
            COALESCE(sub_system.ShortName, '') SubSystemShortName,
            COALESCE(wbs_office.Id, -1) OfficeWorkBreakdownStructureId,
            COALESCE(wbs_office.Name, '') OfficeWorkBreakdownStructureName,
            NULL SubOfficeWorkBreakdownStructureId,
            NULL SubOfficeWorkBreakdownStructureName,
            NULL PortfolioWorkBreakdownStructureId,
            NULL PortfolioWorkBreakdownStructureName,
            NULL ProjectWorkBreakdownStructureId,
            NULL ProjectWorkBreakdownStructureName,
            COALESCE(s.Id, -1) SiteId,
            COALESCE(s.[Name], '') SiteName
        FROM G2.Site s
        LEFT JOIN cteSiteWbs ON cteSiteWbs.SiteId = s.Id
        CROSS JOIN G2.SubSystem sub_system
        INNER JOIN G2.WorkBreakdownStructure wbs_office ON wbs_office.SubSystemId = sub_system.Id
        WHERE cteSiteWbs.SiteId IS NULL
        AND sub_system.Id = 6 -- G2.fnGetNA23SubSystemId()
        AND wbs_office.SubSystemLevelId = 25 -- G2.fnGetOfficeSubSystemLevelId(sub_system.Id)
    )

    SELECT cteSiteWbs.SubSystemId,
           cteSiteWbs.SubSystemName,
           cteSiteWbs.SubSystemShortName,
           cteSiteWbs.OfficeWorkBreakdownStructureId,
           cteSiteWbs.OfficeWorkBreakdownStructureName,
           cteSiteWbs.SubOfficeWorkBreakdownStructureId,
           cteSiteWbs.SubOfficeWorkBreakdownStructureName,
           cteSiteWbs.PortfolioWorkBreakdownStructureId,
           cteSiteWbs.PortfolioWorkBreakdownStructureName,
           cteSiteWbs.ProjectWorkBreakdownStructureId,
           cteSiteWbs.ProjectWorkBreakdownStructureName,
           cteSiteWbs.SiteId,
           cteSiteWbs.SiteName 
    FROM cteSiteWbs
    UNION
    SELECT cteSiteSubSystem.SubSystemId,
           cteSiteSubSystem.SubSystemName,
           cteSiteSubSystem.SubSystemShortName,
           cteSiteSubSystem.OfficeWorkBreakdownStructureId,
           cteSiteSubSystem.OfficeWorkBreakdownStructureName,
           cteSiteSubSystem.SubOfficeWorkBreakdownStructureId,
           cteSiteSubSystem.SubOfficeWorkBreakdownStructureName,
           cteSiteSubSystem.PortfolioWorkBreakdownStructureId,
           cteSiteSubSystem.PortfolioWorkBreakdownStructureName,
           cteSiteSubSystem.ProjectWorkBreakdownStructureId,
           cteSiteSubSystem.ProjectWorkBreakdownStructureName,
           cteSiteSubSystem.SiteId,
           cteSiteSubSystem.SiteName 
    FROM cteSiteSubSystem;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwChangeRequestDetailStatusRecallableStates]'
GO

CREATE VIEW [G2].[vwChangeRequestDetailStatusRecallableStates]
WITH SCHEMABINDING
AS
	SELECT crs.Id AS ChangeRequestDetailStatusId, crs.Name AS ChangeRequestDetailStatusName
	FROM G2.ChangeRequestDetailStatus crs
	INNER JOIN G2.ChangeRequestDetailStatusStateXref crssx ON crs.Id = crssx.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestDetailStatusState crss ON crssx.ChangeRequestDetailStatusStateId = crss.Id
	WHERE crss.Id = 4 --Recallable


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwChangeRequestDetailStatusRecallableStates] on [G2].[vwChangeRequestDetailStatusRecallableStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwChangeRequestDetailStatusRecallableStates] ON [G2].[vwChangeRequestDetailStatusRecallableStates] ([ChangeRequestDetailStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureRegion]'
GO



-- Recreate view G2.vwWorkBreakdownStructureRegion with schema binding and add index
--CREATE UNIQUE CLUSTERED INDEX [uciv_vwProjectRegion_ProjectId_RegionId] ON [G2].[vwProjectRegion]
--( [WorkBreakdownStructueId] ASC, [RegionId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
CREATE VIEW [G2].[vwWorkBreakdownStructureRegion]
AS
	SELECT wbsProject.Id AS WorkBreakdownStructureId, wbsProject.Name AS ProjectName, r.Id AS RegionId, r.Name AS RegionName, COUNT_BIG(*) AS Count
	FROM G2.WorkBreakdownStructure wbsProject
	INNER JOIN G2.Schedule sched ON wbsProject.Id = sched.WorkBreakdownStructureId
	INNER JOIN G2.WorkBreakdownStructureConfiguration wbsConfig ON wbsProject.Id = wbsConfig.WorkBreakdownStructureId 
																AND wbsConfig.ConfigurationTypeId = G2.fnGetScheduleConfigurationTypeId()
	INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsProject.Id = wbsTask.ParentId
	INNER JOIN G2.Task t ON wbsTask.Id = t.WorkBreakdownStructureId
	INNER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
	INNER JOIN G2.Building b ON tb.BuildingId = b.Id
	INNER JOIN G2.Site s ON b.SiteId = s.Id
	INNER JOIN G2.State st ON s.StateId = st.Id
	INNER JOIN Inventory.RegionStateXRef rsxr ON st.Id = rsxr.StateId
	INNER JOIN Inventory.Region r ON rsxr.RegionId = r.Id
	WHERE wbsTask.StatusId NOT IN (G2.fnGetDeletedWorkBreakdownStructureStatusId(), G2.fnGetCancelledWorkBreakdownStructureStatusId())
	GROUP BY wbsProject.Id, wbsProject.Name, r.Id, r.Name


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwSubSystemLevelConfigurationPivot]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/4/2017
-- Release:     8.3
-- Description: Indexed view that pivots SubSystemLevelConfiguration data
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE VIEW [G2].[vwSubSystemLevelConfigurationPivot]
WITH SCHEMABINDING
AS

	SELECT SubSystemLevelId, CAST([1] AS BIT) AS BudgetInd, CAST([2] AS BIT) AS FundingInd, CAST([3] AS BIT) AS CostObligationInd, CAST([4] AS BIT) AS ScheduleInd,
		CAST([5] AS BIT) AS IndirectCostInd, CAST([6] AS BIT) AS OtherDirectCostInd, CAST([7] AS BIT) AS BudgetFormulationInd, CAST([9] AS BIT) AS SpendPlanInd, CAST([10] AS BIT) AS BudgetFormulationGroupInd
	FROM (
		SELECT ssl.Id AS SubSystemLevelId, sslc.ConfigurationTypeId AS Config, sslc.ConfigurationTypeId AS Config2
		FROM G2.SubSystemLevel ssl
        LEFT OUTER JOIN G2.SubSystemLevelConfiguration sslc ON ssl.Id = sslc.SubSystemLevelId
	) X
	PIVOT(COUNT(Config2) FOR Config IN ([1], [2], [3], [4], [5], [6], [7], [9], [10])) AS P
	-- G2.fnGetBudgetConfigurationTypeId(), G2.fnGetFundingConfigurationTypeId(), G2.fnGetCostObligationConfigurationTypeId(), G2.fnGetScheduleConfigurationTypeId(),
	-- G2.fnGetIndirectCostConfigurationTypeId(), G2.fnGetOtherDirectCostConfigurationTypeId(), G2.fnGetBudgetFormulationConfigurationTypeId(), G2.fnGetSpendPlanConfigurationTypeId(), G2.fnGetBudgetFormulationgroupConfigurationTypeId()



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[OrganizationMember]'
GO

CREATE VIEW [Contact].[OrganizationMember] 
WITH SCHEMABINDING
AS
SELECT O.PartyId OrganizationPartyId, O.OrganizationBreakdownStructure, O.Name OrganizationName, O.ParentId OrganizationParentPartyId, P.PartyId PersonPartyId, P.FirstName, P.MiddleName, P.LastName, P.Suffix, P.PreferredName, P.FederalEmployeeInd, PR.Title
FROM Contact.PartyRelationship PR
INNER JOIN Contact.PartyRole PRFrom ON PRFrom.Id = PR.FromPartyRoleId
INNER JOIN Contact.Person P ON PRFrom.PartyId = P.PartyId
INNER JOIN Contact.PartyRole PRTo ON PRTo.Id = PR.ToPartyRoleId
INNER JOIN Contact.Party P2 ON P2.Id = PRTo.PartyId
INNER JOIN Contact.Organization O ON O.PartyId = P2.Id
WHERE PR.PartyRelationshipTypeId = 1 -- Organization Member
AND (PR.ThruDate IS NULL OR PR.ThruDate >= CURRENT_TIMESTAMP)
WITH CHECK OPTION;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwWorkBreakdownStructureStatusOpenRequestStates]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Returns open request statuses.
--   
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE VIEW [G2].[vwWorkBreakdownStructureStatusOpenRequestStates]
WITH SCHEMABINDING
AS
	SELECT wbss.Id AS WorkBreakdownStructureStatusId, wbss.Name AS WorkBreakdownStructureStatusName
	FROM G2.WorkBreakdownStructureStatus wbss
	INNER JOIN G2.WorkBreakdownStructureStatusStateXref wbsssx ON wbss.Id = wbsssx.WorkBreakdownStructureStatusId
	INNER JOIN G2.WorkBreakdownStructureStatusState wbsss ON wbsssx.WorkBreakdownStructureStatusStateId = wbsss.Id
	WHERE wbsss.Id = 4; -- G2.fnGetOpenRequestWorkBreakdownStructureStatusStateId


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [uciv_vwWorkBreakdownStructureStatusOpenRequestStates] on [G2].[vwWorkBreakdownStructureStatusOpenRequestStates]'
GO
CREATE UNIQUE CLUSTERED INDEX [uciv_vwWorkBreakdownStructureStatusOpenRequestStates] ON [G2].[vwWorkBreakdownStructureStatusOpenRequestStates] ([WorkBreakdownStructureStatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwLabLeadList]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     
-- Description: Retrieves a concatenated list of lab leads
--   for each lab.
--
-- mm/dd/yy  Name      Release  Description
-- 01/11/12  s.oneal   3.0      Modified to use the new security model
-- 09/30/14  j.borgers 5.3      Modified to work with schedule data structure changes
-- 09/25/15  a.smith   6.3      Added SubSystemId and the FinPlan: Site Financial Stakeholder for 
--                              subsystems other than 1.
-- 10/27/15  s.oneal   6.3      Modified to remove duplicates from list of users with roles
-- 11/10/15  a.smith   6.3.1    Changed from FinPlan: Site Financial Stakeholder to Schedule: Project Stakeholder
--                              Per Luke Kraemer and Don Hatch.
-- 04/12/17  j.borgers 8.0      Removed Lab Lead RoleId
-- =============================================================
CREATE VIEW [G2].[vwLabLeadList]
AS
	WITH LabLead
	AS
	(
		SELECT DISTINCT wbs.SubSystemId, wbs.Id AS WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Security.PersonRole persR
		INNER JOIN G2.WorkBreakdownStructure wbs ON persR.WorkBreakdownStructureId = wbs.Id AND persR.SubSystemId = wbs.SubSystemId
		INNER JOIN Security.Person p ON persR.PersonId = p.Id
		WHERE persR.RoleId = 102 --Schedule: Project Stakeholder
	),
	LabLeadList
	AS
	(
		SELECT l.SubSystemId, l.WorkBreakdownStructureId, (SELECT l2.Name + ', ' FROM LabLead l2 WHERE l.WorkBreakdownStructureId=l2.WorkBreakdownStructureId FOR XML PATH('')) AS LL
		FROM LabLead l
		GROUP BY l.SubSystemId, l.WorkBreakdownStructureId
	)
	SELECT l.SubSystemId, l.WorkBreakdownStructureId, LEFT(l.LL, (LEN(l.LL) - 1)) AS LabLeads
	FROM LabLeadList l;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[vwGISERIAssetAnswer]'
GO



-- =============================================================
-- Author:      D. Johnson
-- Created:     02/27/2018
-- Release:     8.5
-- Description: Created to link ERI Question Ids to ERI Answer Ids and data
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [AssetManagement].[vwGISERIAssetAnswer]
AS
    SELECT g2a.Id AS AssetId, a.RealPropertyUniqueIdentifier, c.QuestionId, c.GISERIAnswer
  FROM [AssetManagement].[GISERIAssetAnswer] a
  INNER JOIN [G2].Asset g2a ON g2a.RealPropertyUniqueIdentifier = a.RealPropertyUniqueIdentifier
  CROSS APPLY
  (
	SELECT (SELECT x.questionId FROM AssetManagement.GISERIAssetAnswerQuestionFieldMapping x WHERE x.GISERIAssetAnswerField = 'ERIDistanceToFacility') AS QuestionId , a.ERIDistanceToFacility AS GISERIAnswer UNION 
	SELECT (SELECT x.questionId FROM AssetManagement.GISERIAssetAnswerQuestionFieldMapping x WHERE x.GISERIAssetAnswerField = 'ERIDistanceToWorkers') AS QuestionId , a.ERIDistanceToWorkers AS GISERIAnswer UNION 
	SELECT (SELECT x.questionId FROM AssetManagement.GISERIAssetAnswerQuestionFieldMapping x WHERE x.GISERIAssetAnswerField = 'ERIDistanceToPublic') AS QuestionId , a.ERIDistanceToPublic AS GISERIAnswer UNION 
	SELECT (SELECT x.questionId FROM AssetManagement.GISERIAssetAnswerQuestionFieldMapping x WHERE x.GISERIAssetAnswerField = 'ERIDistanceToBoundary') AS QuestionId , a.ERIDistanceToBoundary AS GISERIAnswer UNION 
	SELECT (SELECT x.questionId FROM AssetManagement.GISERIAssetAnswerQuestionFieldMapping x WHERE x.GISERIAssetAnswerField = 'ERIEnvironmentalReceptorsInd') AS QuestionId , a.ERIEnvironmentalReceptorsInd AS GISERIAnswer
	
  ) c;








GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[vwQueuedEventParticipants]'
GO
-- ==========================================================================================
-- Author:		Greg Ogle
-- Create date: 2011-10-14
-- Description:	View to show Queued Site Participants
--
-- mm/dd/yy	Name                Release Description
-- 10/31/11 Dan Fetzer          V2.6    Modify to incorporate the Inventory.fnGetNearestUasi2SiteBldgs.
-- 03/05/13 Nevada Williford    4.0     Replaced TVF call for site milestone info with a join to 
--                                      vwSiteMilestones. Replaced scalar function to get the RegionId.
-- ==========================================================================================
CREATE VIEW [Calendar].[vwQueuedEventParticipants]
AS
SELECT  
    PQ.Id                           AS ParticipantQueueId, 
    PQ.ParticipantTypeId            AS ParticipantTypeId,
    PQ.EventTypeId                  AS EventTypeId,
    PQS.SiteId                      AS EntityId, 
    S.Name                          AS EntityName, 
    S.City + ', ' + ST.Abbreviation AS CityState, 
    U.UasiName                      AS UasiName, 
    U.UasiTierId                    AS UasiTierId, 
    U.DistanceMiles                 AS DistanceMiles, 
    PQ.Comments                     AS Comments, 
    PQ.SeatsRequested               AS SeatsRequested, 
    PQ.DateRequested                AS DateRequested,
    RSX.RegionId                    AS RegionId,
    SM.FirstContractAwardedDate     AS FirstContractAwardDt,
    SM.LastContractAwardedDate      AS LastContractAwardDt,
    SM.SomeContractsAwardedFlag     AS SomeContractAwardCompleteFlag,
    SM.AllContractsAwardedFlag      AS AllContractAwardCompleteFlag,
    SM.FirstAssessmentCompletedDate AS FirstAssessmentDt,
    SM.LastAssessmentCompletedDate  AS LastAssessmentDt,
    SM.SomeAssessmentsCompletedFlag AS SomeAssessmentCompleteFlag,
    SM.AllAssessmentsCompletedFlag  AS AllAssessmentCompleteFlag,
    SM.FirstBuildingSecuredDate     AS FirstUpgradesCompleteDt,
    SM.LastBuildingSecuredDate      AS LastUpgradesCompleteDt,
    SM.SomeBuildingsSecuredFlag     AS SomeUpgradesCompleteFlag,
    SM.AllBuildingsSecuredFlag      AS AllUpgradesCompleteFlag,
                                           
    Calendar.fnGetPriorityLevel
    (
        GETDATE(),                       -- @eventStartDate
        SM.LastBuildingSecuredDate,      -- @lastUpgradesCompletedDate
        SM.LastContractAwardedDate,      -- @lastContractsAwardedDate
        SM.LastAssessmentCompletedDate,  -- @lastAssessmentsCompletedDate
        SM.FirstBuildingSecuredDate,     -- @firstUpgradesCompletedDate
        SM.FirstContractAwardedDate,     -- @firstContractsAwardedDate
        SM.FirstAssessmentCompletedDate, -- @firstAssessmentsCompletedDate
        SM.AllBuildingsSecuredFlag,      -- @allUpgradesCompletedFlag
        SM.AllContractsAwardedFlag,      -- @allContractsAwardedFlag
        SM.AllAssessmentsCompletedFlag,  -- @allAssessmentsCompletedFlag
        SM.SomeBuildingsSecuredFlag,     -- @someUpgradesCompletedFlag
        SM.SomeContractsAwardedFlag,     -- @someContractsAwardedFlag
        SM.SomeAssessmentsCompletedFlag  -- @someAssessmentsCompletedFlag
    ) AS RelativeStatusLevel,

    0 AS EventSiteStatus,
    
    LS.maxOperatingPowerMW,
    LS.maxActivityCi
FROM 
    Calendar.ParticipantQueue PQ 
    INNER JOIN Calendar.ParticipantType PT 
        ON PQ.ParticipantTypeId = PT.Id 
        AND PQ.DeletedInd = 0
    INNER JOIN Calendar.ParticipantQueueSite PQS 
        ON PQ.Id = PQS.Id 
    LEFT OUTER JOIN g2.[Site] S 
        ON PQS.SiteId = S.Id 
    LEFT OUTER JOIN g2.[State] ST 
        ON S.StateId = ST.id
    LEFT OUTER JOIN Inventory.RegionStateXRef RSX 
	    ON RSX.StateId = ST.Id
    LEFT JOIN Calendar.vwSiteMilestones SM 
        ON SM.SiteId = S.Id
    CROSS APPLY Inventory.fnGetNearestUasi2SiteBldgs(S.Id, 1) U
    CROSS APPLY Inventory.fnGetSiteLargestSourceOrMW(S.Id) LS



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[vwAllottee]'
GO
-- =============================================================
-- Author:      Christopher Luttrell (chris.luttrell)
-- Created:     06/09/2014
-- Release:     5.0 (Post release, prod support)
-- Description: view of Allottee with subsystem and Organizational info included, required for NPTD ETL process
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE VIEW [Contact].[vwAllottee]
AS
SELECT o.PartyId AS ReportingEntityOrganizationPartyId, pr.SubSystemId, o.OrganizationBreakdownStructure AS DisplayName, o.Name, A.LegacyCode, A.STARSCode, o.ActiveInd
FROM Contact.Organization o
INNER JOIN Contact.Party pty ON o.PartyId = pty.Id
INNER JOIN Contact.Allottee A ON o.PartyId = A.OrganizationPartyId
INNER JOIN Contact.PartyRole pr ON o.PartyId = pr.PartyId
WHERE pr.PartyRoleTypeId = 8 -- Contact.fnGetAllotteePartyRoleTypeId()
AND pty.DeletedInd = 0
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwOfficeDirectorList]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     
-- Description: Retrieves a concatenated list of office
--   directory for each office.
--
-- mm/dd/yy  Name     Release  Description
-- 01/11/12  s.oneal  3.0      Modified to use the new security model
-- 01/26/12  DNC      3.0      Remove hardcoded role
-- 09/30/14  s.oneal  5.3      Modified to use the new schedule and security data models
-- 09/22/15  a.smith  6.3      Modified to include equivalent roles for NA-21 GMS, NA-23, NA-50, & NA-193.
--                             Added SubSystemId as output column.
-- 10/27/15  s.oneal  6.3      Modified to remove duplicates from list of users with roles
-- =============================================================
CREATE VIEW [G2].[vwOfficeDirectorList]
AS
	WITH OfficeDirectors
	AS
	(
		SELECT DISTINCT persR.SubSystemId, persR.WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Security.PersonRole persR
		INNER JOIN Security.Person p ON persR.PersonId = p.Id
		WHERE persR.RoleId IN (7, 117, 121) --Security.fnGetOfficeDirectorRoleId, FinPlan: Office Director (Primary), Schedule: Office Director (Primary)
	),
	OfficeDirectorList
	AS
	(
		SELECT o.SubSystemId, o.WorkBreakdownStructureId, 
			   (SELECT o2.Name + ', ' FROM OfficeDirectors o2 WHERE o.WorkBreakdownStructureId = o2.WorkBreakdownStructureId FOR XML PATH('')) AS OfficeDirectors
		FROM OfficeDirectors o
		GROUP BY o.SubSystemId, o.WorkBreakdownStructureId
	)
	SELECT o.SubSystemId, o.WorkBreakdownStructureId, LEFT(o.OfficeDirectors, (LEN(o.OfficeDirectors) - 1)) AS OfficeDirectors
	FROM OfficeDirectorList o;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[vwProjectManagerDesigneeList]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     
-- Description: Retrieves a concatenated list of project manager
--   designees for each project.
--
-- mm/dd/yy  Name     Release  Description
-- 09/30/14  s.oneal  5.3      Modified to use the new schedule and security data models
-- 09/22/15  a.smith  6.3      Modified to include equivalent roles for NA-21 GMS, NA-23, NA-50, & NA-193.
--                             Added SubSystemId as output column.
-- 10/27/15  s.oneal   6.3      Modified to remove duplicates from list of users with roles
-- =============================================================
CREATE VIEW [G2].[vwProjectManagerDesigneeList]
AS
	WITH ProjectManagerDesignees
	AS
	(
		SELECT DISTINCT persR.SubSystemId, persR.WorkBreakdownStructureId, p.FirstName + ' ' + p.LastName AS Name
		FROM Security.PersonRole persR
		INNER JOIN Security.Person p ON persR.Personid = p.Id
		WHERE persR.RoleId IN (11, 81, 101) -- Security.fnGetProjectManagerDesigneeRoleId, FinPlan: Project Manager (Secondary), Schedule: Project Manager (Secondary)
	),
	ProjectManagerDesigneeList
	AS
	(
		SELECT p.SubSystemId, p.WorkBreakdownStructureId, 
			   (SELECT p2.Name + ', ' FROM ProjectManagerDesignees p2 WHERE p.WorkBreakdownStructureId = p2.WorkBreakdownStructureId FOR XML PATH('')) AS ProjectManagerDesignees
		FROM ProjectManagerDesignees p
		GROUP BY p.SubSystemId, p.WorkBreakdownStructureId
	)
	SELECT p.SubSystemId, p.WorkBreakdownStructureId, LEFT(p.ProjectManagerDesignees, (LEN(p.ProjectManagerDesignees) - 1)) AS ProjectManagerDesignees
	FROM ProjectManagerDesigneeList p;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[vwEventAttendee]'
GO
CREATE VIEW [Calendar].[vwEventAttendee]
AS
SELECT     
    E.Id                AS EventId,
    E.Name              AS EventName,
    E.StartDt           AS EventStartDt,
    E.EndDt             AS EventEndDt,
    ET.Id               AS EventTypeId,
    ET.Abbreviation     AS EventTypeAbbreviation,
    RG.Id               AS RegionId,
    RG.Abbreviation     AS RegionAbbreviation,
    ST.Id               AS StateId,
    ST.Abbreviation     AS StateAbbreviation,
    S.Id                AS SiteId,
    S.Name              AS SiteName,
    LEA.Id              AS LeaId,
    LEA.Name            AS LeaName,
    EA.Id               AS AttendeeId,
    EA.LastName         AS AttendeeLastName,
    EA.FirstName        AS AttendeeFirstName,
    EA.Organization     AS AttendeeOrganization,
    EA.PositionTitle    AS AttendeePositionTitle,
    AC.Id               AS AttendanceCategoryId,
    AC.Name             AS AttendanceCategoryName,
    OC.Id               AS OrganizationCategoryId,
    OC.Name             AS OrganizationCategoryName
FROM
    Calendar.EventAttendee EA
    INNER JOIN Calendar.[Event] E
        ON E.Id = EA.EventId
        AND E.DeletedInd = 0
        AND EA.DeletedInd = 0
    INNER JOIN Calendar.EventType ET 
        ON ET.Id = E.EventTypeId
    LEFT JOIN G2.[State] ST 
        ON ST.Id = EA.StateId
    LEFT JOIN Inventory.RegionStateXRef RSX 
        ON RSX.StateId = ST.Id
    LEFT JOIN Inventory.Region RG 
        ON RG.Id = RSX.RegionId 
    LEFT JOIN G2.[Site] S 
        ON S.Id = EA.SiteId
    LEFT JOIN Calendar.AttendeeCategory AC 
        ON AC.Id = EA.AttendeeCategoryId
    LEFT JOIN Calendar.OrganizationCategory OC 
        ON OC.Id = EA.OrganizationCategoryId
    LEFT JOIN Inventory.LawEnforcementAgency LEA 
        ON LEA.Id = EA.LeaId





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[vwEventParticipants]'
GO
-- ==========================================================================================
-- Author:		?
-- Create date: ?
-- Description:	?
--
-- mm/dd/yy	Name                Release Description
-- 03/05/13 Nevada Williford    4.0     Replaced TVF call for site milestone info with a join to 
--                                      vwSiteMilestones. Replaced scalar function to get the RegionId.
-- ==========================================================================================
CREATE VIEW [Calendar].[vwEventParticipants]
AS
SELECT
    EP.EventId                      AS EventId, 
    EP.Id                           AS EventParticipantId, 
    EP.ParticipantTypeId            AS ParticipantTypeId, 
    EPS.SiteId                      AS EntityId, 
    S.Name                          AS EntityName, 
    S.City + ', ' + ST.Abbreviation AS CityState, 
    U.UasiName                      AS UasiName, 
    U.UasiTierId                    AS UasiTierId, 
    U.DistanceMiles                 AS DistanceMiles, 
    EP.Comment                      AS Comment, 
    EP.AllocatedAttendees           AS AllocatedAttendees, 
    EP.ApprovedDt                   AS ApprovedDt,
    EP.InvitationsSentDt            AS InvitationsSentDt, 
    EP.ParticipantStatusId          AS ParticipantStatusId, 
    PS.Name                         AS ParticipantStatusName, 
    RSX.RegionId                    AS RegionId,
    SM.FirstContractAwardedDate     AS FirstContractAwardDt,
    SM.LastContractAwardedDate      AS LastContractAwardDt,
    SM.SomeContractsAwardedFlag     AS SomeContractAwardCompleteFlag,
    SM.AllContractsAwardedFlag      AS AllContractAwardCompleteFlag,
    SM.FirstAssessmentCompletedDate AS FirstAssessmentDt,
    SM.LastAssessmentCompletedDate  AS LastAssessmentDt,
    SM.SomeAssessmentsCompletedFlag AS SomeAssessmentCompleteFlag,
    SM.AllAssessmentsCompletedFlag  AS AllAssessmentCompleteFlag,
    EPS.OrigUpgradesCompleteDt      AS origUpgradesCompleteDt,
    SM.FirstBuildingSecuredDate     AS FirstUpgradesCompleteDt,
    SM.LastBuildingSecuredDate      AS LastUpgradesCompleteDt,
    SM.SomeBuildingsSecuredFlag     AS SomeUpgradesCompleteFlag,
    SM.AllBuildingsSecuredFlag      AS AllUpgradesCompleteFlag,
                                           
    Calendar.fnGetPriorityLevel
    (
        E.StartDt,                       -- @eventStartDate
        SM.LastBuildingSecuredDate,      -- @lastUpgradesCompletedDate
        SM.LastContractAwardedDate,      -- @lastContractsAwardedDate
        SM.LastAssessmentCompletedDate,  -- @lastAssessmentsCompletedDate
        SM.FirstBuildingSecuredDate,     -- @firstUpgradesCompletedDate
        SM.FirstContractAwardedDate,     -- @firstContractsAwardedDate
        SM.FirstAssessmentCompletedDate, -- @firstAssessmentsCompletedDate
        SM.AllBuildingsSecuredFlag,      -- @allUpgradesCompletedFlag
        SM.AllContractsAwardedFlag,      -- @allContractsAwardedFlag
        SM.AllAssessmentsCompletedFlag,  -- @allAssessmentsCompletedFlag
        SM.SomeBuildingsSecuredFlag,     -- @someUpgradesCompletedFlag
        SM.SomeContractsAwardedFlag,     -- @someContractsAwardedFlag
        SM.SomeAssessmentsCompletedFlag  -- @someAssessmentsCompletedFlag
    ) AS RelativeStatusLevel,

    -- EventSiteStatus
    --  1 = On event, the UpgradeCompleteDt has slipped past the start date.
    --  2 = On event, the UpgradeCompleteDt has slipped, but not past the start date.
    --  3 = On event, the UpgradeCompleteDt is now forecast to finish earlier than originally projected.
    -- -1 = On event, but no OrigUpgradeCompleteDt found. 
    -- -2 = On the event, but no change in OrigUpgradeCompleteDt.
    CASE 
        WHEN (SM.FirstBuildingSecuredDate > EPS.OrigUpgradesCompleteDt AND SM.FirstBuildingSecuredDate > E.StartDt) THEN 1 
        WHEN (SM.FirstBuildingSecuredDate > EPS.OrigUpgradesCompleteDt AND SM.FirstBuildingSecuredDate < E.StartDt) THEN 2
        WHEN (SM.FirstBuildingSecuredDate < EPS.OrigUpgradesCompleteDt AND SM.FirstBuildingSecuredDate < E.StartDt) THEN 3
        WHEN (SM.FirstBuildingSecuredDate < EPS.OrigUpgradesCompleteDt AND SM.FirstBuildingSecuredDate > E.StartDt) THEN 4
        WHEN (EPS.OrigUpgradesCompleteDt IS NULL)    THEN -1
        ELSE -2
    END AS EventSiteStatus,
    
    LS.maxOperatingPowerMW,
    LS.maxActivityCi
FROM
    Calendar.[Event] E
    INNER JOIN Calendar.EventParticipant EP 
        ON EP.EventId = E.Id
        AND EP.DeletedInd = 0
        AND E.DeletedInd = 0
    INNER JOIN Calendar.ParticipantStatus PS 
        ON EP.ParticipantStatusId = PS.Id 
    INNER JOIN Calendar.ParticipantType PT 
        ON EP.ParticipantTypeId = PT.Id 
    INNER JOIN Calendar.EventParticipantSite EPS 
        ON EP.Id = EPS.Id 
    LEFT OUTER JOIN g2.[Site] S 
        ON EPS.SiteId = S.Id 
    LEFT OUTER JOIN g2.[State] ST 
        ON S.StateId = ST.Id
    LEFT OUTER JOIN Inventory.RegionStateXRef RSX 
	    ON RSX.StateId = ST.Id
    LEFT JOIN Calendar.vwSiteMilestones SM 
        ON SM.SiteId = S.Id
    CROSS APPLY Inventory.fnGetNearestUasi2SiteBldgs(S.id,1) U
    CROSS APPLY Inventory.fnGetSiteLargestSourceOrMW(S.Id) LS



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating extended properties'
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[16] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -134
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AttendeeResponsibility (Calendar)"
            Begin Extent = 
               Top = 190
               Left = 751
               Bottom = 373
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Event (Calendar)"
            Begin Extent = 
               Top = 22
               Left = 117
               Bottom = 218
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Attendee (Calendar)"
            Begin Extent = 
               Top = 26
               Left = 313
               Bottom = 391
               Right = 522
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OrganizationCategory (Calendar)"
            Begin Extent = 
               Top = 332
               Left = 963
               Bottom = 451
               Right = 1124
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Site (G2)"
            Begin Extent = 
               Top = 251
               Left = 11
               Bottom = 469
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "State_1"
            Begin Extent = 
               Top = 421
               Left = 323
               Bottom = 540
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Region (Inventory)"
            Begin Extent = 
               Top = 33
               Left = 878
              ', 'SCHEMA', N'Calendar', 'VIEW', N'vwEventAttendee', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane2', N' Bottom = 152
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RegionStateXRef (Inventory)"
            Begin Extent = 
               Top = 23
               Left = 618
               Bottom = 142
               Right = 779
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "State (G2)"
            Begin Extent = 
               Top = 325
               Left = 576
               Bottom = 444
               Right = 737
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AttendanceCategory (Calendar)"
            Begin Extent = 
               Top = 465
               Left = 928
               Bottom = 584
               Right = 1089
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1785
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2025
         Alias = 1815
         Table = 3795
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'Calendar', 'VIEW', N'vwEventAttendee', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'Calendar', 'VIEW', N'vwEventAttendee', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[12] 4[23] 2[43] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2760
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'Calendar', 'VIEW', N'vwEventParticipants', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'Calendar', 'VIEW', N'vwEventParticipants', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pmm"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 112
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 248
               Bottom = 127
               Right = 412
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pm"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 235
               Right = 204
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 450
               Bottom = 127
               Right = 634
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
 ', 'SCHEMA', N'G2', 'VIEW', N'vwJouleMetrics', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane2', N'  End
End
', 'SCHEMA', N'G2', 'VIEW', N'vwJouleMetrics', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'G2', 'VIEW', N'vwJouleMetrics', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [G2].[vwSearchWBS] KEY INDEX [PK_vwSearchWBS] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE FULLTEXT INDEX ON [Proposal].[vwSearchReportingEntity] KEY INDEX [PK_vwSearchReportingEntity] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE FULLTEXT INDEX ON [Proposal].[vwSearchRequestAttachment] KEY INDEX [PK_vwSearchRequestAttachment] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE FULLTEXT INDEX ON [Proposal].[vwSearchSolicitationAttachment] KEY INDEX [PK_vwSearchSolicitationAttachment] ON [G2] WITH STOPLIST OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [G2].[vwSearchWBS] ADD ([FullWBS] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [G2].[vwSearchWBS] ADD ([Name] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchReportingEntity] ADD ([OrganizationBreakdownStructure] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchRequestAttachment] ADD ([Name] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchSolicitationAttachment] ADD ([Name] LANGUAGE 1033)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [G2].[vwSearchWBS] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchReportingEntity] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchRequestAttachment] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER FULLTEXT INDEX ON [Proposal].[vwSearchSolicitationAttachment] ENABLE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
