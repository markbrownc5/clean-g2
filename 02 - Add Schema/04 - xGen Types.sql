/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 2:59:49 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating types'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[WorkBreakdownStructureDescriptionTable] AS TABLE
(
[WorkBreakdownStructureId] [int] NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[WorkBreakdownStructureAttributeTable] AS TABLE
(
[Id] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[WorkBreakdownStructureAttributeTypeId] [int] NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[WebRequestLogTable] AS TABLE
(
[Id] [int] NULL,
[Module] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Component] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Action] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SessionId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[VendorTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedAs] [int] NULL,
[ActiveInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[VendorDeviceTypeXrefTable] AS TABLE
(
[Id] [int] NULL,
[VendorId] [int] NULL,
[DeviceTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[UserSystemStatisticsTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[UserAgent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrowserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrowserVersion] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobileInd] [bit] NULL,
[ScreenHeight] [int] NULL,
[ScreenWidth] [int] NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[UrbanAreaTable] AS TABLE
(
[Id] [int] NULL,
[StateId] [int] NULL,
[UASITierId] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[UrbanAreaCountyXRefTable] AS TABLE
(
[Id] [int] NULL,
[UrbanAreaId] [int] NULL,
[CountyId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[UASITierTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[Score] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TripTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TransportationVendorTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TransitionReasonTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TopProjectListTable] AS TABLE
(
[Id] [int] NULL,
[ProjectId] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskTable] AS TABLE
(
[Id] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[MilestoneTemplateId] [int] NULL,
[NameBase] [nvarchar] (275) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameSuffix] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadReportingEntityOrganizationPartyId] [int] NULL,
[PlanningPackageInd] [bit] NULL,
[StatusNotes] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusLastModifiedBy] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[StatusLastModifiedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskStatusTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestDetailId] [int] NULL,
[TaskId] [int] NULL,
[Explanation] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadReportingEntityOrganizationPartyId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskSiteTable] AS TABLE
(
[Id] [int] NULL,
[TaskId] [int] NULL,
[SiteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskLevelTable] AS TABLE
(
[Id] [int] NULL,
[ProjectLevelId] [int] NULL,
[TaskId] [int] NULL,
[MilestoneTemplateId] [int] NULL,
[StartDt] [datetime] NULL,
[JouleDt] [datetime] NULL,
[FinishDt] [datetime] NULL,
[ConfidenceLevelId] [int] NULL,
[RemoveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[BuildingId] [int] NULL,
[TaskName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadReportingEntityOrganizationPartyId] [int] NULL,
[SiteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskDeviceTable] AS TABLE
(
[TaskId] [int] NULL,
[SourceId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Id] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[TaskBaselineChangeTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestDetailId] [int] NULL,
[TaskId] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Explanation] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadLabId] [int] NULL,
[ServiceCenterContractId] [int] NULL,
[ConfidenceLevelId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SupportFileTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubfunctionMilestoneTemplateTable] AS TABLE
(
[Id] [int] NULL,
[MilestoneTemplateId] [int] NULL,
[ProjectSubFunctionId] [int] NULL,
[CustomizableInd] [bit] NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubSystemFundTypeBNRXrefTable] AS TABLE
(
[Id] [int] NULL,
[FundTypeId] [int] NULL,
[BNRId] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SubSystemId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubElementTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubElementTable] AS TABLE
(
[Id] [int] NULL,
[Question] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElementId] [int] NULL,
[ParentId] [int] NULL,
[SubElementTypeId] [int] NULL,
[Weight] [decimal] (18, 2) NULL,
[ActiveInd] [bit] NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[ParentAnswerTrigger] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubElementQuestionSetId] [int] NULL,
[ElementWBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubElementQuestionSetTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SubElementAnswerTable] AS TABLE
(
[Id] [int] NULL,
[AssessmentId] [int] NULL,
[SubElementId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Answer] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SpendPlanDetailRequestTable] AS TABLE
(
[Id] [int] NULL,
[SpendPlanHeaderRequestId] [int] NULL,
[FiscalPeriod] [int] NULL,
[CostForecast] [decimal] (18, 2) NULL,
[CommitmentForecast] [decimal] (18, 2) NULL,
[FundingActual] [decimal] (18, 2) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SourceTable] AS TABLE
(
[Id] [int] NULL,
[MaterialTypeId] [int] NULL,
[VendorId] [int] NULL,
[VendorOther] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceTypeId] [int] NULL,
[DeviceMakeModelId] [int] NULL,
[DeviceMakeModelOther] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceSerialNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (18, 6) NULL,
[ActivityQualityId] [int] NULL,
[FissileEnrichment] [decimal] (18, 4) NULL,
[OperationalStatusId] [int] NULL,
[RoomId] [int] NULL,
[NumberOfSources] [int] NULL,
[NumberOfDevices] [int] NULL,
[OriginalActivityDate] [datetime] NULL,
[NoteId] [int] NULL,
[OriginalActivity] [decimal] (18, 4) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IsCompleteInd] [bit] NULL,
[DOEOwnedInd] [bit] NULL,
[NRCLicenseId] [int] NULL,
[IDDInstallDt] [datetime] NULL,
[DeviceAnchoredInd] [bit] NULL,
[IDDNoteId] [int] NULL,
[DeviceSubTypeId] [int] NULL,
[OperatingPower] [decimal] (18, 4) NULL,
[LicensedMaximum] [decimal] (18, 4) NULL,
[SourceTypeId] [int] NULL,
[FissileNuclideId] [int] NULL,
[IDDStatusId] [int] NULL,
[IDDContractId] [int] NULL,
[IDDTaskTypeId] [int] NULL,
[DispositionInd] [bit] NULL,
[DispositionDt] [datetime] NULL,
[DispositionTypeId] [int] NULL,
[DispositionNoteId] [int] NULL,
[ResourcingMethodId] [int] NULL,
[InScopeInd] [bit] NULL,
[IDDCandidateStatusOverrideId] [int] NULL,
[EnergyRange] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnergyUnitId] [int] NULL,
[InstallDt] [datetime] NULL,
[SupportedInstallInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SiteTransportationVendorXrefTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[TransportationVendorId] [int] NULL,
[ShipmentsPerYear] [int] NULL,
[CuriesPerShipment] [decimal] (18, 4) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SiteTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (18, 10) NULL,
[Longitude] [decimal] (18, 10) NULL,
[Zoom] [int] NULL,
[NearestCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgencyOrganizationId] [int] NULL,
[UASITierId] [int] NULL,
[Address1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyId] [int] NULL,
[NoteId] [int] NULL,
[SiteVisitRequestDt] [datetime] NULL,
[EstimatedCost] [decimal] (18, 2) NULL,
[IsCompleteInd] [bit] NULL,
[IsChildrenCompleteInd] [bit] NULL,
[SortOrder] [int] NULL,
[RadSiteInd] [bit] NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[CountyConfirmInd] [bit] NULL,
[ExcludeSPAInd] [bit] NULL,
[TransitionReasonId] [int] NULL,
[TransitionReasonNoteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SiteResponseForceTable] AS TABLE
(
[Id] [int] NULL,
[LeaId] [int] NULL,
[SiteId] [int] NULL,
[PriorityOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SPAMainId] [int] NULL,
[IsOtherInd] [bit] NULL,
[IsOnSiteSecurityInd] [bit] NULL,
[NoContactInd] [bit] NULL,
[NoteId] [int] NULL,
[UnitName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SiteCostShareSubCategoryTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[CostShareSubCategoryId] [int] NULL,
[DefaultVariable] [int] NULL,
[Variable] [int] NULL,
[DefaultWeight] [decimal] (18, 2) NULL,
[Weight] [decimal] (18, 2) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SiteAKATable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ServiceResponseTable] AS TABLE
(
[Id] [int] NULL,
[ServiceRequestId] [int] NULL,
[ServiceResponseStatusId] [int] NULL,
[ResponseDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ServiceRequestTable] AS TABLE
(
[Id] [int] NULL,
[ServiceName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MethodName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HttpRequestHeader] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HttpRequestBody] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CallDt] [datetime] NULL,
[CalledBy] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalledAs] [nvarchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SecurityUpgradeTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[Revision] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[SecurityActionId] [int] NULL,
[Upgrades] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraInfo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SecurityUpgradeSecurityUpgradeOptionTable] AS TABLE
(
[Id] [int] NULL,
[SecurityUpgradeId] [int] NULL,
[SecurityUpgradeOptionId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SecurityUpgradeOptionTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SecurityActionAnswerTable] AS TABLE
(
[Id] [int] NULL,
[SecurityActionId] [int] NULL,
[SPAMainId] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[Upgrades] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[CompletedInd] [bit] NULL,
[ExistsInd] [bit] NULL,
[RecommendedInd] [bit] NULL,
[NANRInd] [bit] NULL,
[ExtraInfo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Revision] [int] NULL,
[FollowOnInd] [bit] NULL,
[FollowOnCompletedInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[STARSImportStagingTable] AS TABLE
(
[Id] [int] NULL,
[BatchId] [int] NULL,
[SubSystemId] [int] NULL,
[Month] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiscalYear] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Performer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BNR] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YTDAFP] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Carryover] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthlyCommitment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Obligations] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalUnencumbered] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppropriationYear] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YTDEndingUncostedObligations] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[RowNumber] [int] NULL,
[StatusCodeId] [int] NULL,
[StatusMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundValue] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAStatusLogTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[Revision] [int] NULL,
[SPAStatusId] [int] NULL,
[SPAStatusDt] [datetime] NULL,
[Comment] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPASectionTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IdCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPASOWTemplateTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportPath] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAMainTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IsCompleteInd] [bit] NULL,
[SPAStatusId] [int] NULL,
[SPAStatusNoteId] [int] NULL,
[ResponseForceNoteId] [int] NULL,
[Revision] [int] NULL,
[CancelledInd] [bit] NULL,
[CancelReasonId] [int] NULL,
[CancelReasonNoteId] [int] NULL,
[SPAStatusDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAMainSectionTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[IsIncludedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SPASectionTypeId] [int] NULL,
[Label] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCompleteInd] [bit] NULL,
[IsCompleteOverrideInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAMainGeneralTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[EventDt] [datetime] NULL,
[Personnel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalBackgroundNoteId] [int] NULL,
[SiteSecurityConditionId] [int] NULL,
[SiteSecurityConditionNoteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[RMSConfirmedInd] [bit] NULL,
[RMSConfirmedDt] [datetime] NULL,
[RMSConfirmedBy] [int] NULL,
[RMSConfirmedAs] [int] NULL,
[Revision] [int] NULL,
[RevisionPurposeId] [int] NULL,
[RevisionPurposeNoteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAMainGeneralSPATeamXrefTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainGeneralId] [int] NULL,
[SPATeamId] [int] NULL,
[PrimaryInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAMainDeviationTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[DeviationCategoryId] [int] NULL,
[DeviationTypeId] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[LocationDetail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviationDetail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAConcurrenceTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[Revision] [int] NULL,
[SPAStatusIdBefore] [int] NULL,
[SPAStatusIdAfter] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAConcurrenceBuildingImplementationOptionTable] AS TABLE
(
[Id] [int] NULL,
[SPAConcurrenceId] [int] NULL,
[BuildingId] [int] NULL,
[RoomId] [int] NULL,
[RMSId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Score] [decimal] (18, 2) NULL,
[ScoreAdjustedInd] [bit] NULL,
[Comments] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImplementationOptionFeatureId] [int] NULL,
[ImplementationOptionId] [int] NULL,
[BuildingOverallImplementationOptionId] [int] NULL,
[BuildingOverallImplementationOptionScore] [decimal] (18, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[SPAArchiveTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[ArchivedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[Revision] [int] NULL,
[SubRevision] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RoomTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[SortOrder] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RoomTable] AS TABLE
(
[Id] [int] NULL,
[BuildingId] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[NoteId] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedAs] [int] NULL,
[IsCompleteInd] [bit] NULL,
[IsChildrenCompleteInd] [bit] NULL,
[UseBuildingPOCInd] [bit] NULL,
[PersonnelWithAccess] [int] NULL,
[HoursOfOperation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[RoomTypeId] [int] NULL,
[AccessControlImplementationOptionId] [int] NULL,
[AccessControlImplementationOptionNoteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RoomRoomAccessControlChallengeXrefTable] AS TABLE
(
[Id] [int] NULL,
[RoomId] [int] NULL,
[RoomAccessControlChallengeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RoomAKATable] AS TABLE
(
[Id] [int] NULL,
[RoomId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RevisionPurposeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ResponseSubFunctionTable] AS TABLE
(
[Id] [int] NULL,
[ResponseFunctionId] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Control] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValidValues] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TextLength] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ResponseSubFunctionAnswerTable] AS TABLE
(
[Id] [int] NULL,
[ResponseSubFunctionId] [int] NULL,
[SPAMainId] [int] NULL,
[AnswerText1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnswerText2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnswerText3] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ResponseOrganizationTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ResponseFunctionTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RegionTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Abbreviation] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (18, 10) NULL,
[Longitude] [decimal] (18, 10) NULL,
[Zoom] [int] NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[CreatedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RegionStateXRefTable] AS TABLE
(
[ID] [int] NULL,
[RegionId] [int] NULL,
[StateId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSTable] AS TABLE
(
[Id] [int] NULL,
[RoomId] [int] NULL,
[SerialNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoteId] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[RMSMonitoringStrategyId] [int] NULL,
[FeaturesImplementationOptionNoteId] [int] NULL,
[MonitoringLocationsImplementationOptionNoteId] [int] NULL,
[CommunicationsImplementationOptionNoteId] [int] NULL,
[AlarmResolutionImplementationOptionNoteId] [int] NULL,
[MonitoringLocationsImplementationOptionId] [int] NULL,
[FeaturesImplementationOptionId] [int] NULL,
[CommunicationsImplementationOptionId] [int] NULL,
[AlarmResolutionImplementationOptionId] [int] NULL,
[InstallInd] [bit] NULL,
[RMSMatrixNoteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSSystemFeatureTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSMonitoringLocationXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSMonitoringLocationId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSMonitoringLocationChallengeXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSMonitoringLocationChallengeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSFeatureXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSFeatureId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSFeatureChallengeXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSFeatureChallengeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSCommunicationXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSCommunicationId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSCommunicationChallengeXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSCommunicationChallengeId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSRMSAlarmResolutionXrefTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[RMSAlarmResolutionId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSMonitorTable] AS TABLE
(
[Id] [int] NULL,
[RMSId] [int] NULL,
[SignalNumber] [int] NULL,
[SiteResponseForceId] [int] NULL,
[AlarmMonitorCompanyServiceCityId] [int] NULL,
[AlarmMonitorOtherId] [int] NULL,
[OtherName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSMatrixTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DescriptionExtra] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level] [int] NULL,
[ParentId] [int] NULL,
[QuestionInd] [bit] NULL,
[RequireCommentInd] [bit] NULL,
[ActiveInd] [bit] NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ExclusiveInd] [bit] NULL,
[DomesticInd] [bit] NULL,
[ElementWBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSMatrixAnswerTable] AS TABLE
(
[Id] [int] NULL,
[RMSMatrixId] [int] NULL,
[SPAMainId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[NoteId] [int] NULL,
[RMSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[RMSFeatureTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ElementWBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[QuestionDataTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Control] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CssClass] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProxyTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[ProxyFor] [int] NULL,
[SubSystemId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProtectionStrategyTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProjectMilestoneTable] AS TABLE
(
[Id] [int] NULL,
[MilestoneId] [int] NULL,
[TaskId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayOrder] [int] NULL,
[BaselineFinishDt] [datetime] NULL,
[FinishDt] [datetime] NULL,
[CompletedInd] [bit] NULL,
[MilestoneWeight] [int] NULL,
[JouleMetricInd] [bit] NULL,
[InternalMetricInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SequenceNumber] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProjectMilestoneMetricTable] AS TABLE
(
[Id] [int] NULL,
[ProjectMilestoneId] [int] NULL,
[MetricId] [int] NULL,
[BaselineValue] [decimal] (18, 9) NULL,
[ForecastValue] [decimal] (18, 9) NULL,
[JouleTargetInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[HasMetricPlanInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProjectBNRXRefTable] AS TABLE
(
[Id] [int] NULL,
[ProjectId] [int] NULL,
[BNRId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ProjectAttributeTable] AS TABLE
(
[Id] [int] NULL,
[ProjectId] [int] NULL,
[ProjectAttributeTypeId] [int] NULL,
[Value] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PreferenceChoiceTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowMultipleValues] [bit] NULL,
[DisplayText] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PointOfContactXrefTable] AS TABLE
(
[Id] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[PointOfContactId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[PositionTypeId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PointOfContactTable] AS TABLE
(
[Id] [int] NULL,
[EmailAddress] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobTitle] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NULL,
[ActiveInd] [bit] NULL,
[InactiveDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PersonTable] AS TABLE
(
[Id] [int] NULL,
[LoginName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AIMSId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[G2UserInd] [bit] NULL,
[FedInd] [bit] NULL,
[ActiveInd] [bit] NULL,
[JobTitle] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[MobileUserInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PermissionRoleTable] AS TABLE
(
[Id] [int] NULL,
[SubSystemId] [int] NULL,
[PermissionId] [int] NULL,
[RoleId] [int] NULL,
[withAdvance] [bit] NULL,
[WorkflowLevel] [int] NULL,
[ChangeRequestTypeId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[PermissionObjectId] [int] NULL,
[ReportCatalogId] [int] NULL,
[SPAStatusId] [int] NULL,
[RoleOnlyOverrideInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[AssessmentHierarchyId] [int] NULL,
[PreventFullDetailExpansionInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PerformanceReportingJouleMilestoneDateRestrictionExceptionTable] AS TABLE
(
[Id] [int] NULL,
[OverrideDt] [datetime] NULL,
[StartDt] [datetime] NULL,
[EndDt] [datetime] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[SubSystemId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[PerformanceReportRawTable] AS TABLE
(
[Id] [int] NULL,
[RowNum] [int] NULL,
[BatchId] [int] NULL,
[TaskId] [int] NULL,
[ProjectMilestoneId] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewForecastDt] [datetime] NULL,
[NewCompletedInd] [bit] NULL,
[NewTaskStatus] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusCodeId] [int] NULL,
[StatusMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ParticipantQueueTable] AS TABLE
(
[Id] [int] NULL,
[EventTypeId] [int] NULL,
[ParticipantTypeId] [int] NULL,
[WillHost] [bit] NULL,
[SeatsRequested] [int] NULL,
[DateRequested] [datetime] NULL,
[Comments] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[DeletedInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ParticipantQueueSiteTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ParticipantQueueLeaTable] AS TABLE
(
[Id] [int] NULL,
[LeaId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OtherCostsImportLineItemTable] AS TABLE
(
[Id] [int] NULL,
[BatchId] [int] NULL,
[LineItemStatusId] [int] NULL,
[FundTypeShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BNRInformalCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullWBS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionTypeShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[TransactionCategoryShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OrphanedSourceTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[FoundDt] [datetime] NULL,
[Isotope] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activity] [decimal] (18, 6) NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[OrphanedDeviceId] [int] NULL,
[Quantity] [int] NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OrphanedDeviceTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [int] NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[FoundDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OrphanSourceTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[DateFound] [datetime] NULL,
[Isotope] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoseRate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasurementDistance] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OrganizationCategoryTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnumName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OperationalStatusTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[OSRPStatusTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[NoteTable] AS TABLE
(
[Id] [int] NULL,
[Note] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[NRCSubGroupTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[NRCLicenseTable] AS TABLE
(
[Id] [int] NULL,
[LicenseNo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[NRCGroupId] [int] NULL,
[NRCSubGroupId] [int] NULL,
[NRCBusinessDesignationId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[NRCGroupTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[NRCBusinessDesignationTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[MobileSitePersonXrefTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[PersonId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[MediaTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[MediaTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoryDt] [datetime] NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediaTypeId] [int] NULL,
[Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeedId] [int] NULL,
[VideoId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL,
[StartDt] [datetime] NULL,
[EndDt] [datetime] NULL,
[DisplayOrder] [int] NULL,
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[MaterialTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LogSubSystemChangeTable] AS TABLE
(
[PersonId] [int] NULL,
[SubSystemId] [int] NULL,
[ChangedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LogSessionActivityTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[LogDate] [datetime] NULL,
[Note] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SessionId] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LogProxyChangeTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[ForPersonId] [int] NULL,
[Note] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogDt] [datetime] NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LawEnforcementAgencyTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LawEnforcementAgencyTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AKA] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyId] [int] NULL,
[StateId] [int] NULL,
[CountryId] [int] NULL,
[AgencyTypeId] [int] NULL,
[SpecialFunction] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialJurisdiction] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GovernmentTypeId] [int] NULL,
[FipsPlaceCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FipsMsaCode] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationCategoryId] [int] NULL,
[WebURL] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (18, 10) NULL,
[Longitude] [decimal] (18, 10) NULL,
[AgencyId] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginatingAgencyId] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UcrMultipleCountyFlag] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCodeLowest] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCodeHighest] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordSource] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[OriginatingAgencyIdentifier] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[LawEnforcementAgencyCountyXrefTable] AS TABLE
(
[Id] [int] NULL,
[LeaId] [int] NULL,
[AgencyId] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[CountyId] [int] NULL,
[PopulationCovered] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[IDDSectionTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[IDDContractTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[GovernmentTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[GeoBookMarkTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[Reference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonBookmark] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[G2ParametersTable] AS TABLE
(
[Id] [int] NULL,
[SubSystemId] [int] NULL,
[ParmKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [nvarchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Datatype] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FissileNuclideTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[SortOrder] [int] NULL,
[ActiveInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FileUploadTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IDCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FileUploadTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameUnique] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Author] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[FileDt] [datetime] NULL,
[FileUploadTypeId] [int] NULL,
[NoteId] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[RoomDiagramId] [int] NULL,
[RoomDiagramUpdateAt] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FileExtensionTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MimeType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ImageInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FeedTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL,
[RetentionDays] [int] NULL,
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[FeedKeywordTable] AS TABLE
(
[Id] [int] NULL,
[Keyword] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL,
[FeedId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventTypeTable] AS TABLE
(
[Id] [int] NULL,
[Abbreviation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultDurationDays] [int] NULL,
[DefaultLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxAttendees] [int] NULL,
[MaxSites] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[CountryId] [int] NULL,
[LinkedMilestoneId] [int] NULL,
[Color] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventTable] AS TABLE
(
[Id] [int] NULL,
[AltEventId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDt] [datetime] NULL,
[EndDt] [datetime] NULL,
[EventStatusId] [int] NULL,
[EventTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[PointOfContactId] [int] NULL,
[DeletedInd] [bit] NULL,
[ChangeRequestId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventRoleTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventParticipantTable] AS TABLE
(
[Id] [int] NULL,
[EventId] [int] NULL,
[ParticipantTypeId] [int] NULL,
[AllocatedAttendees] [int] NULL,
[ParticipantStatusId] [int] NULL,
[ApprovedDt] [datetime] NULL,
[ConfirmedDt] [datetime] NULL,
[InvitationsSentDt] [datetime] NULL,
[Comment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParticipantRoleId] [int] NULL,
[ParticipantQueueId] [int] NULL,
[LastStatusChangeDt] [datetime] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[DeletedInd] [bit] NULL,
[ChangeRequestDetailId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventParticipantSiteTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[OrigUpgradesCompleteDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventParticipantLeaTable] AS TABLE
(
[Id] [int] NULL,
[LeaId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventParticipantCountryTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EventAttendeeTable] AS TABLE
(
[Id] [int] NULL,
[EventId] [int] NULL,
[SiteId] [int] NULL,
[LeaId] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreferredName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Organization] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PositionTitle] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttendeeCategoryId] [int] NULL,
[AttendeeResponsibilityId] [int] NULL,
[OrganizationCategoryId] [int] NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[Occupation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedAs] [int] NULL,
[DeletedInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EquipmentTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[EventId] [int] NULL,
[SerialNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[EquipmentStatusId] [int] NULL,
[EquipmentSuiteId] [int] NULL,
[AgencyName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EquipmentSuiteTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[CountryId] [int] NULL,
[EventId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EquipmentStatusTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EquipmentReturnTable] AS TABLE
(
[Id] [int] NULL,
[EquipmentId] [int] NULL,
[ReturnReason] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReturnDt] [datetime] NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EquipmentRepairTable] AS TABLE
(
[Id] [int] NULL,
[EquipmentId] [int] NULL,
[RepairDescription] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepairDt] [datetime] NULL,
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EntityTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefTable] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IdCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EntityTypeFileUploadTypeXrefTable] AS TABLE
(
[Id] [int] NULL,
[EntityTypeId] [int] NULL,
[FileUploadTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[DisplayInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EntityTypeFileExtensionTypeXrefTable] AS TABLE
(
[Id] [int] NULL,
[EntityTypeId] [int] NULL,
[FileExtensionTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ElementTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElementLevelId] [int] NULL,
[ParentId] [int] NULL,
[SharedRatingApplyInd] [bit] NULL,
[WBSNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[Score] [decimal] (18, 2) NULL,
[RequiredInd] [bit] NULL,
[ElementWBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ElementSupportFileXrefTable] AS TABLE
(
[Id] [int] NULL,
[ElementId] [int] NULL,
[SupportFileId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ElementLevelTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[EffectivenessRatingTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SubElementQuestionSetId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[DeviceTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[MaterialTypeId] [int] NULL,
[MultipleChildrenInd] [bit] NULL,
[SortOrder] [int] NULL,
[ActiveInd] [bit] NULL,
[ResourcingMethodInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[DeviceTypeDeviceSubTypeXrefTable] AS TABLE
(
[Id] [int] NULL,
[DeviceTypeId] [int] NULL,
[DeviceSubTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[DefaultInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[DeviceTypeBuildingTypeXRefTable] AS TABLE
(
[Id] [int] NULL,
[DeviceTypeId] [int] NULL,
[DeviceSubTypeId] [int] NULL,
[BuildingTypeId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[DeviceSubTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[DeviceMakeModelTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[IDDCandidateInd] [bit] NULL,
[DefaultSources] [int] NULL,
[IDDSurveyInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CountyTable] AS TABLE
(
[Id] [int] NULL,
[StateId] [int] NULL,
[Name] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CountryThreatRatingTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Score] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CountryTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (18, 10) NULL,
[Longitude] [decimal] (18, 10) NULL,
[Zoom] [int] NULL,
[HasStateInd] [bit] NULL,
[SortOrder] [int] NULL,
[FIPS] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PinLatitude] [decimal] (18, 10) NULL,
[PinLongitude] [decimal] (18, 10) NULL,
[PWPLatitude] [decimal] (18, 10) NULL,
[PWPLongitude] [decimal] (18, 10) NULL,
[PWPZoom] [int] NULL,
[CreatedDt] [datetime] NULL,
[CountryThreatRatingId] [int] NULL,
[Continent] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastModifiedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CountryAssessmentSPATeamXrefTable] AS TABLE
(
[Id] [int] NULL,
[CountryAssessmentId] [int] NULL,
[SPATeamId] [int] NULL,
[PrimaryInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CostSubmissionImportLineItemTable] AS TABLE
(
[Id] [int] NULL,
[BatchId] [int] NULL,
[LineItemStatusId] [int] NULL,
[FundTypeShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BNRInformalCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullWBS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionTypeShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[TransactionCategoryShortName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CostSharingRatingTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Value] [decimal] (18, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CostShareSubCategoryTable] AS TABLE
(
[Id] [int] NULL,
[CostShareCategoryId] [int] NULL,
[CostShareTypeId] [int] NULL,
[CostShareFrequencyId] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Variable] [int] NULL,
[Weight] [decimal] (18, 2) NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CoordinateAccuracyTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[GlobalZoom] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ContractWorkBreakdownStructureReportingEntityBNRXrefTable] AS TABLE
(
[Id] [int] NULL,
[ContractId] [int] NULL,
[SubSystemId] [int] NULL,
[BnRId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[IsImported] [bit] NULL,
[TransferedBy] [int] NULL,
[TransferDt] [datetime] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[STARSCIDCODE] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ContractTable] AS TABLE
(
[Id] [int] NULL,
[STARSCIDCODE] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InactiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CongressionalJouleTargetFYStatusTable] AS TABLE
(
[Id] [int] NULL,
[FiscalYear] [int] NULL,
[CongressionalJouleTargetStatusId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SubSystemId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CongressionalJouleTable] AS TABLE
(
[Id] [int] NULL,
[RollupId] [int] NULL,
[FiscalYear] [int] NULL,
[MetricValue] [decimal] (18, 4) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[CityTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NULL,
[StateId] [int] NULL,
[CountyId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ChangeRequestTable] AS TABLE
(
[Id] [int] NULL,
[ProjectId] [int] NULL,
[PeriodId] [int] NULL,
[ParentChangeRequestId] [int] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[CreatedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[ChangeRequestDetailTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestId] [int] NULL,
[ChangeRequestTypeId] [int] NULL,
[PeriodId] [int] NULL,
[WorkflowLevel] [int] NULL,
[ChangeRequestDetailStatusId] [int] NULL,
[StatusDt] [datetime] NULL,
[Explanation] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[WorkflowLevelDt] [datetime] NULL,
[ExplanationDt] [datetime] NULL,
[ExplanationBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingTypeTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PluralName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IconImage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[PriorityOrder] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingTagTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[AllowCommentInd] [bit] NULL,
[ElementWBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[CountryId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (18, 10) NULL,
[Longitude] [decimal] (18, 10) NULL,
[Zoom] [int] NULL,
[BuildingTypeId] [int] NULL,
[NearestCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoordinateAccuracyId] [int] NULL,
[ActionCompletedDt] [datetime] NULL,
[VerifiedDt] [datetime] NULL,
[Address1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyId] [int] NULL,
[CountyConfirmInd] [bit] NULL,
[NearStrategicAssetInd] [bit] NULL,
[PersonnelWithAccess] [int] NULL,
[HoursOfOperation] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseSiteAddressInd] [bit] NULL,
[UseSitePOCInd] [bit] NULL,
[NoteId] [int] NULL,
[IsCompleteInd] [bit] NULL,
[IsChildrenCompleteInd] [bit] NULL,
[SortOrder] [int] NULL,
[RadSiteInd] [bit] NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ExcludeSPAInd] [bit] NULL,
[MaterialAttractivenessLevelId] [int] NULL,
[ProtectionStrategyId] [int] NULL,
[AccessControlImplementationOptionId] [int] NULL,
[RMSImplementationOptionId] [int] NULL,
[InScopeInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingHardCopyTable] AS TABLE
(
[Id] [int] NULL,
[SPAMainId] [int] NULL,
[Revision] [int] NULL,
[BuildingId] [int] NULL,
[MailDt] [datetime] NULL,
[NoteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingExtensionSPATable] AS TABLE
(
[BuildingId] [int] NULL,
[SPAVersionId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[SustainabilityRevisionCount] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingBuildingTagXrefTable] AS TABLE
(
[Id] [int] NULL,
[BuildingId] [int] NULL,
[BuildingTagId] [int] NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BuildingAKATable] AS TABLE
(
[Id] [int] NULL,
[BuildingId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeletedInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[BatchTable] AS TABLE
(
[Id] [int] NULL,
[BatchTypeId] [int] NULL,
[BatchStatusId] [int] NULL,
[FilePath] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalFilename] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AuditLogTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[LogDate] [datetime] NULL,
[Note] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SessionId] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AuditLogProxyTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[ForPersonId] [int] NULL,
[Note] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogDt] [datetime] NULL,
[CreatedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AssessmentVisitTable] AS TABLE
(
[Id] [int] NULL,
[BuildingId] [int] NULL,
[FY] [int] NULL,
[NoteId] [int] NULL,
[IsCompleteInd] [bit] NULL,
[DeletedInd] [bit] NULL,
[DeleteComment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[AssessmentVisitDeleteReasonId] [int] NULL,
[DeletedDt] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AssessmentVisitDeleteReasonTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AssessmentTable] AS TABLE
(
[Id] [int] NULL,
[AssessmentDt] [datetime] NULL,
[IsCompleteInd] [bit] NULL,
[NoteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ElementId] [int] NULL,
[EntityTypeId] [int] NULL,
[RefId] [int] NULL,
[CostSharingRatingId] [int] NULL,
[SubElementQuestionSetId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AssessmentSiteYearTable] AS TABLE
(
[Id] [int] NULL,
[SiteId] [int] NULL,
[FY] [int] NULL,
[NoteId] [int] NULL,
[IsCompleteInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AssessmentElementTable] AS TABLE
(
[Id] [int] NULL,
[AssessmentId] [int] NULL,
[AssessmentDt] [datetime] NULL,
[IsCompleteInd] [bit] NULL,
[NoteId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL,
[ElementId] [int] NULL,
[CostSharingRatingId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AlarmMonitorOtherTable] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AlarmMonitorCompanyTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ULIdentifiers] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ULApprovedInd] [bit] NULL,
[ActiveInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGen].[AlarmMonitorCompanyServiceCityTable] AS TABLE
(
[Id] [int] NULL,
[AlarmMonitorCompanyId] [int] NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[ProxyDeleteTable] AS TABLE
(
[PersonId] [int] NULL,
[ProxyFor] [int] NULL,
[SubSystemId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[ProjectMilestoneMetricStatusTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestDetailId] [int] NULL,
[ProjectMilestoneId] [int] NULL,
[MetricID] [int] NULL,
[ForecastValue] [decimal] (18, 4) NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[PersonRoleTable] AS TABLE
(
[Id] [int] NULL,
[SubSystemId] [int] NULL,
[PersonId] [int] NULL,
[RoleId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[SPATeamId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[AssessmentHierarchyId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[GeoBookMarkTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[Reference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonBookmark] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GlobalInd] [bit] NULL,
[SubSystemId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[GeoBookMarkPersonXrefTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[BookMarkId] [int] NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [xGenCustom].[FundingAdjustmentRequestTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestDetailId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[FundTypeId] [int] NULL,
[BnRId] [int] NULL,
[Explanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (18, 2) NULL,
[AppropriationYear] [int] NULL,
[FundingSourceId] [int] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[FundingAdjustmentRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[FundingAdjustmentRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[GeoBookMarkPersonXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[GeoBookMarkPersonXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[GeoBookMarkTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[GeoBookMarkTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[PersonRoleTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[PersonRoleTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[ProjectMilestoneMetricStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[ProjectMilestoneMetricStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGenCustom].[ProxyDeleteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGenCustom].[ProxyDeleteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AlarmMonitorCompanyServiceCityTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AlarmMonitorCompanyServiceCityTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AlarmMonitorCompanyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AlarmMonitorCompanyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AlarmMonitorOtherTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AlarmMonitorOtherTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AssessmentElementTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AssessmentElementTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AssessmentSiteYearTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AssessmentSiteYearTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AssessmentTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AssessmentTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AssessmentVisitDeleteReasonTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AssessmentVisitDeleteReasonTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AssessmentVisitTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AssessmentVisitTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AuditLogProxyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AuditLogProxyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[AuditLogTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[AuditLogTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BatchTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BatchTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingAKATable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingAKATable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingBuildingTagXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingBuildingTagXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingExtensionSPATable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingExtensionSPATable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingHardCopyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingHardCopyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingTagTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingTagTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[BuildingTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[BuildingTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ChangeRequestDetailTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ChangeRequestDetailTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ChangeRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ChangeRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CityTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CityTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CongressionalJouleTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CongressionalJouleTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CongressionalJouleTargetFYStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CongressionalJouleTargetFYStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ContractTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ContractTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ContractWorkBreakdownStructureReportingEntityBNRXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ContractWorkBreakdownStructureReportingEntityBNRXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CoordinateAccuracyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CoordinateAccuracyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CostShareSubCategoryTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CostShareSubCategoryTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CostSharingRatingTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CostSharingRatingTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CostSubmissionImportLineItemTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CostSubmissionImportLineItemTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CountryAssessmentSPATeamXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CountryAssessmentSPATeamXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CountryTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CountryTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CountryThreatRatingTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CountryThreatRatingTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[CountyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[CountyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[DeviceMakeModelTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[DeviceMakeModelTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[DeviceSubTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[DeviceSubTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[DeviceTypeBuildingTypeXRefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[DeviceTypeBuildingTypeXRefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[DeviceTypeDeviceSubTypeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[DeviceTypeDeviceSubTypeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[DeviceTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[DeviceTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EffectivenessRatingTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EffectivenessRatingTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ElementLevelTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ElementLevelTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ElementSupportFileXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ElementSupportFileXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ElementTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ElementTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EntityTypeFileExtensionTypeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EntityTypeFileExtensionTypeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EntityTypeFileUploadTypeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EntityTypeFileUploadTypeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EntityTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EntityTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EquipmentRepairTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EquipmentRepairTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EquipmentReturnTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EquipmentReturnTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EquipmentStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EquipmentStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EquipmentSuiteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EquipmentSuiteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EquipmentTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EquipmentTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventAttendeeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventAttendeeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventParticipantCountryTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventParticipantCountryTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventParticipantLeaTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventParticipantLeaTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventParticipantSiteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventParticipantSiteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventParticipantTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventParticipantTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventRoleTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventRoleTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[EventTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[EventTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FeedKeywordTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FeedKeywordTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FeedTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FeedTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FileExtensionTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FileExtensionTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FileUploadTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FileUploadTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FileUploadTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FileUploadTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[FissileNuclideTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[FissileNuclideTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[G2ParametersTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[G2ParametersTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[GeoBookMarkTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[GeoBookMarkTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[GovernmentTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[GovernmentTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[IDDContractTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[IDDContractTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[IDDSectionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[IDDSectionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LawEnforcementAgencyCountyXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LawEnforcementAgencyCountyXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LawEnforcementAgencyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LawEnforcementAgencyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LawEnforcementAgencyTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LawEnforcementAgencyTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LogProxyChangeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LogProxyChangeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LogSessionActivityTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LogSessionActivityTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[LogSubSystemChangeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[LogSubSystemChangeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[MaterialTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[MaterialTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[MediaTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[MediaTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[MediaTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[MediaTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[MobileSitePersonXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[MobileSitePersonXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[NRCBusinessDesignationTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[NRCBusinessDesignationTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[NRCGroupTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[NRCGroupTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[NRCLicenseTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[NRCLicenseTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[NRCSubGroupTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[NRCSubGroupTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[NoteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[NoteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OSRPStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OSRPStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OperationalStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OperationalStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OrganizationCategoryTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OrganizationCategoryTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OrphanSourceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OrphanSourceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OrphanedDeviceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OrphanedDeviceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OrphanedSourceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OrphanedSourceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[OtherCostsImportLineItemTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[OtherCostsImportLineItemTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ParticipantQueueLeaTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ParticipantQueueLeaTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ParticipantQueueSiteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ParticipantQueueSiteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ParticipantQueueTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ParticipantQueueTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PerformanceReportRawTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PerformanceReportRawTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PerformanceReportingJouleMilestoneDateRestrictionExceptionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PerformanceReportingJouleMilestoneDateRestrictionExceptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PermissionRoleTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PermissionRoleTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PersonTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PersonTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PointOfContactTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PointOfContactTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PointOfContactXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PointOfContactXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[PreferenceChoiceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[PreferenceChoiceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProjectAttributeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProjectAttributeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProjectBNRXRefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProjectBNRXRefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProjectMilestoneMetricTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProjectMilestoneMetricTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProjectMilestoneTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProjectMilestoneTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProtectionStrategyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProtectionStrategyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ProxyTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ProxyTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[QuestionDataTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[QuestionDataTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSFeatureTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSFeatureTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSMatrixAnswerTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSMatrixAnswerTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSMatrixTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSMatrixTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSMonitorTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSMonitorTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSAlarmResolutionXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSAlarmResolutionXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSCommunicationChallengeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSCommunicationChallengeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSCommunicationXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSCommunicationXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSFeatureChallengeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSFeatureChallengeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSFeatureXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSFeatureXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSMonitoringLocationChallengeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSMonitoringLocationChallengeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSRMSMonitoringLocationXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSRMSMonitoringLocationXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSSystemFeatureTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSSystemFeatureTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RMSTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RMSTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RegionStateXRefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RegionStateXRefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RegionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RegionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ResponseFunctionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ResponseFunctionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ResponseOrganizationTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ResponseOrganizationTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ResponseSubFunctionAnswerTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ResponseSubFunctionAnswerTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ResponseSubFunctionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ResponseSubFunctionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RevisionPurposeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RevisionPurposeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RoomAKATable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RoomAKATable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RoomRoomAccessControlChallengeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RoomRoomAccessControlChallengeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RoomTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RoomTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[RoomTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[RoomTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAArchiveTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAArchiveTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAConcurrenceBuildingImplementationOptionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAConcurrenceBuildingImplementationOptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAConcurrenceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAConcurrenceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAMainDeviationTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAMainDeviationTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAMainGeneralSPATeamXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAMainGeneralSPATeamXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAMainGeneralTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAMainGeneralTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAMainSectionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAMainSectionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAMainTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAMainTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPASOWTemplateTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPASOWTemplateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPASectionTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPASectionTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SPAStatusLogTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SPAStatusLogTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[STARSImportStagingTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[STARSImportStagingTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SecurityActionAnswerTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SecurityActionAnswerTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SecurityUpgradeOptionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SecurityUpgradeOptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SecurityUpgradeSecurityUpgradeOptionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SecurityUpgradeSecurityUpgradeOptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SecurityUpgradeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SecurityUpgradeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ServiceRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ServiceRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[ServiceResponseTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[ServiceResponseTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SiteAKATable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SiteAKATable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SiteCostShareSubCategoryTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SiteCostShareSubCategoryTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SiteResponseForceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SiteResponseForceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SiteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SiteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SiteTransportationVendorXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SiteTransportationVendorXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SourceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SourceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SpendPlanDetailRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SpendPlanDetailRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubElementAnswerTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubElementAnswerTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubElementQuestionSetTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubElementQuestionSetTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubElementTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubElementTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubElementTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubElementTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubSystemFundTypeBNRXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubSystemFundTypeBNRXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SubfunctionMilestoneTemplateTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SubfunctionMilestoneTemplateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[SupportFileTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[SupportFileTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskBaselineChangeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskBaselineChangeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskDeviceTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskDeviceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskLevelTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskLevelTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskSiteTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskSiteTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskStatusTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskStatusTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TaskTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TaskTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TopProjectListTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TopProjectListTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TransitionReasonTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TransitionReasonTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TransportationVendorTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TransportationVendorTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[TripTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[TripTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[UASITierTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[UASITierTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[UrbanAreaCountyXRefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[UrbanAreaCountyXRefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[UrbanAreaTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[UrbanAreaTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[UserSystemStatisticsTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[UserSystemStatisticsTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[VendorDeviceTypeXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[VendorDeviceTypeXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[VendorTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[VendorTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[WebRequestLogTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[WebRequestLogTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[WorkBreakdownStructureAttributeTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[WorkBreakdownStructureAttributeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [xGen].[WorkBreakdownStructureDescriptionTable]'
GO
GRANT EXECUTE ON TYPE:: [xGen].[WorkBreakdownStructureDescriptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
