/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 4:42:03 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFundingTransactionTypeGroupId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Obligation TransactionTypeGroup.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFundingTransactionTypeGroupId]()
RETURNS int
AS
BEGIN
	RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetTransactionTypeGroupIdByTransactionTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     5/6/2013
-- Release:     4.1
-- Description: Retrieves the transaction group id for the
--   specified transaction type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetTransactionTypeGroupIdByTransactionTypeId] (
	@TransactionTypeId int
)
RETURNS int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = tt.TransactionTypeGroupId
	FROM Financial.TransactionType tt
	WHERE tt.Id = @TransactionTypeId
	
	RETURN @id;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetNegativeCostAccountErrorTypeId]'
GO
-- ============================================================
-- Author:      Mark Brown
-- Created:     04/12/13
-- Release:     4.1
-- Description: Retrieves the ImportErrorId for Negative Cost
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetNegativeCostAccountErrorTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 2; --Negative Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetImportedLineItemStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Imported line item status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetImportedLineItemStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Imported
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCostTransactionTypeGroupId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Cost TransactionTypeGroup.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCostTransactionTypeGroupId]()
RETURNS int
AS
BEGIN
	RETURN 3;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostOverrunAccountErrorTypeId]'
GO
-- ============================================================
-- Author:      Mark Brown
-- Created:     04/12/13
-- Release:     4.1
-- Description: Retrieves the ImportErrorId for Cost Overrun
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostOverrunAccountErrorTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1; -- Cost Overrun
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCommitmentTransactionTypeGroupId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Commitment TransactionTypeGroup.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCommitmentTransactionTypeGroupId]()
RETURNS int
AS
BEGIN
	RETURN 2;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetAdditionalCommitmentTransactionTypeGroupId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/15/2013
-- Release:     4.1
-- Description: Retrieves the transaction type group for
--   "Additional Commitments".
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetAdditionalCommitmentTransactionTypeGroupId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Additional Commitment
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnCheckSubSystemHasDistinctActiveShortNamePerTransactionTypeGroupId]'
GO
-- =============================================================
-- Author:      Christopher Luttrell (CORP\chris.luttrell)
-- Created:     10/10/2016
-- Release:     7.3
-- Description: Enforces that we only have one site RequestRankGroup 
--				per PlanningConfiguration.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnCheckSubSystemHasDistinctActiveShortNamePerTransactionTypeGroupId]
(
	@SubSystemId int,
	@TransactionTypeId int
)
RETURNS INT
AS 
BEGIN
	DECLARE @valid INT = 1;

	SELECT @valid = 0
	FROM Financial.TransactionType TT
	INNER JOIN Financial.SubSystemTransactionType SSTT ON SSTT.TransactionTypeId = TT.Id
	WHERE SSTT.SubSystemId = @SubSystemId
	AND TransactionTypeId = @TransactionTypeId
	AND SSTT.ActiveInd = 1
	GROUP BY SSTT.SubSystemId,TT.ShortName, TT.TransactionTypeGroupId
	HAVING COUNT(*) > 1;

	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetValidLineItemStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Valid line item status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetValidLineItemStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Valid
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetUnburdenedCommitmentTransactionTypeId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Unburdened Commitment TransactionType.
--
-- mm/dd/yy  Name        Release  Description
-- 03/21/18  GJH         8.5      Changed to use whatever is in the commitment transaction type group for the subsystem.
--                                NOTE: This will break if there is more than one per subsystem - should be modeled as tech debt.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetUnburdenedCommitmentTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	DECLARE @transactionTypeId INT,
			@commitmentTransactionTypeGroupId INT = Financial.fnGetCommitmentTransactionTypeGroupId();

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	WHERE sstt.SubSystemId = @SubSystemId
	AND tt.TransactionTypeGroupId = @commitmentTransactionTypeGroupId
	AND sstt.ActiveInd = 1;

	RETURN @transactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [OtherCosts].[fnGetOtherDirectCostTransactionCategoryId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/24/2015 
-- Release:     6.4
-- Description: Returns the Other Direct Cost TransactionCategoryId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [OtherCosts].[fnGetOtherDirectCostTransactionCategoryId]()
RETURNS int
AS
BEGIN
  RETURN 3; --Other Direct Cost
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidWBSImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid WBS import error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidWBSImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 5; -- Invalid WBS
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidWBSConfigurationImportErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/20/2015 
-- Release:     6.4
-- Description: Returns the Invalid WBS Configuration ImportErrorTypeId 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [CostSubmission].[fnGetInvalidWBSConfigurationImportErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 10; --Invalid WBS Configuration
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidTransactionTypeImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid Transaction Type import
--   error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidTransactionTypeImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Invalid Transaction Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidTransactionCategoryImportErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/20/2015 
-- Release:     6.4
-- Description: Returns the Invalid Transaction Category ImportErrorTypeId 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [CostSubmission].[fnGetInvalidTransactionCategoryImportErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 9; --Invalid Transaction Category
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidLineItemStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid line item status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidLineItemStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Invalid
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidFundTypeImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid Fund Type import error
--   type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidFundTypeImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Invalid Fund Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidFundTypeBNRImportErrorTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid Fund Type/BNR import error
--   type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidFundTypeBNRImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- Invalid Fund Type/BNR
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidBNRImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid BNR import error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidBNRImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Invalid BNR
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidAmountPrecisionImportErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     4/14/2015
-- Release:     6.0
-- Description: Retrieves the Invalid Amount Precision import error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidAmountPrecisionImportErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 8; -- Invalid Amount Precision
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetInvalidAmountImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Invalid Amount import error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetInvalidAmountImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Invalid Amount
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [OtherCosts].[fnGetIndirectCostTransactionCategoryId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/24/2015 
-- Release:     6.4
-- Description: Returns the Indirect Cost TransactionCategoryId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [OtherCosts].[fnGetIndirectCostTransactionCategoryId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Indirect Cost
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetIndirectAndOtherDirectCostTransactionTypeGroupId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/24/2015 
-- Release:     6.4
-- Description: Returns the Indirect and Other Direct Cost TransactionTypeGroupId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Financial].[fnGetIndirectAndOtherDirectCostTransactionTypeGroupId]()
RETURNS int
AS
BEGIN
  RETURN 5; --Indirect and Other Direct Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetDuplicateEntryImportErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/19/2013
-- Release:     4.1
-- Description: Retrieves the Duplicate Entry import error type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetDuplicateEntryImportErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Duplicate Entry
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCommitmentOverheadTransactionTypeId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/17/2013
-- Release:     4.1
-- Description: Retrieves the Commitment Overhead TransactionType.
--
-- mm/dd/yy  Name        Release  Description
-- 03/21/18  GJH         8.5      Changed to use whatever is in the additional commitment transaction type group for the subsystem.
--                                NOTE: This will break if there is more than one per subsystem - should be modeled as tech debt.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCommitmentOverheadTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	DECLARE @transactionTypeId INT,
			@additionalCommitmentTransactionTypeGroupId INT = Financial.fnGetAdditionalCommitmentTransactionTypeGroupId();

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	WHERE sstt.SubSystemId = @SubSystemId
	AND tt.TransactionTypeGroupId = @additionalCommitmentTransactionTypeGroupId
	AND sstt.ActiveInd = 1;

	RETURN @transactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnSubSystemBNRInformalCodeCheck]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     07/17/2014 
-- Release:     5.2
-- Description: Makes sure that a BNR InformalCode is unique per
--				SubSystem.  
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnSubSystemBNRInformalCodeCheck] (
@BNRId int,
@SubSystemId int
)
RETURNS bit
AS
BEGIN
	DECLARE @InvalidInd bit;

	SELECT @InvalidInd = COUNT(ssb.Id)
	FROM Financial.SubSystemBNR ssb
	INNER JOIN Financial.BNR b ON b.Id = ssb.BNRId
	INNER JOIN Financial.BNR b2 
		INNER JOIN Financial.SubSystemBNR ssb2 ON b2.Id = ssb2.BNRId
	ON b.InformalCode = b2.InformalCode AND b.Id <> b2.Id AND ssb.SubSystemId = ssb2.SubSystemId 
	WHERE ssb.BNRId = @BNRId
	AND ssb.SubSystemId = @SubSystemId

  RETURN @InvalidInd; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [OtherCosts].[fnGetDirectCostTransactionCategoryId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/24/2015 
-- Release:     6.4
-- Description: Returns the Direct Cost TransactionCategoryId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [OtherCosts].[fnGetDirectCostTransactionCategoryId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Direct Cost
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostChangeRequestTypeGroupId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Cost ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostChangeRequestTypeGroupId]()
RETURNS
int
AS
BEGIN
	RETURN 11;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCostTransactionTypeId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/25/2014
-- Release:     5.0
-- Description: Retrieves the transaction type for costs by
--   sub-system. This assumes that there is only one cost
--   type in a sub-system.  This currently returns null when
--   the sub-system passed in is NA-21.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	DECLARE @transactionTypeId int;	

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	WHERE sstt.SubSystemId = @SubSystemId
	AND tt.Name = 'Cost';

	RETURN @transactionTypeId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCommitmentTransactionTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/13/2014
-- Release:     5.0
-- Description: Retrieves the transaction type for commitments
--   by sub-system. This assumes that there is only one
--   commitment type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCommitmentTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	DECLARE @transactionTypeId int,
		@commitmentTransactionTypeGroupId int = Financial.fnGetCommitmentTransactionTypeGroupId();

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	WHERE sstt.SubSystemId = @SubSystemId
	AND ttg.Id = @commitmentTransactionTypeGroupId;

	RETURN @transactionTypeId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/11/2013
-- Release:     4.1
-- Description: Retrieves the Cost ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostChangeRequestTypeId]()
RETURNS
int
AS
BEGIN
	RETURN 24;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSourceInformation]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Source Information.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSourceInformation] ()
RETURNS int
AS
BEGIN
	RETURN 11; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetRadDecayConstant]'
GO
-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/14/2009
-- Description:	Returns the Rad Decay Constant
-- =============================================
CREATE FUNCTION [Inventory].[fnGetRadDecayConstant]()
RETURNS NUMERIC(18,6)
AS
BEGIN
	RETURN -0.693147
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetCurrentActivity]'
GO
-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/14/2009
-- Description:	Returns the current radioactivity
-- =============================================
CREATE FUNCTION [Inventory].[fnGetCurrentActivity](
	@reportedActivity NUMERIC(18,6),
	@reportedActivityDate DATETIME,
	@currentActivityDate DATETIME,
	@halfLifeInDays NUMERIC(18,6)
)
RETURNS NUMERIC(18,6)
AS
BEGIN
	DECLARE @decayTimeInDays INT
	DECLARE @currentActivity NUMERIC(18,6)
	
	SET @decayTimeInDays = DATEDIFF(d, @reportedActivityDate, @currentActivityDate)
	
	IF @halfLifeInDays = 0
		SET @currentActivity = 0
	ELSE
		IF @decayTimeInDays < 0
			SET @currentActivity = @reportedActivity
		ELSE
			SET @currentActivity = @reportedActivity * (EXP(Inventory.fnGetRadDecayConstant() * @decayTimeInDays / @halfLifeInDays))
	
	RETURN @currentActivity
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetYearEndForecastChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     01/22/2016
-- Release:     6.5
-- Description: Returns the Year End Forecast change request type group Id
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetYearEndForecastChangeRequestTypeGroupId]  ()
RETURNS INT
AS
BEGIN
  RETURN 16; --Year End Forecast Group
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnIsUsed]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     11/22/2016 
-- Release:     7.4
-- Description: Returns whether or not a given SubSystem uses Year End Forecasting
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnIsUsed] (
	@SubSystemId INT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @usesYEF BIT = 0,
			@yefChangeRequestTypeGroupId INT = YearEndForecast.fnGetYearEndForecastChangeRequestTypeGroupId();

	IF EXISTS(SELECT 1 
		FROM G2.ChangeRequestType crt
		INNER JOIN G2.SubSystemChangeRequestType sscrt ON crt.Id = sscrt.ChangeRequestTypeId
		WHERE crt.ChangeRequestTypeGroupId = @yefChangeRequestTypeGroupId
		AND sscrt.SubSystemId = @SubSystemId)
	BEGIN 
		SELECT @usesYEF = 1;
	END

	RETURN @usesYEF; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetProcessedCollectionStatusId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/3/2016
-- Release:     6.5
-- Description: Return the Id for the processed collection status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetProcessedCollectionStatusId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 2;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCurrentFundingThruPeriod]'
GO
-- =============================================================
-- Author:      Unknown
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================

CREATE FUNCTION [Financial].[fnGetCurrentFundingThruPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Current Funding Thru Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCurrentCostThruPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/10/2013
-- Release:     4.1
-- Description: This function retrieves the current cost thru
--   period.
--
-- mm/dd/yy  Name      Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCurrentCostThruPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @value int;

	SELECT @value = CONVERT(int, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Current Cost Thru Period'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @value;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCurrentCostPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/10/2013
-- Release:     4.1
-- Description: This function retrieves the current cost period.
--
-- mm/dd/yy  Name       Release  Description
-- 01/09/14  j.borgers  5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCurrentCostPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @value int;

	SELECT @value = CONVERT(int, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Current Cost Period'
	AND SubSystemId = @SubSystemId;

	RETURN @value;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetActiveCollectionStatusId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/2/2016
-- Release:     6.5
-- Description: Return the Id for the active collection status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetActiveCollectionStatusId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnCheckQuestionAnswerHasOnlyOneClassificationTypePerClassification]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/18/18 
-- Release:     9.1
-- Description: Checks that a QuesitonAnswer only has one classification type per classification
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnCheckQuestionAnswerHasOnlyOneClassificationTypePerClassification]
(
	@QuestionAnswerXrefId INT,
	@ClassificationTypeId INT
)
RETURNS INT
AS
BEGIN
	--DECLARE @AssetId INT = 1;
	
	DECLARE @valid INT = 1;

	SELECT @valid = 0 
	FROM AssetManagement.ClassificationTypeQuestionAnswerXref ctqax
	INNER JOIN AssetManagement.ClassificationType ct ON ct.Id = ctqax.ClassificationTypeId
	WHERE ctqax.QuestionAnswerXrefId = @QuestionAnswerXrefId
	GROUP BY ct.ClassificationId
	HAVING COUNT(*) > 1;

	--SELECT @valid;
	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnFIMSSiteCodeInFIMSConsolidatedDetails]'
GO
 --=============================================================
 --Author:      Matt Morrell
 --Created:     03/15/2016 
 --Release:     7.0
 --Description: Checks for the existance of a FIMSSiteCode in the FIMSConsolidatedDetails table
 --
 --mm/dd/yy  Name     Release  Description
 --=============================================================
CREATE FUNCTION  [MDI].[fnFIMSSiteCodeInFIMSConsolidatedDetails]
(
	@FIMSSiteCode NVARCHAR(5)
)
RETURNS bit
AS
BEGIN

	DECLARE @existsInd bit = 0;

	SELECT TOP 1 @existsInd = 1
	FROM FIMS.FIMSConsolidatedDetails fcd	
	WHERE fcd.FIMSSiteCode = @FIMSSiteCode
	
	RETURN @existsInd;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ProspectiveFunding].[fnGetDeletedRequestDetailStatusId]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================================
-- Author:      Jill Rochat
-- Created:     9/21/16
-- Release:     7.3
-- Description: Retrieves the Deleted prospective funding request detail status
--
-- mm/dd/yy  Name     Release  Description
-- 04/18/17  JDR      8.0      Modified to facilitate renaming of Funding Request objects
-- =============================================================
CREATE FUNCTION [ProspectiveFunding].[fnGetDeletedRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 1; -- Deleted
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 1/25/2010
-- Description:	Returns the Id of the
--				passed Name
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPAStatusId] (
	@SPAStatusIdCode VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @SPAStatusId INT

	SELECT @SPAStatusId = Id
	FROM SPA.SPAStatus
	WHERE IdCode = @SPAStatusIdCode

	RETURN COALESCE(@SPAStatusId, -1)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbySPATeamLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by SPA Team Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbySPATeamLead] ()
RETURNS int
AS
BEGIN
	RETURN 15; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbySPASubProgramManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by SPA Sub-Program Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbySPASubProgramManager] ()
RETURNS int
AS
BEGIN
	RETURN 23; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by SPA Protect Technical Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead] ()
RETURNS int
AS
BEGIN
	RETURN 19; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbySPAPortfolioManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by SPA Portfolio Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbySPAPortfolioManager] ()
RETURNS int
AS
BEGIN
	RETURN 22; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbySPALabLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by SPA Lab Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbySPALabLead] ()
RETURNS int
AS
BEGIN
	RETURN 18; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPARevisionLastReturnDate]'
GO
-- =============================================
-- Author:        Mike Strickland
-- Create date: 06/30/2017
-- Release:     8.1
-- Description:    Returns the last date that a SPA was returned to originator
--
-- mm/dd/yy  Name     Release  Description
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPARevisionLastReturnDate] (
    @SiteId INT,
    @Revision INT
)
RETURNS DATETIME
AS
BEGIN

    DECLARE
        @SPAStatusIdRejectedbySPATeamLead INT = SPA.fnGetSPAStatusIdRejectedbySPATeamLead(),
        @SPAStatusIdRejectedbySPAPortfolioManager INT = SPA.fnGetSPAStatusIdRejectedbySPAPortfolioManager(),
        @SPAStatusIdRejectedbySPASubProgramManager INT = SPA.fnGetSPAStatusIdRejectedbySPASubProgramManager(),
        @SPAStatusIdRejectedbySPALabLead INT = SPA.fnGetSPAStatusIdRejectedbySPALabLead(),
        @SPAStatusIdRejectedbySPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead(),
        @SPAStatusIdReturnedToOriginator INT = SPA.fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead(),
        @LastReturnDate DATETIME;

    SELECT @LastReturnDate = COALESCE(MAX(LastModifiedDt), '1/1/1900')
    FROM SnapShot.SPAMain 
    WHERE SiteId = @SiteId
    AND Revision = @Revision
    AND SPAStatusId IN (
        @SPAStatusIdRejectedbySPATeamLead,
        @SPAStatusIdRejectedbySPAPortfolioManager,
        @SPAStatusIdRejectedbySPASubProgramManager,
        @SPAStatusIdRejectedbySPALabLead,
        @SPAStatusIdRejectedbySPAProtectTechnicalLead,
        @SPAStatusIdReturnedToOriginator
    );
    
    RETURN @LastReturnDate;

END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoSPATeamLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to SPA Team Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoSPATeamLead] ()
RETURNS int
AS
BEGIN
	RETURN 2; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoSPASubProgramManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to SPA Sub-Program Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoSPASubProgramManager] ()
RETURNS int
AS
BEGIN
	RETURN 25; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to SPA Protect Technical Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead] ()
RETURNS int
AS
BEGIN
	RETURN 4; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoSPAPortfolioManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to SPA Portfolio Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoSPAPortfolioManager] ()
RETURNS int
AS
BEGIN
	RETURN 24; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoSPALabLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to SPA Lab Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoSPALabLead] ()
RETURNS int
AS
BEGIN
	RETURN 13; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdInProgress]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for In Progress.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdInProgress] ()
RETURNS int
AS
BEGIN
	RETURN 1; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdApproved]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Approved.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdApproved] ()
RETURNS int
AS
BEGIN
	RETURN 8; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPARevisionFirstSubmitDate]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 05/14/2014
-- Release:     5.1
-- Description:	Returns the first date that a SPA
--				was submitted
--
-- mm/dd/yyyy   Name        Release  Description 
-- 05/22/2015   Strickland  6.0      Update to determine minimum "submit" date after the previous revision's maximum approved date
-- 08/07/2015   Strickland  6.3      Update for SPA status changes
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPARevisionFirstSubmitDate] (
	@SiteId INT,
	@Revision INT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE
        @SPAStatusIdInProgress INT = SPA.fnGetSPAStatusIdInProgress(),
        @SPAStatusIdSubmittedtoSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
        @SPAStatusIdSubmittedtoSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
        @SPAStatusIdSubmittedtoSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
        @SPAStatusIdSubmittedtoSPASubProgramManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPASubProgramManager(),
        @SPAStatusIdSubmittedtoSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead(),
        @SPAStatusIdApproved INT = SPA.fnGetSPAStatusIdApproved(),
		@RevisionFirstSubmitDate DATETIME,
        @PreviousRevisionMaxApprovedDate DATETIME

    SELECT @PreviousRevisionMaxApprovedDate = COALESCE(MAX(LastModifiedDt), '1/1/1900')
    FROM SnapShot.SPAMain
    WHERE SiteId = @SiteId
    AND Revision = @Revision -1
    AND SPAStatusId = @SPAStatusIdApproved

	SELECT @RevisionFirstSubmitDate = COALESCE(MIN(LastModifiedDt), '12/31/9999')
	FROM SnapShot.SPAMain 
	WHERE SiteId = @SiteId
	AND Revision = @Revision
	AND SPAStatusId IN (
		@SPAStatusIdSubmittedToSPATeamLead, 
		@SPAStatusIdSubmittedToSPAPortfolioManager, 
		@SPAStatusIdSubmittedToSPAProtectTechnicalLead, 
		@SPAStatusIdSubmittedToSPASubProgramManager, 
		@SPAStatusIdSubmittedToSPALabLead,
		@SPAStatusIdApproved
	)
    AND LastModifiedDt > @PreviousRevisionMaxApprovedDate
	
	RETURN @RevisionFirstSubmitDate

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetUseRMSMatrix]'
GO
-- =============================================
-- Author:        Mike Strickland
-- Created:     05/14/2014
-- Release:     5.1
-- Description:    Returns whether or not the SPA should use the RMS Matrix
--
-- mm/dd/yy  Name     Release  Description
-- 08/07/15  MWS      6.3      Update for SPA status changes
-- 06/30/16  MWS      8.1      Update to use new function SPA.fnGetSPARevisionLastReturnDate()
-- =============================================
CREATE FUNCTION [SPA].[fnGetUseRMSMatrix] (
    @SiteId INT
)
RETURNS BIT
AS
BEGIN

    DECLARE 
        @Revision INT,
        @SPAStatusId INT,
        @SPAStatusIdInProgress INT,
        @RMSMatrixStartDate DATETIME,
        @FirstSubmitDate DATETIME,
        @LastRejectDate DATETIME,
        @UseRMSMatrix BIT = 0;

    SELECT 
        @Revision = Revision,
        @SPAStatusId = SPAStatusId,
        @SPAStatusIdInProgress = SPA.fnGetSPAStatusId('InProgress')
    FROM SPA.SPAMain
    WHERE SiteId = @SiteId;

    SELECT @RMSMatrixStartDate = CONVERT(DATETIME, COALESCE(Value, '6/26/2014'))
    FROM G2.G2Parameters
    WHERE ParmKey = 'RMS Matrix Start Date';

    SELECT @FirstSubmitDate = SPA.fnGetSPARevisionFirstSubmitDate(@SiteId, @Revision);
    
    SELECT @LastRejectDate = SPA.fnGetSPARevisionLastReturnDate(@SiteId, @Revision);
    
    IF DATEDIFF(D, @RMSMatrixStartDate, @FirstSubmitDate) > 0 
        OR DATEDIFF(D, @RMSMatrixStartDate, @LastRejectDate) > 0
        OR (@FirstSubmitDate = '12/31/9999' AND @SPAStatusId = @SPAStatusIdInProgress)
        SET @UseRMSMatrix = 1;

    RETURN @UseRMSMatrix;

END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSAlarmResolutionsAndAnswers]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/06/2012
-- Release:     3.7
-- Description: Returns RMS systeam features and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSAlarmResolutionsAndAnswers](
    @RMSId INT
)
RETURNS @RMSAlarmResolutionAndAnswerTable TABLE (
    RMSId INT,
    RMSAlarmResolutionId INT,
    RMSAlarmResolutionDescription NVARCHAR(500),
    RMSAlarmResolutionParentId INT,
    RMSAlarmResolutionSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSAlarmResolutionAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSAlarmResolutionId, F.[Description] RMSAlarmResolutionDescription, COALESCE(F.ParentId, -1) RMSAlarmResolutionParentId, F.SortOrder RMSAlarmResolutionSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSAlarmResolutionXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSAlarmResolution F ON F.Id = X.RMSAlarmResolutionId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSAlarmResolutionId, F.[Description] RMSAlarmResolutionDescription, COALESCE(F.ParentId, -1) RMSAlarmResolutionParentId, F.SortOrder RMSAlarmResolutionSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSAlarmResolution F 
    WHERE F.Id NOT IN (
        SELECT X.RMSAlarmResolutionID
        FROM SPA.RMSRMSAlarmResolutionXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCurrentHQAFPPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the Current HQ AFP Period.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCurrentHQAFPPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Current HQ AFP Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementTypeIdEffectiveness]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/06/2017
-- Release:     8.3
-- Description: Retrieves the id of the SubElementType of Effectiveness
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementTypeIdEffectiveness] ()
RETURNS INT
AS
BEGIN
	RETURN 2; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementsAndAnswers]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     11/02/2012
-- Release:     3.6
-- Description: Returns sustainability sub-element questions and answers
--
-- mm/dd/yy  Name        Release  Description 
-- 02/15/13  Strickland  4.0      Code cleanup.
-- 06/10/13  Strickland  4.2      Add AssessmentDt.
-- 09/16/13  Strickland  4.4      Update to filter on QuestionSet
-- 10/08/13  Strickland  4.4      Add parameters EntityTypeId and RefId to determine the SubElementQuestionSetId for new assessments
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- 10/03/17  Strickland  8.3      Add SubElementIndicatorLevelId, SubElementIndicatorLevelName, SubElementAnswerComment
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementsAndAnswers](
    @AssessmentId INT,
    @ElementId INT,
    @EntityTypeId INT,
    @RefId INT
)
-- The return table should match the user-defined table Sustain.SubElementAndAnswerTable
RETURNS @SubElementAndAnswerTable TABLE (
    ElementId INT,
    ElementName VARCHAR(75),
    ElementScore NUMERIC(4,2),
    SharedRatingApplyInd BIT,
    SubElementId INT,
    SubElementTypeId INT,
    SubElementQuestion NVARCHAR(500),
    SubElementParentId INT,
    SubElementWeight NUMERIC(4,2),
    SubElementIndicatorLevelId INT,
    SubElementIndicatorLevelName VARCHAR(50),
    SortOrder INT,
    AssessmentId INT,
    AssessmentDt DATETIME,
    SubElementAnswerId INT,
    SubElementAnswerAnswer SQL_VARIANT,
    SubElementAnswerComment NVARCHAR(2000)
)
AS
BEGIN
    DECLARE 
        @SubElementQuestionSetId INT,
        @SubElementQuestionSetIdGeneral INT = 1,
        @SubElementQuestionSetIdYesNo INT = 2,
        @Version INT;
    
    SELECT @Version = e.Version
    FROM Sustain.Element e
    WHERE e.Id = @ElementId

    SELECT @SubElementQuestionSetId = 
        CASE WHEN @Version = 1 THEN COALESCE(SubElementQuestionSetId, @SubElementQuestionSetIdYesNo) ELSE @SubElementQuestionSetIdYesNo END
    FROM Sustain.Assessment
    WHERE Id = @AssessmentId;

    IF @SubElementQuestionSetId IS NULL
        SELECT @SubElementQuestionSetId = 
            CASE WHEN @Version = 1 THEN MAX(SubElementQuestionSetId) ELSE @SubElementQuestionSetIdYesNo END
        FROM Sustain.Assessment
        WHERE EntityTypeId = @EntityTypeId
        AND RefId = @RefId;

    IF @SubElementQuestionSetId IS NULL
        SET @SubElementQuestionSetId = @SubElementQuestionSetIdYesNo;
        
    INSERT @SubElementAndAnswerTable (
        ElementId,
        ElementName,
        ElementScore,
        SharedRatingApplyInd,
        SubElementId,
        SubElementTypeId,
        SubElementQuestion,
        SubElementParentId,
        SubElementWeight,
        SubElementIndicatorLevelId,
        SubElementIndicatorLevelName,
        SortOrder,
        AssessmentId,
        AssessmentDt,
        SubElementAnswerId,
        SubElementAnswerAnswer,
        SubElementAnswerComment
    )
    SELECT 
        e.Id ElementId, e.Name ElementName, e.Score ElementScore, e.SharedRatingApplyInd,
        se.Id SubElementId, se.SubElementTypeId, se.Question, se.ParentId, 
        se.Weight, se.IndicatorLevelId, il.Name,
        se.SortOrder, 
        a.Id AssessmentId, a.AssessmentDt,
        sea.Id SubElementAnswerId, sea.Answer, sea.Comment
    FROM Sustain.Element e
    INNER JOIN Sustain.SubElement se ON se.ElementId = e.Id
        AND se.ActiveInd = 1
    INNER JOIN Sustain.SubElementAnswer sea ON sea.SubElementId = se.Id 
    INNER JOIN Sustain.Assessment a ON a.Id = sea.AssessmentId
        AND a.ElementId = e.Id
    LEFT JOIN Sustain.IndicatorLevel il ON il.Id = se.IndicatorLevelId
    WHERE a.Id = @AssessmentId
    AND e.Id = @ElementId
    AND e.Version = @Version
    AND se.SubElementQuestionSetId IN (@SubElementQuestionSetIdGeneral, @SubElementQuestionSetId)
    
    UNION

    SELECT 
        e.Id ElementId, e.Name ElementName, e.Score ElementScore, e.SharedRatingApplyInd,
        se.Id SubElementId, se.SubElementTypeId, se.Question, se.ParentId, 
        se.Weight, se.IndicatorLevelId, il.Name,
        se.SortOrder, 
        -1, '1/1/1900',
        -1, NULL, NULL 
    FROM Sustain.Element e
    INNER JOIN Sustain.SubElement se ON se.ElementId = e.Id
        AND se.ActiveInd = 1
    LEFT JOIN Sustain.IndicatorLevel il ON il.Id = se.IndicatorLevelId
    WHERE se.Id NOT IN (
        SELECT SubElementId
        FROM Sustain.SubElementAnswer sea
        INNER JOIN Sustain.Assessment a ON a.Id = sea.AssessmentId
            AND a.ElementId = se.ElementId
        WHERE a.Id = @AssessmentId
        AND se.ElementId = @ElementId
        AND se.SubElementQuestionSetId IN (@SubElementQuestionSetIdGeneral, @SubElementQuestionSetId)
    )
    AND e.Id = @ElementId
    AND e.Version = @Version
    AND se.SubElementQuestionSetId IN (@SubElementQuestionSetIdGeneral, @SubElementQuestionSetId)

    ORDER BY se.SortOrder, se.SubElementTypeId;

    RETURN;
END;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetAssessmentElementScores]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     02/15/2013
-- Release:     4.0
-- Description: Returns sustainability element scores
--
-- mm/dd/yy  Name        Release  Description 
-- 06/20/13  Strickland  4.2      Update to account for questions answered "Unknown/NA" instead of excluding them
-- 09/23/13  Strickland  4.4      Update to account for effectiveness only questions
-- 11/11/13  Strickland  4.5      Update to exclude zero weighted questions from being included in "effectiveness level" question calculations
-- 04/14/14  Strickland  5.0      Update grouping of select clause when filling @SubElementScore to include child.SubElementParentId
-- 06/12/14  Strickland  5.1      Update grouping of select clause when filling @SubElementScore to include parent.SubElementId
-- 10/09/17  Strickland  8.3      Add support for 2017 sustainability criteria
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetAssessmentElementScores](
	@AssessmentId INT,
	@ElementId INT
)
-- The return table should match the user-defined table Sustain.AssessmentElementSummaryTable
RETURNS @AssessmentElementSummaryTable TABLE (
	AssessmentId INT,
	ElementId INT,
	AverageRating NUMERIC(4,2),
	AverageAdjustedScore NUMERIC(4,2),
	WeightedAverageScore NUMERIC(4,2),
	SharedRatingApplyInd BIT,
	CostSharingRatingValue NUMERIC(4,2),
	FinalElementScore NUMERIC(4,2),
    AcceptabilityStatus VARCHAR(50)
)
AS
BEGIN
	DECLARE
		@UnansweredQuestionCount INT,
		@SubElementAndAnswerTable Sustain.SubElementAndAnswerTable,
		@SubElementQuestionSetId INT,
		@SubElementQuestionSetIdYesNo INT = 2,
		@SubElementTypeIdEffectiveness INT = Sustain.fnGetSubElementTypeIdEffectiveness(),
		@EntityTypeId INT,
		@RefId INT;
	
	SELECT 
		@EntityTypeId = EntityTypeId,
		@RefId = RefId
	FROM Sustain.Assessment
	WHERE Id = @AssessmentId
	AND ElementId = @ElementId;

	-- Put current answers in local table
	INSERT @SubElementAndAnswerTable (
		ElementId,
		ElementName,
		ElementScore,
		SharedRatingApplyInd,
		SubElementId,
		SubElementTypeId,
		SubElementQuestion,
		SubElementParentId,
		SubElementWeight,
		SortOrder,
		AssessmentId,
		SubElementAnswerId,
		SubElementAnswerAnswer
	)
	SELECT
		ElementId,
		ElementName,
		ElementScore,
		SharedRatingApplyInd,
		SubElementId,
		SubElementTypeId,
		SubElementQuestion,
		SubElementParentId,
		SubElementWeight,
		SortOrder,
		AssessmentId,
		SubElementAnswerId,
		SubElementAnswerAnswer
	FROM Sustain.fnGetSubElementsAndAnswers(@AssessmentId, @ElementId, @EntityTypeId, @RefId)
	WHERE SubElementAnswerAnswer NOT IN ('na', '-1');

	-- Check for unanswered questions
	SELECT @UnansweredQuestionCount = COUNT(*)
	FROM @SubElementAndAnswerTable parent
	LEFT JOIN @SubElementAndAnswerTable child ON child.SubElementParentId = parent.SubElementId
		AND parent.SubElementAnswerAnswer = 'yes'
	WHERE 
		-- Unanswered parent question
		(parent.SubElementParentId IS NULL
		AND parent.SubElementAnswerAnswer IS NULL)
		-- Unanswered child question
		OR (child.SubElementId IS NOT NULL
		AND child.SubElementAnswerAnswer IS NULL);

	IF @UnansweredQuestionCount = 0
		BEGIN
			DECLARE @SubElementScore TABLE (
				ElementId INT, 
				SharedRatingApplyInd BIT, 
				CostSharingRatingValue NUMERIC(4,2), 
				SubElementScore NUMERIC(4,2),
				AverageEffectiveness NUMERIC(4,2),
				SubElementWeight NUMERIC(4,2)
			);

			-- Get the sub-element question set
			SELECT @SubElementQuestionSetId = SubElementQuestionSetId
			FROM Sustain.Assessment
			WHERE Id = @AssessmentId;

			IF @SubElementQuestionSetId = @SubElementQuestionSetIdYesNo
				-- Get scores for original "Yes/No" questions
				INSERT @SubElementScore (
					ElementId, 
					SharedRatingApplyInd, 
					CostSharingRatingValue, 
					SubElementScore,
					AverageEffectiveness,
					SubElementWeight
				)
				SELECT
					parent.ElementId, 
					parent.SharedRatingApplyInd, 
					csr.Value CostSharingRatingValue, 
					CASE WHEN parent.SubElementAnswerAnswer = 'yes' THEN parent.ElementScore ELSE 0 END SubElementScore,
					AVG(CAST(CASE WHEN COALESCE(child.SubElementTypeId, @SubElementTypeIdEffectiveness) = @SubElementTypeIdEffectiveness THEN COALESCE(child.SubElementAnswerAnswer, 1) ELSE 1 END AS NUMERIC(4,2))) AverageEffectiveness,
					parent.SubElementWeight
				FROM @SubElementAndAnswerTable parent
				INNER JOIN Sustain.Assessment assessment ON assessment.Id = parent.AssessmentId
				LEFT JOIN Sustain.CostSharingRating csr ON csr.Id = assessment.CostSharingRatingId
				LEFT JOIN @SubElementAndAnswerTable child ON child.SubElementParentId = parent.SubElementId
					AND parent.SubElementAnswerAnswer = 'yes'
				WHERE parent.SubElementParentId IS NULL
				AND parent.ElementScore > 0
				GROUP BY 
					parent.SubElementId, -- 6/12/2014, 5.1, Strickland
					parent.ElementId, 
					parent.ElementScore, 
					parent.SharedRatingApplyInd, 
					child.SubElementParentId, -- 4/15/2014, 5.0, Strickland
					csr.Value, 
					parent.SubElementWeight, 
					parent.SubElementAnswerAnswer;
			ELSE
				-- Get scores for secondary "Effectiveness" questions
				INSERT @SubElementScore (
					ElementId, 
					SharedRatingApplyInd, 
					CostSharingRatingValue, 
					SubElementScore,
					AverageEffectiveness,
					SubElementWeight
				)
				SELECT
					parent.ElementId, 
					parent.SharedRatingApplyInd, 
					csr.Value CostSharingRatingValue, 
					parent.ElementScore SubElementScore,
					AVG(CAST(COALESCE(parent.SubElementAnswerAnswer, '0') AS NUMERIC(4,2))) AverageEffectiveness,
					parent.SubElementWeight
				FROM @SubElementAndAnswerTable parent
				INNER JOIN Sustain.Assessment assessment ON assessment.Id = parent.AssessmentId
				LEFT JOIN Sustain.CostSharingRating csr ON csr.Id = assessment.CostSharingRatingId
				WHERE parent.SubElementParentId IS NULL
				AND parent.ElementScore > 0
				AND parent.SubElementWeight > 0
				GROUP BY 
					parent.SubElementId, -- 6/12/2014, 5.1, Strickland
					parent.ElementId, 
					parent.ElementScore, 
					parent.SharedRatingApplyInd, 
					csr.Value, 
					parent.SubElementWeight, 
					parent.SubElementAnswerAnswer;

			-- Get the average scores
			INSERT @AssessmentElementSummaryTable (
				AssessmentId,
				ElementId,
				AverageRating,
				AverageAdjustedScore,
				WeightedAverageScore,
				SharedRatingApplyInd,
				CostSharingRatingValue,
				FinalElementScore
			)
			SELECT 
				@AssessmentId AssessmentId,
				@ElementId ElementId,
				AverageRating,
				AverageAdjustedScore,
				WeightedAverageScore,
				SharedRatingApplyInd,
				COALESCE(CostSharingRatingValue, 0) CostSharingRatingValue,
				CASE WHEN SharedRatingApplyInd = 1 THEN
					(WeightedAverageScore + CostSharingRatingValue) / 2
				ELSE
					WeightedAverageScore
				END FinalElementScore
			FROM (
				-- Get average scores
				SELECT 
					CASE WHEN @SubElementQuestionSetId = @SubElementQuestionSetIdYesNo THEN SUM(SubElementScore) / COUNT(*) ELSE 0 END AverageRating,
					SUM(SubElementScore * AverageEffectiveness) / COUNT(*) AverageAdjustedScore,
					CASE WHEN SUM(SubElementWeight) = 0 THEN
						0
					ELSE
						SUM(SubElementScore * AverageEffectiveness * SubElementWeight) / SUM(SubElementWeight) 
					END WeightedAverageScore,
					SharedRatingApplyInd,
					CostSharingRatingValue
				FROM @SubElementScore
				GROUP BY SharedRatingApplyInd, CostSharingRatingValue
			) AverageScores;
		END;
	ELSE
		-- Unanswered questions exist so scores can't be calculated
		INSERT @AssessmentElementSummaryTable (
			AssessmentId,
			ElementId,
			AverageRating,
			AverageAdjustedScore,
			WeightedAverageScore,
			SharedRatingApplyInd,
			CostSharingRatingValue,
			FinalElementScore
		)
		SELECT 
			@AssessmentId AssessmentId,
			@ElementId ElementId,
			-1 AverageRating,
			-1 AverageAdjustedScore,
			-1 WeightedAverageScore,
			CAST(0 AS BIT) SharedRatingApplyInd,
			-1 CostSharingRatingValue,
			-1 FinalElementScore;
		
	RETURN;
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnGetBaselineGroupId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/10/2012
-- Release:     3.3
-- Description: Retrieves the Baseline ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Baseline].[fnGetBaselineGroupId]()
RETURNS int
AS
BEGIN
	
	RETURN 10;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsScopeAssignedToSite]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     11/07/2012
-- Description: Returns 1 if scope assigned for the site,
--              else returns 0 if no scope is assigned.
--
-- mm/dd/yy  Name        Release  Description
-- 10/10/14  TYH         5.3      Implemented support for WorkBreakdownStructure
-- 03/23/16  Strickland  7.0      Updated to ignore GTRI scope
-- 04/03/17  Strickland  8.0      Use functions instead of literal values for variables
--                                Added NOEXPAND hint for joined view
-- =============================================================
CREATE FUNCTION [Inventory].[fnIsScopeAssignedToSite] (
    @siteId INT
)
RETURNS BIT
AS
BEGIN
    DECLARE 
        @scopeAssigned BIT,
        @statusIdDeleted INT = G2.fnGetDeletedWorkBreakdownStructureStatusId(),
        @statusIdCancelled INT = G2.fnGetCancelledWorkBreakdownStructureStatusId(),
        @subSystemIdNA21GTRI INT = G2.fnGetNA21SubSystemId();

    -- Check to see if the ID is in the TaskSite table
    SELECT @scopeAssigned = CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
    FROM G2.TaskSite ts
         INNER JOIN G2.Task t ON t.Id = ts.TaskId
         INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsTask.Id = t.WorkBreakdownStructureId
   WHERE ts.SiteId = @siteId
         AND wbsTask.StatusId <> @statusIdDeleted 
         AND wbsTask.StatusId <> @statusIdCancelled 
         AND wbsTask.SubSystemId <> @subSystemIdNA21GTRI;

    IF @scopeAssigned = 0 
    BEGIN
        -- Check to see if the ID is in the TaskChange table with an active status
        SELECT @scopeAssigned = CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
        FROM Baseline.TaskChange tr /*G2.TaskRequest tr*/
             INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = tr.ChangeRequestDetailId
             INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH ( NOEXPAND ) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
       WHERE tr.SiteId = @siteId
             AND tr.DeletedInd = 0;  
    END;

    RETURN @scopeAssigned;
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnCheckActivityWorkBreakdownStructureXrefSubsystemMatchesWorkBreakdownStructureSubsystem]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     08/15/2018
-- Release:     9.2
-- Description: The subsystem associated with ActivityWorkBreakdownStructureXref must match the subsystem
--              associated with the WorkBreakdownStructure
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnCheckActivityWorkBreakdownStructureXrefSubsystemMatchesWorkBreakdownStructureSubsystem]
(   
	@ActivitySubSystemXrefId INT,
	@WorkBreakdownStructureId INT 
)
RETURNS BIT 
AS 
BEGIN 

   DECLARE	@isValidInd BIT = 0, 
			@subSystemId INT = G2.fnGetSubSystemIdFromWorkBreakdownStructure(@WorkBreakdownStructureId);


   SELECT @isValidInd = 1
   FROM Activity.ActivitySubSystemXref assx
   WHERE assx.Id = @ActivitySubSystemXrefId
   AND assx.SubSystemId = @subSystemId;
	
   RETURN @isValidInd;

END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSite]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Site.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSite] ()
RETURNS int
AS
BEGIN
	RETURN 6; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdCountry]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Country.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdCountry] ()
RETURNS int
AS
BEGIN
	RETURN 3; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSustainabilityVersion]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/11/2017
-- Release:     8.3
-- Description: Returns sustainability version for a country or site
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSustainabilityVersion] (
	@EntityTypeId INT,
	@RefId INT
)
RETURNS INT

BEGIN

	DECLARE
		@EntityTypeIdCountry INT = Inventory.fnGetEntityTypeIdCountry(),
		@EntityTypeIdSite INT = Inventory.fnGetEntityTypeIdSite(),
        @SubSystemIdNA21GMS INT = G2.fnGetNA21GMSSubSystemId(),
        @SPA2017CriteriaStartDateParmKey VARCHAR(50) = G2.fnGetSPA2017CriteriaStartDateParmKey(),
        @SPA2017CriteriaStartDate DATETIME,
        @SustainabilityVersion INT;

    SELECT @SPA2017CriteriaStartDate = CAST(gp.Value AS DATETIME)
    FROM G2.G2Parameters gp
    WHERE gp.ParmKey = @SPA2017CriteriaStartDateParmKey
    AND gp.SubSystemId = @SubSystemIdNA21GMS;

    -- Sustainability version for a site
    SELECT @SustainabilityVersion = CASE WHEN @SPA2017CriteriaStartDate > COALESCE(smg.CreatedDt, '1/1/1900') THEN 1 ELSE 2 END
    FROM SPA.SPAMain sm
    INNER JOIN SPA.SPAMainGeneral smg ON smg.SPAMainId = sm.Id AND smg.Revision = sm.Revision
    WHERE @EntityTypeId = @EntityTypeIdSite
    AND sm.SiteId = @RefId;
    
    -- Sustainability version for a country
    SELECT @SustainabilityVersion = CASE WHEN ca.RevisionStartDt IS NULL THEN 1 ELSE 2 END
    FROM SPA.CountryAssessment ca
    WHERE @EntityTypeId = @EntityTypeIdCountry
    AND ca.CountryId = @RefId;

    RETURN @SustainabilityVersion;
END;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetImportedBatchStatusId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/05/2014 
-- Release:     5.0
-- Description: Returns the batch status Id for the Imported batch status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetImportedBatchStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Imported
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetYearEndForecastChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     01/22/2016
-- Release:     6.5
-- Description: Returns the Year End Forecast change request type Id
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetYearEndForecastChangeRequestTypeId]  ()
RETURNS INT
AS
BEGIN
  RETURN 35; --Year End Forecast
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetYearEndForecastDueNotificationId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:		02/18/16
-- Release:     6.5
-- Description: Returns the id for the CostOverrun notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetYearEndForecastDueNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 21; -- Year End Forecast Due
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetMaxWorkflowLevel]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     5/10/17
-- Release:     8.0
-- Description: Return the maximum workflow level
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetMaxWorkflowLevel]
(
)
RETURNS INT
AS
BEGIN
	RETURN 3;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSCommunicationsAndAnswers]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/03/2012
-- Release:     3.7
-- Description: Returns RMS communications and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSCommunicationsAndAnswers](
    @RMSId INT
)
RETURNS @RMSCommunicationAndAnswerTable TABLE (
    RMSId INT,
    RMSCommunicationId INT,
    RMSCommunicationDescription NVARCHAR(500),
    RMSCommunicationSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSCommunicationAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSCommunicationId, F.[Description] RMSCommunicationDescription, F.SortOrder RMSCommunicationSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSCommunicationXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSCommunication F ON F.Id = X.RMSCommunicationId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSCommunicationId, F.[Description] RMSCommunicationDescription, F.SortOrder RMSCommunicationSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSCommunication F 
    WHERE F.Id NOT IN (
        SELECT X.RMSCommunicationId
        FROM SPA.RMSRMSCommunicationXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetNewAccountLineItemStatusId]'
GO

-- =============================================================
-- Author:      Chris O'Neal
-- Created:     4/27/2013
-- Release:     4.1
-- Description: Retrieves the New Account line item status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetNewAccountLineItemStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- New Account
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [STARS].[fnCheckOnlyOneActiveCFOIdPerReportingEntityAndSubSystem]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     3/24/2017
-- Release:     8.0
-- Description: Enforces that we only have one current ChiefFinanceOfficeReportingEntity 
--				per ReportingEntity per SubSystem.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [STARS].[fnCheckOnlyOneActiveCFOIdPerReportingEntityAndSubSystem]
(
	@SubSystemId INT,
	@ReportingEntityOrganizationPartyId INT,
	@CurrentInd BIT
)
RETURNS INT
AS 
BEGIN
	DECLARE @valid INT = 1;

	SELECT @valid = 0
	FROM STARS.ReportingEntityChiefFinanceOfficeReportingEntityXref recforex
	WHERE recforex.SubSystemId = @SubSystemId
	AND recforex.ReportingEntityOrganizationPartyId = @ReportingEntityOrganizationPartyId
	GROUP BY recforex.SubSystemId, recforex.ReportingEntityOrganizationPartyId
	HAVING SUM(CAST(recforex.CurrentInd AS INT)) > 1;

	RETURN @valid;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetPerformerAllotteeRollupPartyRelationshipTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/18/2013
-- Release:     4.6
-- Description: Retrieves the Performer Allottee Rollup party
--   relationship type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetPerformerAllotteeRollupPartyRelationshipTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Performer Allottee Rollup
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNFWAChangeRequestTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the NFWA ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- 03/28/17  b.roland 8.0      Renamed from fnGetNFWATypeId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNFWAChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 17;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLocalAFPChangeRequestTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the Local AFP ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- 03/28/17  b.roland 8.0      Renamed from fnGetLocalAFPTypeId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLocalAFPChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 21;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetHQAFPChangeRequestTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the HQ AFP ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- 03/28/17  b.roland 8.0      Renamed from fnGetHQAFPTypeId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetHQAFPChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetUnobligatedFundingSourceId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/27/2015
-- Release:     6.2
-- Description: Selects Id for Funding Source where the value is for Unobligated
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetUnobligatedFundingSourceId]()
RETURNS INT AS 
BEGIN
	RETURN 2;
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetUncostedFundingSourceId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     10/21/2016
-- Release:     7.3
-- Description: Selects the Uncosted Funding Source Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetUncostedFundingSourceId]()
RETURNS INT AS 
BEGIN
	RETURN 1; -- Uncosted
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetPrimaryFundTypeForSubSystemId]'
GO
-- =============================================================
-- Author:      Ty Hockett
-- Created:     10/08/2015
-- Release:     6.3
-- Description: Returns the explicity defined primary Fund Type for a SubSystem, or derives it 
--              if no Fund Type is explicitly defined
--
-- mm/dd/yy  Name        Release  Description
-- 03/18/16  g.ogle      7.0      Updated to return DisplayName insetad of ShortName
-- =============================================================
CREATE FUNCTION [Financial].[fnGetPrimaryFundTypeForSubSystemId](
	@SubSystemId INT
)
RETURNS
VARCHAR(10)
AS
BEGIN
	DECLARE @primaryFundType VARCHAR(10) = (
        SELECT TOP 1 ft.DisplayName FundType
        FROM Financial.FundType ft 
        INNER JOIN Financial.SubSystemFundType ssft ON ssft.FundTypeId = ft.Id
        WHERE ssft.SubSystemId = @SubSystemId
        AND ssft.PrimaryInd = 1
		--AND ssft.ActiveInd = 1
	);

	IF @primaryFundType IS NULL
	BEGIN
		SELECT @primaryFundType = (
			SELECT TOP 1 ft.DisplayName FundType
			FROM Financial.FundType ft 
			INNER JOIN Financial.SubSystemFundType ssft ON ssft.FundTypeId = ft.Id
			INNER JOIN Financial.Account a ON a.FundTypeId = ft.Id
			INNER JOIN Financial.AccountTransaction at ON at.AccountId = a.Id
			WHERE ssft.SubSystemId = @SubSystemId
			--AND ssft.ActiveInd = 1
			GROUP BY ft.DisplayName
			ORDER BY COUNT(*) DESC
		);
	END

	RETURN @primaryFundType;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetLessonsLearnedResponseCommentTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/04/16
-- Release:     7.2
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetLessonsLearnedResponseCommentTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 2;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetExplanationResponseCommentTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/04/16
-- Release:     7.2
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetExplanationResponseCommentTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 3;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetBestPracticeResponseCommentTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/04/16
-- Release:     7.2
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetBestPracticeResponseCommentTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnGetSiteRankTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     9/16/2014
-- Release:     6.4
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [BudgetFormulation].[fnGetSiteRankTypeId]()
RETURNS INT
AS
BEGIN
    RETURN 2;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnCheckRequestRankGroupHasOnlyOneSiteRankGroup]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     2/17/2016
-- Release:     6.5
-- Description: Enforces that we only have one site RequestRankGroup 
--				per PlanningConfiguration.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [BudgetFormulation].[fnCheckRequestRankGroupHasOnlyOneSiteRankGroup]
(
	@PlanningConfigurationId int,
	@RankTypeId int
)
RETURNS INT
AS 
BEGIN
	DECLARE @siteRankTypeId INT = BudgetFormulation.fnGetSiteRankTypeId(),
		@valid INT = 1;

	SELECT @valid = 0
	FROM BudgetFormulation.RequestRankGroup rrg
	WHERE rrg.PlanningConfigurationId = @PlanningConfigurationId
	AND rrg.RankTypeId = @RankTypeId
	AND rrg.RankTypeId = @siteRankTypeId
	GROUP BY rrg.Id
	HAVING COUNT(*) > 1;

	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnGetHQRankTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     9/16/2014
-- Release:     6.4
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [BudgetFormulation].[fnGetHQRankTypeId]()
RETURNS INT
AS
BEGIN
    RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetUseRMSMatrixTable]'
GO
-- =============================================
-- Author:      Mike Strickland
-- Create date: 05/27/2014
-- Release:     5.1
-- Description: Returns SPA.SPAMainId and whether
--              that SPA should use the RMS Matrix or not.
--
-- mm/dd/yyyy   Name        Release  Description 
-- 01/30/2015   Strickland  5.4      Changed "OR sm.SPAStatusId = @SPAStatusIdInProgress" to "OR (FirstSubmitDates.FirstSubmitDate IS NULL AND sm.SPAStatusId = @SPAStatusIdInProgress)" to eliminate false positives
-- 08/05/2015   Strickland  6.3      Changed "OR (FirstSubmitDates.FirstSubmitDate IS NULL AND sm.SPAStatusId = @SPAStatusIdInProgress)" to "OR (COALESCE(FirstSubmitDates.FirstSubmitDate, @MaxDate) = @MaxDate AND sm.SPAStatusId = @SPAStatusIdInProgress)"
-- 08/07/2015   Strickland  6.3      Update for SPA status changes
-- 03/24/2016   Strickland  7.0      Fixed bug for determining FirstSubmitDate
-- =============================================
CREATE FUNCTION [SPA].[fnGetUseRMSMatrixTable] ()
RETURNS @SPAMainIdUseRMSMatrixInd TABLE (
    SPAMainId INT,
    UseRMSMatrixInd BIT
)
AS
BEGIN

    DECLARE
        @SPAStatusIdInProgress INT = SPA.fnGetSPAStatusIdInProgress(),
        @SPAStatusIdSubmittedToSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
        @SPAStatusIdSubmittedToSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
        @SPAStatusIdSubmittedToSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
        @SPAStatusIdSubmittedToSPASubProgramManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPASubProgramManager(),
        @SPAStatusIdSubmittedToSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead(),
        @SPAStatusIdRejectedBySPATeamLead INT = SPA.fnGetSPAStatusIdRejectedbySPATeamLead(),
        @SPAStatusIdRejectedBySPAPortfolioManager INT = SPA.fnGetSPAStatusIdRejectedbySPAPortfolioManager(),
        @SPAStatusIdRejectedBySPASubProgramManager INT = SPA.fnGetSPAStatusIdRejectedbySPASubProgramManager(),
        @SPAStatusIdRejectedBySPALabLead INT = SPA.fnGetSPAStatusIdRejectedbySPALabLead(),
        @SPAStatusIdRejectedBySPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead(),
        @SPAStatusIdApproved INT = SPA.fnGetSPAStatusIdApproved(),

        @RMSMatrixStartDate DATETIME,
        @RMSMatrixStartDateDefault DATETIME = '6/26/2014',
        @MaxDate DATETIME = '12/31/9999',
        @MinDate DATETIME = '1/1/1900';


    SELECT @RMSMatrixStartDate = CONVERT(DATETIME, COALESCE(Value, @RMSMatrixStartDateDefault))
    FROM G2.G2Parameters
    WHERE ParmKey = 'RMS Matrix Start Date';

    INSERT @SPAMainIdUseRMSMatrixInd (
        SPAMainId,
        UseRMSMatrixInd
    )
    SELECT 
        sm.Id, 
        CASE WHEN 
            DATEDIFF(D, @RMSMatrixStartDate, COALESCE(FirstSubmitDates.FirstSubmitDate, @MaxDate)) > 0 
            OR DATEDIFF(D, @RMSMatrixStartDate, COALESCE(LastRejectDates.LastRejectDate, @MinDate)) > 0 
            OR (COALESCE(FirstSubmitDates.FirstSubmitDate, @MaxDate) = @MaxDate AND sm.SPAStatusId = @SPAStatusIdInProgress)
        THEN
            1
        ELSE
            0
        END UseRMSMatrixInd
    FROM SPA.SPAMain sm
    LEFT JOIN (
        SELECT sm.Id, sm.Revision, COALESCE(MIN(sm.LastModifiedDt), @MaxDate) FirstSubmitDate
        FROM SnapShot.SPAMain sm
        LEFT JOIN (
            SELECT Id, Revision, COALESCE(MAX(LastModifiedDt), @MinDate) MaxApprovedDate
            FROM SnapShot.SPAMain
            WHERE SPAStatusId = @SPAStatusIdApproved
            GROUP BY Id, Revision
        ) PreviousRevisionMaxApprovedDate ON PreviousRevisionMaxApprovedDate.Id = sm.Id
            AND PreviousRevisionMaxApprovedDate.Revision = sm.Revision - 1
        WHERE sm.SPAStatusId IN (
            @SPAStatusIdSubmittedToSPATeamLead, 
            @SPAStatusIdSubmittedToSPAPortfolioManager, 
            @SPAStatusIdSubmittedToSPAProtectTechnicalLead, 
            @SPAStatusIdSubmittedToSPASubProgramManager, 
            @SPAStatusIdSubmittedToSPALabLead,
            @SPAStatusIdApproved
        )
        AND sm.LastModifiedDt > COALESCE(PreviousRevisionMaxApprovedDate.MaxApprovedDate, @MinDate)
        GROUP BY sm.Id, sm.Revision
    ) FirstSubmitDates ON FirstSubmitDates.Id = sm.Id
        AND FirstSubmitDates.Revision = sm.Revision
    LEFT JOIN (
        SELECT Id, Revision, COALESCE(MAX(LastModifiedDt), @MinDate) LastRejectDate
        FROM SnapShot.SPAMain 
        WHERE SPAStatusId IN (
            @SPAStatusIdRejectedBySPATeamLead,
            @SPAStatusIdRejectedBySPAPortfolioManager,
            @SPAStatusIdRejectedBySPASubProgramManager,
            @SPAStatusIdRejectedBySPALabLead,
            @SPAStatusIdRejectedBySPAProtectTechnicalLead
        )
        GROUP BY Id, Revision
    ) LastRejectDates ON LastRejectDates.Id = sm.Id
        AND LastRejectDates.Revision = sm.Revision;
    
    RETURN; 

END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdRoomInformation]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Room Information.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdRoomInformation] ()
RETURNS int
AS
BEGIN
	RETURN 8; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdRMSMonitoringAndImplementation]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of RMS Monitoring & Implementation.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdRMSMonitoringAndImplementation] ()
RETURNS int
AS
BEGIN
	RETURN 21; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsSiteSGIM]'
GO
-- =============================================  
-- Author:      Mike Strickland
-- Created:     12/14/2009
-- Release:     4.2
-- Description: Returns 1 if site is SGI-M
--	
-- mm/dd/yyyy   Name        Release  Description 
-- =============================================  
CREATE FUNCTION [Inventory].[fnIsSiteSGIM] (
	@SiteId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE 
		@IsSiteSGIM BIT,
		@BuildingTypeIdResearchReactor INT = 4,
		@BuildingTagIdSGIM INT = 5,
		@BuildingTagIdNPP INT = 6

	SELECT @IsSiteSGIM = 
		CASE WHEN COUNT(*) > 0 THEN
			1
		ELSE 
			0
		END
	FROM G2.Building b
	LEFT JOIN G2.BuildingBuildingTagXref x ON x.BuildingId = b.Id
	WHERE b.SiteId = @SiteId
	AND (
		(b.BuildingTypeId = @BuildingTypeIdResearchReactor and b.CountryId = 1)
		OR COALESCE(x.BuildingTagId, -1) IN (@BuildingTagIdSGIM, @BuildingTagIdNPP)
	)

	-- Return 
	RETURN @IsSiteSGIM
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSiteSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Site Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSiteSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 4; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdRoomSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Room Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdRoomSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 10; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdResponseSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Response Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdResponseSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 16; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdBuildingSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Building Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdBuildingSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 7; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetPreviousSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Create:      04/24/2015
-- Release:     6.0
-- Description: Returns the previous security upgrade answers.
--              Pass in -1 for EntityTypeId and RefId in @UpdateTable
--              to get previous answers for all entities
--              in a SPA.
--
-- mm/dd/yy  Name        Release  Description
-- 03/29/17  Strickland  8.0      Updated to include multiple previous upgrades
-- =============================================================
CREATE FUNCTION [SPA].[fnGetPreviousSecurityUpgrades] (
    @UpdateTable SPA.SPAMainSectionUpdateTable READONLY
)
RETURNS @PreviousSecurityUpgrades TABLE (
       SPAMainId INT,
       Revision INT,
       EntityTypeId INT,
       RefId INT,
       SecurityActionId INT,
       SecurityUpgradeOptionId INT,
       Upgrades VARCHAR(2000),
       ExtraInfo VARCHAR(100)
)
AS
BEGIN
    ---- debug
    --DECLARE
    --    @UpdateTable SPA.SPAMainSectionUpdateTable,
    --    @SPAMainId INT = (SELECT sm.Id FROM SPA.SPAMain sm WHERE sm.SiteId = 8157),
    --    @Revision INT = 2,
    --    @EntityTypeIdResponse INT = Inventory.fnGetEntityTypeIdPrimaryResponseForce(),
    --    @RefId INT = 8157;
    --DECLARE @PreviousSecurityUpgrades TABLE (
    --   SPAMainId INT,
    --   Revision INT,
    --   EntityTypeId INT,
    --   RefId INT,
    --   SecurityActionId INT,
    --   SecurityUpgradeOptionId INT,
    --   Upgrades VARCHAR(2000),
    --   ExtraInfo VARCHAR(100)
    --);

    --INSERT @UpdateTable
    --        (SPAMainId,
    --         Revision,
    --         EntityTypeId,
    --         RefId
    --        )
    --VALUES  (@SPAMainId,
    --         @Revision,
    --         @EntityTypeIdResponse,
    --         @RefId
    --        );
    ---- /debug

    -- Get previous answers for security actions
    INSERT @PreviousSecurityUpgrades (SPAMainId, EntityTypeId, RefId, SecurityActionId, Revision, SecurityUpgradeOptionId, Upgrades, ExtraInfo)
    SELECT 
        su.SPAMainId, su.EntityTypeId, su.RefId, su.SecurityActionId, MAX(su.Revision) PreviousRevision, 
        susuo.SecurityUpgradeOptionId, su.Upgrades, su.ExtraInfo
    FROM SPA.SecurityUpgrade su
    INNER JOIN SPA.SecurityUpgradeSecurityUpgradeOption susuo 
        ON susuo.SecurityUpgradeId = su.Id
    INNER JOIN @UpdateTable ut
        ON su.SPAMainId = ut.SPAMainId
        AND su.EntityTypeId = CASE WHEN ut.EntityTypeId > 0 THEN ut.EntityTypeId ELSE su.EntityTypeId END
        AND su.RefId = CASE WHEN ut.RefId > 0 THEN ut.RefId ELSE su.RefId END
        AND su.Revision < ut.Revision
    INNER JOIN SPA.SecurityAction sa
        ON sa.Id = su.SecurityActionId
        AND sa.ActiveInd = 1 -- Only include active security actions
    GROUP BY su.SPAMainId, su.EntityTypeId, su.RefId, su.SecurityActionId, susuo.SecurityUpgradeOptionId, su.Upgrades, su.ExtraInfo
    ORDER BY su.SPAMainId, su.EntityTypeId, su.RefId, su.SecurityActionId;
    
    ---- debug
    --SELECT 
    --    sa.Id, sa.Description,
    --    ut.* 
    --FROM @PreviousSecurityUpgrades ut
    --INNER JOIN SPA.SecurityAction sa ON sa.Id = ut.SecurityActionId
    ----WHERE sa.Id = 61;
    ---- /debug

    RETURN; 

END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdRoom]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Room.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdRoom] ()
RETURNS int
AS
BEGIN
	RETURN 8; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdPrimaryResponseForce]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for PrimaryResponseForce.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdPrimaryResponseForce] ()
RETURNS int
AS
BEGIN
	RETURN 12; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdBuilding]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Building.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdBuilding] ()
RETURNS int
AS
BEGIN
	RETURN 7; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/09/2009
-- Description:	Returns the Id of the
--				passed Name
--
-- mm/dd/yyyy   Name        Release  Description 
-- 05/22/2015   Strickland  6.0      Updated to use CASE statement rather than hitting the table, Inventory.EntityType
-- =============================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeId] (
	@EntityTypeIdCode VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @EntityTypeId INT

    SELECT @EntityTypeId = 
	    CASE LOWER(@EntityTypeIdCode)
            WHEN 'program' THEN 1
            WHEN 'office' THEN 2
            WHEN 'country' THEN 3
            WHEN 'region' THEN 4
            WHEN 'state' THEN 5
            WHEN 'site' THEN 6
            WHEN 'building' THEN 7
            WHEN 'room' THEN 8
            WHEN 'source' THEN 9
            WHEN 'fileupload' THEN 10
            WHEN 'spamain' THEN 11
            WHEN 'primaryresponseforce' THEN 12
            WHEN 'secondaryresponseforce' THEN 13
            WHEN 'tertiaryresponseforce' THEN 14
            WHEN 'spaimport' THEN 15
            WHEN 'lea' THEN 16
            WHEN 'rms' THEN 17
            ELSE -1
        END

	RETURN COALESCE(@EntityTypeId, -1)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityMaterialTypeCountTable]'
GO

-- =============================================
-- Author:      Mike Strickland
-- Create date: 07/04/2012
-- Release:     3.4
-- Description:    Returns material type (radiological or nuclear) counts
--              for the entity (site, building, or room).
--                
-- mm/dd/yy  Name        Release  Description 
-- 06/25/13  Strickland  4.2      Update to accounty for response security upgrades
-- 04/01/16  Strickland  7.0      Update to improve performance
-- =============================================
CREATE FUNCTION [Inventory].[fnGetEntityMaterialTypeCountTable](
    @EntityTypeId INT,
    @RefId INT
)
RETURNS 
@MaterialTypeCountTable TABLE (
    EntityTypeId INT,
    RefId INT,
    MaterialTypeCountRad INT,
    MaterialTypeCountNuc INT
)
AS
BEGIN
    DECLARE 
        @EntityTypeIdSite INT = Inventory.fnGetEntityTypeId('Site'),
        @EntityTypeIdPrimaryResponseForce INT = Inventory.fnGetEntityTypeId('PrimaryResponseForce'),
        @EntityTypeIdBuilding INT = Inventory.fnGetEntityTypeId('Building'),
        @EntityTypeIdRoom INT = Inventory.fnGetEntityTypeId('Room'),
        @False BIT = 0,
        @MaterialTypeIdRad INT = 1,
        @MaterialTypeIdNuc INT = 2,
        @OSRPStatusIdRecovered INT = 2;


    INSERT @MaterialTypeCountTable (
        EntityTypeId,
        RefId,
        MaterialTypeCountRad,
        MaterialTypeCountNuc
    )
    SELECT 
        @EntityTypeId EntityTypeId,
        CASE @EntityTypeId
            WHEN @EntityTypeIdSite THEN SiteId 
            WHEN @EntityTypeIdPrimaryResponseForce THEN SiteId 
            WHEN @EntityTypeIdBuilding THEN BuildingId
            ELSE RoomId
        END,
        SUM(CASE WHEN MaterialTypeId = @MaterialTypeIdRad THEN 1 ELSE 0 END) MaterialTypeCountRad,
        SUM(CASE WHEN MaterialTypeId = @MaterialTypeIdNuc THEN 1 ELSE 0 END) MaterialTypeCountNuc
    FROM Inventory.vwMaterialAtSite 
    WHERE SiteId = CASE WHEN @EntityTypeId IN (@EntityTypeIdSite, @EntityTypeIdPrimaryResponseForce) THEN @RefId ELSE SiteId END
    AND BuildingId = CASE WHEN @EntityTypeId = @EntityTypeIdBuilding THEN @RefId ELSE BuildingId END
    AND RoomId = CASE WHEN @EntityTypeId = @EntityTypeIdRoom THEN @RefId ELSE RoomId END
    AND OSRPStatusId <> @OSRPStatusIdRecovered -- Not OSRP recovered
    AND DispositionInd = @False                -- Not dispositioned
    GROUP BY 
        CASE @EntityTypeId
            WHEN @EntityTypeIdSite THEN SiteId 
            WHEN @EntityTypeIdPrimaryResponseForce THEN SiteId 
            WHEN @EntityTypeIdBuilding THEN BuildingId
            ELSE RoomId
        END;

    RETURN;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSFeaturesAndAnswers]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/03/2012
-- Release:     3.7
-- Description: Returns RMS systeam features and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSFeaturesAndAnswers](
    @RMSId INT
)
RETURNS @RMSFeatureAndAnswerTable TABLE (
    RMSId INT,
    RMSFeatureId INT,
    RMSFeatureDescription NVARCHAR(500),
    RMSFeatureSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSFeatureAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSFeatureId, F.[Description] RMSFeatureDescription, F.SortOrder RMSFeatureSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSFeatureXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSFeature F ON F.Id = X.RMSFeatureId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSFeatureId, F.[Description] RMSFeatureDescription, F.SortOrder RMSFeatureSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSFeature F 
    WHERE F.Id NOT IN (
        SELECT X.RMSFeatureID
        FROM SPA.RMSRMSFeatureXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnCheckImageXrefAssetHasOnlyOneDefault]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/28/2016 
-- Release:     7.4
-- Description: Checks that an asset only has one default attachment at a time
--
-- mm/dd/yy  Name      Release  Description
-- 04/26/18  j.borgers 9.0      Rename AssetAttachmentXref to ImageXref
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnCheckImageXrefAssetHasOnlyOneDefault]
(
	@AssetId INT,
	@DefaultInd BIT
)
RETURNS INT
AS
BEGIN
	--DECLARE @AssetId INT = 1;
	
	DECLARE @valid INT = 1;

	SELECT @valid = 0 
	FROM AssetManagement.ImageXref ix
	WHERE ix.AssetId = @AssetId
	AND ix.DefaultInd = 1
	HAVING COUNT(*) > 1;

	--SELECT @valid;
	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnCheckAssetHasOnlyOnePrimaryContact]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     06/22/2017 
-- Release:     8.1
-- Description: Returns 1 if contact can be a primary contact for 
--              an asset
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [MDI].[fnCheckAssetHasOnlyOnePrimaryContact]
(
	@AssetId INT,
	@PrimaryInd INT
)
RETURNS BIT
AS
BEGIN
  
  DECLARE @valid BIT = 1;

  SELECT @valid = 0
  FROM MDI.AssetContact ac
  WHERE ac.AssetId = @AssetId
  AND ac.PrimaryInd = 1
  HAVING COUNT(ac.Id) > 1;

  RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnCheckAssetHasOnlyOnePrimaryAlias]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     06/21/2017 
-- Release:     8.1
-- Description: Returns 1 if the asset has only one primary alias, returns 0 otherwise
--
-- mm/dd/yy  Name     Release  Description
-- 06/22/17  g.hanas  8.1      Updated logic to simplify check. Removed unused 
--                             primaryInd variable.
-- 06/26/17  g.hanas  8.1      Removed unneeded group by statement.
-- =============================================================
CREATE FUNCTION [MDI].[fnCheckAssetHasOnlyOnePrimaryAlias]
(
	@AssetId INT,
	@PrimaryInd INT --NOTE! This has to be passed in because the check constraint will not fire correctly on update
)
RETURNS BIT
AS
BEGIN
  
  DECLARE @valid BIT = 1;


  SELECT @valid = 0
  FROM MDI.AssetAlias aa
  WHERE aa.AssetId = @AssetId
  AND aa.PrimaryInd = 1
  HAVING COUNT(aa.AssetId) > 1;

  RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnCheckSiteDoesNotHaveDuplicateAreaNumbers]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/29/18 
-- Release:     9.1
-- Description: Checks that a site only has at most one of each area number
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnCheckSiteDoesNotHaveDuplicateAreaNumbers]
(
	@SiteLocationId INT,
	@AreaNumber NVARCHAR(3)
)
RETURNS INT
AS
BEGIN
	--DECLARE @SiteLocationId INT = 1,
	--		@AreaNumber NVARCHAR(3) = '001';
	
	DECLARE @valid INT = 1;

	SELECT @valid = 0 
	FROM AssetManagement.SiteArea sa
	WHERE sa.SiteLocationId = @SiteLocationId
	AND sa.AreaNumber = @AreaNumber
	GROUP BY sa.SiteLocationId, sa.AreaNumber
	HAVING COUNT(*) > 1;

	--SELECT @valid;
	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnBNRControlPointCheck]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     01/08/2015 
-- Release:     5.4
-- Description: Makes sure that a BNR only has one control point at any time.  
--
-- mm/dd/yy  Name      Release  Description
-- 02/04/15  CGL       5.4      modified the between to check both ends of the ranges to avoid odd behavior on multiple changes in one month
-- 03/29/17  j.borgers 8.0      Added ControlPointId parameter to ensure check is run when the ControlPointId is updated
-- =============================================================
CREATE FUNCTION  [Financial].[fnBNRControlPointCheck](
	@BNRId INT,
	@ControlPointId INT
)
RETURNS bit
AS
BEGIN
	DECLARE @InvalidInd bit = 0

	SELECT @InvalidInd = COUNT(bcp.Id)
	FROM Financial.BNRControlPoint bcp
	INNER JOIN Financial.BNRControlPoint bcp2 ON bcp.BNRId = bcp2.BNRId AND bcp.Id <> bcp2.Id
	WHERE bcp.BNRId = @BNRId
	AND ((bcp.ThruDate IS NULL AND bcp2.ThruDate IS NULL) OR (bcp.FromDate < bcp2.ThruDate AND bcp.ThruDate > bcp2.FromDate))


	RETURN @InvalidInd;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnFinPlanCheckAnyActiveByBNR]'
GO
 --=============================================================
 --Author:      Jocelyn Borgers
 --Created:     01/15/2015 
 --Release:     5.4
 --Description: Checks for active FinPlans with different BNRs on each side
 --
 --mm/dd/yy  Name      Release  Description
 --03/29/17  j.borgers 8.0      Added ControlPointId parameter to ensure check gets called when the control point is updated
 --=============================================================
CREATE FUNCTION  [Financial].[fnFinPlanCheckAnyActiveByBNR]
(
	@BNRId INT,
	@ControlPointId INT
)
RETURNS bit
AS
BEGIN

	DECLARE @existsInd bit = 0;

	SELECT TOP 1 @existsInd = 1
	FROM Financial.FinPlanRequestDetail fprd
	INNER JOIN Financial.BNRControlPoint bcp ON fprd.BNRId = bcp.BNRId AND bcp.ThruDate IS NULL
	INNER JOIN Financial.FinPlanRequest fpr ON fpr.Id = fprd.FinPlanRequestId
	INNER JOIN Financial.FinPlanRequestDetail fprd2 ON fpr.Id = fprd2.FinPlanRequestId AND fprd.BNRId <> fprd2.BNRId
	INNER JOIN Financial.BNRControlPoint bcp2 ON fprd2.BNRId = bcp2.BNRId AND bcp2.ThruDate IS NULL
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = fpr.ChangeRequestDetailId
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs ON acrs.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
	WHERE fprd.BNRId = @BNRId
	AND bcp.ControlPointId <> bcp2.ControlPointId;


	RETURN @existsInd;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSMonitoringLocationsAndAnswers]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/06/2012
-- Release:     3.7
-- Description: Returns RMS monitoring locations and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSMonitoringLocationsAndAnswers](
    @RMSId INT
)
RETURNS @RMSMonitoringLocationAndAnswerTable TABLE (
    RMSId INT,
    RMSMonitoringLocationId INT,
    RMSMonitoringLocationDescription NVARCHAR(500),
    RMSMonitoringLocationParentId INT,
    RMSMonitoringLocationSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSMonitoringLocationAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSMonitoringLocationId, F.[Description] RMSMonitoringLocationDescription, COALESCE(F.ParentId, -1) RMSMonitoringLocationParentId, F.SortOrder RMSMonitoringLocationSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSMonitoringLocationXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSMonitoringLocation F ON F.Id = X.RMSMonitoringLocationId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSMonitoringLocationId, F.[Description] RMSMonitoringLocationDescription, COALESCE(F.ParentId, -1) RMSMonitoringLocationParentId, F.SortOrder RMSMonitoringLocationSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSMonitoringLocation F 
    WHERE F.Id NOT IN (
        SELECT X.RMSMonitoringLocationID
        FROM SPA.RMSRMSMonitoringLocationXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Performance].[fnGetPerformanceTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/14/2012
-- Release:     3.5
-- Description: Retrieves the Performance ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Performance].[fnGetPerformanceTypeId]()
RETURNS int
AS
BEGIN
	
	RETURN 15;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFinPlanGroupId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the FinPlan ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFinPlanGroupId]()
RETURNS int
AS
BEGIN
	
	RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructureByConfigurationType]'
GO

--SET QUOTED_IDENTIFIER ON
--SET ANSI_NULLS ON
--GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     4/19/13
-- Description: Returns a list of WorkBreakdownStructures that are 
--              associated with the given configuration type(s) and are  
--              descendants of the WorkBreakdownStructure passed in.
--
-- mm/dd/yy  Name      Release  Description
-- 10/06/14  a.smith   5.3      Renamed from fnGetProjectsfromWBSId, dropped parameter isProjectInd,
--                              added HasFunding, HasCostObligation, and SubSystemId as parameters, and reworked 
--                              function to only return those work breakdown structures that have the 
--                              funding or cost disposition as specified by the parameters.
-- 10/15/14  j.borgers 5.3      Renamed again and modifed to take any ConfigurationType or multiple ConfigurationTypes
-- =============================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructureByConfigurationType]
(
	@WorkBreakdownStructureId INT,
	@ConfigurationTypeIdTable G2.GenericUniqueIdTable READONLY
)
RETURNS @WorkBreakdownStructureList TABLE
(
	Id INT IDENTITY(1, 1) NOT NULL, 
	WorkBreakdownStructureId INT
)
AS 
BEGIN
	/* Debug Declarations */
	--DECLARE @WorkBreakdownStructureId INT = 2458/*2*/ /*6*/ /*10*/ /*707*/;
	
	--DECLARE @WorkBreakdownStructureList TABLE (id INT IDENTITY(1,1) NOT NULL, WorkBreakdownStructureId INT);
	
	--DECLARE @ConfigurationTypeIdTable G2.GenericUniqueIdTable;
	--INSERT INTO @ConfigurationTypeIdTable (Id)
	--SELECT Id
	--FROM G2.ConfigurationType ct;
	/* End Debug Declarations */

	
	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT wbs.Id
	FROM G2.WorkBreakdownStructure wbs 
	INNER JOIN G2.WorkBreakdownStructureHierarchy wbsh ON wbs.Id = wbsh.WorkBreakdownStructureId
	INNER JOIN G2.WorkBreakdownStructureHierarchy wbshParent ON wbsh.WorkBreakdownStructureHierarchyId.IsDescendantOf(wbshParent.WorkBreakdownStructureHierarchyId) = 1
	CROSS APPLY
	(
		SELECT DISTINCT wbsc.WorkBreakdownStructureId
		FROM G2.WorkBreakdownStructureConfiguration wbsc
		INNER JOIN @ConfigurationTypeIdTable ctit ON wbsc.ConfigurationTypeId = ctit.Id
		WHERE wbsc.WorkBreakdownStructureId = wbs.Id
	) hasConfig
	WHERE wbshParent.WorkBreakdownStructureId = @WorkBreakdownStructureId;

	/* Debug Return */
	--SELECT wbs.* 
	--FROM @WorkBreakdownStructureList wbsl
	--INNER JOIN G2.WorkBreakdownStructure wbs ON wbsl.WorkBreakdownStructureId = wbs.Id;
	/* End Debug Return */


	RETURN;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetUploadedBatchStatusId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     10/5/2016
-- Release:     7.3
-- Description: Returns the batch status id for status of upload
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetUploadedBatchStatusId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 8;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetFundingRequestUploadBatchTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     10/5/2016
-- Release:     7.3
-- Description: Returns the batch type id for funding request upload
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetFundingRequestUploadBatchTypeId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 12;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     9/28/2016
-- Release:     7.3
-- Description: Returns the change request type id for funding requests
--
-- mm/dd/yy  Name     Release  Description
-- 04/18/17  JDR      8.0      Modified to facilitate renaming of Funding Request objects
-- =============================================================
CREATE FUNCTION [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 37; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSiteFileUpload]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Site File Upload.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSiteFileUpload] ()
RETURNS int
AS
BEGIN
	RETURN 2; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdRoomFileUpload]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Room File Upload.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdRoomFileUpload] ()
RETURNS int
AS
BEGIN
	RETURN 9; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdResponseFileUpload]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Response File Upload.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdResponseFileUpload] ()
RETURNS int
AS
BEGIN
	RETURN 19; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdBuildingInformation]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Building Information.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdBuildingInformation] ()
RETURNS int
AS
BEGIN
	RETURN 5; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdBuildingFileUpload]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Building File Upload.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdBuildingFileUpload] ()
RETURNS int
AS
BEGIN
	RETURN 6; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPARevisionApprovedDate]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 9/15/2014
-- Description:	Returns the SPA approved date for 
--              for the given SPA revision
--
-- mm/dd/yyyy   Name        Release  Description 
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPARevisionApprovedDate] (
	@SiteId INT,
	@Revision INT
)
RETURNS DATETIME
AS
BEGIN
	DECLARE
		@ApprovedDate DATETIME,
		@SPAStatusIdApproved INT = 8
		
		-- Determine revision approved date from SPA.SPAStatusLog
		SELECT @ApprovedDate = MIN(sl.SPAStatusDt) 
		FROM SPA.SPAStatusLog sl
		INNER JOIN SPA.SPAMain sm ON sm.Id = sl.SPAMainId
		WHERE sm.SiteId = @SiteId
		AND sl.Revision = @Revision
		AND sl.SPAStatusId = @SPAStatusIdApproved

		IF @ApprovedDate IS NULL
			-- Determine previous revision approved date from SnapShot.SPAMain
			SELECT @ApprovedDate = MIN(SPAStatusDt) 
			FROM SnapShot.SPAMain
			WHERE SiteId = @SiteId
			AND Revision = @Revision
			AND SPAStatusId = @SPAStatusIdApproved

	RETURN @ApprovedDate

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSource]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Source.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSource] ()
RETURNS int
AS
BEGIN
	RETURN 9; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetWarningStatusCodeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     07/16/2014
-- Release:     5.2
-- Description: Returns the Warning status code Id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetWarningStatusCodeId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Warning
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetTaskAlreadyCompletedErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Task Already Completed
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetTaskAlreadyCompletedErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 19; --Task Already Completed
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPermissionDeniedErrorTypeId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves Permission Denied error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPermissionDeniedErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 14; -- Permission Denied
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPastDateNotMarkedCompleteErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Past Date Not Marked Complete
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPastDateNotMarkedCompleteErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 10; --Past Date Not Marked Complete
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetOkStatusCodeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     07/16/2014
-- Release:     5.2
-- Description: Returns the Warning status code Id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetOkStatusCodeId]()
RETURNS int
AS
BEGIN
	RETURN 1; -- OK
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMilestoneAlreadyCompletedErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Milestone Already Completed
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetMilestoneAlreadyCompletedErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 8; --Milestone Already Completed
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetLeadLabNeedsAssignedErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Lead Lab Needs Assigned error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetLeadLabNeedsAssignedErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 12; -- Milestone Metric Mismatch
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetJouleMilestoneCompletedOutOfRangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Joule Milestone Completed Out Of Range
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetJouleMilestoneCompletedOutOfRangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 20; --Joule Milestone Completed Out Of Range
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidProjectMilestoneErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for an invalid project milestone
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidProjectMilestoneErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 3; --Invalid ProjectMilestone
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInErrorStatusCodeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/05/2014 
-- Release:     5.0
-- Description: Returns the status code Id for In Error status code
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInErrorStatusCodeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- In Error
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetForecastDateRequiredErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     5/26/2015
-- Release:     6.0
-- Description: Retrieves Invalid Forecast Date Required error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetForecastDateRequiredErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 58; -- Forecast Date Required
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetForecastDateOutOfRangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Forecast Date Out Of Range
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetForecastDateOutOfRangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 6; --Forecast Date Out Of Range
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetCompletedMilestoneWithFutureDateErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Completed Milestone With Future Date
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetCompletedMilestoneWithFutureDateErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 9; --Completed Milestone With Future Date
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetFinalizedAssessmentStatusId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/04/16
-- Release:     7.2
-- Description: Retrieves the finalized status for assessment
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetFinalizedAssessmentStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 2;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Travel].[fnGetTravelChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/18/2013
-- Release:     4.4
-- Description: Retrieves the Travel change request type group
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Travel].[fnGetTravelChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
	RETURN 12; -- Travel
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     9/27/2016
-- Release:     7.3
-- Description: Returns the ChangeRequestTypeGroupId for funding requests
--
-- mm/dd/yy  Name     Release  Description
-- 04/18/17  JDR      8.0      Modified to facilitate renaming of Funding Request objects
-- =============================================================
CREATE FUNCTION [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
	
	RETURN 18;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnGetActivityFundingChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     08/29/18
-- Release:     9.2
-- Description: Returns the change request group type id for activity funding
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnGetActivityFundingChangeRequestTypeGroupId]()
RETURNS INT
AS
BEGIN
  RETURN 21; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Workflow].[fnCanChangeRequestDetailAdvance]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     02/14/2013
-- Release:     
-- Description: Takes a list of change request details and returns whether or not
--              they can advance in the workflow
--
-- mm/dd/yy  Name     Release  Description
-- 09/27/13  CGL      4.4      Modified for allowing ProjectId to be null in Concurrences for Travel Request
-- 11/22/13  m.brown  4.5      Modified so that travel considers both sides of approval and we ignore a side that doesn't have any permissions assigned
-- 01/22/14  m.brown  4.6      Added optional flag
-- 01/23/14  m.brown  4.6      Added workflow level 2 for travel requests to the list irrespective of whether or not we have approvers (this prevents
--                             requests from getting stuck where there are no approvers and we throw them out of consideration)
-- 02/25/14  m.brown  5.0      Refactored for new permissions model and multi-program support
-- 03/12/14  m.brown  5.0      Modified to fix a bug where this function wasn't working correctly when the approval tree was more than one level
-- 03/20/14  m.brown  5.0      Updated to handle multi-sided approval correctly again
-- 03/21/14  m.brown  5.0      Updated to an outer apply for multi-sided approval ... I apparently like to break all kinds of things ... 
-- 03/24/14  s.oneal  5.0      Fixed unanimous by checking to see if all immediate children have AdvancedInd = 1;
--                             Added JOIN condition to LOJ in PermissionsNeeded sub-query to properly check Organization and WBS to make the
--                             JOIN deterministic for both sides of multi-sided requests
-- 04/01/14  m.brown  5.0      Added capability to have optional set on non-leaf levels. 
-- 04/04/14  m.brown  5.0      Modified to consider side now (except on concurrences because we don't know how to find that yet)
-- 04/08/14  s.oneal  5.0      Added OPTION(FORCE ORDER) on function call to get advancement details
-- =============================================================
CREATE FUNCTION [Workflow].[fnCanChangeRequestDetailAdvance]
(
	@ChangeRequestDetailIdList G2.GenericUniqueIdTable READONLY 
)
RETURNS @CRDAdvance TABLE
(
	 ChangeRequestDetailId INT
	,CanAdvance BIT
)
AS
BEGIN

	DECLARE @level INT = 200, @crdIdWorkflowLevelList G2.GenericUniqueIdUniqueIntTable, @concurChangeRequestDetailConcurrenceStatusId INT = G2.fnGetConcurChangeRequestDetailConcurrenceStatusId()

	DECLARE @CRDNodeList TABLE (ChangeRequestDetailId INT, NodeId HIERARCHYID)
	DECLARE @CRDList TABLE (ChangeRequestDetailId INT, NodeId HIERARCHYID, WorkBreakdownStructureId INT, OrganizationPartyId INT, Side VARCHAR(20))
	DECLARE @crdPermissionBreakdown TABLE (ChangeRequestDetailId INT, WorkBreakdownStructureId INT, OrganizationPartyId INT, WorkflowLevel INT, SubSystemChangeRequestTypeId INT, Side VARCHAR(20), RoleId INT, UserAssignmentCount INT)
	DECLARE @PermissionTreeTable TABLE (ChangeRequestDetailId INT, WorkbreakdownStructureId INT, OrganizationPartyId INT, Side VARCHAR(20), AdvanceWorkflowId INT, ParentId INT, NodeLevel INT, UnamiousInd BIT, OptionalWhenNoAssignedUsersInd BIT, AdvanceInd BIT, ConcurrencesNeeded INT, RoleCount INT)
	
	INSERT INTO @CRDNodeList (ChangeRequestDetailId, NodeId)
	SELECT crd.Id, aw.HID
	FROM @ChangeRequestDetailIdList crdIdList
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = crdIdList.Id
	INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
	INNER JOIN G2.SubSystem ss ON ss.Id = cr.SubSystemId
	INNER JOIN G2.SubSystemChangeRequestType sscrt ON sscrt.SubSystemId = ss.Id AND sscrt.ChangeRequestTypeId = crd.ChangeRequestTypeId
	INNER JOIN G2.AdvanceWorkflow aw ON aw.SubSystemChangeRequestTypeId = sscrt.Id AND aw.WorkflowLevel = crd.WorkflowLevel
	WHERE aw.ParentId IS NULL

	INSERT INTO @crdIdWorkflowLevelList
	SELECT crd.Id, crd.WorkflowLevel
	FROM @ChangeRequestDetailIdList idList
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = idList.Id
	GROUP BY crd.Id, crd.WorkflowLevel

	INSERT INTO @crdPermissionBreakdown (ChangeRequestDetailId, WorkBreakdownStructureId, OrganizationPartyId, WorkflowLevel, SubSystemChangeRequestTypeId, Side, RoleId, UserAssignmentCount)
	SELECT  fgcrdaad.ChangeRequestDetailId, fgcrdaad.WorkBreakdownstructureId, fgcrdaad.OrganizationPartyId, fgcrdaad.WorkflowLevel, fgcrdaad.SubSystemChangeRequestTypeId, fgcrdaad.Side, fgcrdaad.RoleId, fgcrdaad.UserAssignedCount
	FROM Workflow.fnGetChangeRequestDetailApprovalAdvancementDetails(@crdIdWorkflowLevelList) fgcrdaad
	OPTION (FORCE ORDER);

	INSERT INTO @CRDList (ChangeRequestDetailId, NodeId, WorkBreakdownStructureId, OrganizationPartyId, Side)
	SELECT cl.ChangeRequestDetailId, cl.NodeId, x.WorkBreakdownStructureId, x.OrganizationPartyId, x.Side
	FROM @CRDNodeList cl
	INNER JOIN G2.ChangeRequestDetail crd ON cl.ChangeRequestDetailId = crd.Id
	OUTER APPLY
	(
		SELECT cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side
		FROM @crdPermissionBreakdown cpb 
		WHERE cpb.ChangeRequestDetailId = crd.Id AND cpb.WorkflowLevel = crd.WorkflowLevel
		GROUP BY cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side
	) x
	
	/*
	The goal here is to build the advance workflow tree structure for each "side" of a change request detail (i.e. baseline has one side, finplans and travel have two sides). The deliniation
	happens by WorkBreakdownStructureId/OrganizationPartyId in addition to ChangeRequestDetailId. In cases where both sides are for the same thing, only one record is inserted into this table.
	In addition to building the tree, we are setting whether or not the concurrence requirements at the leaf levels are obtained.
	*/
	INSERT INTO @PermissionTreeTable (ChangeRequestDetailId, WorkbreakdownStructureId, OrganizationPartyId, Side, AdvanceWorkflowId, ParentId, NodeLevel, UnamiousInd, OptionalWhenNoAssignedUsersInd, AdvanceInd, ConcurrencesNeeded, RoleCount)
	SELECT PermissionsNeeded.ChangeRequestDetailId, PermissionsNeeded.WorkBreakdownStructureId, PermissionsNeeded.OrganizationPartyId, PermissionsNeeded.Side
			,PermissionsNeeded.AdvanceWorkflowId, PermissionsNeeded.ParentId, PermissionsNeeded.NodeLevel
			,PermissionsNeeded.UnanimousInd, PermissionsNeeded.OptionalWhenNoAssignedUsersInd
			,CASE WHEN (PermissionsNeeded.RoleId IS NULL)  THEN NULL     -- it is unknown at this time if the workflow can advance because this is a node level with children that need to be evaluated                                                                             
				WHEN COALESCE(PermissionsAttained.RoleCount,0) >= PermissionsNeeded.ConcurrencesNeeded THEN 1	-- if we have all the needed approvals then advance
				ELSE 0																													
			END AS AdvanceWorkflowInd
			,PermissionsNeeded.ConcurrencesNeeded
			,COALESCE(PermissionsNeeded.RoleCount, 0) AS RoleCount
	FROM 
	(
		SELECT cl.ChangeRequestDetailId, cl.WorkBreakdownStructureId, cl.OrganizationPartyId, cl.Side
				,aw.Id AS AdvanceWorkflowId, aw.ParentId, aw.UnanimousInd, aw.OptionalWhenNoAssignedUsersInd, aw.RoleId, aw.PermissionId
				,aw.HID, aw.HID.GetLevel() AS NodeLevel
				,CASE WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 0 THEN 1
					WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 1 AND COALESCE(cpb.UserAssignmentCount, 0) = 0 THEN 0
					WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 1 AND COALESCE(cpb.UserAssignmentCount, 0) <> 0 THEN 1
					WHEN aw.PermissionId IS NOT NULL AND aw.UnanimousInd = 1 THEN CASE WHEN cpb.UserAssignmentCount = 0 THEN 1 ELSE cpb.UserAssignmentCount END
				END AS ConcurrencesNeeded
				,cpb.UserAssignmentCount AS RoleCount 
		FROM @CRDList cl
		INNER JOIN G2.ChangeRequestDetail crd ON cl.ChangeRequestDetailId = crd.Id
		INNER JOIN G2.AdvanceWorkflow aw ON aw.HID.IsDescendantOf(cl.NodeId) = 1
		LEFT OUTER JOIN @crdPermissionBreakdown cpb ON cl.ChangeRequestDetailId = cpb.ChangeRequestDetailId AND aw.WorkflowLevel = cpb.WorkflowLevel AND aw.RoleId = cpb.RoleId AND aw.SubSystemChangeRequestTypeId = cpb.SubSystemChangeRequestTypeId AND COALESCE(cl.WorkBreakdownStructureId, -1) = COALESCE(cpb.WorkBreakdownStructureId, -1) AND COALESCE(cl.OrganizationPartyId, -1) = COALESCE(cpb.OrganizationPartyId, -1) AND COALESCE(cl.Side, '') = COALESCE(cpb.Side, '')
	) AS PermissionsNeeded
	LEFT OUTER JOIN 
	(
		SELECT cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side, crdcr.RoleId AS RoleId, COUNT(*) AS RoleCount
		FROM @crdPermissionBreakdown cpb
		INNER JOIN G2.ChangeRequestDetailConcurrence crdc ON cpb.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND COALESCE(crdc.WorkBreakdownStructureId, -1) = COALESCE(cpb.WorkBreakdownStructureId, -1) AND COALESCE(crdc.OrganizationPartyId, -1) = COALESCE(cpb.OrganizationPartyId, -1) AND COALESCE(crdc.Side, '') = COALESCE(cpb.Side, '') 
		INNER JOIN G2.ChangeRequestDetailConcurrenceRole crdcr ON crdc.Id = crdcr.ChangeRequestDetailConcurrenceId
		WHERE crdc.WorkflowLevel = cpb.WorkflowLevel
		AND crdc.ActiveInd = 1
		AND crdc.CourtesyInd = 0
		AND crdc.StatusId = @concurChangeRequestDetailConcurrenceStatusId
		GROUP BY cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side, crdcr.RoleId
	) PermissionsAttained ON PermissionsAttained.RoleId = PermissionsNeeded.RoleId AND PermissionsAttained.ChangeRequestDetailId = PermissionsNeeded.ChangeRequestDetailId AND COALESCE(PermissionsAttained.WorkBreakdownStructureId, -1) = COALESCE(PermissionsNeeded.WorkBreakdownStructureId, -1) AND COALESCE(PermissionsAttained.OrganizationPartyId, -1) = COALESCE(PermissionsNeeded.OrganizationPartyId, -1) AND COALESCE(PermissionsAttained.Side, '') = COALESCE(PermissionsNeeded.Side, '')
		
	SELECT @level = COALESCE(MAX(NodeLevel),0)
	FROM @PermissionTreeTable pt
	WHERE pt.AdvanceInd IS NULL
    
    -- At this point, the algorithm starts and the lowest non-leaf  level of the tree and determines if all necessary concurs have been obtained
    -- (this is done by comparing the ConcurNeeded with the count of concurs of the child nodes).  If all concurs are satisfied the function then looks at the
    -- next lowest level and so forth.  If at any level the number of concurs is not obtained the function returns 0 otherwise the function   returns 1.
    
	WHILE COALESCE(@level,0) <> 0 
	BEGIN	 		 
	UPDATE parent
	SET AdvanceInd = CASE WHEN ((parent.UnamiousInd = 1) AND ((SELECT MIN(COALESCE(CAST (child.AdvanceInd AS INT),0)) 
																FROM @PermissionTreeTable child
																WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
																AND COALESCE(child.WorkbreakdownStructureId, -1) = COALESCE(parent.WorkbreakdownStructureId, -1)
																AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
																AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
																AND child.ParentId = Parent.AdvanceWorkflowId) = 1)) THEN CAST (1 AS BIT)
							WHEN ((parent.UnamiousInd = 0) AND (parent.OptionalWhenNoAssignedUsersInd = 1) AND 
								  (SELECT SUM(child.RoleCount) 
								   FROM @PermissionTreeTable child
								   WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
								   AND COALESCE(child.WorkBreakdownStructureId, -1) = COALESCE(parent.WorkBreakdownStructureId, -1)
								   AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
								   AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
								   AND child.ParentId = Parent.AdvanceWorkflowId) = 0) THEN CAST(1 AS BIT)
							WHEN ((parent.UnamiousInd = 0) AND 
								 ((SELECT SUM(COALESCE(CAST (child.AdvanceInd AS INT),0)) 
								   FROM @PermissionTreeTable child
								   WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
								   AND COALESCE(child.WorkBreakdownStructureId, -1) = COALESCE(parent.WorkBreakdownStructureId, -1)
								   AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
								   AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
								   AND child.ParentId = Parent.AdvanceWorkflowId) > 0)) THEN CAST (1 AS BIT)
							ELSE CAST (0 AS BIT)
					   END
		, RoleCount = (SELECT SUM(child.RoleCount) 
					   FROM @PermissionTreeTable child
				       WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
					   AND COALESCE(child.WorkBreakdownStructureId, -1) = COALESCE(parent.WorkBreakdownStructureId, -1)
					   AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
					   AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
					   AND child.ParentId = Parent.AdvanceWorkflowId)
	FROM @PermissionTreeTable parent
	WHERE @level = parent.NodeLevel 
	AND parent.AdvanceInd IS NULL

	SELECT @level = COALESCE(MAX(NodeLevel),0)
	FROM @PermissionTreeTable pt
	WHERE pt.AdvanceInd IS NULL
	END   
	
	INSERT INTO @CRDAdvance
	SELECT t.ChangeRequestDetailId, CAST(MIN(CAST(AdvanceInd AS INT)) AS BIT) AS CanAdvance
	FROM @PermissionTreeTable t
	WHERE t.ParentId IS NULL
	GROUP BY t.ChangeRequestDetailId

	RETURN
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Workflow].[fnChangeRequestDetailMaxNonOptionalWorkflowLevel]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     01/27/2013
-- Release:     4.6
-- Description: Gives the maximum workflow level that lower than the current workflow
--              level that isn't optional; this is used for auto concurrence consideration
--              and passback to figure out what workflow level we should compare those things to.
--
-- mm/dd/yy  Name     Release  Description
-- 02/25/14  m.brown  5.0      Refactored for new permissions model and multi-program support
-- 03/17/14  m.brown  5.0      Modified to fix a bug where this function wasn't working correctly when the approval tree was more than one level
-- 03/20/14  m.brown  5.0      Updated to handle multi-sided approval correctly again
-- 04/04/14  m.brown  5.0      Updated to include refactored changes in canadvance
-- 04/04/14  m.brown  5.0      Modified to consider side now (except on concurrences because we don't know how to find that yet)
-- 04/08/14  s.oneal  5.0      Added OPTION(FORCE ORDER) on function call to get advancement details
-- 04/28/14  s.oneal  5.0      Fixed optional issue where we were considering the MAX of the child OptionalInd instead of the MIN
-- =============================================================
CREATE FUNCTION [Workflow].[fnChangeRequestDetailMaxNonOptionalWorkflowLevel]
(
	@ChangeRequestDetailIdList G2.GenericUniqueIdTable READONLY 
)
RETURNS @MaxCRDLevel TABLE
(
	 ChangeRequestDetailId INT
	,MaxNonOptionalWorkflowLevel INT
)
AS
BEGIN

	/*
	
	  AAAA   AAAA   AAAA   AAAA  RRRRR   GGGG  HH  HH  ######
	 AA  AA AA  AA AA  AA AA  AA RR  RR GG  GG HH  HH  ######
	 AA  AA AA  AA AA  AA AA  AA RR  RR GG     HH  HH   ####
	 AAAAAA AAAAAA AAAAAA AAAAAA RRRRR  GG GGG HHHHHH    ##
	 AA  AA AA  AA AA  AA AA  AA RR  RR GG  GG HH  HH
	 AA  AA AA  AA AA  AA AA  AA RR  RR  GGG G HH  HH    ##

	 If you change this function, change Workflow.fnCanChangeRequestDetailAdvance as well!!
	
	*/
	
	DECLARE @level INT = 200, @crdIdWorkflowLevelList G2.GenericUniqueIdUniqueIntTable, @concurChangeRequestDetailConcurrenceStatusId INT = G2.fnGetConcurChangeRequestDetailConcurrenceStatusId()
	DECLARE @tempNums AS TABLE (Number INT IDENTITY(1, 1), Holder INT)
	DECLARE @CRDNodeList TABLE (ChangeRequestDetailId INT, WorkflowLevel INT, NodeId HIERARCHYID, PotentialOptionalLevel BIT NULL)
	DECLARE @CRDList TABLE (ChangeRequestDetailId INT, WorkflowLevel INT, NodeId HIERARCHYID, PotentialOptionalLevel BIT NULL, WorkBreakdownStructureId INT, OrganizationPartyId INT, Side VARCHAR(20))
	DECLARE @crdPermissionBreakdown TABLE (ChangeRequestDetailId INT, WorkBreakdownStructureId INT, OrganizationPartyId INT, Side VARCHAR(20), WorkflowLevel INT, SubSystemChangeRequestTypeId INT, RoleId INT, UserAssignmentCount INT)
	DECLARE @PermissionTreeTable TABLE (ChangeRequestDetailId INT, WorkflowLevel INT, WorkBreakdownStructureId INT, OrganizationPartyId INT, Side VARCHAR(20), AdvanceWorkflowId INT, ParentId INT, NodeLevel INT, UnanimousInd BIT, OptionalWhenNoAssignedUsersInd BIT, OptionalInd BIT, RoleCount INT) 
		
	--DECLARE @ChangeRequestDetailIdList G2.GenericUniqueIdTable
	--DECLARE @MaxCRDLevel AS TABLE (ChangeRequestDetailId INT, MaxNonOptionalWorkflowLevel INT)

	--INSERT INTO @ChangeRequestDetailIdList (Id) VALUES (79559) --Baseline
	--INSERT INTO @ChangeRequestDetailIdList (Id) VALUES (79858) --Travel

	INSERT INTO @tempNums (Holder)
	SELECT TOP 100 1
	FROM sys.objects s1
	CROSS JOIN sys.objects s2

	INSERT INTO @CRDNodeList (ChangeRequestDetailId, WorkflowLevel, NodeId)
	SELECT crd.Id, tn.Number, aw.HID
	FROM @ChangeRequestDetailIdList crdIdList
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = crdIdList.Id
	INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
	INNER JOIN G2.SubSystemChangeRequestType sscrt ON sscrt.ChangeRequestTypeId = crd.ChangeRequestTypeId AND sscrt.SubSystemId = cr.SubSystemId
	INNER JOIN @tempNums tn ON tn.Number < crd.WorkflowLevel
	LEFT OUTER JOIN G2.AdvanceWorkflow aw ON sscrt.Id = aw.SubSystemChangeRequestTypeId AND aw.WorkflowLevel = tn.Number
	WHERE aw.ParentId IS NULL

	UPDATE c
	SET PotentialOptionalLevel = COALESCE(x.MaxOptionalInd, 0)
	FROM @CRDNodeList c
	OUTER APPLY 
	(
		SELECT CAST(MAX(CAST(aw.OptionalWhenNoAssignedUsersInd AS INT)) AS BIT) AS MaxOptionalInd
		FROM G2.AdvanceWorkflow aw
		WHERE aw.HID.IsDescendantOf(c.NodeId) = 1
		GROUP BY aw.SubSystemChangeRequestTypeId, aw.WorkflowLevel
	) x

	INSERT INTO @crdIdWorkflowLevelList
	SELECT cl.ChangeRequestDetailId, cl.WorkflowLevel
	FROM @CRDNodeList cl
	WHERE cl.PotentialOptionalLevel = 1
	GROUP BY cl.ChangeRequestDetailId, cl.WorkflowLevel
	
	INSERT INTO @crdPermissionBreakdown (ChangeRequestDetailId, WorkBreakdownStructureId, OrganizationPartyId, WorkflowLevel, SubSystemChangeRequestTypeId, Side, RoleId, UserAssignmentCount)
	SELECT  fgcrdaad.ChangeRequestDetailId, fgcrdaad.WorkBreakdownstructureId, fgcrdaad.OrganizationPartyId, fgcrdaad.WorkflowLevel, fgcrdaad.SubSystemChangeRequestTypeId, fgcrdaad.Side, fgcrdaad.RoleId, fgcrdaad.UserAssignedCount
	FROM Workflow.fnGetChangeRequestDetailApprovalAdvancementDetails(@crdIdWorkflowLevelList) fgcrdaad
	OPTION (FORCE ORDER);

	INSERT INTO @CRDList (ChangeRequestDetailId, WorkflowLevel, NodeId, PotentialOptionalLevel, WorkBreakdownStructureId, OrganizationPartyId, Side) 
	SELECT cl.ChangeRequestDetailId, cl.WorkflowLevel, cl.NodeId, cl.PotentialOptionalLevel, x.WorkBreakdownStructureId, x.OrganizationPartyId, x.Side
	FROM @CRDNodeList cl
	INNER JOIN G2.ChangeRequestDetail crd ON cl.ChangeRequestDetailId = crd.Id
	OUTER APPLY
	(
		SELECT cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side
		FROM @crdPermissionBreakdown cpb 
		WHERE cpb.ChangeRequestDetailId = crd.Id AND cpb.WorkflowLevel = cl.WorkflowLevel
		GROUP BY cpb.ChangeRequestDetailId, cpb.WorkBreakdownStructureId, cpb.OrganizationPartyId, cpb.Side
	) x
	
	INSERT INTO @PermissionTreeTable (ChangeRequestDetailId, WorkflowLevel, WorkBreakdownStructureId, OrganizationPartyId, Side, AdvanceWorkflowId, ParentId, NodeLevel, UnanimousInd, OptionalWhenNoAssignedUsersInd, OptionalInd, RoleCount) 
	SELECT PermissionsNeeded.ChangeRequestDetailId, PermissionsNeeded.WorkflowLevel, PermissionsNeeded.WorkBreakdownStructureId, PermissionsNeeded.OrganizationPartyId, PermissionsNeeded.Side
		  ,PermissionsNeeded.AdvanceWorkflowId, PermissionsNeeded.ParentId, PermissionsNeeded.NodeLevel
		  ,PermissionsNeeded.UnanimousInd
		  ,PermissionsNeeded.OptionalWhenNoAssignedUsersInd
		  ,CAST(CASE WHEN PermissionsNeeded.ConcurrencesNeeded = 0 AND PermissionsNeeded.OptionalWhenNoAssignedUsersInd = 1 AND PermissionsNeeded.RoleId IS NOT NULL THEN 1 
					 WHEN PermissionsNeeded.RoleId IS NOT NULL THEN 0
				END AS BIT) AS OptionalInd
		  ,COALESCE(PermissionsNeeded.RoleCount, 0) AS RoleCount
	FROM 
	(
		SELECT cl.ChangeRequestDetailId, cl.WorkflowLevel, cl.WorkBreakdownStructureId, cl.OrganizationPartyId, cl.Side
			  ,aw.Id AS AdvanceWorkflowId, aw.ParentId, aw.UnanimousInd, aw.RoleId, aw.PermissionId
			  ,aw.HID, aw.HID.GetLevel() AS NodeLevel
			  ,CASE WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 0 AND aw.RoleId IS NOT NULL THEN 1
					WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 1 AND COALESCE(cpb.UserAssignmentCount, 0) = 0 AND aw.RoleId IS NOT NULL THEN 0
					WHEN aw.UnanimousInd = 0 AND aw.OptionalWhenNoAssignedUsersInd = 1 AND COALESCE(cpb.UserAssignmentCount, 0) <> 0 AND aw.RoleId IS NOT NULL THEN 1
					WHEN aw.PermissionId IS NOT NULL AND aw.UnanimousInd = 1 THEN CASE WHEN cpb.UserAssignmentCount = 0 THEN 1 ELSE cpb.UserAssignmentCount END
			   END AS ConcurrencesNeeded
			   ,aw.OptionalWhenNoAssignedUsersInd
			   ,cpb.UserAssignmentCount AS RoleCount 
		FROM @CRDList cl
		INNER JOIN G2.ChangeRequestDetail crd ON cl.ChangeRequestDetailId = crd.Id
		INNER JOIN G2.AdvanceWorkflow aw ON aw.HID.IsDescendantOf(cl.NodeId) = 1
		LEFT OUTER JOIN @crdPermissionBreakdown cpb ON cl.ChangeRequestDetailId = cpb.ChangeRequestDetailId AND aw.WorkflowLevel = cpb.WorkflowLevel AND aw.RoleId = cpb.RoleId AND aw.SubSystemChangeRequestTypeId = cpb.SubSystemChangeRequestTypeId AND COALESCE(cl.WorkBreakdownStructureId, -1) = COALESCE(cpb.WorkBreakdownStructureId, -1) AND COALESCE(cl.OrganizationPartyId, -1) = COALESCE(cpb.OrganizationPartyId, -1)  AND COALESCE(cl.Side, '') = COALESCE(cpb.Side, '')
	) AS PermissionsNeeded
	LEFT OUTER JOIN 
	(
		SELECT cpb.ChangeRequestDetailId, COALESCE(cpb.WorkBreakdownStructureId, -1) AS WorkBreakdownStructureId, COALESCE(cpb.OrganizationPartyId, -1) AS OrganizationPartyId, cpb.Side, crdcr.RoleId AS RoleId, COUNT(*) AS RoleCount
		FROM @crdPermissionBreakdown cpb
		INNER JOIN G2.ChangeRequestDetailConcurrence crdc ON cpb.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND crdc.WorkflowLevel = cpb.WorkflowLevel AND COALESCE(crdc.WorkBreakdownStructureId, -1) = COALESCE(cpb.WorkBreakdownStructureId, -1) AND COALESCE(crdc.OrganizationPartyId, -1) = COALESCE(cpb.OrganizationPartyId, -1) AND COALESCE(crdc.Side, '') = COALESCE(cpb.Side, '')
		INNER JOIN G2.ChangeRequestDetailConcurrenceRole crdcr ON crdc.Id = crdcr.ChangeRequestDetailConcurrenceId
		WHERE crdc.ActiveInd = 1
		AND crdc.CourtesyInd = 0
		AND crdc.StatusId = @concurChangeRequestDetailConcurrenceStatusId
		GROUP BY cpb.ChangeRequestDetailId, COALESCE(cpb.WorkBreakdownStructureId, -1), COALESCE(cpb.OrganizationPartyId, -1), cpb.Side, crdcr.RoleId
	) PermissionsAttained ON PermissionsAttained.RoleId = PermissionsNeeded.RoleId AND PermissionsAttained.ChangeRequestDetailId = PermissionsNeeded.ChangeRequestDetailId AND COALESCE(PermissionsAttained.WorkBreakdownStructureId, -1) = PermissionsNeeded.WorkBreakdownStructureId AND COALESCE(PermissionsAttained.OrganizationPartyId, -1) = PermissionsNeeded.OrganizationPartyId AND COALESCE(PermissionsAttained.Side, '') = COALESCE(PermissionsNeeded.Side, '')
		
	SELECT @level = COALESCE(MAX(NodeLevel),0) - 1 -- We just want 1 up from the leaf level
	FROM @PermissionTreeTable pt
    
    -- At this point, the algorithm starts and the lowest non-leaf  level of the tree and determines if all necessary concurs have been obtained
    -- (this is done by comparing the ConcurNeeded with the count of concurs of the child nodes).  If all concurs are satisfied the function then looks at the
    -- next lowest leveland so forth.  If at any level the number of concurs is not obtained the function returns 0 otherwise the function   returns 1.
	WHILE COALESCE(@level,0) <> 0 BEGIN

	UPDATE parent
	SET OptionalInd = CASE
						  WHEN parent.UnanimousInd = 0 AND parent.OptionalWhenNoAssignedUsersInd = 1
						  THEN  CASE WHEN 
									(SELECT SUM(child.RoleCount) 
									 FROM @PermissionTreeTable child
									 WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
									 AND child.WorkflowLevel = parent.WorkflowLevel
									 AND COALESCE(child.WorkBreakdownStructureId, -1) = COALESCE(parent.WorkBreakdownStructureId, -1)
									 AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
									 AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
									 AND child.ParentId = Parent.AdvanceWorkflowId) = 0 
									THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT)
								END
						  WHEN parent.UnanimousInd = 1
						  THEN CASE WHEN 
									(SELECT MIN(COALESCE(CAST (child.OptionalInd AS INT),0)) 
									 FROM @PermissionTreeTable child
									 WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
									 AND child.WorkflowLevel = parent.WorkflowLevel
									 AND COALESCE(child.WorkbreakdownStructureId, -1) = COALESCE(parent.WorkbreakdownStructureId, -1)
									 AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
									 AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
									 AND child.ParentId = Parent.AdvanceWorkflowId) = 1
									THEN CAST (1 AS BIT) ELSE CAST(0 AS BIT)
							   END
						  WHEN parent.UnanimousInd = 0
						  THEN CASE WHEN 
									(SELECT SUM(COALESCE(CAST (child.OptionalInd AS INT),0)) 
									 FROM @PermissionTreeTable child
									 WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
									 AND child.WorkflowLevel = parent.WorkflowLevel
									 AND COALESCE(child.WorkbreakdownStructureId, -1) = COALESCE(parent.WorkbreakdownStructureId, -1)
									 AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
									 AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
									 AND child.ParentId = Parent.AdvanceWorkflowId) > 0
									THEN CAST (1 AS BIT) ELSE CAST(0 AS BIT)
								END
						  ELSE CAST (0 AS BIT)
					   END
		, RoleCount = (SELECT SUM(child.RoleCount) 
					   FROM @PermissionTreeTable child
				       WHERE child.ChangeRequestDetailId = parent.ChangeRequestDetailId
					   AND COALESCE(child.WorkBreakdownStructureId, -1) = COALESCE(parent.WorkBreakdownStructureId, -1)
					   AND COALESCE(child.OrganizationPartyId, -1) = COALESCE(parent.OrganizationPartyId, -1)
					   AND COALESCE(child.Side, '') = COALESCE(parent.Side, '')
					   AND child.ParentId = Parent.AdvanceWorkflowId)
	FROM @PermissionTreeTable parent
	WHERE @level = parent.NodeLevel 
	AND parent.OptionalInd IS NULL

	SELECT @level = COALESCE(MAX(NodeLevel),0)
	FROM @PermissionTreeTable pt
	WHERE pt.OptionalInd IS NULL
			    
	END

	UPDATE c
		SET PotentialOptionalLevel = x.OptionalInd
	FROM @CRDList c 
	INNER JOIN 
	( 
		SELECT t.ChangeRequestDetailId, t.WorkflowLevel, CAST(MIN(CAST(OptionalInd AS INT)) AS BIT) AS OptionalInd 
		FROM @CRDList c
		INNER JOIN @PermissionTreeTable t ON c.WorkflowLevel = t.WorkflowLevel AND c.ChangeRequestDetailId = t.ChangeRequestDetailId
		WHERE t.ParentId IS NULL
		GROUP BY t.ChangeRequestDetailId, t.WorkflowLevel
	) x ON x.ChangeRequestDetailId = c.ChangeRequestDetailId AND x.WorkflowLevel = c.WorkflowLevel

	INSERT INTO @MaxCRDLevel
	SELECT c.ChangeRequestDetailId, MAX(c.WorkflowLevel)
	FROM @CRDList c
	WHERE c.PotentialOptionalLevel = 0
	GROUP BY c.ChangeRequestDetailId
	
	RETURN
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSFeatureChallengesAndAnswers]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/14/2012
-- Release:     3.7
-- Description: Returns RMS systeam features and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSFeatureChallengesAndAnswers](
    @RMSId INT
)
RETURNS @RMSFeatureChallengeAndAnswerTable TABLE (
    RMSId INT,
    RMSFeatureChallengeId INT,
    RMSFeatureChallengeDescription NVARCHAR(500),
    RMSFeatureChallengeSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSFeatureChallengeAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSFeatureChallengeId, F.[Description] RMSFeatureChallengeDescription, F.SortOrder RMSFeatureChallengeSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSFeatureChallengeXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSFeatureChallenge F ON F.Id = X.RMSFeatureChallengeId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSFeatureChallengeId, F.[Description] RMSFeatureChallengeDescription, F.SortOrder RMSFeatureChallengeSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSFeatureChallenge F 
    WHERE F.Id NOT IN (
        SELECT X.RMSFeatureChallengeID
        FROM SPA.RMSRMSFeatureChallengeXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnCheckRequestHasOnlyOnePrimaryReportingEntity]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     06/25/2018
-- Release:     9.1
-- Description: There should only be one active activvty fund request for a period, performer and project
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnCheckRequestHasOnlyOnePrimaryReportingEntity]
(   
	@ChangeRequestDetailId INT -- from activity
)
RETURNS BIT 
AS 
BEGIN 

   DECLARE @valid BIT = 1;


   SELECT @valid = 0
   --awbx.WorkBreakdownStructureId, crd.PeriodId--, fr.ReportingEntityOrganizationPartyId
   FROM Activity.ActivityWorkBreakdownStructureXref awbx
   INNER JOIN Activity.FundingRequest fr ON fr.ActivityWorkBreakdownStructureXrefId = awbx.Id 
   INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = fr.ChangeRequestDetailId
   INNER JOIN G2.vwChangeRequestDetailStatusActiveStatesAndProcessedStates craa ON craa.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
   WHERE fr.ChangeRequestDetailId = @ChangeRequestDetailId
   GROUP BY awbx.WorkBreakdownStructureId,  crd.PeriodId
   HAVING COUNT(Distinct fr.ReportingEntityOrganizationPartyId) > 1;
	
   RETURN @valid;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnGetActivityFundingChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     6/21/2018
-- Release:     9.1
-- Description: Returns the change request type id for activity funding
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnGetActivityFundingChangeRequestTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 42; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetCanceledBatchStatusId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/3/2016
-- Release:     6.5
-- Description: Return the batch status id of canceled
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetCanceledBatchStatusId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 5;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetSupplementalFundingTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the Supplemental Funding
--   ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetSupplementalFundingTypeId]()
RETURNS int
AS
BEGIN
	RETURN 19;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetRemovedFundingTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the Removed Funding
--   ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetRemovedFundingTypeId]()
RETURNS int
AS
BEGIN
	RETURN 20;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNewAppropriationTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the New Appropriation
--   ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNewAppropriationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 18;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPerformanceReportingImportBatchTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the Batch Type Id of Performance Reporting Import
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPerformanceReportingImportBatchTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 1; --Performance Reporting Import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInProcessBatchStatusId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/05/2014 
-- Release:     5.0
-- Description: Returns the batch status Id for the In Process batch status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInProcessBatchStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- In Process
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSMonitoringLocationChallengesAndAnswers]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/14/2012
-- Release:     3.7
-- Description: Returns RMS systeam MonitoringLocations and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSMonitoringLocationChallengesAndAnswers](
    @RMSId INT
)
RETURNS @RMSMonitoringLocationChallengeAndAnswerTable TABLE (
    RMSId INT,
    RMSMonitoringLocationChallengeId INT,
    RMSMonitoringLocationChallengeDescription NVARCHAR(500),
    RMSMonitoringLocationChallengeParentId INT,
    RMSMonitoringLocationChallengeSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSMonitoringLocationChallengeAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSMonitoringLocationChallengeId, F.[Description] RMSMonitoringLocationChallengeDescription, COALESCE(F.ParentId, -1) RMSMonitoringLocationChallengeParentId, F.SortOrder RMSMonitoringLocationChallengeSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSMonitoringLocationChallengeXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSMonitoringLocationChallenge F ON F.Id = X.RMSMonitoringLocationChallengeId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSMonitoringLocationChallengeId, F.[Description] RMSMonitoringLocationChallengeDescription, COALESCE(F.ParentId, -1) RMSMonitoringLocationChallengeParentId, F.SortOrder RMSMonitoringLocationChallengeSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSMonitoringLocationChallenge F
    WHERE F.Id NOT IN (
        SELECT X.RMSMonitoringLocationChallengeID
        FROM SPA.RMSRMSMonitoringLocationChallengeXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIStructuralRiskQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the question id for the ERI Structural Risk Question.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIStructuralRiskQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 111; --Inquery.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIEnvironmentalReceptorsQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Environmental Receptors Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIEnvironmentalReceptorsQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 106; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIDistanceToWorkersQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Distance To Workers Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIDistanceToWorkersQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 110; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIDistanceToPublicQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Distance To Public Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIDistanceToPublicQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 108; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIDistanceToFacilityQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Distance to facility Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIDistanceToFacilityQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 109; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIDistanceToBoundaryQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Distance To Boundary Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIDistanceToBoundaryQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 107; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIContaminationRiskQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Contamination Risk Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIContaminationRiskQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 112; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetAssetDetailsQuestionnaireId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the Asset Details Questionnaire Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetAssetDetailsQuestionnaireId]()
RETURNS INT
AS
BEGIN
  RETURN 4; --Asset Details Questionnaire 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetProposedParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Proposed ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetProposedParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 2; -- Proposed
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetPersonalRadiationDetectorTrainingEventTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the PRDT EventType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetPersonalRadiationDetectorTrainingEventTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 4; -- PRDT
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetApprovedParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Approved ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetApprovedParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 3; -- Approved
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetAlarmResponseTrainingEventTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the ART EventType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetAlarmResponseTrainingEventTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 1; -- ART
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPermissionDeniedDueToExistingBCRErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Permission Denied Due To Existing BCR error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPermissionDeniedDueToExistingBCRErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 49; -- Permission Denied Due To Existing BCR
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetOverwriteExistingBCRErrorTypeId]'
GO

-- =============================================================
-- Author:      Chris O'Neal
-- Created:     8/18/2014
-- Release:     5.2
-- Description: Retrieves Overwrite Existing BCR warning
--   type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetOverwriteExistingBCRErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 48; -- Overwrite Existing BCR warning
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMilestoneMetricMismatchErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Milestone Metric Mismatch error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetMilestoneMetricMismatchErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 55; -- Milestone Metric Mismatch
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMetricValueOutOfRangeErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Metric Value Out of Range error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetMetricValueOutOfRangeErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 15; -- Metric Value Out of Range
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMetricValueCannotBeChangedErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Metric Value Cannot be Changed error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetMetricValueCannotBeChangedErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 16; -- Metric Value Cannot be Changed
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMetricPlanExistsErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     8/13/2014
-- Release:     5.2
-- Description: Retrieves Metric Plan Exists error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetMetricPlanExistsErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 57; -- Metric Plan Exists
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetLeadLabPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     7/1/14
-- Release:     5.2
-- Description: Retrieves the Lead Lab party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetLeadLabPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 9; -- Lead Lab
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidLeadPerformerErrorTypeId]'
GO

-- =============================================================
-- Author:      Jason Frank
-- Created:     06/23/2015
-- Release:     6.1
-- Description: Retrieves Invalid Lead Performer error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidLeadPerformerErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 60; -- Invalid Lead Performer
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidDefaultDateWarningErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Invalid Default Date Warning error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidDefaultDateWarningErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 47; -- Invalid Default Date Warning
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidDefaultDateErrorErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Invalid Default Date Error error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidDefaultDateErrorErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 54; -- Invalid Default Date Error
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInactiveTaskErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Inactive Task error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInactiveTaskErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 50; -- Inactive Task
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInactiveBCRErrorTypeId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Retrieves Inactive BCR error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInactiveBCRErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 51; -- Inactive BCR
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetCompletedMilestoneErrorTypeId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     10/5/2014
-- Release:     5.3
-- Description: Returns the completed milestone error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetCompletedMilestoneErrorTypeId]()
RETURNS INT
AS 
BEGIN
	RETURN 52;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnGetBaselineTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/10/2012
-- Release:     3.3
-- Description: Retrieves the Baseline ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Baseline].[fnGetBaselineTypeId]()
RETURNS int
AS
BEGIN
	RETURN 23;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetBuildingExcludeSPAInd]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 8/23/2012
-- Description:	Returns true if a bulding should be
--              excluded from SPA security upgrades,
--              photographs, and diagrams. 
--
-- mm/dd/yyyy   Name        Release  Description 
-- 02/22/2013   Strickland  3.8.2    Update to exclude only domestic reactors - international reactors should NOT be excluded
-- =============================================
CREATE FUNCTION [SPA].[fnGetBuildingExcludeSPAInd] (
	@BuildingId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE 
		@ExcludeSPAInd BIT = 0,
		@BuildingTypeIdResearchReactor INT = 4,
		@BuildingTagIdSafeguardsInformationModified INT = 5,
		@BuildingTagIdNuclearPowerPlant INT = 6,
		@RowCount INT	

	SELECT @ExcludeSPAInd = CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
	FROM G2.Building b
	LEFT JOIN G2.BuildingBuildingTagXref x ON x.BuildingId = b.Id
	WHERE b.Id = @BuildingId
	AND (
		b.ExcludeSPAInd = 1
		OR (b.BuildingTypeId = @BuildingTypeIdResearchReactor AND b.CountryId = 1)
		OR x.BuildingTagId IN (@BuildingTagIdSafeguardsInformationModified, @BuildingTagIdNuclearPowerPlant)
	)

	RETURN @ExcludeSPAInd
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Travel].[fnGetTravelChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/18/2013
-- Release:     4.4
-- Description: Retrieves the Travel change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Travel].[fnGetTravelChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 25; -- Travel
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Travel].[fnGetApprovedTripStatusId]'
GO
-- =============================================================
-- Author:      Jason Frank
-- Created:		9/25/2013
-- Release:     4.4
-- Description: Retrieves the Approved TripStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Travel].[fnGetApprovedTripStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 1; -- Approved.
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetRadiologicalMaterialTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/02/14
-- Release:     5.3
-- Description: Returns the Radiological material type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetRadiologicalMaterialTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1; --Radiological
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnCheckRequestHasOnlyOneClassificationTypePerClassification]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/18/18 
-- Release:     9.1
-- Description: Checks that a request only has one classification type per classification
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnCheckRequestHasOnlyOneClassificationTypePerClassification]
(
	@RequestId INT,
	@ClassificationTypeId INT
)
RETURNS INT
AS
BEGIN
	--DECLARE @AssetId INT = 1;
	
	DECLARE @valid INT = 1;

	SELECT @valid = 0 
	FROM AssetManagement.RequestClassification rc
	INNER JOIN AssetManagement.ClassificationType ct ON ct.Id = rc.ClassificationTypeId
	WHERE rc.RequestId = @RequestId
	GROUP BY ct.ClassificationId
	HAVING COUNT(*) > 1;

	--SELECT @valid;
	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnCheckAssetHasOnlyOneClassificationTypePerClassification]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/18/18 
-- Release:     9.1
-- Description: Checks that an asset only has one classification type per classification
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnCheckAssetHasOnlyOneClassificationTypePerClassification]
(
	@AssetId INT,
	@ClassificationTypeId INT
)
RETURNS INT
AS
BEGIN
	--DECLARE @AssetId INT = 1;
	
	DECLARE @valid INT = 1;

	SELECT @valid = 0 
	FROM AssetManagement.AssetClassification ac
	INNER JOIN AssetManagement.ClassificationType ct ON ct.Id = ac.ClassificationTypeId
	WHERE ac.AssetId = @AssetId
	GROUP BY ct.ClassificationId
	HAVING COUNT(*) > 1;

	--SELECT @valid;
	RETURN @valid;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnValidateParameters]'
GO
-- =============================================================
-- Author:      Robin Auerbach
-- Created:     6/12/2012
-- Description: Validates parameters for baseline procedures
--
-- mm/dd/yy  Name     Release  Description
-- 06/19/12  RLA      3.3      Modified to incorporate Recall
-- 06/19/12  s.oneal  3.3      Modified to not perform change request validation
--                             on Recall because of validation messages in code
-- 07/11/12  m.brown  3.3      Modified to use fnGetBaselineGroupId
-- 07/23/12  s.oneal  3.4      Modified to use a list of change change request details
--                             instead of a list of change requests
-- =============================================================
CREATE FUNCTION [Baseline].[fnValidateParameters] (
	@ChangeRequestDetailIdTable G2.GenericUniqueIntTable READONLY,
	@By int,
	@As int,
	@Action varchar(20)
) RETURNS varchar(100)
AS
BEGIN

	DECLARE @BaselineCRTypeGroupId int = Baseline.fnGetBaselineGroupId(), -- Baseline
		@ErrorMsg nvarchar(100) = NULL, @CRDId int = NULL;
	
	--Ensure at least one change request was passed in
	IF (SELECT COUNT(*) FROM @ChangeRequestDetailIdTable) = 0
	BEGIN
		SET @ErrorMsg = 'Error, No change request detail id(s) were received for Baseline' + @Action;
	END		
	ELSE --Ensuring the user modifying the records exists
	BEGIN
		IF	@By IS NULL OR NOT EXISTS(SELECT 1 FROM Security.Person WHERE Id=@By)
		BEGIN
			SET @ErrorMsg = 'Error, invalid ' + @Action + 'edBy user id (' + COALESCE(CONVERT(varchar, @By),'Null') + ') for Baseline ' + @Action; 
		END
		ELSE --Ensuring the user advancing the records as exists
		BEGIN
			IF @As IS NULL OR NOT EXISTS(SELECT 1 FROM Security.Person WHERE Id=@As)
			BEGIN
				SET @ErrorMsg = 'Error, invalid ' + @Action + 'edAs user id (' + COALESCE(CONVERT(varchar, @As),'Null') + ') for Baseline ' + @Action;
			END
			ELSE --Ensure the change request detail records are baseline components and not processed or rejected
			BEGIN
                IF @Action = 'Recall'
                BEGIN
					-- Removed validation, validation checks are done in the code
					-- and the SP can't throw an error in this case
					SET @ErrorMsg = NULL;
				END 
				ELSE
				BEGIN
				
					SELECT TOP 1 @CRDId = crd.Id
					FROM @ChangeRequestDetailIdTable crdt
					INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = crdt.Id
					INNER JOIN G2.ChangeRequestType crt ON crt.Id = crd.ChangeRequestTypeId
					INNER JOIN G2.vwChangeRequestDetailStatusFinalStates fcrs ON crd.ChangeRequestDetailStatusId = fcrs.ChangeRequestDetailStatusId			   
					WHERE crt.ChangeRequestTypeGroupId = @BaselineCRTypeGroupId;
				 			      
					IF @CRDId IS NOT NULL
					BEGIN
						SET @ErrorMsg = 'Error, invalid change request id (' + COALESCE(CONVERT(varchar, @CRDId), '') + ') for Baseline ' + @Action;
					END
					
			    END
			END
		END
	END
			
	RETURN(@ErrorMsg);
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRoomAccessControlChallengesAndAnswers]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/20/2012
-- Release:     3.7
-- Description: Returns room access control challenges and answers for a given Room.
--
-- mm/dd/yyyy   Name        Release  Description 
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRoomAccessControlChallengesAndAnswers](
	@RoomId INT
)
RETURNS @RoomAccessControlChallengeAndAnswerTable TABLE (
	RoomId INT,
	RoomAccessControlChallengeId INT,
	RoomAccessControlChallengeDescription NVARCHAR(500),
	RoomAccessControlChallengeSortOrder INT,
	IsIncludedInd BIT
)
AS
BEGIN

	INSERT @RoomAccessControlChallengeAndAnswerTable
	SELECT 
		R.Id RoomId, 
		F.Id RoomAccessControlChallengeId, F.[Description] RoomAccessControlChallengeDescription, F.SortOrder RoomAccessControlChallengeSortOrder,
		1 IsIncludedInd
	FROM Inventory.Room R
	INNER JOIN SPA.RoomRoomAccessControlChallengeXref X ON X.RoomId = R.Id
	INNER JOIN SPA.RoomAccessControlChallenge F ON F.Id = X.RoomAccessControlChallengeId
	WHERE R.Id = @RoomId
	
	UNION

	SELECT 
		R.Id RoomId, 
		F.Id RoomAccessControlChallengeId, F.[Description] RoomAccessControlChallengeDescription, F.SortOrder RoomAccessControlChallengeSortOrder,
		0 IsIncludedInd
	FROM Inventory.Room R
	CROSS JOIN SPA.RoomAccessControlChallenge F 
	WHERE F.Id NOT IN (
		SELECT X.RoomAccessControlChallengeId
		FROM SPA.RoomRoomAccessControlChallengeXref X
		WHERE X.RoomId = @RoomId
	)
	AND R.Id = @RoomId

	ORDER BY F.SortOrder

	RETURN
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestSubmissionNotificationId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     02/13/2017 
-- Release:     7.5
-- Description: Returns Funding Request Submission Notification Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestSubmissionNotificationId] ()
RETURNS INT
AS
BEGIN
  RETURN 27; -- Funding Request Submission Notification Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLocalAFPCreationStatus]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the current Local AFP Creation Status.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLocalAFPCreationStatus]
(
@SubSystemId int
)
RETURNS
nvarchar(100)
AS
BEGIN
	DECLARE @status nvarchar(100);
	
	SELECT @status = CONVERT(nvarchar(100), value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey = 'Local AFP Creation Status'
	AND SubSystemId = @SubSystemId;

	RETURN @status;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSCommunicationChallengesAndAnswers]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/14/2012
-- Release:     3.7
-- Description: Returns RMS systeam Communications and answers for a given RMS.
--
-- mm/dd/yy  Name        Release  Description 
-- 09/03/14  Strickland  5.2.1    Update to include SPA Element WBS
-- 03/30/16  Strickland  7.0      Remove SPA Element WBS
-- =============================================================
CREATE FUNCTION [SPA].[fnGetRMSCommunicationChallengesAndAnswers](
    @RMSId INT
)
RETURNS @RMSCommunicationChallengeAndAnswerTable TABLE (
    RMSId INT,
    RMSCommunicationChallengeId INT,
    RMSCommunicationChallengeDescription NVARCHAR(500),
    RMSCommunicationChallengeSortOrder INT,
    IsIncludedInd BIT
)
AS
BEGIN

    INSERT @RMSCommunicationChallengeAndAnswerTable
    SELECT 
        R.Id RMSId, 
        F.Id RMSCommunicationChallengeId, F.[Description] RMSCommunicationChallengeDescription, F.SortOrder RMSCommunicationChallengeSortOrder,
        1 IsIncludedInd
    FROM SPA.RMS R
    INNER JOIN SPA.RMSRMSCommunicationChallengeXref X ON X.RMSId = R.Id
    INNER JOIN SPA.RMSCommunicationChallenge F ON F.Id = X.RMSCommunicationChallengeId
    WHERE R.Id = @RMSId
    
    UNION

    SELECT 
        R.Id RMSId, 
        F.Id RMSCommunicationChallengeId, F.[Description] RMSCommunicationChallengeDescription, F.SortOrder RMSCommunicationChallengeSortOrder,
        0 IsIncludedInd
    FROM SPA.RMS R
    CROSS JOIN SPA.RMSCommunicationChallenge F 
    WHERE F.Id NOT IN (
        SELECT X.RMSCommunicationChallengeID
        FROM SPA.RMSRMSCommunicationChallengeXref X
        WHERE X.RMSId = @RMSId
    )
    AND R.Id = @RMSId

    ORDER BY F.SortOrder

    RETURN
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetMapSecondaryAssetGroupTypeId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     02/20/2018
-- Release:     
-- Description: Returns the id for the MDI MAP Secondary Group Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetMapSecondaryAssetGroupTypeId] ()
RETURNS INT
BEGIN
	
	RETURN 2; -- MAP Secondary Asset Group Type Id

END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetMapPrimaryAssetGroupTypeId]'
GO

-- =============================================================
-- Author:      Byron Roland
-- Created:     02/20/2018
-- Release:     
-- Description: Returns the id for the MDI MAP Primary Group Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetMapPrimaryAssetGroupTypeId] ()
RETURNS INT
BEGIN
	
	RETURN 1; -- MAP Primary Asset Group Type Id

END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetTrainingChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     02/18/2014
-- Release:     5.0
-- Description: Retrieves the Training change request type group id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetTrainingChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
	RETURN 13; -- Training
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetScheduledParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Scheduled ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetScheduledParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 4; -- Scheduled
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetAttendedParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Attended ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetAttendedParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 5; -- Attended
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetCompletedEventStatusId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the Completed EventStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetCompletedEventStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 2; -- Completed
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetHQAFPCreationStatus]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the current HQ AFP Creation Status.
--
-- mm/dd/yy  Name        Release  Description
-- 01/09/14  j.borgers   5.0      Added SubSystemId
-- 09/23/15  strickland  6.3      Added call to Financial.fnGetHQAFPCreationStatusParmKey()
-- 04/05/17  JDR         8.0      Changed reference to fnGetHQAFPCreationStatusParmKey upon switch to G2 schema
-- =============================================================
CREATE FUNCTION [Financial].[fnGetHQAFPCreationStatus]
(
@SubSystemId int
)
RETURNS
nvarchar(100)
AS
BEGIN
	DECLARE 
        @status nvarchar(100),
        @hqAFPCreationStatusParmKey VARCHAR(50) = G2.fnGetHQAFPCreationStatusParmKey();
	
	SELECT @status = CONVERT(nvarchar(100), value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey = @hqAFPCreationStatusParmKey
	AND SubSystemId = @SubSystemId;

	RETURN @status;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnCalcDistanceBetween]'
GO
-- =============================================
-- Author:		Dan Fetzer
-- Create date: 4/1/2011
-- Description:	Calculates distance between two lat, longs.
--  NOTE: uses formulas which account for curvature
--    of the earth. Adapted from source below.
--  Source: http://blog.wekeroad.com/2007/08/30/linq-and-geocoding/
-- =============================================
CREATE FUNCTION [Inventory].[fnCalcDistanceBetween] 
(
	@lat1 decimal(18,10),
	@long1 decimal(18,10),
	@lat2 decimal(18,10),
	@long2 decimal(18,10)
)
RETURNS real
AS
BEGIN
	DECLARE @dLat1InRad as float(53);

	if isnull(@lat1,0) = 0 or isnull(@lat2,0) = 0
	  or isnull(@lat2,0) = 0 or isnull(@long2,0) = 0
		Return null;	
	
	
	SET @dLat1InRad = @Lat1 * (PI()/180.0);
	DECLARE @dLong1InRad as float(53);
	SET @dLong1InRad = @Long1 * (PI()/180.0);
	DECLARE @dLat2InRad as float(53);
	SET @dLat2InRad = @Lat2 * (PI()/180.0);
	DECLARE @dLong2InRad as float(53);
	SET @dLong2InRad = @Long2 * (PI()/180.0);

	DECLARE @dLongitude as float(53);
	SET @dLongitude = @dLong2InRad - @dLong1InRad;
	DECLARE @dLatitude as float(53);
	SET @dLatitude = @dLat2InRad - @dLat1InRad;
	/* Intermediate result a. */
	DECLARE @a as float(53);
	SET @a = SQUARE (SIN (@dLatitude / 2.0)) + COS (@dLat1InRad)
					 * COS (@dLat2InRad)
					 * SQUARE(SIN (@dLongitude / 2.0));
	/* Intermediate result c (great circle distance in Radians). */
	DECLARE @c as real;
	SET @c = 2.0 * ATN2 (SQRT (@a), SQRT (1.0 - @a));
	DECLARE @kEarthRadius as real;
	SET @kEarthRadius = 3956.0;   -- miles;
	/* SET @kEarthRadius = 6376.5;        kilometers */

	DECLARE @dDistance as real;
	SET @dDistance = @kEarthRadius * @c;

	return (@dDistance);

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetLeasNearSite]'
GO
-- ====================================================================================================================
-- Author:		Dan Fetzer
-- Created:		03/31/2011
-- Description:	Function to retrieve LEAs near a Site.
--
-- Parameters:
--    @siteId -	Identifies the site where you need to find nearby Law Enforcement Agencies.
--    @includeMsa - If 1, broadens the search to include any agencies within a metropolitan statistical area of the 
--                  location.
--    @maxRadiusMiles - If > 0, the results will be expanded to include local agencies within the specified mileage 
--                      radius. This filter will not apply to state agencies.
--
--  Results returned:
--     The function returns results based upon the input parameters.
--
--     First, the function will retrieve the site and determine
--     its location state, county, city, and lat, long coordinates.
--     If the @includeMSA = 1, the function will determine the
--			larger metropolitan area code (FipsMsaCode) that the
--			site may be contained within.  (The site may not be part of
--			a larger area).
--		The results will include:
--			The state wide organization (e.g., State Police)
--			Any local LEAs whose jurisdiction is within the specified
--				city or county of the site.
--			If @includeMSA =1 parameter is specified, 
--				the results will also include any LEAs within the larger
--				metropolitan area.
--			If the @maxRadius parameter is specified,
--				the results will be expanded to include
--				sites that are within the specified mileage radius.
--        
-- Modification History
-- By	Date		Description
-- DTF	10/28/2011	Revise logic to search on building counties and
--					geo-coordinates, instead of site counties and
--					geo-coordinates.
-- JNW  11/29/2011  Added brief comments for clarity. Reformatted for readability. Added checks for AgencyTypeId 6 
--                  and 7 when populating return table with local LEA's.
-- JNW  11/30/2011  Added check for CountryId to each WHERE clause to ensure we only return LEA's that belong to the
--                  same country as the specified @siteId.
-- ====================================================================================================================
CREATE FUNCTION [Inventory].[fnGetLeasNearSite] 
(
    @siteId INT, 
    @includeMsa BIT,
    @maxRadiusMiles REAL 
)
RETURNS @RetTable TABLE 
(
    LeaId               INT, 
    AgencyName          VARCHAR(150),
    City                VARCHAR(50),
    County              VARCHAR(50),
    StateCode           VARCHAR(2),
    GovernmentTypeId    INT,
    AgencyTypeId        INT,
    SpecialFunction     VARCHAR(2),
    SpecialJurisdiction VARCHAR(2),
    FipsMsaCode         VARCHAR(4),
    DistFromSiteMiles   REAL
)
AS
BEGIN

    -- Get CountryId of the site. We'll use this to restrict LEA's returned to those within the same
    -- country of the specified @siteId.
    DECLARE @countryId INT;
    SELECT @countryId = CountryId FROM G2.[Site] WHERE Id = @siteId;
    
    -- This table used to hold a list of buildings associated with the @siteId.
    DECLARE @BldgList AS TABLE
    (
        CountyId        INT,
        StateId         INT,
        CountryId       INT,
        City            VARCHAR(50),
        NearestCity     NVARCHAR(50),
        ZipCode         VARCHAR(10),
        Latitude        DECIMAL(18,10),
        Longitude       DECIMAL(18,10)
    );

    -- This table used to hold a list of Metropolitan Statistical Area (MSA) codes for the @siteId.
    DECLARE @FipsMsaList as TABLE
    (
        FipsMsaCode varchar(4)
    );

    -------------------------------------------------------------------------------
    -- Populate our list of buildings for this site.
    -------------------------------------------------------------------------------
    INSERT INTO @BldgList 
    (
        CountyId, 
        StateId,
        CountryId, 
        Latitude, 
        Longitude, 
        City, 
        NearestCity, 
        ZipCode
    )
    SELECT DISTINCT 
        b.CountyId, 
        b.StateId, 
        b.CountryId,
        b.Latitude, 
        b.Longitude, 
        b.City, 
        b.NearestCity, 
        b.ZipCode
    FROM 
        G2.Building b
    WHERE 
        b.SiteId = @siteId;

    -------------------------------------------------------------------------------
    -- Populate our list of MSA codes if @includeMsa is set.
    -------------------------------------------------------------------------------
    IF (@includeMsa = 1)
        BEGIN
        INSERT INTO @FipsMsaList 
        (
            FipsMsaCode
        )
        SELECT DISTINCT 
            l.FipsMsaCode
        FROM 
            Inventory.LawEnforcementAgency l
            INNER JOIN @BldgList b ON l.CountyId = b.CountyId
        WHERE 
            l.AgencyTypeId IN(0,1,2,3)
            AND LTRIM(ISNULL(l.SpecialFunction,'')) IN('','2','4','6')
            AND LTRIM(ISNULL(l.SpecialJurisdiction,'')) IN('','88','10','12','13','41')
            AND ISNULL(l.FipsMsaCode,'') <> ''
            AND l.CountryId = @countryId;
        END

    -------------------------------------------------------------------------------
    -- Populate our return table with local LEAs.
    -------------------------------------------------------------------------------
    INSERT INTO @RetTable 
    (
        LeaId, 
        AgencyName, 
        City, 
        County, 
        StateCode,
        GovernmentTypeId, 
        AgencyTypeId, 
        SpecialFunction,
        SpecialJurisdiction, 
        FipsMsaCode, 
        DistFromSiteMiles
    )
    SELECT 
        l.Id, 
        l.Name, 
        l.City, 
        l.County, 
        s.Abbreviation AS StateCode,
        l.GovernmentTypeId, 
        l.AgencyTypeId, 
        LTRIM(l.SpecialFunction), 
        l.SpecialJurisdiction, 
        l.FipsMsaCode, 
        MIN(Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.Latitude, l.Longitude))
    FROM 
        Inventory.LawEnforcementAgency l 
        LEFT JOIN G2.[State] s ON s.Id = l.StateId 
        INNER JOIN Inventory.LawEnforcementAgencyCountyXref x ON x.LeaId = l.Id
        INNER JOIN @BldgList b ON b.CountyId = l.CountyId
                   OR (
                          ISNULL(b.ZipCode,'') <> '' 
                          AND (
                                  LEFT(b.ZipCode,5) BETWEEN l.ZipCodeLowest AND l.ZipCodeHighest 
                                  OR LEFT(b.zipcode,5) = l.ZipCode 
                              )
                      )
    WHERE 
        l.AgencyTypeId IN(0,1,2,3,6,7,8,9)
        AND LTRIM(ISNULL(l.SpecialFunction,'')) IN('','2','4','6')
        AND LTRIM(ISNULL(l.SpecialJurisdiction,'')) IN('','88','10','12','13','41')
        AND l.CountryId = @countryId
    GROUP BY 
        l.Id, 
        l.Name, 
        l.City, 
        l.County, 
        s.Abbreviation,
        l.GovernmentTypeId, 
        l.AgencyTypeId, 
        LTRIM(l.SpecialFunction), 
        l.SpecialJurisdiction, 
        l.FipsMsaCode, 
        l.ZipCode, 
        l.ZipCodeLowest, 
        l.ZipCodeHighest;

    -------------------------------------------------------------------------------
    -- Populate our return table with state LEAs.
    -------------------------------------------------------------------------------
    INSERT INTO @RetTable 
    (
        LeaId, 
        AgencyName, 
        City, 
        County, 
        StateCode,
        GovernmentTypeId, 
        AgencyTypeId, 
        SpecialFunction,
        SpecialJurisdiction, 
        FipsMsaCode, 
        DistFromSiteMiles
    )
    SELECT 
        l.Id, 
        l.Name, 
        l.City, 
        l.County, 
        s.Abbreviation as StateCode,
        l.GovernmentTypeId, 
        l.AgencyTypeId, 
        LTRIM(l.SpecialFunction), 
        l.SpecialJurisdiction, 
        l.FipsMsaCode,
        MIN(Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.Latitude, l.Longitude))
    FROM 
        Inventory.LawEnforcementAgency l 
        LEFT JOIN G2.[State] s ON l.StateId = s.Id
        INNER JOIN @BldgList b ON l.StateId = b.StateId
    WHERE 
        l.GovernmentTypeId = 0 
        AND l.AgencyTypeId = 5
        AND l.CountryId = @countryId
    GROUP BY 
        l.Id, 
        l.Name, 
        l.City, 
        l.County, 
        s.Abbreviation,
        l.GovernmentTypeId, 
        l.AgencyTypeId, 
        LTRIM(l.SpecialFunction), 
        l.SpecialJurisdiction, 
        l.FipsMsaCode, 
        l.ZipCode, 
        l.ZipCodeLowest, 
        l.ZipCodeHighest
    ORDER BY 
        MIN(Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.Latitude, l.Longitude));

    -------------------------------------------------------------------------------
    -- Populate our return table with LEAs in the same MSA.
    -------------------------------------------------------------------------------
    IF (@includeMsa = 1) 
        BEGIN
            INSERT INTO @RetTable 
            (
                LeaId, 
                AgencyName, 
                City, 
                County, 
                StateCode,
                GovernmentTypeId, 
                AgencyTypeId, 
                SpecialFunction,
                SpecialJurisdiction, 
                FipsMsaCode, 
                DistFromSiteMiles
            )
            SELECT 
                l.Id, 
                l.Name, 
                l.City, 
                l.County, 
                s.Abbreviation AS StateCode,
                l.GovernmentTypeId, 
                l.AgencyTypeId, 
                LTRIM(l.SpecialFunction), 
                l.SpecialJurisdiction, 
                l.FipsMsaCode, 
                MIN(Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.latitude, l.longitude))
            FROM 
                Inventory.LawEnforcementAgency l 
                LEFT JOIN G2.State s on l.StateId = s.Id, @BldgList b
            WHERE 
                ISNULL(l.FipsMsaCode,'') <> ''
                AND EXISTS(SELECT NULL FROM @FipsMsaList ml WHERE ml.FipsMsaCode = l.FipsMsaCode)
                AND NOT EXISTS(SELECT NULL FROM @RetTable rt WHERE rt.LeaId = l.Id)
                AND l.AgencyTypeId IN(0,1,2,3,8,9)
                AND LTRIM(ISNULL(SpecialFunction,'')) IN('','2','4','6')
                AND LTRIM(ISNULL(SpecialJurisdiction,'')) IN('','88','10','12','13','41')
                AND l.CountryId = @countryId
            GROUP BY 
                l.Id, 
                l.Name, 
                l.City, 
                l.County, 
                s.Abbreviation,
                l.GovernmentTypeId, 
                l.AgencyTypeId, 
                LTRIM(l.SpecialFunction), 
                l.SpecialJurisdiction, 
                l.FipsMsaCode, 
                l.ZipCode, 
                l.ZipCodeLowest, 
                l.ZipCodeHighest; 
        END

    -------------------------------------------------------------------------------
    -- Populate our return table with LEAs in the specified radius.
    -------------------------------------------------------------------------------
    IF ISNULL(@MaxRadiusMiles,0) > 0
        BEGIN
            INSERT INTO @RetTable 
            (
                LeaId, 
                AgencyName, 
                City, 
                County, 
                StateCode,
                GovernmentTypeId, 
                AgencyTypeId, 
                SpecialFunction,
                SpecialJurisdiction, 
                FipsMsaCode, 
                DistFromSiteMiles
            )
            SELECT 
                l.Id, 
                l.Name, 
                l.City, 
                l.County, 
                s.Abbreviation as StateCode,
                l.GovernmentTypeId, 
                l.AgencyTypeId, 
                LTRIM(l.SpecialFunction), 
                l.SpecialJurisdiction, 
                l.FipsMsaCode, 
                MIN(Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.Latitude, l.Longitude))
            FROM 
                Inventory.LawEnforcementAgency l 
                LEFT JOIN G2.[State] s ON l.StateId = s.Id, @BldgList b
            WHERE 
                Inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, l.Latitude, l.Longitude) <= @maxRadiusMiles	
                AND NOT EXISTS(SELECT NULL FROM @RetTable rt WHERE rt.LeaId = l.Id)
                AND l.AgencyTypeId IN(0,1,2,3,8,9)
                AND LTRIM(ISNULL(SpecialFunction,'')) IN('','2','4','6')
                AND LTRIM(ISNULL(SpecialJurisdiction,'')) IN('','88','10','12','13','41')
                AND l.CountryId = @countryId
            GROUP BY 
                l.Id, 
                l.Name, 
                l.City, 
                l.County, 
                s.Abbreviation,
                l.GovernmentTypeId, 
                l.AgencyTypeId, 
                LTRIM(l.SpecialFunction), 
                l.SpecialJurisdiction, 
                l.FipsMsaCode, 
                l.ZipCode, 
                l.ZipCodeLowest, 
                l.ZipCodeHighest;
        END

    -- Return @RetTable.
    RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementTypeIdYesNo]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/06/2017
-- Release:     8.3
-- Description: Retrieves the id of the SubElementType of Yes/No
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementTypeIdYesNo] ()
RETURNS INT
AS
BEGIN
	RETURN 1; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementTypeIdText]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/06/2017
-- Release:     8.3
-- Description: Retrieves the id of the SubElementType of Text
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementTypeIdText] ()
RETURNS INT
AS
BEGIN
	RETURN 4; 
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNFWACreationStatus]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/4/2012
-- Release:     3.7
-- Description: Retrieves the current NFWA Creation Status.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNFWACreationStatus]
(
@SubSystemId int
)
RETURNS
nvarchar(100)
AS
BEGIN
	DECLARE @status nvarchar(100);
	
	SELECT @status = CONVERT(nvarchar(100), value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey = 'NFWA Creation Status'
	AND SubSystemId = @SubSystemId;

	RETURN @status;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdResponse]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/1/2017
-- Release:     8.4
-- Description: Retrieves the id for Response
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdResponse] ()
RETURNS INT
AS
BEGIN
	RETURN 23; 
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbySPATeamLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by SPA Team Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbySPATeamLead] ()
RETURNS int
AS
BEGIN
	RETURN 5; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbySPAPortfolioManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by SPA Portfolio Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbySPAPortfolioManager] ()
RETURNS int
AS
BEGIN
	RETURN 20; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbySPALabLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by SPA Lab Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbySPALabLead] ()
RETURNS int
AS
BEGIN
	RETURN 14; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetCountryAssessmentsSubmittedTo]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     4/4/2017
-- Release:     8.0
-- Description: Converted SPA.fnGetCountryAssessmentSubmittedToPersonName to a table-valued function
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetCountryAssessmentsSubmittedTo] 
(
	@IncludeParensInd BIT = 1
)
RETURNS @Persons TABLE
(
	CountryAssessmentId INT,
	Names VARCHAR(MAX)
)
AS
BEGIN
    -- debug
    --DECLARE
	    --@CountryAssessmentId INT = (SELECT ca.Id FROM SPA.CountryAssessment ca WHERE ca.CountryId = 148), --116--173--161--164--165--154--147--148
	    --@IncludeParensInd BIT = 0;
    -- /debug

	DECLARE @RoleId INT = -1,
			@RoleIdSPAProtectTechnicalLead INT = [Security].fnGetSPAProtectTechnicalLeadRoleId(),
			@RoleIdSPAPortfolioManagerPrimary INT = [Security].fnGetSPAPortfolioManagerPrimaryRoleId(),
			@RoleIdSPAPortfolioManagerSecondary INT = [Security].fnGetSPAPortfolioManagerSecondaryRoleId(),
			@RoleIdSPALabLead INT = [Security].fnGetSPALabLeadRoleId(),
			@RoleIdSPATeamLead INT = [Security].fnGetSPATeamLeadRoleId(),
			@SPAStatusIdSubmittedtoSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
			@SPAStatusIdSubmittedtoSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
			@SPAStatusIdSubmittedtoSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
			@SPAStatusIdSubmittedtoSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead();
        
	DECLARE @temp TABLE
	(
		CountryAssessmentId INT,
		ProgramWorkBreakdownStructureId INT,
		PortfolioWorkBreakdownStructureId INT,
		SPATeamId INT,
		SPAStatusId INT NULL,
		RoleId INT NULL
	);

	INSERT INTO @temp (CountryAssessmentId, ProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SPATeamId, SPAStatusId, RoleId)
	SELECT ca.Id AS CountryAssessmentId, vcwbs.ProgramWorkBreakdownStructureId, vcwbs.PortfolioWorkBreakdownStructureId, st.Id AS SPATeamId, ca.SPAStatusId,
		CASE WHEN ca.SPAStatusId = @SPAStatusIdSubmittedtoSPATeamLead THEN @RoleIdSPATeamLead
			 WHEN ca.SPAStatusId = @SPAStatusIdSubmittedtoSPAPortfolioManager THEN @RoleIdSPAPortfolioManagerPrimary
			 WHEN ca.SPAStatusId = @SPAStatusIdSubmittedtoSPALabLead THEN 
				  CASE WHEN SLLCount.SLLCount > 0 THEN @RoleIdSPALabLead 
					   ELSE @RoleIdSPAPortfolioManagerPrimary 
				  END
			 WHEN ca.SPAStatusId = @SPAStatusIdSubmittedtoSPAProtectTechnicalLead THEN @RoleIdSPAProtectTechnicalLead
  		END AS RoleId
	FROM SPA.CountryAssessment ca
	INNER JOIN G2.Country c ON c.Id = ca.CountryId
	INNER JOIN G2.WorkBreakdownStructureCountryXref wbscx ON wbscx.CountryId = c.Id
    INNER JOIN SPA.vwCountryWorkBreakdownStructure vcwbs ON vcwbs.CountryId = c.Id
    LEFT JOIN SPA.CountryAssessmentSPATeamXref castx ON castx.CountryAssessmentId = ca.Id
	LEFT JOIN SPA.SPATeam st ON st.Id = castx.SPATeamId
    LEFT JOIN 
	(
		-- Get SPA Lab Lead count for the SPA team
		SELECT SPATeamId, COUNT(*) SLLCount
		FROM [Security].PersonRole
		WHERE RoleId = @RoleIdSPALabLead
		GROUP BY SPATeamId
	) SLLCount ON SLLCount.SPATeamId = castx.SPATeamId;

    -- Account for any secondary roles not inserted above
	INSERT INTO @temp (CountryAssessmentId, ProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SPATeamId, SPAStatusId, RoleId)
    SELECT t.CountryAssessmentId, t.ProgramWorkBreakdownStructureId, t.PortfolioWorkBreakdownStructureId, t.SPATeamId, t.SPAStatusId, @RoleIdSPAPortfolioManagerSecondary
    FROM @temp t
    WHERE t.RoleId = @RoleIdSPAPortfolioManagerPrimary;

	DECLARE @PersonNameTable TABLE
	(
		CountryAssessmentId INT,
        LastName VARCHAR(50),
        FirstName VARCHAR(50)
    );
				
	INSERT @PersonNameTable (CountryAssessmentId, LastName, FirstName)

	SELECT DISTINCT t.CountryAssessmentId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.SPATeamId = pr.SPATeamId 
	WHERE t.RoleId IN (@RoleIdSPATeamLead, @RoleIdSPALabLead)
			
	UNION 

	SELECT DISTINCT t.CountryAssessmentId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.PortfolioWorkBreakdownStructureId = pr.WorkBreakdownStructureId 
	WHERE t.RoleId IN (@RoleIdSPAPortfolioManagerPrimary, @RoleIdSPAPortfolioManagerSecondary)
			
	UNION 

	SELECT DISTINCT t.CountryAssessmentId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.ProgramWorkBreakdownStructureId = pr.WorkBreakdownStructureId
	WHERE t.RoleId = @RoleIdSPAProtectTechnicalLead;


	INSERT INTO @Persons (CountryAssessmentId, Names)
	SELECT n.CountryAssessmentId, CASE WHEN @IncludeParensInd = 1 THEN '(' + LEFT(n.Names, LEN(n.Names) - 1) + ')' ELSE LEFT(n.Names, LEN(n.Names) - 1) END AS Names
	FROM 
	(
		SELECT pnt.CountryAssessmentId, 
		(
			SELECT n.LastName + ', ' + n.FirstName + '; '
			FROM @PersonNameTable n
			WHERE n.CountryAssessmentId = pnt.CountryAssessmentId
			ORDER BY n.LastName, n.FirstName
			FOR XML PATH('')
		) AS Names
		FROM @PersonNameTable pnt
		GROUP BY pnt.CountryAssessmentId
	) n;

	RETURN;			
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnGetActivityFundingReconciliationTransactionTypeId]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     08/28/2018
-- Release:     9.2
-- Description: Get reconciliation type from activity.transactiontype (Reconcile Funding)
--              
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnGetActivityFundingReconciliationTransactionTypeId]()
RETURNS INT 
AS 
BEGIN 

RETURN 3;

END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnGetActivityFundingReconciliationChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     08/27/2018
-- Release:     9.2
-- Description: Returns the change request type id for activity funding reconciliation
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnGetActivityFundingReconciliationChangeRequestTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 43; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCurrentNFWAPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the Current NFWA Period.
--
-- mm/dd/yy  Name      Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCurrentNFWAPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Current NFWA Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetCurrentLocalAFPPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the Current Local AFP Period.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetCurrentLocalAFPPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Current Local AFP Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetFileUploadTypeId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 2/19/2010
-- Description:	Returns the Id of the
--				passed Name
-- =============================================
CREATE FUNCTION [Inventory].[fnGetFileUploadTypeId] (
	@FileUploadTypeIdCode VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @FileUploadTypeId INT

	SELECT @FileUploadTypeId = Id
	FROM Inventory.FileUploadType
	WHERE IdCode = @FileUploadTypeIdCode

	RETURN COALESCE(@FileUploadTypeId, -1)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMinimumFundingAdjustmentPeriod]'
GO
-- =============================================
-- Author:		Marshall Lee
-- Create date: 04/10/2015
-- Description:	retrun period Id for given Subsystem 
-- =============================================
CREATE FUNCTION [Financial].[fnGetMinimumFundingAdjustmentPeriod]
(
@SubSystemId int
)
RETURNS 
INT
AS
BEGIN
	DECLARE @periodId INT;

	SELECT @periodId = CAST(gp.Value AS INT)
	FROM G2.G2Parameters gp 
	WHERE gp.SubSystemId = @SubSystemId 
	AND gp.ParmKey = 'Minimum Funding Adjustment Period';

	RETURN @periodId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetMaterialAttractivenessLevelId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 09/23/2010
-- Description:	Returns the Material Attractiveness
--				Level (MAL) Id
--
-- mm/dd/yyyy   Name        Release  Description 
-- 04/17/2013   Strickland  4.1      Add RMS entity type in order to get the actual room MAL
-- =============================================
CREATE FUNCTION [SPA].[fnGetMaterialAttractivenessLevelId] (
	@SPAMainId INT,
	@EntityTypeId INT,
	@RefId INT
)
RETURNS INT
AS
BEGIN
	DECLARE 
		@EntityTypeId_Site INT = Inventory.fnGetEntityTypeId('Site'),
		@EntityTypeId_PrimaryResponseForce INT = Inventory.fnGetEntityTypeId('PrimaryResponseForce'),
		@EntityTypeId_SecondaryResponseForce INT = Inventory.fnGetEntityTypeId('SecondaryResponseForce'),
		@EntityTypeId_TertiaryResponseForce INT = Inventory.fnGetEntityTypeId('TertiaryResponseForce'),
		@EntityTypeId_Building INT = Inventory.fnGetEntityTypeId('Building'),
		@EntityTypeId_Room INT = Inventory.fnGetEntityTypeId('Room'),
		@EntityTypeId_RMS INT = Inventory.fnGetEntityTypeId('RMS'),
		@MaterialAttractivenessLevelId INT = -1

	-- Get material attractiveness level
	IF @EntityTypeId IN (@EntityTypeId_Site, @EntityTypeId_PrimaryResponseForce, @EntityTypeId_SecondaryResponseForce, @EntityTypeId_TertiaryResponseForce)
		SELECT @MaterialAttractivenessLevelId = MaterialAttractivenessLevelId
		FROM SPA.vwMaterialAttractivenessLevelBySite
		WHERE SPAMainId = @SPAMainId
		AND SiteId = @RefId
	IF @EntityTypeId = @EntityTypeId_Building
		SELECT @MaterialAttractivenessLevelId = MaterialAttractivenessLevelId
		FROM SPA.vwMaterialAttractivenessLevelByBuilding
		WHERE SPAMainId = @SPAMainId
		AND BuildingId = @RefId
	IF @EntityTypeId = @EntityTypeId_Room
		SELECT @MaterialAttractivenessLevelId = MaterialAttractivenessLevelId
		FROM SPA.vwMaterialAttractivenessLevelByBuilding
		WHERE SPAMainId = @SPAMainId
		AND BuildingId = (
			SELECT BuildingId
			FROM Inventory.Room 
			WHERE Id = @RefId
		)
	IF @EntityTypeId = @EntityTypeId_RMS
		SELECT @MaterialAttractivenessLevelId = MaterialAttractivenessLevelId
		FROM SPA.vwMaterialAttractivenessLevel
		WHERE SPAMainId = @SPAMainId
		AND RoomId = (
			SELECT RoomId
			FROM SPA.RMS
			WHERE Id = @RefId
		)

	RETURN COALESCE(@MaterialAttractivenessLevelId, -1)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestProcessEnabledDisabledNotificationId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:		11/11/16
-- Release:     7.4
-- Description: Returns the id for the Funding Request Process Enabled/Disbled notification
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestProcessEnabledDisabledNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 23; 
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetReviewerRoleBySubSystemId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     8/16/2017
-- Release:     8.2
-- Description: Returns the reviewer role used by a subsystem. Review occurs at work flow level 2.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetReviewerRoleBySubSystemId]  
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @roleId INT,
			@concurPermissionId INT = Security.fnGetConcurPermissionId(),
			@yefChangeRequestTypeId INT = YearEndForecast.fnGetYearEndForecastChangeRequestTypeId(),
			@programAdminRoleId INT = Security.fnGetProgramAdministratorRoleId();

	SELECT @roleId = permR.RoleId 
	FROM Security.PermissionRole permR
	WHERE permR.SubSystemId = @SubSystemId
	AND permR.WorkflowLevel = 2
	AND permR.PermissionId = @concurPermissionId
	AND permR.ChangeRequestTypeId = @yefChangeRequestTypeId
	AND permR.RoleId <> @programAdminRoleId;

	RETURN @roleId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetSiteFacilityTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Site facility type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetSiteFacilityTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Site
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetRoomFacilityTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Room facility type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetRoomFacilityTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Room
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetBuildingFacilityTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Building facility type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetBuildingFacilityTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Building
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetUnassignedPerformerPartyId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     1/21/2015
-- Release:     5.4
-- Description: Returns the unassigned performer
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetUnassignedPerformerPartyId]()
RETURNS INT
AS 
BEGIN

	DECLARE @unassignedPerformerId INT;

	SELECT @unassignedPerformerId = bp.ReportingEntityOrganizationPartyId
	FROM Contact.BudgetPerformer bp
	WHERE bp.Name = 'Unassigned';

	RETURN @unassignedPerformerId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetOSRPStatusId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 9/24/2010
-- Description:	Returns the Id of the
--				passed Name
-- =============================================
CREATE FUNCTION [Inventory].[fnGetOSRPStatusId] (
	@OSRPStatusIdCode VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @OSRPStatusId INT

	SELECT @OSRPStatusId = Id
	FROM Inventory.OSRPStatus
	WHERE IdCode = @OSRPStatusIdCode

	RETURN COALESCE(@OSRPStatusId, -1)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetInputRoleBySubSystemId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     8/16/2017
-- Release:     8.2
-- Description: Returns the input role used by a subsystem which occurs at work flow level 1
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetInputRoleBySubSystemId]  
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @roleId INT,
			@editPermissionId INT = Security.fnGetEditPermissionId(),
			@yefChangeRequestTypeId INT = YearEndForecast.fnGetYearEndForecastChangeRequestTypeId(),
			@programAdminRoleId INT = Security.fnGetProgramAdministratorRoleId();

	SELECT @roleId = permR.RoleId 
	FROM Security.PermissionRole permR
	WHERE permR.SubSystemId = @SubSystemId
	AND permR.WorkflowLevel = 1
	AND permR.PermissionId = @editPermissionId
	AND permR.ChangeRequestTypeId = @yefChangeRequestTypeId
	AND permR.RoleId <> @programAdminRoleId
	GROUP BY permR.RoleId;

	RETURN @roleId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetStateGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the State geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetStateGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- State
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetPostalCodeGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Postal Code geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetPostalCodeGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Postal Code
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetCityGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the City geographic boundary
--   type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetCityGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- City
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnUnqiueEventRoleAssigned]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     7/17/2014
-- Release:     5.2
-- Description: Checks to see whether an EventPersonEventRoleEventTypeXref record already exists
--              for the specified EventRoleEventTypeXrefId where the ThruDate is null and and the 
--              UniqueInd column is true for a particular event.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnUnqiueEventRoleAssigned](@EventPersonEventRoleEventTypeXrefId INT)
RETURNS BIT
AS 
BEGIN

    DECLARE @returnValue BIT = 0;
    DECLARE @eventId INT;
    DECLARE @eventRoleEventTypeXrefId INT;

    SELECT @EventId = epx.EventId, @EventRoleEventTypeXrefId = eperetx.EventRoleEventTypeXrefId
    FROM Calendar.EventPersonEventRoleEventTypeXref eperetx
    INNER JOIN Calendar.EventPersonXref epx ON epx.Id = eperetx.EventPersonXrefId
    WHERE eperetx.Id = @EventPersonEventRoleEventTypeXrefId;

    IF EXISTS(
        SELECT 1
        FROM Calendar.EventPersonEventRoleEventTypeXref eperetx
        INNER JOIN Calendar.EventRoleEventTypeXref eretx ON eretx.Id = eperetx.EventRoleEventTypeXrefId
        INNER JOIN Calendar.EventPersonXref epx ON epx.Id = eperetx.EventPersonXrefId
        WHERE eretx.Id = @eventRoleEventTypeXrefId
        AND eretx.UniqueInd = 1
        AND eretx.ThruDate IS NULL
        AND epx.EventId = @eventId
        AND eperetx.Id <> @EventPersonEventRoleEventTypeXrefId
    )
    BEGIN
        SET @returnValue = 1;
    END
    
    RETURN @returnValue;
    
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNAMBChangeTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/4/2015
-- Release:     6.3
-- Description: Translate G2 Change Request Type ID into NAMB-53 Change Type ID
--
-- mm/dd/yy  Name     Release  Description
-- 03/28/17  b.roland 8.0      ChangeRequestType function renamed
-- =============================================================
CREATE FUNCTION [Report].[fnGetNAMBChangeTypeId]( @ChangeRequestTypeId AS INT )
RETURNS INT
AS
BEGIN 
    DECLARE 
        @NAMBChangeTypeId INT,
        @HQAFPChangeRequestTypeId INT = Financial.fnGetHQAFPChangeRequestTypeId(),
        @NewAppropriationChangeRequestTypeId INT = Financial.fnGetNewAppropriationTypeId(),
        @SupplementalFundingChangeRequestTypeId INT= Financial.fnGetSupplementalFundingTypeId(),
        @RemovedFundingChangeRequestTypeId INT = Financial.fnGetRemovedFundingTypeId()
        ;

    IF @ChangeRequestTypeId IN(
            @HQAFPChangeRequestTypeId,
            @NewAppropriationChangeRequestTypeId,
            @SupplementalFundingChangeRequestTypeId,
            @RemovedFundingChangeRequestTypeId
    ) BEGIN
        SET @NAMBChangeTypeId = 2;
    END;
    ELSE BEGIN
        SET @NAMBChangeTypeId = 1;
    END;
    
    RETURN @NAMBChangeTypeId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSiteResponseForces]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Site Response Forces.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSiteResponseForces] ()
RETURNS int
AS
BEGIN
	RETURN 3; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnFlexiplaceCheckCountOfNonFedsButSelected]'
GO
-- =============================================================
-- Author:      Christopher Luttrell (chris.luttrell)
-- Created:     05/19/2014
-- Release:     5.1
-- Description: Returns Count of Non-Feds with any of the Flexiplace fields marked, should be 0
--
-- mm/dd/yy  Name     Release  Description
-- 07/17/14  CGL      5.2      Refactored Name to be more logical
-- =============================================================
CREATE FUNCTION [Contact].[fnFlexiplaceCheckCountOfNonFedsButSelected]()
RETURNS int
AS 
BEGIN
   DECLARE @retval int
   SELECT @retval = COUNT(*) FROM Contact.Flexiplace F INNER JOIN Contact.Person P ON P.PartyId = F.PersonPartyId WHERE P.FederalEmployeeInd = 0 AND (F.MedicalInd = 1 OR F.SituationalInd = 1 OR F.RegularInd = 1)
   RETURN @retval
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetOrganizationMemberPartyRelationshipTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     09/27/2013
-- Release:     4.4
-- Description: Retrieves the organization member party relationship type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetOrganizationMemberPartyRelationshipTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetUSCostsTransactionTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/12/16
-- Release:     7.3
-- Description: Retrieves the transaction type for US Costs
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetUSCostsTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
    /* Debug statements
	DECLARE @SubSystemId INT = 5;
	-- Debug statements*/
	
    DECLARE @TransactionTypeId INT;

    SELECT @TransactionTypeId = tt.Id
    FROM Financial.TransactionType tt
    INNER JOIN Financial.SubSystemTransactionType sstt ON sstt.TransactionTypeId = tt.Id
    WHERE sstt.SubSystemId = @SubSystemId
    AND sstt.ActiveInd = 1
    AND tt.ShortName = 'USC';

	RETURN @TransactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetIndicatorLevelIdInitialProgress]'
GO



-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/10/2017
-- Release:     8.3
-- Description: Retrieves the id of the IndicatorLevel of Initial Progress
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetIndicatorLevelIdInitialProgress] ()
RETURNS INT
AS
BEGIN
	RETURN 1; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetIndicatorLevelIdAcceptableAchievement]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/10/2017
-- Release:     8.3
-- Description: Retrieves the id of the IndicatorLevel of Acceptable Achievement
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetIndicatorLevelIdAcceptableAchievement] ()
RETURNS INT
AS
BEGIN
	RETURN 2; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetAssessmentElementAcceptabilityStatus]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/10/2017
-- Release:     8.3
-- Description: Returns sustainability element acceptability status
--
-- mm/dd/yy  Name        Release  Description 
-- 04/30/18  Strickland  9.0      DE8255 Exclude grouping rows when determining status
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetAssessmentElementAcceptabilityStatus] (
	@AssessmentId INT,
	@ElementId INT
)
-- The return table should match the user-defined table Sustain.AssessmentElementSummaryTable
RETURNS @AssessmentElementSummaryTable TABLE (
	AssessmentId INT,
	ElementId INT,
    AcceptabilityStatus VARCHAR(50)
)
AS
BEGIN
    ---- debug
    --DECLARE
    --    @AssessmentId INT = 24612,
    --    @ElementId INT = 54;
    ---- /debug

	DECLARE
		@isAcceptableInd INT = 1,
        @indicatorLevelIdInitialProgress INT = Sustain.fnGetIndicatorLevelIdInitialProgress(),
        @indicatorLevelIdAcceptableAchievement INT = Sustain.fnGetIndicatorLevelIdAcceptableAchievement(),
		@entityTypeId INT,
		@refId INT,
        @acceptabilityStatusInd BIT,
        @subElementTypeIdGroup INT = 5;
	
	SELECT 
		@entityTypeId = a.EntityTypeId,
		@refId = a.RefId,
        @acceptabilityStatusInd = e.AcceptabilityStatusInd
	FROM Sustain.Assessment a
    INNER JOIN Sustain.Element e ON e.Id = a.ElementId
	WHERE a.Id = @AssessmentId
	AND a.ElementId = @ElementId;

	SELECT @isAcceptableInd = 0
	FROM Sustain.fnGetSubElementsAndAnswers(@AssessmentId, @ElementId, @entityTypeId, @refId) ans
	WHERE ans.SubElementIndicatorLevelId IN (@indicatorLevelIdInitialProgress, @indicatorLevelIdAcceptableAchievement)
    AND ans.SubElementTypeId <> @subElementTypeIdGroup
    AND LOWER(COALESCE(CAST(ans.SubElementAnswerAnswer AS VARCHAR), '')) <> 'yes'
    GROUP BY ans.AssessmentId, ans.ElementId;

    INSERT @AssessmentElementSummaryTable
    (
        AssessmentId,
        ElementId,
        AcceptabilityStatus
    )
    SELECT 
        @AssessmentId, 
        @ElementId, 
        CASE WHEN @acceptabilityStatusInd = 1 THEN 
            CASE WHEN @isAcceptableInd = 0 THEN 'Not Acceptable' 
            ELSE 'OK' 
            END 
        ELSE NULL 
        END;
		
	RETURN;
END;




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLabContractFeesTransactionTypeId]'
GO

-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/12/16
-- Release:     7.3
-- Description: Retrieves the transaction type for Lab Contract Fees
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLabContractFeesTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
    /* Debug statements
	DECLARE @SubSystemId INT = 5;
	-- Debug statements*/
	
    DECLARE @TransactionTypeId INT;

    SELECT @TransactionTypeId = tt.Id
    FROM Financial.TransactionType tt
    INNER JOIN Financial.SubSystemTransactionType sstt ON sstt.TransactionTypeId = tt.Id
    WHERE sstt.SubSystemId = @SubSystemId
    AND sstt.ActiveInd = 1
    AND tt.ShortName = 'LCF';

	RETURN @TransactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeId]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/09/2009
-- Description:	Returns the Id of the
--				passed Name
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeId] (
	@SPASectionTypeIdCode VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @SPASectionTypeId INT

	SELECT @SPASectionTypeId = Id
	FROM SPA.SPASectionType
	WHERE IdCode = @SPASectionTypeIdCode

	RETURN COALESCE(@SPASectionTypeId, -1)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetTotalCostCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     01/21/2016
-- Release:     6.5
-- Description: Retrieves the transaction type for Total Cost.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetTotalCostCostTransactionTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 17;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetIndirectCostChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     01/25/2013
-- Release:     6.5
-- Description: Retrieves the Indirect Cost ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetIndirectCostChangeRequestTypeId]()
RETURNS
INT
AS
BEGIN
	RETURN 31;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetProxyUpdateNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the ProxyUpdate notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetProxyUpdateNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 14; -- Proxy Update
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetSTARSAllowedCommitmentTransactionTypeId]'
GO


-- =============================================================
-- Author:      Melissa Cole
-- Created:     4/17/2013
-- Release:     4.1
-- Description: Retrieves the STARS Allowed Indirect Commitment TransactionType.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetSTARSAllowedCommitmentTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	DECLARE @transactionTypeId int;

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	WHERE sstt.SubSystemId = @SubSystemId
	AND tt.Name = 'STARS-Allowed Indirect Commitment';

	RETURN @transactionTypeId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetPartnerCostsTransactionTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/12/16
-- Release:     7.3
-- Description: Retrieves the transaction type for Partner Costs
--
-- mm/dd/yy  Name      Release  Description
-- 02/09/17  g.ogle    7.5      Added Name filter to remove ambiguity
-- =============================================================
CREATE FUNCTION [Financial].[fnGetPartnerCostsTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
    /* Debug statements
	DECLARE @SubSystemId INT = 5;
	-- Debug statements*/
	
    DECLARE @TransactionTypeId INT;

    SELECT @TransactionTypeId = tt.Id
    FROM Financial.TransactionType tt
    INNER JOIN Financial.SubSystemTransactionType sstt ON sstt.TransactionTypeId = tt.Id
    WHERE sstt.SubSystemId = @SubSystemId
    AND sstt.ActiveInd = 1
    AND tt.ShortName = 'PSC'
	AND tt.Name = 'Partner Costs';

	RETURN @TransactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetAssessmentHierarchyHierarchyId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     07/30/16
-- Release:     5.0
-- Description: Returns the hierarchyid data type for the
--   corresponding parent id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetAssessmentHierarchyHierarchyId]
(
	@ParentId INT
)
RETURNS HIERARCHYID
AS
BEGIN
	DECLARE @hierarchyId AS HIERARCHYID,
		@parentHierarchyId AS HIERARCHYID,
		@lastChildHierarchyId AS HIERARCHYID;

	IF @ParentId IS NULL
	BEGIN
		SET @hierarchyId = HIERARCHYID::GetRoot().GetDescendant((SELECT MAX(ah.AssessmentHierarchyId)
															     FROM Assessment.AssessmentHierarchy ah
																 WHERE ah.AssessmentHierarchyId.GetAncestor(1) = hierarchyid::GetRoot()), NULL)
			 
	END
	ELSE
	BEGIN   					
		-- Need to create the hierarchy_id
		SET @parentHierarchyId = (SELECT ah.AssessmentHierarchyId
								  FROM Assessment.AssessmentHierarchy ah
								  WHERE ah.Id = @ParentId);
		SET @lastChildHierarchyId = (SELECT MAX(ah.AssessmentHierarchyId)
									 FROM Assessment.AssessmentHierarchy ah
									 WHERE ah.AssessmentHierarchyId.GetAncestor(1) = @parentHierarchyId);								 
		SET @hierarchyId = @parentHierarchyId.GetDescendant(@lastChildHierarchyId, NULL);
	END
	
	RETURN(@hierarchyId)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLocalAFPPeriodAdvancementDay]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the day of the month on which the
--   Local AFP period should advance.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers  5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLocalAFPPeriodAdvancementDay]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Local AFP Period Advancement Day'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetPriorityLevel]'
GO
	
-----------------------------------------------------------------------------------------------------------------------
-- Author:		Dan Fetzer
-- Created:     7/4/2011
-- Description:	Set priority level for ART sites.
--
-- Modification History
-- Date			By			        Description
-- 2011-07-26   Dan Fetzer          Modified parameter names to be somewhat generic.
-- 2012-07-05   Nevada Williford    Formatting.
-- 2012-10-18   Nevada Williford    Renaming from fnSetPriorityLevelForArt to fnGetPriorityLevel. Adding in parameters
--                                  for @some* dates and flags. These are used to get a priority level where some but
--                                  not all milestones have been met. 
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [Calendar].[fnGetPriorityLevel] 
(
	@eventStartDate                 DATETIME,
	@lastUpgradesCompletedDate      DATETIME,
	@lastContractsAwardedDate       DATETIME,
	@lastAssessmentsCompletedDate   DATETIME,
	@firstUpgradesCompletedDate     DATETIME,
	@firstContractsAwardedDate      DATETIME,
	@firstAssessmentsCompletedDate  DATETIME,
	@allUpgradesCompletedFlag       BIT,
	@allContractsAwardedFlag        BIT,
	@allAssessmentsCompletedFlag    BIT,
	@someUpgradesCompletedFlag      BIT,
	@someContractsAwardedFlag       BIT,
	@someAssessmentsCompletedFlag   BIT
)
RETURNS INT
AS
BEGIN
	DECLARE @PriorityLevel INT
	SET @PriorityLevel = 0;
	
	IF (@lastUpgradesCompletedDate <= @eventStartDate AND @allUpgradesCompletedFlag = 1)
	    BEGIN
        
        SET @PriorityLevel = 30;
	    
	    END
	    
	ELSE IF (@firstUpgradesCompletedDate <= @eventStartDate AND @someUpgradesCompletedFlag = 1)
	    BEGIN
	    
        SET @PriorityLevel = 25;
	    
	    END
	    
	ELSE IF (@lastContractsAwardedDate <= @eventStartDate AND @allContractsAwardedFlag = 1)
	    BEGIN
	    
	    SET @PriorityLevel = 20;
	    
	    END
	    
	ELSE IF (@firstContractsAwardedDate <= @eventStartDate AND @someContractsAwardedFlag = 1)
	    BEGIN
	    
	    SET @PriorityLevel = 15;
	    
	    END
	
	ELSE IF (@lastAssessmentsCompletedDate <= @eventStartDate AND @allAssessmentsCompletedFlag = 1)
	    BEGIN
	    
	    SET @PriorityLevel = 10; 
	    
	    END
	    
	ELSE IF (@firstAssessmentsCompletedDate <= @eventStartDate AND @someAssessmentsCompletedFlag = 1)
	    BEGIN
	    
	    SET @PriorityLevel = 05; 
	    
	    END
	
	RETURN @PriorityLevel
END
	
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetEventContactPartyRoleTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     07/17/2014
-- Release:     5.2
-- Description: Retrieves the Event Contact party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetEventContactPartyRoleTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 10; -- Event Contact
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsBuildingIncludedInSPA]'
GO
-- =============================================  
-- Author: Mike Strickland
-- Modified date: 12/14/2009
-- Description: Returns 1 if building is included
--				in a SPA, else returns 0 if not.
--
-- mm/dd/yyyy   Name        Release  Description 
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================  
CREATE FUNCTION [Inventory].[fnIsBuildingIncludedInSPA] (
	@BuildingId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @IncludedInSPA BIT

	SELECT @IncludedInSPA = 
		CASE WHEN COUNT(*) = 0 THEN
			0
		ELSE 
			1
		END
	FROM SPA.SPAMainSection
	WHERE SPASectionTypeId = SPA.fnGetSPASectionTypeIdBuildingInformation()
	AND RefId = @BuildingId
	AND IsIncludedInd = 1

	-- Return 
	RETURN @IncludedInSPA
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNFWAPeriodAdvancementDay]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the day of the month on which the
--   NFWA period should advance.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers  5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNFWAPeriodAdvancementDay]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'NFWA Period Advancement Day'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSituationalMonitoring]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/19/2017
-- Release:     8.4
-- Description: Retrieves the Entity Type Id for Situational Monitoring.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSituationalMonitoring] ()
RETURNS INT
AS
BEGIN
	RETURN 20;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdOtherMonitoring]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/19/2017
-- Release:     8.4
-- Description: Retrieves the Entity Type Id for Other Monitoring.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdOtherMonitoring] ()
RETURNS INT
AS
BEGIN
	RETURN 21;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdOnsiteMonitoring]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/19/2017
-- Release:     8.4
-- Description: Retrieves the Entity Type Id for Onsite Monitoring.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdOnsiteMonitoring] ()
RETURNS INT
AS
BEGIN
	RETURN 18;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdOffsiteMonitoring]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/19/2017
-- Release:     8.4
-- Description: Retrieves the Entity Type Id for Offsite Monitoring.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdOffsiteMonitoring] ()
RETURNS INT
AS
BEGIN
	RETURN 19;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInitialFYPerformerFundingDistributionExportImportBatchTypeId]'
GO

-- =============================================================
-- Author:      Christopher Luttrell (chris.luttrell)
-- Created:     08/06/2014
-- Release:     5.2
-- Description: Retrieves the BatchType Id for the Initial FY Performer Funding Distribution Export/Import
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInitialFYPerformerFundingDistributionExportImportBatchTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 9; -- Initial FY Performer Funding Distribution Export/Import
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsJouleMilestoneCompleteForBuilding]'
GO
-- =============================================  
-- Author: Mike Strickland
-- Modified date: 12/14/2009
-- Description: Returns 1 if building is included
--				in a project with a joule milestone
--				complete, else returns 0 if not.
-- =============================================  
CREATE FUNCTION [Inventory].[fnIsJouleMilestoneCompleteForBuilding] (
	@BuildingId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @JouleMilestoneComplete BIT

	SELECT @JouleMilestoneComplete = 
		CASE WHEN COUNT(*) = 0 THEN
			0
		ELSE 
			1
		END
	--FROM G2.ProjectMilestone pm
	--INNER JOIN ProjectMilestoneSite pms ON pms.ProjectMilestoneId = pm.Id
	--WHERE pms.SiteId = @BuildingId
	FROM G2.TaskBuilding tb
	INNER JOIN G2.Task t ON t.id = tb.TaskId
	INNER JOIN G2.ProjectMilestone pm ON pm.TaskId = t.Id
	WHERE tb.BuildingId = @BuildingId
	AND pm.JouleMetricInd = 1
	AND pm.CompletedInd = 1
		
	-- Return 
	RETURN @JouleMilestoneComplete
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMinimumHQAFPPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the minimum allowed period for which
--   HQ AFPs can be processed.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetMinimumHQAFPPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Minimum HQ AFP Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsRoomIncludedInSPA]'
GO
-- =============================================  
-- Author: Mike Strickland
-- Modified date: 12/14/2009
-- Description: Returns 1 if Room is included
--				in a SPA, else returns 0 if not.
--
-- mm/dd/yyyy   Name        Release  Description 
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================  
CREATE FUNCTION [Inventory].[fnIsRoomIncludedInSPA] (
	@RoomId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @IncludedInSPA BIT

	SELECT @IncludedInSPA = 
		CASE WHEN COUNT(*) = 0 THEN
			0
		ELSE 
			1
		END
	FROM SPA.SPAMainSection
	WHERE SPASectionTypeId = SPA.fnGetSPASectionTypeIdRoomInformation()
	AND RefId = @RoomId
	AND IsIncludedInd = 1

	-- Return 
	RETURN @IncludedInSPA
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMinimumLocalAFPPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the minimum allowed period for which
--   Local AFPs can be processed.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetMinimumLocalAFPPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Minimum Local AFP Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetAssessmentCompleteNotificationId]'
GO
-- =============================================================
-- Author:    Gabriel Hanas
-- Created:   10/03/2016
-- Release:   7.3
-- Description: Returns the id for the AssessmentComplete notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================

CREATE FUNCTION [Notification].[fnGetAssessmentCompleteNotificationId]()
RETURNS INT
AS
BEGIN
    RETURN 22; --AssessmentComplete notification 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetReportingEntitiesFromUserPermissions]'
GO
-- =============================================================
-- Author:      Byron Roland
-- Created:     7/29/15
-- Release:     6.2
-- Description: Returns a table list of reporting entities that
--                a specific user has permission to access
--
-- mm/dd/yy  Name     Release  Description
-- 05/23/16  b.roland 7.1      Updated to allow NULL UserId, improve performance
-- 10/09/16  a.smith  7.3      Changed IJ to LOJ on Security.PersonRole
-- =============================================================
CREATE FUNCTION [Report].[fnGetReportingEntitiesFromUserPermissions]
(
	@UserId int = NULL,
	@SubSystemId INT,
	@ReportingEntityOrganizationPartyId INT = NULL,
	@ReportName VARCHAR(100) = NULL,
	@FilterPerformerInd BIT = 0
)
RETURNS 
@ReportingEntitiesWithPermissions TABLE 
(
	OrganizationPartyId INT
)
AS
BEGIN
	/* START DEBUG */
	--DECLARE @UserId INT = 1291,
	--		@SubSystemId INT = 5,
	--		@ReportingEntityOrganizationPartyId INT = 1320, --NULL,
	--		@ReportName VARCHAR(100) = 'Elements of Cost',
	--		@FilterPerformerInd BIT = 1;
	--DECLARE @ReportingEntitiesWithPermissions TABLE (Id INT IDENTITY(1,1) NOT NULL, OrganizationPartyId INT);
	/* END DEBUG */

	IF @FilterPerformerInd = 1
		INSERT INTO @ReportingEntitiesWithPermissions (OrganizationPartyId)
		SELECT DISTINCT p.ReportingEntityOrganizationPartyId
		FROM Security.PermissionRole prm
		LEFT OUTER JOIN Security.PersonRole pr ON pr.SubSystemId = prm.SubSystemId AND pr.RoleId = prm.RoleId
		INNER JOIN Contact.Performer p ON pr.SubSystemId = p.SubSystemId AND p.ReportingEntityOrganizationPartyId = COALESCE(pr.OrganizationPartyId, p.ReportingEntityOrganizationPartyId)
		INNER JOIN Report.ReportCatalog rc ON rc.SubSystemId = prm.SubSystemId AND rc.Id = prm.ReportCatalogId
		WHERE pr.PersonId = @UserId
		AND rc.Name = @ReportName
		AND pr.SubSystemId = @SubSystemId
		AND p.ReportingEntityOrganizationPartyId = COALESCE(@ReportingEntityOrganizationPartyId, p.ReportingEntityOrganizationPartyId)
	ELSE
		INSERT INTO @ReportingEntitiesWithPermissions (OrganizationPartyId)
		SELECT DISTINCT p.ReportingEntityOrganizationPartyId
		FROM Contact.Performer p
		WHERE p.SubSystemId = @SubSystemId
		AND p.ReportingEntityOrganizationPartyId = COALESCE(@ReportingEntityOrganizationPartyId, p.ReportingEntityOrganizationPartyId)

	/* START DEBUG */
	--SELECT * FROM @ReportingEntitiesWithPermissions;
	/* END DEBUG */

	RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[fnCheckRequestHasOnlyOnePrimaryReportingEntity]'
GO
-- =============================================================
-- Author:      Dakota Abston
-- Created:     02/20/2018
-- Release:     8.5
-- Description: Checks if more than one Reporting Entity is set as Primary for a given Request 
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Proposal].[fnCheckRequestHasOnlyOnePrimaryReportingEntity]
(
	@RequestId INT,
	@PrimaryInd INT
)
RETURNS BIT 
AS 
BEGIN 

	DECLARE @valid BIT = 1;

	SELECT @valid = 0
	FROM Proposal.RequestReportingEntityXref rrex
	WHERE rrex.RequestId = @RequestId
	AND rrex.PrimaryInd = 1
	HAVING COUNT(rrex.Id) > 1;
	
	RETURN @valid;

END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsScopeAssignedToBuilding]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/14/2009
-- Description: Returns 1 if scope assigned for the building,
--              else returns 0 if no scope is assigned.
--
-- mm/dd/yy  Name        Release  Description
-- 09/09/10  s.oneal     1.17     Modified query to use new view for active statuses
-- 06/28/12  RLA         3.3      Modified to used Baseline.taskchange since G2.TaskRequest is being deplicated
-- 10/10/14  TYH         5.3      Implemented support for WorkBreakdownStructure
-- 03/23/16  Strickland  7.0      Updated to ignore GTRI scope
-- 04/03/17  Strickland  8.0      Use functions instead of literal values for variables
--                                Added NOEXPAND hint for joined view
-- =============================================================
CREATE FUNCTION [Inventory].[fnIsScopeAssignedToBuilding] (
    @BuildingId INT
)
RETURNS BIT
AS
BEGIN
    DECLARE 
        @scopeAssigned BIT,
        @statusIdDeleted INT = G2.fnGetDeletedWorkBreakdownStructureStatusId(),
        @statusIdCancelled INT = G2.fnGetCancelledWorkBreakdownStructureStatusId(),
        @subSystemIdNA21GTRI INT = G2.fnGetNA21SubSystemId();

    -- Check to see if the ID is in the TaskBuilding table
    SELECT @scopeAssigned = CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
    FROM G2.TaskBuilding tb
         INNER JOIN G2.Task t ON t.Id = tb.TaskId
         INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsTask.Id = t.WorkBreakdownStructureId
   WHERE tb.BuildingId = @BuildingId
         AND wbsTask.StatusId <> @statusIdDeleted 
         AND wbsTask.StatusId <> @statusIdCancelled 
         AND wbsTask.SubSystemId <> @subSystemIdNA21GTRI;

    IF @scopeAssigned = 0 
    BEGIN
        -- Check to see if the ID is in the TaskChange table with an active status
        SELECT @scopeAssigned = CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END
        FROM Baseline.TaskChange tr /*G2.TaskRequest tr*/
             INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = tr.ChangeRequestDetailId
             INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH ( NOEXPAND ) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
       WHERE tr.BuildingId = @BuildingId
             AND tr.DeletedInd = 0;  
    END;

    RETURN @scopeAssigned;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMinimumNFWAPeriod]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/26/2012
-- Release:     3.7
-- Description: Retrieves the minimum allowed period for which
--   NFWAs can be processed.
--
-- mm/dd/yy  Name     Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [Financial].[fnGetMinimumNFWAPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Minimum NFWA Period'
	AND SubSystemId = @SubSystemId;

	RETURN @id
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetSpendPlansForFiscalYearSumbyWorkBreakdownStructureId]'
GO

-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     4/18/2013
-- Description: Returns pivoted spend plan commitments and costs
--				for selected fiscal year.  Sums by ProjectId
--				(not by G2PerformerId) 
--
-- mm/dd/yy  Name     Version  Description
-- 09/27/14  a.smith  5.3      Changed name from fnGetSpendPlansForFiscalYearSumbyProjectId.
--                             Modified to use 5.3 data model structure.
-- 04/24/18  g.ogle   9.0      Updated to accommodate new Spend Plan model
--                             Added SubSystemId Parameter. 
-- 05/02/18  g.ogle   9.0      Fixed alias and related qualifier for Period table
-- =============================================
CREATE FUNCTION [Report].[fnGetSpendPlansForFiscalYearSumbyWorkBreakdownStructureId](@SubSystemId INT, @FiscalYear INT)
RETURNS @SpendPlans TABLE (WorkBreakdownStructureId INT, G2PerformerId INT, FiscalYear INT,
		C10 MONEY, C11 MONEY, C12 MONEY, C01 MONEY, C02 MONEY, C03 MONEY,
		C04 MONEY, C05 MONEY, C06 MONEY, C07 MONEY, C08 MONEY, C09 MONEY,
		E10 MONEY, E11 MONEY, E12 MONEY, E01 MONEY, E02 MONEY, E03 MONEY,
		E04 MONEY, E05 MONEY, E06 MONEY, E07 MONEY, E08 MONEY, E09 MONEY
		)
AS
BEGIN

    /* Debug Declarations */
        --DECLARE @SpendPlans TABLE (
        --    WorkBreakdownStructureId INT, FiscalYear INT,
        --    C10 MONEY, C11 MONEY, C12 MONEY, C01 MONEY, C02 MONEY, C03 MONEY,
        --    C04 MONEY, C05 MONEY, C06 MONEY, C07 MONEY, C08 MONEY, C09 MONEY,
        --    E10 MONEY, E11 MONEY, E12 MONEY, E01 MONEY, E02 MONEY, E03 MONEY,
        --    E04 MONEY, E05 MONEY, E06 MONEY, E07 MONEY, E08 MONEY, E09 MONEY
        --);
        --DECLARE @FiscalYear INT = 2014, @SubSystemId INT = 5;
    /* Debug Declarations */

    INSERT INTO @SpendPlans ( WorkBreakdownStructureId, FiscalYear, C10, C11, C12, C01, C02, C03, C04, C05, C06, C07, C08, C09)
    SELECT	pvt.WorkBreakdownStructureId, pvt.FiscalYear, 
        pvt.[1] AS C10, pvt.[2] AS C11, pvt.[3] AS C12, pvt.[4]  AS C01, pvt.[5]  AS C02, pvt.[6]  AS C03, 
        pvt.[7] AS C04, pvt.[8] AS C05, pvt.[9] AS C06, pvt.[10] AS C07, pvt.[11] AS C08, pvt.[12] AS C09
    FROM (
	    SELECT sph.WorkBreakdownStructureId, p.FiscalYear, p.FiscalPeriod, spd.CommitmentForecast
	    FROM Financial.SpendPlanHeader sph
	    INNER JOIN Financial.SpendPlanDetail spd ON sph.Id = spd.SpendPlanHeaderId
        INNER JOIN G2.Period p ON p.Id = spd.PeriodId
        INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = sph.WorkBreakdownStructureId 
	    WHERE p.FiscalYear = COALESCE(@FiscalYear, p.FiscalYear) AND wbs.SubSystemId = @SubSystemId
    ) X PIVOT( SUM(X.CommitmentForecast) FOR FiscalPeriod IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] ) ) pvt




    UPDATE @SpendPlans 
    SET sp.E10 = Y.E10,
	    sp.E11 = Y.E11,
	    sp.E12 = Y.E12,
	    sp.E01 = Y.E01,
	    sp.E02 = Y.E02,
	    sp.E03 = Y.E03,
	    sp.E04 = Y.E04,
	    sp.E05 = Y.E05,
	    sp.E06 = Y.E06,
	    sp.E07 = Y.E07,
	    sp.E08 = Y.E08,
	    sp.E09 = Y.E09
    FROM @SpendPlans sp
    INNER JOIN 
    (
        SELECT pvt.WorkBreakdownStructureId, pvt.FiscalYear, 
            pvt.[1] AS E10, pvt.[2] AS E11, pvt.[3] AS E12, pvt.[4]  AS E01, pvt.[5]  AS E02, pvt.[6]  AS E03, 
            pvt.[7] AS E04, pvt.[8] AS E05, pvt.[9] AS E06, pvt.[10] AS E07, pvt.[11] AS E08, pvt.[12] AS E09
        FROM	(
            SELECT sph.WorkBreakdownStructureId, FiscalYear, FiscalPeriod, spd.CostForecast
            FROM Financial.SpendPlanHeader sph
            INNER JOIN Financial.SpendPlanDetail spd ON sph.Id = spd.SpendPlanHeaderId
            INNER JOIN G2.Period p ON p.Id = spd.PeriodId
            INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = sph.WorkBreakdownStructureId 
            WHERE FiscalYear = COALESCE(@FiscalYear, FiscalYear) AND wbs.SubSystemId = @SubSystemId
        ) X PIVOT( SUM(X.CostForecast) FOR X.FiscalPeriod IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]) ) pvt
    ) Y ON sp.WorkBreakdownStructureId = Y.WorkBreakdownStructureId AND sp.FiscalYear = Y.FiscalYear

    /* Debug Return */
    --SELECT * FROM @SpendPlans sp
    --ORDER BY WorkBreakdownStructureId, sp.FiscalYear
    /* End Debug Return */

RETURN;

END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnIsSourceIncludedInSPA]'
GO
-- =============================================  
-- Author: Mike Strickland
-- Modified date: 12/14/2009
-- Description: Returns 1 if Source is included
--				in a SPA, else returns 0 if not.
--
-- mm/dd/yyyy   Name        Release  Description 
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================  
CREATE FUNCTION [Inventory].[fnIsSourceIncludedInSPA] (
	@SourceId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @IncludedInSPA BIT

	SELECT @IncludedInSPA = 
		CASE WHEN COUNT(*) = 0 THEN
			0
		ELSE 
			1
		END
	FROM SPA.SPAMainSection
	WHERE SPASectionTypeId = SPA.fnGetSPASectionTypeIdSourceInformation()
	AND RefId = @SourceId
	AND IsIncludedInd = 1

	-- Return 
	RETURN @IncludedInSPA
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetRequestAlreadyProcessedErrorTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     05/12/2017 
-- Release:     8.0
-- Description: Returns the Request Already Processed ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetRequestAlreadyProcessedErrorTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 70; -- Request Already Processed ErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetInvalidWBSErrorTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/2/2016
-- Release:     6.5
-- Description: Returns the error type id for an invalid wbs
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetInvalidWBSErrorTypeId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 61;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidPerformerErrorTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     06/02/2016
-- Release:     7.1
-- Description: Retrieves Invalid performer ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidPerformerErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 63; -- Invalid performer
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidMonthErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Month error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidMonthErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 22; -- Invalid Month
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidFundTypeErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Fund Type error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidFundTypeErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 26; -- Invalid Fund Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidFiscalYearErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Fiscal Year error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidFiscalYearErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 23; -- Invalid Fiscal Year
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidFinancialAmountErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Financial Amount error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidFinancialAmountErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 27; -- Invalid Financial Amount
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetImproperPeriodErrorTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     04/13/2017 
-- Release:     8.0
-- Description: Returns the Improper Period ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetImproperPeriodErrorTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 68; -- Improper Period ErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetExplanationTooLongErrorTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     05/11/2017 
-- Release:     8.0
-- Description: Retrieves the Explanation Too Long Error Type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetExplanationTooLongErrorTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 69; -- Explanation Too Long ErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetDuplicateRowErrorTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/2/2016
-- Release:     6.5
-- Description: Returns the error type id for a duplicate row
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetDuplicateRowErrorTypeId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 62;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetHeartbeatNotificationId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     10/2/2017
-- Release:     8.3
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetHeartbeatNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN (SELECT n.Id
	FROM Notification.Notification n
	WHERE n.Name = 'Heartbeat Notification')
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLabLaborCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for lab labor by
--   sub-system. This assumes that there is only one lab labor
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLabLaborCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	--DECLARE @transactionTypeId int,
	--	@costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Lab Labor';

	--RETURN @transactionTypeId;
	RETURN 3;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetDeputyWorkBreakdownStructures]'
GO
-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 7/8/2011	Release 2.3.1
-- Description:	Returns project list for Deputy Cost Reports
-- Modified:
-- mm/dd/yy  Name     Release  Description 
-- 04/24/13  DNC	   4.1	   Remove NAFD references
-- 05/03/13	 DNC	   4.1	   Update to use Account tables for determining relevant projects
-- 05/07/13  DNC	   4.1     Check to see if project had any carryover.
-- 01/27/13  m.cole    5.0	   Using WorkBreakdownStructureId instead of ProjectId to connect Account
-- 09/29/14  a.smith   5.3     Modified to use WorkBreakdownStructure instead of Project; Renamed from
--                             fnGetDeputyProjects. Created local variables for hardcoded values.
-- 10/21/14  a.smith   5.3     fnGetLabLaborTransactionTypeId renamed to fnGetLabLaborCostTransactionTypeId.
-- 11/19/14  CGL       5.3     Modified via Smart Rename after creating table WorkBreakdownStructureGroups to replace the view.
-- 08/27/15  a.smith   6.2     Added local @subSystemId variable and used for parameter to *WorkTypeId function calls
-- 06/06/17  a.smith   8.1 PROD Removed @subSystemId parameter from *WorkTypeId function calls
-- =============================================
CREATE FUNCTION [Report].[fnGetDeputyWorkBreakdownStructures] 
(
	-- Add the parameters for the function here
	@DeputyOfficeId INT, 
	@FiscalYear INT,
	@FiscalPeriod INT 
)
RETURNS 
@WorkBreakdownStructureList TABLE 
(
	id INT IDENTITY(1,1) NOT NULL,
	WorkBreakdownStructureId int  
)AS
BEGIN
	/* Debug Declarations */
	--DECLARE @DeputyOfficeId INT = 211,
	--		@FiscalYear INT = 2015,
	--		@FiscalPeriod INT = 8;
	--DECLARE @WorkBreakdownStructureList TABLE (id INT IDENTITY(1,1) NOT NULL,WorkBreakdownStructureId INT);
	/* End Debug Declarations */

	DECLARE @subSystemId INT = G2.fnGetNA21SubSystemId();
	DECLARE @fundingConfigurationTypeId INT = G2.fnGetFundingConfigurationTypeId(),
	        @RemoveRadInternationalWorkTypeId INT = G2.fnGetRemoveRadInternationalWorkTypeId(@subSystemId),           -- 5
			@RemoveRadDomesticWorkTypeId INT = G2.fnGetRemoveRadDomesticWorkTypeId(@subSystemId),                     -- 6
			@ProtectNuclearInternationalWorkTypeId INT = G2.fnGetProtectNuclearInternationalWorkTypeId(@subSystemId), -- 7
			@ProtectRadInternationalWorkTypeId INT = G2.fnGetProtectRadInternationalWorkTypeId(@subSystemId),         -- 8
			@ProtectRadDomesticWorkTypeId INT = G2.fnGetProtectRadDomesticWorkTypeId(@subSystemId),                   -- 9
			@ProtectNuclearDomesticWorkTypeId INT = G2.fnGetProtectNuclearDomesticWorkTypeId(@subSystemId),           -- 13
			@ConvertWorkTypeId INT = G2.fnGetConvertWorkTypeId(@subSystemId),                                         -- 1
			@RemoveNuclearRussianOriginWorkTypeId INT = G2.fnGetRemoveNuclearRussianOriginWorkTypeId(),   -- 2
			@RemoveNuclearUSOriginWorkTypeId INT = G2.fnGetRemoveNuclearUSOriginWorkTypeId(),             -- 3
			@RemoveNuclearGapOriginWorkTypeId INT = G2.fnGetRemoveNuclearGapOriginWorkTypeId(),           -- 4
			@LabLaborTransactionTypeId INT = Financial.fnGetLabLaborCostTransactionTypeId(G2.fnGetNA21SubSystemId()),  -- 3
			@NorthAndSouthAmericaDeputyOfficeIntWBS INT = 211, @NorthAndSouthAmericaDeputyOfficeId INT = 2,
			@EuropeAndAfricaDeputyOfficeIntWBS INT = 212, @EuropeAndAfricaDeputyOfficeId INT = 3,
			@FSUAndAsiaDeputyOfficeIntWBS INT = 213, @FSUAndAsiaDeputyOfficeId INT = 4;
	IF @DeputyOfficeId = @NorthAndSouthAmericaDeputyOfficeIntWBS
	BEGIN
		INSERT INTO @WorkBreakdownStructureList  (WorkBreakdownStructureId)
		SELECT DISTINCT wbs.Id WorkBreakdownStructureId 
		FROM G2.WorkType wt
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkTypeId = wt.Id
		INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbswtx.WorkBreakdownStructureId
		-- WorkBreakdownStructure has funding
		INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbsc.WorkBreakdownStructureId = wbs.Id AND wbsc.ConfigurationTypeId = @fundingConfigurationTypeId
		INNER JOIN G2.WorkBreakdownStructureGroups vwbsg ON vwbsg.WorkBreakdownStructureId = wbs.Id
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			INNER JOIN Financial.AccountTransaction at ON a.Id = at.AccountId
			INNER JOIN Financial.TransactionType tt ON at.TransactionTypeId = tt.Id AND tt.TransactionTypeGroupId <= @LabLaborTransactionTypeId
			INNER JOIN G2.Period per ON at.PeriodId = per.Id AND per.FiscalYear = @FiscalYear AND per.FiscalPeriod <= @FiscalPeriod    
			WHERE a.WorkBreakdownStructureId = wbs.Id          
		) X 
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			INNER JOIN  Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
			WHERE a.WorkBreakdownStructureId = wbs.Id          
		) Y 
		WHERE( X.Active IS NOT NULL OR Y.Active IS NOT NULL )
		AND
		(
			(wt.Id IN (@RemoveRadInternationalWorkTypeId, 
						@RemoveRadDomesticWorkTypeId, 
						@ProtectNuclearInternationalWorkTypeId,
						@ProtectRadInternationalWorkTypeId,
						@ProtectRadDomesticWorkTypeId,
						@ProtectNuclearDomesticWorkTypeId) AND vwbsg.SecondaryGroupId = @NorthAndSouthAmericaDeputyOfficeId /* was 1*/) /* N & S America */
			OR
			(vwbsg.PrimaryGroupName LIKE 'Protect Integration%')
		);	
	END;
	IF @DeputyOfficeId = @EuropeAndAfricaDeputyOfficeIntWBS
	BEGIN
		INSERT INTO @WorkBreakdownStructureList  (WorkBreakdownStructureId)
		SELECT DISTINCT wbs.Id WorkBreakdownStructureId
		FROM G2.WorkType wt
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkTypeId = wt.Id
		INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbswtx.WorkBreakdownStructureId
		-- WorkBreakdownStructure has funding
		INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbsc.WorkBreakdownStructureId = wbs.Id AND wbsc.ConfigurationTypeId = @fundingConfigurationTypeId
		INNER JOIN G2.WorkBreakdownStructureGroups vwbsg ON vwbsg.WorkBreakdownStructureId = wbs.Id
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			INNER JOIN  Financial.AccountTransaction at ON a.Id = at.AccountId
			INNER JOIN Financial.TransactionType tt ON at.TransactionTypeId = tt.Id AND tt.TransactionTypeGroupId <= @LabLaborTransactionTypeId
			INNER JOIN G2.Period per ON at.PeriodId = per.Id AND per.FiscalYear = @FiscalYear AND per.FiscalPeriod <= @FiscalPeriod    
			WHERE a.WorkBreakdownStructureId = wbs.Id          
		) X 
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			INNER JOIN  Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
			WHERE a.WorkBreakdownStructureId = wbs.Id
		) Y 
		WHERE( X.Active IS NOT NULL OR Y.Active IS NOT NULL )
		AND
		(
			(wt.Id IN (@RemoveRadInternationalWorkTypeId, 
						@RemoveRadDomesticWorkTypeId, 
						@ProtectNuclearInternationalWorkTypeId,
						@ProtectRadInternationalWorkTypeId,
						@ProtectRadDomesticWorkTypeId,
						@ProtectNuclearDomesticWorkTypeId)
				AND vwbsg.SecondaryGroupId = @EuropeAndAfricaDeputyOfficeId /* was 2*/ /* Europe & Africa */
				AND vwbsg.PrimaryGroupName NOT LIKE 'Protect Integration%')
		OR
			wt.Id = @ConvertWorkTypeId /* Convert */
		OR
			vwbsg.PrimaryGroupName LIKE 'Fuel Dev/Qual/Fab'
			OR 
			vwbsg.PrimaryGroupName LIKE 'Mo-99'
			OR 
			vwbsg.PrimaryGroupName LIKE 'Convert Integration'
		);
	END;
	IF @DeputyOfficeId = @FSUAndAsiaDeputyOfficeIntWBS
	BEGIN
		INSERT INTO @WorkBreakdownStructureList  (WorkBreakdownStructureId)
		SELECT DISTINCT wbs.Id WorkBreakdownStructureId
		FROM G2.WorkType wt
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbswtx.WorkTypeId = wt.Id
		INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbswtx.WorkBreakdownStructureId
		-- WorkBreakdownStructure has funding
		INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbsc.WorkBreakdownStructureId = wbs.Id AND wbsc.ConfigurationTypeId = @fundingConfigurationTypeId
		INNER JOIN G2.WorkBreakdownStructureGroups vwbsg ON vwbsg.WorkBreakdownStructureId = wbs.Id
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			LEFT OUTER JOIN Financial.AccountTransaction at ON a.Id = at.AccountId
			LEFT OUTER JOIN Financial.TransactionType tt ON at.TransactionTypeId = tt.Id AND tt.TransactionTypeGroupId <= @LabLaborTransactionTypeId
			LEFT OUTER JOIN G2.Period per ON at.PeriodId = per.Id AND per.FiscalYear = @FiscalYear AND per.FiscalPeriod <= @FiscalPeriod    
			LEFT OUTER JOIN Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
			WHERE a.WorkBreakdownStructureId = wbs.Id          
		) X 
		OUTER APPLY	
		( 
			SELECT 'Y' AS Active
			FROM Financial.Account a 
			INNER JOIN  Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
			WHERE a.WorkBreakdownStructureId = wbs.Id          
		) Y 
		WHERE( X.Active IS NOT NULL OR Y.Active IS NOT NULL )
		AND
		(	
			(wt.Id IN (@RemoveRadInternationalWorkTypeId, @RemoveRadDomesticWorkTypeId, @ProtectNuclearInternationalWorkTypeId, @ProtectRadInternationalWorkTypeId,
					   @ProtectRadDomesticWorkTypeId, @ProtectNuclearDomesticWorkTypeId)
				AND vwbsg.SecondaryGroupId = @FSUAndAsiaDeputyOfficeId /* was 3*/ /* FSU & Asia */
				AND vwbsg.PrimaryGroupName NOT LIKE 'Protect Integration%')
		OR
			wt.Id IN (@RemoveNuclearRussianOriginWorkTypeId,
						@RemoveNuclearUSOriginWorkTypeId,
						@RemoveNuclearGapOriginWorkTypeId)  /* Russian-Origin, Gap Remove, US-Origin */
		OR
			vwbsg.PrimaryGroupName LIKe ('Remove Integration')
		);
	END;

	-- Debug statement
	--SELECT * FROM @WorkBreakdownStructureList wbsl
	-- Debug statement
	RETURN 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetYearEndForecastCloseReminderNotificationId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     04/10/2017 
-- Release:     8.0
-- Description: Returns the Id for the Year End Forecast Close Reminder Notification
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetYearEndForecastCloseReminderNotificationId] ()
RETURNS INT
AS
BEGIN
  RETURN 29; -- YEF Close Reminder Notification Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetTravelMovementNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the TravelMovement notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetTravelMovementNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 18; -- Travel Movement
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetTravelCostRequiredNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the TravelCostsRequired notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetTravelCostRequiredNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 17; -- Travel Costs Required
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetSPAStatusChangeSiteNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the SPAStatusChangeSite notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetSPAStatusChangeSiteNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 16; -- SPA Status Change Site
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetSPAStatusChangeCountryNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the SPAStatusChangeCountry notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetSPAStatusChangeCountryNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 15; -- SPA Status Change Country
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetJouleMilestoneCompletionNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the JouleMilestoneCompletion notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetJouleMilestoneCompletionNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 13; -- Joule Milestone Completion
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetInventoryQANoticeNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the InventoryQANotice notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetInventoryQANoticeNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 12; -- Inventory QA Notice
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetIDDSurveyUploadNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the IDDSurveyUpload notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetIDDSurveyUploadNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 11; -- IDD Survey Upload
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestWeeklyReminderNotificationId]'
GO

-- =============================================================
-- Author:      Jill Rochat
-- Created:     02/01/2017 
-- Release:     7.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestWeeklyReminderNotificationId] ()
RETURNS INT
AS
BEGIN
  RETURN 25; -- Funding Request Weekly Reminder Notification Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestMovementNotificationId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     02/06/2017 
-- Release:     7.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestMovementNotificationId] ()
RETURNS INT
AS
BEGIN
  RETURN 26; -- Funding Request Movement Notification Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestFinalNotificationId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     02/17/2017 
-- Release:     7.5
-- Description: Returns the Funding Request Final Notification Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestFinalNotificationId] ()
RETURNS INT
AS
BEGIN
  RETURN 28; -- Funding Request Final Notification Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFinPlanWeeklyReminderNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the FinPlanWeeklyReminder notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFinPlanWeeklyReminderNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 10; -- FinPlan Weekly Reminder
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFinPlanSubmissionNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the FinPlanSubmission notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFinPlanSubmissionNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 9; -- FinPlan Submission
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFinPlanMovementNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the FinPlanMovement notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFinPlanMovementNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 8; -- FinPlan Movement
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFinPlanHQAFPEnabledDisabledNotificationId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:		09/22/2015
-- Release:     6.3
-- Description: Returns the id for the FinPlan HQ AFP Process Enabled/Disbled notification
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFinPlanHQAFPEnabledDisabledNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 20; 
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFinPlanFinalNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the FinPlanFinal notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFinPlanFinalNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 7; -- FinPlan Final
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetEventParticipantStatusChangeNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the EventParticipantStatusChange notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetEventParticipantStatusChangeNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 6; -- Event Participant Status Change
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetCostSubmissionReminderNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the CostReminder notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetCostSubmissionReminderNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 5; -- Cost Reminder
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetCostSubmissionReconciliationNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the CostReconciliation notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetCostSubmissionReconciliationNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 4; -- Cost Reconciliation
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetCostSubmissionOverrunNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the CostOverrun notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetCostSubmissionOverrunNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 3; -- Cost Overrun
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetCostsProcessedNotificationId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:		09/22/2015
-- Release:     6.3
-- Description: Returns the id for the Costs Processed notification
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetCostsProcessedNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 19; 
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetBaselineMovementNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the BaselineMovement notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetBaselineMovementNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 2; -- Baseline Movement
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetBaselineFinalNotificationId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		04/01/2014
-- Release:     5.0
-- Description: Returns the id for the BaselineFinal notification.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetBaselineFinalNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 1; -- Baseline Final
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnGetEnterpriseBudgetCaseTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     01/26/2016 
-- Release:     6.5
-- Description: Returns the Enterprise BudgetCaseTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [BudgetFormulation].[fnGetEnterpriseBudgetCaseTypeId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Enterprise
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetContaminationCategoryClassificationId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/19/2018 
-- Release:     9.1
-- Description: Returns the Contamination Category ClassificationId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetContaminationCategoryClassificationId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Contamination Category
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetSiteParticipantTypeId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the 'Site' ParticipantType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetSiteParticipantTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Site
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetSiteLargestSourceOrMW]'
GO
-- =============================================
-- Author:		Dan Fetzer
-- Create date: 6/30/2011
-- Description:	Calculate largest source or MW of reactor
--				
-- Author:      Mike Strickland
-- Modified:    4/9/2012
-- Description: Updated to reflect Inventory.Room.DeletedInd.
-- =============================================
CREATE FUNCTION [Inventory].[fnGetSiteLargestSourceOrMW]
(
	-- Add the parameters for the function here
	@siteId int
)
RETURNS 
@Table_Var TABLE 
(
	-- Add the column definitions for the TABLE variable here
	DeviceTypeId int,
	MaxOperatingPowerMW numeric(18,4), -- Fissile Nuclide ID = 1, DeviceType
	MaxCurrentActivityCi numeric(18,4),
	MaxLicensedCi numeric(18,4),
	MaxActivityCi numeric(18,4),
	FissileNuclideId int,
	MaxAmountKg numeric(18,4)
)
AS
BEGIN
	DECLARE @deviceTypeId int;
	DECLARE @deviceTypeName varchar(50);
	DECLARE @maxOperatingPowerMW numeric(18,4);
	DECLARE @maxCurrentActivityCi numeric(18,4);
	DECLARE @maxLicensedCi numeric(18,4);
	DECLARE @maxActivityCi numeric(18,4);
	DECLARE @fissileNuclideId int;	
	DECLARE @maxAmountKg numeric(18,4);

	SELECT top 1 @deviceTypeId=dt.Id, @deviceTypeName=dt.Name, @maxOperatingPowerMW = MAX(src.OperatingPower)
	FROM G2.Site s 
	INNER JOIN G2.Building b ON b.SiteId = s.Id
	LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id
	LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
	LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
	WHERE src.OperatingPower > 0 
	  AND src.MaterialTypeId=2
	  AND s.Id = @siteId
	GROUP BY dt.Id, dt.Name
	ORDER BY MAX(src.OperatingPower) DESC;

	SELECT @maxLicensedCi=COALESCE(MAX(src.LicensedMaximum),0)
	FROM G2.Site s 
	INNER JOIN G2.Building b ON b.SiteId = s.Id
	LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id
	LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
	LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
	WHERE dt.UseLicensedMaximumInd = 1
	  AND s.Id = @siteId;
	
	SELECT @maxCurrentActivityCi = MAX(RadCurrentActivity)
	FROM(
		SELECT 
		s.Id SiteId,
		st.CalculateActivityInd,
		COALESCE(dt.MultipleChildrenInd, 0)MultipleChildrenInd,
		b.DeletedInd,
		COALESCE(
		-- Calculated Activity based on current date
		CASE WHEN m.Id = 1 THEN
			-- Need to calculate activity if we know source type (isotope),
			-- reported activity, and reported activity date
			Inventory.fnGetCurrentActivity(COALESCE(src.OriginalActivity, 0), 
				COALESCE(src.OriginalActivityDate, CURRENT_TIMESTAMP), 
				CURRENT_TIMESTAMP, 
				COALESCE(st.HalfLifeDays, 0))
		ELSE
			0
		END 
		,0)	RadCurrentActivity
		FROM G2.Site s 
		INNER JOIN G2.Building b ON b.SiteId = s.Id
		LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id
			AND r.DeletedInd = 0
		LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
		LEFT JOIN Inventory.MaterialType m ON m.Id = src.MaterialTypeId
		--LEFT JOIN Inventory.SourceSourceTypeXref sstx ON sstx.SourceId = src.Id
		LEFT JOIN Inventory.SourceType st ON st.Id = src.SourceTypeId
		--LEFT JOIN Inventory.SourceFissileNuclideXref sfnx ON sfnx.SourceId = src.Id
		LEFT JOIN Inventory.FissileNuclide fn ON fn.Id = src.FissileNuclideId
		LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
		WHERE COALESCE(dt.MultipleChildrenInd, 0) = 0
			AND b.DeletedInd = 0
	)r
	WHERE CalculateActivityInd = 1
	  AND SiteId = @siteId
	;
	

	SELECT TOP 1
		@maxAmountKg = MAX(COALESCE(CASE WHEN m.Id = 2 THEN src.Amount ELSE 0 END, 0)),
		@fissileNuclideId = COALESCE(src.FissileNuclideId, -1) 
	FROM G2.Site s 
	INNER JOIN G2.Building b ON b.SiteId = s.Id
	LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id
	LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
	LEFT JOIN Inventory.MaterialType m ON m.Id = src.MaterialTypeId
	--LEFT JOIN Inventory.SourceFissileNuclideXref sfnx ON sfnx.SourceId = src.Id
	LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
	--LEFT JOIN Inventory.SourceSourceTypeXref sstx ON sstx.SourceId = src.Id
	LEFT JOIN Inventory.SourceType st ON st.Id = src.SourceTypeId
	WHERE s.Id = @siteId
	  AND src.FissileNuclideId <> 1
	  AND dt.UseLicensedMaximumInd = 0
	  AND st.CalculateActivityInd = 0
	GROUP BY src.FissileNuclideId
	ORDER BY MAX(COALESCE(CASE WHEN m.Id = 2 THEN src.Amount ELSE 0 END, 0)) desc;

	SELECT TOP 1
		@maxAmountKg = MAX(NucFissileAmount),
		@fissileNuclideId = COALESCE(FissileNuclideId, -1) 
	FROM
	(
			SELECT
				s.Id SiteId,
				CASE WHEN m.Id = 2 THEN
					src.Amount
				ELSE
					0
				END NucFissileAmount,
				COALESCE(fn.Id, -1) FissileNuclideId,
				COALESCE(dt.UseLicensedMaximumInd, CONVERT(BIT, 0)) UseLicensedMaximumInd,
				COALESCE(st.CalculateActivityInd, CAST(0 AS BIT)) CalculateActivityInd
			FROM G2.Site s 
			INNER JOIN G2.Building b ON b.SiteId = s.Id
			LEFT JOIN Inventory.Room r ON r.BuildingId = b.Id
				AND r.DeletedInd = 0
			LEFT JOIN Inventory.Source src ON src.RoomId = r.Id
			LEFT JOIN Inventory.MaterialType m ON m.Id = src.MaterialTypeId
			--LEFT JOIN Inventory.SourceSourceTypeXref sstx ON sstx.SourceId = src.Id
			LEFT JOIN Inventory.SourceType st ON st.Id = src.SourceTypeId
			--LEFT JOIN Inventory.SourceFissileNuclideXref sfnx ON sfnx.SourceId = src.Id
			LEFT JOIN Inventory.FissileNuclide fn ON fn.Id = src.FissileNuclideId
			LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
			WHERE COALESCE(dt.MultipleChildrenInd, 0) = 0
			AND b.DeletedInd = 0
		UNION
			SELECT
				s.Id SiteId,
				CASE WHEN m.Id = 2 THEN
					COALESCE(src.Amount, 0)
				ELSE
					0
				END NucFissileAmount,
				-1 FissileNuclideId,
				COALESCE(dt.UseLicensedMaximumInd, CONVERT(BIT, 0)) UseLicensedMaximumInd,
				CAST(0 AS BIT) CalculateActivityInd
			FROM G2.Site s 
			INNER JOIN G2.Building b ON b.SiteId = s.Id
			INNER JOIN Inventory.Room r ON r.BuildingId = b.Id
			INNER JOIN Inventory.Source src ON src.RoomId = r.Id
			INNER JOIN Inventory.MaterialType m ON m.Id = src.MaterialTypeId
			LEFT JOIN Inventory.DeviceType dt ON dt.Id = src.DeviceTypeId
			LEFT JOIN Inventory.OperationalStatus os ON os.Id = src.OperationalStatusId
			WHERE COALESCE(dt.MultipleChildrenInd, 0) = 1
			AND b.DeletedInd = 0
			AND r.DeletedInd = 0
	)x
	WHERE SiteId = @siteId
	AND FissileNuclideId <> 1
	AND UseLicensedMaximumInd = 0
	AND CalculateActivityInd = 0
	GROUP BY FissileNuclideId
	ORDER BY MAX(NucFissileAmount)DESC

	 set @maxActivityCi = @maxCurrentActivityCi;
	 if (COALESCE(@maxactivityci,0) < COALESCE(@maxLicensedCi,0))
	   BEGIN
		 set @maxActivityCi = @maxLicensedCi;
	   END
	   
	set @maxActivityCi = COALESCE(@maxActivityCi, 0);
	set @maxOperatingPowerMW = COALESCE(@maxOperatingPowerMW,0);

	INSERT INTO @Table_Var
		(DeviceTypeId, MaxOperatingPowerMW, 
		 MaxCurrentActivityCi, MaxLicensedCi, MaxActivityCi,
		 FissileNuclideId, MaxAmountKg)
	VALUES (@deviceTypeId, @maxOperatingPowerMW,
			@maxCurrentActivityCi, @maxLicensedCi, @maxActivityCi,
			@fissileNuclideId, @maxAmountKg);
	
	RETURN 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetNearestUasi2SiteBldgs]'
GO
-----------------------------------------------------------------------------------------------------------------------
-- Author:		Dan Fetzer
-- Created:     5/6/2011
-- Description:	Returns a single row identifying the urban area that the given lat long is in, or the or the nearest 
--              one to the given lat/long.
--
-- Modification History
-- Date			By			        Description
-- 2011-10-14	Dan Fetzer	        Adapted copy of fnGetNearestUasi to use building county and lat/longs for finding 
--                                  nearest Uasi instead of less accurate site lat/longs.
-- 2012-07-05   Nevada Williford    Formatting.
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [Inventory].[fnGetNearestUasi2SiteBldgs] 
(
	@siteId INT,
	@getNearestFlag BIT
)
RETURNS 
@Table_Var TABLE 
(
	UasiAreaId INT,       -- Id of the urban area
	UasiName VARCHAR(50), -- Name of the urban area
	UasiTierId INT,       -- Tier of the urban area
	UasiCountyId INT,     -- Nearest Uasi county
	DistanceMiles REAL,   -- Miles to the nearest urban county.
	BuildingId INT	      -- Which building lat/long & county was used in calculation.
)
AS
BEGIN
	DECLARE @UasiCountyId INT;
	DECLARE @UrbanAreaId INT;
	DECLARE @UasiName VARCHAR(50);
	DECLARE @UasiTierId INT;
	DECLARE @DistMiles REAL;
	DECLARE @BuildingId INT;
	
	SET @BuildingId = NULL;	
	SET @UrbanAreaId = NULL;
	SET @UasiName = '';
	SET @UasiTierId = -1;
	SET @UasiCountyId = NULL;
	SET @DistMiles = 0;
	
	DECLARE @BldgList AS TABLE
	(
		CountyId INT,
		StateId INT,
		Latitude DECIMAL(18,10),
		Longitude DECIMAL(18,10),
		BldgId INT
	);

    
    BEGIN	

    INSERT INTO @BldgList 
    (
        CountyId, 
        StateId, 
        Latitude, 
        Longitude, 
        BldgId
    )
    SELECT 
        b.CountyId, 
        b.StateId, 
        b.Latitude, 
        b.Longitude, 
        MIN(b.id) AS BldgId
    FROM 
        g2.Building b
    WHERE 
        b.SiteId = @siteId 
        AND ISNULL(b.CountyId,0) > 0 
        AND ISNULL(b.Latitude,0) <> 0
    GROUP BY 
        b.CountyId, 
        b.StateId, 
        b.Latitude, 
        b.Longitude;


    SELECT TOP 1 
        @UasiCountyId = x.CountyId, 
        @UrbanAreaId = UrbanAreaId, 
        @BuildingId = b.BldgId
    FROM 
        Inventory.UrbanAreaCountyXref x
        INNER JOIN @BldgList b 
            ON x.CountyId = b.CountyId
        INNER JOIN Inventory.UrbanArea ua 
            ON x.UrbanAreaId = ua.id
    ORDER BY 
        ua.UASITierId ASC;

    
    IF (@@ROWCOUNT  > 0)
        BEGIN
	    
	    SELECT 
	        @UasiName = name, 
	        @UasiTierId = uasitierid
	    FROM 
	        Inventory.UrbanArea
	    WHERE 
	        id = @UrbanAreaId;
        
        END
        
    ELSE IF (@getNearestFlag = 1)
        BEGIN
	    
	    -- Try to find the nearest UASI to the site's buildings
	    SELECT TOP 1
	       @DistMiles = inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, c.latitude, c.longitude), 
	       @UasiCountyId = cx.CountyId,
	       @UrbanAreaId = cx.UrbanAreaId,
	       @UasiTierId = u.UASITierId,
	       @UasiName = u.Name,
	       @BuildingId = b.BldgId
	     FROM 
	        Inventory.UrbanAreaCountyXref cx
		    INNER JOIN Inventory.County c 
		        ON cx.CountyId = c.Id
		    INNER JOIN Inventory.UrbanArea u 
		        ON cx.UrbanAreaId = u.Id,
		    @BldgList b
	    ORDER BY 
	        inventory.fnCalcDistanceBetween(b.Latitude, b.Longitude, c.latitude, c.longitude) ASC;
        
        END
    END

	SET @UasiName = ISNULL(@uasiName,'');
	--set @UasiTierId = ISNULL(@UasiTierId, 3);
	--if (@UasiTierId = -1)
	--  Set @UasiTierId = 3;
	
	INSERT INTO @Table_Var 
	(
	    UasiAreaId, 
	    UasiName, 
	    UasiTierId, 
	    UasiCountyId, 
	    DistanceMiles, 
	    BuildingId
	)
	VALUES 
	(
	    @UrbanAreaId, 
	    @UasiName, 
	    @UasiTierId, 
	    @UasiCountyId, 
	    @DistMiles, 
	    @BuildingId
	);
	
	RETURN 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetElementsOfCostImportBatchTypeId]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     06/02/2016
-- Release:     7.1
-- Description: Returns the Batch Type Id of the Elements of Cost Import
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetElementsOfCostImportBatchTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 10; --Elements of Cost Export/Import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetAllotteePartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/18/2013
-- Release:     4.6
-- Description: Retrieves the Allottee party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetAllotteePartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- Allottee
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[fnGetOperatingSystemAndVersion]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     3/15/2013
-- Release:     4.0
-- Description: Converts the specified User Agent string to
--   an operating system and version.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Audit].[fnGetOperatingSystemAndVersion]
(
	@UserAgent nvarchar(MAX)
)
RETURNS
nvarchar(MAX)
AS
BEGIN
	RETURN CASE WHEN @UserAgent LIKE '%WINDOWS NT 5.1%' OR @UserAgent LIKE '%WINDOWS NT 5.2%' THEN 'Windows XP'
				WHEN @UserAgent LIKE '%WINDOWS NT 6.0%' THEN 'Windows Vista'
				WHEN @UserAgent LIKE '%WINDOWS NT 6.1%' THEN 'Windows 7'
				WHEN @UserAgent LIKE '%WINDOWS NT 6.2%' THEN 'Windows 8'
				WHEN @UserAgent LIKE '%MAC OS X 10.6%' OR @UserAgent LIKE '%MAC OS X 10_6%' THEN 'Mac OS X 10.6'
				WHEN @UserAgent LIKE '%MAC OS X 10.7%' OR @UserAgent LIKE '%MAC OS X 10_7%' THEN 'Mac OS X 10.7'
				WHEN @UserAgent LIKE '%MAC OS X 10.8%' OR @UserAgent LIKE '%MAC OS X 10_8%' THEN 'Mac OS X 10.8'
				WHEN @UserAgent LIKE '%MAC OS X%' AND @UserAgent LIKE '%ipad%' THEN 'iPad'
				ELSE 'Other'
		   END;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[fnGetOperatingSystem]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     3/15/2013
-- Release:     4.0
-- Description: Converts the specified User Agent string to
--   an operating system.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Audit].[fnGetOperatingSystem]
(
	@UserAgent nvarchar(MAX)
)
RETURNS
nvarchar(MAX)
AS
BEGIN
	RETURN CASE WHEN @UserAgent LIKE '%WINDOWS NT%' THEN 'Windows'
				WHEN @UserAgent LIKE '%MAC OS%' THEN 'Mac'
				ELSE 'Other'
		   END;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[fnGetActualBrowserVersion]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     3/15/2013
-- Release:     4.0
-- Description: Retrieves the actual browser version from the
--   User Agent string, IE reports the comaptibility version
--   instead of the actual browser version.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Audit].[fnGetActualBrowserVersion]
(
	@UserAgent nvarchar(MAX),
	@ReportedBrowserVersion nvarchar(25)
)
RETURNS
nvarchar(25)
AS
BEGIN
	RETURN 	CASE WHEN @UserAgent LIKE '%Trident/6.0%' THEN '10.0'
		 WHEN @UserAgent LIKE '%Trident/5.0%' THEN '9.0'
		 WHEN @UserAgent LIKE '%Trident/4.0%' THEN '8.0'
		 ELSE @ReportedBrowserVersion
	END;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructuresInReportGroupTree]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     4/19/2017
-- Release:     8.0
-- Description: Gets the Id's for all WorkBreakdownStructures in the Report Group and all it children
-- NOTE: Converted original stored procedure to function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructuresInReportGroupTree]
(
	@ReportGroupId AS INT
)
RETURNS
@WorkBreakdownStructureList TABLE 
(
	WorkBreakdownStructureId INT
)
AS
BEGIN

    --/* Begin Debug Declarations *//*
  --      DECLARE @ReportGroupId INT = 2474;
		--DECLARE @WorkBreakdownStructureList TABLE (WorkBreakdownStructureId INT);
    --*//* End Debug Declarations */

    WITH ReporGroupTree AS(
		SELECT rg.Id ReportGroupId
		FROM G2.ReportGroup rg
		WHERE rg.id = @ReportGroupId

		UNION ALL 

		SELECT rg.Id ReportGroupId
		FROM G2.ReportGroup rg
		INNER JOIN ReporGroupTree rgt ON rgt.ReportGroupId = rg.ParentId
	)
	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT DISTINCT rgwbsx.WorkBreakdownStructureId
	FROM ReporGroupTree g
	INNER JOIN G2.ReportGroupWorkBreakdownStructureXref rgwbsx ON rgwbsx.ReportGroupId = g.ReportGroupId;

	/* Debug Statement */
	--SELECT * FROM @WorkBreakdownStructureList wbsl;
	/* Debug Statement */

	RETURN; 

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA24FinPlanTeamLeadApprovalWorkflowLevel]'
GO

-- =============================================================
-- Author:      Ed Putkonen
-- Created:     10/17/2017
-- Release:     8.3
-- Description: Returns the value for the Pending Team Lead  Approval
--              Review Change Request Type Group Workflow Level Id. 
--              This is an NA-24 specific function.
--
-- mm/dd/yy  Name     Release  Description
-- 
-- =============================================================
Create FUNCTION [Report].[fnGetNA24FinPlanTeamLeadApprovalWorkflowLevel]()
RETURNS int
AS
BEGIN
	
	RETURN 3; -- Actually called "Pending Team Lead Approval"

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA24FinPlanProjectManagerWorkflowLevel]'
GO


-- =============================================================
-- Author:      Ed Putkonen
-- Created:     10/18/2017
-- Release:     8.3
-- Description: Returns the value for the Pending Project Manager 
--              Review Change Request Type Group Workflow Level Id. 
--              This is an NA-24 specific function.
--
-- mm/dd/yy  Name     Release  Description
-- 
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA24FinPlanProjectManagerWorkflowLevel]()
RETURNS INT
AS
BEGIN
	
	RETURN 2;

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA24FinPlanOfficeDirectorWorkflowLevel]'
GO

-- =============================================================
-- Author:      Ed Putkonen
-- Created:     10/17/2017
-- Release:     8.3
-- Description: Returns the value for the Pending Office Director  Approval
--              Review Change Request Type Group Workflow Level Id. 
--              This is an NA-24 MMM specific function.
--
-- mm/dd/yy  Name     Release  Description
-- 
-- =============================================================
Create FUNCTION [Report].[fnGetNA24FinPlanOfficeDirectorWorkflowLevel]()
RETURNS int
AS
BEGIN
	
	RETURN 4; -- Actually called "Pending Office Director Approval"

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA23MMMFinPlanOfficeDirectorWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the Pending Office Director 
--              Review Change Request Type Group Workflow Level Id. 
--              This is an NA-23 MMM specific function.
--
-- mm/dd/yy  Name     Release  Description
-- 10/20/15  a.smith    6.3    Modified return value from role id to 8.
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA23MMMFinPlanOfficeDirectorWorkflowLevel]()
RETURNS int
AS
BEGIN
	
	RETURN 8; -- Actually called "Pending Executive Manager Review"

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21OfficeTechnicalDirectorWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     11/18/2014
-- Release:     5.3
-- Description: Returns the value for the Pending Officer Director 
--              Technical Director Review Change Request Type Group 
--              Workflow Level Id. This is an NA-21 specific function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21OfficeTechnicalDirectorWorkflowLevel]()
RETURNS int
AS
BEGIN
	
	RETURN 3;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21GMSFinPlanProgramManagerWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the Pending Program Manager 
--              Review Change Request Type Group Workflow Level Id. 
--              This is an NA-21 GMS specific function.
--
-- mm/dd/yy  Name     Release  Description
-- 10/19/15  a.smith    6.3    Corrected returned value from roleid to 6.
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21GMSFinPlanProgramManagerWorkflowLevel]()
RETURNS int
AS
BEGIN
	
	RETURN 6;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21AADAWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     11/18/2014
-- Release:     5.3
-- Description: Returns the value for the Pending AADA Review Change 
--              Request Type Group Workflow Level Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21AADAWorkflowLevel]()
RETURNS int
AS
BEGIN

	RETURN 4;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetFinPlanReadytoImplementWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan Ready to Implement  
--              Change Request Type Group Workflow Level Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetFinPlanReadytoImplementWorkflowLevel]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN

	/* Debug Declarations */
	--DECLARE	@SubSystemId INT = 6;
	/* Debug Declarations */

	DECLARE @workflowLevelId INT;

	SELECT @workflowLevelId = MAX(crtgwt.WorkflowLevel)
	FROM G2.ChangeRequestTypeGroupWorkflowText crtgwt
	INNER JOIN G2.ChangeRequestTypeGroup crtg ON crtgwt.ChangeRequestTypeGroupId = crtg.Id
	WHERE crtgwt.SubSystemId = @SubSystemId
	AND crtg.Name = 'FinPlan'
	AND crtgwt.DisplayText = 'Ready To Implement';

	/* Debug Code */
	--SELECT @workflowLevelId;
	/* Debug Code */

	RETURN @workflowLevelId;

END

	
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidSTARSPerformerErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid STARS Performer error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidSTARSPerformerErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 24; -- Invalid STARS Performer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidFundTypeBNRErrorTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid FundType/B&R error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidFundTypeBNRErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 30; -- Invalid Fund Type - B&R
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidCostPeriodErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Cost Period error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidCostPeriodErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 28; -- Invalid Cost Period
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidContractIdErrorTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid Contract Id error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidContractIdErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 29; -- Invalid Contract ID
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidBNRCodeErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Invalid B&R Code error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidBNRCodeErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 25; -- Invalid B&R Code
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetContractIdNotMappedToProjectErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/28/2013
-- Release:     4.1
-- Description: Retrieves Contract ID is not mapped to a project
--   error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetContractIdNotMappedToProjectErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 21; -- Contract ID is not mapped to a project
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBNRNotMappedToPortfolio]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/24/2014 
-- Release:     5.0
-- Description: Returns the B&R mapping to WorkBreakdownStructure Error
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetBNRNotMappedToPortfolio] ()
RETURNS int
AS
BEGIN
  RETURN 37; --B&R is not mapped to a portfolio
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetSpendPlansForFiscalYear]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     4/18/2013
-- Description: Returns pivoted spend plan commitments and costs
--				for selected fiscal year.   
--
-- mm/dd/yy  Name     Version  Description
-- 09/27/14  a.smith  5.3      Updated to use new 5.3 data model structure.
-- 04/24/18  g.ogle   9.0      Updated to accommodate new Spend Plan model
--                             Added SubSystemId Parameter. 
-- 05/02/18  g.ogle   9.0      Fixed alias and related qualifier for Period table
-- =============================================================
CREATE FUNCTION [Report].[fnGetSpendPlansForFiscalYear] ( @SubSystemId INT, @FiscalYear INT )
RETURNS @SpendPlans TABLE
	(
	  WorkBreakdownStructureId INT
	, G2PerformerId INT
	, FiscalYear INT
	, C10 MONEY
	, C11 MONEY
	, C12 MONEY
	, C01 MONEY
	, C02 MONEY
	, C03 MONEY
	, C04 MONEY
	, C05 MONEY
	, C06 MONEY
	, C07 MONEY
	, C08 MONEY
	, C09 MONEY
	, E10 MONEY
	, E11 MONEY
	, E12 MONEY
	, E01 MONEY
	, E02 MONEY
	, E03 MONEY
	, E04 MONEY
	, E05 MONEY
	, E06 MONEY
	, E07 MONEY
	, E08 MONEY
	, E09 MONEY
	)
AS 
	BEGIN

    /* Debug Declarations 
    DECLARE @SpendPlans TABLE (WorkBreakdownStructureId INT, G2PerformerId INT, FiscalYear INT, C10 MONEY, C11 MONEY, C12 MONEY, C01 MONEY, C02 MONEY, C03 MONEY, C04 MONEY, C05 MONEY, C06 MONEY, C07 MONEY, C08 MONEY, C09 MONEY, E10 MONEY, E11 MONEY, E12 MONEY, E01 MONEY, E02 MONEY, E03 MONEY, E04 MONEY, E05 MONEY, E06 MONEY, E07 MONEY, E08 MONEY, E09 MONEY );
    DECLARE @FiscalYear INT = 2014, @SubSystemId INT = 5;
    -- Debug Declarations */

    INSERT	INTO @SpendPlans ( WorkBreakdownStructureId, G2PerformerId, FiscalYear, C10, C11, C12, C01, C02, C03, C04, C05, C06, C07, C08, C09 )
    SELECT	pvt.WorkBreakdownStructureId, pvt.ReportingEntityOrganizationPartyId, pvt.FiscalYear, pvt.[1] C10, pvt.[2] C11, pvt.[3] C12, pvt.[4] C01, pvt.[5] C02, pvt.[6] C03, pvt.[7] C04, pvt.[8] C05, pvt.[9] C06, pvt.[10] C07, pvt.[11] C08, pvt.[12] C09
    FROM	(
        SELECT sph.WorkBreakdownStructureId, sph.ReportingEntityOrganizationPartyId, p.FiscalYear, p.FiscalPeriod, spd.CommitmentForecast
        FROM Financial.SpendPlanHeader sph
        INNER JOIN Financial.SpendPlanDetail spd ON sph.Id = spd.SpendPlanHeaderId
        INNER JOIN G2.Period p ON p.Id = spd.PeriodId
        INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = sph.WorkBreakdownStructureId
        WHERE p.FiscalYear = COALESCE(@FiscalYear, p.FiscalYear) AND wbs.SubSystemId = @SubSystemId
    ) X PIVOT( SUM(X.CommitmentForecast) FOR X.FiscalPeriod IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] ) ) pvt

	UPDATE @SpendPlans
	SET sp.E10 = Y.E10, sp.E11 = Y.E11, sp.E12 = Y.E12, sp.E01 = Y.E01, sp.E02 = Y.E02, sp.E03 = Y.E03, sp.E04 = Y.E04, sp.E05 = Y.E05, sp.E06 = Y.E06, sp.E07 = Y.E07, sp.E08 = Y.E08, sp.E09 = Y.E09 
    FROM @SpendPlans sp
	INNER JOIN (
        SELECT	pvt.WorkBreakdownStructureId, pvt.ReportingEntityOrganizationPartyId, pvt.FiscalYear, pvt.[1] AS E10, pvt.[2]  AS E11, pvt.[3]  AS E12, pvt.[4]  AS E01, pvt.[5] AS E02, pvt.[6]  AS E03, pvt.[7]  AS E04, pvt.[8]  AS E05, pvt.[9] AS E06, pvt.[10] AS E07, pvt.[11] AS E08, pvt.[12] AS E09
        FROM (
            SELECT	sph.WorkBreakdownStructureId, sph.ReportingEntityOrganizationPartyId, FiscalYear, FiscalPeriod, spd.CostForecast
            FROM Financial.SpendPlanHeader sph
            INNER JOIN Financial.SpendPlanDetail spd ON sph.Id = spd.SpendPlanHeaderId
            INNER JOIN G2.Period p ON p.Id = spd.PeriodId
            INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = sph.WorkBreakdownStructureId
            WHERE FiscalYear = COALESCE(@FiscalYear, FiscalYear) AND wbs.SubSystemId = @SubSystemId
        ) X 
        PIVOT( SUM(X.CostForecast) FOR X.FiscalPeriod IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] ) ) pvt
    ) Y
    ON sp.WorkBreakdownStructureId = Y.WorkBreakdownStructureId AND sp.G2PerformerId = Y.ReportingEntityOrganizationPartyId AND sp.FiscalYear = Y.FiscalYear

	/* Debug Return 
	SELECT * FROM @SpendPlans sp
	ORDER BY WorkBreakdownStructureId, sp.G2PerformerId, sp.FiscalYear
	-- End Debug Return */

    RETURN;

END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructuresForWorkTypeGroup]'
GO
-- =============================================================
-- Author:      Greg Ogle/Adela Smith
-- Created:     4/19/2017
-- Release:     8.0
-- Description: Gets the Id's for all WorkBreakdownStructures for all the WorkTypes of the WorkTypeGroup
-- NOTE: Converted original stored procedure to function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructuresForWorkTypeGroup]
(
	@WorkTypeGroupId AS INT
)
RETURNS
@WorkBreakdownStructureList TABLE 
(
	WorkBreakdownStructureId INT
)
AS
BEGIN
    /* Begin Debug Declarations *//*
        DECLARE @WorkTypeGroupId INT = 22;
		DECLARE @WorkBreakdownStructureList TABLE (WorkBreakdownStructureId INT);
    --*//* End Debug Declarations */

	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT wbs.Id WorkBreakdownStructureId
	FROM G2.WorkType wt
	INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbstx ON wbstx.WorkTypeId = wt.Id
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbstx.WorkBreakdownStructureId
	WHERE wt.WorkTypeGroupId = @WorkTypeGroupId;

	/* Debug Statement */
	--SELECT * FROM @WorkBreakdownStructureList wbsl;
	/* Debug Statement */

	RETURN;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructuresForWorkType]'
GO
-- =============================================================
-- Author:      Greg Ogle/Adela Smith
-- Created:     4/19/2017
-- Release:     8.0
-- Description: Gets the Id's for all WorkBreakdownStructures for the WorkType
-- NOTE: Converted original stored procedure to function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructuresForWorkType]
(
	@WorkTypeId AS INT
)
RETURNS
@WorkBreakdownStructureList TABLE 
(
	WorkBreakdownStructureId INT
)
AS
BEGIN
    /* Begin Debug Declarations *//*
        DECLARE @WorkTypeId INT = 44;
		DECLARE @WorkBreakdownStructureList TABLE (WorkBreakdownStructureId INT);
    --*//* End Debug Declarations */

	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT wbs.Id WorkBreakdownStructureId
	FROM G2.WorkBreakdownStructureWorkTypeXref wbstx
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbstx.WorkBreakdownStructureId
	WHERE wbstx.WorkTypeId = @WorkTypeId;

	/* Debug Statement */
	--SELECT * FROM @WorkBreakdownStructureList wbsl;
	/* Debug Statement */

	RETURN;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetFinancialInfoforFiscalYearFiscalPeriods]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     5/26/2016
-- Release:     7.1
-- Description: This function will return financial info for 
--              Carryover, Funding, Costs, and Commitments for 
--              the specified SubSystem, Fiscal Year, Funding
--              Thru Fiscal Period and Costs Thru Fiscal Period.
--              The resulting data is grouped by Fiscal Year,
--              ReportingEntityOrganizationPartyId, FundTypeId, BNRId,
--              and WorkBreakdownStructureId.
--              NOTE: These period values are FISCAL Periods not Period Ids!
--
-- mm/dd/yy  Name     Release  Description
-- 06/01/16  a.smith    7.1    Added InternationalInd parameter.
-- 06/30/16  a.smith    7.1    Removed @FiscalYear filter on FundingProfile CTE
--                             and added FiscalYear filter on results.
-- =============================================================
CREATE FUNCTION [Report].[fnGetFinancialInfoforFiscalYearFiscalPeriods]
(
	@SubSystemId INT,
	@FiscalYear INT,
	@CostThruFiscalPeriod INT,
	@FundingThruFiscalPeriod INT,
	@InternationalInd BIT = NULL
)
RETURNS @FinancialData TABLE
(
	FiscalYear INT,
	ReportingEntityOrganizationPartyId INT,
	FundTypeId INT,
	BNRId INT,
	WorkBreakdownStructureId INT,
	Carryover MONEY,
	Funding MONEY,
	Costs MONEY,
	Commitments MONEY
)
BEGIN 

	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5,
	--		@FiscalYear INT = 2016,
	--		@CostThruFiscalPeriod INT = 5,
	--		@FundingThruFiscalPeriod INT = 7,
	--		@InternationalInd BIT = NULL;
	--DECLARE @FinancialData TABLE
	--(
	--	FiscalYear INT,
	--	ReportingEntityOrganizationPartyId INT,
	--	FundTypeId INT,
	--	BNRId INT,
	--	WorkBreakdownStructureId INT,
	--	Carryover MONEY,
	--	Funding MONEY,
	--	Costs MONEY,
	--	Commitments MONEY 
	--);
	/* Debug Declarations */

	DECLARE @fundingConfigurationTypeId INT = G2.fnGetFundingConfigurationTypeId(),
			@fundingTransactionGroupId INT = Financial.fnGetFundingTransactionTypeGroupId(),
			@costTransactionGroupId INT = Financial.fnGetCostTransactionTypeGroupId(),
			@commitmentTransactionGroupId INT = Financial.fnGetCommitmentTransactionTypeGroupId();
	WITH FundingProfile AS
	(
		SELECT FY.FiscalYear, a.ReportingEntityOrganizationPartyId, a.FundTypeId, a.BNRId, a.WorkBreakdownStructureId,
				SUM
				(
					CASE WHEN tt.TransactionTypeGroupId = @fundingTransactionGroupId AND p.FiscalYear < FY.FiscalYear THEN at.Amount
						 WHEN tt.TransactionTypeGroupId = @costTransactionGroupId AND p.FiscalYear < FY.FiscalYear THEN -at.Amount 
						 ELSE 0
					END
				) AS Carryover,
				SUM
				(
					CASE WHEN tt.TransactionTypeGroupId = @fundingTransactionGroupId 
							  AND p.FiscalYear = FY.FiscalYear 
							  AND p.FiscalPeriod <= @FundingThruFiscalPeriod
						 THEN at.Amount 
						 ELSE 0
					END
				) AS Funding,
				SUM
				(
					CASE WHEN tt.TransactionTypeGroupId = @commitmentTransactionGroupId 
							  AND p.FiscalYear = FY.FiscalYear
							  AND p.FiscalPeriod <= @CostThruFiscalPeriod
						 THEN at.Amount 
						 ELSE 0
					END
				) AS Commitments,
				SUM
				(
					CASE WHEN tt.TransactionTypeGroupId = @costTransactionGroupId 
							  AND p.FiscalYear = FY.FiscalYear 
							  AND p.FiscalPeriod <= @CostThruFiscalPeriod
						 THEN at.Amount 
						 ELSE 0
					END
				) AS Costs
		FROM Financial.Account a
		INNER JOIN G2.WorkBreakdownStructure wbs ON a.WorkBreakdownStructureId = wbs.Id AND wbs.SubSystemId = @SubSystemId
		CROSS JOIN G2.FiscalYear FY
		INNER JOIN Financial.AccountTransaction at ON at.AccountId = a.Id
		INNER JOIN Financial.TransactionType tt ON tt.Id = at.TransactionTypeId
		INNER JOIN G2.Period p ON p.Id = at.PeriodId
		INNER JOIN Financial.FundType ft ON a.FundTypeId = ft.Id
		WHERE ft.InternationalInd = COALESCE(@InternationalInd, ft.InternationalInd)
		GROUP BY  FY.FiscalYear, a.ReportingEntityOrganizationPartyId, a.FundTypeId, a.BNRId, a.WorkBreakdownStructureId
	)
	INSERT INTO @FinancialData (FiscalYear, ReportingEntityOrganizationPartyId, FundTypeId, BNRId, WorkBreakdownStructureId, Carryover, Funding, Costs, Commitments)
	SELECT 	fp.FiscalYear, fp.ReportingEntityOrganizationPartyId, fp.FundTypeId, fp.BNRId, fp.WorkBreakdownStructureId, fp.Carryover, fp.Funding, fp.Costs, fp.Commitments
	FROM FundingProfile fp
	WHERE fp.FiscalYear = @FiscalYear;

	/* Debug Statement */
	--SELECT * FROM @FinancialData fd
	/* Debug Statement */

	RETURN;

END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetPrivateSectorNonLabCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for Private Sector/Non-Lab by
--   sub-system. This assumes that there is only one Private Sector/Non-Lab
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetPrivateSectorNonLabCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	--DECLARE @transactionTypeId int,
	--	    @costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Private Sector/Non-Lab';

	--RETURN @transactionTypeId;
	RETURN 10;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLabTravelCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for lab travel by
--   sub-system. This assumes that there is only one lab travel
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLabTravelCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	--DECLARE @transactionTypeId int,
	--	@costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Lab Travel';

	--RETURN @transactionTypeId;
	RETURN 4;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLabEquipmentCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for lab equipment by
--   sub-system. This assumes that there is only one lab equipment
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLabEquipmentCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	--DECLARE @transactionTypeId int,
	--	    @costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Lab Equipment';

	--RETURN @transactionTypeId;
	RETURN 5;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetLabContractFeesCostTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for lab contract fees by
--   sub-system. This assumes that there is only one lab contract fees
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetLabContractFeesCostTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	--DECLARE @transactionTypeId int,
	--	    @costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Lab Contract Fees';

	--RETURN @transactionTypeId;
	RETURN 6;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFSUTravelTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/19/2015
-- Release:     6.2
-- Description: Retrieves the transaction type for FSU Travel.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFSUTravelTransactionTypeId] ()
RETURNS int
AS
BEGIN
	
	RETURN 7;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFSUServicesEquipmentTransactionTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/19/2015
-- Release:     6.2
-- Description: Retrieves the transaction type for FSU Services
--              Equipment - from FSU.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFSUServicesEquipmentTransactionTypeId] ()
RETURNS int
AS
BEGIN
	
	RETURN 8;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFSUServicesEquipmentfromUSEuropeTransactionTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/19/2015
-- Release:     6.2
-- Description: Retrieves the transaction type for FSU Services
--              Equipment - from US/Europe.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFSUServicesEquipmentfromUSEuropeTransactionTypeId] ()
RETURNS int
AS
BEGIN
	
	RETURN 9;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetInvalidPerformerErrorTypeId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/2/2016
-- Release:     6.5
-- Description: Returns the error type id for an invalid performer
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetInvalidPerformerErrorTypeId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 63;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetInvalidFinancialAccountErrorTypeId]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/23/2016
-- Release:     6.5
-- Description: Returns the error type id for an invalid financial account
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetInvalidFinancialAccountErrorTypeId]
(
)
RETURNS INT
AS
BEGIN
	RETURN 64;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetCurrentActiveCollectionPeriodBySubsystemId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/2/2016
-- Release:     6.5
-- Description: Retrieves the
--
-- mm/dd/yy  Name     Release  Description
-- 09/01/16  n.coffey 7.2      DE5617 Removed funding thru criteria from query
-- 01/12/17  mws      7.4      DE6304 Changed to use current cost period
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetCurrentActiveCollectionPeriodBySubsystemId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @currentCostPeriodId INT = CostSubmission.fnGetCurrentCostPeriod(@SubSystemId),
			@activeCollectionStatusId INT = YearEndForecast.fnGetActiveCollectionStatusId(),
			@collectionPeriodId INT;

	SELECT @collectionPeriodId = cp.Id 
	FROM YearEndForecast.CollectionPeriod cp 
	WHERE CP.SubSystemId = @SubSystemId
	AND cp.PeriodId = @currentCostPeriodId
	AND cp.CollectionStatusId = @activeCollectionStatusId;

	RETURN @collectionPeriodId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetPerformerGroupRollupPartyRelationshipTypeId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/18/2013
-- Release:     4.6
-- Description: Retrieves the Performer Group Rollup party
--   relationship type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetPerformerGroupRollupPartyRelationshipTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Performer Group Rollup
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Chris Luttrell
-- Created:     5/31/2007
-- Description: Returns table of WorkBreakdownStructures based on the parameters passed in.  
--
-- mm/dd/yy  Name     StoryId	Release		Description
-- 02/23/09  s.oneal						Modified query to handle Region/State filters
-- 07/29/09  DNC		Added				Portfolio option
-- 09/15/09  d.cantrell						Modified query to handle Portfolio Officer filter
-- 10/23/09	 DNC		2032				Update to use SiteExt table
-- 02/09/10  DNC		Added				Mid-Year Group option
-- 06/02/10	 DNC							Split out Mid-Year and Non-Mid-Year Project Lists; No doing so was leaving
--											out projects at the State and Region Level	
-- 12/02/10  m.brown						Modified to support the new Building/Site schema changes
-- 12/03/10  DNC				Atlantis	Updated reference to RegionStateXRef to match current schema name
-- 12/14/10  DNC				Atlantis	Update to use new Portfolio table and role
-- 01/13/11  BScott             Atlantis    Fixed site/building stuff that wasn't completely fixed
-- 01/07/12  CGL                3.0         Changing PersonRoleWBS to the Security schema
-- 01/12/12  DNC				3.0			Updated to use Security schema for role
-- 04/26/13	 DNC				4.1			Remove NAFD references
-- 10/15/14  j.borgers          5.3         Modified to work with schedule data structure changes and changed the description
-- =============================================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructureId]
(
	@WorkBreakdownStructureId int,
	@ReportGroupId int,
	@WorkTypeList varchar(100),
	@StartDt datetime,
	@EndDt datetime,
	@ConfigurationTypeIdTable G2.GenericUniqueIdTable READONLY
)
RETURNS 
@WorkBreakdownStructures TABLE 
(
	WorkBreakdownStructureId int, 
	ParentId int, 
	Name nvarchar(376), 
	WorkTypeId int, 
	WorkTypeGroupName nvarchar(50), 
	StartDt datetime, 
	FinishDt datetime
)
AS
BEGIN
	/*Debug*/
	--DECLARE @WorkBreakdownStructures TABLE (WorkBreakdownStructureId int, ParentId int, Name nvarchar(400), WorkTypeId int, WorkTypeGroupName nvarchar(50), StartDt datetime, FinishDt datetime)

	--DECLARE @WorkBreakdownStructureId int = 231,--1,
	--		@ReportGroupId int = NULL,--4,
	--		@WorkTypeList varchar(100) = '1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19',
	--		@StartDt datetime,
	--		@EndDt datetime,
	--		@ConfigurationTypeIdTable G2.GenericUniqueIdTable;

	--INSERT INTO @ConfigurationTypeIdTable (Id)
	--SELECT Id
	--FROM G2.ConfigurationType ct;
	/*End Debug*/

	DECLARE @subSystemId int = G2.fnGetSubSystemIdFromWorkBreakdownStructure(@WorkBreakdownStructureId),
			@allWorkTypes bit;
	DECLARE @Projects TABLE (WorkBreakdownStructureId int);

	IF @StartDt IS NULL
	BEGIN
		SELECT @StartDt = MIN(BeginDt) FROM G2.Period;
	END;

	IF @EndDt IS NULL
	BEGIN
		SELECT @EndDt = MAX(EndDt) FROM G2.Period;
	END;

	SELECT @allWorkTypes=~CONVERT(bit, (COUNT(*))) 
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	LEFT OUTER JOIN G2.fnParseList(@WorkTypeList, '|') vwt ON wt.id= vwt.id 
	WHERE wt.DashboardFilterInd = 1 
	AND wtg.SubSystemId = @subSystemId
	AND vwt.id IS NULL;

	INSERT INTO @Projects (WorkBreakdownStructureId)
	SELECT WorkBreakdownStructureId 
	FROM Report.fnGetWorkBreakdownStructureByConfigurationType(@WorkBreakdownStructureId, @ConfigurationTypeIdTable) fgwbsbct;

	INSERT INTO @WorkBreakdownStructures (WorkBreakdownStructureId, ParentId, Name, WorkTypeId, WorkTypeGroupName, StartDt, FinishDt)
	SELECT wbs.Id, wbs.ParentId, wbs.Name, wt.Id AS WorkTypeId, wtg.Name AS WorkTypeGroupName, s.StartDt, s.FinishDt
	FROM @Projects p
	INNER JOIN G2.WorkBreakdownStructure wbs ON p.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.Schedule s ON wbs.Id = s.WorkBreakdownStructureId
	INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbs.Id = wbswtx.WorkBreakdownStructureId
	INNER JOIN G2.WorkType wt ON wbswtx.WorkTypeId = wt.Id
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	LEFT OUTER JOIN G2.ReportGroupWorkBreakdownStructureXref rgwbsx ON wbs.Id = rgwbsx.WorkBreakdownStructureId AND rgwbsx.ReportGroupId = COALESCE(@ReportGroupId, -1)
	WHERE ((@allWorkTypes = 1) OR (wt.Id IN (SELECT Id FROM G2.fnParseList(@WorkTypeList, '|'))))
	AND (@ReportGroupId IS NULL OR rgwbsx.Id IS NOT NULL)
	AND s.StartDt <= G2.fnDayEnd(@EndDt)
	AND s.FinishDt >= @StartDt;
	
	
	--SELECT * FROM @WorkBreakdownStructures
	--ORDER BY Name

	RETURN;
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetPerformerPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/18/2013
-- Release:     4.6
-- Description: Retrieves the Performer party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetPerformerPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Performer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnGetLifecycleBudgetTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/10/2012
-- Release:     3.3
-- Description: Retrieves the LifecycleBudget (formerly Out-Year Funding)
--   ChangeRequestTypeId.
--
-- mm/dd/yy  Name      Release  Description
-- 09/18/14  j.frank   5.3      Renamed the function, replacing "OutYearFunding" with "LifecycleBudget"
--                              since the OutYearFunding tables got renamed.
-- =============================================================
CREATE FUNCTION [Baseline].[fnGetLifecycleBudgetTypeId]()
RETURNS int
AS
BEGIN
	RETURN 8;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidScoreErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/20/2016 
-- Release:     7.3
-- Description: Returns the Invalid Score Error Type
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidScoreErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 67; --Invalid Score
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidProjectErrorTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     08/11/2016
-- Release:     7.2
-- Description: Retrieves Invalid Project error type id.
--
-- mm/dd/yy  Name      Release  Description
-- 09/20/16  j.borgers 7.3		Renamed Error Type to be more user friendly
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInvalidProjectErrorTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 66; -- Invalid Project
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMnOOtherCostsTransactionTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     12/08/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for Other M&O Costs 
--   by sub-system. This assumes that there is only one lab travel
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- 08/06/15  a.smith    6.2     Replaced select statement with return of
--                              static value since there is only one.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetMnOOtherCostsTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
    -- Debug statements
	--DECLARE @SubSystemId INT = 3;
	-- Debug statements
	
	--DECLARE @transactionTypeId int,
	--	@costTransactionTypeGroupId int = Financial.fnGetCostTransactionTypeGroupId();	

	--SELECT @transactionTypeId = tt.Id
	--FROM Financial.TransactionType tt
	--INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	--INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	--WHERE sstt.SubSystemId = @SubSystemId
	--AND ttg.Id = @costTransactionTypeGroupId
	--AND tt.Name = 'Other M&O Costs';

	--RETURN @transactionTypeId;
	RETURN 16;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetFundingTransactionTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/13/2014
-- Release:     5.0
-- Description: Retrieves the transaction type for funding by
--   sub-system. This assumes that there is only one funding
--   type in a sub-system.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetFundingTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	DECLARE @transactionTypeId int,
		@fundingTransactionTypeGroupId int = Financial.fnGetFundingTransactionTypeGroupId();	

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	INNER JOIN Financial.TransactionTypeGroup ttg ON tt.TransactionTypeGroupId = ttg.Id
	WHERE sstt.SubSystemId = @SubSystemId
	AND ttg.Id = @fundingTransactionTypeGroupId;

	RETURN @transactionTypeId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInfrastructurePlanningScoreImportBatchTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     08/10/2016 
-- Release:     7.2
-- Description: Returns the Infrastructure Planning Score Import batch type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetInfrastructurePlanningScoreImportBatchTypeId]()
RETURNS int
AS
BEGIN
  RETURN 11; --Infrastructure Planning Score Import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetWorkBreakdownStructureHierarchyIdbyLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/25/2015
-- Release:     6.2
-- Description: Returns the hierarchyid data type for the
--   specified level for the given workbreakdownstructureid.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetWorkBreakdownStructureHierarchyIdbyLevel]
(
	@WorkBreakdownStructureId INT,
	@TargetLevel INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @WorkBreakdownStructureId INT = 22599,
	--		@TargetLevel INT = 2;
	/* Debug Declarations */

	DECLARE @hierarchyId AS HIERARCHYID,
			@targetWorkBreakdownStructureId INT = NULL,
			@currentLevel INT;

	SELECT @currentLevel = wbsh.WorkBreakdownStructureHierarchyId.GetLevel()
	FROM G2.WorkBreakdownStructureHierarchy wbsh
	WHERE wbsh.WorkBreakdownStructureId = @WorkBreakdownStructureId;

	IF (@currentLevel - @TargetLevel) > 0
	BEGIN 
		SELECT @hierarchyId = wbsh.WorkBreakdownStructureHierarchyId.GetAncestor(@currentLevel-@TargetLevel) 
		FROM G2.WorkBreakdownStructureHierarchy wbsh
		WHERE wbsh.WorkBreakdownStructureId = @WorkBreakdownStructureId;

		SELECT @TargetWorkBreakdownStructureId = wbs.Id
		FROM G2.WorkBreakdownStructure wbs
		INNER JOIN G2.WorkBreakdownStructureHierarchy wbsh ON wbs.Id = wbsh.WorkBreakdownStructureId
		WHERE wbsh.WorkBreakdownStructureHierarchyId = @hierarchyId;
	END;
	
	/* Debug Declarations */
	--SELECT *
	--FROM G2.WorkBreakdownStructure wbs
	--WHERE wbs.Id = @TargetWorkBreakdownStructureId;
	/* Debug Declarations */

	RETURN(@TargetWorkBreakdownStructureId);
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetTableTopExerciseEventTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the TTX EventType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetTableTopExerciseEventTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 2; -- TTX
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetCanceledEventStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the Canceled EventStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetCanceledEventStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 3; -- Canceled
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[fnGetOpenSolicitationStatusId]'
GO

-- =============================================================
-- Author:      Dakota Abston
-- Created:     12/21/2017
-- Release:     8.4
-- Description: Returns the Open SolicitationStatusId
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Proposal].[fnGetOpenSolicitationStatusId] ()
RETURNS INT
AS 
BEGIN 
	RETURN 1; --Open
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetTravelerOrganizationPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/21/2013
-- Release:     4.4
-- Description: Retrieves the traveler organization party role
--   type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetTravelerOrganizationPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Traveler Organization
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetInternalOrganizationPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/21/2013
-- Release:     4.4
-- Description: Retrieves the internal organization party role
--   type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetInternalOrganizationPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Internal Organization
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAsSubmittedTo]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     4/4/2017
-- Release:     8.0
-- Description: Converted SPA.fnGetSPASubmittedToPersonName to a table-valued function
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAsSubmittedTo] 
(
	@IncludeParensInd BIT = 1
)
RETURNS @Persons TABLE
(
	SPAMainId INT,
	Names VARCHAR(MAX)
)
AS
BEGIN
    ---- debug
    --DECLARE @IncludeParensInd INT = 1,
    --    @SPAMainId INT = (SELECT sm.Id FROM SPA.SPAMain sm WHERE sm.SiteId = 7338);
    ----debug

	DECLARE @RoleIdSPAProtectTechnicalLead INT = Security.fnGetSPAProtectTechnicalLeadRoleId(),
			@RoleIdSPAPortfolioManagerPrimary INT = Security.fnGetSPAPortfolioManagerPrimaryRoleId(),
			@RoleIdSPAPortfolioManagerSecondary INT = Security.fnGetSPAPortfolioManagerSecondaryRoleId(),
			@RoleIdSPALabLead INT = Security.fnGetSPALabLeadRoleId(),
			@RoleIdSPATeamLead INT = Security.fnGetSPATeamLeadRoleId(),
			@SPAStatusIdSubmittedtoSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
			@SPAStatusIdSubmittedtoSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
			@SPAStatusIdSubmittedtoSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
			@SPAStatusIdSubmittedtoSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead();

	DECLARE @temp TABLE
	(
		SPAMainId INT,
		ProgramWorkBreakdownStructureId INT,
		PortfolioWorkBreakdownStructureId INT,
		SPATeamId INT,
		SPAStatusId INT NULL,
		RoleId INT NULL
	);
		
	INSERT INTO @temp (SPAMainId, ProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SPATeamId, SPAStatusId, RoleId)
	SELECT sm.Id AS SPAMainId, vswbs.ProgramWorkBreakdownStructureId, vswbs.PortfolioWorkBreakdownStructureId,  smgstx.SPATeamId, sm.SPAStatusId,
		CASE WHEN sm.SPAStatusId = @SPAStatusIdSubmittedtoSPATeamLead THEN @RoleIdSPATeamLead
			 WHEN sm.SPAStatusId = @SPAStatusIdSubmittedtoSPAPortfolioManager THEN @RoleIdSPAPortfolioManagerPrimary
			 WHEN sm.SPAStatusId = @SPAStatusIdSubmittedtoSPALabLead THEN 
				  CASE WHEN LSLCount.LSLCount > 0 THEN @RoleIdSPALabLead 
					   ELSE @RoleIdSPAPortfolioManagerPrimary 
				  END
			 WHEN sm.SPAStatusId = @SPAStatusIdSubmittedtoSPAProtectTechnicalLead THEN @RoleIdSPAProtectTechnicalLead
  		END AS RoleId
	FROM SPA.SPAMain sm
	INNER JOIN SPA.SPAMainGeneral smg ON smg.SPAMainId = sm.Id AND smg.Revision = sm.Revision
	INNER JOIN SPA.SPAMainGeneralSPATeamXref smgstx ON smgstx.SPAMainGeneralId = smg.Id AND smgstx.PrimaryInd = 1
	INNER JOIN SPA.SPAStatus ss ON ss.Id = sm.SPAStatusId
	INNER JOIN G2.Site s ON s.Id = sm.SiteId
    INNER JOIN SPA.vwSiteWorkBreakdownStructure vswbs ON vswbs.SiteId = sm.SiteId
    LEFT JOIN 
	(
		-- Get SLL count for the SPA team
		SELECT SPATeamId, COUNT(*) AS LSLCount
		FROM Security.PersonRole
		WHERE RoleId = @RoleIdSPALabLead
		GROUP BY SPATeamId
	) LSLCount ON LSLCount.SPATeamId = smgstx.SPATeamId;

    -- Account for any secondary roles not inserted above
	INSERT INTO @temp (SPAMainId, ProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SPATeamId, SPAStatusId, RoleId)
    SELECT t.SPAMainId, t.ProgramWorkBreakdownStructureId, t.PortfolioWorkBreakdownStructureId, t.SPATeamId, t.SPAStatusId, @RoleIdSPAPortfolioManagerSecondary
    FROM @temp t
    WHERE t.RoleId = @RoleIdSPAPortfolioManagerPrimary;

	DECLARE @PersonNameTable TABLE
			(
				SPAMainId INT,
				LastName VARCHAR(50),
				FirstName VARCHAR(50)
			);
				
	INSERT @PersonNameTable (SPAMainId, LastName, FirstName)

	SELECT DISTINCT t.SPAMainId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.SPATeamId = pr.SPATeamId 
	WHERE t.RoleId IN (@RoleIdSPATeamLead, @RoleIdSPALabLead)
			
	UNION 

	SELECT DISTINCT t.SPAMainId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.PortfolioWorkBreakdownStructureId = pr.WorkBreakdownStructureId 
	WHERE t.RoleId IN (@RoleIdSPAPortfolioManagerPrimary, @RoleIdSPAPortfolioManagerSecondary)
			
	UNION 

	SELECT DISTINCT t.SPAMainId, p.LastName, p.FirstName
	FROM Security.Person p
	INNER JOIN Security.PersonRole pr ON pr.PersonId = p.Id
	INNER JOIN @temp t ON t.RoleId = pr.RoleId AND t.ProgramWorkBreakdownStructureId = pr.WorkBreakdownStructureId
	WHERE t.RoleId = @RoleIdSPAProtectTechnicalLead;


	INSERT INTO @Persons (SPAMainId, Names)
	SELECT n.SPAMainId, CASE WHEN @IncludeParensInd = 1 THEN '(' + LEFT(n.Names, LEN(n.Names) - 1) + ')' ELSE LEFT(n.Names, LEN(n.Names) - 1) END AS Names
	FROM 
	(
		SELECT pnt.SPAMainId, 
		(
			SELECT n.LastName + ', ' + n.FirstName + '; '
			FROM @PersonNameTable n
			WHERE n.SPAMainId = pnt.SPAMainId
			ORDER BY n.LastName, n.FirstName
			FOR XML PATH('')
		) AS Names
		FROM @PersonNameTable pnt
		GROUP BY pnt.SPAMainId
	) n;

	RETURN;
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNOAFundingSourceId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     10/19/2016
-- Release:     7.3
-- Description: Selects the NOA Funding Source Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNOAFundingSourceId]()
RETURNS INT AS 
BEGIN
	RETURN 3; -- NOA FundingSourceId
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetOrganizationHierarchyId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/25/2013
-- Release:     4.4
-- Description: Returns the hierarchyid data type for the
--   corresponding parent id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetOrganizationHierarchyId]
(
	@ParentId int
)
RETURNS hierarchyid
AS
BEGIN
	DECLARE @hierarchyId AS hierarchyid,
		@parentHierarchyId AS hierarchyid,
		@lastChildHierarchyId AS hierarchyid;

	IF @ParentId IS NULL
	BEGIN
		SET @hierarchyId = HIERARCHYID::GetRoot().GetDescendant((SELECT MAX(o.OrganizationHierarchyId)
															     FROM Contact.Organization o
																 WHERE o.OrganizationHierarchyId.GetAncestor(1) = hierarchyid::GetRoot()), NULL)
			 
	END
	ELSE
	BEGIN   					
		-- Need to create the hierarchy_id
		SET @parentHierarchyId = (SELECT OrganizationHierarchyId
								  FROM Contact.Organization
								  WHERE PartyId = @ParentId);
		SET @lastChildHierarchyId = (SELECT MAX(OrganizationHierarchyId)
									 FROM Contact.Organization
									 WHERE OrganizationHierarchyId.GetAncestor(1) = @parentHierarchyId);								 
		SET @hierarchyId = @parentHierarchyId.GetDescendant(@lastChildHierarchyId, NULL);
	END
	
	RETURN(@hierarchyId)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetPerformerGroupPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     12/18/2013
-- Release:     4.6
-- Description: Retrieves the Performer Group party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetPerformerGroupPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- Performer Group
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetSMPPerformerPartyRoleTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/15/2016 
-- Release:     7.4
-- Description: Returns the SMP Performer party role type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetSMPPerformerPartyRoleTypeId]()
RETURNS int
AS
BEGIN
  RETURN 16; --SMP Performer
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetInfrastructurePlanningSitePartyRoleTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/01/2016 
-- Release:     7.1
-- Description: Returns the PartyRoleType Id for Infrastructure Planning Site
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetInfrastructurePlanningSitePartyRoleTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 15; --Infrastructure Planning Site
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetIndirectAndOtherDirectCostPerformerPartyRoleTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/19/2015 
-- Release:     6.4
-- Description: Returns the Indirect and Other Direct Cost Performer PartyRoleType
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Contact].[fnGetIndirectAndOtherDirectCostPerformerPartyRoleTypeId]()
RETURNS int
AS
BEGIN
  RETURN 14; --Indirect and Other Direct Cost Performer
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetBudgetPerformerPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     01/15/15
-- Release:     5.4
-- Description: Retrieves the Budget Performer party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetBudgetPerformerPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 11; -- Performer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetBudgetFormulationPerformerPartyRoleTypeId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/15
-- Release:     6.4
-- Description: Retrieves the Budget Formulation Performer
--   party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetBudgetFormulationPerformerPartyRoleTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 13; -- Budget Formulation Performer
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetAnnex1LabPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Chris Luttrell
-- Create date: Nov 17 2016  8:12:02:880PM
-- Release:     7.4
-- Description: Retrieves the Annex1 Lab party role type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetAnnex1LabPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 17; -- Annex1 Lab
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementTypeIdDate]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/06/2017
-- Release:     8.3
-- Description: Retrieves the id of the SubElementType of Date
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementTypeIdDate] ()
RETURNS INT
AS
BEGIN
	RETURN 3; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSiteSustainabilityVersions]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/16/2017
-- Release:     8.3
-- Description: Returns site sustainability versions
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSiteSustainabilityVersions] ()
RETURNS @SiteSustainabilityVersion TABLE (
    SiteId INT,
    SustainabilityVersion INT
)
AS
BEGIN

	DECLARE
        @SubSystemIdNA21GMS INT = G2.fnGetNA21GMSSubSystemId(),
        @SPA2017CriteriaStartDateParmKey VARCHAR(50) = G2.fnGetSPA2017CriteriaStartDateParmKey(),
        @SPA2017CriteriaStartDate DATETIME;

    SELECT @SPA2017CriteriaStartDate = CAST(gp.Value AS DATETIME)
    FROM G2.G2Parameters gp
    WHERE gp.ParmKey = @SPA2017CriteriaStartDateParmKey
    AND gp.SubSystemId = @SubSystemIdNA21GMS;

    INSERT @SiteSustainabilityVersion
    (
        SiteId,
        SustainabilityVersion
    )
    SELECT sm.SiteId, CASE WHEN @SPA2017CriteriaStartDate > COALESCE(smg.CreatedDt, '1/1/1900') THEN 1 ELSE 2 END
    FROM SPA.SPAMain sm
    INNER JOIN SPA.SPAMainGeneral smg ON smg.SPAMainId = sm.Id AND smg.Revision = sm.Revision;

    RETURN;
END;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetSearchAndSecureEventTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the SS EventType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetSearchAndSecureEventTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 10; -- SS
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetObligationTransactionTypeId]'
GO


-- =============================================================
-- Author:      Adela Smith
-- Created:     10/13/2014
-- Release:     5.3
-- Description: Retrieves the transaction type for obligations by
--   sub-system. This assumes that there is only one obligation
--   type in a sub-system.  
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetObligationTransactionTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	
	DECLARE @transactionTypeId int;	

	SELECT @transactionTypeId = tt.Id
	FROM Financial.TransactionType tt
	INNER JOIN Financial.SubSystemTransactionType sstt ON tt.Id = sstt.TransactionTypeId
	WHERE sstt.SubSystemId = 1--@SubSystemId
	AND tt.Name = 'Obligation';

	RETURN @transactionTypeId;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSMonitoringStrategyIdFromRMSId]'
GO


-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/21/2011
-- Description:	Returns the RMS Monitoring 
--				Strategy ID for the RMS of the
--				passed in RMS ID.
--
-- Author:		Mike Strickland
-- Create date: 12/09/2011
-- Description:	Update to return "Acceptable"
--				when two or more unarmed on-site
--				responders are selected.
--
-- =============================================
CREATE FUNCTION [SPA].[fnGetRMSMonitoringStrategyIdFromRMSId] (
	@RMSId INT
)
RETURNS INT
AS
BEGIN
	DECLARE
		@SecurityActionId_RemoteMonitoringSystem INT = 19,
		@EntityId_Room INT = 8,
		@SPASectionTypeId_Room INT = 8,
		@ResponseSubFunctionId_AuthorizedToUseDeadlyForce INT = 4,
		@RMSMonitoringStrategyId INT = NULL,
		@RMSMonitoringStrategyId_Marginal INT = 1,
		@RMSMonitoringStrategyId_Acceptable INT = 2,
		@RMSMonitoringStrategyId_Preferred INT = 3,
		@AlarmMonitorOtherId_247ArmedResponseForceDispatch INT = 4,
		@AlarmMonitorOtherId_247UnarmedResponseForceDispatch INT = 1,
		@AlarmMonitorOtherId_247NonULMonitoring INT = 2,
		@ResponseForceArmedCount INT,
		@ResponseForceUnarmedCount INT,
		@AlarmMonitorULApprovedCount INT,
		@AlarmMonitorNotULApprovedCount INT

	SELECT 
		-- Response Forces (Armed) Count
		@ResponseForceArmedCount = SUM(
			CASE COALESCE(srf.PriorityOrder, -1)
				WHEN 1 THEN
					-- Primary responder
					CASE WHEN COALESCE(rsfa.AnswerText1, '') LIKE '%yes%' THEN
						-- Armed
						1
					ELSE
						-- Unarmed
						0
					END
				WHEN 2 THEN
					-- Secondary responder
					CASE WHEN COALESCE(rsfa.AnswerText2, '') LIKE '%yes%' THEN
						-- Armed
						1
					ELSE
						-- Unarmed
						0
					END
				WHEN 3 THEN
					-- Tertiary responder
					CASE WHEN COALESCE(rsfa.AnswerText3, '') LIKE '%yes%' THEN
						-- Armed
						1
					ELSE
						-- Unarmed
						0
					END
				ELSE
					-- Other 24/7 Armed Response Force Dispatch
					CASE WHEN COALESCE(mon.AlarmMonitorOtherId, -1) = @AlarmMonitorOtherId_247ArmedResponseForceDispatch THEN
						1
					ELSE
						0
					END
			END),
		-- Response Forces (Unarmed) Count
		@ResponseForceUnarmedCount = SUM(
			CASE COALESCE(srf.PriorityOrder, -1)
				WHEN 1 THEN
					-- Primary responder
					CASE WHEN COALESCE(rsfa.AnswerText1, '') LIKE '%no%' THEN
						-- Unarmed
						1
					ELSE
						-- Armed
						0
					END
				WHEN 2 THEN
					-- Secondary responder
					CASE WHEN COALESCE(rsfa.AnswerText2, '') LIKE '%no%' THEN
						-- Unarmed
						1
					ELSE
						-- Armed
						0
					END
				WHEN 3 THEN
					-- Tertiary responder
					CASE WHEN COALESCE(rsfa.AnswerText3, '') LIKE '%no%' THEN
						-- Unarmed
						1
					ELSE
						-- Armed
						0
					END
				ELSE
					-- Other 24/7 Unarmed Response Force Dispatch
					CASE WHEN COALESCE(mon.AlarmMonitorOtherId, -1) = @AlarmMonitorOtherId_247UnarmedResponseForceDispatch THEN
						1
					ELSE
						0
					END
			END),
		-- Alarm Monitor (UL Approved) Count
		@AlarmMonitorULApprovedCount = SUM(
			CASE WHEN COALESCE(mon.AlarmMonitorCompanyServiceCityId, -1) > 0 THEN
				1
			ELSE
				0
			END),
		-- Alarm Monitor (Not UL Approved) Count
		@AlarmMonitorNotULApprovedCount = SUM(
			CASE WHEN COALESCE(mon.AlarmMonitorOtherId, -1) > 0 THEN
				CASE WHEN amo.Id = @AlarmMonitorOtherId_247NonULMonitoring THEN
					-- Alarm Monitor (Not UL Approved)
					1
				ELSE
					0
				END
			ELSE
				0
			END)
	FROM SPA.RMS rms
	INNER JOIN SPA.RMSMonitor mon ON mon.RMSId = rms.Id
	LEFT JOIN SPA.SiteResponseForce srf ON srf.Id = mon.SiteResponseForceId
	LEFT JOIN SPA.ResponseSubFunctionAnswer rsfa ON rsfa.SPAMainId = srf.SPAMainId
		AND rsfa.ResponseSubFunctionId = @ResponseSubFunctionId_AuthorizedToUseDeadlyForce
	LEFT JOIN SPA.AlarmMonitorCompanyServiceCity amcsc ON amcsc.Id = mon.AlarmMonitorCompanyServiceCityId
	LEFT JOIN SPA.AlarmMonitorOther amo ON amo.Id = mon.AlarmMonitorOtherId
	WHERE rms.Id = @RMSId
	GROUP BY rms.Id
	
	-- See RMS Signals Strategy Matrix (https://nnspad.ornl.gov/NA21/FacilityData/Inventory and SPA/SPA/RMS Signals Strategy.xls)
	-- Row 1
	IF @ResponseForceArmedCount = 1
		BEGIN
			IF @ResponseForceUnarmedCount > 0 OR @AlarmMonitorULApprovedCount > 0 OR @AlarmMonitorNotULApprovedCount > 0
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
			ELSE
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Acceptable
		END
	ELSE IF @ResponseForceArmedCount > 1 
		BEGIN
			SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
		END
	-- Row 2
	ELSE IF @ResponseForceUnarmedCount > 0
		BEGIN
			IF @AlarmMonitorULApprovedCount > 0
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
			ELSE IF @AlarmMonitorNotULApprovedCount > 0
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Marginal
			ELSE IF @ResponseForceUnarmedCount > 1
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Acceptable
		END
	-- Row 3
	ELSE IF @AlarmMonitorULApprovedCount > 0
		BEGIN
			IF @AlarmMonitorULApprovedCount > 1 OR @AlarmMonitorNotULApprovedCount > 0
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
			ELSE 
				SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Acceptable
		END
	-- Row 4
	ELSE IF @AlarmMonitorNotULApprovedCount > 1
		BEGIN
			SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Marginal
		END
			
	RETURN @RMSMonitoringStrategyId
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostOverrunNoteTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/22/2013
-- Release:     4.1
-- Description: Retrieves the Cost Overrun note type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostOverrunNoteTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Cost Overrun
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetSiteMapMarkers]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 3/12/2012
-- Description:	Returns the building coordinates
--				for a site as a single text field
--				for use in the site map feature.
--
-- mm/dd/yyyy   Name        Release  Description 
-- 08/10/2015   Strickland  6.3      Update with new function calls to get Ids
-- =============================================
CREATE FUNCTION [Inventory].[fnGetSiteMapMarkers] (
	@SiteId INT
)
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE 
		@SiteMapMarkers VARCHAR(2000),
		@SiteMapLabel VARCHAR(1),
		@Latitude DECIMAL(18,10),
		@Longitude DECIMAL(18,10),
		@SPAMainId INT,
		@SPASectionTypeIdBuildingInformation INT = SPA.fnGetSPASectionTypeIdBuildingInformation()
		
	SELECT @SPAMainId = Id
	FROM SPA.SPAMain
	WHERE SiteId = @SiteId
		
	DECLARE SiteMapMarkersCursor CURSOR LOCAL FORWARD_ONLY READ_ONLY
		FOR
	SELECT 
		b.Latitude, b.Longitude, 
		CASE WHEN ROW_NUMBER() OVER(ORDER BY b.[Name]) < 27 THEN
			-- Site Map Labels for first 27 buildings A-Z 
			CHAR(ROW_NUMBER() OVER(ORDER BY b.[Name]) + 64)
		ELSE
			''
		END SiteMapLabel
	FROM SPA.SPAMain SPAMain
	INNER JOIN G2.Building b ON b.SiteId = SPAMain.SiteId
	INNER JOIN SPA.SPAMainSection sms ON sms.SPAMainId = SPAMain.Id 
		AND sms.SPASectionTypeId = @SPASectionTypeIdBuildingInformation
		AND sms.RefId = b.Id
	WHERE b.SiteId = @SiteId
	AND b.DeletedInd = 0
	AND sms.IsIncludedInd = 1
	AND b.CoordinateAccuracyId > 5 
	ORDER BY b.[Name]

	SET @SiteMapMarkers = ''
	
	OPEN SiteMapMarkersCursor

	FETCH NEXT FROM SiteMapMarkersCursor INTO @Latitude, @Longitude, @SiteMapLabel
		
	WHILE (@@FETCH_STATUS = 0)
		BEGIN
			-- Latitude and longitude are separated by a comma
			-- Latitude and longitude pairs are separated by the pipe which is URL encoded as &7C
			SET @SiteMapMarkers = @SiteMapMarkers + 
				'&markers=color:blue%7Clabel:' + @SiteMapLabel + '%7C' +
				SUBSTRING(CAST(@Latitude AS VARCHAR), 1, CHARINDEX('.', @Latitude) + 6)  + ',' + 
				SUBSTRING(CAST(@Longitude AS VARCHAR), 1, CHARINDEX('.', @Longitude) + 6) + '%7C'

			FETCH NEXT FROM SiteMapMarkersCursor INTO @Latitude, @Longitude, @SiteMapLabel
		END
	
	CLOSE SiteMapMarkersCursor

	DEALLOCATE SiteMapMarkersCursor

	RETURN @SiteMapMarkers
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetEmployeePartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/21/2013
-- Release:     4.4
-- Description: Retrieves the employee party role type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetEmployeePartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Employee
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetRMSMonitoringStrategyId]'
GO


-- =============================================
-- Author:		Mike Strickland
-- Create date: 10/21/2011
-- Description:	Returns the RMS Monitoring 
--				Strategy ID
-- =============================================
CREATE FUNCTION [SPA].[fnGetRMSMonitoringStrategyId] (
	@RMSId INT
)
RETURNS INT
AS
BEGIN
	DECLARE
		------------------------------------------------------------------------
		--@RMSId INT = 8, -- don't forget to delete this line or comment it out
		------------------------------------------------------------------------
		@SecurityActionId_RemoteMonitoringSystem INT = 19,
		@EntityId_Room INT = 8,
		@SPASectionTypeId_Room INT = 8,
		@ResponseSubFunctionId_AuthorizedToUseDeadlyForce INT = 4,
		--@ResponseSubFunctionId_WeaponsLethal INT = 8,
		@RMSMonitoringStrategyId INT = -1,
		@RMSMonitoringStrategyId_Marginal INT = 1,
		@RMSMonitoringStrategyId_Acceptible INT = 2,
		@RMSMonitoringStrategyId_Preferred INT = 3,
		@AlarmMonitorOtherId_247UnarmedResponseForceDispatch INT = 1,
		@AlarmMonitorOtherId_247NonULMonitoring INT = 2,
		@SiteResponseForceArmedCount INT,
		@ULApprovedMonitorCount INT,
		@Other247MonitorCount INT

	SELECT 
		-- Armed Responder from Site Response Forces
		@SiteResponseForceArmedCount = SUM(CASE COALESCE(srf.PriorityOrder, -1)
			WHEN 1 THEN
				-- Primary responder
				CASE WHEN rsfa.AnswerText1 LIKE '%yes%' THEN
					-- Armed
					1
				ELSE
					-- Unarmed
					0
				END
			WHEN 2 THEN
				-- Secondary responder
				CASE WHEN rsfa.AnswerText2 LIKE '%yes%' THEN
					-- Armed
					1
				ELSE
					-- Unarmed
					0
				END
			WHEN 3 THEN
				-- Tertiary responder
				CASE WHEN rsfa.AnswerText3 LIKE '%yes%' THEN
					-- Armed
					1
				ELSE
					-- Unarmed
					0
				END
			ELSE
				NULL
		END),
		-- UL approved monitor
		@ULApprovedMonitorCount = SUM(CASE WHEN COALESCE(mon.AlarmMonitorCompanyServiceCityId, -1) > 0 THEN
			1
		ELSE
			0
		END),
		-- Other 24/7 alarm monitor
		@Other247MonitorCount = SUM(CASE WHEN COALESCE(mon.AlarmMonitorOtherId, -1) > 0 THEN
			CASE WHEN amo.Id IN (@AlarmMonitorOtherId_247UnarmedResponseForceDispatch, @AlarmMonitorOtherId_247NonULMonitoring) THEN
				-- 24/7 unarmed response force dispatch or 24/7 non-UL approved monitor
				1
			ELSE
				0
			END
		ELSE
			0
		END)
	FROM SPA.RMS rms
	INNER JOIN SPA.RMSMonitor mon ON mon.RMSId = rms.Id
	LEFT JOIN SPA.SiteResponseForce srf ON srf.Id = mon.SiteResponseForceId
	LEFT JOIN SPA.ResponseSubFunctionAnswer rsfa ON rsfa.SPAMainId = srf.SPAMainId
		AND rsfa.ResponseSubFunctionId = @ResponseSubFunctionId_AuthorizedToUseDeadlyForce
	LEFT JOIN SPA.AlarmMonitorCompanyServiceCity amcsc ON amcsc.Id = mon.AlarmMonitorCompanyServiceCityId
	LEFT JOIN SPA.AlarmMonitorOther amo ON amo.Id = mon.AlarmMonitorOtherId
	WHERE rms.Id = @RMSId
	GROUP BY rms.Id
	
	IF @SiteResponseForceArmedCount + @ULApprovedMonitorCount > 1
		-- Preferred - One or more armed responders or one or more UL approved monitors
		SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
	ELSE IF (@SiteResponseForceArmedCount + @ULApprovedMonitorCount = 1) AND @Other247MonitorCount > 0
		-- Preferred - One armed responders or one UL approved monitors and one or more other other 24/7 monitor
		SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Preferred
	ELSE IF (@SiteResponseForceArmedCount + @ULApprovedMonitorCount = 1) AND @Other247MonitorCount = 0
		-- Acceptible - One armed responders or one UL approved monitors and no other other 24/7 monitor
		SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Acceptible
	ELSE IF (@SiteResponseForceArmedCount + @ULApprovedMonitorCount = 0) AND @Other247MonitorCount > 1
		-- Marginal - No armed responders and no UL approved monitors and more than one other other 24/7 monitor
		SET @RMSMonitoringStrategyId = @RMSMonitoringStrategyId_Marginal
	ELSE
		-- Doesn't meet any criteria
		SET @RMSMonitoringStrategyId = -1
		
	--SELECT @RMSId, @SiteResponseForceArmedCount, @ULApprovedMonitorCount, @Other247MonitorCount, @RMSMonitoringStrategyId
	
	RETURN COALESCE(@RMSMonitoringStrategyId, -1)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetGammaWeightTypeId]'
GO

-- =============================================================
-- Author:      Nicole Harris
-- Created:     5/11/2016 
-- Release:     7.1
-- Description: Returns the Gamma WeightTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [MDI].[fnGetGammaWeightTypeId]()
RETURNS int
AS
BEGIN
  RETURN 3; --Gamma
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetDependenciesMatrixTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     05/11/2016 
-- Release:     7.1
-- Description: Retrieves the Dependencies Matrix Type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetDependenciesMatrixTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 2; -- Dependencies
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetCapabilitiesMatrixTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     05/11/2016 
-- Release:     7.1
-- Description: Retrieves the Capabilities Matrix Type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetCapabilitiesMatrixTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 1; -- Capabilities
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetBetaWeightTypeId]'
GO

-- =============================================================
-- Author:      Nicole Harris
-- Created:     5/11/2016 
-- Release:     7.1
-- Description: Returns the Beta WeightTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [MDI].[fnGetBetaWeightTypeId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Beta
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetAlphaWeightTypeId]'
GO

-- =============================================================
-- Author:      Nicole Harris
-- Created:     5/11/2016 
-- Release:     7.1
-- Description: Returns the Alpha WeightTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [MDI].[fnGetAlphaWeightTypeId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Alpha
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetUncostedBalanceValidationMethod]'
GO


-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/21/2017
-- Release:     7.5
-- Description: Returns the uncosted balance funding validation method 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetUncostedBalanceValidationMethod]
(
)
RETURNS INT
AS
BEGIN
	RETURN 2;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ReportParameter].[fnConvertToCamelCase]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/21/2016
-- Release:     7.3
-- Description: Converts input text to camel case. 
--              Reference: http://stackoverflow.com/questions/230138/sql-server-make-all-upper-case-to-proper-case-title-case
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE function [ReportParameter].[fnConvertToCamelCase]
(
	@InputString as varchar(8000)
)
RETURNS VARCHAR(8000)
AS 
BEGIN 

	/* Debug Declarations */
	--DECLARE @InputString VARCHAR(8000) = 'THIS IS AN INPUT STRING; WITH A SEMI-COLON.'
	/* Debug Declarations */
	DECLARE @changeCase BIT = 1,
			@returnString VARCHAR(8000) = '',
			@currentCharIndex INT = 1,
			@textLength INT = LEN(@InputString),
			@charToEvaluate CHAR(1);

	WHILE (@currentCharIndex <= LEN(@InputString))
	BEGIN	
		SELECT @charToEvaluate= SUBSTRING(@InputString, @currentCharIndex, 1),
			   @returnString = @returnString + CASE WHEN @changeCase = 1 THEN UPPER(@charToEvaluate) ELSE LOWER(@charToEvaluate) END,
			   @changeCase = CASE WHEN @charToEvaluate LIKE '[a-zA-Z]' THEN 0 ELSE 1 END,
			   @currentCharIndex = @currentCharIndex +1;
	END;
	--SELECT @returnString;
	RETURN @returnString;

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[fnStoredProcedureWhereUsed]'
GO

-- =============================================================
-- Author:      BScott
-- Created:     09/07/2010
-- Description: Function to return a list of reports that use a 
--	given stored procedure.
--
-- mm/dd/yy  Name     Version		Description
-- 04/24/13  DNC      4.1           Point to new ReportServer. 
-- 04/26/13  DNC      4.1           Update to use Alias
-- 10/20/14  DNC      5.3           Update to look at other G2 Subsystem folders,
--                                  include 2010 element namespace
--                                  and to pull new SSRS catalog types
-- 06/21/16  b.roland 7.1           Updated to exclude Infrastructure Planning Project Data Export
--                                  until we can fix the issue
-- 03/17/17  a.smith  8.0           Use TRY_CAST instead of filtering out specific reports that
--                                  cause errors
-- 03/17/17  b.roland 8.0           Updated to include specific reports that cause errors
-- 03/20/17  a.smith  8.0           Increased size of NameSpaceVersion field in return table as 
--                                  the function was failing.
-- 05/10/17  a.smith  8.0           Added 2016 namespace query
-- =============================================================
CREATE FUNCTION [Tools].[fnStoredProcedureWhereUsed]
(
	@StoredProcedureName NVARCHAR(512)
)
RETURNS @Report TABLE 
(	
	[NameSpaceVersion] NVARCHAR(8),
	[PATH] nVARCHAR(425) NOT NULL,
	[Name] nVARCHAR(425) NOT NULL,
	[Description] nVARCHAR(512)
)
AS
BEGIN

	/* Begin Debug Declarations */
		--DECLARE @StoredProcedureName NVARCHAR(512) = 'spPlanning';
		--DECLARE @Report TABLE
		--	(  [NameSpaceVersion] NVARCHAR(8),
		--	  [PATH] NVARCHAR(425) NOT NULL
		--	, [Name] NVARCHAR(425) NOT NULL
		--	, [Description] NVARCHAR(512)
		--	);
	/* End Debug Declarations */

	--SET STATISTICS TIME ON;
    WITH CatalogWithXML ( ItemId, [Path], [Name], [Description], [Hidden], xContent )AS 
	( 
		SELECT   [C].[ItemID], [C].[Path], [C].[Name], [C].[Description], [C].[Hidden],
                            TRY_CAST(CAST([C].[Content] AS VARBINARY(MAX)) AS XML) AS xContent
                   FROM     Report.ReportServerCatalog AS C
                   WHERE    [C].[Type] IN ( 2, 4, 6 )
                            AND ( [C].[Path] LIKE '/G2%'
                                  OR [C].[Path] LIKE '/NA%'
                                  OR [C].[Path] LIKE '/SubSystem%'
                                )
					AND [C].[Content] IS NOT NULL
     )
    INSERT @Report
	SELECT 'Unknown' [NameSpaceVersion], CWX.[Path], CWX.[Name], CWX.[Description]
	FROM CatalogWithXML AS CWX
	WHERE CWX.xContent IS NULL

	UNION

    SELECT '2016' [NameSpaceVersion], CWX.[Path], CWX.[Name], CWX.[Description]--, Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition"; ./CommandText[1]', 'varchar(255)') AS CommandText
    FROM CatalogWithXML AS CWX
    CROSS APPLY 
		CWX.xContent.nodes('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2016/01/reportdefinition"; //Query') AS C2 ( Loc )
    WHERE Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2016/01/reportdefinition"; ./CommandText[1]',
                    'varchar(255)') LIKE '%' + @StoredProcedureName + '%'

	UNION

    SELECT '2010' [NameSpaceVersion], CWX.[Path], CWX.[Name], CWX.[Description]--, Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition"; ./CommandText[1]', 'varchar(255)') AS CommandText
    FROM CatalogWithXML AS CWX
    CROSS APPLY 
		CWX.xContent.nodes('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition"; //Query') AS C2 ( Loc )
    WHERE Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition"; ./CommandText[1]',
                    'varchar(255)') LIKE '%' + @StoredProcedureName + '%'

	UNION

    SELECT '2008' [NameSpaceVersion], CWX.[Path], CWX.[Name], CWX.[Description]--, Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition"; ./CommandText[1]', 'varchar(255)') AS CommandText
    FROM CatalogWithXML AS CWX
    CROSS APPLY 
		CWX.xContent.nodes('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition"; //Query') AS C2 ( Loc )
    WHERE   Loc.value('declare default element namespace "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition"; ./CommandText[1]',
                      'varchar(255)') LIKE '%' + @StoredProcedureName + '%';

	--SELECT * FROM @Report r;

	--SET STATISTICS TIME OFF;
	RETURN;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnGetActivePlanningStatusId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     02/01/2016 
-- Release:     6.5
-- Description: Returns the Active PlanningStatusId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [BudgetFormulation].[fnGetActivePlanningStatusId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Active
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetFunctionalAreaAssessmentHierarchyLevelId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     07/30/2016
-- Release:     7.2
-- Description: Retrieves the functional area assessment hierarchy level id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetFunctionalAreaAssessmentHierarchyLevelId] ()
RETURNS INT
AS
BEGIN
	RETURN 2;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetHQProgramOfficeClassificationId]'
GO
-- =============================================================
-- Author:      Dakota Abston
-- Created:     06/29/2018 
-- Release:     9.1
-- Description: Returns the HQ Program Office ClassificationId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetHQProgramOfficeClassificationId]()
RETURNS INT
AS
BEGIN
  RETURN 4; --HQ Program Office
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ReportParameter].[fnGetDefaultReports]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/10/2016
-- Release:     7.2
-- Description: Selects teh default reports for a given subsystem  & FY
--
-- mm/dd/yy  Name     Release  Description
-- 08/28/17  asmith     8.2    Removed reference to the G2 database
-- =============================================================
CREATE FUNCTION [ReportParameter].[fnGetDefaultReports](@SubSystemId INT, @FiscalYear INT)
RETURNS @reports TABLE (ReportId INT, ReportName VARCHAR(50))
AS
BEGIN

    DECLARE @PlanningReportId INT = G2.fnGetPlanningReportId(@SubSystemId, @FiscalYear);
    DECLARE @NA50ExecutiveSummaryPackageReportId INT = G2.fnGetNA50ExecutiveSummaryPackageReportIdReportId(@SubSystemId, @FiscalYear);
    
    INSERT INTO @reports (ReportId, ReportName)
	SELECT 
        r.Id AS ReportId,
        r.Name AS ReportName
    FROM G2.Report r
    WHERE r.Id IN(@PlanningReportId, @NA50ExecutiveSummaryPackageReportId);
  RETURN
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPlanningYearOutOfRangeErrorTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     06/02/2016
-- Release:     7.1
-- Description: Retrieves Planning Year Out of Range ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPlanningYearOutOfRangeErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 53; -- Planning Year Out of Range
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetOverwriteExistingValueErrorTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     06/02/2016
-- Release:     7.1
-- Description: Retrieves Overwrite Existing Value ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetOverwriteExistingValueErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 65; -- Overwrite Existing Value
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetAmountCannotBeNegativeErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves Amount Cannot Be Negative error type
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetAmountCannotBeNegativeErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 36; -- Amount Cannot Be Negative
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetMinimumAppropriationStartFY]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     08/14/2015
-- Release:     6.2
-- Description: Retrieves the Minimum Appropriation Start FY.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetMinimumAppropriationStartFY]
(
	@SubSystemId int
)
RETURNS INT
AS
BEGIN
	DECLARE @fiscalYear INT;

	SELECT @fiscalYear = CAST(Value AS INT)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey = 'Minimum Appropriation Start FY'
	AND SubSystemId = 3;

	RETURN @fiscalYear;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetResetLineItemStatusId]'
GO

-- =============================================================
-- Author:      Chris O'Neal
-- Created:     4/22/2013
-- Release:     4.1
-- Description: Retrieves the Reset line item status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetResetLineItemStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 5; -- Reset
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetUnassignedAssetGroupId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/23/2017
-- Release:     8.1
-- Description: Gets the unassigned Asset Group.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetUnassignedAssetGroupId]()
RETURNS
INT
AS
BEGIN
	RETURN 1; -- Unassigned AssetGroup
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetOwnershipClassificationId]'
GO
-- =============================================================
-- Author:      Dakota Abston
-- Created:     06/29/2018 
-- Release:     9.1
-- Description: Returns the Ownership ClassificationId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetOwnershipClassificationId]()
RETURNS INT
AS
BEGIN
  RETURN 5; --Ownership
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetOperatingStatusClassificationId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/25/2018 
-- Release:     9.1
-- Description: Returns the Operating Status ClassificationId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetOperatingStatusClassificationId]()
RETURNS int
AS
BEGIN
  RETURN 3; --Operating Status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetFinPlanProcessDatesforPeriodId]'
GO

-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 1/4/13	Release 3.7
-- Description:	Returns list of distinct 'active' or 'processed' 
-- status dates for selected fiscal period.  Includes all processed items,
--	but only those active items ready for the Budget Officer
-- =============================================
CREATE FUNCTION [Report].[fnGetFinPlanProcessDatesforPeriodId] 
(
	@PeriodId INT,
	@IsPreview BIT  
)
RETURNS 
@ActiveOrProcessDateList TABLE 
(
  id INT IDENTITY(1,1) NOT NULL,
	ProcessDate DATE 
)
AS
BEGIN
/* Debug Declarations */
--DECLARE @PeriodId INT = 222, @IsPreview BIT = 0
--DECLARE @ActiveOrProcessDateList TABLE (id INT IDENTITY(1,1) NOT NULL, ProcessDate DATE);
/* End Debug Declarations */


		INSERT INTO  @ActiveOrProcessDateList  (ProcessDate)
		SELECT  DISTINCT CAST(StatusDt AS DATE) ProcessDate  
		--ChangeRequestTypeId, WorkflowLevel,
		--crd.ChangeRequestDetailStatusId, 
		--ChangeRequestDetailId,
		--Amount, LoanMemoInd, AppropriationYear,
		--FundingSourceId, FinPlanRequestId, ProjectId, BNRId, FundTypeId, LabId, FromInd,
		--va.ChangeRequestDetailStatusId, va.ChangeRequestDetailStatusName,
		--vp.ChangeRequestDetailStatusId, vp.ChangeRequestDetailStatusName
FROM g2.ChangeRequestDetail crd
INNER JOIN Financial.FinPlanRequest fpr ON crd.Id = fpr.ChangeRequestDetailId
INNER JOIN Financial.FinPlanRequestDetail fprd ON fpr.Id = fprd.FinPlanRequestId
LEFT OUTER JOIN g2.vwChangeRequestDetailStatusActiveStates va ON crd.ChangeRequestDetailStatusId = va.ChangeRequestDetailStatusId AND @IsPreview = 1
LEFT OUTER JOIN g2.vwChangeRequestDetailStatusProcessedStates vp ON crd.ChangeRequestDetailStatusId = vp.ChangeRequestDetailStatusId AND @IsPreview = 0
WHERE crd.PeriodId = @PeriodId
AND ((va.ChangeRequestDetailStatusId IS NOT NULL AND crd.WorkflowLevel = 5) OR vp.ChangeRequestDetailStatusId IS NOT null)
ORDER BY CAST(StatusDt AS DATE)

	--SELECT * FROM @ActiveOrProcessDateList
	RETURN ;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Travel].[fnGetRejectedTripStatusId]'
GO

-- =============================================================
-- Author:      Jason Frank
-- Created:		9/25/2013
-- Release:     4.4
-- Description: Retrieves the Rejected TripStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Travel].[fnGetRejectedTripStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 2; -- Rejected.
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21UnitedStatesWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2014
-- Release:     5.3
-- Description: Returns the value for the NA-21 WorkBreakdownStructure 
--              Id for the United States portfolio.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21UnitedStatesWorkBreakdownStructureId]()
RETURNS INT
AS
BEGIN

	RETURN 6;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21GMSUnitedStatesWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2014
-- Release:     5.3
-- Description: Returns the value for the NA-21 WorkBreakdownStructure 
--              Id for the United States portfolio.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21GMSUnitedStatesWorkBreakdownStructureId]()
RETURNS INT
AS
BEGIN
	DECLARE @wbsId INT,
			@na21GMSSubSystemId INT = G2.fnGetNA21GMSSubSystemId();

	SELECT @wbsId = wbs.Id
	FROM G2.WorkBreakdownStructure wbs
	WHERE wbs.SubSystemId = @na21GMSSubSystemId
	AND wbs.Name LIKE 'United States%';

	/* Debug */
	--SELECT @wbsId;
	/* Debug */

	RETURN @wbsId;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetVoidedParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Voided ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetVoidedParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 7; -- Voided
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetRejectedParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Rejected ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetRejectedParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 6; -- Rejected
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetParticipantAttendeeCategoryId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the Participant AttendeeCategory Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetParticipantAttendeeCategoryId] ()
RETURNS INT
AS
BEGIN
	RETURN 1; -- Participant
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetLEAParticipantTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the 'LEA' ParticipantType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetLEAParticipantTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- LEA
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Planning].[fnGetTravelElementOfCostTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     05/26/2016
-- Release:     7.1
-- Description: Retrieves the Travel Element of Cost Type Id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Planning].[fnGetTravelElementOfCostTypeId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Travel
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Planning].[fnGetLaborElementOfCostTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     05/26/2016
-- Release:     7.1
-- Description: Retrieves the Labor Element of Cost Type Id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Planning].[fnGetLaborElementOfCostTypeId]()
RETURNS int
AS
BEGIN
	RETURN 1; -- Labor
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Planning].[fnGetContractsElementOfCostTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     05/26/2016
-- Release:     7.1
-- Description: Retrieves the Contracts Element of Cost Type Id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Planning].[fnGetContractsElementOfCostTypeId]()
RETURNS int
AS
BEGIN
	RETURN 3; -- Contracts
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetReportingEntityOrganizationTypeId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     01/21/2014
-- Release:     5.0
-- Description: Retrieves the reporting entity organization type
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetReportingEntityOrganizationTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Reporting Entity
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetReportServerBaseUploadPath]'
GO
-- =============================================
-- Author:		Mike Strickland
-- Create date: 2/19/2010
-- Description:	Returns the report server base
--				upload path
--
-- mm/dd/yyyy   Name        Release  Description 
-- 05/15/2013   Strickland  4.1      Change "M:\G2Uploads" to "\\NNSTGDB-01.ORNL.GOV\G2Uploads"
-- 06/14/2013   CGL         4.2      Change hard coded servername into Property retrival
-- =============================================
CREATE FUNCTION [SPA].[fnGetReportServerBaseUploadPath]()
RETURNS VARCHAR(50)
AS
BEGIN
	RETURN '\\'+
	CONVERT(VARCHAR(40),SERVERPROPERTY('MachineName'))
	+'.ORNL.GOV\G2Uploads\'
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetHistoricDesignationClassificationId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/19/2018 
-- Release:     9.1
-- Description: Returns the Historic Designation ClassificationId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetHistoricDesignationClassificationId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Historic Designation
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BudgetFormulation].[fnGetInactivePlanningStatusId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     02/01/2016 
-- Release:     6.5
-- Description: Returns the Inactive PlanningStatusId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [BudgetFormulation].[fnGetInactivePlanningStatusId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Inactive
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Workflow].[fnGetMaxWorkflowLevel]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/08/2014
-- Release:     5.3
-- Description: Returns the maximum workflow level for a given
--              Change Request Type for a given SubSysetem
--
-- mm/dd/yyyy   Name        Release  Description 
-- =============================================================
CREATE FUNCTION [Workflow].[fnGetMaxWorkflowLevel](
	@SubSystemId INT,
	@ChangeRequestTypeId INT
)
RETURNS @MaxWorkflowTable TABLE (
	MaxWorkflowLevel INT
)
AS
BEGIN

	INSERT INTO @MaxWorkflowTable
	SELECT COALESCE(MAX(crtc.WorkflowLevel), 0) FROM G2.SubSystemChangeRequestType sscrt
	INNER JOIN G2.ChangeRequestType crt ON crt.Id = sscrt.ChangeRequestTypeId
	INNER JOIN G2.ChangeRequestTypeConfig crtc ON crtc.SubSystemChangeRequestTypeId = sscrt.Id
	WHERE crt.Id = @ChangeRequestTypeId
	AND sscrt.SubSystemId = @SubSystemId
	AND crtc.AutoProcessInd = 0

	RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetNoCustomSpendPlanErrorTypeId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     1/10/2018
-- Release:     8.4
-- Description: Retrieves no custom spend plan error type
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetNoCustomSpendPlanErrorTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 72;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBaselineChangeRequestInProgressErrorTypeId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     1/10/2018
-- Release:     8.4
-- Description: Retrieves the BCR in approval workflow error type
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetBaselineChangeRequestInProgressErrorTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 71;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetFinPlanTeamLeadPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Diedre Nickum
-- Created:     7/14/2018
-- Release:     9.1
-- Description: Returns the RoleId for FinPlan: Team Lead (Primary)
--              No SubSystemId Parameter at this time as only NA24
--              has this role 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetFinPlanTeamLeadPrimaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 187; 

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by SPA Protect Technical Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead] ()
RETURNS int
AS
BEGIN
	RETURN 7; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetInitialParticipantStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:		1/24/2014
-- Release:     5.0
-- Description: Retrieves the Initial ParticipantStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetInitialParticipantStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 1; -- Initial
END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSiteInformation]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Site Information.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSiteInformation] ()
RETURNS int
AS
BEGIN
	RETURN 1; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[fnCreateSortableWBS]'
GO
-- =============================================================
-- Author:      Nathan Coffey/Jocelyn Borgers
-- Created:     06/05/2015
-- Release:     6.1
-- Description: Utility for creating a zero padded aka sortable WBS
--
-- mm/dd/yy  Name     Release  Description
-- 08/11/15  s.oneal  6.1.1    Decreased the sortable WBS part length from 5 chars to 4 chars; we were running into
--                             issues where the size of the field was being exceeded because there were so many
--                             levels in the WBS hierarchy
-- 03/28/16  j.rochat 7.0      Increased size of FullWBS and SortableWBS from 50 to 100
-- =============================================================
CREATE FUNCTION [Tools].[fnCreateSortableWBS]
(
	@FullWBS NVARCHAR(100)
)
RETURNS NVARCHAR(100)
AS
BEGIN
	RETURN STUFF (
					(
							SELECT '.' + REPLACE(STR(ABS(fpl.ID), 4), ' ', '0')
							FROM G2.fnParseList(@FullWBS,'.') fpl
							ORDER BY fpl.SortOrder
							FOR XML PATH(''), TYPE
					).value('.', 'varchar(max)'),1,1,''
				);

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[fnGetServiceResponseStatusIdByHttpStatusCode]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/17/2013
-- Release:     4.2
-- Description: Retrieves the appropriate service response
--   status id for the specified HTTP status code.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Audit].[fnGetServiceResponseStatusIdByHttpStatusCode] (
	@HttpStatusCode int
)
RETURNS int
AS
BEGIN
	RETURN CASE WHEN @HttpStatusCode BETWEEN 200 AND 299 THEN 1 -- Success
				ELSE 3 -- Failure
		   END;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetTravelerPartyRoleTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/21/2013
-- Release:     4.4
-- Description: Retrieves the traveler party role type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetTravelerPartyRoleTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Traveler
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ReportParameter].[fnGetWbsDescriptionPermission]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/3/2016
-- Release:     
-- Description: Gets user's permission as to whether or not they 
--              may see the WBS Description
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [ReportParameter].[fnGetWbsDescriptionPermission](@SubSystemId INT, @UserId INT)
RETURNS BIT
AS
BEGIN
    DECLARE 
        @UserMaySeeWBSDescription BIT = 0, 
        @ViewPermissionId INT = Security.fnGetViewPermissionId(),
        @WBS_DASHBOARD_DETAILS_DESCRIPTION__PermissionObjectId INT = 141;

    IF EXISTS(
        SELECT permRole.Id
        FROM Security.PermissionRole permRole
        INNER JOIN Security.PersonRole persRole ON persRole.RoleId = permRole.RoleId --persRole.RoleId = r.Id
        INNER JOIN Security.Permission p ON p.Id = permRole.PermissionId
        INNER JOIN Security.PermissionObject po ON po.Id = permRole.PermissionObjectId
        WHERE persRole.PersonId = @UserId
        AND permRole.PermissionId = @ViewPermissionId
        AND po.Id = @WBS_DASHBOARD_DETAILS_DESCRIPTION__PermissionObjectId
        AND permRole.SubSystemId = @SubSystemId
    ) BEGIN 
        SET @UserMaySeeWBSDescription = 1;
    END;

    RETURN @UserMaySeeWBSDescription;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdFileUpload]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for FileUpload.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdFileUpload] ()
RETURNS int
AS
BEGIN
	RETURN 10; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Travel].[fnGetCancelledTripStatusId]'
GO

-- =============================================================
-- Author:      Jason Frank
-- Created:		9/25/2013
-- Release:     4.4
-- Description: Retrieves the Cancelled TripStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Travel].[fnGetCancelledTripStatusId]()
RETURNS INT 
AS
BEGIN
	RETURN 3; -- Cancelled.
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdMonitoring]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     12/1/2017
-- Release:     8.4
-- Description: Retrieves the id for Monitoring
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdMonitoring] ()
RETURNS INT
AS
BEGIN
	RETURN 24; 
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdIDDSurvey]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of IDD Survey.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdIDDSurvey] ()
RETURNS int
AS
BEGIN
	RETURN 15; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetADATargetImportBatchTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     07/16/2014
-- Release:     5.2
-- Description: Retrieves the BatchType Id for the ADA Target
--   Import.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetADATargetImportBatchTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- ADA Target Import
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[fnGetSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     06/05/2015
-- Release:     6.1
-- Description: Utility for getting subsystem level ID for a subsystem provided the label
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Tools].[fnGetSubSystemLevelId]
(
	@SubSystemId INT,
	@Label NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	RETURN  (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = @Label
			AND ssl.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetSpendPlanAssumptionsWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/13/2014 
-- Release:     5.0
-- Description: Returns the Spend Plan Assumptions project attribute type Id
--
-- mm/dd/yy  Name     Release  Description
-- 09/25/14  a.smith  5.3      Renamed from fnGetSpendPlanAssumptionsProjectAttributeTypeId
--                             and moved to Financial schema.
-- =============================================================
CREATE FUNCTION [Financial].[fnGetSpendPlanAssumptionsWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 16; --Spend Plan Assumptions
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetNoneSpendPlanTypeId]'
GO



-- =============================================================
-- Author:      Chad Gilbert
-- Created:     8/19/15
-- Release:     6.2
-- Description: Retrieves the none spend plan type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetNoneSpendPlanTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1; --none spend plan type id
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostPastDueDates]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     05/02/2013
-- Release:     4.1
-- Description: This function returns the date when the past due notifications
--              should go out for each subsystem.
--
-- mm/dd/yy  Name      Release  Description
-- 05/14/13  m.brown   4.1      Updated to get the period AFTER the current cost period for due date
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- 04/10/14  n.williford 5.0    Rewritten to return a table instead of a scalar value.
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostPastDueDates]()
RETURNS @SubSystemCostsPastDueDateTable TABLE (SubSystemId INT, CostPeriodId INT, CostsPastDueDate DATETIME)
AS
BEGIN
    ---------------------------------------------------------------------------
    -- Local variables.
    ---------------------------------------------------------------------------
    DECLARE @currentCostPeriodParmKey VARCHAR(50) = G2.fnGetCurrentCostPeriodParmKey(),
        @costSubmissionLateDay VARCHAR(50) = G2.fnGetCostSubmissionLateDayParmKey();

    DECLARE @SubSystemCostPeriodTable TABLE (SubSystemId INT, CostPeriodId INT);
    DECLARE @SubSystemLateDayTable TABLE (SubSystemId INT, LateDay INT);
    DECLARE @SubSystemNextPeriodTable TABLE (SubSystemId INT, NextPeriodId INT);

    ---------------------------------------------------------------------------
    -- Get subsystem and current cost period.
    ---------------------------------------------------------------------------
    INSERT INTO @SubSystemCostPeriodTable (SubSystemId, CostPeriodId)
    SELECT gp.SubSystemId, CONVERT(INT, gp.Value) AS CostPeriodId
    FROM G2.G2Parameters gp
    WHERE gp.ParmKey = @currentCostPeriodParmKey
    
    ---------------------------------------------------------------------------
    -- Get subsystem and late day.
    ---------------------------------------------------------------------------
    INSERT INTO @SubSystemLateDayTable (SubSystemId, LateDay)
    SELECT gp.SubSystemId, CONVERT(INT, gp.Value) AS LateDay
    FROM G2.G2Parameters gp
    WHERE gp.ParmKey = @costSubmissionLateDay

    ---------------------------------------------------------------------------
    -- Get subsystem and next period id.
    ---------------------------------------------------------------------------
    INSERT INTO @SubSystemNextPeriodTable
    SELECT sscpt.SubSystemId, G2.fnGetNextPeriodId(sscpt.CostPeriodId, 0) AS NextPeriodId
    FROM G2.Period p
    INNER JOIN @SubSystemCostPeriodTable sscpt ON sscpt.CostPeriodId = p.Id;

    ---------------------------------------------------------------------------
    -- Get subsystem and costs past due date.
    ---------------------------------------------------------------------------
    INSERT INTO @SubSystemCostsPastDueDateTable (SubSystemId, CostPeriodId, CostsPastDueDate)
    SELECT ssldt.SubSystemId, sscpt.CostPeriodId, DATEADD(DAY, ssldt.LateDay - 1, p.BeginDt) AS CostsPastDueDate
    FROM G2.Period p
    INNER JOIN @SubSystemNextPeriodTable ssnpt ON ssnpt.NextPeriodId = p.Id
    INNER JOIN @SubSystemCostPeriodTable sscpt ON sscpt.SubSystemId = ssnpt.SubSystemId
	INNER JOIN @SubSystemLateDayTable ssldt ON ssldt.SubSystemId = sscpt.SubSystemId

    RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPARevisionLastRejectDate]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 05/21/2014
-- Release:     5.1
-- Description:	Returns the last date that a SPA
--				was rejected
--
-- mm/dd/yyyy   Name        Release  Description 
-- 08/07/2015   Strickland  6.3    Update for new roles
-- =============================================
CREATE FUNCTION [SPA].[fnGetSPARevisionLastRejectDate] (
	@SiteId INT,
	@Revision INT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE
        @SPAStatusIdRejectedbySPATeamLead INT = SPA.fnGetSPAStatusIdRejectedbySPATeamLead(),
        @SPAStatusIdRejectedbySPAPortfolioManager INT = SPA.fnGetSPAStatusIdRejectedbySPAPortfolioManager(),
        @SPAStatusIdRejectedbySPASubProgramManager INT = SPA.fnGetSPAStatusIdRejectedbySPASubProgramManager(),
        @SPAStatusIdRejectedbySPALabLead INT = SPA.fnGetSPAStatusIdRejectedbySPALabLead(),
        @SPAStatusIdRejectedbySPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead(),
        @LastRejectDate DATETIME

	SELECT @LastRejectDate = COALESCE(MAX(LastModifiedDt), '1/1/1900')
	FROM SnapShot.SPAMain 
	WHERE SiteId = @SiteId
	AND Revision = @Revision
	AND SPAStatusId IN (
		@SPAStatusIdRejectedBySPATeamLead,
		@SPAStatusIdRejectedBySPAPortfolioManager,
		@SPAStatusIdRejectedBySPASubProgramManager,
		@SPAStatusIdRejectedBySPALabLead,
		@SPAStatusIdRejectedBySPAProtectTechnicalLead
	)
	
	RETURN @LastRejectDate

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetUnconstrainedBudgetCannotBeAssignedErrorTypeId]'
GO

-- =============================================================
-- Author:      Chad Gilbert
-- Created:     06/18/2015
-- Release:     6.1
-- Description: Retrieves Unconstrained Budget Cannot be Assigned ErrorTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetUnconstrainedBudgetCannotBeAssignedErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 59; -- Unconstrained Budget Cannot be Assigned
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetNoScheduledWorkInFiscalYearErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves No Scheduled Work in Fiscal Year error
--   type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetNoScheduledWorkInFiscalYearErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 32; -- No Scheduled Work in Fiscal Year
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetLifecycleBudgetBCRinProgressErrorTypeId]'
GO


-- =============================================================
-- Author:      Chris O'Neal
-- Created:     8/12/2014
-- Release:     5.2
-- Description: Retrieves OutYear Funding change in progress error type
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- 10/13/14  j.frank  5.3      Changed name from [FileImport].[fnGetOutYearFundingBCRinProgressErrorTypeId]
--                             to [FileImport].[fnGetLifecycleBudgetBCRinProgressErrorTypeId] since the 
--                             "out year funding" phrasing has changed to "lifecycle budget".
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetLifecycleBudgetBCRinProgressErrorTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 56; -- Lifecycle Budget BCR in progress
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetFiscalYearOutOfRangeErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves Fiscal Year Out of Range error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetFiscalYearOutOfRangeErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 31; -- Fiscal Year Out of Range
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetPerformerCostSummaryWorkBreakdownStructures]'
GO
-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 7/8/2011	Release 2.3.1
-- Description:	Returns project list for Deputy Cost Reports
-- Modified:
-- mm/dd/yy  Name     Release  Description 
-- 04/24/13  DNC	   4.1	   Remove NAFD references
-- 05/03/13	 DNC	   4.1	   Update to use Account tables for determining relevant projects
-- 05/07/13  DNC	   4.1     Check to see if project had any carryover.
-- 01/27/13  m.cole    5.0	   Using WorkBreakdownStructureId instead of ProjectId to connect Account
-- 09/29/14  a.smith   5.3     Modified to use WorkBreakdownStructure instead of Project; Renamed from
--                             fnGetDeputyProjects. Created local variables for hardcoded values.
-- 10/21/14  a.smith   5.3     fnGetLabLaborTransactionTypeId renamed to fnGetLabLaborCostTransactionTypeId.
-- 11/19/14  CGL       5.3     Modified via Smart Rename after creating table WorkBreakdownStructureGroups to replace the view.
-- 08/27/15  a.smith   6.2     Added local @subSystemId variable and used for parameter to *WorkTypeId function calls
-- 09/25/15  b.roland  6.2     Renamed from fnGetDeputyWorkBreakdownStructures, rewrote to support NA23
-- 10/15/15  b.roland  6.3     Updated to more accurately reflect work type metrics in NA-23, now works for any WBS Id
-- 11/18/15  b.roland  6.3.1   Updated to use WorkTypeGroup instead of WBS Id for Remove
-- 01/19/16  a.smith   6.5     Copied from Report.fnGetNA23DeputyWorkBreakdownStructures; Modified to work with any
--                             WorkTypeGroup or SubSystem.
-- =============================================
CREATE FUNCTION [Report].[fnGetPerformerCostSummaryWorkBreakdownStructures]
(
	@WorkTypeGroupId INT,
	@WBSId int,
	@FiscalYear INT,
	@FiscalPeriod INT 
)
RETURNS 
@WorkBreakdownStructureList TABLE 
(
	WorkBreakdownStructureId int  
)
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @WorkTypeGroupId int = 17, --22, -- 17, 
	--		@WBSId INT = 21163,
	--		@FiscalYear int = 2015,
	--		@FiscalPeriod int = 8;
	--DECLARE @WorkBreakdownStructureList table (WorkBreakdownStructureId int);
	/* End Debug Declarations */

	DECLARE @subSystemId int = 
	(
		SELECT DISTINCT ssId.SubSystemId FROM
		(
			SELECT wbs.SubSystemId FROM G2.WorkBreakdownStructure wbs WHERE wbs.Id = @WBSId
			UNION
			SELECT wtg.SubSystemId FROM G2.WorkTypeGroup wtg WHERE wtg.Id = @WorkTypeGroupId
		) ssId
	);
	DECLARE @fundingConfigurationTypeId int = G2.fnGetFundingConfigurationTypeId(),
			@LabLaborTransactionTypeId int = Financial.fnGetLabLaborCostTransactionTypeId(@subSystemId);
	DECLARE @workBreakdownStructureIds table
    (
		WorkBreakdownStructureId int
	);

	IF @WorkTypeGroupId IS NOT NULL	
	BEGIN 
		INSERT INTO @workBreakdownStructureIds (WorkBreakdownStructureId)
		SELECT DISTINCT wbs.Id AS WorkBreakdownStructureId
		FROM G2.WorkBreakdownStructure wbs
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbs.Id = wbswtx.WorkBreakdownStructureId
		INNER JOIN G2.WorkType wt ON wbswtx.WorkTypeId = wt.Id
		WHERE wt.WorkTypeGroupId = @WorkTypeGroupId
		AND wbs.SubSystemId = @subSystemId;
	END
	ELSE
	BEGIN 
		WITH WorkBreakdownStructureCTE AS
		(
			SELECT wbs2.Id AS WorkBreakdownStructureId
			FROM G2.WorkBreakdownStructure wbs
			INNER JOIN G2.WorkBreakdownStructure wbs2 ON wbs.Id = wbs2.ParentId
			WHERE wbs.Id = @WBSId

			UNION ALL

			SELECT wbs.Id AS WorkBreakdownStructureId
			FROM WorkBreakdownStructureCTE
			INNER JOIN G2.WorkBreakdownStructure wbs ON WorkBreakdownStructureCTE.WorkBreakdownStructureId = wbs.ParentId
		)
		INSERT INTO @workBreakdownStructureIds (WorkBreakdownStructureId)
		SELECT DISTINCT wbscte.WorkBreakdownStructureId
		FROM WorkBreakdownStructureCTE wbscte;
	END;

	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT DISTINCT wbsi.WorkBreakdownStructureId
	FROM @workBreakdownStructureIds wbsi
	INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbsi.WorkBreakdownStructureId = wbsc.WorkBreakdownStructureId
	OUTER APPLY	
	(
		SELECT 'Y' AS Active
		FROM Financial.Account a
		INNER JOIN Financial.AccountTransaction at ON a.Id = at.AccountId
		INNER JOIN Financial.TransactionType tt ON at.TransactionTypeId = tt.Id AND tt.TransactionTypeGroupId <= @LabLaborTransactionTypeId
		INNER JOIN G2.Period per ON at.PeriodId = per.Id AND per.FiscalYear = @FiscalYear AND per.FiscalPeriod <= @FiscalPeriod
		WHERE a.WorkBreakdownStructureId = wbsi.WorkBreakdownStructureId
	) X
	OUTER APPLY	
	(
		SELECT 'Y' AS Active
		FROM Financial.Account a
		INNER JOIN  Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
		WHERE a.WorkBreakdownStructureId = wbsi.WorkBreakdownStructureId
	) Y
	WHERE (X.Active IS NOT NULL OR Y.Active IS NOT NULL)
	AND wbsc.ConfigurationTypeId = @fundingConfigurationTypeId;

	-- Debug statement
	--SELECT * FROM @WorkBreakdownStructureList wbsl;
	-- Debug statement
	RETURN 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetCountryParticipantTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the 'Country' ParticipantType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetCountryParticipantTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Country
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBCRExportImportBatchTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/10/2014
-- Release:     5.3
-- Description: Retrieves the BatchType Id for the BCR Export/Import
--   Import.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetBCRExportImportBatchTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- BCR Export/Import
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdTertiaryResponseForce]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for TertiaryResponseForce.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdTertiaryResponseForce] ()
RETURNS int
AS
BEGIN
	RETURN 14; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSecondaryResponseForce]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for SecondaryResponseForce.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSecondaryResponseForce] ()
RETURNS int
AS
BEGIN
	RETURN 13; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetInProcessStatusId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/04/16
-- Release:     7.2
-- Description: Retrieves the in process status for assessment
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetInProcessStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 1;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnGetBaselineCreationStatus]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/19/2012
-- Release:     3.3
-- Description: Retrieves the current Baseline Creation Status.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Baseline].[fnGetBaselineCreationStatus]
(
)
RETURNS
nvarchar(100)
AS
BEGIN
	DECLARE @status nvarchar(100);
	
	SELECT @status = CONVERT(nvarchar(100), value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey = 'Baseline Creation Status';

	RETURN @status;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetMnOContractorNAFDGroupBnRBNRId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/15/2014 
-- Release:     5.3
-- Description: Returns the M&O Contractor NAFD GroupBNR BNR Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetMnOContractorNAFDGroupBnRBNRId]
(
)
RETURNS INT
AS
BEGIN

	RETURN 15  -- M&O Contractor

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetDPRKNAFDGroupBnRBNRId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/15/2014 
-- Release:     5.3
-- Description: Returns the DPRK NAFD GroupBNR BNR Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetDPRKNAFDGroupBnRBNRId]
(
)
RETURNS INT
AS
BEGIN

	RETURN 16  -- DPRK Value

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [ReportParameter].[fnGetFIMSAssetViewPermission]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/3/2016
-- Release:     
-- Description: Gets user's permission as to whether or not they 
--              may see the WBS Description
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [ReportParameter].[fnGetFIMSAssetViewPermission](@SubSystemId INT, @UserId INT)
RETURNS BIT
AS
BEGIN
    DECLARE 
        @Allowed BIT = 0, 
        @ViewPermissionId INT = Security.fnGetViewPermissionId(),
        @WBS_DASHBOARD_FIMS_ASSETS__PermissionObjectId INT = 163;

    IF EXISTS(
        SELECT permRole.Id
        FROM Security.PermissionRole permRole
        INNER JOIN Security.PersonRole persRole ON persRole.RoleId = permRole.RoleId --persRole.RoleId = r.Id
        INNER JOIN Security.Permission p ON p.Id = permRole.PermissionId
        INNER JOIN Security.PermissionObject po ON po.Id = permRole.PermissionObjectId
        WHERE persRole.PersonId = @UserId
        AND permRole.PermissionId = @ViewPermissionId
        AND po.Id = @WBS_DASHBOARD_FIMS_ASSETS__PermissionObjectId 
        AND permRole.SubSystemId = @SubSystemId
    ) BEGIN 
        SET @Allowed = 1;
    END;

    RETURN @Allowed;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetContractorWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Returns the Contractor workbreakdownstructure attribute type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetContractorWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 12; --Contractor
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetHEUorLEUWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Returns the HEU or LEU  workbreakdownstructure 
--              attribute type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetHEUorLEUWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 4; -- HEU or LEU
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetConvertStatusCommentsWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Returns the ConvertStatusComments workbreakdownstructure 
--              attribute type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetConvertStatusCommentsWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 2; -- Convert Status Comments
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdIDDQuestionsforRoom]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of IDD Questions for Room.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdIDDQuestionsforRoom] ()
RETURNS int
AS
BEGIN
	RETURN 14; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetWorktypeChangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/01/2018 
-- Release:     8.5
-- Description: Returns the ErrorType for Worktype
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetWorktypeChangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 75; --Worktype Change
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetWorkflowStatusChangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/01/2018 
-- Release:     8.5
-- Description: Returns the ErrorType for WorkflowStatusChange
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetWorkflowStatusChangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 73; --Workflow Status Change
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetTimeframeChangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/01/2018 
-- Release:     8.5
-- Description: Returns the ErrorType for TimeframeChange
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetTimeframeChangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 74; --Timeframe Change
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetSitePermissionDeniedErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the site permission denied error type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetSitePermissionDeniedErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 77;--sitePermissionDenied
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetProjectStatusInvisibleErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the project status invisible error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetProjectStatusInvisibleErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 79;--projectStatusInvisibleErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidSiteErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid site error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidSiteErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 86; --invalidSite
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidProjectStatusErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid project status error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidProjectStatusErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 80;--invalidProjectStatusErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidPrimarySystemCodeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid primary system code error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidPrimarySystemCodeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 83;--invalidPrimarySystemCodeErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidPortfolioErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid portfolio error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidPortfolioErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 81;--invalidPortfolioErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidInfrastructurePlanningPropertyTypeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid infrastructure Planning property type error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidInfrastructurePlanningPropertyTypeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 84;--invalidInfrastructurePlanningPropertyTypeErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidCostEstimationClassificationErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/06/2018 
-- Release:     8.5
-- Description: Return the cost estimation classification error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidCostEstimationClassificationErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 87; --InvalidCostEstimationClassificationErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidAssetErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the invalid asset error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidAssetErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 82;--invalidAssetErrorTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetFundingProgramChangeErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/01/2018 
-- Release:     8.5
-- Description: Returns the ErrorType for FundingProgram
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetFundingProgramChangeErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 76; --Funding Program Change
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBudgetAmountInInvalidFiscalYearErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the budget amount in invalid fiscal year error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetBudgetAmountInInvalidFiscalYearErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 85;--budgetAmountInInvalidFiscalYear
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBlankProjectNameErrorTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Return the blank project name error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetBlankProjectNameErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 78; --blankProjectName
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementSelectQuestionSet]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     09/16/2013
-- Release:     4.4
-- Description: Returns sustainability QuestionSet (number) that 
--              should be used for the given assessment and element.
--
-- mm/dd/yyyy   Name        Release  Description 
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementSelectQuestionSet] (
	@AssessmentId INT,
	@ElementId INT
)
RETURNS INT
AS
BEGIN
	DECLARE 
		@ElementIdCountry INT = 3,
		@ElementIdSite INT = 6,
		@QuestionSet INT
	
	IF @ElementId = @ElementIdCountry
		SELECT @QuestionSet = CASE WHEN ca.Revision > 1 THEN 2 ELSE 1 END
		FROM SPA.CountryAssessment ca
		INNER JOIN Sustain.Assessment a ON a.RefId = ca.CountryId
			AND a.EntityTypeId = @ElementIdCountry
		WHERE a.Id = @AssessmentId
	ELSE
		SELECT @QuestionSet = 1

	RETURN @QuestionSet
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetCostPeriodAdvancementDay]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/10/2013
-- Release:     4.1
-- Description: This function retrieves the day on which the
--   cost period advancement should be attempted.
--
-- mm/dd/yy  Name       Release  Description
-- 01/09/14  j.borgers  5.0      Added SubSystemId
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetCostPeriodAdvancementDay](
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @value int;

	SELECT @value = CONVERT(int, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Cost Period Advancement Day'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @value;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetScheduledEventStatusId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the Scheduled EventStatus Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetScheduledEventStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 1; -- Scheduled
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [MDI].[fnGetERIEnablingWorkTypes]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2017 
-- Release:     8.1
-- Description: Get the Work Types that when associated with an Asset will enable the ERI questions
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [MDI].[fnGetERIEnablingWorkTypes]()
RETURNS @WorkTypes TABLE (
	Id INT
)
AS
BEGIN
	INSERT INTO @WorkTypes (Id)
	VALUES  (18); --Disposition

	RETURN; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetProgramOrganizationTypeId]'
GO
-- =============================================================
-- Author:   Greg Ogle
-- Created:  2/21/2014
-- Release:	 5.0
-- Description: Returning a ID for Program Organization Type
--
-- mm/dd/yy  Name     Revision  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetProgramOrganizationTypeId] ( )
RETURNS INT
AS
    BEGIN
        DECLARE @ProgramOrganizationTypeId INT;
        SELECT  @ProgramOrganizationTypeId = Id
        FROM    Contact.OrganizationType
        WHERE   Name = 'Program';

        RETURN @ProgramOrganizationTypeId;
    END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Baseline].[fnGetBaselinePeriodAdvancementDay]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     5/13/2011
-- Release:     2.3
-- Description: This function retrieves the day on which the
--   baseline period advancement should occur.
--
-- mm/dd/yy  Name      Release  Description
-- 12/12/12  s.oneal   3.7      Moved to the Baseline schema
-- 05/30/14  j.borgers 5.2      Modified to get data based on SubSystemId
-- =============================================================
CREATE FUNCTION [Baseline].[fnGetBaselinePeriodAdvancementDay]
(
@SubSystemId int
)
RETURNS int
AS
BEGIN
	DECLARE @value int;

	SELECT @value = CONVERT(int, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Baseline Period Advancement Day'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @value;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetFireProtectionFunctionalAreaAssessmentHierarchyId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     09/28/16
-- Release:     7.3
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetFireProtectionFunctionalAreaAssessmentHierarchyId]()
RETURNS INT
AS
BEGIN
	RETURN 2;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIStructuralRiskJustificationQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Structural Risk Justification Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIStructuralRiskJustificationQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 104; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERINEPAEvaluationCompleteQuestionId]'
GO

-- =============================================================
-- Author:      Diedre Nickum
-- Created:     12/19/2017 
-- Release:     8.4
-- Description: Returns the question id for the ERI NEPA 
--              Evaluation Complete Question.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inquiry].[fnGetERINEPAEvaluationCompleteQuestionId] ()
RETURNS INT
AS
BEGIN
    RETURN 115; /* Is the NEPA evaluation complete? from Inquiry.Question */
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERINEPAEvaluationAnticipatedActionQuestionId]'
GO

-- =============================================================
-- Author:      Diedre Nickum
-- Created:     12/19/2017 
-- Release:     8.4
-- Description: Returns the follow-on question id for the ERI NEPA 
--              Evaluation Complete Question, i.e., If NEPA not complete, 
--              which NEPA action is anticipated to be required?
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inquiry].[fnGetERINEPAEvaluationAnticipatedActionQuestionId] ()
RETURNS INT
AS
BEGIN
    RETURN 116; /* If NEPA not complete, which NEPA action is anticipated to be required? from Inquiry.Question */
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIHistoricEvaluationWasItHistoricQuestionId]'
GO

-- =============================================================
-- Author:      Diedre Nickum
-- Created:     12/19/2017 
-- Release:     8.4
-- Description: Returns the follow-on question id for the ERI Historic 
--              Evaluation Complete Question, i.e., was it determined 
--              to be historic or not?
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inquiry].[fnGetERIHistoricEvaluationWasItHistoricQuestionId] ()
RETURNS INT
AS
BEGIN
    RETURN 114; /* Was it determined to be historic? from Inquiry.Question */
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIHistoricEvaluationCompleteQuestionId]'
GO

-- =============================================================
-- Author:      Diedre Nickum
-- Created:     12/19/2017 
-- Release:     8.4
-- Description: Returns the question id for the ERI Historic 
--              Evaluation Complete Question.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inquiry].[fnGetERIHistoricEvaluationCompleteQuestionId] ()
RETURNS INT
AS
BEGIN
    RETURN 113; /* Is the historic evaluation complete? from Inquiry.Question */
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inquiry].[fnGetERIContaminationRiskJustificationQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/12/2017 
-- Release:     8.1
-- Description: Returns the ERI Contamination Risk Justification Question Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Inquiry].[fnGetERIContaminationRiskJustificationQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 105; --Inquiry.Question
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetAlternateTechnologyMaterialTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     7/13/18
-- Release:     9.1
-- Description: Returns the Alternate Technology material type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetAlternateTechnologyMaterialTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 3; --Alternate Technology
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetAnnex1LabRollupPartyRelationshipTypeId]'
GO
---- =============================================================
---- Author:      Christopher Luttrell (CORP\chris.luttrell)
---- Created:     11/17/2016
---- Release:     7.4
---- Description: Retrieves the Annex1 Lab party relationship type id.
----
---- mm/dd/yy  Name     Release  Description
---- =============================================================
CREATE FUNCTION [Contact].[fnGetAnnex1LabRollupPartyRelationshipTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Annex1 Lab Rollup
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Activity].[fnGetActivityFundingActivityTransactionTypeId]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     08/23/2018
-- Release:     9.2
-- Description: Get new funding value from activity.transactiontype (Additional Funding)
--              
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Activity].[fnGetActivityFundingActivityTransactionTypeId]()
RETURNS INT 
AS 
BEGIN 

RETURN 1;

END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetCriteriaAssessmentHierarchyLevelId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     07/30/2016
-- Release:     7.2
-- Description: Retrieves the category assessment hierarchy level id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetCriteriaAssessmentHierarchyLevelId] ()
RETURNS INT
AS
BEGIN
	RETURN 4;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetCategoryAssessmentHierarchyLevelId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     07/30/2016
-- Release:     7.2
-- Description: Retrieves the category assessment hierarchy level id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetCategoryAssessmentHierarchyLevelId] ()
RETURNS INT
AS
BEGIN
	RETURN 3;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [CostSubmission].[fnGetNegativeCostNoteTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/22/2013
-- Release:     4.1
-- Description: Retrieves the Negative Cost note type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [CostSubmission].[fnGetNegativeCostNoteTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Negative Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetTaskStatusTooLongErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/20/2014 
-- Release:     5.3
-- Description: Returns the error type id for task status too long
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetTaskStatusTooLongErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 42; --Task Status Too Long
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetProjectMilestoneDuplicateErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/20/2014 
-- Release:     5.3
-- Description: Returns the error type id for a project milestone duplicate
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetProjectMilestoneDuplicateErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 46; --ProjectMilestone Duplicate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidDateErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/20/2014 
-- Release:     5.3
-- Description: Returns the error type id for a date thats in an invalid format
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidDateErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 5; --Invalid Date
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidCompletionStatusErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/20/2014 
-- Release:     5.3
-- Description: Returns the error type id for an invalid completion status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidCompletionStatusErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 7; --Invalid Completion Status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBlankTaskStatusErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/20/2014 
-- Release:     5.3
-- Description: Returns the error type id for a blank task status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetBlankTaskStatusErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 38; --Blank Task Status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Notification].[fnGetFundingRequestVoidNotificationId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:		11/11/16
-- Release:     7.4
-- Description: Returns the id for the Funding Request void notification
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Notification].[fnGetFundingRequestVoidNotificationId]()
RETURNS INT 
AS
BEGIN
	RETURN 24; 
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetPerformanceReportingRawImportBatchTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the Batch Type Id of type Performance Reporting Raw Import
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetPerformanceReportingRawImportBatchTypeId]()
RETURNS int
AS
BEGIN
  RETURN 6; --Performance Reporting Raw Import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA23DeputyWorkBreakdownStructures]'
GO
-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 7/8/2011	Release 2.3.1
-- Description:	Returns project list for Deputy Cost Reports
-- Modified:
-- mm/dd/yy  Name     Release  Description 
-- 04/24/13  DNC	   4.1	   Remove NAFD references
-- 05/03/13	 DNC	   4.1	   Update to use Account tables for determining relevant projects
-- 05/07/13  DNC	   4.1     Check to see if project had any carryover.
-- 01/27/13  m.cole    5.0	   Using WorkBreakdownStructureId instead of ProjectId to connect Account
-- 09/29/14  a.smith   5.3     Modified to use WorkBreakdownStructure instead of Project; Renamed from
--                             fnGetDeputyProjects. Created local variables for hardcoded values.
-- 10/21/14  a.smith   5.3     fnGetLabLaborTransactionTypeId renamed to fnGetLabLaborCostTransactionTypeId.
-- 11/19/14  CGL       5.3     Modified via Smart Rename after creating table WorkBreakdownStructureGroups to replace the view.
-- 08/27/15  a.smith   6.2     Added local @subSystemId variable and used for parameter to *WorkTypeId function calls
-- 09/25/15  b.roland  6.2     Renamed from fnGetDeputyWorkBreakdownStructures, rewrote to support NA23
-- 10/15/15  b.roland  6.3     Updated to more accurately reflect work type metrics in NA-23, now works for any WBS Id
-- 11/18/15  b.roland  6.3.1   Updated to use WorkTypeGroup instead of WBS Id for Remove
-- =============================================
CREATE FUNCTION [Report].[fnGetNA23DeputyWorkBreakdownStructures]
(
	@WorkTypeGroupId INT,
	@WBSId int,
	@FiscalYear INT,
	@FiscalPeriod INT 
)
RETURNS 
@WorkBreakdownStructureList TABLE 
(
	WorkBreakdownStructureId int  
)
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @WorkTypeGroupId int = 22,
	--		@FiscalYear int = 2015,
	--		@FiscalPeriod int = 8;
	--DECLARE @WorkBreakdownStructureList table (WorkBreakdownStructureId int);
	/* End Debug Declarations */

	DECLARE @subSystemId int = G2.fnGetNA23SubSystemId();
	DECLARE @fundingConfigurationTypeId int = G2.fnGetFundingConfigurationTypeId(),
			@LabLaborTransactionTypeId int = Financial.fnGetLabLaborCostTransactionTypeId(@subSystemId),
			@removeWorkTypeGroupId int = G2.fnGetRemoveWorkTypeGroupId(@subSystemId);
	DECLARE @workBreakdownStructureIds table
    (
		WorkBreakdownStructureId int
	);

	IF @WorkTypeGroupId = @removeWorkTypeGroupId
		INSERT INTO @workBreakdownStructureIds (WorkBreakdownStructureId)
		SELECT DISTINCT wbs.Id AS WorkBreakdownStructureId
		FROM G2.WorkBreakdownStructure wbs
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbs.Id = wbswtx.WorkBreakdownStructureId
		INNER JOIN G2.WorkType wt ON wbswtx.WorkTypeId = wt.Id
		WHERE wt.WorkTypeGroupId = @WorkTypeGroupId
	ELSE
		WITH WorkBreakdownStructureCTE AS
		(
			SELECT wbs2.Id AS WorkBreakdownStructureId
			FROM G2.WorkBreakdownStructure wbs
			INNER JOIN G2.WorkBreakdownStructure wbs2 ON wbs.Id = wbs2.ParentId
			WHERE wbs.Id = @WBSId

			UNION ALL

			SELECT wbs.Id AS WorkBreakdownStructureId
			FROM WorkBreakdownStructureCTE
			INNER JOIN G2.WorkBreakdownStructure wbs ON WorkBreakdownStructureCTE.WorkBreakdownStructureId = wbs.ParentId
		)
		INSERT INTO @workBreakdownStructureIds (WorkBreakdownStructureId)
		SELECT DISTINCT wbscte.WorkBreakdownStructureId
		FROM WorkBreakdownStructureCTE wbscte

	INSERT INTO @WorkBreakdownStructureList (WorkBreakdownStructureId)
	SELECT DISTINCT wbsi.WorkBreakdownStructureId
	FROM @workBreakdownStructureIds wbsi
	INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbsi.WorkBreakdownStructureId = wbsc.WorkBreakdownStructureId
	OUTER APPLY	
	(
		SELECT 'Y' AS Active
		FROM Financial.Account a
		INNER JOIN Financial.AccountTransaction at ON a.Id = at.AccountId
		INNER JOIN Financial.TransactionType tt ON at.TransactionTypeId = tt.Id AND tt.TransactionTypeGroupId <= @LabLaborTransactionTypeId
		INNER JOIN G2.Period per ON at.PeriodId = per.Id AND per.FiscalYear = @FiscalYear AND per.FiscalPeriod <= @FiscalPeriod
		WHERE a.WorkBreakdownStructureId = wbsi.WorkBreakdownStructureId
	) X
	OUTER APPLY	
	(
		SELECT 'Y' AS Active
		FROM Financial.Account a
		INNER JOIN  Financial.ProjectTranFYTotal pt ON a.Id = pt.AccountId AND pt.FiscalYear = @FiscalYear AND pt.PriorFyCarryOver <> 0.00
		WHERE a.WorkBreakdownStructureId = wbsi.WorkBreakdownStructureId
	) Y
	WHERE (X.Active IS NOT NULL OR Y.Active IS NOT NULL)
	AND wbsc.ConfigurationTypeId = @fundingConfigurationTypeId;

	-- Debug statement
	--SELECT * FROM @WorkBreakdownStructureList wbsl;
	-- Debug statement
	RETURN 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Assessment].[fnGetSMPAssessmentHierarchyLevelId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     07/30/2016
-- Release:     7.2
-- Description: Retrieves the SMP assessment hierarchy level id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Assessment].[fnGetSMPAssessmentHierarchyLevelId] ()
RETURNS INT
AS
BEGIN
	RETURN 1;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetActiveDescisionMadeToRetireClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Active/Decision Made to Retire
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetActiveDescisionMadeToRetireClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 11; --Active/Decision Made to Retire
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetActiveFullOperationsClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Active/Full Operations
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetActiveFullOperationsClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 10; --Active/Full Operations
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetArchivedClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Archived
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetArchivedClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 15; --Archived
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetExcessToDOEClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Excess to DOE
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetExcessToDOEClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 13; --Excess to DOE
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetExcessToSiteClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Excess to Site
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetExcessToSiteClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 12; --Excess to Site
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [AssetManagement].[fnGetHistoricUnableToDemolishClassificationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/26/2018 
-- Release:     9.1
-- Description: Returns the ClassificationType Id for Historic/Unable to Demolish
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [AssetManagement].[fnGetHistoricUnableToDemolishClassificationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 14; --Historic/Unable to Demolish
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Audit].[fnGetBrowserName]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     07/17/2015
-- Release:     6.1
-- Description: Retrieves the actual browser version from the
--   User Agent string, IE reports the comaptibility version
--   instead of the actual browser version.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Audit].[fnGetBrowserName]
(
	@BrowserName NVARCHAR(100)
)
RETURNS
NVARCHAR(100)
AS
BEGIN
	RETURN CASE WHEN @BrowserName IN ('InternetExplorer', 'Internet Explorer') THEN 'IE' ELSE @BrowserName END;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetAlarmResponseTrainingChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/16/2014
-- Release:     5.0
-- Description: Retrieves the ART change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetAlarmResponseTrainingChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 26; -- ART
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetEquipmentTrainerEventRoleId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     7/22/2014
-- Release:     5.0
-- Description: Retrieves the Id for Equipment Trainer from EventRole.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetEquipmentTrainerEventRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 2; -- Equipment Trainer
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetEventSupportEventRoleId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     7/22/2014
-- Release:     5.0
-- Description: Retrieves the Id for Event Support from EventRole.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetEventSupportEventRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 4; -- Event Support
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetLeadTrainerEventRoleId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     7/22/2014
-- Release:     5.0
-- Description: Retrieves the Id for Lead Trainer from EventRole.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetLeadTrainerEventRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 1; -- Lead Trainer
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetObserverAttendeeCategoryId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the Observer AttendeeCategory Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetObserverAttendeeCategoryId] ()
RETURNS INT
AS
BEGIN
	RETURN 2; -- Observer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetPackagingAndTransportationTrainerEventRoleId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     7/22/2014
-- Release:     5.0
-- Description: Retrieves the Id for the Packaging and Transportation Trainer from EventRole.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetPackagingAndTransportationTrainerEventRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 3; -- Packaging and Transportation Trainer
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetPersonalRadiationDetectorTrainingChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/16/2014
-- Release:     5.0
-- Description: Retrieves the PRDT change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetPersonalRadiationDetectorTrainingChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 27; -- PRDT
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetSearchAndSecureChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/16/2014
-- Release:     5.0
-- Description: Retrieves the SS change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetSearchAndSecureChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 28; -- SS
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetTableTopExerciseChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/16/2014
-- Release:     5.0
-- Description: Retrieves the TTX change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetTableTopExerciseChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 29; -- TTX
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetTableTopExerciseCoordinationVisitChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/16/2014
-- Release:     5.0
-- Description: Retrieves the TTXCV change request type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetTableTopExerciseCoordinationVisitChangeRequestTypeId]()
RETURNS int
AS
BEGIN
	RETURN 30; -- TTXCV
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Calendar].[fnGetTableTopExerciseCoordinationVisitEventTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/24/2014
-- Release:     5.0
-- Description: Retrieves the TTXCV EventType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Calendar].[fnGetTableTopExerciseCoordinationVisitEventTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 9; -- TTXCV
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetCountryGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Country geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetCountryGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Country
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetCountyGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the County geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetCountyGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- County
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetDistrictGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the District geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetDistrictGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 10; -- District
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetLeadLabRollupPartyRelationshipTypeId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     7/1/14
-- Release:     5.2
-- Description: Retrieves the Lead Lab Rollup party relationship type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetLeadLabRollupPartyRelationshipTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 5; -- Lead Lab Rollup
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetMunicipalityGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Municipality geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetMunicipalityGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 12; -- Municipality
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetOblastGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Oblast geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetOblastGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 9; -- Oblast
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetParishGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Parish geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetParishGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 11; -- Parish
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetProvinceGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Province geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetProvinceGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 5; -- Province
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetRegionGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Region geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetRegionGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Region
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Contact].[fnGetTerritoryGeographicBoundaryTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/6/2013
-- Release:     4.3
-- Description: Retrieves the id of the Territory geographic
--   boundary type.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Contact].[fnGetTerritoryGeographicBoundaryTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- Territory
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetBNRNotAvailableForProjectErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves B&R Not Available for Project error
--   type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetBNRNotAvailableForProjectErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 35; -- B&R Not Available for Project
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetInvalidTaskErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for an invalid task
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetInvalidTaskErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Invalid Task
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetLabDistributionForPlanningYearErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves Lab Distribution for Planning Year
--   error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetLabDistributionForPlanningYearErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 33; -- Lab Distribution for Planning Year
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetLabDistributionMustMatchRequestErrorTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.4
-- Description: Retrieves Lab Distribution Must Match Request
--   error type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetLabDistributionMustMatchRequestErrorTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 34; -- Lab Distribution Must Match Request
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetMovingJouleTargetFYErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for a Moving Joule Target FY
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetMovingJouleTargetFYErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 11; --Moving Joule Target FY
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetProspectiveFundingRequestImportBatchTypeId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     04/13/2017 
-- Release:     8.0
-- Description: Gets the ProspectiveFundingRequestImport BatchTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [FileImport].[fnGetProspectiveFundingRequestImportBatchTypeId] ()
RETURNS INT
AS
BEGIN
  RETURN 13; -- ProspectiveFUndingRequestImport BatchTypeId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [FileImport].[fnGetTaskProjectMilestoneMismatchErrorTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the error type id for an task project milestone mismatch
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [FileImport].[fnGetTaskProjectMilestoneMismatchErrorTypeId]()
RETURNS int
AS
BEGIN
  RETURN 4; --task project milestone mismatch
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetAvailableFundingValidationMethod]'
GO


-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/21/2017
-- Release:     7.5
-- Description: Returns the available funding validation method 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetAvailableFundingValidationMethod]
(
)
RETURNS INT
AS
BEGIN
	RETURN 1;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetIndirectAndOtherDirectCostTransactionTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/24/2015 
-- Release:     6.4
-- Description: Returns the Indirect and Other Direct Cost TransactionTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Financial].[fnGetIndirectAndOtherDirectCostTransactionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 17; --Indirect and Other Direct Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Financial].[fnGetShipmentNumberWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Returns the Shipment Number workbreakdownstructure 
--              attribute type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Financial].[fnGetShipmentNumberWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 10; --Shipment Number
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdLawEnforcementAgency]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for LawEnforcementAgency.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdLawEnforcementAgency] ()
RETURNS int
AS
BEGIN
	RETURN 16; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdOffice]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Office.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdOffice] ()
RETURNS int
AS
BEGIN
	RETURN 2; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdProgram]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Program.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdProgram] ()
RETURNS int
AS
BEGIN
	RETURN 1; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdRegion]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for Region.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdRegion] ()
RETURNS int
AS
BEGIN
	RETURN 4; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdRemoteMonitoringSystem]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for RemoteMonitoringSystem.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdRemoteMonitoringSystem] ()
RETURNS int
AS
BEGIN
	RETURN 17; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSPAImport]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for SPAImport.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSPAImport] ()
RETURNS int
AS
BEGIN
	RETURN 15; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdSPAMain]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for SPAMain.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdSPAMain] ()
RETURNS int
AS
BEGIN
	RETURN 11; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetEntityTypeIdState]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the Entity Type for State.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetEntityTypeIdState] ()
RETURNS int
AS
BEGIN
	RETURN 5; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Inventory].[fnGetNuclearMaterialTypeId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/02/14
-- Release:     5.3
-- Description: Returns the Nuclear material type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Inventory].[fnGetNuclearMaterialTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 2; --Nuclear
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Performance].[fnGetPerformanceGroupId]'
GO
-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     9/21/2012
-- Release:     3.5
-- Description: Retrieves the Performance ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Performance].[fnGetPerformanceGroupId]()
RETURNS int
AS
BEGIN
	
	RETURN 2;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Proposal].[fnGetClosedSolicitationStatusId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     01/09/2018 
-- Release:     8.4
-- Description: Returns the Closed solicitation status Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Proposal].[fnGetClosedSolicitationStatusId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Closed
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21ADAPortfolioWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/10/2014 
-- Release:     5.3
-- Description: Returns the WorkBreakdownStructure Id for ADA Portfolio
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA21ADAPortfolioWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 5; --ADA Portfolio

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21EuropeAndAfricaWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/10/2014 
-- Release:     5.3
-- Description: Returns the WorkBreakdownStructure Id for Europe and Africa
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA21EuropeAndAfricaWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 3; --Europse and Africa

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21FSUAndAsiaWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/10/2014 
-- Release:     5.3
-- Description: Returns the WorkBreakdownStructure Id for FSU and Asia
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA21FSUAndAsiaWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 4; --FSU and Asia

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21NorthAndSouthAmericaWorkBreakdownStructureId]'
GO



-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/10/2014 
-- Release:     5.3
-- Description: Returns the WorkBreakdownStructure Id for North and South America
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA21NorthAndSouthAmericaWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 2; --North and South America

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA21ReadytoImplementWorkflowLevel]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     11/18/2014
-- Release:     5.3
-- Description: Returns the value for the Ready to Implement Change 
--              Request Type Group Workflow Level Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Report].[fnGetNA21ReadytoImplementWorkflowLevel]()
RETURNS int
AS
BEGIN

	RETURN 5;

END

	
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA23ConvertWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/25/15
-- Release:     6.3
-- Description: Retrieves NA-23 Convert WBS Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA23ConvertWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 23262; --Convert

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Report].[fnGetNA23RemoveWorkBreakdownStructureId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/25/15
-- Release:     6.3
-- Description: Retrieves NA-23 Remove WBS Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Report].[fnGetNA23RemoveWorkBreakdownStructureId] ()
RETURNS INT
AS
BEGIN

  RETURN 23263; --Remove

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdDocumentLibrary]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Document Library.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdDocumentLibrary] ()
RETURNS int
AS
BEGIN
	RETURN 20; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdIDDQuestionsforBuilding]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of IDD Questions for Building.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdIDDQuestionsforBuilding] ()
RETURNS int
AS
BEGIN
	RETURN 13; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdIDDQuestionsforSite]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of IDD Questions for Site.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdIDDQuestionsforSite] ()
RETURNS int
AS
BEGIN
	RETURN 12; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSecondaryResponseSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Secondary Response Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSecondaryResponseSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 17; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdSustainabilityAssessments]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Sustainability Assessments.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdSustainabilityAssessments] ()
RETURNS int
AS
BEGIN
	RETURN 22; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPASectionTypeIdTertiaryResponseSecurityUpgrades]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/10/2015
-- Release:     6.2.1
-- Description: Retrieves the id of Tertiary Response Security Upgrades.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPASectionTypeIdTertiaryResponseSecurityUpgrades] ()
RETURNS int
AS
BEGIN
	RETURN 18; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbyCountryTeamLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by Country Team Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbyCountryTeamLead] ()
RETURNS int
AS
BEGIN
	RETURN 12; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbyPortfolioOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by Portfolio Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbyPortfolioOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 10; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbyRegionOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by Region Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbyRegionOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 6; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdPassedBackbySPASubProgramManager]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Passed Back by SPA Sub-Program Manager.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdPassedBackbySPASubProgramManager] ()
RETURNS int
AS
BEGIN
	RETURN 21; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbyPortfolioOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by Portfolio Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbyPortfolioOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 17; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdRejectedbyRegionOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Rejected by Region Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdRejectedbyRegionOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 16; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdReturnedToOriginator]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     06/30/2017
-- Release:     8.1
-- Description: Retrieves the id of the SPA Status for Returned to Originator
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdReturnedToOriginator] ()
RETURNS int
AS
BEGIN
    RETURN 26; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoCountryTeamLead]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to Country Team Lead.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoCountryTeamLead] ()
RETURNS int
AS
BEGIN
	RETURN 11; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoPortfolioOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to Portfolio Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoPortfolioOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 9; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [SPA].[fnGetSPAStatusIdSubmittedtoRegionOfficer]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Status for Submitted to Region Officer.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [SPA].[fnGetSPAStatusIdSubmittedtoRegionOfficer] ()
RETURNS int
AS
BEGIN
	RETURN 3; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetIndicatorLevelIdMaturity]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/10/2017
-- Release:     8.3
-- Description: Retrieves the id of the IndicatorLevel of Maturity
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetIndicatorLevelIdMaturity] ()
RETURNS INT
AS
BEGIN
	RETURN 3; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Sustain].[fnGetSubElementTypeIdGroup]'
GO


-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/06/2017
-- Release:     8.3
-- Description: Retrieves the id of the SubElementType of Group
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Sustain].[fnGetSubElementTypeIdGroup] ()
RETURNS INT
AS
BEGIN
	RETURN 5; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[GetDependents]'
GO
CREATE FUNCTION [Tools].[GetDependents](
    @ObjectName AS SYSNAME
)
RETURNS @result TABLE (
	Seq INT IDENTITY, 
	ObjectName SYSNAME, 
	Hierarchy VARCHAR(128),
	Recursive BIT)
AS
BEGIN
    ;WITH Obj AS (
        SELECT DISTINCT
            SED.referencing_id  AS ParentID,
            SED.referenced_id AS ObjectID,
            o1.Name AS ParentName,
            o2.Name AS ChildName,
            CONVERT(NVARCHAR(1024),QUOTENAME(sch1.name) + '.' + QUOTENAME(o1.Name) 
			+ '(' + RTRIM(o1.type) + ')' )
                COLLATE SQL_Latin1_General_CP1_CI_AS 
			AS ParentObject, 
            QUOTENAME(sch2.name) + '.' + QUOTENAME(o2.Name) 
			+ '(' + RTRIM(o2.type) + ')' 
                COLLATE SQL_Latin1_General_CP1_CI_AS AS ObjectName
        FROM sys.sql_expression_dependencies SED
        INNER JOIN sys.all_objects o1 ON SED.referencing_id = o1.object_id
        INNER JOIN sys.schemas sch1 ON sch1.schema_id = o1.schema_id
        INNER JOIN sys.all_objects o2 on SED.referenced_id = o2.object_id
        INNER JOIN sys.schemas sch2 ON sch2.schema_id = o2.schema_id
    ), cte AS (
        SELECT 
            0 AS lvl, 
            CONVERT(INT,NULL) ParentID,
            o1.object_id ObjectId,
            CONVERT(NVARCHAR(1024),NULL) ParentObject,
            QUOTENAME(sch1.name) + '.' + QUOTENAME(o1.Name) 
			+ '(' + RTRIM(o1.type) + ')' 
                COLLATE SQL_Latin1_General_CP1_CI_AS 
			AS ObjectName,
            CONVERT(BIT,0) Recursive,
            CAST(o1.object_id AS VARBINARY(512)) AS Sort
        FROM sys.all_objects o1
        INNER JOIN sys.schemas sch1 ON sch1.schema_id = o1.schema_id
        WHERE o1.name = @ObjectName
        UNION ALL 
        SELECT
            p.lvl+ 1, 
            c.ParentID,
            c.ObjectId,
            c.ParentObject,
            c.ObjectName,
            CASE WHEN c.ParentID = c.ObjectID THEN CONVERT(BIT,1) ELSE p.Recursive END Recursive,
            CAST(p.sort + CAST(c.ObjectID AS VARBINARY(16)) 
		AS VARBINARY(512))
        FROM cte p 
        INNER JOIN obj c ON p.ObjectID = c.ParentID AND p.Recursive = 0
    )
    INSERT INTO @result (ObjectName, Hierarchy, Recursive)
    SELECT 
        ObjectName,
        '|-' + REPLICATE('-',(lvl * 4)) + ObjectName ,
        Recursive
    FROM cte
   ORDER BY Sort
         OPTION (MAXRECURSION 300)
    
    RETURN 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[GetReferences]'
GO
CREATE FUNCTION [Tools].[GetReferences](
    @ObjectName AS SYSNAME
)
RETURNS @result TABLE (
	Seq INT IDENTITY, 
	ObjectName SYSNAME, 
	Hierarchy VARCHAR(128),
	Recursive BIT)
AS
BEGIN
    ;WITH Obj AS (
        SELECT DISTINCT
            SED.referencing_id  AS ParentID,
            SED.referenced_id AS ObjectID,
            o1.Name AS ParentName,
            o2.Name AS ChildName,
            CONVERT(NVARCHAR(1024),QUOTENAME(sch1.name) + '.' + QUOTENAME(o1.Name) 
			+ '(' + RTRIM(o1.type) + ')' )
                COLLATE SQL_Latin1_General_CP1_CI_AS 
			AS ParentObject, 
            QUOTENAME(sch2.name) + '.' + QUOTENAME(o2.Name) 
			+ '(' + RTRIM(o2.type) + ')' 
                COLLATE SQL_Latin1_General_CP1_CI_AS AS ObjectName
        FROM sys.sql_expression_dependencies SED
        INNER JOIN sys.all_objects o1 ON SED.referencing_id = o1.object_id
        INNER JOIN sys.schemas sch1 ON sch1.schema_id = o1.schema_id
        INNER JOIN sys.all_objects o2 on SED.referenced_id = o2.object_id
        INNER JOIN sys.schemas sch2 ON sch2.schema_id = o2.schema_id
    ), cte AS (
        SELECT 
            0 AS lvl, 
            CONVERT(INT,NULL) ParentID,
            o1.object_id ObjectId,
            CONVERT(NVARCHAR(1024),NULL) ParentObject,
            QUOTENAME(sch1.name) + '.' + QUOTENAME(o1.Name) 
			+ '(' + RTRIM(o1.type) + ')' 
                COLLATE SQL_Latin1_General_CP1_CI_AS 
			AS ObjectName,
            CONVERT(BIT,0) Recursive,
            CAST(o1.object_id AS VARBINARY(512)) AS Sort
        FROM sys.all_objects o1
        INNER JOIN sys.schemas sch1 ON sch1.schema_id = o1.schema_id
        WHERE o1.name = @ObjectName
        UNION ALL 
        SELECT
            p.lvl+ 1, 
            c.ParentID,
            c.ObjectId,
            c.ParentObject,
            c.ObjectName,
            CASE WHEN c.ParentID = c.ObjectID THEN CONVERT(BIT,1) ELSE p.Recursive END Recursive,
            CAST(p.sort + CAST(c.ObjectID AS VARBINARY(16)) 
		AS VARBINARY(512))
        FROM cte p 
        INNER JOIN obj c ON p.ObjectID = c.ParentID AND p.Recursive = 0
    )
    INSERT INTO @result (ObjectName, Hierarchy, Recursive)
    SELECT 
        ObjectName,
        '|-' + REPLICATE('-',(lvl * 4)) + ObjectName ,
        Recursive
    FROM cte
   ORDER BY Sort
         OPTION (MAXRECURSION 300)
    
    RETURN 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[ParmsToList]'
GO

CREATE FUNCTION [Tools].[ParmsToList] (@Parameters varchar(500))
returns @result TABLE (Value varchar(30))
AS  
begin
     DECLARE @TempList table
          (
          Value varchar(30)
          ) 
     DECLARE @Value varchar(30), @Pos int 
     SET @Parameters = LTRIM(RTRIM(@Parameters))+ ','
     SET @Pos = CHARINDEX(',', @Parameters, 1) 
     IF REPLACE(@Parameters, ',', '') <> ''
     BEGIN
          WHILE @Pos > 0
          BEGIN
               SET @Value = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               IF @Value <> ''
               BEGIN
                    INSERT INTO @TempList (Value) VALUES (@Value) --Use Appropriate conversion
               END
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1) 
          END
     END    
     INSERT @result
     SELECT value
        FROM @TempList
     RETURN
END     

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[fnMultiValue_Split]'
GO

CREATE FUNCTION [Tools].[fnMultiValue_Split]
   (@InputStr nvarchar(4000), @Delim char(1)= ',', @MVDelim char(1)= '|')
RETURNS @Values TABLE (Id INT IDENTITY(1,1), MainValue nvarchar(1000), SubValue nvarchar(1000))AS
  BEGIN
  DECLARE @chrind INT, @chrind2 INT
  DECLARE @Piece nvarchar(2000), @MainValue nvarchar(1000), @SubValue nvarchar(1000)
  SELECT @chrind = 1,  @chrind2 = 1
  WHILE @chrind > 0
    BEGIN
      SELECT @chrind = CHARINDEX(@Delim,@InputStr)
      IF @chrind  > 0
        SELECT @Piece = LEFT(@InputStr,@chrind - 1)
      ELSE
        SELECT @Piece = @InputStr
      -- now split on sub value if there
      SELECT @chrind2 = CHARINDEX(@MVDelim,@Piece)
      IF @chrind2  > 0
        SELECT @MainValue = LEFT(@Piece,@chrind2 - 1), @SubValue = SUBSTRING(@Piece,@chrind2+1, LEN(@Piece))
      ELSE
        SELECT @MainValue = @Piece, @SubValue = NULL
      INSERT  @Values(MainValue, SubValue) VALUES(@MainValue, @SubValue)
      SELECT @InputStr = RIGHT(@InputStr,LEN(@InputStr) - @chrind)
      IF LEN(@InputStr) = 0 BREAK
    END
  RETURN
  END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Tools].[fnRemoveSpecialCharactersFromString]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     08/03/2016 
-- Release:     7.2
-- Description: Removes special characters from an input string
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Tools].[fnRemoveSpecialCharactersFromString]
(
	@InputString NVARCHAR(2000)
)
RETURNS NVARCHAR(2000)
AS
BEGIN
	
	DECLARE @position INT = 1,
			@temp NVARCHAR(500) = @InputString,
			@charNum INT;

	WHILE @position <= LEN(@temp)  
	BEGIN
		SET @charNum = ASCII(SUBSTRING(@temp, @position, 1));
		
		-- Replace 'non-breaking space' with 'space'
		SET @temp = REPLACE(@temp, CHAR(160), ' ');

		-- Remove other special characters
		IF (@charNum < 32 OR @charNum > 126)
		BEGIN
			SET @temp = STUFF(@temp, @position, 1, '');
			SET @position = @position - 1;
		END
			
		SET @position = @position + 1;
	END;

	--SELECT '|' + @InputString + '|' AS RawString, '|' + @temp + '|' AS CleanString, '|' + RTRIM(LTRIM(@temp)) + '|' AS TrimCleanString;

	RETURN LTRIM(RTRIM(@temp)); -- Clean string
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetYearEndForecastHQFORoleId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     06/07/2017
-- Release:     8.1
-- Description: Returns YEF HQFO role id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetYearEndForecastHQFORoleId]()
RETURNS INT
AS
BEGIN
  RETURN 167; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetYearEndForecastSiteFinancialOfficerRoleId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     04/28/2017
-- Release:     8.0
-- Description: Returns YEF SFO role id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetYearEndForecastSiteFinancialOfficerRoleId]()
RETURNS INT
AS
BEGIN
  RETURN 168; 
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [YearEndForecast].[fnGetYearEndForecastSubProgramManagerRoleId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/10/2017
-- Release:     7.5
-- Description: Returns YEF SPM role id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [YearEndForecast].[fnGetYearEndForecastSubProgramManagerRoleId]  ()
RETURNS INT
AS
BEGIN
  RETURN 169; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Baseline].[fnGetBaselineGroupId]'
GO
GRANT EXECUTE ON  [Baseline].[fnGetBaselineGroupId] TO [G2NotificatonServices]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Baseline].[fnGetBaselineTypeId]'
GO
GRANT EXECUTE ON  [Baseline].[fnGetBaselineTypeId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Baseline].[fnGetLifecycleBudgetTypeId]'
GO
GRANT EXECUTE ON  [Baseline].[fnGetLifecycleBudgetTypeId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [BudgetFormulation].[fnGetActivePlanningStatusId]'
GO
GRANT EXECUTE ON  [BudgetFormulation].[fnGetActivePlanningStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [BudgetFormulation].[fnGetInactivePlanningStatusId]'
GO
GRANT EXECUTE ON  [BudgetFormulation].[fnGetInactivePlanningStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [CostSubmission].[fnGetNewAccountLineItemStatusId]'
GO
GRANT EXECUTE ON  [CostSubmission].[fnGetNewAccountLineItemStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [CostSubmission].[fnGetResetLineItemStatusId]'
GO
GRANT EXECUTE ON  [CostSubmission].[fnGetResetLineItemStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetCommitmentOverheadTransactionTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetCommitmentOverheadTransactionTypeId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetCommitmentTransactionTypeGroupId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetCommitmentTransactionTypeGroupId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetCostTransactionTypeGroupId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetCostTransactionTypeGroupId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetFinPlanGroupId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetFinPlanGroupId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetFundingTransactionTypeGroupId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetFundingTransactionTypeGroupId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetHQAFPChangeRequestTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetHQAFPChangeRequestTypeId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetLocalAFPChangeRequestTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetLocalAFPChangeRequestTypeId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetNFWAChangeRequestTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetNFWAChangeRequestTypeId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetSTARSAllowedCommitmentTransactionTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetSTARSAllowedCommitmentTransactionTypeId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Financial].[fnGetUnburdenedCommitmentTransactionTypeId]'
GO
GRANT EXECUTE ON  [Financial].[fnGetUnburdenedCommitmentTransactionTypeId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Inventory].[fnIsSiteSGIM]'
GO
GRANT EXECUTE ON  [Inventory].[fnIsSiteSGIM] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [ProspectiveFunding].[fnGetDeletedRequestDetailStatusId]'
GO
GRANT EXECUTE ON  [ProspectiveFunding].[fnGetDeletedRequestDetailStatusId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeGroupId]'
GO
GRANT EXECUTE ON  [ProspectiveFunding].[fnGetFundingRequestChangeRequestTypeGroupId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Report].[fnGetNA21FSUAndAsiaWorkBreakdownStructureId]'
GO
GRANT EXECUTE ON  [Report].[fnGetNA21FSUAndAsiaWorkBreakdownStructureId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetBuildingExcludeSPAInd]'
GO
GRANT EXECUTE ON  [SPA].[fnGetBuildingExcludeSPAInd] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdApproved]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdApproved] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdInProgress]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdInProgress] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdPassedBackbySPALabLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdPassedBackbySPALabLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdPassedBackbySPAPortfolioManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdPassedBackbySPAPortfolioManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdPassedBackbySPASubProgramManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdPassedBackbySPASubProgramManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdPassedBackbySPATeamLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdPassedBackbySPATeamLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdRejectedbySPALabLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdRejectedbySPALabLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdRejectedbySPAPortfolioManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdRejectedbySPAPortfolioManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdRejectedbySPAProtectTechnicalLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdRejectedbySPASubProgramManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdRejectedbySPASubProgramManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdRejectedbySPATeamLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdRejectedbySPATeamLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdReturnedToOriginator]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdReturnedToOriginator] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdSubmittedtoSPALabLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdSubmittedtoSPALabLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdSubmittedtoSPAPortfolioManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdSubmittedtoSPAPortfolioManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdSubmittedtoSPASubProgramManager]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdSubmittedtoSPASubProgramManager] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [SPA].[fnGetSPAStatusIdSubmittedtoSPATeamLead]'
GO
GRANT EXECUTE ON  [SPA].[fnGetSPAStatusIdSubmittedtoSPATeamLead] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
