/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 3:05:35 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating types'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [YearEndForecast].[LineItemTable] AS TABLE
(
[Id] [int] NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[ReviewedAmount] [money] NOT NULL,
[ReviewerExplanation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [YearEndForecast].[ImportLineItemTable] AS TABLE
(
[Performer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullWBS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Explanation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Workflow].[PersonRoleChangeMaintenanceTable] AS TABLE
(
[SubSystemId] [int] NOT NULL,
[PersonId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Workflow].[PersonApprovalPrivilegesTable] AS TABLE
(
[PersonId] [int] NULL,
[RoleId] [int] NULL,
[SubSystemId] [int] NOT NULL,
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestDetailStatusId] [int] NULL,
[ChangeRequestTypeId] [int] NULL,
[WorkflowLevel] [int] NULL,
[PreviousWorkflowLevel] [int] NULL,
[FinPlanRequestId] [int] NULL,
[ConcurInd] [bit] NULL,
[CourtesyConcurInd] [bit] NULL,
[ModifiedBy] [int] NULL,
[ModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Workflow].[PermissionChangeMaintenanceTable] AS TABLE
(
[SubSystemId] [int] NOT NULL,
[PermissionId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[WorkflowLevel] [int] NULL,
[ChangeRequestTypeId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[RoleOnlyOverrideInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Travel].[TripRequestSave] AS TABLE
(
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NULL,
[TripId] [int] NULL,
[TripRequestId] [int] NULL,
[TripTypeId] [int] NOT NULL,
[ProjectedCost] [money] NOT NULL,
[ActualCost] [money] NULL,
[RequestingOrganizationPartyId] [int] NOT NULL,
[FundingOrganizationPartyId] [int] NOT NULL,
[TravelerOrganizationPartyId] [int] NOT NULL,
[TravelerPersonPartyId] [int] NOT NULL,
[PriorYearFundsInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Travel].[TripLegRequestSave] AS TABLE
(
[TripLegRequestId] [int] NULL,
[TripRequestId] [int] NULL,
[TripLegId] [int] NULL,
[Location] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GoogleLocationReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BeginDt] [date] NOT NULL,
[EndDt] [date] NOT NULL,
[Explanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Travel].[OrganizationAnnualBudgetSave] AS TABLE
(
[OrganizationPartyId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Sustain].[SubElementAnswerUpdateTable] AS TABLE
(
[SubElementId] [int] NOT NULL,
[AssessmentId] [int] NOT NULL,
[Answer] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([SubElementId], [AssessmentId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Sustain].[SubElementAndAnswerTable] AS TABLE
(
[ElementId] [int] NULL,
[ElementName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElementScore] [numeric] (4, 2) NULL,
[SharedRatingApplyInd] [bit] NULL,
[SubElementId] [int] NULL,
[SubElementTypeId] [int] NULL,
[SubElementQuestion] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubElementParentId] [int] NULL,
[SubElementWeight] [numeric] (4, 2) NULL,
[SubElementIndicatorLevelId] [int] NULL,
[SubElementIndicatorLevelName] [int] NULL,
[SortOrder] [int] NULL,
[AssessmentId] [int] NULL,
[AssessmentDt] [datetime] NULL,
[SubElementAnswerId] [int] NULL,
[SubElementAnswerAnswer] [sql_variant] NULL,
[SubElementAnswerComment] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Sustain].[AssessmentElementSummaryTable] AS TABLE
(
[OfficeId] [int] NULL,
[CountryId] [int] NULL,
[StateId] [int] NULL,
[SiteId] [int] NULL,
[ElementLevelId] [int] NULL,
[ElementId] [int] NULL,
[ElementName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequiredInd] [bit] NULL,
[AssessmentId] [int] NULL,
[AssessmentDt] [datetime] NULL,
[AverageRating] [numeric] (4, 2) NULL,
[AverageAdjustedScore] [numeric] (4, 2) NULL,
[WeightedAverageScore] [numeric] (4, 2) NULL,
[SharedRatingApplyInd] [bit] NULL,
[CostSharingRatingValue] [numeric] (4, 2) NULL,
[FinalElementScore] [numeric] (4, 2) NULL,
[SortOrder] [int] NULL,
[SummaryDt] [datetime] NULL,
[SubElementQuestionSetId] [int] NULL,
[AcceptabilityStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Security].[ReportPermissionRole] AS TABLE
(
[RoleId] [int] NOT NULL,
[Permissionid] [int] NOT NULL,
[WBSId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Security].[PersonRoleChangeTable] AS TABLE
(
[PersonRoleId] [int] NOT NULL,
[SubSystemId] [int] NOT NULL,
[PersonId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[CreatedBy] [int] NOT NULL,
[CreatedAs] [int] NOT NULL,
[LastModifiedBy] [int] NOT NULL,
[LastModifiedAs] [int] NOT NULL,
[DeletedId] [int] NULL,
[WorkBreakdownStructureChangeInd] [bit] NOT NULL DEFAULT ((0)),
[OrganizationChangeInd] [bit] NOT NULL DEFAULT ((0)),
[RequireWorkBreakdownStructureRebuildInd] [bit] NOT NULL DEFAULT ((0)),
[RequireOrganizationRebuildInd] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED  ([PersonRoleId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Security].[PermissionRoleChangeTable] AS TABLE
(
[PermissionRoleId] [int] NOT NULL,
[SubSystemId] [int] NOT NULL,
[PermissionId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[withAdvance] [bit] NOT NULL,
[WorkflowLevel] [int] NULL,
[ChangeRequestTypeId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[PermissionObjectId] [int] NULL,
[ReportCatalogId] [int] NULL,
[SPAStatusId] [int] NULL,
[PreventFullDetailExpansionInd] [bit] NOT NULL,
[CreatedBy] [int] NOT NULL,
[CreatedAs] [int] NOT NULL,
[LastModifiedBy] [int] NOT NULL,
[LastModifiedAs] [int] NOT NULL,
[DeletedId] [int] NULL,
[WorkBreakdownStructureChangeInd] [bit] NOT NULL DEFAULT ((0)),
[OrganizationChangeInd] [bit] NOT NULL DEFAULT ((0)),
[RequireWorkBreakdownStructureRebuildInd] [bit] NOT NULL DEFAULT ((0)),
[RequireOrganizationRebuildInd] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED  ([PermissionRoleId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [STARS].[ReportingEntityChiefFinanceOfficeReportingEntityXrefSave] AS TABLE
(
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[ChiefFinanceOfficeReportingEntity] [int] NOT NULL,
[CurrentInd] [bit] NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[SecurityActionAnswerUpdateTable] AS TABLE
(
[SecurityActionId] [int] NOT NULL,
[SPAMainId] [int] NOT NULL,
[Revision] [int] NOT NULL,
[EntityTypeId] [int] NOT NULL,
[RefId] [int] NOT NULL,
[ExistsInd] [bit] NOT NULL,
[RecommendedInd] [bit] NOT NULL,
[NANRInd] [bit] NOT NULL,
[CompletedInd] [bit] NOT NULL,
[FollowOnInd] [bit] NOT NULL,
[FollowOnCompletedInd] [bit] NOT NULL,
[Upgrades] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraInfo] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([SecurityActionId], [SPAMainId], [Revision], [EntityTypeId], [RefId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[SPAMainSectionUpdateTable] AS TABLE
(
[SPAMainId] [int] NOT NULL,
[Revision] [int] NOT NULL,
[EntityTypeId] [int] NOT NULL,
[RefId] [int] NOT NULL,
[MaterialTypeCountRad] [int] NULL,
[MaterialTypeCountNuc] [int] NULL,
PRIMARY KEY CLUSTERED  ([SPAMainId], [Revision], [EntityTypeId], [RefId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[ResponseAnswerSave] AS TABLE
(
[SPAMainId] [int] NOT NULL,
[EntityTypeId] [int] NOT NULL,
[ResponseQuestionId] [int] NOT NULL,
[AnswerText] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnswerComment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[RMSUpdateTable] AS TABLE
(
[Id] [int] NOT NULL,
[SerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeaturesImplementationOptionNoteId] [int] NULL,
[MonitoringLocationsImplementationOptionNoteId] [int] NULL,
[CommunicationsImplementationOptionNoteId] [int] NULL,
[AlarmResolutionImplementationOptionNoteId] [int] NULL,
[InstallInd] [int] NULL,
PRIMARY KEY CLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[RMSMatrixAnswerUpdateTable] AS TABLE
(
[RMSMatrixId] [int] NOT NULL,
[SPAMainId] [int] NOT NULL,
[RMSId] [int] NOT NULL,
[NoteId] [int] NULL,
[CheckedInd] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([RMSMatrixId], [SPAMainId], [RMSId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[RMSMatrixAnswerTable] AS TABLE
(
[RMSMatrixId] [int] NOT NULL,
[SPAMainId] [int] NOT NULL,
[RoomId] [int] NOT NULL,
[NoteId] [int] NULL,
PRIMARY KEY CLUSTERED  ([RMSMatrixId], [SPAMainId], [RoomId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[MonitoringAnswerSave] AS TABLE
(
[SPAMainId] [int] NOT NULL,
[EntityTypeId] [int] NOT NULL,
[MonitoringQuestionId] [int] NOT NULL,
[AnswerText] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnswerComment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[CountryAssessmentTable] AS TABLE
(
[Id] [int] NULL,
[CountryId] [int] NULL,
[Revision] [int] NULL,
[RevisionPurposeId] [int] NULL,
[IsCompleteInd] [bit] NULL,
[SPAStatusId] [int] NULL,
[SPAStatusDt] [datetime] NULL,
[SPAStatusNoteId] [int] NULL,
[NoteId] [int] NULL,
[Note] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [SPA].[CostSharingDetailUpdateTable] AS TABLE
(
[CostShareSubCategoryId] [int] NOT NULL,
[Variable] [int] NOT NULL,
[Weight] [numeric] (3, 2) NOT NULL,
[SiteId] [int] NULL,
[DefaultVariable] [int] NULL,
[DefaultWeight] [numeric] (3, 2) NULL,
[IsIncludedInd] [bit] NULL,
PRIMARY KEY CLUSTERED  ([CostShareSubCategoryId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [ProspectiveFunding].[RequestSave] AS TABLE
(
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[BnrId] [int] NULL,
[FundTypeId] [int] NOT NULL,
[Amount] [money] NOT NULL,
[Explanation] [nvarchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [ProspectiveFunding].[RequestDetailSplitTable] AS TABLE
(
[RequestDetailId] [int] NULL,
[BNRId] [int] NOT NULL,
[FundTypeId] [int] NOT NULL,
[Amount] [money] NOT NULL,
[AppropriationYear] [int] NULL,
[FundingSourceId] [int] NULL,
[Explanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [ProspectiveFunding].[AutoMatchTable] AS TABLE
(
[FromRequestDetailId] [int] NOT NULL,
[ToRequestDetailId] [int] NOT NULL,
[Amount] [money] NOT NULL,
[FromBNRId] [int] NOT NULL,
[FromFundTypeId] [int] NOT NULL,
[ToBNRId] [int] NOT NULL,
[ToFundTypeId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Proposal].[SolicitationTagSave] AS TABLE
(
[Value] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Proposal].[AttachmentSave] AS TABLE
(
[AttachmentId] [int] NULL,
[Name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Length] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Content] [varbinary] (max) NULL,
[MimeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Performance].[MetricPlanChangeSave] AS TABLE
(
[MilestoneId] [int] NOT NULL,
[SequenceNumber] [int] NOT NULL,
[MetricId] [int] NOT NULL,
[FiscalPeriod] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[ForecastValue] [decimal] (18, 4) NOT NULL,
[ActualInd] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([MilestoneId], [SequenceNumber], [MetricId], [FiscalPeriod], [FiscalYear])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Notification].[PersonRoleChangeTable] AS TABLE
(
[PersonRoleId] [int] NOT NULL,
[SubSystemId] [int] NOT NULL,
[PersonId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[SPATeamId] [int] NULL,
[OrganizationPartyId] [int] NULL,
PRIMARY KEY CLUSTERED  ([PersonRoleId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [MDI].[ScoreMatrixSave] AS TABLE
(
[Id] [int] NOT NULL,
[Value] [decimal] (18, 4) NOT NULL,
UNIQUE NONCLUSTERED  ([Id])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Inquiry].[QuestionnaireAnswerSave] AS TABLE
(
[QuestionnaireId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[AnswerId] [int] NULL,
[UserText] [nvarchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Geo].[UniqueWorkBreakdownStructureIdReportingEntityOrganizationPartyIdTable] AS TABLE
(
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([WorkBreakdownStructureId], [ReportingEntityOrganizationPartyId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Geo].[GeoBookmarkPersonXrefTable] AS TABLE
(
[Id] [int] NULL,
[PersonId] [int] NULL,
[BookMarkId] [int] NULL,
[DefaultInd] [bit] NULL,
[CreatedDt] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDt] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureSave] AS TABLE
(
[SubSystemId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[ParentWorkBreakdownStructureId] [int] NOT NULL,
[FullWBS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (376) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkTypeId] [int] NULL,
[SubSystemLevelId] [int] NOT NULL,
[ConfigurationGroupId] [int] NULL,
[CountryId] [int] NULL,
[StateId] [int] NULL,
[IndirectCostConfiguration] [bit] NULL,
[OtherDirectCostConfiguration] [bit] NULL,
[BudgetFormulationConfiguration] [bit] NULL,
[BudgetFormulationGroupConfiguration] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeSave] AS TABLE
(
[WorkBreakdownStructureChangeId] [int] NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeQuestionnaireAnswerSave] AS TABLE
(
[QuestionnaireId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[AnswerId] [int] NOT NULL,
[UserText] [nvarchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeQuestionDetailSave] AS TABLE
(
[Alternatives] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpaceEliminatedSqFt] [decimal] (18, 0) NULL,
[HighRiskFacilityInd] [bit] NULL,
[OwnedByOtherProgramInd] [bit] NULL,
[OtherProgramDispositionCost] [decimal] (18, 0) NULL,
[IncludedInProgramBudgetInd] [bit] NULL,
[ProgramBudgetYear] [int] NULL,
[WorkBreakdownStructurePlanStatusId] [int] NULL,
[CompletionDate] [datetime] NULL,
[CompletedTotalCost] [decimal] (18, 0) NULL,
[WorkPlaceImprovementInd] [bit] NULL,
[PropertyType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LifeExpectancyYears] [int] NULL,
[AnnualOandMSavings] [decimal] (18, 0) NULL,
[AnnualOandMSavingsExplanation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesignIndependentOfExecutionInd] [bit] NULL,
[AdvanceDesignBenefitInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangePlanDetailSave] AS TABLE
(
[OrganizationPartyId] [int] NOT NULL,
[FundingProgramId] [int] NOT NULL,
[YearRequirementIdentified] [int] NULL,
[TimeFrame] [int] NOT NULL,
[EstimatedCompletionFY] [int] NULL,
[SpecialInterestCodeInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeContactSave] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Title] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactTypeId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeBudgetSave] AS TABLE
(
[BudgetTypeId] [int] NOT NULL,
[FiscalYear] [int] NULL,
[Amount] [decimal] (14, 2) NOT NULL,
[BudgetRange] [decimal] (4, 3) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeAttributeSave] AS TABLE
(
[Id] [int] NULL,
[WorkBreakdownStructureAttributeTypeId] [int] NOT NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeAttachmentSave] AS TABLE
(
[Id] [int] NULL,
[Name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Extension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Length] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [varbinary] (max) NULL,
[MimeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[WorkBreakdownStructureChangeAssetSave] AS TABLE
(
[Id] [int] NULL,
[AssetId] [int] NULL,
[GrossSquareFeetAdded] [decimal] (10, 0) NULL,
[GrossSquareFeetEliminated] [decimal] (10, 0) NULL,
[EMEvaluatedInd] [bit] NULL,
[YearOfBeneficialOccupancy] [int] NULL,
[PropertyTypeId] [int] NULL,
[PrimaryAssetInd] [bit] NULL,
[PrimaryCapabilityId] [int] NULL,
[SecondaryCapabilityId] [int] NULL,
[TertiaryCapabilityId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[TagSave] AS TABLE
(
[ChangeRequestDetailId] [int] NULL,
[SolicitationId] [int] NULL,
[TagValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[TagGroupSave] AS TABLE
(
[TagGroupId] [int] NOT NULL,
[ChangeRequestDetailId] [int] NULL,
[TagValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[SpendPlanDetailInsertTable] AS TABLE
(
[SpendPlanHeaderId] [int] NULL,
[FiscalPeriod] [int] NULL,
[CostForecast] [decimal] (18, 4) NULL,
[CommitmentForecast] [decimal] (18, 4) NULL,
[FundingActual] [decimal] (18, 4) NULL,
[SnapShotStartDate] [datetime] NULL,
[CreatedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[CreatedAs] [int] NULL,
[LastModifiedDate] [datetime] NULL,
[LastModifiedBy] [int] NULL,
[LastModifiedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[NAFDSpendPlanUpdateTable] AS TABLE
(
[ProjectId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[FiscalPeriod] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([ProjectId], [FiscalYear], [FiscalPeriod])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[FundingCaseDetailSave] AS TABLE
(
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[ExplanationTemplateRequestTable] AS TABLE
(
[UniqueKey] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChangeRequestTypeGroupId] [int] NOT NULL,
[SubSystemId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[WorkTypeId] [int] NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[MilestoneTemplateId] [int] NULL,
[ToFromFlag] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([UniqueKey])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[CongressionalJouleTargetChangesTable] AS TABLE
(
[TaskId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[NewTarget] [decimal] (18, 4) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[ChangeRequestDetailUpdateTable] AS TABLE
(
[ChangeRequestDetailId] [int] NOT NULL,
[FinPlanRequestId] [int] NULL,
[ChangeRequestActionId] [int] NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreviousChangeRequestDetailId] [int] NOT NULL,
[PreviousWorkflowLevel] [int] NOT NULL,
[PreviousChangeRequestId] [int] NOT NULL,
[PreviousChangeRequestTypeId] [int] NOT NULL,
[PreviousChangeRequestDetailStatusId] [int] NOT NULL,
[PreviousPeriodId] [int] NOT NULL,
[LastModifiedBy] [int] NOT NULL,
[LastModifiedAs] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[ChangeRequestDetailRowVersionKeeperTouchTable] AS TABLE
(
[ChangeRequestDetailId] [int] NOT NULL,
[LastModifiedDt] [datetime] NOT NULL,
[LastModifiedBy] [int] NOT NULL,
[LastModifiedAs] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[ChangeRequestDetailConcurrenceTable] AS TABLE
(
[ChangeRequestDetailId] [int] NOT NULL,
[FinPlanRequestId] [int] NULL,
[WorkflowLevel] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NULL,
[OrganizationPartyId] [int] NULL,
[Side] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForPersonId] [int] NOT NULL,
[ByPersonId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[StatusDate] [datetime] NOT NULL DEFAULT (getdate()),
[Comments] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoConcurInd] [bit] NOT NULL DEFAULT ((0)),
[CourtesyConcurInd] [bit] NOT NULL DEFAULT ((0)),
[ActiveInd] [bit] NOT NULL DEFAULT ((1)),
[RoleId] [int] NOT NULL,
[SavedBy] [int] NOT NULL,
[SavedAs] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[ChangeRequestDetailActionTable] AS TABLE
(
[ChangeRequestDetailId] [int] NULL,
[WorkFlowLevel] [int] NULL,
[ApprovedBy] [int] NULL,
[ApprovedAs] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[AttachmentSave] AS TABLE
(
[Name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Length] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Content] [varbinary] (max) NOT NULL,
[MimeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [G2].[AccountTransactionChange] AS TABLE
(
[AccountId] [int] NOT NULL,
[PeriodId] [int] NOT NULL,
[TransactionTypeId] [int] NOT NULL,
[DeltaAmount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Financial].[FundTypeTable] AS TABLE
(
[RowId] [int] NOT NULL,
[Id] [int] NULL,
[SubSystemId] [int] NOT NULL,
[ShortName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundValue] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActiveInd] [bit] NOT NULL,
[PrimaryInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Financial].[FinancialTransactionSave] AS TABLE
(
[FundTypeId] [int] NULL,
[BNRId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[PeriodId] [int] NULL,
[TransactionDt] [datetime] NULL,
[Amount] [money] NULL,
[Explanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionTypeId] [int] NULL,
[FinPlanRequestId] [int] NULL,
[FromInd] [bit] NULL,
[FundingAdjustmentId] [int] NULL,
[BatchId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Financial].[FinPlanSave] AS TABLE
(
[FinPlanSaveId] [int] NOT NULL,
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestTypeId] [int] NOT NULL,
[PeriodId] [int] NOT NULL,
[ToWorkBreakdownStructureId] [int] NOT NULL,
[ToReportingEntityOrganizationPartyId] [int] NOT NULL,
[ToFundTypeId] [int] NOT NULL,
[ToBNRId] [int] NOT NULL,
[ToExplanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromWorkBreakdownStructureId] [int] NOT NULL,
[FromReportingEntityOrganizationPartyId] [int] NOT NULL,
[FromFundTypeId] [int] NOT NULL,
[FromBNRId] [int] NOT NULL,
[FromExplanation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[LoanMemoInd] [bit] NOT NULL,
[AppropriationYear] [int] NOT NULL,
[FundingSourceId] [int] NOT NULL,
[RequestTimeStamp] [binary] (8) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Financial].[BNRTable] AS TABLE
(
[RowId] [int] NOT NULL,
[Id] [int] NULL,
[SubSystemId] [int] NOT NULL,
[ControlPointId] [int] NOT NULL,
[ParentId] [int] NULL,
[ShortName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InformalCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProgramValue] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppropriationStartFY] [int] NOT NULL,
[AppropriationEndFY] [int] NULL,
[ActiveInd] [bit] NOT NULL,
[AbbreviatedName] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[WorkBreakdownStructurePerformerDistributionImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[BNRId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[ProspectiveFundingRequestImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[FullWBS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Performer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiscalYear] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Explanation] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Worksheet] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[PlanningTargetImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[ReportProjectGroupingId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[AllocationAmount] [money] NULL,
[MetricId] [int] NULL,
[MetricValue] [decimal] (18, 4) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[PerformanceReportRawImportTable] AS TABLE
(
[RowNum] [int] NOT NULL,
[BatchId] [int] NOT NULL,
[ProjectMilestoneId] [nvarchar] (41) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewForecastDt] [nvarchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewCompletedInd] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewTaskStatus] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[PerformanceReportImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[ProjectMilestoneId] [int] NOT NULL,
[NewLeadPerformer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewFinishDt] [datetime] NOT NULL,
[NewCompletedInd] [bit] NOT NULL,
[Notes] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[InfrastructurePlanningScoreImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[WorkBreakdownStructureChangeId] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Score] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreSafety] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreProgram] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreSustainability] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreRoi] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreLnSafetyRRTec] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreSafetyRRTec] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreTotalSafetyRR] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreLnMdiAdjustedProgramRRTec] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreMdi] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreProgramRRTec] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreTotalProgramRR] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreSirSustainability] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreNpvSustainabilityConsumptionSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScorePvFactorSustainability] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreAnnualElectricSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreAnnualWaterSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreAnnualNaturalGasSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreSirRoi] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreNpvSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScoreAnnualOandMSavings] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[BudgetByElementOfCostImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[ElementOfCostTypeId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[RequestedBudgetAmount] [money] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[BaselineChangeRequestLifecycleBudgetAnnualRequestImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[RequestedAmount] [money] NOT NULL,
[UnconstrainedBudgetAmount] [money] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [FileImport].[BaselineChangeRequestImportTable] AS TABLE
(
[BatchId] [int] NOT NULL,
[RowNumber] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[TaskId] [int] NULL,
[TaskChangeId] [int] NULL,
[RowVersion] [binary] (8) NULL,
[MilestoneId] [int] NOT NULL,
[SequenceNumber] [int] NULL,
[OldLeadPerformer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewLeadPerformer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewBaselineDt] [datetime] NOT NULL,
[MetricId] [int] NULL,
[NewMetricValue] [decimal] (18, 4) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [CostSubmission].[ReportingEntityNoteSave] AS TABLE
(
[ChangeRequestDetailId] [int] NOT NULL,
[Note] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [CostSubmission].[LineItemSave] AS TABLE
(
[LineItemId] [int] NULL,
[RowVersion] [binary] (8) NULL,
[TransactionCategoryId] [int] NULL,
[TransactionTypeId] [int] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [CostSubmission].[BatchAccountNoteSave] AS TABLE
(
[BatchId] [int] NOT NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[FundTypeId] [int] NOT NULL,
[BNRId] [int] NOT NULL,
[TransactionCategoryId] [int] NOT NULL,
[NoteTypeId] [int] NOT NULL,
[Note] [nvarchar] (3975) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatchAccountNoteRowVersion] [binary] (8) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Contact].[PhoneSave] AS TABLE
(
[ContactMechanismId] [int] NULL,
[Number] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extension] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactMechanismTypeId] [int] NOT NULL,
[PreferredInd] [bit] NULL,
[Comment] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Contact].[PartyRelationshipSave] AS TABLE
(
[PartyRoleId] [int] NULL,
[PartyRelationshipId] [int] NULL,
[ToPartyId] [int] NOT NULL,
[Comment] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Contact].[FlexiplaceTable] AS TABLE
(
[MedicalInd] [bit] NULL,
[RegularInd] [bit] NULL,
[SituationalInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Contact].[FacilitySave] AS TABLE
(
[Id] [int] NULL,
[MailStop] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Abbreviation] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FacilityTypeId] [int] NOT NULL,
[ParentId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Contact].[EmailSave] AS TABLE
(
[ContactMechanismId] [int] NULL,
[Address] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactMechanismTypeId] [int] NOT NULL,
[PreferredInd] [bit] NULL,
[Comment] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[ParticipatingSiteDetailTable] AS TABLE
(
[SiteId] [int] NOT NULL,
[EventParticipantId] [int] NULL,
[ParticipantRoleId] [int] NOT NULL,
[AllocatedSeats] [int] NOT NULL,
[Comment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NULL,
PRIMARY KEY CLUSTERED  ([SiteId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[ParticipatingLeaDetailTable] AS TABLE
(
[LeaId] [int] NOT NULL,
[EventParticipantId] [int] NULL,
[ParticipantRoleId] [int] NOT NULL,
[AllocatedSeats] [int] NOT NULL,
[Comment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NULL,
PRIMARY KEY CLUSTERED  ([LeaId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[ParticipatingCountryDetailTable] AS TABLE
(
[CountryId] [int] NOT NULL,
[EventParticipantId] [int] NULL,
[ParticipantRoleId] [int] NOT NULL,
[AllocatedSeats] [int] NOT NULL,
[Comment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeRequestDetailId] [int] NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NULL,
PRIMARY KEY CLUSTERED  ([CountryId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[EventParticipantDetailTable] AS TABLE
(
[EventParticipantId] [int] NOT NULL,
[ParticipantRoleId] [int] NOT NULL,
[AllocatedSeats] [int] NOT NULL,
[Comment] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangeRequestDetailId] [int] NOT NULL,
[ChangeRequestDetailRowVersion] [binary] (8) NOT NULL,
PRIMARY KEY CLUSTERED  ([EventParticipantId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[EventContactTable] AS TABLE
(
[EventId] [int] NULL,
[PartyId] [int] NULL,
[EventRoleId] [int] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Suffix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreferredName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NOT NULL,
[StateId] [int] NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Calendar].[EventAttendeeTable] AS TABLE
(
[EventId] [int] NOT NULL,
[SiteId] [int] NULL,
[LeaId] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreferredName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Organization] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PositionTitle] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttendeeCategoryId] [int] NULL,
[AttendeeResponsibilityId] [int] NULL,
[OrganizationCategoryId] [int] NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HomeCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [int] NULL,
[Occupation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[RequestWorkforceSave] AS TABLE
(
[FiscalYear] [int] NOT NULL,
[Amount] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([FiscalYear])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[RequestRankSave] AS TABLE
(
[ScenarioId] [int] NOT NULL,
[RequestId] [int] NOT NULL,
[RankTypeId] [int] NOT NULL,
[Rank] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([ScenarioId], [RequestId], [RankTypeId], [Rank])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[RequestNoteSave] AS TABLE
(
[BudgetCategoryId] [int] NOT NULL,
[Note] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
PRIMARY KEY CLUSTERED  ([BudgetCategoryId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[RequestFederalFundingSave] AS TABLE
(
[BudgetCategoryId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL,
PRIMARY KEY CLUSTERED  ([BudgetCategoryId], [FiscalYear])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[RequestBudgetSave] AS TABLE
(
[BudgetCategoryId] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL,
PRIMARY KEY CLUSTERED  ([BudgetCategoryId], [FiscalYear])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [BudgetFormulation].[BudgetCaseDetailSave] AS TABLE
(
[Id] [int] NULL,
[WorkBreakdownStructureId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[FiscalYear] [int] NOT NULL,
[Amount] [money] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Baseline].[TaskChangeSave] AS TABLE
(
[TaskId] [int] NULL,
[TaskRequestId] [int] NULL,
[MilestoneTemplateId] [int] NOT NULL,
[NameBase] [varchar] (275) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameSuffix] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteId] [int] NULL,
[BuildingId] [int] NULL,
[SourceId] [int] NULL,
[LeadReportingEntityOrganizationPartyId] [int] NULL,
[DeletedInd] [bit] NOT NULL,
[CancelDt] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Baseline].[MilestoneChangeSave] AS TABLE
(
[MilestoneId] [int] NOT NULL,
[SequenceNumber] [int] NOT NULL,
[BaselineFinishDt] [datetime] NOT NULL,
[CompletedInd] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([MilestoneId], [SequenceNumber])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Baseline].[MetricPlanChangeSave] AS TABLE
(
[MilestoneId] [int] NOT NULL,
[SequenceNumber] [int] NOT NULL,
[MetricId] [int] NOT NULL,
[FiscalPeriod] [int] NOT NULL,
[FiscalYear] [int] NOT NULL,
[BaselineValue] [decimal] (18, 4) NOT NULL,
PRIMARY KEY CLUSTERED  ([MilestoneId], [SequenceNumber], [MetricId], [FiscalPeriod], [FiscalYear])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Baseline].[MetricChangeSave] AS TABLE
(
[MilestoneId] [int] NOT NULL,
[SequenceNumber] [int] NOT NULL,
[MetricId] [int] NOT NULL,
[BaselineValue] [decimal] (18, 4) NOT NULL,
PRIMARY KEY CLUSTERED  ([MilestoneId], [SequenceNumber], [MetricId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Baseline].[LifecycleBudgetChangeSave] AS TABLE
(
[FiscalYear] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[RequestedAmount] [money] NULL,
[RequestedUnconstrainedBudgetAmount] [money] NULL,
[DeletedInd] [bit] NOT NULL,
PRIMARY KEY CLUSTERED  ([FiscalYear], [ReportingEntityOrganizationPartyId])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[SupportingAssetRequestSave] AS TABLE
(
[SupportingAssetRequestId] [int] NULL,
[SupportingAssetId] [int] NULL,
[SupportedAssetId] [int] NOT NULL,
[SortOrder] [int] NOT NULL,
[Justification] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[SupportingAssetQuestionAnswerRequestSave] AS TABLE
(
[SupportingAssetQuestionAnswerRequestId] [int] NULL,
[SupportingAssetRequestId] [int] NULL,
[SupportingAssetQuestionAnswerId] [int] NULL,
[SupportedAssetId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[AnswerId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[QuestionnaireAnswerRequestSave] AS TABLE
(
[QuestionnaireId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[AnswerId] [int] NOT NULL,
[UserText] [nvarchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[ContactRequestSave] AS TABLE
(
[ContactRequestId] [int] NULL,
[AssetContactId] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Title] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FederalEmployeeInd] [bit] NOT NULL,
[PrimaryInd] [bit] NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[CapabilityRequestSave] AS TABLE
(
[CapabilityRequestId] [int] NULL,
[AssetCapabilityId] [int] NULL,
[CapabilityId] [int] NOT NULL,
[SortOrder] [int] NOT NULL,
[Justification] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[CapabilityQuestionAnswerRequestSave] AS TABLE
(
[CapabilityQuestionAnswerRequestId] [int] NULL,
[CapabilityRequestId] [int] NULL,
[AssetCapabilityQuestionAnswerId] [int] NULL,
[CapabilityId] [int] NOT NULL,
[QuestionId] [int] NOT NULL,
[AnswerId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[AttachmentRequestSave] AS TABLE
(
[AttachmentId] [int] NULL,
[AttachmentRequestId] [int] NULL,
[Name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Extension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Length] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [varbinary] (max) NULL,
[MimeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultInd] [bit] NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [AssetManagement].[AliasRequestSave] AS TABLE
(
[AliasRequestId] [int] NULL,
[AssetAliasId] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryInd] [bit] NOT NULL,
[SensitiveInd] [bit] NOT NULL,
[DeletedInd] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Assessment].[AssessmentResponseCommentSave] AS TABLE
(
[Id] [int] NULL,
[ResponseCommentTypeId] [int] NULL,
[Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Assessment].[AssessmentContactSave] AS TABLE
(
[Id] [int] NULL,
[AssessmentId] [int] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadContactInd] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Assessment].[AssessmentAttachmentSave] AS TABLE
(
[Id] [int] NULL,
[AssessmentHierarchyId] [int] NULL,
[Name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Extension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Length] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [varbinary] (max) NULL,
[MimeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Activity].[AssumptionTable] AS TABLE
(
[AssumptionId] [int] NULL,
[ActivityId] [int] NULL,
[WorkBreakdownStructureId] [int] NULL,
[ReportingEntityOrganizationPartyId] [int] NULL,
[AssumptionText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [Activity].[ActivityFundingRequestTable] AS TABLE
(
[Id] [int] NULL,
[ChangeRequestDetailId] [int] NULL,
[ActivityWorkBreakdownStructureXrefId] [int] NOT NULL,
[ReportingEntityOrganizationPartyId] [int] NOT NULL,
[FundTypeId] [int] NOT NULL,
[Amount] [money] NULL,
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Activity].[ActivityFundingRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [Activity].[ActivityFundingRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Activity].[AssumptionTable]'
GO
GRANT EXECUTE ON TYPE:: [Activity].[AssumptionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Assessment].[AssessmentAttachmentSave]'
GO
GRANT EXECUTE ON TYPE:: [Assessment].[AssessmentAttachmentSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Assessment].[AssessmentContactSave]'
GO
GRANT EXECUTE ON TYPE:: [Assessment].[AssessmentContactSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Assessment].[AssessmentResponseCommentSave]'
GO
GRANT EXECUTE ON TYPE:: [Assessment].[AssessmentResponseCommentSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[AliasRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[AliasRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[AttachmentRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[AttachmentRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[CapabilityQuestionAnswerRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[CapabilityQuestionAnswerRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[CapabilityRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[CapabilityRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[ContactRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[ContactRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[QuestionnaireAnswerRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[QuestionnaireAnswerRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[SupportingAssetQuestionAnswerRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[SupportingAssetQuestionAnswerRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [AssetManagement].[SupportingAssetRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [AssetManagement].[SupportingAssetRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Baseline].[LifecycleBudgetChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Baseline].[LifecycleBudgetChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Baseline].[MetricChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Baseline].[MetricChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Baseline].[MetricPlanChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Baseline].[MetricPlanChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Baseline].[MilestoneChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Baseline].[MilestoneChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Baseline].[TaskChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Baseline].[TaskChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[BudgetCaseDetailSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[BudgetCaseDetailSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[RequestBudgetSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[RequestBudgetSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[RequestFederalFundingSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[RequestFederalFundingSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[RequestNoteSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[RequestNoteSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[RequestRankSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[RequestRankSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [BudgetFormulation].[RequestWorkforceSave]'
GO
GRANT EXECUTE ON TYPE:: [BudgetFormulation].[RequestWorkforceSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[EventAttendeeTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[EventAttendeeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[EventContactTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[EventContactTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[EventParticipantDetailTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[EventParticipantDetailTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[ParticipatingCountryDetailTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[ParticipatingCountryDetailTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[ParticipatingLeaDetailTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[ParticipatingLeaDetailTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Calendar].[ParticipatingSiteDetailTable]'
GO
GRANT EXECUTE ON TYPE:: [Calendar].[ParticipatingSiteDetailTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Contact].[EmailSave]'
GO
GRANT EXECUTE ON TYPE:: [Contact].[EmailSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Contact].[FacilitySave]'
GO
GRANT EXECUTE ON TYPE:: [Contact].[FacilitySave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Contact].[FlexiplaceTable]'
GO
GRANT EXECUTE ON TYPE:: [Contact].[FlexiplaceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Contact].[PartyRelationshipSave]'
GO
GRANT EXECUTE ON TYPE:: [Contact].[PartyRelationshipSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Contact].[PhoneSave]'
GO
GRANT EXECUTE ON TYPE:: [Contact].[PhoneSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [CostSubmission].[BatchAccountNoteSave]'
GO
GRANT EXECUTE ON TYPE:: [CostSubmission].[BatchAccountNoteSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [CostSubmission].[LineItemSave]'
GO
GRANT EXECUTE ON TYPE:: [CostSubmission].[LineItemSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [CostSubmission].[ReportingEntityNoteSave]'
GO
GRANT EXECUTE ON TYPE:: [CostSubmission].[ReportingEntityNoteSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[BaselineChangeRequestImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[BaselineChangeRequestImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[BaselineChangeRequestLifecycleBudgetAnnualRequestImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[BaselineChangeRequestLifecycleBudgetAnnualRequestImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[BudgetByElementOfCostImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[BudgetByElementOfCostImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[InfrastructurePlanningScoreImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[InfrastructurePlanningScoreImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[PerformanceReportImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[PerformanceReportImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[PerformanceReportRawImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[PerformanceReportRawImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[PlanningTargetImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[PlanningTargetImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[ProspectiveFundingRequestImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[ProspectiveFundingRequestImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [FileImport].[WorkBreakdownStructurePerformerDistributionImportTable]'
GO
GRANT EXECUTE ON TYPE:: [FileImport].[WorkBreakdownStructurePerformerDistributionImportTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Financial].[BNRTable]'
GO
GRANT EXECUTE ON TYPE:: [Financial].[BNRTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Financial].[FinPlanSave]'
GO
GRANT EXECUTE ON TYPE:: [Financial].[FinPlanSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Financial].[FinancialTransactionSave]'
GO
GRANT EXECUTE ON TYPE:: [Financial].[FinancialTransactionSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Financial].[FundTypeTable]'
GO
GRANT EXECUTE ON TYPE:: [Financial].[FundTypeTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[AccountTransactionChange]'
GO
GRANT EXECUTE ON TYPE:: [G2].[AccountTransactionChange] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[AttachmentSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[AttachmentSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[ChangeRequestDetailActionTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[ChangeRequestDetailActionTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[ChangeRequestDetailConcurrenceTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[ChangeRequestDetailConcurrenceTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[CongressionalJouleTargetChangesTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[CongressionalJouleTargetChangesTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[ExplanationTemplateRequestTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[ExplanationTemplateRequestTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[FundingCaseDetailSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[FundingCaseDetailSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[SpendPlanDetailInsertTable]'
GO
GRANT EXECUTE ON TYPE:: [G2].[SpendPlanDetailInsertTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[TagGroupSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[TagGroupSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[TagSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[TagSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeAssetSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeAssetSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeAttachmentSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeAttachmentSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeAttributeSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeAttributeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeBudgetSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeBudgetSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeContactSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeContactSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangePlanDetailSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangePlanDetailSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeQuestionDetailSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeQuestionDetailSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeQuestionnaireAnswerSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeQuestionnaireAnswerSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [G2].[WorkBreakdownStructureSave]'
GO
GRANT EXECUTE ON TYPE:: [G2].[WorkBreakdownStructureSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Geo].[GeoBookmarkPersonXrefTable]'
GO
GRANT EXECUTE ON TYPE:: [Geo].[GeoBookmarkPersonXrefTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Geo].[UniqueWorkBreakdownStructureIdReportingEntityOrganizationPartyIdTable]'
GO
GRANT EXECUTE ON TYPE:: [Geo].[UniqueWorkBreakdownStructureIdReportingEntityOrganizationPartyIdTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Inquiry].[QuestionnaireAnswerSave]'
GO
GRANT EXECUTE ON TYPE:: [Inquiry].[QuestionnaireAnswerSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Performance].[MetricPlanChangeSave]'
GO
GRANT EXECUTE ON TYPE:: [Performance].[MetricPlanChangeSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Proposal].[AttachmentSave]'
GO
GRANT EXECUTE ON TYPE:: [Proposal].[AttachmentSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Proposal].[SolicitationTagSave]'
GO
GRANT EXECUTE ON TYPE:: [Proposal].[SolicitationTagSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [ProspectiveFunding].[AutoMatchTable]'
GO
GRANT EXECUTE ON TYPE:: [ProspectiveFunding].[AutoMatchTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [ProspectiveFunding].[RequestDetailSplitTable]'
GO
GRANT EXECUTE ON TYPE:: [ProspectiveFunding].[RequestDetailSplitTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [ProspectiveFunding].[RequestSave]'
GO
GRANT EXECUTE ON TYPE:: [ProspectiveFunding].[RequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[CostSharingDetailUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[CostSharingDetailUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[CountryAssessmentTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[CountryAssessmentTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[MonitoringAnswerSave]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[MonitoringAnswerSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[RMSMatrixAnswerTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[RMSMatrixAnswerTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[RMSMatrixAnswerUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[RMSMatrixAnswerUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[RMSUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[RMSUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[ResponseAnswerSave]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[ResponseAnswerSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[SPAMainSectionUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[SPAMainSectionUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [SPA].[SecurityActionAnswerUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [SPA].[SecurityActionAnswerUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [STARS].[ReportingEntityChiefFinanceOfficeReportingEntityXrefSave]'
GO
GRANT EXECUTE ON TYPE:: [STARS].[ReportingEntityChiefFinanceOfficeReportingEntityXrefSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Security].[ReportPermissionRole]'
GO
GRANT EXECUTE ON TYPE:: [Security].[ReportPermissionRole] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Sustain].[SubElementAnswerUpdateTable]'
GO
GRANT EXECUTE ON TYPE:: [Sustain].[SubElementAnswerUpdateTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Travel].[OrganizationAnnualBudgetSave]'
GO
GRANT EXECUTE ON TYPE:: [Travel].[OrganizationAnnualBudgetSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Travel].[TripLegRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [Travel].[TripLegRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Travel].[TripRequestSave]'
GO
GRANT EXECUTE ON TYPE:: [Travel].[TripRequestSave] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [Workflow].[PersonApprovalPrivilegesTable]'
GO
GRANT EXECUTE ON TYPE:: [Workflow].[PersonApprovalPrivilegesTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [YearEndForecast].[ImportLineItemTable]'
GO
GRANT EXECUTE ON TYPE:: [YearEndForecast].[ImportLineItemTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on TYPE:: [YearEndForecast].[LineItemTable]'
GO
GRANT EXECUTE ON TYPE:: [YearEndForecast].[LineItemTable] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
