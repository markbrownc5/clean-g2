/*
Run this script on:

        NN1DEVDB03.Maguires    -  This database will be modified

to synchronize it with:

        NN1DEVDB03.G2

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.3.5.6244 from Red Gate Software Ltd at 10/16/2018 4:08:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetViewReportPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/3/2013
-- Release:     4.4
-- Description: Retrieves the View Report permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetViewReportPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 26; -- View Report
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnActiveNuclearProjectTaskExists]'
GO


-- =============================================================
-- Author:      Robin Auerbach
-- Create date: 08/01/2012
-- Description: Will return 1 if a uncompleted task already exists for a nuclear project and 0 otherwise. If the
--			project is not a nuclear project then the function will return 0  
--
-- NOTE: Used in constraint validation on table g2.task [CK_Task_OneActiveNuclear]
--
-- Change Log History
-- mm/dd/yy  Name      Release  Description
-- 09/12/14  m.brown   5.3      Fixed to use WorkBreakdownStructureId
-- 10/09/14  j.borgers 5.3      Modified to work wit schedule data structure changes
-- =============================================================
CREATE FUNCTION [G2].[fnActiveNuclearProjectTaskExists]
( 
	@TaskWorkBreakdownStructureId int
) 
RETURNS bit AS
BEGIN
	
	DECLARE @returnVal bit = 0;

	IF EXISTS(
		SELECT 1
		FROM G2.WorkBreakdownStructure wbsTask
		INNER JOIN G2.WorkBreakdownStructure wbs ON wbsTask.ParentId = wbs.Id
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wbswtx ON wbs.Id = wbswtx.WorkBreakdownStructureId
		INNER JOIN G2.WorkType wt ON wbswtx.WorkTypeId = wt.Id
		INNER JOIN G2.WorkTypeCategory wtc ON wt.WorkTypeCategoryId = wtc.Id
		INNER JOIN G2.vwWorkBreakdownStructureStatusActiveStates awbs WITH (NOEXPAND) ON awbs.WorkBreakdownStructureStatusId = wbsTask.StatusId
		WHERE wbsTask.Id = @TaskWorkBreakdownStructureId
		AND wbsTask.CompletedInd = 0
		AND wtc.Name = 'Nuclear'
		HAVING COUNT(*) > 1
	)
	BEGIN
		SET @returnVal = 1;
	END
 

    RETURN @returnVal;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA21GMSSubSystemId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/10/2015
-- Release:     6.1
-- Description: Retrieves the NA-21 GMS sub-system id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA21GMSSubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 5; -- GMS � Radiological Security	 / NA-21
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetIndirectAndOtherDirectCostChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/19/2015 
-- Release:     6.4
-- Description: Returns the Indirect and Other Direct Cost ChangeRequestTypeGroup Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetIndirectAndOtherDirectCostChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
  RETURN 14; --Indirect and Other Direct Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPreviousPeriodId]'
GO

-- =============================================================
-- Author:     Unknown
-- Created:    ?/??/???
-- Description: 
--
-- Usage:     @IncludeReconciliationPeriod
--	1 if you want to include the 13th Period when determining the next Fiscal Period
--  0 if you DONT want to include the 13th Period when determining the next Fiscal Period
--
-- mm/dd/yy		Name     StoryId  Description
-- 05/02/2012	RLA		 3.2	   Made the function Period 13 aware ...
-- =============================================================
CREATE FUNCTION [G2].[fnGetPreviousPeriodId]
(
	@CurrentPeriodId INT, 
	@IncludeReconciliationPeriod BIT
)
RETURNS
INT
AS
BEGIN
	DECLARE @id int, @beginDt Datetime, @EndDt Datetime, @FiscalPeriod INT
	
	SELECT @beginDt = BeginDt, @EndDt = EndDt, @FiscalPeriod = FiscalPeriod
	  FROM G2.Period
	 WHERE id = @CurrentPeriodId

    IF @FiscalPeriod = 1 AND @IncludeReconciliationPeriod = 1  SET @beginDt = DATEADD(hh,-1,@beginDt)
    ELSE IF @FiscalPeriod = 1 SET @beginDt = DATEADD(m,-1,@beginDt)
         ELSE IF @FiscalPeriod = 13 SET @beginDt = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@beginDt)-1),@beginDt),101)
                  ELSE SET @beginDt =DATEADD(m,-1,@beginDt)

	SELECT @id =  Id
	FROM Period 
	WHERE BeginDt = @beginDt

	--SELECT @id = PreviousPeriod.Id
	--FROM Period PreviousPeriod
	--INNER JOIN G2.Period CurrentPeriod ON PreviousPeriod.BeginDt = DATEADD(m,-1,CurrentPeriod.BeginDt)
	--WHERE CurrentPeriod.Id = @CurrentPeriodId
	
	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDefaultCostTransactionTypeIdBySubSystemId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     04/19/2017 
-- Release:     8.0
-- Description: Returns the G2Parameter Id for the default cost transaction type to be used in STARS contract mappings
--
-- mm/dd/yy  Name      Release  Description
-- 05/02/17  j.borgers 8.0      Modified to use the correct table
-- =============================================================
CREATE FUNCTION [G2].[fnGetDefaultCostTransactionTypeIdBySubSystemId] (@SubSystemId INT)
RETURNS INT
AS
BEGIN

	DECLARE @returnId INT;

	SELECT @returnId = CAST(gp.Value AS INT)
	FROM G2.G2Parameters gp 
	WHERE gp.ParmKey = 'Default Cost Transaction Type Id' 
	AND gp.SubSystemId = @SubSystemId;

  RETURN @returnId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDefaultCommitmentTransactionTypeIdBySubSystemId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     04/19/2017 
-- Release:     8.0
-- Description: Returns the G2Parameter Id for the default commitment transaction type to be used in STARS contract mappings
--
-- mm/dd/yy  Name      Release  Description
-- 05/02/17  j.borgers 8.0      Fixed defect where wrong table was being used
-- =============================================================
CREATE FUNCTION [G2].[fnGetDefaultCommitmentTransactionTypeIdBySubSystemId] (@SubSystemId INT)
RETURNS INT
AS
BEGIN

	DECLARE @returnId INT;

	SELECT @returnId = CAST(gp.Value AS INT)
	FROM G2.G2Parameters gp 
	WHERE gp.ParmKey = 'Default Commitment Transaction Type Id' 
	AND gp.SubSystemId = @SubSystemId;

  RETURN @returnId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetVoidedChangeRequestDetailStatusId]'
GO
-- =============================================================
-- Author:      Chris O'Neal
-- Created:     4/27/2013
-- Release:     4.1
-- Description: Retrieve the Voided change request detail
--   status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetVoidedChangeRequestDetailStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- Voided
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInProcessChangeRequestDetailStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/27/2013
-- Release:     4.1
-- Description: Retrieves the In Process change request detail
--   status.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInProcessChangeRequestDetailStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- In Process
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFinalizedChangeRequestDetailStatusId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/27/2013
-- Release:     4.1
-- Description: Retrieve the Finalized change request detail
--   status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFinalizedChangeRequestDetailStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 12; -- Finalized
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAnnualPlanningFiscalYear]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     7/31/2014
-- Release:     
-- Description:  Get the Annual Planning Fiscal Year configured parameter for given SubSystem
--
-- mm/dd/yy  Name     Release  Description
-- 01/29/15  g.ogle   5.4      Added SubSystemId parameter
-- =============================================================
CREATE FUNCTION [G2].[fnGetAnnualPlanningFiscalYear](@SubSystemId INT)
RETURNS INT
AS
BEGIN
	DECLARE @FiscalYear int;

	SELECT @FiscalYear = CONVERT(int, Value)
	FROM G2.G2Parameters
	WHERE ParmKey = 'Annual Planning Fiscal Year'
    AND SubSystemId = @SubSystemId;

	RETURN @FiscalYear;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnTaskDeviceCheck]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     09/30/2015 
-- Release:     6.3
-- Description: Verifies that a source isn't used more than once in a given subsystem
--
-- mm/dd/yy  Name      Release  Description
-- 04/28/16  j.borgers 7.0      Modified so only an active or open task or task change is considered
-- =============================================================
CREATE FUNCTION  [G2].[fnTaskDeviceCheck](
	@SourceId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @invalidInd BIT = 0
	
	SELECT @invalidInd = 1
	FROM G2.TaskDevice td
	INNER JOIN G2.Task t ON t.Id = td.TaskId
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = t.WorkBreakdownStructureId
	INNER JOIN G2.vwWorkBreakdownStructureStatusActiveAndOpenRequestStates aowbs WITH (NOEXPAND) ON aowbs.WorkBreakdownStructureStatusId = wbs.StatusId
	WHERE td.SourceId = @SourceId
	GROUP BY wbs.SubSystemId, td.SourceId
	HAVING COUNT(*) > 1;
	
	SELECT @invalidInd = 1
	FROM Baseline.TaskChange tc
	INNER JOIN G2.ChangeRequestDetail crd ON tc.ChangeRequestDetailId = crd.Id
	INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
	WHERE tc.SourceId = @SourceId
	GROUP BY cr.SubSystemId, tc.SourceId
	HAVING COUNT(*) > 1;

	RETURN @invalidInd;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsNonMultiSelectWorkBreakdownStructureAttributeTypeUnique]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     1/15/2015
-- Release:     5.4
-- Description: Checks to make sure the MultiSelectInd is set to true for a specified WorkBreakdownStructureAttribute
--              that does not have a unique (WorkBreakdownStructureId,WorkBreakdownStructureAttributeTypeId) tuple.
--              Returns 0 if MultiSelectInd is set, 1 if not.
--
-- mm/dd/yy  Name     Release  Description
-- 03/30/17  N.Harris 8.0      Added WorkBreakdownStructureAttributeTypeId to parameters
-- =============================================================
CREATE FUNCTION [G2].[fnIsNonMultiSelectWorkBreakdownStructureAttributeTypeUnique]
(
	@WorkBreakdownStructureAttributeId INT,
	@WorkBreakdownStructureAttributeTypeId INT
)
RETURNS BIT
AS
BEGIN

	DECLARE @returnValue BIT = 1;

	IF EXISTS(
		SELECT 1
		FROM G2.WorkBreakdownStructureAttribute wbsa
		INNER JOIN G2.WorkBreakdownStructureAttributeType wbsat ON wbsat.Id = wbsa.WorkBreakdownStructureAttributeTypeId
		INNER JOIN ( 
			SELECT * FROM G2.WorkBreakdownStructureAttribute wbsa
			WHERE wbsa.Id = @WorkBreakdownStructureAttributeId
		) match ON match.WorkBreakdownStructureId = wbsa.WorkBreakdownStructureId AND match.WorkBreakdownStructureAttributeTypeId = wbsa.WorkBreakdownStructureAttributeTypeId
		WHERE wbsat.MultiSelectInd = 0
		GROUP BY wbsa.WorkBreakdownStructureId, wbsa.WorkBreakdownStructureAttributeTypeId
		HAVING COUNT(*) > 1
	)
	BEGIN
		SET @returnValue = 0;
	END

	RETURN @returnValue;

END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNotificationServicesEnabled]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     6/12/2017
-- Release:     
-- Description: Returns the current status of notification services
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNotificationServicesEnabled] ()
RETURNS
INT
AS
BEGIN
	DECLARE @enabledInd BIT;

	SELECT @enabledInd = CAST(gp.Value AS BIT)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Notification Services Enabled'

	RETURN @enabledInd;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSystemUser]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/17/2010
-- Description: This function returns the id of the system user.
--
-- mm/dd/yy  Name     Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSystemUser]
(
)
RETURNS int
AS
BEGIN
	DECLARE @userId int;

	SELECT @userId = CAST(value AS int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'System User';

	RETURN @userId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentCostThruPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/12/2014
-- Release:     5.0
-- Description: Retrieves the Current Cost Thru Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentCostThruPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Cost Thru Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentCostPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/12/2014
-- Release:     5.0
-- Description: Retrieves the Current Cost Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentCostPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Cost Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCostPeriodAdvancementDayParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/12/2014
-- Release:     5.0
-- Description: Retrieves the Cost Period Advancement day parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCostPeriodAdvancementDayParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Cost Period Advancement Day';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNSATUser]'
GO
-- =============================================================
-- Author:      Neavda Williford
-- Created:     1/21/2014
-- Description: This function returns the id of the NSAT user.
--
-- mm/dd/yy  Name     Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNSATUser]
(
)
RETURNS int
AS
BEGIN
	DECLARE @userId int;

	SELECT @userId = CAST(value AS int)
	FROM G2.G2Parameters
	WHERE ParmKey = 'NSAT User';

	RETURN @userId
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetGeneralUserRoleId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/3/2013
-- Release:     4.4
-- Description: Retrieves the id of General User.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetGeneralUserRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- General User
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAIMSUser]'
GO

-- =============================================================
-- Author:      Byron Roland
-- Created:     7/17/15
-- Release:     6.2
-- Description: Returns the AIMS User Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAIMSUser]
(
)
RETURNS INT
AS
BEGIN
	DECLARE @userId INT;

	SELECT @userId = CAST(value AS INT)
	FROM G2.G2Parameters
	WHERE ParmKey = 'AIMS User';

	RETURN @userId
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnStringParseList]'
GO
-- =============================================================
-- Author:      Unknown
-- Created:     Unknown
-- Release:     
-- Description: Parses delimited string into Table
--
-- mm/dd/yy  Name     Release  Description
-- 06/02/16  g.ogle   7.1      expanding capacity from 500-1000 for @List and 10-50 for the values to support sets with larger text values
-- 06/13/16  g.ogle   7.1      Change cast which was truncating the values
-- =============================================================
CREATE FUNCTION [G2].[fnStringParseList]
(
	@List varchar(1000),
	@Delim varchar(1)
)
RETURNS 
@ParsedList table
(
	Value varchar(50)
)
AS
BEGIN
	DECLARE @ListValue varchar(50), @Pos int

	SET @List = LTRIM(RTRIM(@List))+ @Delim
	SET @Pos = CHARINDEX(@Delim, @List, 1)

	IF REPLACE(@List, @Delim, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ListValue = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
			IF @ListValue <> ''
			BEGIN
				INSERT INTO @ParsedList (Value) 
				VALUES (CAST(@ListValue AS varchar(50))) --Use Appropriate conversion
			END
			SET @List = RIGHT(@List, LEN(@List) - @Pos)
			SET @Pos = CHARINDEX(@Delim, @List, 1)

		END
	END	
	RETURN
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentFiscalYear]'
GO
-- =============================================================
-- Author:      Unknown
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 01/09/14  j.borgers 5.0      Added SubSystemId
-- =============================================================

CREATE FUNCTION [G2].[fnGetCurrentFiscalYear]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @fy int;

	SELECT @fy = cast (value as int)
	FROM G2Parameters
	WHERE ParmKey = 'Current FY'
	AND SubSystemId = @SubSystemId;

	RETURN @fy;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetScheduleConfigurationTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Get the Configuration Type ID for Schedule
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetScheduleConfigurationTypeId]()
RETURNS INT
AS
BEGIN
    RETURN 4;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentFiscalYearParmKey]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     3/15/2016
-- Release:     7.0
-- Description: Returns current fy parm key
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentFiscalYearParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current FY';
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkingDays]'
GO

-- =============================================
-- Author:		Diedre Cantrell (per Article)
-- Create date: 4/9/2010
-- Description:	Returns the number of working days based 
--     on the Start and End dates passed in
-- =============================================
CREATE FUNCTION [G2].[fnGetWorkingDays] 
      ( 
         @StartDate DATETIME, 
         @EndDate   DATETIME = NULL --@EndDate replaced by @StartDate when DEFAULTed 
        ) 
 
RETURNS INT 
 
AS 

  BEGIN 

        DECLARE @Swap DATETIME 

        IF @StartDate IS NULL 
        RETURN NULL 
 
        
         IF @EndDate IS NULL 
            SELECT @EndDate = @StartDate 
 
        /* Strip the time element from both dates (just to be safe) by converting 
              to whole days and back to a date.  Usually faster than CONVERT. 
              0 is a date (01/01/1900 00:00:00.000) */
         SELECT @StartDate = DATEADD(dd,DATEDIFF(dd,0,@StartDate),0), 
           @EndDate   = DATEADD(dd,DATEDIFF(dd,0,@EndDate)  ,0) 
 
        /* If the inputs are in the wrong order, reverse them */
             IF @StartDate > @EndDate 
                SELECT @Swap      = @EndDate, 
                       @EndDate   = @StartDate, 
                       @StartDate = @Swap 
 
       /* Calculate and return the number of workdays */

         RETURN ( 
                SELECT 
              --Start with total number of days including weekends 
                (DATEDIFF(dd,@StartDate,@EndDate)+1) 
 
              --Subtact 2 days for each full weekend 
               -(DATEDIFF(wk,@StartDate,@EndDate)*2) 
 
              --If StartDate is a Sunday, Subtract 1 
               -(CASE WHEN DATENAME(dw,@StartDate) = 'Sunday' 
                      THEN 1 
                      ELSE 0 
                  END) 
 
              --If EndDate is a Saturday, Subtract 1 
               -(CASE WHEN DATENAME(dw,@EndDate) = 'Saturday' 
                      THEN 1 
                      ELSE 0 
                  END) 
                ) 
    END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPeriod]'
GO
-- =============================================================
-- Author:      Unknown
-- Created:     
-- Description: Gets the current period
--
-- mm/dd/yy  Name     Release  Description
-- 05/04/12  RLA		3.2      Modified to exclude Period 13 and returm Period 12
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPeriod]()
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = CASE WHEN p.reconciliationind = 1 THEN g2.fnGetPreviousPeriodId(id,0) else P.Id END 	
	FROM Period P
	WHERE CURRENT_TIMESTAMP BETWEEN P.BeginDt AND P.EndDt
	
	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnDayEnd]'
GO
-- =============================================
-- Author:		Chris Luttrell
-- Create date: 8/23/06
-- Description:	Returns the last second of the day based on the date passed in
-- =============================================
CREATE FUNCTION [G2].[fnDayEnd] 
(

	@theDay datetime = null
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here

	set @theDay = DATEADD(ms,-3,DATEADD(d, DATEDIFF(d,0,@theDay), 1))


	RETURN @theDay

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnDayBegin]'
GO
-- =============================================
-- Author:		Chris Luttrell
-- Create date: 8/23/06
-- Description:	Returns the first second of the day based on the date passed in
-- =============================================
CREATE FUNCTION [G2].[fnDayBegin] 
(
	@theDay datetime = null
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
		set @theDay = DATEADD(d, DATEDIFF(d,0,@theDay), 0)
	RETURN @theDay
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProcessedChangeRequestDetailStatusId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     4/26/13
-- Release:     4.1
-- Description: Retrieves the Processed change request detail status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProcessedChangeRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 6;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnMultiValue_Param]'
GO

CREATE FUNCTION [G2].[fnMultiValue_Param]
   (@RepParam nvarchar(4000), @Delim char(1)= ',')
RETURNS @Values TABLE (Param nvarchar(4000))AS
  BEGIN
  DECLARE @chrind INT
  DECLARE @Piece nvarchar(10)
  SELECT @chrind = 1 
  WHILE @chrind > 0
    BEGIN
      SELECT @chrind = CHARINDEX(@Delim,@RepParam)
      IF @chrind  > 0
        SELECT @Piece = LEFT(@RepParam,@chrind - 1)
      ELSE
        SELECT @Piece = @RepParam
      INSERT  @Values(Param) VALUES(Cast(@Piece AS INT))
      SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)
      IF LEN(@RepParam) = 0 BREAK
    END
  RETURN
  END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetImportPermissionId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/16/2014 
-- Release:     5.3
-- Description: Returns the permission Id for import
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetImportPermissionId]()
RETURNS int
AS
BEGIN
  RETURN 59; --import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDeletedWorkBreakdownStructureStatusId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     9/24/2014
-- Release:     5.3
-- Description: Returns ID for Deleted WorkBreakdownStruct Status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDeletedWorkBreakdownStructureStatusId]()
RETURNS INT
AS
BEGIN
  RETURN 6;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCancelledWorkBreakdownStructureStatusId]'
GO


-- =============================================================
-- Author:      Chris O'Neal
-- Created:     10/02/2014
-- Release:     5.3
-- Description: Retrieves the WorkBreakdownStructure canceled status ID.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCancelledWorkBreakdownStructureStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- Cancelled
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetActiveWorkBreakdownStructureStatusId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/05/2014
-- Release:     5.0
-- Description: Retrieves the Active workbreakdownstructure
--   status id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetActiveWorkBreakdownStructureStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Active
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnCheckWorkBreakdownStructureChangeWithCapabilityHasNoAssetId]'
GO

-- =============================================================
-- Author:      Jill Rochat
-- Created:     6/2/2016
-- Release:     7.1
-- Description: Enforces that there is no asset when there is a
--				capability
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnCheckWorkBreakdownStructureChangeWithCapabilityHasNoAssetId]
(
	@WorkBreakdownStructureChangeAssetId INT
)
RETURNS INT
AS 
BEGIN
	DECLARE	@valid INT = 1;

	SELECT @valid = 0
	FROM G2.WorkBreakdownStructureChangeAsset wbsca
	WHERE wbsca.Id = @WorkBreakdownStructureChangeAssetId
	AND wbsca.AssetId IS NOT NULL;

	RETURN @valid;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA21SubSystemId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     01/23/2014
-- Release:     5.0
-- Description: Retrieves the NA-21 sub-system id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA21SubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Global Threat Reduction Initiative / NA-21
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubSystemIdFromWorkBreakdownStructure]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/05/2014
-- Release:     5.0
-- Description: Retrieves the sub-system id for the specified
--   workbreakdownstructure.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSubSystemIdFromWorkBreakdownStructure]
(
	@WorkBreakdownStructureId int
)
RETURNS int
AS
BEGIN
	DECLARE @subSystemId int;
	SELECT @subSystemid = wbs.SubSystemId FROM G2.WorkBreakdownStructure wbs WHERE wbs.Id = @WorkBreakdownStructureId;
	RETURN @subSystemId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnG2Parameter]'
GO

-- =============================================
-- Author:		Chris Luttrell
-- Create date: 6/27/2007
-- Description:	returns a G2 Parameter based on Key
--
-- mm/dd/yy  Name      Release  Description
-- 11/3/14   a.smith   5.3      Modified to be multi-system capable.
-- 09/03/15  DNC       6.3      Format and remove default SubSystemId
-- =============================================
CREATE FUNCTION [G2].[fnG2Parameter]
      (
       @Key VARCHAR(50)
      ,@SubSystemId INT
      )
RETURNS SQL_VARIANT
AS
    BEGIN
    /* Begin Debug Declarations */
	--DECLARE @SubSystemId INT = 3, --NULL,
	--		@Key VARCHAR(50) = 'Current Cost Period';
    /* End Debug Declarations */

	-- Declare the return variable here
        DECLARE @result SQL_VARIANT;
    
	-- Add the T-SQL statements to compute the return value here
        SELECT @result = g2p.Value
        FROM G2.G2Parameters g2p
        WHERE g2p.SubSystemId = @SubSystemId
                AND g2p.ParmKey = @Key;

    /* Begin Debug Statements */
	--SELECT @result G2Parmeter;
    /* END Debug Statements */

	/* Return the result of the function */
        RETURN @result;

    END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetIndirectAndOtherDirectCostChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     11/19/2015 
-- Release:     6.4
-- Description: Returns the Indirect and Other Direct Cost ChangeRequestType Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetIndirectAndOtherDirectCostChangeRequestTypeId]()
RETURNS int
AS
BEGIN
  RETURN 31; --Indirect and Other Direct Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubProgramSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     09/15/2016
-- Release:     7.3
-- Description: Return SubSystemLevelId for Sub-Program
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSubProgramSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT sslv.Id
			FROM G2.SubSystemLevel sslv
			WHERE (sslv.Label = 'Sub Program' OR sslv.Label = 'Sub-Program')	-- NA21 does not have the hyphen, all other subsystems do.
			AND sslv.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubOfficeSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     09/15/2016
-- Release:     7.3
-- Description: Return SubSystemLevelId for Sub-Office
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSubOfficeSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT sslv.Id
			FROM G2.SubSystemLevel sslv
			WHERE (sslv.Label = 'Sub Office' OR sslv.Label = 'Sub-Office')
			AND sslv.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProgramSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/17/2016
-- Release:     7.3
-- Description: Return SubSystemLevelId for Program
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProgramSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Program'
			AND ssl.SubSystemId = @SubSystemId);

END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPortfolioSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     12/03/2014
-- Release:     5.3
-- Description: Return SubSystemLevelId for Portfolio
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPortfolioSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Portfolio'
			AND ssl.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOfficeSubSystemLevelId]'
GO

-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     12/08/2014
-- Release:     5.3
-- Description: Return SubSystemLevelId for Office
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetOfficeSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Office'
			AND ssl.SubSystemId = @SubSystemId);

END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSPA2017CriteriaStartDateParmKey]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/5/2017
-- Release:     8.3
-- Description: Get the ParmKey for SPA 2017 Criteria Start Date
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSPA2017CriteriaStartDateParmKey] ()
RETURNS NVARCHAR(100)
AS
BEGIN
    RETURN 'SPA 2017 Criteria Start Date';
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPresidentsFourYearPlanWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     9/29/2014
-- Release:     5.3
-- Description: Returns PlanWorkBreakdownStructureAttributeType Id for President's 4-Year Plan
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPresidentsFourYearPlanWorkBreakdownStructureAttributeTypeId]()
RETURNS INT
AS
BEGIN
    RETURN 17;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPeriodBegin]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/15/2013
-- Release:     4.1
-- Description: Retrieves the begin date for the specified
--   period.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPeriodBegin] (
	@PeriodId int
)
RETURNS datetime
AS
BEGIN
	DECLARE @dt datetime;
	SELECT @dt = BeginDt FROM G2.Period prd WHERE prd.Id = @PeriodId;
	RETURN @dt;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNotProvidingChangeRequestDetailStatusId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     11/29/2016 
-- Release:     7.4
-- Description: Returns the Not Providing ChangeRequestDetailStatusId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNotProvidingChangeRequestDetailStatusId] ()
RETURNS INT
AS
BEGIN
  RETURN 13; -- Not Providing ChangeRequestDetailStatusId
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCuriesMetricId]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/06/2014
-- Release:     5.3
-- Description: Get the MetricId for the Curies metric
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetCuriesMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Curies'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSecondaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/14/2014
-- Release:     5.3
-- Description: Retrieves the key for the Secondary Group
--   WorkBreakdownStructure Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSecondaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]()
RETURNS NVARCHAR(100)
AS 
BEGIN
	RETURN 'Secondary Group WorkBreakdownStructure Label';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRealtimeStatusChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/13/2014 
-- Release:     5.0
-- Description: Returns the Realtime Status change request type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRealtimeStatusChangeRequestTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 15; --Realtime Status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPrimaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/14/2014
-- Release:     5.3
-- Description: Retrieves the key for the Primary Group
--   WorkBreakdownStructure Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPrimaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]()
RETURNS NVARCHAR(100)
AS 
BEGIN
	RETURN 'Primary Group WorkBreakdownStructure Label';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetViewPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     5/20/2013
-- Release:     4.2
-- Description: Retrieves the View permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetViewPermissionId] ()
RETURNS
int
AS
BEGIN
	RETURN 28; -- View
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPurviewPermissionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/25/2013
-- Release:     4.4
-- Description: Retrieves the Purview permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPurviewPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 58; -- Purview
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationOverTargetChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2015
-- Release:     6.4
-- Description: Retrieves the Budget Formulation Over Target
--   Change Request Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationOverTargetChangeRequestTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 34; -- Budget Formulation Over Target
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetBudgetFormulationFederalFundingPermissionObjectId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     12/05/2016 
-- Release:     7.4
-- Description: Returns the Budget Formulation Federal Funding Permission Object Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetBudgetFormulationFederalFundingPermissionObjectId]()
RETURNS int
AS
BEGIN
  RETURN 185; --Budget Formulation Federal Funding
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetBudgetFormulationCommentPermissionObjectId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     12/06/2016 
-- Release:     7.4
-- Description: Returns the Budget Formulation Comment Permission Object Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetBudgetFormulationCommentPermissionObjectId]()
RETURNS int
AS
BEGIN
  RETURN 184; --Budget Formulation Comment
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2015
-- Release:     6.4
-- Description: Retrieves the Budget Formulation Change Request
--   Type Group Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationChangeRequestTypeGroupId] ()
RETURNS INT
AS
BEGIN
	RETURN 15; -- Budget Formulation
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSSubProgramManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the SS: Sub Program Manager (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSSubProgramManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 156; -- SS: Sub Program Manager (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSSubProgramManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the SS: Sub Program Manager (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSSubProgramManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 155; -- SS: Sub Program Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSPortfolioManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/1/2015
-- Release:     6.3
-- Description: Retrieves the SS: Portfolio Manager (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSPortfolioManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 158; -- SS: Portfolio Manager (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSPortfolioManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/1/2015
-- Release:     6.3
-- Description: Retrieves the SS: Portfolio Manager (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSPortfolioManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 157; -- SS: Portfolio Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSOfficeDirectorSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the SS: Office Director (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSOfficeDirectorSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 153; -- SS: Office Director (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSOfficeDirectorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the SS: Office Director (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSOfficeDirectorPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 152; -- SS: Office Director (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSHQFinancialOfficerSecondaryRoleId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     10/09/2015
-- Release:     6.3
-- Description: Retrieves the SS: HQ Financial Officer (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSHQFinancialOfficerSecondaryRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 151; -- SS: HQ Financial Officer (Secondary)
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSHQFinancialOfficerPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     10/09/2015
-- Release:     6.3
-- Description: Retrieves the SS: HQ Financial Officer (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSHQFinancialOfficerPrimaryRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 150; -- SS: HQ Financial Officer (Primary)
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSSAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the SS: Administrator role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSSAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 154; -- SS: Administrator
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProgramAdministratorRoleId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/6/2013
-- Release:     4.4
-- Description: Retrieves the Program Administrator role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProgramAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Program Administrator
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPerformancePeriod]'
GO
/****** Object:  UserDefinedFunction [G2].[fnGetCurrentPerformancePeriod]    Script Date: 5/30/2014 3:49:48 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
-- =============================================================
-- Author:      
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 06/04/14  j.borgers 5.2      Added SubSystemId
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPerformancePeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2Parameters gp
	WHERE gp.ParmKey = 'Current Performance Period'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSubmitPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/4/2013
-- Release:     4.2
-- Description: Retrieves the submit permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSubmitPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 27; -- Submit
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetShowInInboxPermissionId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the Show in Inbox PermissionId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetShowInInboxPermissionId]()
RETURNS
int
AS
BEGIN
	RETURN 42;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProcessPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/3/2013
-- Release:     4.2
-- Description: Retrieves the process permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProcessPermissionId]()
RETURNS int
AS
BEGIN
	RETURN 23; -- Process
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNonResponseChangeRequestDetailConcurrenceStatusId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/2013
-- Release:     4.4
-- Description: Retrieves the Non-Response change request detail
--   concurrence status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNonResponseChangeRequestDetailConcurrenceStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Non-Response
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetConcurPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/3/2013
-- Release:     4.2
-- Description: Retrieves the concur permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetConcurPermissionId]()
RETURNS int
AS
BEGIN
	RETURN 15; -- Concur
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingConfigurationTypeId]'
GO



-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/03/2014 
-- Release:     5.0
-- Description: Returns the Id for the Funding ConfigurationType
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Funding
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingAdjustmentChangeRequestTypeGroupId]'
GO



-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the FundingAdjustment ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingAdjustmentChangeRequestTypeGroupId]()
RETURNS
int
AS
BEGIN
	RETURN 7;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceMinimumAllowedJouleCompletionDate]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     
-- Release:     3.1
-- Description: This function returns the minimum allowed joule
--   completion date.
--
-- mm/dd/yy  Name     Release  Description
-- 10/08/14  s.oneal  5.3      Added SubSystemId as a parameter for multi sub-system schedule support
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceMinimumAllowedJouleCompletionDate]
(
	@SubSystemId int
)
RETURNS DATETIME
AS
BEGIN
	DECLARE @value DATETIME;

	SELECT @value = CONVERT(datetime, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Performance Minimum Allowed Joule Completion Date'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @value;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFiscalYearFromDate]'
GO

CREATE FUNCTION [G2].[fnGetFiscalYearFromDate]
(
	@DateIn datetime = null
)
RETURNS
int
AS
BEGIN
	--DECLARE @FY int;

	if @DateIn is null set @DateIn = CURRENT_TIMESTAMP

	return
	(SELECT FiscalYear
	FROM FiscalYear
	WHERE @DateIn between FYBeginDt and FYEndDt)
	
	--RETURN @FY
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentBaselinePeriod]'
GO
--/****** Object:  UserDefinedFunction [G2].[fnGetCurrentBaselinePeriod]    Script Date: 5/30/2014 3:42:34 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

-- =============================================================
-- Author:      
-- Created:      
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 06/04/14  j.borgers 5.2      Added SubSystemId 
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentBaselinePeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;
	
	SELECT @id = cast (value as int)
	FROM G2Parameters gp
	WHERE gp.ParmKey = 'Current Baseline Period'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubmittedChangeRequestDetailStatusId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/2013
-- Release:     4.4
-- Description: Retrieves the Submitted change request detail
--   status.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSubmittedChangeRequestDetailStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 7; -- Submitted
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProposalChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/13/2018 
-- Release:     8.5
-- Description: Returns the Id for the Proposal ChangeRequestTypeGroup
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProposalChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
  RETURN 20; --Proposal
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAssetManagementChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     08/08/2017 
-- Release:     8.2
-- Description: Returns the Asset Management Change Request Type Group Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAssetManagementChangeRequestTypeGroupId]()
RETURNS INT
AS
BEGIN
  RETURN 19; --Asset Management
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAdvanceChangeRequestActionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/18/2014
-- Release:     5.0
-- Description: Retrieves the Advance change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAdvanceChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- Advance
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConcurChangeRequestDetailConcurrenceStatusId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/2013
-- Release:     4.4
-- Description: Retrieves the Concur change request detail
--   concurrence status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetConcurChangeRequestDetailConcurrenceStatusId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Concur
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnAdvanceWorkflowLeafRolePerm]'
GO
-- =============================================================
-- Author:      Robin Auerbach?
-- Created:     12/09/2011?
-- Release:     ?
-- Description: Checks that a child record has a PermissionId and RoleId filled in
--
-- mm/dd/yy  Name      Release  Description
-- 03/30/17  j.borgers 8.0      Reformatted to bring up to standards and added header block
-- =============================================================
CREATE FUNCTION [G2].[fnAdvanceWorkflowLeafRolePerm] 
(
	@ParentId INT
) 
RETURNS BIT 
AS
BEGIN
	DECLARE @valid BIT = CAST(1 AS BIT);
         
	SELECT @valid = CAST(0 AS BIT)
	FROM G2.AdvanceWorkflow awf
	WHERE awf.Id = @ParentId
	AND ((PermissionId IS NOT NULL) OR (RoleId IS NOT NULL))
	AND @ParentId IS NOT NULL;
	
	RETURN @valid;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCreateChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Create change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCreateChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Create
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetReadytoImplementChangeRequestTypeGroupWorkflowLevel]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/14/2014
-- Release:     5.3
-- Description: Retrieves the Ready to Implement Change Request
--              Type Group Workflow Level Id for the given parameters.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetReadytoImplementChangeRequestTypeGroupWorkflowLevel]
( @SubSystemId INT,
  @ChangeRequestTypeGroupId INT
)
RETURNS int
AS
BEGIN

	DECLARE @id INT, @processPermissionId INT = Security.fnGetProcessPermissionId();

	SELECT DISTINCT @id = permR.WorkflowLevel
	FROM Security.PermissionRole permR
	INNER JOIN G2.ChangeRequestType crt ON crt.Id = permR.ChangeRequestTypeId
	INNER JOIN G2.ChangeRequestTypeGroup crtg ON crtg.Id = crt.ChangeRequestTypeGroupId
	WHERE PermissionId = @processPermissionId
	AND crt.ChangeRequestTypeGroupId = @ChangeRequestTypeGroupId
	AND permR.SubSystemId = @SubSystemId 
	
	RETURN @id;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceReportProjectCutOffDate]'
GO

-- =============================================
-- Author:		Sara O'Neal
-- Create date: 4/15/2008
-- Description:	Returns the cut-off date used to determine which
--				projects should be displayed on review grid so that user's can status.
--              The previous logic showed projects with scope starting prior to FY or
--              within the next six months when in Q3. Now we want to show all projects.
-- mm/dd/yy  Name     Release  Description
-- 05/04/12  RLA		3.2      Modified to exclude Period 13 
-- 11/28/12  c.oneal    3.7      Released as prod support prior to official 3.7 release; 
--                                ignore previous rules and return max period in G2
-- 10/22/14  j.borgers  5.3      Removed unnecessary comments
-- =============================================
CREATE FUNCTION [G2].[fnGetPerformanceReportProjectCutOffDate] 
(
	@PeriodId int
)
RETURNS datetime
AS
BEGIN	
	RETURN (SELECT MAX(EndDt) FROM G2.Period p WHERE ReconciliationInd=0)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPerformerCostDataPermissionObjectId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     5/20/2013
-- Release:     4.2
-- Description: Retrieves the Performer Cost Data permission
--   object id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPerformerCostDataPermissionObjectId] ()
RETURNS int
AS
BEGIN
	RETURN 85; -- Performer Cost Data
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCostsAdminRoleId]'
GO


-- =============================================================
-- Author:      Mark Brown
-- Created:     1/19/2016
-- Release:     6.5 Prod Support
-- Description: Returns the cost admin role
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCostsAdminRoleId] ()
RETURNS INT
AS
BEGIN

	RETURN 97;

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnRptMaxOutYearwithFunding]'
GO




-- =============================================
-- Author:		Diedre Cantrell 
-- Create date: 3/1/11
-- Release:  2.1 Betazed
-- Description:	Return Max OutYear with Funding from LifecycleBudget
--				Used in GTRI Planning reports.
-- =============================================
CREATE FUNCTION [G2].[fnRptMaxOutYearwithFunding] ()
RETURNS INT
AS 
    BEGIN 

        DECLARE @MaxOutYearwithFunding INT ;
   
        
        SET @MaxOutYearwithFunding = (
                                      SELECT MAX(lb.FiscalYear) MaxOutYear
                                        FROM Financial.LifecycleBudget lb
                                        WHERE lb.Amount > 0.00
                                     )
 
     
        RETURN @MaxOutYearwithFunding ;
    END 



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCreatePermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/4/2013
-- Release:     4.2
-- Description: Retrieves the create permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCreatePermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 17; -- Create
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProgramWBSLevelId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/6/2013
-- Release:     4.4
-- Description: Retrieves the Program wbs level id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProgramWBSLevelId] ()
RETURNS int
AS
BEGIN
	RETURN 1; -- Program
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInfrastructurePlanningCurrentBudgetYear]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/24/2016 
-- Release:     7.1
-- Description: Returns the current Budget Year given a SubSystem
--
-- mm/dd/yy  Name      Release  Description
-- 09/16/16  j.borgers 7.3      Removed OpenDt and CloseDt from PlanningConfiguration
-- =============================================================
CREATE FUNCTION  [G2].[fnGetInfrastructurePlanningCurrentBudgetYear]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	DECLARE @year INT;

	SELECT @year = pc.BudgetYear 
	FROM G2.PlanningConfiguration pc 
	WHERE pc.SubSystemId = @SubSystemId;

  RETURN @year; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProcessChangeRequestActionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     10/22/2013
-- Release:     4.4.1
-- Description: Retrieves the Process change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProcessChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- Process
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTemplateConversionChangeRequestActionId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/02/2014
-- Release:     5.3
-- Description: Retrieves the Template Conversion change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTemplateConversionChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 27; -- Void
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnOffsetBusinessDays]'
GO

-- =============================================================
-- Author:      
-- Created:     
-- Release:     
-- Description: Takes in a starting date and an offset and calculates
-- the end date based on the fact that every offset date should be a 
-- business day
--
-- mm/dd/yy  Name     Release  Description
-- 03/06/12  m.brown  3.1      Modified to handle the case where start date
--                             from the bad, bad user (i.e. Sara) is a weekend
-- =============================================================
CREATE FUNCTION [G2].[fnOffsetBusinessDays]
(
	 @StartDate DATETIME
	,@businessDays INT
)
RETURNS DATETIME
AS 
BEGIN 
	DECLARE @EndDate DATETIME
		  , @AddSub INT
		  , @NumWeeks INT
		
	IF @businessDays >= 0
		SET @AddSub = 1
	ELSE
		SET @AddSub = -1 
		  
	WHILE (CASE DATEPART(WEEKDAY, @StartDate)
				WHEN 1 THEN 1
				WHEN 2 THEN 1
				ELSE 0
			END = 1)
	BEGIN
		SET @StartDate = DATEADD(DAY, 1 * @AddSub, @StartDate)
	END

	SET @NumWeeks = @businessDays / 5
	SET @StartDate = DATEADD(WEEK, @NumWeeks, @StartDate)
	SET @businessDays = @businessDays % 5		  

	SET @EndDate = @StartDate
	
	WHILE @businessDays <> 0 
	BEGIN		
		SELECT @EndDate = DATEADD(DAY,1 * @AddSub,@EndDate)
		
		SELECT @businessDays =
		CASE DATEPART(WEEKDAY,@EndDate)
			WHEN 1 THEN @businessDays
			WHEN 2 THEN @businessDays
			ELSE @businessDays - 1 * @AddSub
		END
	END
	    
	RETURN @EndDate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnBackCalculateBaseOffsetDate]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     3/6/2012
-- Release:     3.1
-- Description: Back calculates the base offset date using the
--   specified template, milestone and date.
--
-- Notes: Any call of this function MUST MUST MUST FIRST 
--   'SET DATEFIRST 6' otherwise the results will be totally
--   wrong, so BEWARE!!!!
--
-- mm/dd/yy  Name      Release  Description
-- 05/14/14  j.borgers 5.1      Added SequenceNumber
-- =============================================================
CREATE FUNCTION [G2].[fnBackCalculateBaseOffsetDate]
(
	@MilestoneTemplateId int,
	@MilestoneId int,
	@MilestoneDt datetime,
	@SequenceNumber int
)
RETURNS
datetime
AS
BEGIN
	--DECLARE @MilestoneTemplateId int = 91, @MilestoneId int, @MilestoneDt datetime = '2/12/2013';
	--SET @MilestoneId = 222; --Non Base
	--SET @MilestoneId = 51; --Base
	--SET @MilestoneId = 230; --Optional

	DECLARE @calculatedDt datetime;

	SELECT @calculatedDt = G2.fnOffsetBusinessDays(DATEADD(WEEK, -mtd.WeekOffset, DATEADD(MONTH, -mtd.MonthOffset, DATEADD(YEAR, -mtd.YearOffset, @MilestoneDt))), -mtd.DayOffset)
	FROM G2.MilestoneTemplateDetail mtd
	INNER JOIN G2.Milestone m ON mtd.MilestoneId = m.Id
	WHERE mtd.MilestoneTemplateId = @MilestoneTemplateId
	AND mtd.MilestoneId = @MilestoneId
	AND COALESCE(mtd.SequenceNumber, 0) = COALESCE(@SequenceNumber, 0)
	AND m.OptionalInd = 0;

	RETURN @calculatedDt;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetMilestoneDateSpread]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     5/31/2013
-- Release:     4.2
-- Description: Applies the default date spread for the
--   specified template, milestone and date.
--
-- mm/dd/yy  Name      Release  Description
-- 05/31/13  s.oneal   4.2      Ported/pulled logic from [G2].[spMilestoneDateSpreadCalculateByMilestoneTemplateId]
--                              and [G2].[fnGetScopeMilestoneDateSpread]
-- 05/14/14  j.borgers 5.1      Added SequenceNumber
-- =============================================================
CREATE FUNCTION [G2].[fnGetMilestoneDateSpread]
(
	@MilestoneTemplateId int,
	@MilestoneId int,
	@MilestoneDt datetime,
	@SequenceNumber int
)
RETURNS
@MilestoneDateSpread TABLE (MilestoneId int, SequenceNumber int, MilestoneDt datetime, BaseScheduleOffsetInd bit)
AS
BEGIN
	--DECLARE @MilestoneTemplateId int, @MilestoneId int, @MilestoneDt datetime;
	--DECLARE @MilestoneDateSpread TABLE (MilestoneId int, MilestoneDt datetime, BaseScheduleOffsetInd bit);
	--SET DATEFIRST 6;

	--SET @MilestoneTemplateId = 25;
	--SET @MilestoneId = 23;
	--SET @MilestoneDt = '6/6/2013';

	--SET @MilestoneId = 43;
	--SET @MilestoneDt = '6/2/2013';
	--SET @MilestoneDt = '9/30/2012';


	DECLARE @baseDt datetime, @isOriginalDateWeekend bit, @originalDateFiscalYear int;

	-- Back calculating the base offset date
	SELECT @baseDt = G2.fnBackCalculateBaseOffsetDate(@MilestoneTemplateId, @MilestoneId, @MilestoneDt, @SequenceNumber);
	SET @isOriginalDateWeekend = CASE WHEN DATEPART(WEEKDAY, @MilestoneDt) IN (1,2) THEN 1 ELSE 0 END;
	SET @originalDateFiscalYear = g2.fnGetFiscalYearFromDate(@MilestoneDt);


	INSERT INTO @MilestoneDateSpread
	SELECT m.Id AS MilestoneId, mtd.SequenceNumber,
		G2.fnOffsetBusinessDays(DATEADD(WEEK, mtd.WeekOffset, DATEADD(MONTH, mtd.MonthOffset, DATEADD(YEAR, mtd.YearOffset, @baseDt))), mtd.DayOffset) AS MilestoneDate,
		mtd.BaseScheduleOffsetInd
	FROM G2.MilestoneTemplate mt
	INNER JOIN G2.MilestoneTemplateDetail mtd ON mt.Id = mtd.MilestoneTemplateId
	INNER JOIN G2.Milestone m ON mtd.MilestoneId = m.Id
	WHERE mt.Id = @MilestoneTemplateId AND m.OptionalInd = 0
	ORDER BY mtd.DisplayOrder;


	-- Case 1
	-- This updates the BaseScheduleOffsetInd milestone date if it has been moved to a different
	-- fiscal year from what was passed in; the only reason this date would get moved would be if
	-- the date provided is on a weekend and would be moved to the next Monday, therefore, the date
	-- can be moved back by 3 days to move it to Friday instead, which should be in the same FY
	-- Case 2
	-- This keeps the non-base schedule milestone the date is being applied off of the same
	-- as long as it wasn't moved because it's a weekend
	UPDATE @MilestoneDateSpread SET
		MilestoneDt = CASE WHEN ds.BaseScheduleOffsetInd = 1 AND @originalDateFiscalYear <> prd.FiscalYear THEN DATEADD(DAY, -3, ds.MilestoneDt)
						   WHEN ds.BaseScheduleOffsetInd = 0 AND ds.MilestoneDt <> @MilestoneDt AND @isOriginalDateWeekend = 0 THEN @MilestoneDt
						   ELSE ds.MilestoneDt
					  END
	--SELECT ds.*,
	--	CASE WHEN ds.BaseScheduleOffsetInd = 1 AND @originalDateFiscalYear <> prd.FiscalYear THEN 'Case 1'
	--		 WHEN ds.BaseScheduleOffsetInd = 0 AND ds.MilestoneDt <> @MilestoneDt AND @isOriginalDateWeekend = 0 THEN 'Case 2'
	--		 ELSE 'Else'
	--	END AS WhichCase,
	--	CASE WHEN ds.BaseScheduleOffsetInd = 1 AND @originalDateFiscalYear <> prd.FiscalYear THEN DATEADD(DAY, -3, ds.MilestoneDt)
	--		 WHEN ds.BaseScheduleOffsetInd = 0 AND ds.MilestoneDt <> @MilestoneDt AND @isOriginalDateWeekend = 0 THEN @MilestoneDt
	--		 ELSE ds.MilestoneDt
	--	END AS WhichCase
	FROM @MilestoneDateSpread ds
	INNER JOIN G2.Period prd ON ds.MilestoneDt BETWEEN prd.BeginDt AND prd.EndDt
	WHERE ds.MilestoneId = @MilestoneId
	AND COALESCE(ds.SequenceNumber,0) = COALESCE(@SequenceNumber, 0)


	RETURN;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProjectCanceledMilestoneId]'
GO

-- =============================================================
-- Author:      Chris O'Neal
-- Created:     10/01/2014
-- Release:     5.3
-- Description: Return the milestone id of the 'project canceled' milestone for a given subsystem.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProjectCanceledMilestoneId]
( 
	@SubSystemId INT 
)
RETURNS INT
AS
BEGIN
	DECLARE	@id INT;

    SELECT @id = id
    FROM G2.Milestone m
    WHERE m.SubSystemId = @SubSystemId
	AND m.name = 'Project Canceled'

	RETURN	@id;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkBreakdownStructureHierarchyId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     1/8/2014
-- Release:     5.0
-- Description: Returns the hierarchyid data type for the
--   corresponding parent id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetWorkBreakdownStructureHierarchyId]
(
	@ParentId int
)
RETURNS hierarchyid
AS
BEGIN
	DECLARE @hierarchyId AS hierarchyid,
		@parentHierarchyId AS hierarchyid,
		@lastChildHierarchyId AS hierarchyid;

	IF @ParentId IS NULL
	BEGIN
		SET @hierarchyId = HIERARCHYID::GetRoot().GetDescendant((SELECT MAX(wh.WorkBreakdownStructureHierarchyId)
															     FROM G2.WorkBreakdownStructureHierarchy wh
																 WHERE wh.WorkBreakdownStructureHierarchyId.GetAncestor(1) = hierarchyid::GetRoot()), NULL)
			 
	END
	ELSE
	BEGIN   					
		-- Need to create the hierarchy_id
		SET @parentHierarchyId = (SELECT wbsh.WorkBreakdownStructureHierarchyId
								  FROM G2.WorkBreakdownStructure wbs
								  INNER JOIN G2.WorkBreakdownStructureHierarchy wbsh ON wbs.Id = wbsh.WorkBreakdownStructureId
								  WHERE wbs.Id = @ParentId);
		SET @lastChildHierarchyId = (SELECT MAX(WorkBreakdownStructureHierarchyId)
									 FROM G2.WorkBreakdownStructureHierarchy wh
									 WHERE wh.WorkBreakdownStructureHierarchyId.GetAncestor(1) = @parentHierarchyId);								 
		SET @hierarchyId = @parentHierarchyId.GetDescendant(@lastChildHierarchyId, NULL);
	END
	
	RETURN(@hierarchyId)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTaskSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     11/20/2014
-- Release:     5.3
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTaskSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Task'
			AND ssl.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSourcesMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/14/2014
-- Release:     5.3
-- Description: Returns Metric Id for Source(s)
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetSourcesMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Source(s)'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingConstrainedBudgetWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     6/16/2016
-- Release:     7.1
-- Description: Retrieves the Attribute Type Id for Funding Constrained Budget
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [G2].[fnGetFundingConstrainedBudgetWorkBreakdownStructureAttributeTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 29; -- Funding Constrained Budget
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApprovedChangeRequestDetailStatusId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/13
-- Release:     4.4
-- Description: Retrieves the Approved change request detail status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApprovedChangeRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 4; -- Approved
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetVoidChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Void change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetVoidChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 11; -- Void
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubmitChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Submit change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSubmitChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 9; -- Submit
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSplitChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Split change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSplitChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 21; -- Split
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRejectedChangeRequestDetailStatusId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/23/2013
-- Release:     4.4
-- Description: Retrieve the rejected change request detail status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRejectedChangeRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 5; -- Rejected
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRejectChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/13
-- Release:     4.4
-- Description: Retrieves the Reject change request action Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRejectChangeRequestActionId]()
RETURNS INT
AS
BEGIN
	RETURN 5; -- Reject
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRecallChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Recall change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRecallChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 18; -- Recall
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPassedBackChangeRequestDetailStatusId]'
GO



-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/13
-- Release:     4.4
-- Description: Retrieves the Passed Back change request detail status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPassedBackChangeRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 9; -- Passed Back
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPassBackChangeRequestActionId]'
GO



-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/13
-- Release:     4.4
-- Description: Retrieves the Pass Back change request action Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPassBackChangeRequestActionId]()
RETURNS INT
AS
BEGIN
	RETURN 7; -- Pass Back
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNeedsAttentionChangeRequestDetailStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     02/21/2014
-- Release:     5.0
-- Description: Retrieves the Needs Attention change request
--   detail status id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNeedsAttentionChangeRequestDetailStatusId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Needs Attention
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInsufficientFundsChangeRequestDetailStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     02/21/2014
-- Release:     5.0
-- Description: Retrieves the Insufficient Funds change request
--   detail status id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInsufficientFundsChangeRequestDetailStatusId]()
RETURNS int
AS
BEGIN
	RETURN 11; -- Insufficient Funds
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundsValidationFailureChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Funds Validation Failure change
--   request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundsValidationFailureChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 26; -- Funds Validation Failure
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetEditChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/18/2014
-- Release:     5.0
-- Description: Retrieves the Edit change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetEditChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Edit
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetClaimChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Claim change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetClaimChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 19; -- Claim
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBackgroundProcessAdjustmentChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Background Process Adjustment
--   change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBackgroundProcessAdjustmentChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 23; -- Background Process Adjustment
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApproveChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/24/13
-- Release:     4.4
-- Description: Retrieves the Approve change request action Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApproveChangeRequestActionId]()
RETURNS INT
AS
BEGIN
	RETURN 4; -- Approve
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPerformanceThruPeriod]'
GO
/****** Object:  UserDefinedFunction [G2].[fnGetCurrentPerformanceThruPeriod]    Script Date: 5/30/2014 3:51:19 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
-- =============================================================
-- Author:      
-- Created:     
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 06/04/14  j.borgers 5.2      Added SubSystemId
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPerformanceThruPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2Parameters gp
	WHERE gp.ParmKey = 'Current Performance Thru Period'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPassBackPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/19/2013
-- Release:     4.4
-- Description: Retrieves the Pass Back permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPassBackPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 51; -- Pass Back
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetReactorsMetricId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     11/21/2014
-- Release:     5.3
-- Description: Get the MetricId for the Reactors metric
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetReactorsMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Reactor(s)'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetStandardPrimaryFilterTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/15/2014 
-- Release:     5.3
-- Description: Gets the Filter Type Id for the Standard Primary Filter
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetStandardPrimaryFilterTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 1; --Standard Primary Filter
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnCheckFilterTypeConfiguration]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     3/13/2015
-- Release:     5.5
-- Description: Ensures that we don't have a SSL showing up in more than one filter type
--
-- mm/dd/yy  Name     Release  Description
-- 03/28/17  N.Harris 8.0      Added FilterTypeId to parameters
-- =============================================================
CREATE FUNCTION [G2].[fnCheckFilterTypeConfiguration]
(
	@SubSystemLevelId INT,
	@SubSystemRoleId INT,
	@FilterTypeId INT
)
RETURNS INT
AS 
BEGIN

	DECLARE @valid INT = 1;

	SELECT @valid = 0
	FROM G2.FilterTypeConfiguration ftc
	WHERE ftc.SubSystemLevelId = @SubSystemLevelId
	AND @SubSystemLevelId IS NOT NULL
	GROUP BY ftc.SubSystemLevelId
	HAVING COUNT(*) > 1

	RETURN @valid;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetHQAFPCreationStatusParmKey]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     09/23/2016
-- Release:     6.3
-- Description: Returns the HQ AFP Creation Status ParmKey
--
-- mm/dd/yy  Name        Release  Description
-- 04/04/17  JDR         8.0      Moved to the G2 schema
-- =============================================================
CREATE FUNCTION [G2].[fnGetHQAFPCreationStatusParmKey]()
RETURNS
VARCHAR(50)
AS
BEGIN
	RETURN 'HQ AFP Creation Status';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSystemAdministratorRoleId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/6/2013
-- Release:     4.4
-- Description: Retrieves the System Administrator role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSystemAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 48; -- System Administrator
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentBaselineThruPeriod]'
GO
--/****** Object:  UserDefinedFunction [G2].[fnGetCurrentBaselineThruPeriod]    Script Date: 5/30/2014 3:46:24 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

-- =============================================================
-- Author:      
-- Created:      
-- Release:     
-- Description: 
--
-- mm/dd/yy  Name      Release  Description
-- 06/04/14  j.borgers 5.2      Added SubSystemId 
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentBaselineThruPeriod]
(
@SubSystemId int
)
RETURNS
int
AS
BEGIN
	DECLARE @id int;

	SELECT @id = cast (value as int)
	FROM G2Parameters gp
	WHERE gp.ParmKey = 'Current Baseline Thru Period'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCrossCuttingWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/29/2014
-- Release:     5.3
-- Description: Retrieves the Cross-Cutting work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCrossCuttingWorkTypeId] 
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	DECLARE @id INT;

	SELECT @id = wt.Id 
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Cross-Cutting';

	RETURN @id; -- Cross-Cutting / Work Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetMinimumFinancialCRReportParameterStartDate]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     10/20/2015
-- Release:     5.3
-- Description: Retrieves the Minimum Financial CR Report Parameter Start Date
--   G2 Parameter value.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetMinimumFinancialCRReportParameterStartDate] 
(
	@SubSystemId INT 
)
RETURNS DATETIME
AS
BEGIN

	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */

	DECLARE @parmKeyValue VARCHAR(50);
	SELECT @parmKeyValue = CAST(gp.Value AS VARCHAR(50))
	FROM G2.G2Parameters gp
	WHERE gp.SubSystemId = @SubSystemId
	AND gp.ParmKey = 'Minimum Financial CR Report Parameter Start Date';

	/* Debug Declarations */
	--SELECT @parmKeyValue, CONVERT(DATETIME, @parmKeyValue);
	/* Debug Declarations */

	RETURN CONVERT(DATETIME, @parmKeyValue)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPATeamMemberRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the SPA Team Member role.
--
-- mm/dd/yy  Name        Release  Description
-- 08/07/15  Strickland  6.3    Update for new roles
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPATeamMemberRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 125; -- SPA Team Member
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPATeamLeadRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the SPA Team Lead role.
--
-- mm/dd/yy  Name        Release  Description
-- 08/07/15  Strickland  6.3    Update for new roles
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPATeamLeadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 126; -- SPA Team Lead
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAProtectTechnicalLeadRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Protect Technical Lead role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAProtectTechnicalLeadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 132; -- SPA Protect Technical Lead 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAPortfolioManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Portfolio Manager (Secondary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAPortfolioManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 129; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAPortfolioManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Portfolio Manager (Primary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAPortfolioManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 128; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPALabLeadRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Lab Lead role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPALabLeadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 127; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkBreakdownStructureLabelSubSystemResourceTextName]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/19/2014
-- Release:     5.3
-- Description: Retrieves the key for the WorkBreakdownStructure
--   Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetWorkBreakdownStructureLabelSubSystemResourceTextName]()
RETURNS NVARCHAR(100)
AS 
BEGIN
	RETURN 'WorkBreakdownStructure Label';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOtherDirectCostConfigurationTypeId]'
GO
-- =============================================
-- Author:		Marshall Lee
-- Create date: 11/17/2015
-- Release:     6.4
--
-- Description:	Returns the Id for the Other Direct Cost ConfigurationType
-- =============================================
CREATE FUNCTION [G2].[fnGetOtherDirectCostConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 6; --Other Direct Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetIndirectCostConfigurationTypeId]'
GO
-- =============================================
-- Author:		Marshall Lee
-- Create date: 11/17/2015
-- Release:     6.4
--
-- Description:	Returns the Id for the Indirect Cost ConfigurationType
-- =============================================
CREATE FUNCTION [G2].[fnGetIndirectCostConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 5; --Indirect Cost
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCostObligationConfigurationTypeId]'
GO



-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/03/2014 
-- Release:     5.0
-- Description: Returns the Id for the Cost/Obligation ConfigurationType
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCostObligationConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 3; -- Cost/Obligation
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationBaseCaseChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2015
-- Release:     6.4
-- Description: Retrieves the Budget Formulation Base Case
--   Change Request Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationBaseCaseChangeRequestTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 32; -- Budget Formulation Base Case
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetRecallPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/19/2013
-- Release:     4.4
-- Description: Retrieves the Recall permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetRecallPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 24; -- Recall
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNonConcurChangeRequestDetailConcurrenceStatusId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/20/2013
-- Release:     4.4
-- Description: Retrieves the Non-Concur change request detail
--   concurrence status id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNonConcurChangeRequestDetailConcurrenceStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Non-Concur
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetResponsibleRolesPerWorkflowPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/3/2013
-- Release:     4.2
-- Description: Retrieves the Responsible Roles Per Workflow
--   permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetResponsibleRolesPerWorkflowPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 43; -- Responsible Roles Per Workflow
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceGroupId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/27/2012
-- Release:     3.4
-- Description: Retrieves the Performance ChangeRequestTypeGroupId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceGroupId]()
RETURNS int
AS
BEGIN
	
	RETURN 2;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetHistoricSpendPlanTypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     08/27/2012
-- Release:     3.4
-- Description: Retrieves the Historic Spend Plan ChangeRequestTypeId.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetHistoricSpendPlanTypeId]()
RETURNS int
AS
BEGIN
	
	RETURN 14;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnParseList]'
GO
CREATE FUNCTION [G2].[fnParseList]
(
	@List varchar(500),
	@Delim varchar(1)
)
RETURNS 
@ParsedList table
(
	ID INT,
	SortOrder INT IDENTITY(1, 1)
)
AS
BEGIN
	DECLARE @ListValue varchar(10), @Pos int

	SET @List = LTRIM(RTRIM(@List))+ @Delim
	SET @Pos = CHARINDEX(@Delim, @List, 1)

	IF REPLACE(@List, @Delim, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ListValue = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
			IF @ListValue <> ''
			BEGIN
				INSERT INTO @ParsedList (ID) 
				VALUES (CAST(@ListValue AS int)) --Use Appropriate conversion
			END
			SET @List = RIGHT(@List, LEN(@List) - @Pos)
			SET @Pos = CHARINDEX(@Delim, @List, 1)

		END
	END	
	RETURN
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnMultiSelectStringValue_Param]'
GO
-- =============================================================
-- Author:      Unknown
-- Created:     Unknown
-- Release:     Unknown
-- Description: Returns a table-values list from the provided 
--              string parameter by unpacking the delimited values.
--
-- mm/dd/yy   Name     Release  Description
-- 03/03/17   a.smith    7.5    Set the RepParm parameter to MAX size
-- 07/14/17   a.smith    8.1    Changed size of @Piece from 100 to 500
-- =============================================================
CREATE FUNCTION [G2].[fnMultiSelectStringValue_Param]
   (@RepParam NVARCHAR(MAX), @Delim char(1)= ',')
RETURNS @Values TABLE (Param NVARCHAR(MAX))AS
  BEGIN
  DECLARE @chrind INT
  DECLARE @Piece NVARCHAR(500)
  SELECT @chrind = 1 
  WHILE @chrind > 0
    BEGIN
      SELECT @chrind = CHARINDEX(@Delim,@RepParam)
      IF @chrind  > 0
        SELECT @Piece = LEFT(@RepParam,@chrind - 1)
      ELSE
        SELECT @Piece = @RepParam
      INSERT  @Values(Param) VALUES(@Piece)
      SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)
      IF LEN(@RepParam) = 0 BREAK
    END
  RETURN
  END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetStandardSecondaryFilterTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/15/2014 
-- Release:     5.3
-- Description: Gets the Filter Type Id for the Standard Secondary Filter
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetStandardSecondaryFilterTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 2; --Standard Secondary Filter
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSecondaryFilterSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Billy Welch
-- Created:     12/22/2017 
-- Release:     8.4
-- Description: Returns the SubsystemLevel.Id associated with the standard
--              secondary filter type for the passed-in subsystem.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSecondaryFilterSubSystemLevelId] (@SubsystemId INT)
RETURNS INT
AS
BEGIN
	DECLARE @subsystemLevelId INT,
			@standardSecondaryFilterTypeId INT = G2.fnGetStandardSecondaryFilterTypeId();

	SELECT @subsystemLevelId = ssl.Id
	FROM G2.FilterType ft
	INNER JOIN G2.FilterTypeConfiguration sslftx ON sslftx.FilterTypeId = ft.Id
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = sslftx.SubSystemLevelId 
	WHERE ssl.SubSystemId = @SubsystemId
	AND ft.Id = @standardSecondaryFilterTypeId;

	RETURN @subsystemLevelId;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetEditPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/19/2013
-- Release:     4.4
-- Description: Retrieves the Edit permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetEditPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 19; -- Edit
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetStandardRoleGroupFilter2TypeId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     3/6/2015
-- Release:     5.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetStandardRoleGroupFilter2TypeId]()
RETURNS INT
AS 
BEGIN
	RETURN 4;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetStandardRoleGroupFilter1TypeId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     3/6/2015
-- Release:     5.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetStandardRoleGroupFilter1TypeId]()
RETURNS INT
AS 
BEGIN
	RETURN 3;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetConfigurationTypeId]'
GO
-- =============================================================
-- Author:      Christopher Luttrell (chrisl)
-- Created:     01/15/2015 
-- Release:     5.4
-- Description: Returns the Id for the Budget ConfigurationType
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 1; -- Budget
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProjectManagerSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the Schedule: Project Manager (Secondary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleProjectManagerSecondaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'Schedule: Project Manager (Secondary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProjectManagerPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/19/2014
-- Release:     5.3
-- Description: Retrieves the Schedule: Project Manager role id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleProjectManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 100; -- Schedule: Project Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProjectManagerRoleId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     01/29/2014 
-- Release:     5.0
-- Description: Retrieves the Project Manager role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProjectManagerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 9; -- Project Manager
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProjectManagerDesigneeRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     02/13/2014 
-- Release:     5.0
-- Description: Retrieves the Project Manager Designee role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetProjectManagerDesigneeRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 11; -- Project Manager Designee
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNewWorkBreakdownStructureStatusId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     07/01/2014
-- Release:     5.2
-- Description: Retrieves the New WorkBreakdownStructure
--   status id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNewWorkBreakdownStructureStatusId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- New
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanProjectManagerSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Project Manager (Secondary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanProjectManagerSecondaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'FinPlan: Project Manager (Secondary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanProjectManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/06/2015
-- Release:     6.3
-- Description: Returns the value for the FinPlan: Project Manager (Primary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanProjectManagerPrimaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'FinPlan: Project Manager (Primary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFundingRequestUserRoleId]'
GO
-- =============================================================
-- Author:      Jill Rochat
-- Created:     02/03/2017 
-- Release:     7.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFundingRequestUserRoleId] ()
RETURNS INT
AS
BEGIN
  RETURN 186; -- Funding Request User Role Id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetMassProcessPermissionId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     11/5/2014
-- Release:     5.3
-- Description: Return Permission ID for Mass Process
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetMassProcessPermissionId]()
RETURNS INT
AS
BEGIN
  RETURN 21;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetOfficeDirectorRoleId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/11/2014
-- Release:     5.0
-- Description: Retrieves the Office Director role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetOfficeDirectorRoleId] ()
RETURNS int
AS
BEGIN

	RETURN 7; -- Office Director
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoffParmKey]'
GO

-- =============================================================
-- Author:      Chad Gilbert
-- Created:     11/04/2014
-- Release:     
-- Description: Returns the ParamKey of the G2Parameters for Performance Allowed Joule Completion Cutoff
--
-- mm/dd/yy    Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoffParmKey] ()
RETURNS nvarchar(50)
AS
BEGIN
	RETURN 'Performance Allowed Joule Completion Cutoff';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNextPeriodId]'
GO

-- =============================================================
-- Author:     Unknown
-- Created:    ?/??/???
-- Description: 
--
-- Usage:     @IncludeReconciliationPeriod
--	1 if you want to include the 13th Period when determining the next Fiscal Period
--  0 if you DONT want to include the 13th Period when determining the next Fiscal Period
--
-- mm/dd/yy		Name     StoryId  Description
-- 05/02/2012	RLA		 3.2	   Made the function Period 13 aware ...
-- =============================================================
CREATE FUNCTION [G2].[fnGetNextPeriodId]
(
	@CurrentPeriodId INT,
	@IncludeReconciliationPeriod BIT
)
RETURNS
INT
AS
BEGIN

--DECLARE @CurrentPeriodId INT = 783, @IncludeReconciliationPeriod BIT = 1
	
	DECLARE @id int, @beginDt Datetime, @EndDt Datetime, @FiscalPeriod INT
	
	SELECT @beginDt = BeginDt, @EndDt = EndDt, @FiscalPeriod = FiscalPeriod
	  FROM G2.Period
	 WHERE id = @CurrentPeriodId

    IF @FiscalPeriod = 12 AND @IncludeReconciliationPeriod = 1  SET @begindt = DATEADD(ms,+3,@EndDt)
    ELSE IF @FiscalPeriod = 13 SET @begindt = DATEADD(ms,+3,@EndDt)
          ELSE SET @begindt =DATEADD(m,+1,@beginDt)

	SELECT @id =  Id
	FROM Period 
	WHERE Begindt = @beginDt

	RETURN @id
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPerformanceThruPeriodParmKey]'
GO

-- =============================================================
-- Author:      Chad Gilbert
-- Created:     11/04/2014
-- Release:     
-- Description: Returns the ParamKey of the G2Parameters for Current Performance Thru Period
--
-- mm/dd/yy    Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPerformanceThruPeriodParmKey] ()
RETURNS nvarchar(50)
AS
BEGIN
	RETURN 'Current Performance Thru Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPerformancePeriodParmKey]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/09/2014 
-- Release:     5.3
-- Description: Returns the ParamKey of the G2Parameters for the Current PerformancePeriod
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPerformancePeriodParmKey] ()
RETURNS nvarchar(50)
AS
BEGIN
	RETURN 'Current Performance Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceMinimumAllowedJouleCompletionDateParmKey]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/09/2014 
-- Release:     5.3
-- Description: Returns the ParamKey of the G2Parameters for the Performance Minimum Allowed Joule Completion Date
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceMinimumAllowedJouleCompletionDateParmKey] ()
RETURNS nvarchar(50)
AS
BEGIN
	RETURN 'Performance Minimum Allowed Joule Completion Date';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnOffsetCalendarDays]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     12/02/2016
-- Release:     
-- Description: Offsets a number of calendar days. When the resulting calendar day falls
--              on a saturday, the function returns the following Sunday because we know
--              we're going to add 1 day to it when we consume it so we will advance on a Monday
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnOffsetCalendarDays]
(
	 @StartDate DATETIME
	,@CalendarDays INT
)
RETURNS DATETIME
AS 
BEGIN 

	DECLARE @endDate DATETIME, @dayMovement INT;

	SELECT @dayMovement = CASE WHEN @CalendarDays >= 0 THEN 1 ELSE -1 END;

	SELECT @endDate = DATEADD(dd, @CalendarDays, @StartDate);
	
	WHILE (DATEPART(dw, @endDate) IN (1, 2))
	BEGIN
		SELECT @endDate = DATEADD(dd, @dayMovement, @endDate);
	END
	    
	RETURN @endDate
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoffTypeParmKey]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     12/02/16
-- Release:     
-- Description: Returns the ParamKey of the G2Parameters for Performance Allowed Joule Completion Cutoff
--
-- mm/dd/yy    Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoffTypeParmKey]()
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN 'Performance Allowed Joule Completion Cutoff Type';
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCalculatedMinimumJouleCompletionDate]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     12/02/16
-- Release:     7.4
-- Description: Returns the CalculatedDefaultMinimumAllowedJouleCompletionDate and 
--              CalculatedPeriod.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCalculatedMinimumJouleCompletionDate]
(
	@SubSystemId INT
)
RETURNS @calculatedMinimumJouleCompletionInfo TABLE
(
	SubSystemId INT, CalculatedDefaultMinimumAllowedJouleCompletionDate DATETIME, CalculatedPeriod INT, CalculatedPreviousPeriod INT
)
AS
BEGIN 
	DECLARE @performanceAllowedJouleCompletionCutoffParmKey VARCHAR(50) = G2.fnGetPerformanceMinimumAllowedJouleCompletionCutoffParmKey(),
		@currentPeriodId INT = G2.fnGetCurrentPeriod(),
		@performanceAllowedJouleCompletionCutoffTypeParmKey VARCHAR(50) = G2.fnGetPerformanceMinimumAllowedJouleCompletionCutoffTypeParmKey();
	
	DECLARE @previousPeriodId INT = G2.fnGetPreviousPeriodId(@currentPeriodId, 0);

	INSERT INTO @calculatedMinimumJouleCompletionInfo (SubSystemId, CalculatedDefaultMinimumAllowedJouleCompletionDate, CalculatedPeriod, CalculatedPreviousPeriod)
	/* This next line is a little complicated. Since the fnOffsetBusiness days takes a business day offset and always returns a business day
	we're passing in the default cutoff - 1. This ensures that we get the last business day of the available time. Then we add 1 day to that 
	because the business function would return the next business day (which could be a Monday) when Ty wants the window to move on the day after
	the cutoff business day*/
	SELECT x.SubSystemId,
		CASE WHEN CURRENT_TIMESTAMP >= DATEADD(dd, 1, G2.fnOffsetBusinessDays(DATEADD(dd, -(DAY(CURRENT_TIMESTAMP) - 1), CURRENT_TIMESTAMP), x.DefaultCutoff - 1)) THEN p.BeginDt	ELSE priorPeriod.BeginDt END AS CalculatedDefaultMinimumAllowedJouleCompletionDate,
		CASE WHEN CURRENT_TIMESTAMP >= DATEADD(dd, 1, G2.fnOffsetBusinessDays(DATEADD(dd, -(DAY(CURRENT_TIMESTAMP) - 1), CURRENT_TIMESTAMP), x.DefaultCutoff - 1)) THEN p.Id ELSE priorPeriod.Id END AS CalculatedPeriod,
		CAST(NULL AS INT) AS CalculatedPreviousPeriod
	FROM 
	(
		SELECT gp.SubSystemId, CONVERT(INT, gp.Value) AS DefaultCutoff
		FROM G2.G2Parameters gp
		WHERE gp.ParmKey = @performanceAllowedJouleCompletionCutoffParmKey
		AND (COALESCE(@SubSystemId, -1) = gp.SubSystemId OR @SubSystemId IS NULL)
	) x
	CROSS APPLY 
	(
		SELECT gpType.Value
		FROM G2.G2Parameters gpType
		WHERE gpType.ParmKey = @performanceAllowedJouleCompletionCutoffTypeParmKey
		AND gpType.SubSystemId = x.SubSystemId
	) y
	CROSS JOIN G2.Period p
	CROSS JOIN G2.Period priorPeriod 
	WHERE p.Id = @currentPeriodId
	AND priorPeriod.Id = @previousPeriodId
	AND y.Value = 'working'

	UNION 

	/*
		Handle calendar days
	*/
	SELECT x.SubSystemId,
		CASE WHEN CURRENT_TIMESTAMP >= DATEADD(dd, 1, G2.fnOffsetCalendarDays(DATEADD(dd, -(DAY(CURRENT_TIMESTAMP) - 1), CURRENT_TIMESTAMP), x.DefaultCutoff - 1)) THEN p.BeginDt	ELSE priorPeriod.BeginDt END AS CalculatedDefaultMinimumAllowedJouleCompletionDate,
		CASE WHEN CURRENT_TIMESTAMP >= DATEADD(dd, 1, G2.fnOffsetCalendarDays(DATEADD(dd, -(DAY(CURRENT_TIMESTAMP) - 1), CURRENT_TIMESTAMP), x.DefaultCutoff - 1)) THEN p.Id ELSE priorPeriod.Id END AS CalculatedPeriod,
		CAST(NULL AS INT) AS CalculatedPreviousPeriod
	FROM 
	(
		SELECT gp.SubSystemId, CONVERT(INT, gp.Value) AS DefaultCutoff
		FROM G2.G2Parameters gp
		WHERE gp.ParmKey = @performanceAllowedJouleCompletionCutoffParmKey
		AND (COALESCE(@SubSystemId, -1) = gp.SubSystemId OR @SubSystemId IS NULL)
	) x
	CROSS APPLY 
	(
		SELECT gpType.Value
		FROM G2.G2Parameters gpType
		WHERE gpType.ParmKey = @performanceAllowedJouleCompletionCutoffTypeParmKey
		AND gpType.SubSystemId = x.SubSystemId
	) y
	CROSS JOIN G2.Period p
	CROSS JOIN G2.Period priorPeriod 
	WHERE p.Id = @currentPeriodId
	AND priorPeriod.Id = @previousPeriodId
	AND y.Value = 'calendar';

	UPDATE ajcd SET
		CalculatedPreviousPeriod = G2.fnGetPreviousPeriodId(ajcd.CalculatedPeriod, 0)
	FROM @calculatedMinimumJouleCompletionInfo ajcd;

	RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetMassApprovePermissionId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     11/5/2014
-- Release:     5.3
-- Description: Return Permission ID for Mass Approve
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetMassApprovePermissionId]()
RETURNS INT
AS
BEGIN
  RETURN 20;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProjectSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     12/03/2014
-- Release:     5.3
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProjectSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	
	RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Project'
			AND ssl.SubSystemId = @SubSystemId)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsRequestsRequiringMyAttentionFilter]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/20/2013
-- Release:     4.4
-- Description: Function to determine if specified filter is
--   the "Requests Requiring My Attention" filter.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnIsRequestsRequiringMyAttentionFilter]
(
	@Filter varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @match bit = 0;
	SET @match = CASE WHEN @Filter = 'Requests Requiring My Attention' THEN 1 ELSE 0 END;
	RETURN @match;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsAllRequestsUnderMyPurviewFilter]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/20/2013
-- Release:     4.4
-- Description: Function to determine if specified filter is
--   the "All Requests Under My Purview" filter.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnIsAllRequestsUnderMyPurviewFilter]
(
	@Filter varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @match bit = 0;
	SET @match = CASE WHEN @Filter = 'All Requests Under My Purview' THEN 1 ELSE 0 END;
	RETURN @match;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationPlanningYearParmKey]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     12/2/2015
-- Release:     6.4
-- Description: Get the Parm Key for Budget Formulation Planning Year
--   period.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [G2].[fnGetBudgetFormulationPlanningYearParmKey] (
)
RETURNS NVarChar(100)
AS
BEGIN
	return 'Budget Formulation Planning Year';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationPlanningYear]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     11/30/2015
-- Release:     6.4
-- Description: Returns the G2Parameter that stores the Budget Formulation Planning Year
--
-- mm/dd/yy  Name      Release  Description
-- 12/2/15   KIA       6.4      Changed to get Parm Key from a function
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationPlanningYear] (@SubSystemId INT)
RETURNS INT
AS
BEGIN
	DECLARE @year INT;
	DECLARE @BudgetFormulationPlanningYearParmKey NVarChar(100) = G2.fnGetBudgetFormulationPlanningYearParmKey();

	SELECT @year = CAST (value AS INT)
	FROM G2Parameters
	WHERE ParmKey = @BudgetFormulationPlanningYearParmKey
	AND SubSystemId = @SubSystemId;

	RETURN @year;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetUnconstrainedBudgetWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     1/20/2015
-- Release:     5.4
-- Description: Retrieves the Attribute Type Id for Unconstrained Budget.
--
-- mm/dd/yy  Name     Release  Description
-- 01/29/15  m.brown  5.4      moved from 21 to 20
-- =============================================================
CREATE FUNCTION [G2].[fnGetUnconstrainedBudgetWorkBreakdownStructureAttributeTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 20; -- Unconstrained Budget
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPrimarySiteWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/18/2016
-- Release:     7.4
-- Description: Get the WorkBreakdownStructureAttributeTypeId for
--              Primary Site
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPrimarySiteWorkBreakdownStructureAttributeTypeId] ()
RETURNS INT
AS
BEGIN

	DECLARE	@attributeTypeId INT;

	SELECT	@attributeTypeId = wat.Id
	FROM	G2.WorkBreakdownStructureAttributeType wat
	WHERE	wat.Name = 'Primary Site';

	/* Debug Declarations */
	--SELECT @attributeTypeId;
	/* Debug Declarations */

	RETURN @attributeTypeId;
END;


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA50PESubSystemId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     07/07/2014 
-- Release:     5.2
-- Description: Retrieves the NA-50 Program Execution SubSystem Id.
--
-- mm/dd/yy  Name      Release  Description
-- 04/08/15  j.borgers 6.0      Smart rename
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA50PESubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Office of Safety, Infrastructure and Operations / NA-50 PE
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetResearchReactorBuildingTypeId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     1/27/2014
-- Release:     5.0
-- Description: Retrieves the Research Reactor BuildingType Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetResearchReactorBuildingTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 4; -- Research Reactor
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetImplementationMilestoneTemplateCategoryId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/3/2014
-- Release:     5.0
-- Description: Retrieves the Implementation Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 09/29/15  JNW      6.3      Updated for NA21 split
-- =============================================================
CREATE FUNCTION [G2].[fnGetImplementationMilestoneTemplateCategoryId]
(
    @SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = 5;
	/* Debug Declarations */

	DECLARE @milestoneTemplateCategoryId int;

	SELECT @milestoneTemplateCategoryId = mtc.Id
	FROM G2.MilestoneTemplateCategory mtc
	WHERE mtc.Name = 'Implementation'
	AND mtc.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneTemplateCategoryId;
	/* Debug Declarations */

	RETURN @milestoneTemplateCategoryId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConductSiteAssessmentMilestoneId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     4/9/2014
-- Release:     5.0
-- Description: Retrieves the Conduct Site Assessment Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 10/22/15  Nevada W 6.3      Updated to point to new Id for GMS. 
-- =============================================================
CREATE FUNCTION [G2].[fnGetConductSiteAssessmentMilestoneId]()
RETURNS int
AS
BEGIN
	RETURN 599; -- Conduct Site Assessment
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBuildingSecureMilestoneId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/3/2014
-- Release:     5.0
-- Description: Retrieves the Building Secure Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 10/22/15  Nevada W 6.3      Updated to point to new Id for GMS. 
-- =============================================================
CREATE FUNCTION [G2].[fnGetBuildingSecureMilestoneId]()
RETURNS int
AS
BEGIN
	RETURN 596; -- Building Secure
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAwardContractMilestoneId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     4/9/2014
-- Release:     5.0
-- Description: Retrieves the Award Contract Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 10/22/15  Nevada W 6.3      Updated to point to new Id for GMS. 
-- =============================================================
CREATE FUNCTION [G2].[fnGetAwardContractMilestoneId]()
RETURNS INT
AS
BEGIN
	RETURN 595; -- Award Contract
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentPWPPeriod]'
GO
 

---- =============================================================
---- Author:      
---- Created:    
---- Description: 
----
---- mm/dd/yy    Name       Release  Description
---- 08/17/2015  a.smith       6.2   Added @SubSystemId as parameter and associated usage.
---- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentPWPPeriod]
(
	@SubSystemId INT
)
RETURNS
int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */

	DECLARE @id int;

	SELECT @id = cast (gp.value as int)
	FROM G2.G2Parameters gp 
	WHERE gp.ParmKey = 'Current PWP Period'
	AND gp.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @id;
	/* Debug Declarations */

	RETURN @id;
END
 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTaskSecondaryGroupLabelSubSystemResourceTextName]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/3/2014
-- Release:     5.3
-- Description: Retrieves the key for the Task Secondary Group
--   Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTaskSecondaryGroupLabelSubSystemResourceTextName]()
RETURNS nvarchar(100)
AS 
BEGIN
	RETURN 'Task Secondary Group Label';
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkBreakdownStructurePlanChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     05/23/2016 
-- Release:     7.1
-- Description: Returns the Work Breakdown Structure Plan ChangeRequestType Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetWorkBreakdownStructurePlanChangeRequestTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 36; --Work Breakdown Structure Plan
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNASVSubSystemId]'
GO

-- =============================================================
-- Author:      Melissa Cole
-- Created:     01/31/2014
-- Release:     5.0
-- Description: Retrieves the NA-50 PD sub-system id.
--
-- mm/dd/yy  Name      Release  Description
-- 04/08/15  j.borgers 6.0      Smart rename to fnGetNA50PDSubSystemId
-- 04/11/17  j.borgers 8.0		Smart rename to fnGetNASVSubSystemId
-- =============================================================
CREATE FUNCTION [G2].[fnGetNASVSubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Savannah River Field Office / NA-SV
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA10SubSystemId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     07/21/2014 
-- Release:     5.2
-- Description: Returns the sub-system Id for NA-10 Office of Defense Programs
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA10SubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Office of Defense Programs / NA-10
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoff]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     
-- Release:     3.1
-- Description: This function returns the minimum allowed joule completion cutoff 
--              day. This refers to the number of working days in a month a PM has 
--              to status prior month protect joule milestones before the actual
--              minimum allowed date moves forward to the first of the current month
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceMinimumAllowedJouleCompletionCutoff]
(
	@SubSystemId int
)
RETURNS INT
AS
BEGIN
	DECLARE @value INT;

	SELECT @value = CONVERT(INT, gp.Value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Performance Allowed Joule Completion Cutoff'
	AND gp.SubSystemId = @SubSystemId;

	RETURN @value;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetM3MasterSpendPlanReportId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     12/09/2016
-- Release:     7.4
-- Description: Returns M3 Master Spend Plan Report ID for SubSystem and FiscalYear
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetM3MasterSpendPlanReportId](@SubSystemId INT, @FiscalYear INT)
RETURNS INT
AS
BEGIN
  DECLARE @ReportId INT = (
          SELECT Id 
          FROM G2.Report r 
          WHERE r.SubSystemId = @SubSystemId 
          AND (r.FiscalYear = @FiscalYear OR (@FiscalYear IS NULL AND r.FiscalYear IS NULL))
          AND r.Name= 'M3 Master Spend Plan'
    );
    RETURN @ReportId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveRadInternationalWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Remove RAD International work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveRadInternationalWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Rem Rad Intl';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove RAD International / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveRadDomesticWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Remove RAD Domestic work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveRadDomesticWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Rem Rad Dom';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove RAD Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveNuclearUSOriginWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Remove Nuclear US-Origin work type id.
--
-- mm/dd/yy  Name      Release    Description
-- 08/27/15  a.smith     6.2      Added parameter to function and 
--                                refactored for multi-subsystem.
-- 08/28/15  a.smith     6.2      Modified query to use LongName instead
--                                of the Name field because that is the 
--                                one that has the common value between
--                                subsystems.
-- 06/06/17  a.smitn     8.1 PROD Upddated to return hard-coded id instead of searching for id; Removed parameter
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveNuclearUSOriginWorkTypeId] ()
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = 56;

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove Nuclear US-Origin / Work Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveNuclearRussianOriginWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Remove Nuclear Russian-Origin work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- 08/28/15  a.smith     6.2    Modified query to use LongName instead
--                              of the Name field because that is the 
--                              one that has the common value between
--                              subsystems.
-- 06/06/17  a.smith     8.1    Changed to return hard-coded id instead of 
--                              search on name; Removed Parameter
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveNuclearRussianOriginWorkTypeId] ()
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = 55;

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove Nuclear Russian-Origin / Work Type
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveNuclearGapOriginWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Remove Nuclear Gap-Origin work type id.
--
-- mm/dd/yy  Name      Release    Description
-- 08/27/15  a.smith     6.2      Added parameter to function and 
--                                refactored for multi-subsystem.
-- 08/28/15  a.smith     6.2      Modified query to use LongName instead
--                                of the Name field because that is the 
--                                one that has the common value between
--                                subsystems.
-- 06/06/17 a.smith      8.1 PROD Updated to return hard-coded id instead of searching on wt name; 
--                                Removed parameter.
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveNuclearGapOriginWorkTypeId] ()
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = 54;

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove Nuclear Gap-Origin / Work Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectRadInternationalWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Protect RAD International work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectRadInternationalWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Prot Rad Intl';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect RAD International / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectRadDomesticWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Protect RAD Domestic work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectRadDomesticWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Prot Rad Dom';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect RAD Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectNuclearInternationalWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Protect Nuclear International work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectNuclearInternationalWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Prot Nuc Intl';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Nuclear International / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectNuclearDomesticWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Protect Nuclear Domestic work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectNuclearDomesticWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Prot Nuc Dom';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Nuclear Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConvertWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Convert work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- 10/09/15  a.smith     6.3    Modified to search for work type name 
--                              "like" Convert. This assumes that only 1 
--                              Convert exists per subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetConvertWorkTypeId] 
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name LIKE 'Convert%';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Convert / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPASubProgramManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Sub-Program Manager (Secondary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPASubProgramManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 131;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPASubProgramManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Sub-Program Manager (Primary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPASubProgramManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 130;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPARMSCoordinatorRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     09/02/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA RMS Coordinator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPARMSCoordinatorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 142; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAIDDCoordinatorSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/06/2015
-- Release:     6.3
-- Description: Retrieves the id of the IDD Coordinator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAIDDCoordinatorSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 137; -- SPA: IDD Coordinator (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAIDDCoordinatorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/06/2015
-- Release:     6.3
-- Description: Retrieves the id of the IDD Coordinator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAIDDCoordinatorPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 136; -- SPA: IDD Coordinator (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAInventoryQARoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Inventory QA role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAInventoryQARoleId] ()
RETURNS int
AS
BEGIN
	RETURN 140;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOtherProjectBudgetTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the other project budget type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetOtherProjectBudgetTypeId]()
RETURNS int
AS
BEGIN
  RETURN 4; -- Other Project budget type id
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetExecutionBudgetTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the execution budget type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetExecutionBudgetTypeId]()
RETURNS int
AS
BEGIN
  RETURN 2; -- Execution budget type id
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDesignBudgetTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the design budget type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDesignBudgetTypeId]()
RETURNS int
AS
BEGIN
  RETURN 1; -- Design budget type id
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetContingencyBudgetTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the contingency budget type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetContingencyBudgetTypeId]()
RETURNS int
AS
BEGIN
  RETURN 3; -- Contingency budget type id
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSafetySystemJustificationQuestionnaireQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/09/2017 
-- Release:     8.3
-- Description: Returns the safety system ERI questionnaire question justification id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetSafetySystemJustificationQuestionnaireQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 106; --'What is the facility''s structural and safety system integrity?' Justification
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetContaminantsImpactJustificationQuestionnaireQuestionId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/09/2017 
-- Release:     8.3
-- Description: Returns the contaminants impact ERI question justification id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetContaminantsImpactJustificationQuestionnaireQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 107; --'What is the potential impact of contaminants?' Justification
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInactiveWorkBreakdownStructureStatusId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Retrieves the Inactive workbreakdownstructure
--   status id.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInactiveWorkBreakdownStructureStatusId] ()
RETURNS INT
AS
BEGIN
	RETURN 2; -- Inactive
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationPlanningReportId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     03/28/2016
-- Release:     7.0
-- Description: Get the Report ID for Budget Formulation Planning report.
--
-- mm/dd/yy  Name       Release  Description
-- 04/05/16  a.smith    7.0      Removed hard-coding of subsystem and added
--                               consideration of ActiveInd = 1.
-- 01/04/17  j.borgers  7.4		 Added FiscalYear parameter
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationPlanningReportId]
(
	@SubSystemId INT,
	@FiscalYear INT
)
RETURNS INT
AS
BEGIN
	DECLARE @ReportId INT = 
	(
		SELECT r.Id 
		FROM G2.Report r 
		WHERE r.Name = 'Budget Formulation Planning'
		AND r.ActiveInd = 1
		AND r.SubSystemId = @SubSystemId
		AND r.FiscalYear = @FiscalYear
	);

    RETURN @ReportId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnCheckWorkBreakdownStructureChangeAttributeUniqueOrMultiSelect]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     5/31/2016
-- Release:     7.1
-- Description: Checks to make sure the MultiSelectInd is set to true for a specified WorkBreakdownStructureAttribute
--              that does not have a unique (WorkBreakdownStructureId,WorkBreakdownStructureAttributeTypeId) tuple.
--              Returns 0 if MultiSelectInd is set, 1 if not.
--
-- mm/dd/yy  Name     Release  Description
-- 03/30/17  N.Harris 8.0      Added WorkBreakdownStructureAttributeTypeId to parameters
-- =============================================================
CREATE FUNCTION [G2].[fnCheckWorkBreakdownStructureChangeAttributeUniqueOrMultiSelect]
(
	@WorkBreakdownStructureChangeAttributeId INT,
	@WorkBreakdownStructureAttributeTypeId INT
)
RETURNS BIT
AS
BEGIN		

	DECLARE @returnValue BIT = 1;

	IF EXISTS(
		SELECT 1
		FROM G2.WorkBreakdownStructureChangeAttribute wbsca
		INNER JOIN G2.WorkBreakdownStructureAttributeType wbsat ON wbsat.Id = wbsca.WorkBreakdownStructureAttributeTypeId
		INNER JOIN G2.WorkBreakdownStructureChangeAttribute match ON match.Id = @WorkBreakdownStructureChangeAttributeId AND match.WorkBreakdownStructureChangeId = wbsca.WorkBreakdownStructureChangeId AND match.WorkBreakdownStructureAttributeTypeId = wbsca.WorkBreakdownStructureAttributeTypeId
		WHERE wbsat.MultiSelectInd = 0
		GROUP BY wbsca.WorkBreakdownStructureChangeId, wbsca.WorkBreakdownStructureAttributeTypeId
		HAVING COUNT(*) > 1
	)
	BEGIN
		SET @returnValue = 0;
	END

	RETURN @returnValue;

END;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSecondaryAssociatedSystemWorkBreakdownStructureAttributeTypeId]'
GO

-- =============================================================
-- Author:      Matt Morrell
-- Created:     06/10/2016 
-- Release:     7.1
-- Description: Returns the Secondary Associated System(s) project attribute type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSecondaryAssociatedSystemWorkBreakdownStructureAttributeTypeId]()
RETURNS int
AS
BEGIN
  RETURN 25; --Secondary Associated System(s)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTableTopExerciseCompletedMilestoneId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id Table Top Exercise by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTableTopExerciseCompletedMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'Table Top Exercise Completed'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetLabBudgetPOCRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/14/2014
-- Release:     5.0
-- Description: Retrieves the id of the Lab Budget POC role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetLabBudgetPOCRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 13; -- Lab Budget POC
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanSiteFinancialStakeholderRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/06/2015
-- Release:     6.3
-- Description: Returns the value for the FinPlan: Site Financial Stakeholder
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanSiteFinancialStakeholderRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'FinPlan: Site Financial Stakeholder'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRadProtectDomProjectsInitMilestoneId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id Radiological Protection
--               Domestic Projects Initiated by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRadProtectDomProjectsInitMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'Radiological Protection Domestic Projects Initiated'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetRankPermissionId]'
GO
-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/04/2015
-- Release:     6.4
-- Description: Returns the Rank Permission Id
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetRankPermissionId]()
RETURNS INT
AS
BEGIN
	RETURN 73; --Rank PermissionId
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetHQRankPermissionId]'
GO
-- =============================================================
-- Author:      Chad Gilbert
-- Created:     12/04/2015
-- Release:     6.4
-- Description: Returns the HQ Rank Permission Id
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetHQRankPermissionId]()
RETURNS INT
AS
BEGIN
	RETURN 74; --Rank PermissionId
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA24SubSystemId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/08/2017 
-- Release:     8.1
-- Description: Returns the NA-24 SubSystemId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetNA24SubSystemId]()
RETURNS INT
AS
BEGIN
  RETURN 8; --NA-24
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA23SubSystemId]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     06/09/2015
-- Release:     6.1
-- Description: Retrieves the NA-23 sub-system id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA23SubSystemId] ()
RETURNS int
AS
BEGIN
	RETURN 6; -- M3 aka MMM aka NA-23
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPerformerDistributionImportPermissionObjectId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     7/24/2015
-- Release:     6.1
-- Description: Retrieves the Performer Distribution Import permission object
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPerformerDistributionImportPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 135; -- Performer Distribution Import
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProjectsCompletedMilestoneId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id for projects completed by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProjectsCompletedMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'Projects Completed'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetReserveWorkBreakdownStructureAttributeTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/01/2014
-- Release:     5.3
-- Description: Retrieves the ADA Reserve WorkBreakdownStrucutre
--              Attribute type id.
--
-- mm/dd/yy  Name      Release  Description
-- 03/11/15  JCG       7.0      Smart rename of function
-- =============================================================
CREATE FUNCTION [G2].[fnGetReserveWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 18; -- Reserve
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInDeviceDelayKitsInstalledMilestoneId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id IDD Kits Installed by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInDeviceDelayKitsInstalledMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'In-Device Delay Kit(s) Installed'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetContactSiteMilestoneId]'
GO

-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id for contact site by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetContactSiteMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'Contact Site'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDeletedChangeRequestDetailStatusId]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     02/18/14
-- Release:     5.0
-- Description: Retrieves the Deleted change request detail status
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDeletedChangeRequestDetailStatusId]()
RETURNS INT
AS
BEGIN
	RETURN 10; -- Deleted
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPortfolioOfficerRoleId]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     1/29/2014
-- Release:     5.0
-- Description: Retrieves the Portfolio Officer role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPortfolioOfficerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 33; -- Portfolio Officer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOfficeLevelCostSummaryBNRGroupReportId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     02/02/2017
-- Release:     7.5
-- Description: Returns Office Level B&R Group Report ID for SubSystem and FiscalYear
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetOfficeLevelCostSummaryBNRGroupReportId](@SubSystemId INT, @FiscalYear INT)
RETURNS INT
AS
BEGIN
  DECLARE @ReportName VARCHAR(50) = 'Office Level Cost Summary B&R Group';
  DECLARE @ReportId INT = (
          SELECT Id 
          FROM G2.Report r 
          WHERE r.SubSystemId = @SubSystemId 
          AND (r.FiscalYear = @FiscalYear OR (@FiscalYear IS NULL AND r.FiscalYear IS NULL))
          AND r.Name= @ReportName
    );
    RETURN @ReportId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAlarmResponseTrainingCompletedMilestoneId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     9/29/15
-- Release:     6.3
-- Description: Returns milestone id Alarm Response Training by subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAlarmResponseTrainingCompletedMilestoneId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = G2.fnGetNA23SubSystemId();
	/* Debug Declarations */

	DECLARE @milestoneId int;

	SELECT @milestoneId = m.Id
	FROM G2.Milestone m
	WHERE m.Name = 'Alarm Response Training Completed'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneId;
	/* Debug Declarations */
	
	RETURN @milestoneId;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetChangeRequestStatusDate]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/20/2012
-- Release:     3.3
-- Description: Retrieves the max status date for the specified
--   change request.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetChangeRequestStatusDate]
(
	@ChangeRequestId int
)
RETURNS
datetime
AS
BEGIN

	DECLARE @statusDt datetime = NULL;

	SELECT @statusDt = MAX(crd.StatusDt)
	FROM G2.ChangeRequest cr
	INNER JOIN G2.ChangeRequestDetail crd ON cr.Id = crd.ChangeRequestId
	WHERE cr.Id = @ChangeRequestId;

	RETURN @statusDt;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleAdminRoleId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     8/10/2016
-- Release:     7.2
-- Description: Return RoleId for Schedule: Admin role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleAdminRoleId]()
RETURNS INT
AS
BEGIN
    RETURN 111;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleSecondaryReportingRoleId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/16/2014
-- Release:     5.3
-- Description: Retrieves the Secondary Reporting Role for Schedule
--              Configuration Type.  This is the secondary role responsible
--              for performance reporting for a given subsystem
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleSecondaryReportingRoleId] (@SubSystemId INT)
RETURNS INT
AS
BEGIN

/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
/* End Debug Declarations */

    DECLARE @scheduleConfigurationTypeId INT = G2.fnGetScheduleConfigurationTypeId(),
        @secondaryReportingRoleId INT;

    SET @secondaryReportingRoleId = (
									   SELECT   rm.secondaryRoleId
									   FROM     Report.PrimarySecondaryRoleMapping rm
									   WHERE    rm.SubsystemId = @SubSystemId
												AND rm.ConfigurationTypeId = @scheduleConfigurationTypeId
                                  );

    RETURN @secondaryReportingRoleId; 
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSchedulePrimaryReportingRoleId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/14/2014
-- Release:     5.3
-- Description: Retrieves the Primary Reporting Role for Schedule
--              Configuration Type.  This is the primary role responsible
--              for performance reporting for a given subsystem
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSchedulePrimaryReportingRoleId] (@SubSystemId INT)
RETURNS INT
AS
BEGIN

/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
/* End Debug Declarations */

    DECLARE @scheduleConfigurationTypeId INT = G2.fnGetScheduleConfigurationTypeId(),
        @primaryReportingRoleId INT;

    SET @primaryReportingRoleId = (
									   SELECT   rm.PrimaryRoleId
									   FROM     Report.PrimarySecondaryRoleMapping rm
									   WHERE    rm.SubsystemId = @SubSystemId
												AND rm.ConfigurationTypeId = @scheduleConfigurationTypeId
                                  );

    RETURN @primaryReportingRoleId; 
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubSystemIdFromChangeRequestDetailId]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     10/02/2014
-- Release:     5.0
-- Description: Retrieves the sub-system id for the specified
--   Change Request Detail Id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
create FUNCTION [G2].[fnGetSubSystemIdFromChangeRequestDetailId]
(
	@ChangeRequestDetailId int
)
RETURNS int
AS
BEGIN
	DECLARE @subSystemId int;

	SELECT @subSystemid = cr.SubSystemId 
	FROM G2.ChangeRequestDetail crd 
	INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId 
	WHERE crd.Id = @ChangeRequestDetailId;

	RETURN @subSystemId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveSourcesToUsMetricKPIReportRollupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Rollup ID for Remove Sources to U.S.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveSourcesToUsMetricKPIReportRollupId]()
RETURNS INT
AS
BEGIN
    RETURN 27;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveRtgsMetricKPIReportRollupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Rollup ID for Remove RTGs
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveRtgsMetricKPIReportRollupId]()
RETURNS INT
AS
BEGIN
    RETURN 25;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveOsrSourceConsolidationMetricKPIReportRollupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Rollup ID for Remove OSR/Source Consolidations
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveOsrSourceConsolidationMetricKPIReportRollupId]()
RETURNS INT
AS
BEGIN
    RETURN 26;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveDomesticRadJouleMetricKPIReportRollupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Rollup ID for Remove Domestic Rad Joule
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveDomesticRadJouleMetricKPIReportRollupId]()
RETURNS INT
AS
BEGIN
    RETURN 28;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRadRemoveMetricKPIReportRollupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Rollup ID for Rad Remove
--
-- mm/dd/yy  Name     Release  Description
-- 10/27/14  g.ogle   5.3      Renamed G2.fnGetRadRemoveRollupId to G2.fnGetRadRemoveMetricKPIReportRollupId
-- =============================================================
CREATE FUNCTION [G2].[fnGetRadRemoveMetricKPIReportRollupId]()
RETURNS INT
AS
BEGIN
    RETURN 24;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBetaGammaMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Metric ID for Beta-Gamma
--
-- mm/dd/yy  Name      Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetBetaGammaMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Beta-Gamma'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAlphaMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     10/21/2014
-- Release:     5.3
-- Description: Get the Metric ID for Alpha
--
-- mm/dd/yy  Name      Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetAlphaMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Alpha'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSourcesReturnedToUSMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the Source(s) Returned to US Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetSourcesReturnedToUSMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Source(s) Returned to US'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRTGsMetricId]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the RTGs Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetRTGsMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'RTGs'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     7/5/17
-- Release:     8.1
-- Description: gets Remove metric id
--   
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveMetricId]()
RETURNS INT
AS
BEGIN
	RETURN 62;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPuMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the Pu Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetPuMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Pu'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPlanningReportId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     08/01/2016
-- Release:     7.2
-- Description: Returns Planning Report ID for SubSystem and FiscalYear
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPlanningReportId](@SubSystemId INT, @FiscalYear INT)
RETURNS INT
AS
BEGIN
  DECLARE @ReportId INT = (
          SELECT Id 
          FROM G2.Report r 
          WHERE r.SubSystemId = @SubSystemId 
          AND r.FiscalYear = @FiscalYear 
          AND r.Name= 'Planning'
    );
    RETURN @ReportId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOrphanSourcesMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     9/24/2014
-- Release:     5.3
-- Description: Get Metric ID for Orphan sources
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetOrphanSourcesMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Orphan Sources'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetLEUMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the LEU Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetLEUMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'LEU'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInternalSourcesMetricId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     7/5/17
-- Release:     8.1
-- Description: gets Remove metric id
--   
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInternalSourcesMetricId]()
RETURNS INT
AS
BEGIN
	RETURN 63;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetHEUSNFMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the HEU SNF Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetHEUSNFMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'HEU SNF'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetHEUFreshMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the HEU Fresh Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetHEUFreshMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'HEU Fresh'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFuelAssembliesMetricId]'
GO

-- =============================================================
-- Author:      Greg Ogle
-- Created:     9/29/2014
-- Release:     5.3
-- Description: Returns Metric Id for Fuel Assemblies
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetFuelAssembliesMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Fuel Assemblies'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBuildingsMetricId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     10/23/2014
-- Release:     5.3
-- Description: Returns the Building(s) Metric Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/26/15  a.smith     6.2    Added @SubSystemId as parameter and modified
--                              to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetBuildingsMetricId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @metricId INT;

	SELECT @metricId = m.Id
	FROM G2.Metric m
	WHERE m.Name = 'Building(s)'
	AND m.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @metricId;
	/* Debug Declarations */

    RETURN @metricId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPrimaryAssociatedSystemWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the Primary Associated System(s) project attribute type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPrimaryAssociatedSystemWorkBreakdownStructureAttributeTypeId]()
RETURNS int
AS
BEGIN
  RETURN 23; --Primary Associated System(s)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInfrastructurePlanningScoreImportPermissionObjectId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:		08/11/2016
-- Release:     7.2
-- Description: Retrieves the Infrastructure Planning Score Import permission object
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInfrastructurePlanningScoreImportPermissionObjectId]()
RETURNS INT
AS
BEGIN
	RETURN 179; -- Infrastructure Planning Score Import
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveWorkTypeGroupId]'
GO
-- =============================================================
-- Author:      Jason Frank		
-- Created:     10/17/2014
-- Release:     5.3
-- Description: Returns the 'Remove' WorkTypeGroup Id
--
-- mm/dd/yy  Name     Release  Description
-- 08/12/15  a.smith    6.2    Added @SubSystemId parameter and 
--                             refactored to be multi-subsystem capable.
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveWorkTypeGroupId] 
(
	@SubSystemId INT
)
RETURNS INT
AS 
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @removalId INT;

	SELECT @removalId = wtg.Id 
	FROM G2.WorkTypeGroup wtg
	WHERE wtg.Name = 'Remove'
	AND wtg.SubSystemId = @SubSystemid;

	--SELECT @removalId;
	RETURN @removalId;
	--RETURN 2; -- Remove
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetGTRIProgram]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     09/05/13
-- Release:     4.4
-- Description: This function retrieves the GTRI Program Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetGTRIProgram]()
RETURNS
INT
AS
BEGIN
	
	RETURN 1;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSustainabilityMilestoneTemplateCategoryId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/3/2014
-- Release:     5.0
-- Description: Retrieves the Sustainability Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 09/29/15  b.roland 6.3      Updated for NA21 split
-- =============================================================
CREATE FUNCTION [G2].[fnGetSustainabilityMilestoneTemplateCategoryId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = 5;
	/* Debug Declarations */

	DECLARE @milestoneTemplateCategoryId int;

	SELECT @milestoneTemplateCategoryId = mtc.Id
	FROM G2.MilestoneTemplateCategory mtc
	WHERE mtc.Name = 'Sustainability'
	AND mtc.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneTemplateCategoryId;
	/* Debug Declarations */

	RETURN @milestoneTemplateCategoryId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectDomesticWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect Domestic work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectDomesticWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect Domestic';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectDomesticReactorWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect Domestic Reactor work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectDomesticReactorWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect Domestic Reactor';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFollowOnMilestoneTemplateCategoryId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/3/2014
-- Release:     5.0
-- Description: Retrieves the Follow-on Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- 09/29/15  b.roland 6.3      Updated for NA21 split
-- =============================================================
CREATE FUNCTION [G2].[fnGetFollowOnMilestoneTemplateCategoryId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId int = 5;
	/* Debug Declarations */

	DECLARE @milestoneTemplateCategoryId int;

	SELECT @milestoneTemplateCategoryId = mtc.Id
	FROM G2.MilestoneTemplateCategory mtc
	WHERE mtc.Name = 'Follow-On'
	AND mtc.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @milestoneTemplateCategoryId;
	/* Debug Declarations */

	RETURN @milestoneTemplateCategoryId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetLandPropertyTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     06/25/2018 
-- Release:     9.1
-- Description: Returns the Id for the Property Type of Land
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetLandPropertyTypeId]()
RETURNS int
AS
BEGIN
  RETURN 2; --Land
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFIMSAdminRoleId]'
GO

-- =============================================================
-- Author:      Byron Roland
-- Created:     11/25/15
-- Release:     6.4
-- Description: Returns FIMS: Admin role id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFIMSAdminRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 165; -- FIMS: Admin
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetReceiveEmailPermissionId]'
GO
-- =============================================================
-- Author:      David Prenshaw
-- Created:     5/10/2014
-- Release:     5.2
-- Description: Retrieves the Receive Email permission ID
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetReceiveEmailPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 52; -- Receive E-mail
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCopyOnEmailPermissionId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2014
-- Release:     5.3
-- Description: Retrieves the Copy on Email permission ID.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCopyOnEmailPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 67; -- Copy on E-mail
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetY12ReportingEntityOrganizationPartyId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     6/29/2017
-- Release:     8.1
-- Description: Returns Y-12 ReportingEntityOrganizationPartyId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetY12ReportingEntityOrganizationPartyId] (
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN

	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */

	DECLARE @organizationPartyId INT;

	SELECT @organizationPartyId = p.ReportingEntityOrganizationPartyId
	FROM Contact.Performer p
	WHERE p.DisplayName = 'Y-12'
	AND p.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @organizationPartyId;
	/* Debug Declarations */

    RETURN @organizationPartyId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPXReportingEntityOrganizationPartyId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     6/29/2017
-- Release:     8.1
-- Description: Returns PX ReportingEntityOrganizationPartyId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPXReportingEntityOrganizationPartyId] (
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN

	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */

	DECLARE @organizationPartyId INT;

	SELECT @organizationPartyId = p.ReportingEntityOrganizationPartyId
	FROM Contact.Performer p
	WHERE p.DisplayName = 'PX'
	AND p.SubSystemId = @SubSystemId;

	/* Debug Declarations */
	--SELECT @organizationPartyId;
	/* Debug Declarations */

    RETURN @organizationPartyId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPermissionObjectHID]'
GO


create function [Security].[fnGetPermissionObjectHID] (@parent_id integer) returns hierarchyid AS
BEGIN
    DECLARE @hid AS HIERARCHYID;
    DECLARE @parent_hid AS HIERARCHYID;
    DECLARE @last_child_hid AS HIERARCHYID;
                                                      
		  IF @parent_id IS NULL begin
			-- SET @hid = HIERARCHYID::GetRoot().GetDescendant(NULL,NULL)
			 SET @hid = HIERARCHYID::GetRoot().GetDescendant((select MAX(HID) 
															    from [Security].[PermissionObject]
															   where HID.GetAncestor(1) = hierarchyid::GetRoot()),NULL)
			 
			  end
		  ELSE BEGIN   					
   				  -- Need to create the hierarchy_id
		  		 SET @parent_hid = (SELECT HID
			   						  FROM [Security].[PermissionObject]
									 WHERE ID = @parent_id);
				 SET @last_child_hid = (SELECT MAX(HID) 
										  FROM [Security].[PermissionObject]
										 WHERE [HID].GetAncestor(1) = @parent_hid);								 
				 SET @hid = @parent_hid.GetDescendant(@last_child_hid, NULL);
         END
  RETURN(@hid)
  END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFireProtectionPermissionObjectId]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     08/12/2016
-- Release:     7.2
-- Description: Retrieves the Fire Protection PermissionObjectId
--   object id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFireProtectionPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 181; -- Fire Protection
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkBreakdownStructuresByDashboardFilters]'
GO

-- =============================================================
-- Author:      Chris Luttrell
-- Created:     5/31/2007
-- Description: Returns table of projects from the values from
--   the Dashboard Filter.
--
-- mm/dd/yy  Name     Release  Description
-- 02/23/09  s.oneal           Modified query to handle Region/State filters
-- 07/29/09  DNC               Added Portfolio option
-- 02/17/10  s.oneal           Modified to use Building Ext table instead of state site xref table
-- 12/02/10  m.brown  2.0      Modified to accommodate the new Building/Site schema changes
-- 01/13/11  s.oneal  2.0      Fixed site/building stuff that wasn't completely fixed
-- 04/25/13  s.oneal  4.1      Removed NAFD references
-- 11/12/14  s.oneal  5.3      Renamed and modified to use the new schedule data model
-- =============================================================
CREATE FUNCTION [G2].[fnGetWorkBreakdownStructuresByDashboardFilters]
(
	@Id int,
	@Type varchar(50),
	@WorkTypeIdTable G2.GenericUniqueIdTable READONLY,
	@StartDt datetime,
	@EndDt datetime,
	@IgnoreDatesInd bit
)
RETURNS @WorkBreakdownStructures TABLE (Id int NOT NULL, WorkTypeId int NULL)
AS
BEGIN
	--******************************DEBUG******************************
	--DECLARE @Id int, @Type varchar(50), @WorkTypeIdTable G2.GenericUniqueIdTable, @StartDt datetime, @EndDt datetime, @IgnoreDatesInd bit = 1;

	--SET @Id = 1;
	--SET @Type = 'Program';

	--SET @Id = 2;
	--SET @Type = 'Office';

	--SET @Id = 1;
	--SET @Type = 'Country';

	--SET @Id = 6;
	--SET @Type = 'Portfolio';

	--SET @Id = 1;
	--SET @Type = 'Region';

	--SET @Id = 50;
	--SET @Type = 'State';

	--SET @Id = 184;
	--SET @Type = 'Site';

	--SET @Id = 62;
	--SET @Type = 'Building';

	--SET @Id = 775;
	--SET @Type = 'Project';

	--INSERT INTO @WorkTypeIdTable
	--SELECT wt.Id
	--FROM G2.WorkType wt
	--INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	--WHERE wtg.SubSystemId = 1
	--AND wt.DashboardFilterInd = 1;

	--SET @StartDt = '10/1/2009'
	--SET @EndDt = '9/30/2010'

	--SET @StartDt = '10/1/2010'
	--SET @EndDt = '9/30/2011'

	--SET @StartDt = '10/1/2012'
	--SET @EndDt = '9/30/2013'

	--SET @StartDt = '10/1/2020'
	--SET @EndDt = '9/30/2021'
	--******************************DEBUG******************************

	DECLARE @subSystemId int = G2.fnGetNA21SubSystemId(),
		@allWorkTypes bit,
		@nuclearWorkTypeCategoryId int;

	SELECT @Id = COALESCE(@Id, Id) FROM G2.WorkBreakdownStructure WHERE @Id IS NULL AND SubSystemId = @subSystemId AND ParentId IS NULL;
	SELECT @StartDt = COALESCE(@StartDt, MIN(BeginDt)) FROM G2.Period WHERE @StartDt IS NULL;
	SELECT @EndDt = G2.fnDayEnd(COALESCE(@EndDt, MAX(EndDt))) FROM G2.Period WHERE @EndDt IS NULL;
	SELECT @nuclearWorkTypeCategoryId = wtc.Id FROM G2.WorkTypeCategory wtc WHERE wtc.SubSystemId = @subSystemId AND wtc.Name = 'Nuclear';

	IF (@Type = 'Country')
	BEGIN
		SELECT @Id = wbscx.WorkBreakdownStructureId
		FROM G2.WorkBreakdownStructureCountryXref wbscx
		INNER JOIN G2.WorkBreakdownStructure w on wbscx.WorkBreakdownStructureId = w.Id
		INNER JOIN G2.SubSystemLevel ssl ON w.SubSystemLevelId = ssl.Id
		WHERE @Type = 'Country'
		AND ssl.Label = 'Portfolio'
		AND wbscx.CountryId = @Id;
	END

	SET @Type = CASE WHEN @Type = 'World' THEN 'Program' ELSE @Type END;
	SET @Type = CASE WHEN @Type = 'Country' THEN 'Portfolio' ELSE @Type END;

	SELECT @allWorkTypes = ~CONVERT(bit, COUNT(*))
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	LEFT OUTER JOIN @WorkTypeIdTable wti ON wt.Id = wti.Id
	WHERE wtg.SubSystemId = @subSystemId
	AND wt.DashboardFilterInd = 1
	AND wti.Id IS NULL;

	WITH FilteredTasks
	AS
	(
		SELECT w.Id AS WorkBreakdownStructureId, wtx.WorkTypeId
		FROM G2.Task t
		INNER JOIN G2.WorkBreakdownStructure tw ON t.WorkBreakdownStructureId = tw.Id
		INNER JOIN G2.WorkBreakdownStructure w ON tw.ParentId = w.Id
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wtx ON w.Id = wtx.WorkBreakdownStructureId
		INNER JOIN G2.WorkType wt ON wtx.WorkTypeId = wt.Id
		INNER JOIN G2.TaskBuilding tb ON t.Id = tb.TaskId
		INNER JOIN G2.Building b ON tb.BuildingId = b.Id
		LEFT OUTER JOIN Inventory.RegionStateXRef rsxr ON b.StateId = rsxr.StateId
		WHERE
		(
			(@Type = 'Region' AND rsxr.RegionId = @Id)
			OR
			(@Type = 'State' AND b.StateId = @Id)
			OR
			(@Type = 'Site' AND b.SiteId = @Id AND wt.WorkTypeCategoryId = @nuclearWorkTypeCategoryId)
			OR
			(@Type = 'Building' AND b.Id = @Id AND wt.WorkTypeCategoryId = @nuclearWorkTypeCategoryId)
		)
	--) SELECT * FROM FilteredTasks
	),
	FilteredWorkBreakdownStructures
	AS
	(
		SELECT w.Id AS WorkBreakdownStructureId
		FROM G2.WorkBreakdownStructure w
		INNER JOIN G2.SubSystemLevel ssl ON w.SubSystemLevelId = ssl.Id
		WHERE w.Id = @Id
		AND ssl.Label = @Type

		UNION ALL

		SELECT w.Id AS WorkBreakdownStructureId
		FROM FilteredWorkBreakdownStructures fw
		INNER JOIN G2.WorkBreakdownStructure w ON fw.WorkBreakdownStructureId = w.ParentId
		INNER JOIN G2.SubSystemLevel ssl ON w.SubSystemLevelId = ssl.Id
		WHERE ssl.Label <> 'Task'
	--) SELECT * FROM FilteredWorkBreakdownStructures
	),
	CombinedWorkBreakdownStructures
	AS
	(
		SELECT ft.WorkBreakdownStructureId, ft.WorkTypeId
		FROM FilteredTasks ft
		LEFT OUTER JOIN @WorkTypeIdTable wt ON ft.WorkTypeId = wt.Id
		WHERE (@allWorkTypes = 1 OR wt.Id IS NOT NULL)
		AND @Type IN ('Region', 'State', 'Site', 'Building')

		UNION

		SELECT fw.WorkBreakdownStructureId, wtx.WorkTypeId
		FROM FilteredWorkBreakdownStructures fw
		INNER JOIN G2.WorkBreakdownStructureWorkTypeXref wtx ON fw.WorkBreakdownStructureId = wtx.WorkBreakdownStructureId
		LEFT OUTER JOIN @WorkTypeIdTable wt ON wtx.WorkTypeId = wt.Id
		WHERE (@allWorkTypes = 1 OR wt.Id IS NOT NULL)
	)

	INSERT INTO @WorkBreakdownStructures
	SELECT w.Id, wt.Id AS WorkTypeId
	FROM CombinedWorkBreakdownStructures c
	INNER JOIN G2.WorkBreakdownStructure w ON c.WorkBreakdownStructureId = w.Id
	INNER JOIN G2.Schedule s ON w.Id = s.WorkBreakdownStructureId
	LEFT OUTER JOIN @WorkTypeIdTable wt ON c.WorkTypeId = wt.Id
	WHERE (@allWorkTypes = 1 OR wt.Id IS NOT NULL)
	AND
	(
		(@IgnoreDatesInd = 0 AND s.StartDt <= @EndDt AND s.FinishDt >= @StartDt)
		OR
		(@IgnoreDatesInd = 1)
	);

	RETURN;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSpendPlanChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/13/2014 
-- Release:     5.0
-- Description: Returns the Spend Plan change request type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSpendPlanChangeRequestTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 22; --Spend Plan
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBuildingTagData]'
GO

-- =============================================
-- Author:		Mike Strickland
-- Create date: 05/31/2012
-- Release:     3.3
-- Description:	Returns multiple building tag names and values for a building as a single semicolon-separated string.
--	
-- mm/dd/yyyy   Name        Release  Description 
-- 07/31/2012   Strickland  3.4      Changed StrVal to Comment
--                                   Removed IntVal and BitVal
-- =============================================
CREATE FUNCTION [G2].[fnGetBuildingTagData] (
	@BuildingId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE 
		@BuildingTagNames VARCHAR(1000),
		@BuildingTagName VARCHAR(50),
		@BuildingTagComment VARCHAR(255)
		
	DECLARE BuildingTagCursor CURSOR LOCAL FORWARD_ONLY READ_ONLY
		FOR
		SELECT bt.Name, x.Comment
		FROM G2.BuildingBuildingTagXref x
		INNER JOIN G2.BuildingTag bt ON bt.Id = x.BuildingTagId
		WHERE x.BuildingId = @BuildingId

	SET @BuildingTagNames = ''
	
	OPEN BuildingTagCursor

	FETCH NEXT FROM BuildingTagCursor INTO @BuildingTagName, @BuildingTagComment--, @BuildingTagIntVal, @BuildingTagBitVal
		
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @BuildingTagNames = @BuildingTagNames +  
			@BuildingTagName + ':' +
			CASE WHEN @BuildingTagComment IS NOT NULL THEN REPLACE(REPLACE(@BuildingTagComment, ';', '|semicolon|'), ':', '|colon|') ELSE 'Yes' END +
			';'

		FETCH NEXT FROM BuildingTagCursor INTO @BuildingTagName, @BuildingTagComment--, @BuildingTagIntVal, @BuildingTagBitVal
	END
	
	IF LEN(@BuildingTagNames) > 0
		BEGIN
			-- Remove trailing semicolon and space
			SET @BuildingTagNames = SUBSTRING(@BuildingTagNames, 1, LEN(@BuildingTagNames) - 1)
		END
	
	CLOSE BuildingTagCursor
	DEALLOCATE BuildingTagCursor

	RETURN @BuildingTagNames
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCloseProcessPermissionId]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     6/8/2017
-- Release:     8.1
-- Description: Returns the close process permission id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCloseProcessPermissionId]()
RETURNS int
AS
BEGIN
	RETURN 34; -- Close Process
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnCheckConfigurationGroupSubSystemLevelHasOneDefault]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     2/06/2017
-- Release:     7.5
-- Description: Enforces that we have one configuration group selected 
--				as the default for every SubSystem level that has groups.
--
-- mm/dd/yy  Name     Release  Description
-- 03/30/17  N.Harris 8.0	   Added DefaultInd to parameters
-- =============================================================
CREATE FUNCTION [G2].[fnCheckConfigurationGroupSubSystemLevelHasOneDefault]
(
	@SubSystemLevelId INT,
	@DefaultInd BIT
)
RETURNS INT
AS 
BEGIN
	DECLARE @valid INT = 1;

	SELECT @valid = 0
	FROM G2.ConfigurationGroup cg
	WHERE cg.SubSystemLevelId = @SubSystemLevelId
	AND cg.DefaultInd = 1
	HAVING COUNT(*) <> 1;

	RETURN @valid;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPeriodEnd]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/15/2013
-- Release:     4.1
-- Description: Retrieves the end date for the specified period.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPeriodEnd] (
	@PeriodId int
)
RETURNS datetime
AS
BEGIN
	DECLARE @dt datetime;
	SELECT @dt = EndDt FROM G2.Period prd WHERE prd.Id = @PeriodId;
	RETURN @dt;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSuperSiteWorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     09/30/2014
-- Release:     5.3
-- Description: Retrieves the Super Site work type id.
--
-- mm/dd/yy  Name      Release  Description
-- 08/27/15  a.smith     6.2    Added parameter to function and 
--                              refactored for multi-subsystem.
-- =============================================================
CREATE FUNCTION [G2].[fnGetSuperSiteWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Super Site';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Super Site / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentFundingThruPeriodParmKey]'
GO

-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     08/25/2015
-- Release:     6.2
-- Description: Retrieves the Funding Thru Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentFundingThruPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Funding Thru Period';
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAddChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Add change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAddChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 13; -- Add
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSchedulePortfolioManagerSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the Schedule: Portfolio Manager (Secondary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSchedulePortfolioManagerSecondaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'Schedule: Portfolio Manager (Secondary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSchedulePortfolioManagerPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the Schedule: Portfolio Manager (Primary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSchedulePortfolioManagerPrimaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'Schedule: Portfolio Manager (Primary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanPortfolioManagerSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Portfolio Manager (Secondary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanPortfolioManagerSecondaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'FinPlan: Portfolio Manager (Secondary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanPortfolioManagerPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/31/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Portfolio Manager (Primary)
--              Role Id for the given subsystem id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanPortfolioManagerPrimaryRoleId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @roleId INT;
	
	SELECT @roleId = r.Id
	FROM Security.Role r
	INNER JOIN Security.SubSystemRole ssr ON r.Id = ssr.RoleId
	WHERE r.Name = 'FinPlan: Portfolio Manager (Primary)'
	AND ssr.SubSystemId = @SubSystemId;

	/* Debug */
	--SELECT @roleId;
	/* Debug */

	RETURN @roleId;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanHQFinancialOfficerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     02/17/2017
-- Release:     7.5
-- Description: Returns the value for the FinPlan: HQ Financial Officer (Secondary)
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanHQFinancialOfficerSecondaryRoleId]()
RETURNS int
AS
BEGIN
	RETURN 91;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanHQFinancialOfficerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     02/17/2017
-- Release:     7.5
-- Description: Returns the value for the FinPlan: HQ Financial Officer (Primary)
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanHQFinancialOfficerPrimaryRoleId]()
RETURNS int
AS
BEGIN
	RETURN 90;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCostsHQFinancialOfficerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     02/17/2017
-- Release:     7.5
-- Description: Returns the value for the Costs: HQ Financial Officer (Secondary)
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCostsHQFinancialOfficerSecondaryRoleId]()
RETURNS int
AS
BEGIN
	RETURN 96;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCostsHQFinancialOfficerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     02/17/2017
-- Release:     7.5
-- Description: Returns the value for the Costs: HQ Financial Officer (Primary)
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCostsHQFinancialOfficerPrimaryRoleId]()
RETURNS int
AS
BEGIN
	RETURN 95;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingValidationMethodParmKey]'
GO

-- =============================================================
-- Author:      Nathan Coffey
-- Created:     2/21/2017
-- Release:     7.5
-- Description: Return the funding validation method parmkey
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingValidationMethodParmKey]
(
)
RETURNS VARCHAR(50)
AS
BEGIN
	RETURN 'Funding Validation Method';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAdvanceWorkflowHID]'
GO

-- =============================================================
-- Author:      Robin Auerbach
-- Create date: 11/09/2011
-- Description: Returns the hierarchyID data-type with the corresponding hierarchy
--
--
-- Change Log History
-- mm/dd/yy  Name     Release  Description
-- 
-- =============================================================
 
Create function [G2].[fnGetAdvanceWorkflowHID] (@parent_id integer) returns hierarchyid AS
BEGIN
    DECLARE @hid AS HIERARCHYID;
    DECLARE @parent_hid AS HIERARCHYID;
    DECLARE @last_child_hid AS HIERARCHYID;
                                                      
		  IF @parent_id IS NULL begin
			-- SET @hid = HIERARCHYID::GetRoot().GetDescendant(NULL,NULL)
			 SET @hid = HIERARCHYID::GetRoot().GetDescendant((select MAX(HID) 
															    from [G2].[AdvanceWorkflow]
															   where HID.GetAncestor(1) = hierarchyid::GetRoot()),NULL)
			 
			  end
		  ELSE BEGIN   					
   				  -- Need to create the hierarchy_id
		  		 SET @parent_hid = (SELECT HID
			   						  FROM [G2].[AdvanceWorkflow]
									 WHERE ID = @parent_id);
				 SET @last_child_hid = (SELECT MAX(HID) 
										  FROM [G2].[AdvanceWorkflow]
										 WHERE [HID].GetAncestor(1) = @parent_hid);								 
				 SET @hid = @parent_hid.GetDescendant(@last_child_hid, NULL);
         END
  RETURN(@hid)
  END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsConfigurationTypeInWorkBreakdownStructureTree]'
GO
-- =============================================
-- Author:		Marshall Lee
-- Create date: 11/23/15
-- Description:	Looks for any ancestor or descendant of a 
--				WorkBreakdownStructure that has the provided Configuration Type.
--
-- mm/dd/yy  Name      Release  Description
-- 12/09/15  DGP       6.4      Modified to skip looking down the tree if the WorkBreakdownStructure is a parent
-- 12/14/15  Marshall  6.4		Modified to not look at passed in WBS
-- =============================================
CREATE FUNCTION [G2].[fnIsConfigurationTypeInWorkBreakdownStructureTree]
(
	@WorkBreakdownStructureId INT,
	@ParentInd BIT,
	@ConfigurationTypeId INT
)
RETURNS BIT
AS
BEGIN

DECLARE @configurationTypeExistsInTree BIT = 0;

SELECT TOP 1 @configurationTypeExistsInTree = 1
FROM (
	--Walking up the tree
	SELECT pwbs.Id
	FROM G2.WorkBreakdownStructure pwbs
	INNER JOIN G2.WorkBreakdownStructureHierarchy pwbsh ON pwbsh.WorkBreakdownStructureId = pwbs.Id
	INNER JOIN G2.WorkBreakdownStructureHierarchy cwbsh ON cwbsh.WorkBreakdownStructureHierarchyId.IsDescendantOf(pwbsh.WorkBreakdownStructureHierarchyId) = 1 AND (pwbs.Id <> @WorkBreakdownStructureId OR @ParentInd = 1)
	INNER JOIN G2.WorkBreakdownStructure cwbs ON cwbsh.WorkBreakdownStructureId = cwbs.Id
	INNER JOIN G2.vwWorkBreakdownStructureStatusActiveStates awbs ON awbs.WorkBreakdownStructureStatusId = cwbs.StatusId
	LEFT OUTER JOIN G2.WorkBreakdownStructureConfiguration wbsc 
		INNER JOIN G2.ConfigurationType ct ON ct.Id = wbsc.ConfigurationTypeId
	ON wbsc.WorkBreakdownStructureId = pwbs.Id
	WHERE cwbs.Id = @WorkBreakdownStructureId
	AND ct.Id = @ConfigurationTypeId
	
	UNION 

	--Walking down the tree
	SELECT pwbs.Id
	FROM G2.WorkBreakdownStructure pwbs
	INNER JOIN G2.WorkBreakdownStructureHierarchy pwbsh ON pwbsh.WorkBreakdownStructureId = pwbs.Id
	INNER JOIN G2.WorkBreakdownStructureHierarchy cwbsh ON pwbsh.WorkBreakdownStructureHierarchyId.IsDescendantOf(cwbsh.WorkBreakdownStructureHierarchyId) = 1 AND (pwbs.Id <> @WorkBreakdownStructureId OR @ParentInd = 1)
	INNER JOIN G2.WorkBreakdownStructure cwbs ON cwbsh.WorkBreakdownStructureId = cwbs.Id
	INNER JOIN G2.vwWorkBreakdownStructureStatusActiveStates awbs ON awbs.WorkBreakdownStructureStatusId = cwbs.StatusId
	LEFT OUTER JOIN G2.WorkBreakdownStructureConfiguration wbsc 
		INNER JOIN G2.ConfigurationType ct ON ct.Id = wbsc.ConfigurationTypeId
	ON wbsc.WorkBreakdownStructureId = pwbs.Id
	WHERE cwbs.Id = @WorkBreakdownStructureId
	AND ct.Id = @ConfigurationTypeId
	AND @ParentInd = 0
)configurationTypeExists

RETURN @configurationTypeExistsInTree;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationGroupConfigurationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     10/4/2017
-- Release:     8.3
-- Description: Returns the Id for the Budget Formulation Group ConfigurationType
-- =============================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationGroupConfigurationTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 10; --Budget Formulation Group
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationConfigurationTypeId]'
GO
-- =============================================
-- Author:		Marshall Lee
-- Create date: 11/17/2015
-- Release:     6.4
--
-- Description:	Returns the Id for the Budget Formulation ConfigurationType
-- =============================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationConfigurationTypeId]()
RETURNS int
AS
BEGIN
	RETURN 7; --Budget Formulation
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnMonthEnd]'
GO
-- =============================================
-- Author:		Chris Luttrell
-- Create date: 8/23/06
-- Description:	Returns the last second of the last day of the month based on the date passed in
-- =============================================
CREATE FUNCTION [G2].[fnMonthEnd] 
(

	@theDay datetime = null
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	set @theDay = dateadd(m, datediff(m,0,@theDay)+1,0)
	set @theDay = DATEADD(ms,-3,@theDay)


	RETURN @theDay

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA50ExecutiveSummaryPackageReportIdReportId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     08/10/2016
-- Release:     7.2
-- Description: Get ReportId for NA-50 Executive Summary Package
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA50ExecutiveSummaryPackageReportIdReportId]
(
	@SubSystemId INT,
    @FiscalYear INT
)
RETURNS INT
AS
BEGIN
    
	DECLARE @ReportId INT = 
	(
		SELECT r.Id FROM G2.Report r 
		WHERE r.Name = 'NA-50 Executive Summary Package'
		AND r.ActiveInd = 1
		AND r.SubSystemId = @SubSystemId
        AND r.FiscalYear = @FiscalYear
	);
    RETURN @ReportId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPlanningProjectPlanningManager]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/29/2016 
-- Release:     7.3
-- Description: Returns the Planning: Project Planning Manager
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Security].[fnGetPlanningProjectPlanningManager]()
RETURNS int
AS
BEGIN
  RETURN 176; --Planning: Project Planning Manager
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInfrastructurePlanningScenarioPermissionObjectId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/29/2016 
-- Release:     7.3
-- Description: Returns the Infrastructure Planning Scenario permission object Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInfrastructurePlanningScenarioPermissionObjectId]()
RETURNS int
AS
BEGIN
  RETURN 177; --Infrastructure Planning Scenario
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConvertWorkTypeGroupId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     10/26/15
-- Release:     6.3
-- Description: Returns the convert work type group id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetConvertWorkTypeGroupId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */

	DECLARE @ConvertWorkTypeGroupId int;

	SELECT @ConvertWorkTypeGroupId = wtg.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wtg.Name = 'Convert';

	/* Debug Declarations */
	--SELECT @WorkTypeGroupId;
	/* Debug Declarations */

	RETURN @ConvertWorkTypeGroupId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDeferredMaintenanceReducedWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the Deferred maintenance project attribute type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDeferredMaintenanceReducedWorkBreakdownStructureAttributeTypeId]()
RETURNS int
AS
BEGIN
  RETURN 24; --Deferred Maintenance Reduced Attribute
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCostEstimateWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/14/2016 
-- Release:     7.1
-- Description: Returns the Cost estimate project attribute type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCostEstimateWorkBreakdownStructureAttributeTypeId]()
RETURNS int
AS
BEGIN
  RETURN 21; --Cost Estimate Attribute
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApproveStartOperationsCriticalDecisionTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/15/2016 
-- Release:     7.1
-- Description: Returns the approve start of operations critical decision id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApproveStartOperationsCriticalDecisionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 5; --approve start of operations  critical decision type id - CD4
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApproveStartConstructionCriticalDecisionTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/15/2016 
-- Release:     7.1
-- Description: Returns the approve start construction critical decision id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApproveStartConstructionCriticalDecisionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 4; --approve start construction critical decision type id - CD3
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApprovePerformanceBaselineCriticalDecisionTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/15/2016 
-- Release:     7.1
-- Description: Returns the approve performance baseline critical decision id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApprovePerformanceBaselineCriticalDecisionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 3; --approve performance baseline critical decision type id - CD2
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApproveMissionNeedCriticalDecisionTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/15/2016 
-- Release:     7.1
-- Description: Returns the approve mIssion need critical decision type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApproveMissionNeedCriticalDecisionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 1; -- Approve mission need critical decision type id - CD0
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetApproveAlternativeSelectionCriticalDecisionTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     06/15/2016 
-- Release:     7.1
-- Description: Returns the approve alternative selection critical decision id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetApproveAlternativeSelectionCriticalDecisionTypeId]()
RETURNS int
AS
BEGIN
  RETURN 2; -- approve alternative selection critical decision type id - CD1
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetElementsofCostImportPermissionObjectId]'
GO


-- =============================================================
-- Author:      David Prenshaw
-- Created:     6/2/2016
-- Release:     7.1
-- Description: Retrieves the Elements of Cost Import permission object
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetElementsofCostImportPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 173; -- Elements of Cost Export/Import
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetScopeMilestoneDateSpread]'
GO

-- =============================================================
-- Author:      Robert Waltz
-- Created:     6/23/2009
-- Description: 
-- pass in milestone template id and return a list of milestones
-- with dates calculated along with a bunch of other information
-- needed to populate projectmilestone table
--  Tables affected:
--		G2.ProjectMilestone
--
-- Dependency:  Any call of this function MUST MUST MUST FIRST 
-- 'SET DATEFIRST 6'
-- otherwise the results will be totally wrong, so BEWARE!!!!
--
-- mm/dd/yy  Name     StoryId  Description
-- 07/06/09  s.oneal           Modified to not move base date into different FY
-- 07/22/09  rpw               Modified to make Saturday the first day of the week, and
--                             corrected logic concerning how to determine weekdays
-- 07/19/10  cgl & sno         simplified logic for determining weekdays/ends to shift dates when needed
-- 10/15/10  kia               modified to use function fnOffsetBusinessDays to figure out days offset; there was a problem involving weekends. 
-- 07/25/11  cbd/dnc		   Exlude optional milestones from date spread
-- 05/14/14  j.borgers  5.1    Added SequenceNumber
-- =============================================================
CREATE FUNCTION [G2].[fnGetScopeMilestoneDateSpread]
(
	@MilestoneTemplateId int,
	@JouleDate datetime
)
RETURNS 
@ScopeMilestoneDateSpread TABLE (MilestoneId int, SequenceNumber int, MilestoneDt datetime, BaseScheduleOffsetInd bit)
AS
BEGIN
--	DECLARE @MilestoneTemplateId INT;
--	DECLARE @JouleDate DATETIME;
--	DECLARE @ScopeMilestoneDateSpread TABLE (MilestoneId int, MilestoneDt datetime, BaseScheduleOffsetInd bit);

	--SET @MilestoneTemplateId = 25;
	--SET @JouleDate = '9/30/2015';
	--SET @JouleDate = '10/1/2012';
	--SET @JouleDate = '10/1/2011';
	--SET @JouleDate = '10/1/2021';
	--SET @JouleDate = '9/30/2011';
	--SET @JouleDate = '9/30/2017';
	--SET @JouleDate = '9/29/2017';
	INSERT INTO @ScopeMilestoneDateSpread
	SELECT M.Id as MilestoneId, mtd.SequenceNumber,
				G2.fnOffsetBusinessDays(DATEADD(WEEK, WEEKOFFSET, DATEADD(MONTH, MONTHOFFSET, DATEADD(YEAR, YEAROFFSET, @JouleDate))), DAYOFFSET) AS MilestoneDate,
		BaseScheduleOffsetInd
	FROM G2.MilestoneTemplate MT
	INNER JOIN G2.MilestoneTemplateDetail MTD on MT.id = MTD.MilestoneTemplateId
	INNER JOIN G2.Milestone M on MTD.MilestoneId = M.id
	WHERE MT.id = @MilestoneTemplateId
		AND M.OptionalInd = 0
	ORDER BY MTD.DisplayOrder;


	-- This updates the BaseScheduleOffsetInd milestone date if it has been moved to a different
	-- fiscal year from what was passed in; the only reason this date would get moved would be if
	-- the date provided is on a weekend and would be moved to the next Monday, therefore, the date
	-- can be moved back by 3 days to move it to Friday instead, which should be in the same FY
	UPDATE @ScopeMilestoneDateSpread SET MilestoneDt=DATEADD(DAY, -3, ds.MilestoneDt)
	--SELECT DATEADD(DAY, -3, ds.MilestoneDt), ds.MilestoneDt, G2.fnGetFiscalYearFromDate(ds.MilestoneDt), @JouleDate, G2.fnGetFiscalYearFromDate(@JouleDate)
	FROM @ScopeMilestoneDateSpread ds
	WHERE ds.BaseScheduleOffsetInd = 1
	AND G2.fnGetFiscalYearFromDate(ds.MilestoneDt) <> G2.fnGetFiscalYearFromDate(@JouleDate)
	
	RETURN;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetRejectPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/19/2013
-- Release:     4.4
-- Description: Retrieves the Reject permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetRejectPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 25; -- Reject
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectInternationalWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect International work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectInternationalWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect International';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectInternationalReactorWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect International Reactor work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectInternationalReactorWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect International Reactor';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPrimaryFilterSubSystemLevelId]'
GO

-- =============================================================
-- Author:      Jason Frank
-- Created:     10/23/2014 
-- Release:     5.3
-- Description: Returns the SubsystemLevel.Id associated with the standard
--              primary filter type for the passed-in subsystem.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPrimaryFilterSubSystemLevelId] (@SubsystemId INT)
RETURNS INT
AS
BEGIN
	DECLARE @subsystemLevelId INT,
			@standardPrimaryFilterTypeId INT = G2.fnGetStandardPrimaryFilterTypeId();

	SELECT @subsystemLevelId = ssl.Id
	FROM G2.FilterType ft
	INNER JOIN G2.FilterTypeConfiguration sslftx ON sslftx.FilterTypeId = ft.Id
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = sslftx.SubSystemLevelId 
	WHERE ssl.SubSystemId = @SubsystemId
	AND ft.Id = @standardPrimaryFilterTypeId;

	RETURN @subsystemLevelId;
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetMassSubmitPermissionId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     11/5/2014
-- Release:     5.3
-- Description: Return Permission ID for Mass Submit
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetMassSubmitPermissionId]()
RETURNS INT
AS
BEGIN
  RETURN 22;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetWorkBreakdownStructureChangeRequestTypeGroupId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     05/23/2016 
-- Release:     7.1
-- Description: Returns the Work Breakdown Structure ChangeRequestTypeGroup Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetWorkBreakdownStructureChangeRequestTypeGroupId]()
RETURNS int
AS
BEGIN
  RETURN 17; --Work Breakdown Structure
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCourtesyConcurPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     1/20/2014
-- Release:     5.0
-- Description: Retrieves the courtesy concur permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCourtesyConcurPermissionId]()
RETURNS int
AS
BEGIN
	RETURN 16; -- Courtesy Concur
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnMetricExceptions]'
GO
-- =============================================================
-- Author:      Christopher Luttrell
-- Created:     ?
-- Release:     ?
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- 01/31/13  CGL      4.0      Removing ProjectId and MilestoneTemplateId from ProjectMilestone table
-- =============================================================
CREATE FUNCTION [G2].[fnMetricExceptions]
(
	@ProjectId int,
	@ChangeRequestDetailId int
)
RETURNS @ExceptionList TABLE
(
	ProjectId int,
	TaskId int,
	ExceptionType varchar(50)
)
AS
BEGIN
--	DECLARE @ChangeRequestDetailId int;
--	DECLARE @ProjectId int;
--	DECLARE @StatusDt datetime;
	
	--Filling in the project id if one wasn't provided, but a changerequestdetail id was
	SELECT @ProjectId=cr.ProjectId FROM G2.ChangeRequestDetail crd INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id WHERE @ProjectId IS NULL AND crd.Id=@ChangeRequestDetailId AND crd.ChangeRequestTypeId = 15 --Status

	INSERT INTO @ExceptionList
		SELECT DISTINCT T.ProjectId, pm.TaskId, 'Red' AS ExceptionType
		FROM G2.ProjectMilestone pm
		INNER JOIN G2.Task T ON pm.TaskId = T.Id
		INNER JOIN G2.ProjectMilestoneMetric pmm ON pm.Id = pmm.ProjectMilestoneId
		WHERE
		(
			T.ProjectId = COALESCE(@ProjectId, T.ProjectId)
			AND pmm.BaselineValue <> pmm.ForecastValue
		)

	RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnConcatChangeRequestDetailConcurrenceRoles]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     
-- Description: Concatenates all roles for which the concurrence
--   record was created for.
--
-- mm/dd/yy  Name     Release  Description
-- 01/11/12  s.oneal  3.0      Modified to use the new security tables
-- =============================================================
CREATE FUNCTION [G2].[fnConcatChangeRequestDetailConcurrenceRoles]
(
	@ChangeRequestDetailConcurrenceId int,
	@delimeter varchar(10)
)
RETURNS varchar(250)
AS
BEGIN
	DECLARE @roles varchar(250);
 
	SELECT @roles = COALESCE(@roles + @delimeter, '') + r.Abbreviation
	FROM G2.ChangeRequestDetailConcurrenceRole c
	INNER JOIN Security.Role r ON c.RoleId = r.Id
	WHERE c.ChangeRequestDetailConcurrenceId = @ChangeRequestDetailConcurrenceId
 
	RETURN @roles;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInternationalContributionsParentBNRGroupId]'
GO
-- =============================================================
-- Author:      Greg Ogle
-- Created:     03/01/2017
-- Release:     7.5
-- Description: Returns SubSystemBNRGroup ID for International Contributions
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInternationalContributionsParentBNRGroupId](@ReportId INT)
RETURNS INT
AS
BEGIN
    DECLARE @IntlContribParentBNRGroupId INT = (
        SELECT Id 
        FROM Report.SubSystemBNRGroup ssbg
        WHERE ssbg.ReportId = @ReportId
        AND ssbg.Name = 'International Contributions'
        AND ssbg.ParentId IS NULL
    );
    RETURN @IntlContribParentBNRGroupId;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCongressionalJouleRollupType]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/10/2017 
-- Release:     8.1
-- Description: Return the congression joule rollup type text
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetCongressionalJouleRollupType]()
RETURNS NVARCHAR(50)
AS
BEGIN
  RETURN 'Congressional Joule'; --Congressional Joule
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInventorySourcePermissionObjectId]'
GO

-- =============================================================
-- Author:      Mike Strickland
-- Created:     8/9/2016
-- Release:     7.2
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInventorySourcePermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 42; 
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnScheduleExceptions]'
GO

-- =============================================================
-- Author:		Sara O'Neal
-- Created:     prior to 08/12/08
-- Description: Schedule Exception Logic. The returns projects
--  and templates that have schedule exceptions.
--  A red exception means that the project contains a template
--    with an uncompleted Joule/Internal milestone whose baseline
--    date is less than by more than 1 month the new forecast
--    date if one exists or the existing forecast date.
--  A yellow exception means that the project contains past due
--    milestones that are not complete, whose sum of their
--    milestone weights is greater than some threshhold (20%).
--
--	mm/dd/yy  Name     StoryId  Description
--  08/12/08  s.oneal  #364     ignore completed templates (red)
--  08/12/08  s.oneal  #366     only look at existing forecast dates if not new forecast date is available (red)
--  10/12/11  m.brown           Modified to check the finish date against the beginning of the performance
--                              reporting period
-- 01/31/13  CGL       4.0      Removing WorkBreakdownStructureId and MilestoneTemplateId from ProjectMilestone table
-- 10/13/14  j.borgers 5.3      Modified to work with data structure changes 
-- =============================================================
CREATE FUNCTION [G2].[fnScheduleExceptions]
(
	@WorkBreakdownStructureId int,
	@ChangeRequestDetailId int,
	@StatusDt datetime
)
RETURNS @ExceptionList TABLE
(
	WorkBreakdownStructureId int,
	TaskId int,
	ExceptionType varchar(50)
)
AS
BEGIN
	--DECLARE @ChangeRequestDetailId int =  85476;
	--DECLARE @WorkBreakdownStructureId int = NULL;
	--DECLARE @StatusDt datetime;
	--DECLARE @ExceptionList TABLE(WorkBreakdownStructureId int,	TaskId int,	ExceptionType varchar(50));

	DECLARE @performanceChangeRequestTypeId int = G2.fnGetRealtimeStatusChangeRequestTypeId(),
			@threshold int,
			@subSystemId int = G2.fnGetNA21SubSystemId();

	SELECT @threshold = CAST(Value AS int) 
	FROM G2.G2Parameters 
	WHERE ParmKey = 'ScheduleVarianceMilestoneWeightLimit';

	SELECT @WorkBreakdownStructureId = cr.WorkBreakdownStructureId 
	FROM G2.ChangeRequestDetail crd 
	INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id 
	WHERE @WorkBreakdownStructureId IS NULL 
	AND crd.Id = @ChangeRequestDetailId 
	AND crd.ChangeRequestTypeId = @performanceChangeRequestTypeId;

	SELECT @StatusDt = BeginDt 
	FROM G2.Period 
	WHERE @StatusDt IS NULL 
	AND Id = G2.fnGetCurrentPerformancePeriod(@subSystemId);


	INSERT INTO @ExceptionList
	SELECT WorkBreakdownStructureId, TaskId, MIN(ExceptionType) AS ExceptionType
	FROM (
		SELECT wbsTask.ParentId AS WorkBreakdownStructureId, pm.TaskId, 'Red' AS ExceptionType
		FROM G2.ProjectMilestone pm
		INNER JOIN G2.Task t ON pm.TaskId = T.Id
		INNER JOIN G2.WorkBreakdownStructure wbsTask ON t.WorkBreakdownStructureId = wbsTask.Id
		WHERE wbsTask.ParentId = COALESCE(@WorkBreakdownStructureId, wbsTask.ParentId)
		AND (pm.JouleMetricInd = 1 OR pm.InternalMetricInd = 1)
		AND pm.CompletedInd <> 1
		AND pm.BaselineFinishDt < pm.FinishDt
		AND DATEDIFF(month, pm.BaselineFinishDt, pm.FinishDt) > 0

		UNION		

		SELECT wbsTask.ParentId AS WorkBreakdownStructureId, pm.TaskId, 'Yellow' AS ExceptionType
		FROM G2.ProjectMilestone pm
		INNER JOIN G2.Task t ON pm.TaskId = t.Id
		INNER JOIN G2.WorkBreakdownStructure wbsTask ON t.WorkBreakdownStructureId = wbsTask.Id
		WHERE wbsTask.ParentId = COALESCE(@WorkBreakdownStructureId, wbsTask.ParentId)
		GROUP BY wbsTask.ParentId, TaskId
		HAVING CASE WHEN SUM(CAST(pm.MilestoneWeight AS numeric(18,5))) = 0 THEN 0
					ELSE SUM(CASE WHEN pm.BaselineFinishDt < @StatusDt AND pm.CompletedInd = 0 THEN CAST(pm.MilestoneWeight AS numeric(18,5)) ELSE 0.0 END) / SUM(CAST(pm.MilestoneWeight AS numeric(18,5))) * 100 
			   END > @threshold
	) s
	GROUP BY WorkBreakdownStructureId, TaskId;

	--SELECT * FROM @ExceptionList el
	RETURN;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDefaultSubSystemPreferenceChoiceId]'
GO

-- =============================================================
-- Author:      Sara O'Neal/Karl Allen
-- Created:     12/27/2013
-- Release:     4.4
-- Description: Retrieves the Default Sub-System preference choice
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- 01/07/13  KIA      4.6      Updated description to be "Sub-System" (with dash)
-- =============================================================
CREATE FUNCTION [G2].[fnGetDefaultSubSystemPreferenceChoiceId] ()
RETURNS int
AS
BEGIN
	RETURN 4; -- Default Sub-System
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProgramManagementPermissionObjectId]'
GO

-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/10/2017 
-- Release:     8.1
-- Description: Returns the program management permission object
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [Security].[fnGetProgramManagementPermissionObjectId]()
RETURNS INT
AS
BEGIN
  RETURN 180; --Program Management
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDetailsViewLevel1RoleAssociationType]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     07/10/2017 
-- Release:     8.1
-- Description: Return the Details View Level 1 Role type
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetDetailsViewLevel1RoleAssociationType]()
RETURNS NVARCHAR(100)
AS
BEGIN
  RETURN 'Details View Level 1 Role'; --Details View Level 1 Role
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsFilteredRequestsUnderMyPurviewFilter]'
GO

-- =============================================================
-- Author:      Jason Frank
-- Created:     01/14/2014
-- Release:     4.6
-- Description: Function to determine if specified filter is
--   the "Filtered Requests Under My Purview" filter option.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnIsFilteredRequestsUnderMyPurviewFilter]
(
	@Filter varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @match bit = 0;
	SET @match = CASE WHEN @Filter = 'Filtered Requests Under My Purview' THEN 1 ELSE 0 END;
	RETURN @match;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNumberfromString]'
GO

-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 10/24/2011
-- Description:	Extract whole number from string value like Device Serial Number
-- =============================================
CREATE FUNCTION [G2].[fnGetNumberfromString]
(
	@StringwithNumbers varchar(8000)
)
RETURNS varchar(8000)
AS
BEGIN

	DECLARE @Result varchar(8000)

	SELECT @Result = LEFT(	SUBSTRING(@StringwithNumbers,PATINDEX('%[0-9]%',@StringwithNumbers),8000)
							,PATINDEX('%[^0-9]%',SUBSTRING(@StringwithNumbers,PATINDEX('%[0-9]%',@StringwithNumbers),8000) + 'X')-1
						 )

	RETURN @Result

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetIPLinkableConfigurationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     12/05/2017 
-- Release:     8.4
-- Description: Returns the IP Linkable ConfigurationTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetIPLinkableConfigurationTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 11; --IP Linkable
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnFormatPhoneNumber]'
GO

-- =============================================================
-- Author:      Melissa Cole
-- Created:     9/11/2013
-- Release:		4.4
-- Description: Formats the phone number taken from Contact.Phone into
--				a more traditional format. Assumed that the paremeter only
--				consists of numbers.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnFormatPhoneNumber]
(
	@Number varchar(20)
)
RETURNS varchar(25)
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @Number VARCHAR(20);
	--SET @Number = '8658675309'
	/* End Debug */

DECLARE	@NumberLength INT,
		@FormattedNumber VARCHAR(25);
		
SET @NumberLength = Len(@Number)

SELECT @FormattedNumber = 
		-- (123) 456-7890
	CASE WHEN @NumberLength = 10 THEN		
			'(' + Substring(@Number,1,3) + ') ' 
			   + Substring(@Number,4,3) + '-' 
			   + Substring(@Number,7,4)
		
		 -- 1-800-123-4567
		WHEN @NumberLength = 11 THEN				
			Substring(@Number,1,1) + '-' + Substring(@Number,2,3) + '-' 
			   + Substring(@Number,5,3) + '-' 
			   + Substring(@Number,8,4)
	    
		-- 123-4567, 23-4567, 3-4567
		WHEN @NumberLength < 8 AND @NumberLength > 4 THEN 
			Substring(@Number,1,@NumberLength-4) + '-' 
			   + Substring(@Number,@NumberLength-3,4)
	    
		-- When the @NumberLength is <= 4, 10 < @NumberLength > 7, or @NumberLength > 11, return the number without formatting
		ELSE @Number END
		
	RETURN @FormattedNumber;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTravelInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTravelInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 103; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTravelInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the baseline inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/19/16  m.brown  7.0      Added a group by for subsystemTravelInbox subquery because it was 
--                             getting multiple records if a user had more than one role that had
--                             view permission for the travel change request type
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetTravelInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @travelInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN
	
	DECLARE @travelChangeRequestTypeGroupId int = Travel.fnGetTravelChangeRequestTypeGroupId(),
		@nonResponseStatusId int = G2.fnGetNonResponseChangeRequestDetailConcurrenceStatusId(),
		@approvedTripStatusId int = Travel.fnGetApprovedTripStatusId(),
		@overdueDate datetime = G2.fnDayBegin(CURRENT_TIMESTAMP),
		@travelInboxPermissionObjectId INT = G2.fnGetTravelInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId();
	
	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	)

	DECLARE @subsystemTravelInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @travelRequests TABLE
	(
		SubSystemId INT, 
		TripKey VARCHAR(50),
		DaysAtLevel INT
	)

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subsystemTravelInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestTypeGroup crtg
	INNER JOIN G2.ChangeRequestType crt ON crt.ChangeRequestTypeGroupId = crt.Id
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @travelInboxPermissionObjectId
	AND crtg.Id = @travelChangeRequestTypeGroupId
	GROUP BY persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id;

	INSERT INTO @travelRequests (SubSystemId, TripKey, DaysAtLevel)
	SELECT ss.Id, COALESCE(CAST(x.TripId AS VARCHAR), '') + '_' + COALESCE(CAST(x.TripRequestId AS VARCHAR), ''), DATEDIFF(d, x.WorkflowLevelDt, CURRENT_TIMESTAMP)
	FROM (
		-- Approved trips without request, with overdue actual cost
		SELECT t.Id AS TripId, CONVERT(int, NULL) AS TripRequestId, @overdueDate AS WorkflowLevelDt, t.SubSystemId
		FROM Travel.Trip t
		OUTER APPLY (
			SELECT tr.Id AS TripRequestId
			FROM G2.ChangeRequestDetail crd
			INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id
			INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH(NOEXPAND) ON acrs.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
			INNER JOIN Travel.TripRequest tr ON tr.ChangeRequestDetailId = crd.Id
			WHERE tr.TripId = t.Id
			AND crt.ChangeRequestTypeGroupId = @travelChangeRequestTypeGroupId
		) q
		LEFT OUTER JOIN Contact.SecurityContactPersonMapping scpm ON scpm.PartyId = t.TravelerPersonPartyId
		WHERE @PersonId IN (t.CreatedAs, scpm.PersonId)
		AND t.TripStatusId = @approvedTripStatusId
		AND q.TripRequestId IS NULL
		AND t.ActualCost IS NULL
		AND t.EndDt < @overdueDate

		UNION

		-- Open requests
		SELECT tr.TripId, tr.Id AS TripRequestId, crd.WorkflowLevelDt, cr.SubSystemId
		FROM G2.ChangeRequestDetail crd
		INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id
		INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id
		INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH (NOEXPAND) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
		INNER JOIN Travel.TripRequest tr ON crd.Id = tr.ChangeRequestDetailId
		LEFT OUTER JOIN Travel.Trip t ON tr.TripId = t.Id
		LEFT OUTER JOIN Contact.SecurityContactPersonMapping scpm ON tr.TravelerPersonPartyId = scpm.PartyId
		LEFT OUTER JOIN G2.ChangeRequestDetailConcurrence crdc ON crd.Id = crdc.ChangeRequestDetailId AND crd.WorkflowLevel = crdc.WorkflowLevel AND crdc.ForPersonId = @PersonId AND crdc.StatusId = @nonResponseStatusId AND crdc.ActiveInd = 1
		WHERE crt.ChangeRequestTypeGroupId = @travelChangeRequestTypeGroupId
		AND 
		(
			(crd.WorkflowLevel = 1 AND @PersonId IN (COALESCE(t.CreatedAs, tr.CreatedAs), scpm.PersonId))
			OR
			(crd.WorkflowLevel <> 1 AND crdc.Id IS NOT NULL)
		)
	) x
	INNER JOIN G2.SubSystem ss ON x.SubSystemId = ss.Id;

	INSERT INTO @travelInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3,
	                           LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT sti.SubSystemId, sti.SubSystemName, sti.PermissionObjectId, NULL, COUNT(DISTINCT x.TripKey), COALESCE(SUM(CASE WHEN x.ThresholdId = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN x.ThresholdId = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN x.ThresholdId = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(x.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT) AS HasLargeFinPlanInd
	FROM @subsystemTravelInbox sti
	OUTER APPLY
	(
		SELECT tr.TripKey, tr.DaysAtLevel, t.Id AS ThresholdId
		FROM @travelRequests tr
		INNER JOIN @thresholds t ON tr.DaysAtLevel >= t.Start AND tr.DaysAtLevel < t.[End]
		WHERE tr.SubSystemId = sti.SubSystemId
	) x
	GROUP BY sti.SubSystemId, sti.SubSystemName, sti.PermissionObjectId

	RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSuperProxyPermissionId]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     6/10/2016
-- Release:     7.1
-- Description: Retrieves the Super Proxy Permission Id
--   object id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSuperProxyPermissionId] ()
RETURNS INT
AS
BEGIN
	RETURN 76; -- Super proxy
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSPAInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSPAInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 64; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSPAInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the SPA inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 02/18/16  m.brown  6.5      Added days at level and tier calculations
-- 03/15/20  mws      7.0      Remove SPA SubProgram Manager from SPA workflow
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetSPAInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @spaInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE @CountryIdUnitedStates INT = 1,
        -- SPA roles
		@RoleIdSPAProtectTechnicalLead INT = Security.fnGetSPAProtectTechnicalLeadRoleId(),
		@RoleIdSPAPortfolioManagerPrimary INT = Security.fnGetSPAPortfolioManagerPrimaryRoleId(),
		@RoleIdSPAPortfolioManagerSecondary INT = Security.fnGetSPAPortfolioManagerSecondaryRoleId(),
		@RoleIdSPALabLead INT = Security.fnGetSPALabLeadRoleId(),
		@RoleIdSPATeamLead INT = Security.fnGetSPATeamLeadRoleId(),
		@RoleIdSPATeamMember INT = Security.fnGetSPATeamMemberRoleId(),
        -- SPA statuses
        @SPAStatusIdInProgress INT = SPA.fnGetSPAStatusIdInProgress(),
        @SPAStatusIdSubmittedtoSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
        @SPAStatusIdPassedBackbySPATeamLead INT = SPA.fnGetSPAStatusIdPassedBackbySPATeamLead(),
        @SPAStatusIdSubmittedtoSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
        @SPAStatusIdPassedBackbySPALabLead INT = SPA.fnGetSPAStatusIdPassedBackbySPALabLead(),
        @SPAStatusIdSubmittedtoSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
        @SPAStatusIdPassedBackbySPAPortfolioManager INT = SPA.fnGetSPAStatusIdPassedBackbySPAPortfolioManager(),
        @SPAStatusIdSubmittedtoSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead(),
        @SPAStatusIdPassedBackbySPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead(),
		@spaInboxPermissionObjectId INT = G2.fnGetSPAInboxPermissionObjectId(), 
		@viewPermissionId INT = Security.fnGetViewPermissionId();

	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	);

	DECLARE @subsystemSPAInbox TABLE
	(
		SubSystemId INT PRIMARY KEY CLUSTERED,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @siteWBS TABLE
	(
		SubSystemId INT,
		SubSystemName NVARCHAR(100),
		SubSystemShortName NVARCHAR(25),
		ProgramWorkBreakdownStructureId INT,
		OfficeWorkBreakdownStructureId INT,
		SubProgramWorkBreakdownStructureId INT,
		PortfolioWorkBreakdownStructureId INT,
		SiteId INT
	);

	DECLARE @personSPAsAttentionRequired TABLE
	(
		ProgramWorkBreakdownStructureId INT,
		OfficeWorkBreakdownStructureId INT,
		SubProgramWorkBreakdownStructureId INT,
		PortfolioWorkBreakdownStructureId INT,
		SubsystemId INT, 
		SPAMainId INT,
		SPATeamId INT,
		SPAStatusId INT, 
		DaysAtLevel INT
	)

	DECLARE @inboxSPAs TABLE 
	(
		SubSystemId INT, 
		SPAMainId INT,
		DaysAtLevel INT
	);

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subsystemSPAInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT DISTINCT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, NULL
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = persR.SubSystemId
	INNER JOIN @SubsystemList sl ON sl.Id = ss.Id
	WHERE permR.PermissionObjectId = @spaInboxPermissionObjectId
	AND persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId;

	IF EXISTS (SELECT 1 FROM @subsystemSPAInbox ssi)
	BEGIN
		-- Select fields from view into temp table for better performance in the CTE
		INSERT INTO @siteWBS (SubSystemId, ProgramWorkBreakdownStructureId, OfficeWorkBreakdownStructureId, SubProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SiteId)
		SELECT v.SubSystemId, v.ProgramWorkBreakdownStructureId, v.OfficeWorkBreakdownStructureId, v.SubProgramWorkBreakdownStructureId, v.PortfolioWorkBreakdownStructureId, v.SiteId
		FROM SPA.vwSiteWorkBreakdownStructure v;

		INSERT INTO @personSPAsAttentionRequired (ProgramWorkBreakdownStructureId, OfficeWorkBreakdownStructureId, SubProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SubsystemId, SPAMainId, SPATeamId, SPAStatusId, DaysAtLevel)
		SELECT siteWbs.ProgramWorkBreakdownStructureId, siteWbs.OfficeWorkBreakdownStructureId, siteWbs.SubProgramWorkBreakdownStructureId, siteWbs.PortfolioWorkBreakdownStructureId, siteWbs.SubSystemId, sm.Id SPAMainId, t.Id SPATeamId, ss.Id SPAStatusId,
			DATEDIFF(DAY, sm.SPAStatusDt, CURRENT_TIMESTAMP) AS DaysAtLevel
		FROM SPA.SPAMain sm
		INNER JOIN SPA.SPAMainGeneral smg ON smg.SPAMainId = sm.Id AND smg.Revision = sm.Revision
		INNER JOIN SPA.SPAMainGeneralSPATeamXref smgstx ON smgstx.SPAMainGeneralId = smg.Id
		INNER JOIN SPA.SPAStatus ss ON ss.Id = sm.SPAStatusId
		INNER JOIN SPA.SPATeam t ON t.Id = smgstx.SPATeamId
		INNER JOIN G2.Site s ON s.Id = sm.SiteId AND s.DeletedInd = 0
		INNER JOIN G2.Country c ON c.Id = s.CountryId
		INNER JOIN @siteWBS siteWbs ON siteWbs.SiteId = s.Id
		LEFT JOIN G2.State st ON st.Id = s.StateId;

		-- SPA Team Member (STM)
		-- STM should be tied to the SPA Team
		INSERT INTO @inboxSPAs (SubSystemId, SPAMainId, DaysAtLevel)
		SELECT cte.SubSystemId, cte.SPAMainId, cte.DaysAtLevel
		FROM @personSPAsAttentionRequired cte
		INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
		WHERE pr.PersonId = @PersonId
		AND pr.RoleId = @RoleIdSPATeamMember
		AND cte.SPAStatusId IN (@SPAStatusIdInProgress, @SPAStatusIdPassedBackbySPATeamLead, @SPAStatusIdPassedBackbySPALabLead, @SPAStatusIdPassedBackbySPAProtectTechnicalLead)

		UNION 

		-- SPA Team Lead (STL)
		-- STL should be tied to the SPA Team
		SELECT cte.SubSystemId, cte.SPAMainId, cte.DaysAtLevel
		FROM @personSPAsAttentionRequired cte
		INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
		WHERE pr.PersonId = @PersonId
		AND pr.RoleId = @RoleIdSPATeamLead
		AND cte.SPAStatusId IN (@SPAStatusIdSubmittedtoSPATeamLead, @SPAStatusIdPassedBackbySPALabLead, @SPAStatusIdPassedBackbySPAPortfolioManager, @SPAStatusIdPassedBackbySPAProtectTechnicalLead)

		UNION 

		-- SPA Lab Lead
		SELECT cte.SubSystemId, cte.SPAMainId, cte.DaysAtLevel
		FROM @personSPAsAttentionRequired cte
		INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
		WHERE pr.PersonId = @PersonId
		AND pr.RoleId = @RoleIdSPALabLead
		AND cte.SPAStatusId IN (@SPAStatusIdSubmittedtoSPALabLead, @SPAStatusIdPassedBackbySPAPortfolioManager)

		UNION 

		-- SPA Portfolio Manager
		SELECT cte.SubSystemId, cte.SPAMainId, cte.DaysAtLevel
		FROM @personSPAsAttentionRequired cte
		INNER JOIN Security.PersonRole pr ON pr.WorkBreakdownStructureId = cte.PortfolioWorkBreakdownStructureId
		WHERE pr.PersonId = @PersonId
		AND pr.RoleId IN (@RoleIdSPAPortfolioManagerPrimary, @RoleIdSPAPortfolioManagerSecondary)
		AND cte.SPAStatusId IN (@SPAStatusIdSubmittedtoSPAPortfolioManager, @SPAStatusIdPassedBackbySPAProtectTechnicalLead)

		UNION 

		-- SPA Protect Technical Lead
		SELECT cte.SubSystemId, cte.SPAMainId, cte.DaysAtLevel
		FROM @personSPAsAttentionRequired cte
		INNER JOIN Security.PersonRole pr ON pr.WorkBreakdownStructureId = cte.ProgramWorkBreakdownStructureId
		WHERE pr.PersonId = @PersonId
		AND pr.RoleId = @RoleIdSPAProtectTechnicalLead
		AND cte.SPAStatusId = @SPAStatusIdSubmittedtoSPAProtectTechnicalLead
	END

	INSERT INTO @spaInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT ssi.SubSystemId, ssi.SubSystemName, ssi.PermissionObjectId, ssi.ChangeRequestTypeId, COUNT(DISTINCT x.SPAMainId), COALESCE(SUM(CASE WHEN x.ThresholdId = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN x.ThresholdId = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN x.ThresholdId = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(x.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT)
	FROM @subsystemSPAInbox ssi
	OUTER APPLY
	(
		SELECT isa.SubSystemId, isa.SPAMainId, isa.DaysAtLevel, t.Id AS ThresholdId
		FROM @inboxSPAs isa
		INNER JOIN @thresholds t ON isa.DaysAtLevel >= t.Start AND isa.DaysAtLevel < t.[End]
		WHERE isa.SubSystemId = ssi.SubSystemId
	) x
	GROUP BY ssi.SubSystemId, ssi.SubSystemName, ssi.PermissionObjectId, ssi.ChangeRequestTypeId;

	RETURN;

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceReportingInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceReportingInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 63; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPerformanceReportingInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the baseline inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetPerformanceReportingInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @performanceReportingInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN
	
	DECLARE @showInInboxPermissionId int = Security.fnGetShowInInboxPermissionId(), 
			@performanceReportChangeRequestTypeId INT = G2.fnGetRealtimeStatusChangeRequestTypeId(),
			@scheduleConfigurationTypeId int = G2.fnGetScheduleConfigurationTypeId(),
			@minimumAllowedJouleCompletionDateParmKey nvarchar(50) = G2.fnGetPerformanceMinimumAllowedJouleCompletionDateParmKey(),
			@currentPerformancePeriodParmKey nvarchar(50) = G2.fnGetCurrentPerformancePeriodParmKey(),
			@viewPermissionId INT = Security.fnGetViewPermissionId(),
			@performanceReportingInboxPermissionObjectId INT = G2.fnGetPerformanceReportingInboxPermissionObjectId(),
			@performanceReportingGroupId INT = G2.fnGetPerformanceGroupId(); 
			
	DECLARE @minimumJouleDateAndPeriodBySubSystem TABLE
	(
		SubSystemId INT, 
		BeginDt DATETIME, 
		MinimumAllowedJouleCompletionDate DATETIME
	)

	DECLARE @subsystemPerformanceReportingInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @workBreakdownStructuresInPurview TABLE
	(
		SubSystemId INT,
		WorkBreakdownStructureId INT
	)

	DECLARE @wbsStatusRequired TABLE
	(
		SubSystemId INT,
		WorkBreakdownStructureId INT
	)

	INSERT INTO @subsystemPerformanceReportingInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestTypeGroup crtg
	INNER JOIN G2.ChangeRequestType crt ON crt.ChangeRequestTypeGroupId = crt.Id
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @performanceReportingInboxPermissionObjectId
	AND crtg.Id = @performanceReportingGroupId;

	INSERT INTO @minimumJouleDateAndPeriodBySubSystem (SubSystemId, BeginDt, MinimumAllowedJouleCompletionDate)
	SELECT ss.Id as SubSystemId, period.BeginDt, CAST(mj.Value AS datetime) AS MinimumAllowedJouleCompletionDate
	FROM G2.SubSystem ss
	INNER JOIN G2.G2Parameters gp ON ss.Id = gp.SubSystemId AND gp.ParmKey = @currentPerformancePeriodParmKey
	INNER JOIN G2.Period period ON period.Id = CAST(gp.Value AS int)
	INNER JOIN G2.G2Parameters mj ON ss.Id = mj.SubSystemId AND mj.ParmKey = @minimumAllowedJouleCompletionDateParmKey;

	INSERT INTO @workBreakdownStructuresInPurview (SubSystemId, WorkBreakdownStructureId)
	SELECT ss.Id, wbs.Id AS WorkBreakdownStructureId
	FROM Security.PermissionRoleWorkBreakdownStructureDetail permRWBSD
	INNER JOIN G2.WorkBreakdownStructure wbs ON permRWBSD.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.SubSystem ss ON wbs.SubSystemId = ss.Id
	INNER JOIN G2.WorkBreakdownStructureConfiguration wbsc ON wbs.Id = wbsc.WorkBreakdownStructureId
	INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON permRWBSD.SubSystemId = persRWBSD.SubSystemId AND permRWBSD.RoleId = persRWBSD.RoleId AND wbs.Id = persRWBSD.WorkBreakdownStructureId
	WHERE wbsc.ConfigurationTypeId = @scheduleConfigurationTypeId
	AND persRWBSD.PersonId = @PersonId
	AND permRWBSD.PermissionId = @showInInboxPermissionId 
	AND permRWBSD.ChangeRequestTypeId = @performanceReportChangeRequestTypeId;

	INSERT INTO @wbsStatusRequired (SubSystemId, WorkBreakdownStructureId)
	SELECT wbsip.SubSystemId, wbs.Id AS WorkBreakdownStructureId
	FROM @workBreakdownStructuresInPurview wbsip
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbsip.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbs.Id = wbsTask.ParentId
	INNER JOIN G2.Task t ON wbsTask.Id = t.WorkBreakdownStructureId
	INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
	INNER JOIN G2.Milestone m ON pm.MilestoneId = m.Id
	INNER JOIN @minimumJouleDateAndPeriodBySubSystem periodBySubSystem ON periodBySubSystem.SubSystemId = wbsip.SubSystemId
	WHERE m.OptionalInd = 0
	AND pm.CompletedInd <> 1
	AND pm.FinishDt < periodBySubSystem.BeginDt
    
	UNION

	SELECT wbsip.SubSystemId, wbs.Id AS WorkBreakdownStructureId
	FROM @workBreakdownStructuresInPurview wbsip
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbsip.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbs.Id = wbsTask.ParentId
	INNER JOIN G2.Schedule s ON wbsTask.Id = s.WorkBreakdownStructureId
	INNER JOIN G2.Task t ON wbsTask.Id = t.WorkBreakdownStructureId
	INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
	INNER JOIN G2.ProjectMilestoneMetric pmm ON pm.Id = pmm.ProjectMilestoneId
	INNER JOIN G2.Period prd ON prd.EndDt >= s.StartDt AND prd.BeginDt <= pm.FinishDt AND prd.ReconciliationInd = 0
	INNER JOIN @minimumJouleDateAndPeriodBySubSystem periodBySubSystem ON periodBySubSystem.SubSystemId = wbsip.SubSystemId
	LEFT OUTER JOIN G2.ProjectMilestoneMetricPlan pmmp ON pmmp.ProjectMilestoneMetricId = pmm.id AND pmmp.PeriodId = prd.id
	WHERE pmm.HasMetricPlanInd = 1
	AND (pmmp.ActualInd = 0 OR pmmp.Id IS NULL)
	AND pm.CompletedInd = 0
	AND prd.EndDt < periodBySubSystem.BeginDt
	
	UNION
	
	-- Pick up Projects that have out of range metric plans
	SELECT  x.SubSystemId, x.WorkBreakdownStructureId
	FROM (
		SELECT wbsip.SubSystemId, wbsip.WorkBreakdownStructureId, t.Id AS TaskId, jm.Id AS ProjectMilestoneId,
			MIN(pm.FinishDt) AS MinDt, jm.FinishDt AS JouleDt
		FROM @workBreakdownStructuresInPurview wbsip
		INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbsip.WorkBreakdownStructureId = wbsTask.ParentId
		INNER JOIN G2.Task t ON wbsTask.Id = t.WorkBreakdownStructureId
		INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId
		INNER JOIN G2.ProjectMilestone jm ON t.Id = jm.TaskId AND jm.JouleMetricInd = 1
		GROUP BY wbsip.WorkBreakdownStructureId, t.Id, wbsTask.Name, jm.Id, jm.FinishDt, wbsip.SubSystemId
	) x
	INNER JOIN G2.ProjectMilestoneMetric pmm ON pmm.ProjectMilestoneId = x.ProjectMilestoneId
	INNER JOIN G2.ProjectMilestoneMetricPlan pmmp ON pmmp.ProjectMilestoneMetricId = pmm.id
	INNER JOIN G2.Period pmmpprd ON pmmp.PeriodId = pmmpprd.id
	INNER JOIN G2.WorkBreakdownStructure wbs ON x.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.SubSystem ss ON x.SubSystemId = ss.Id
	WHERE pmm.HasMetricPlanInd = 1
	AND pmmp.ForecastValue <> 0
	AND pmmp.ActualInd = 0
	AND
	(
		pmmpprd.EndDt < x.MinDt
		OR
		pmmpprd.BeginDt > x.JouleDt
	)
		
	UNION 
	
	SELECT wbsip.SubSystemId, wbs.Id AS WorkBreakdownStructureId
	FROM @workBreakdownStructuresInPurview wbsip
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbsip.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.WorkBreakdownStructure wbsTask ON wbs.Id = wbsTask.ParentId
	INNER JOIN G2.Task t ON wbsTask.Id = t.WorkBreakdownStructureId
	INNER JOIN G2.ProjectMilestone pm ON t.Id = pm.TaskId AND pm.JouleMetricInd = 1	
	INNER JOIN G2.ProjectMilestoneStatus pms ON pms.ProjectMilestoneId = pm.Id
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = pms.ChangeRequestDetailId
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates vcrsas WITH(NOEXPAND) ON vcrsas.ChangeRequestDetailStatusId = crd.ChangeRequestDetailStatusId
	INNER JOIN @minimumJouleDateAndPeriodBySubSystem mj ON wbs.SubSystemId = mj.SubSystemId
	WHERE pms.CompletedInd = 1 
	AND pms.FinishDt < mj.MinimumAllowedJouleCompletionDate;


	INSERT INTO @performanceReportingInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT spi.SubSystemId, spi.SubSystemName, spi.PermissionObjectId, NULL, COUNT(DISTINCT x.WorkBreakdownStructureId), NULL, NULL, NULL, NULL, NULL
	FROM @subsystemPerformanceReportingInbox spi
	OUTER APPLY 
	(
		SELECT w.SubSystemId, w.WorkBreakdownStructureId 
		FROM @wbsStatusRequired w
		WHERE w.SubSystemId = spi.SubSystemId
	) x
	GROUP BY spi.SubSystemId, spi.SubSystemName, spi.PermissionObjectId

	RETURN

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingAdjustmentInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingAdjustmentInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 60; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingAdjustmentInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the funding adjustment inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- 08/21/17  s.oneal  8.2      Fixed JOIN condition on Role when looking at Show in Inbox permission
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingAdjustmentInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @fundingAdjustmentInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE @showInInboxPermissionId INT = Security.fnGetShowInInboxPermissionId(),
		@fundingAdjustmentChangeRequestTypeGroupId INT = G2.fnGetFundingAdjustmentChangeRequestTypeGroupId(), 
		@fundingAdjustmentInboxPermissionObjectId INT = G2.fnGetFundingAdjustmentInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId();
	
	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	);

	DECLARE @activeFundingAdjustments TABLE
	(
		SubSystemId INT, ChangeRequestDetailId INT, WorkflowLevel INT, ChangeRequestTypeId INT, 
		WorkBreakdownStructureId INT, ReportingEntityOrganizationPartyId INT, DaysAtLevel INT
	);

	DECLARE @inboxFundingAdjustments TABLE
	(
		ChangeRequestDetailId INT
	);

	DECLARE @subsystemFundingAdjustmentInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subsystemFundingAdjustmentInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestTypeGroup crtg 
	INNER JOIN G2.ChangeRequestType crt ON crt.ChangeRequestTypeGroupId = crtg.Id
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @fundingAdjustmentInboxPermissionObjectId
	AND crtg.Id = @fundingAdjustmentChangeRequestTypeGroupId
	GROUP BY persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id;

	INSERT INTO @activeFundingAdjustments (SubSystemId, ChangeRequestDetailId, WorkflowLevel, 
		ChangeRequestTypeId, WorkBreakdownStructureId, ReportingEntityOrganizationPartyId, DaysAtLevel)
	SELECT ss.Id, crd.Id, crd.WorkflowLevel, 
		crt.Id, wbs.Id, far.ReportingEntityOrganizationPartyId, DATEDIFF(DAY, crd.WorkflowLevelDt, CURRENT_TIMESTAMP) AS DaysAtLevel
	FROM G2.ChangeRequest cr
	INNER JOIN G2.ChangeRequestDetail crd ON cr.Id = crd.ChangeRequestId
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH (NOEXPAND) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id 
	INNER JOIN G2.ChangeRequestTypeGroup crtg ON crtg.Id = crt.ChangeRequestTypeGroupId
	INNER JOIN Financial.FundingAdjustmentRequest far ON crd.Id = far.ChangeRequestDetailId
	INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = far.WorkBreakdownStructureId
	INNER JOIN G2.SubSystem ss ON cr.SubSystemId = ss.Id
	WHERE crt.ChangeRequestTypeGroupId = @fundingAdjustmentChangeRequestTypeGroupId;

	INSERT INTO @inboxFundingAdjustments (ChangeRequestDetailId)
	SELECT fa.ChangeRequestDetailId
	FROM @activeFundingAdjustments fa
	INNER JOIN Security.PermissionRoleWorkBreakdownStructureDetail permRWBSD ON fa.SubSystemId = permRWBSD.SubSystemId AND fa.ChangeRequestTypeId = permRWBSD.ChangeRequestTypeId AND fa.WorkflowLevel = permRWBSD.WorkflowLevel AND fa.WorkBreakdownStructureId = permRWBSD.WorkBreakdownStructureId
	INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON fa.SubSystemId = persRWBSD.SubSystemId AND permRWBSD.RoleId = persRWBSD.RoleId AND fa.WorkBreakdownStructureId = persRWBSD.WorkBreakdownStructureId
	WHERE permRWBSD.PermissionId = @showInInboxPermissionId
	AND persRWBSD.PersonId = @PersonId

	UNION

	SELECT fa.ChangeRequestDetailId
	FROM @activeFundingAdjustments fa
	INNER JOIN Security.PermissionRoleOrganizationBreakdownStructureDetail permROBSD ON fa.SubSystemId = permROBSD.SubSystemId AND fa.ChangeRequestTypeId = permROBSD.ChangeRequestTypeId AND fa.WorkflowLevel = permROBSD.WorkflowLevel AND fa.ReportingEntityOrganizationPartyId = permROBSD.OrganizationPartyId
	INNER JOIN Security.PersonRoleOrganizationBreakdownStructureDetail persROBSD ON fa.SubSystemId = persROBSD.SubSystemId AND permROBSD.RoleId = persROBSD.RoleId AND fa.ReportingEntityOrganizationPartyId = persROBSD.OrganizationPartyId
	WHERE permROBSD.PermissionId = @showInInboxPermissionId
	AND persROBSD.PersonId = @PersonId;

	INSERT INTO @fundingAdjustmentInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId,
		ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT sfi.SubSystemId, sfi.SubSystemName, sfi.PermissionObjectId, NULL,
		COUNT(DISTINCT afa.ChangeRequestDetailId) AS ItemCount, COALESCE(SUM(CASE WHEN afa.ThresholdId = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN afa.ThresholdId = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN afa.ThresholdId = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(afa.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT) AS HasLargeFinPlanInd
	FROM @subsystemFundingAdjustmentInbox sfi
	OUTER APPLY
	(
		SELECT afa.DaysAtLevel, afa.ReportingEntityOrganizationPartyId, afa.WorkBreakdownStructureId, afa.ChangeRequestTypeId, afa.WorkflowLevel,
               afa.ChangeRequestDetailId, afa.SubSystemId, t.Id AS ThresholdId
		FROM @inboxFundingAdjustments ifa
		INNER JOIN @activeFundingAdjustments afa ON afa.ChangeRequestDetailId = ifa.ChangeRequestDetailId
		INNER JOIN @thresholds t ON afa.DaysAtLevel >= t.Start AND afa.DaysAtLevel < t.[End]
		WHERE afa.ChangeRequestTypeId = sfi.ChangeRequestTypeId
		AND sfi.SubSystemId = afa.SubSystemId
	) afa
	GROUP BY sfi.SubSystemId, sfi.SubSystemName, sfi.PermissionObjectId

	RETURN

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFinPlanInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFinPlanInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 59; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFinPlanInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the finplan inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetFinPlanInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @finplanInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE @finPlanGroupId INT = Financial.fnGetFinPlanGroupId(),
		@showInInboxPermissionId INT = Security.fnGetShowInInboxPermissionId(), -- Show in Inbox
		@nonResponseStatusId INT = G2.fnGetNonResponseChangeRequestDetailConcurrenceStatusId(), -- Non-Response
		@submitPermissionId INT = Security.fnGetSubmitPermissionId(),
		@concurPermissionId INT = Security.fnGetConcurPermissionId(),
		@processPermissionId INT = Security.fnGetProcessPermissionId(), 
		@finplanInboxPermissionObjectId INT = G2.fnGetFinPlanInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId(); 

	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	)

	DECLARE @responsibleWorkBreakdownStructures TABLE
	(
		WorkBreakdownStructureId INT,
		WorkflowLevel INT, 
		ChangeRequestTypeId INT
	)

	DECLARE @responsibleOrganizations TABLE
	(
		SubSystemId INT, 
		OrganizationPartyId INT,
		WorkflowLevel INT,
		ChangeRequestTypeId INT
	)
	
	DECLARE @activeFinPlans TABLE
	(
		ChangeRequestDetailId INT,
		FinPlanRequestId INT,
		LargeFinPlanInd BIT,
		ChangeRequestTypeId INT,
		ChangeRequestTypeGroupId INT,
		WorkflowLevel INT,
		DaysAtLevel INT, 
		SubSystemId INT, 
		CreatedAs INT
	)

	DECLARE @subsystemFinPlanInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	INSERT INTO @subsystemFinPlanInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestTypeGroup crtg
	INNER JOIN G2.ChangeRequestType crt ON crt.ChangeRequestTypeGroupId = crtg.Id
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @finplanInboxPermissionObjectId
	AND crtg.Id = @finPlanGroupId;

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @responsibleWorkBreakdownStructures (WorkBreakdownStructureId, WorkflowLevel, ChangeRequestTypeId)
	SELECT permRWBSD.WorkBreakdownStructureId, permRWBSD.WorkflowLevel, permRWBSD.ChangeRequestTypeId
	FROM Security.PermissionRoleWorkBreakdownStructureDetail permRWBSD
	INNER JOIN G2.WorkBreakdownStructure wbs ON permRWBSD.WorkBreakdownStructureId = wbs.Id
	INNER JOIN G2.ChangeRequestType crt ON permRWBSD.ChangeRequestTypeId = crt.Id
	INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON permRWBSD.RoleId = persRWBSD.RoleId AND wbs.Id = persRWBSD.WorkBreakdownStructureId AND persRWBSD.SubSystemId = permRWBSD.SubSystemId
	WHERE persRWBSD.PersonId = @PersonId
	AND permRWBSD.PermissionId = @showInInboxPermissionId
	AND crt.ChangeRequestTypeGroupId = @finPlanGroupId;

	INSERT INTO @responsibleOrganizations (SubSystemId, OrganizationPartyId, WorkflowLevel, ChangeRequestTypeId)
	SELECT permROBSD.SubSystemId, permROBSD.OrganizationPartyId, permROBSD.WorkflowLevel, permROBSD.ChangeRequestTypeId
	FROM Security.PermissionRoleOrganizationBreakdownStructureDetail permROBSD
	INNER JOIN G2.ChangeRequestType crt ON permROBSD.ChangeRequestTypeId = crt.Id
	INNER JOIN Security.PersonRoleOrganizationBreakdownStructureDetail persROBSD ON permROBSD.OrganizationPartyId = persROBSD.OrganizationPartyId AND permROBSD.OrganizationPartyId = persROBSD.OrganizationPartyId AND permROBSD.RoleId = persROBSD.RoleId AND permROBSD.SubSystemId = persROBSD.SubSystemId
	WHERE persROBSD.PersonId = @PersonId
	AND permROBSD.PermissionId = @showInInboxPermissionId
	AND crt.ChangeRequestTypeGroupId = @finPlanGroupId;

	INSERT INTO @activeFinPlans (ChangeRequestDetailId, FinPlanRequestId, LargeFinPlanInd, ChangeRequestTypeId, ChangeRequestTypeGroupId, WorkflowLevel, DaysAtLevel, SubSystemId, CreatedAs)
	SELECT crd.Id AS ChangeRequestDetailId, fpr.Id AS FinPlanRequestId, CAST(CASE WHEN fpr.Amount >= 250000 THEN 1 ELSE 0 END AS BIT), crt.Id, crt.ChangeRequestTypeGroupId, crd.WorkflowLevel, DATEDIFF(DAY, crd.WorkflowLevelDt, CURRENT_TIMESTAMP) AS DaysAtLevel, cr.SubSystemId, crd.CreatedAs
	FROM G2.ChangeRequestDetail crd
	INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH(NOEXPAND) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id
	INNER JOIN Financial.FinPlanRequest fpr ON crd.Id = fpr.ChangeRequestDetailId
	WHERE crt.ChangeRequestTypeGroupId = @finPlanGroupId

	INSERT INTO @finplanInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId,
	                            ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT sfi.SubSystemId, sfi.SubSystemName, sfi.PermissionObjectId, NULL,
		COUNT(DISTINCT q.FinPlanRequestId) AS ItemCount, COALESCE(SUM(CASE WHEN t.Id = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN t.Id = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN t.Id = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(q.DaysAtLevel) AS LongestDaysWaiting, CAST(MAX(CAST(q.LargeFinPlanInd AS INT)) AS BIT) AS HasLargeFinPlanInd
	FROM @subsystemFinPlanInbox sfi
	LEFT OUTER JOIN 
	(
		SELECT afp.SubSystemId, fprd.FinPlanRequestId, afp.ChangeRequestTypeId, afp.DaysAtLevel, afp.LargeFinPlanInd
		FROM @activeFinPlans afp
		INNER JOIN Financial.FinPlanRequestDetail fprd ON afp.FinPlanRequestId = fprd.FinPlanRequestId
		INNER JOIN G2.vwSubSystemWorkflowActionRequired vsswar WITH(NOEXPAND) ON afp.ChangeRequestTypeId = vsswar.ChangeRequestTypeId AND afp.WorkflowLevel = vsswar.WorkflowLevel AND afp.SubSystemId = vsswar.SubSystemId
		WHERE afp.CreatedAs = @PersonId
		AND vsswar.PermissionId = @submitPermissionId
	
		UNION 
	
		SELECT afp.SubSystemId, fprd.FinPlanRequestId, afp.ChangeRequestTypeId, afp.DaysAtLevel, afp.LargeFinPlanInd
		FROM @activeFinPlans afp
		INNER JOIN Financial.FinPlanRequestDetail fprd ON afp.FinPlanRequestId = fprd.FinPlanRequestId
		LEFT OUTER JOIN @responsibleWorkBreakdownStructures r ON fprd.WorkBreakdownStructureId = r.WorkBreakdownStructureId AND afp.ChangeRequestTypeId = r.ChangeRequestTypeId AND afp.WorkflowLevel = r.WorkflowLevel
		LEFT OUTER JOIN @responsibleOrganizations tro ON afp.SubSystemId = tro.SubSystemId AND fprd.ReportingEntityOrganizationPartyId = tro.OrganizationPartyId AND afp.ChangeRequestTypeId = tro.ChangeRequestTypeId AND afp.WorkflowLevel = tro.WorkflowLevel
		LEFT OUTER JOIN G2.ChangeRequestDetailConcurrence crdc ON afp.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND crdc.WorkflowLevel = afp.WorkflowLevel AND crdc.ForPersonId = @PersonId AND crdc.StatusId = @nonResponseStatusId and crdc.ActiveInd = 1
		INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON afp.ChangeRequestTypeId = swar.ChangeRequestTypeId AND afp.WorkflowLevel = swar.WorkflowLevel AND swar.SubSystemId = afp.SubSystemId
		WHERE (r.WorkBreakdownStructureId IS NOT NULL OR tro.OrganizationPartyId IS NOT NULL)
		AND 
		(
			(swar.PermissionId = @concurPermissionId AND crdc.Id IS NOT NULL)
			OR
			(swar.PermissionId = @processPermissionId)
		)
	) q ON sfi.ChangeRequestTypeId = q.ChangeRequestTypeId AND q.SubSystemId = sfi.SubSystemId
	LEFT OUTER JOIN @thresholds t ON q.DaysAtLevel >= t.Start AND q.DaysAtLevel < t.[End]
	GROUP BY sfi.SubSystemId, sfi.SubSystemName, sfi.PermissionObjectId

	RETURN

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetEventManagerInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetEventManagerInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 65; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetEventManagementInbox]'
GO
-- =============================================================

-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the event management inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetEventManagementInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @eventManagementInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE @intitalParticipantStatusId INT = Calendar.fnGetInitialParticipantStatusId(), 
		@eventManagementInboxPermissionObjectId INT = G2.fnGetEventManagerInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId(),
		@eventManagementGroupId INT = Calendar.fnGetTrainingChangeRequestTypeGroupId();

	DECLARE @subsystemEventManagementInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @inboxNotices TABLE
	(
		RoleId INT,
		EventTypeId INT,
		ParticipantStatusId INT,
		Notice NVARCHAR(250)
	);

	DECLARE @emInboxItems TABLE
	(
		ChangeRequestTypeId INT,
		EventParticipantId INT
	)

	INSERT INTO @subsystemEventManagementInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON sl.Id = ss.Id
	CROSS JOIN G2.ChangeRequestType crt
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @eventManagementInboxPermissionObjectId
	AND crt.ChangeRequestTypeGroupId = @eventManagementGroupId
	GROUP BY persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id;

	INSERT INTO @inboxNotices (RoleId, EventTypeId, ParticipantStatusId, Notice)
	SELECT pr.RoleId, n.EventTypeId, n.ParticipantStatusId, n.Notice
    FROM Security.PersonRole pr 
    INNER JOIN Calendar.InboxNoticeRoleXref inrx ON inrx.RoleId = pr.RoleId
    INNER JOIN Calendar.InboxNotice n ON n.Id = inrx.InboxNoticeId
    WHERE pr.PersonId = @PersonId;
	

	INSERT INTO @emInboxItems (ChangeRequestTypeId, EventParticipantId)
	SELECT crd.ChangeRequestTypeId, ep.Id
    FROM Calendar.EventParticipant ep 
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = ep.ChangeRequestDetailId
	INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
    INNER JOIN Calendar.ParticipantType pt ON pt.Id = ep.ParticipantTypeId
    INNER JOIN Calendar.ParticipantStatus ps ON ps.Id = ep.ParticipantStatusId
    INNER JOIN Calendar.Event e ON e.Id = ep.EventId
    INNER JOIN Calendar.EventType et ON et.Id = e.EventTypeId
    INNER JOIN Calendar.EventParticipantSite eps ON eps.Id = ep.Id
    INNER JOIN G2.Site s ON s.Id = eps.SiteId
    INNER JOIN G2.State st ON st.Id = s.StateId
    LEFT OUTER JOIN Inventory.RegionStateXRef rsx ON rsx.StateId = st.Id
    LEFT OUTER JOIN Inventory.Region r ON r.Id = rsx.RegionId
    LEFT OUTER JOIN Calendar.vwSiteMilestones sm ON sm.SiteId = eps.SiteId
    INNER JOIN G2.WorkBreakdownStructureStateXref wbssx ON wbssx.StateId = s.StateId
    INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbssx.WorkBreakdownStructureId 
    INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON persRWBSD.SubSystemId = wbs.SubSystemId AND persRWBSD.WorkBreakdownStructureId = wbs.Id
    INNER JOIN @inboxNotices tin ON tin.EventTypeId = e.EventTypeId 
        AND tin.ParticipantStatusId = ep.ParticipantStatusId 
        AND tin.RoleId = persRWBSD.RoleId
    WHERE persRWBSD.PersonId = @PersonId
    AND ep.DeletedInd = 0
    AND e.StartDt > CURRENT_TIMESTAMP
    AND wbs.SubSystemId = cr.SubSystemId
    
    UNION

    ---------------------------------------------------------------------------------------------------
    -- LEAs
    ---------------------------------------------------------------------------------------------------
    SELECT crd.ChangeRequestTypeId, ep.Id
    FROM Calendar.EventParticipant ep 
	INNER JOIN G2.ChangeRequestDetail crd ON crd.Id = ep.ChangeRequestDetailId
	INNER JOIN G2.ChangeRequest cr ON cr.Id = crd.ChangeRequestId
    INNER JOIN Calendar.ParticipantType pt ON pt.Id = ep.ParticipantTypeId
    INNER JOIN Calendar.ParticipantStatus ps ON ps.Id = ep.ParticipantStatusId
    INNER JOIN Calendar.Event e ON e.Id = ep.EventId
    INNER JOIN Calendar.EventType et ON et.Id = e.EventTypeId
    INNER JOIN Calendar.EventParticipantLea epl ON epl.Id = ep.Id
    INNER JOIN Inventory.LawEnforcementAgency lea ON lea.Id = epl.LeaId
    INNER JOIN G2.State st ON st.Id = lea.StateId
    LEFT OUTER JOIN Inventory.RegionStateXRef rsx ON rsx.StateId = st.Id
    LEFT OUTER JOIN Inventory.Region r ON r.Id = rsx.RegionId
    INNER JOIN G2.WorkBreakdownStructureStateXref wbssx ON wbssx.StateId = lea.StateId
    INNER JOIN G2.WorkBreakdownStructure wbs ON wbs.Id = wbssx.WorkBreakdownStructureId 
    INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON persRWBSD.SubSystemId = wbs.SubSystemId AND persRWBSD.WorkBreakdownStructureId = wbs.Id
    INNER JOIN @inboxNotices tin ON tin.EventTypeId = e.EventTypeId 
        AND tin.ParticipantStatusId = ep.ParticipantStatusId 
        AND tin.RoleId = persRWBSD.RoleId
    WHERE persRWBSD.PersonId = @PersonId
    AND ep.DeletedInd = 0
    AND e.StartDt > CURRENT_TIMESTAMP
    AND wbs.SubSystemId = cr.SubSystemId


	INSERT INTO @eventManagementInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT sei.SubSystemId, sei.SubSystemName, sei.PermissionObjectId, NULL, COUNT(DISTINCT x.EventParticipantId), NULL, NULL, NULL, NULL, CAST(NULL AS BIT) AS HasLargeFinPlanInd
	FROM @subsystemEventManagementInbox sei
	OUTER APPLY
	(
		SELECT eii.ChangeRequestTypeId, eii.EventParticipantId
		FROM @emInboxItems eii 
		WHERE eii.ChangeRequestTypeId = sei.ChangeRequestTypeId
	) x
	GROUP BY sei.SubSystemId, sei.SubSystemName, sei.PermissionObjectId

	RETURN
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNLSAInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNLSAInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 170; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCountryAssessmentInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the SPA inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 02/18/16  m.brown  6.5      Added days at level and tier calculations
-- 03/15/20  mws      7.0      Remove SPA SubProgram Manager from SPA workflow
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetCountryAssessmentInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @countryAssessmentInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE 
        -- SPA roles
		@RoleIdSPAProtectTechnicalLead INT = [Security].fnGetSPAProtectTechnicalLeadRoleId(),
		@RoleIdSPAPortfolioManagerPrimary INT = [Security].fnGetSPAPortfolioManagerPrimaryRoleId(),
		@RoleIdSPAPortfolioManagerSecondary INT = [Security].fnGetSPAPortfolioManagerSecondaryRoleId(),
		@RoleIdSPALabLead INT = [Security].fnGetSPALabLeadRoleId(),
		@RoleIdSPATeamLead INT = [Security].fnGetSPATeamLeadRoleId(),
		@RoleIdSPATeamMember INT = [Security].fnGetSPATeamMemberRoleId(),
        -- SPA statuses
        @SPAStatusIdInProgress INT = SPA.fnGetSPAStatusIdInProgress(),
        @SPAStatusIdSubmittedtoSPATeamLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPATeamLead(),
        @SPAStatusIdPassedBackbySPATeamLead INT = SPA.fnGetSPAStatusIdPassedBackbySPATeamLead(),
        @SPAStatusIdSubmittedtoSPALabLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPALabLead(),
        @SPAStatusIdPassedBackbySPALabLead INT = SPA.fnGetSPAStatusIdPassedBackbySPALabLead(),
        @SPAStatusIdSubmittedtoSPAPortfolioManager INT = SPA.fnGetSPAStatusIdSubmittedtoSPAPortfolioManager(),
        @SPAStatusIdPassedBackBySPAPortfolioManager INT = SPA.fnGetSPAStatusIdPassedBackbySPAPortfolioManager(),
        @SPAStatusIdSubmittedtoSPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdSubmittedtoSPAProtectTechnicalLead(),
        @SPAStatusIdPassedBackbySPAProtectTechnicalLead INT = SPA.fnGetSPAStatusIdPassedBackbySPAProtectTechnicalLead(),
		@niaInboxPermissionObjectId INT = G2.fnGetNLSAInboxPermissionObjectId(), 
		@viewPermissionId INT = Security.fnGetViewPermissionId();

	
	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	);
	
	DECLARE @subsystemNIAInbox TABLE
	(
		SubSystemId INT PRIMARY KEY CLUSTERED,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @personSPAsAttentionRequired TABLE
	(
		ProgramWorkBreakdownStructureId INT,
		OfficeWorkBreakdownStructureId INT,
		SubProgramWorkBreakdownStructureId INT,
		PortfolioWorkBreakdownStructureId INT,
		SubsystemId INT, 
		CountryAssessmentId INT,
		SPATeamId INT,
		SPAStatusId INT, 
		DaysAtLevel INT
	);

	DECLARE @inboxSPAs TABLE 
	(
		SubSystemId INT, 
		CountryAssessmentId INT,
		DaysAtLevel INT
	);

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subsystemNIAInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT DISTINCT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, NULL
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = persR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	WHERE permR.PermissionObjectId = @niaInboxPermissionObjectId
	AND persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId;

	INSERT INTO @personSPAsAttentionRequired (ProgramWorkBreakdownStructureId, OfficeWorkBreakdownStructureId, SubProgramWorkBreakdownStructureId, PortfolioWorkBreakdownStructureId, SubsystemId, CountryAssessmentId, SPATeamId, SPAStatusId, DaysAtLevel)
	SELECT countryWbs.ProgramWorkBreakdownStructureId, countryWbs.OfficeWorkBreakdownStructureId, countryWbs.SubProgramWorkBreakdownStructureId, countryWbs.PortfolioWorkBreakdownStructureId, countryWbs.SubSystemId, 
		castx.CountryAssessmentId, st.Id SPATeamId, ss.Id SPAStatusId, DATEDIFF(DAY, ca.SPAStatusDt, CURRENT_TIMESTAMP) AS DaysAtLevel
	FROM SPA.CountryAssessment ca
	INNER JOIN SPA.CountryAssessmentSPATeamXref castx ON castx.CountryAssessmentId = ca.Id
	INNER JOIN SPA.SPAStatus ss ON ss.Id = ca.SPAStatusId
	INNER JOIN SPA.SPATeam st ON st.Id = castx.SPATeamId
	INNER JOIN G2.Country c ON c.Id = ca.CountryId
    INNER JOIN SPA.vwCountryWorkBreakdownStructure countryWbs ON countryWbs.CountryId = c.Id;
	
	INSERT INTO @inboxSPAs (SubSystemId, CountryAssessmentId, DaysAtLevel)
	-- SPA Team Member
	SELECT cte.SubSystemId, cte.CountryAssessmentId, cte.DaysAtLevel
	FROM @personSPAsAttentionRequired cte
	INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
	WHERE pr.PersonId = @PersonId
	AND pr.RoleId = @RoleIdSPATeamMember
	AND cte.SPAStatusId IN (@SPAStatusIdInProgress, @SPAStatusIdPassedBackBySPATeamLead, @SPAStatusIdPassedBackBySPALabLead, @SPAStatusIdPassedBackBySPAPortfolioManager)

	UNION

	-- SPA Team Lead
	SELECT cte.SubSystemId, cte.CountryAssessmentId, cte.DaysAtLevel
	FROM @personSPAsAttentionRequired cte
	INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
	WHERE pr.PersonId = @PersonId
	AND pr.RoleId = @RoleIdSPATeamLead
	AND cte.SPAStatusId IN (@SPAStatusIdSubmittedToSPATeamLead, @SPAStatusIdPassedBackBySPALabLead, @SPAStatusIdPassedBackBySPAPortfolioManager)

	UNION 
	
	-- SPA Lab Lead
	SELECT cte.SubSystemId, cte.CountryAssessmentId, cte.DaysAtLevel
	FROM @personSPAsAttentionRequired cte
	INNER JOIN Security.PersonRole pr ON pr.SPATeamId = cte.SPATeamId
	WHERE pr.PersonId = @PersonId
	AND pr.RoleId = @RoleIdSPALabLead
	AND cte.SPAStatusId IN (@SPAStatusIdSubmittedToSPALabLead, @SPAStatusIdPassedBackBySPAPortfolioManager)

	UNION 
	
	-- SPA Portfolio Manager 
	SELECT cte.SubSystemId, cte.CountryAssessmentId, cte.DaysAtLevel
	FROM @personSPAsAttentionRequired cte
	INNER JOIN Security.PersonRole pr ON pr.WorkBreakdownStructureId = cte.PortfolioWorkBreakdownStructureId
	WHERE pr.PersonId = @PersonId
	AND pr.RoleId IN (@RoleIdSPAPortfolioManagerPrimary, @RoleIdSPAPortfolioManagerSecondary)
	AND cte.SPAStatusId IN (@SPAStatusIdSubmittedtoSPAPortfolioManager)

	UNION 

	-- SPA Protect Technical Lead
	SELECT cte.SubSystemId, cte.CountryAssessmentId, cte.DaysAtLevel
	FROM @personSPAsAttentionRequired cte
	INNER JOIN Security.PersonRole pr ON pr.WorkBreakdownStructureId =  cte.ProgramWorkBreakdownStructureId
	WHERE pr.PersonId = @PersonId
	AND pr.RoleId = @RoleIdSPAProtectTechnicalLead
	AND cte.SPAStatusId = @SPAStatusIdSubmittedtoSPAProtectTechnicalLead

	INSERT INTO @countryAssessmentInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT ssi.SubSystemId, ssi.SubSystemName, ssi.PermissionObjectId, ssi.ChangeRequestTypeId, COUNT(DISTINCT x.CountryAssessmentId), COALESCE(SUM(CASE WHEN x.ThresholdId = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN x.ThresholdId = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN x.ThresholdId = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(x.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT)
	FROM @subsystemNIAInbox ssi
	OUTER APPLY
	(
		SELECT isa.SubSystemId, isa.CountryAssessmentId, isa.DaysAtLevel, t.Id AS ThresholdId
		FROM @inboxSPAs isa
		INNER JOIN @thresholds t ON isa.DaysAtLevel >= t.Start AND isa.DaysAtLevel < t.[End]
		WHERE isa.SubSystemId = ssi.SubSystemId
	) x
	GROUP BY ssi.SubSystemId, ssi.SubSystemName, ssi.PermissionObjectId, ssi.ChangeRequestTypeId;

	RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBaselineInboxPermissionObjectId]'
GO


-- =============================================================
-- Author:      Mark Brown
-- Created:     2/10/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBaselineInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 62; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBaselineInbox]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/5/2016
-- Release:     6.5
-- Description: Returns the baseline inbox for the specified user
--
-- mm/dd/yy  Name     Release  Description
-- 04/29/16  m.brown  7.0      Added subsystem list parameters
-- =============================================================
CREATE FUNCTION [G2].[fnGetBaselineInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @baselineInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN

	DECLARE @baselineGroupId INT = Baseline.fnGetBaselineGroupId(),
		@baselineTypeId INT = Baseline.fnGetBaselineTypeId(),
		@spendPlanTypeId INT = G2.fnGetSpendPlanChangeRequestTypeId(),
		@lifecycleBudgetTypeId INT = Baseline.fnGetLifecycleBudgetTypeId(),
		@nonResponseStatusId INT = G2.fnGetNonResponseChangeRequestDetailConcurrenceStatusId(),
		@showInInboxPermissionId INT = Security.fnGetShowInInboxPermissionId(),
		@submitPermissionId INT = Security.fnGetSubmitPermissionId(),
		@concurPermissionId INT = Security.fnGetConcurPermissionId(),
		@processPermissionId INT = Security.fnGetProcessPermissionId(), 
		@baselineInboxPermissionObjectId INT = G2.fnGetBaselineInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId();

	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	)

	DECLARE @activeBaselines TABLE
	(
		SubSystemId INT, ChangeRequestDetailId INT, WorkflowLevel INT,
		ChangeRequestTypeId INT, WorkBreakdownStructureId INT, OriginatorPersonId INT, DaysAtLevel INT
	);

	DECLARE @subsystemBaselineInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	DECLARE @inboxBaseline TABLE 
	(
		ChangeRequestDetailId INT NOT NULL
	);

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subsystemBaselineInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestType crt
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @baselineInboxPermissionObjectId
	AND crt.Id IN (@baselineTypeId, @spendPlanTypeId, @lifecycleBudgetTypeId)
	GROUP BY persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id;

	INSERT INTO @activeBaselines (SubSystemId, ChangeRequestDetailId, WorkflowLevel, ChangeRequestTypeId, WorkBreakdownStructureId, OriginatorPersonId, DaysAtLevel)
	SELECT ss.Id AS SubSystemId, crd.Id AS ChangeRequestDetailId, crd.WorkflowLevel, crd.ChangeRequestTypeId, w.Id AS WorkBreakdownStructureId, crd.CreatedAs AS OrginatorPersonId,
		DATEDIFF(DAY, crd.WorkflowLevelDt, CURRENT_TIMESTAMP) AS DaysAtLevel
	FROM G2.ChangeRequestDetail crd
	INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id
	INNER JOIN G2.ChangeRequestTypeGroup crtg ON crtg.Id = crt.ChangeRequestTypeGroupId
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH(NOEXPAND) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequest cr ON crd.ChangeRequestId = cr.Id
	INNER JOIN G2.WorkBreakdownStructure w ON cr.WorkBreakdownStructureId = w.Id
	INNER JOIN G2.SubSystem ss ON cr.SubSystemId = ss.Id
	WHERE crt.ChangeRequestTypeGroupId = @baselineGroupId;

	-- Concur, process or submit permissions
	INSERT INTO @inboxBaseline (ChangeRequestDetailId)
	SELECT bcr.ChangeRequestDetailId 
	FROM @activeBaselines bcr
	INNER JOIN Security.PermissionRoleWorkBreakdownStructureDetail permRWBSD ON bcr.SubSystemId = permRWBSD.SubSystemId AND bcr.ChangeRequestTypeId = permRWBSD.ChangeRequestTypeId AND bcr.WorkflowLevel = permRWBSD.WorkflowLevel AND bcr.WorkBreakdownStructureId = permRWBSD.WorkBreakdownStructureId
	INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON permRWBSD.SubSystemId = persRWBSD.SubSystemId AND permRWBSD.RoleId = persRWBSD.RoleId AND permRWBSD.WorkBreakdownStructureId = persRWBSD.WorkBreakdownStructureId
	INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON bcr.SubSystemId = swar.SubSystemId AND bcr.ChangeRequestTypeId = swar.ChangeRequestTypeId AND bcr.WorkflowLevel = swar.WorkflowLevel
	LEFT OUTER JOIN G2.ChangeRequestDetailConcurrence crdc ON bcr.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND bcr.WorkflowLevel = crdc.WorkflowLevel AND persRWBSD.PersonId = crdc.ForPersonId AND crdc.ActiveInd = 1 AND crdc.StatusId = @nonResponseStatusId
	WHERE permRWBSD.PermissionId = @showInInboxPermissionId
	AND persRWBSD.PersonId = @PersonId
	AND
	(
		(swar.PermissionId = @concurPermissionId AND crdc.Id IS NOT NULL)
		OR
		(swar.PermissionId IN (@submitPermissionId, @processPermissionId))
	)

	UNION

	-- Originator
	SELECT bcr.ChangeRequestDetailId
	FROM @activeBaselines bcr
	INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON bcr.SubSystemId = swar.SubSystemId AND bcr.ChangeRequestTypeId = swar.ChangeRequestTypeId AND bcr.WorkflowLevel = swar.WorkflowLevel
	INNER JOIN Security.PermissionRoleWorkBreakdownStructureDetail permRWBSD ON bcr.SubSystemId = permRWBSD.SubSystemId AND bcr.ChangeRequestTypeId = permRWBSD.ChangeRequestTypeId AND bcr.WorkBreakdownStructureId = permRWBSD.WorkBreakdownStructureId AND bcr.WorkflowLevel = permRWBSD.WorkflowLevel AND permRWBSD.PermissionId = swar.PermissionId
	INNER JOIN Security.PersonRoleWorkBreakdownStructureDetail persRWBSD ON bcr.SubSystemId = persRWBSD.SubSystemId AND permRWBSD.RoleId = persRWBSD.RoleId AND bcr.WorkBreakdownStructureId = persRWBSD.WorkBreakdownStructureId AND bcr.OriginatorPersonId = persRWBSD.PersonId
	WHERE swar.PermissionId = @submitPermissionId
	AND bcr.OriginatorPersonId = @PersonId;

	INSERT INTO @baselineInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, 
	                             ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT sbi.SubSystemId, sbi.SubSystemName, sbi.PermissionObjectId, sbi.ChangeRequestTypeId, 
		COUNT(DISTINCT ab.ChangeRequestDetailId) AS ItemCount, COALESCE(SUM(CASE WHEN ab.ThresholdId = 1 THEN 1 END), 0) DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN ab.ThresholdId = 2 THEN 1 END), 0) DaysWaitingTier2Count, COALESCE(SUM(CASE WHEN ab.ThresholdId = 3 THEN 1 END), 0) DaysWaitingTier3Count,
		MAX(ab.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT) AS HasLargeFinPlanInd
	FROM @subsystemBaselineInbox sbi
	OUTER APPLY
	(
		SELECT ab.SubSystemId, ab.ChangeRequestDetailId, ab.WorkflowLevel, ab.ChangeRequestTypeId, ab.WorkBreakdownStructureId, ab.OriginatorPersonId, ab.DaysAtLevel, t.Id AS ThresholdId
		FROM @inboxBaseline ib
		INNER JOIN @activeBaselines ab ON ab.ChangeRequestDetailId = ib.ChangeRequestDetailId
		INNER JOIN @thresholds t ON ab.DaysAtLevel >= t.Start AND ab.DaysAtLevel < t.[End]
		WHERE sbi.SubSystemId = ab.SubSystemId
		AND sbi.ChangeRequestTypeId = ab.ChangeRequestTypeId
	) ab
	GROUP BY GROUPING SETS ((sbi.SubSystemId, sbi.SubSystemName, sbi.PermissionObjectId),
							(sbi.SubSystemId, sbi.SubSystemName, sbi.PermissionObjectId, sbi.ChangeRequestTypeId));

	RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetAssetManagementInboxPermissionObjectId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/21/2017
-- Release:     8.2
-- Description: Gets the permission object id for asset management inbox.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetAssetManagementInboxPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 190;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAssetManagementInbox]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     8/21/2017
-- Release:     8.2
-- Description: Returns the asset management inbox for the specified user
--
-- mm/dd/yy  Name      Release  Description
-- 07/02/18  j.borgers 9.1      Changed where organization comes from
-- =============================================================
CREATE FUNCTION [G2].[fnGetAssetManagementInbox]
(
	@PersonId INT,
	@SubsystemList G2.GenericUniqueIdTable READONLY
)
RETURNS @assetManagementInbox TABLE
(
	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
)
AS
BEGIN
	--******************************DEBUG******************************
	----DECLARE @PersonId INT = 733,
	--DECLARE @PersonId INT = 208,
	----DECLARE @PersonId INT = 1210,
	--	@SubsystemList G2.GenericUniqueIdTable;

	--INSERT INTO @SubsystemList (Id) VALUES (3);
	--DECLARE @assetManagementInbox TABLE (
	--	SubSystemId INT, SubSystemName VARCHAR(50), PermissionObjectId INT, ChangeRequestTypeId INT, 
	--	ItemCount INT, DaysWaitingTier1 INT, DaysWaitingTier2 INT, DaysWaitingTier3 INT, LongestDaysWaiting INT, HasLargeFinPlanInd BIT
	--);
	--******************************DEBUG******************************

	DECLARE @assetManagementChangeRequestTypeGroupId INT = G2.fnGetAssetManagementChangeRequestTypeGroupId(),
		@assetManagementInboxPermissionObjectId INT = Security.fnGetAssetManagementInboxPermissionObjectId(),
		@viewPermissionId INT = Security.fnGetViewPermissionId(),
		@concurPermissionId INT = Security.fnGetConcurPermissionId(),
		@submitPermissionId INT = Security.fnGetSubmitPermissionId(),
		@showInInboxPermissionId INT = Security.fnGetShowInInboxPermissionId(),
		@nonResponseStatusId INT = G2.fnGetNonResponseChangeRequestDetailConcurrenceStatusId();
	
	DECLARE @thresholds TABLE
	(
		Id INT NOT NULL PRIMARY KEY CLUSTERED,
		Start INT NOT NULL,
		[End] INT NOT NULL
	);

	DECLARE @activeAssetManagementRequests TABLE
	(
		SubSystemId INT, ChangeRequestDetailId INT, WorkflowLevel INT, ChangeRequestTypeId INT, 
		ReportingEntityOrganizationPartyId INT, DaysAtLevel INT, CreatedAs INT
	);

	DECLARE @inboxAssetManagementRequests TABLE
	(
		ChangeRequestDetailId INT
	);

	DECLARE @subSystemAssetManagementInbox TABLE
	(
		SubSystemId INT,
		SubSystemName VARCHAR(50),
		PermissionObjectId INT,
		ChangeRequestTypeId INT
	);

	INSERT INTO @thresholds(Id, Start, [End])
	VALUES (1, 0, 2), (2, 2, 5), (3, 5, 9999);

	INSERT INTO @subSystemAssetManagementInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId)
	SELECT persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id
	FROM Security.PersonRole persR
	INNER JOIN Security.PermissionRole permR ON persR.SubSystemId = permR.SubSystemId AND persR.RoleId = permR.RoleId
	INNER JOIN G2.SubSystem ss ON ss.Id = permR.SubSystemId
	INNER JOIN @SubsystemList sl ON ss.Id = sl.Id
	CROSS JOIN G2.ChangeRequestTypeGroup crtg 
	INNER JOIN G2.ChangeRequestType crt ON crt.ChangeRequestTypeGroupId = crtg.Id
	WHERE persR.PersonId = @PersonId
	AND permR.PermissionId = @viewPermissionId
	AND permR.PermissionObjectId = @assetManagementInboxPermissionObjectId
	AND crtg.Id = @assetManagementChangeRequestTypeGroupId
	GROUP BY persR.SubSystemId, ss.ShortName, permR.PermissionObjectId, crt.Id;
	

	INSERT INTO @activeAssetManagementRequests (SubSystemId, ChangeRequestDetailId, WorkflowLevel, ChangeRequestTypeId, ReportingEntityOrganizationPartyId, DaysAtLevel, CreatedAs)
	SELECT cr.SubSystemId, crd.Id, crd.WorkflowLevel, crt.Id, s.OrganizationPartyId, DATEDIFF(DAY, crd.WorkflowLevelDt, CURRENT_TIMESTAMP) AS DaysAtLevel, crd.CreatedAs
	FROM G2.ChangeRequest cr
	INNER JOIN G2.ChangeRequestDetail crd ON cr.Id = crd.ChangeRequestId
	INNER JOIN G2.vwChangeRequestDetailStatusActiveStates acrs WITH (NOEXPAND) ON crd.ChangeRequestDetailStatusId = acrs.ChangeRequestDetailStatusId
	INNER JOIN G2.ChangeRequestType crt ON crd.ChangeRequestTypeId = crt.Id 
	INNER JOIN G2.ChangeRequestTypeGroup crtg ON crtg.Id = crt.ChangeRequestTypeGroupId
	INNER JOIN AssetManagement.Request r ON crd.Id = r.ChangeRequestDetailId
	INNER JOIN G2.Asset a ON r.AssetId = a.Id
	INNER JOIN MDI.AssetDetail ad ON ad.AssetId = a.Id
	LEFT OUTER JOIN AssetManagement.SiteArea sa ON sa.LocationId = ad.LocationId
	LEFT OUTER JOIN AssetManagement.[Site] s ON (s.LocationId = ad.LocationId OR s.LocationId = sa.SiteLocationId)
	WHERE crt.ChangeRequestTypeGroupId = @assetManagementChangeRequestTypeGroupId;


	INSERT INTO @inboxAssetManagementRequests (ChangeRequestDetailId)
	SELECT amr.ChangeRequestDetailId
	FROM @activeAssetManagementRequests amr
	INNER JOIN G2.ChangeRequestDetail crd ON amr.ChangeRequestDetailId = crd.Id
	INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON amr.SubSystemId = swar.SubSystemId AND amr.ChangeRequestTypeId = swar.ChangeRequestTypeId AND amr.WorkflowLevel = swar.WorkflowLevel
	WHERE amr.CreatedAs = @PersonId
	AND swar.PermissionId = @submitPermissionId

	UNION

	SELECT amr.ChangeRequestDetailId
	FROM @activeAssetManagementRequests amr
	INNER JOIN Security.PermissionRole permR ON amr.SubSystemId = permR.SubSystemId AND amr.ChangeRequestTypeId = permR.ChangeRequestTypeId AND amr.WorkflowLevel = permR.WorkflowLevel AND permR.RoleOnlyOverrideInd = 1
	INNER JOIN Security.PersonRole persR ON permR.SubSystemId = persR.SubSystemId AND permR.RoleId = persR.RoleId
	INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON permR.SubSystemId = swar.SubSystemId AND permR.ChangeRequestTypeId = swar.ChangeRequestTypeId AND permR.WorkflowLevel = swar.WorkflowLevel
	LEFT OUTER JOIN G2.ChangeRequestDetailConcurrence crdc ON amr.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND permR.WorkflowLevel = crdc.WorkflowLevel AND persR.PersonId = crdc.ForPersonId AND crdc.ActiveInd = 1 AND crdc.StatusId = @nonResponseStatusId
	WHERE permR.PermissionId = @showInInboxPermissionId
	AND persR.PersonId = @PersonId
	AND swar.PermissionId = @concurPermissionId
	AND crdc.Id IS NOT NULL

	UNION

	SELECT amr.ChangeRequestDetailId
	FROM @activeAssetManagementRequests amr
	INNER JOIN Security.PermissionRoleOrganizationBreakdownStructureDetail permROBSD ON amr.SubSystemId = permROBSD.SubSystemId AND amr.ChangeRequestTypeId = permROBSD.ChangeRequestTypeId AND amr.WorkflowLevel = permROBSD.WorkflowLevel AND amr.ReportingEntityOrganizationPartyId = permROBSD.OrganizationPartyId
	INNER JOIN Security.PersonRoleOrganizationBreakdownStructureDetail persROBSD ON permROBSD.SubSystemId = persROBSD.SubSystemId AND permROBSD.RoleId = persROBSD.RoleId AND amr.ReportingEntityOrganizationPartyId = persROBSD.OrganizationPartyId
	INNER JOIN G2.vwSubSystemWorkflowActionRequired swar WITH(NOEXPAND) ON permROBSD.SubSystemId = swar.SubSystemId AND permROBSD.ChangeRequestTypeId = swar.ChangeRequestTypeId AND permROBSD.WorkflowLevel = swar.WorkflowLevel
	LEFT OUTER JOIN G2.ChangeRequestDetailConcurrence crdc ON amr.ChangeRequestDetailId = crdc.ChangeRequestDetailId AND permROBSD.WorkflowLevel = crdc.WorkflowLevel AND persROBSD.PersonId = crdc.ForPersonId AND permROBSD.OrganizationPartyId = crdc.OrganizationPartyId AND crdc.ActiveInd = 1 AND crdc.StatusId = @nonResponseStatusId
	WHERE permROBSD.PermissionId = @showInInboxPermissionId
	AND persROBSD.PersonId = @PersonId
	AND swar.PermissionId = @concurPermissionId
	AND crdc.Id IS NOT NULL;


	INSERT INTO @assetManagementInbox (SubSystemId, SubSystemName, PermissionObjectId, ChangeRequestTypeId, ItemCount, DaysWaitingTier1, DaysWaitingTier2, DaysWaitingTier3, LongestDaysWaiting, HasLargeFinPlanInd)
	SELECT si.SubSystemId, si.SubSystemName, si.PermissionObjectId, NULL,
		COUNT(DISTINCT a.ChangeRequestDetailId) AS ItemCount,
		COALESCE(SUM(CASE WHEN a.ThresholdId = 1 THEN 1 END), 0) AS DaysWaitingTier1Count, 
		COALESCE(SUM(CASE WHEN a.ThresholdId = 2 THEN 1 END), 0) AS DaysWaitingTier2Count,
		COALESCE(SUM(CASE WHEN a.ThresholdId = 3 THEN 1 END), 0) AS DaysWaitingTier3Count,
		MAX(a.DaysAtLevel) AS LongestDaysWaiting, CAST(NULL AS BIT) AS HasLargeFinPlanInd
	FROM @subSystemAssetManagementInbox si
	OUTER APPLY (
		SELECT amr.DaysAtLevel, amr.ReportingEntityOrganizationPartyId, CAST(NULL AS INT) AS WorkBreakdownStructureId,
			amr.ChangeRequestTypeId, amr.WorkflowLevel, amr.ChangeRequestDetailId, amr.SubSystemId, t.Id AS ThresholdId
		FROM @inboxAssetManagementRequests i
		INNER JOIN @activeAssetManagementRequests amr ON i.ChangeRequestDetailId = amr.ChangeRequestDetailId
		INNER JOIN @thresholds t ON amr.DaysAtLevel >= t.Start AND amr.DaysAtLevel < t.[End]
		WHERE amr.ChangeRequestTypeId = si.ChangeRequestTypeId
		AND amr.SubSystemId = si.SubSystemId
	) a
	GROUP BY si.SubSystemId, si.SubSystemName, si.PermissionObjectId;

	RETURN;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSortByWBSPersonPreferenceChoice]'
GO

-- =============================================================
-- Author:      David Prenshaw
-- Created:     06/09/2015
-- Release:     6.1
-- Description:  Gets the "Sort projects by WBS" Person Preference choice for 
--               the specified person and will return true if it is turned On
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSortByWBSPersonPreferenceChoice](@PersonId INT)
RETURNS BIT
AS
BEGIN
	DECLARE @sortByWBSInd INT = 0;

	SELECT @sortByWBSInd = CONVERT(BIT, 1)
	FROM G2.PersonPreferenceChoice ppc
	WHERE ppc.PersonId = @PersonId
	AND ppc.PreferenceChoiceId = 3
	AND ppc.PreferenceChoiceValueId = 1;

	RETURN @sortByWBSInd;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnIsAllRequestsFilter]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/20/2013
-- Release:     4.4
-- Description: Function to determine if specified filter is
--   the "All Requests" filter.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnIsAllRequestsFilter]
(
	@Filter varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @match bit = 0;
	SET @match = CASE WHEN @Filter = 'All Requests' THEN 1 ELSE 0 END;
	RETURN @match;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetReceiveJouleMilestoneCompleteNotificationPreferenceChoiceId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     02/24/2014
-- Release:     5.0
-- Description: Retrieves the Receive Joule Milestone Complete
--   Notification preference choice id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetReceiveJouleMilestoneCompleteNotificationPreferenceChoiceId] ()
RETURNS int
AS
BEGIN
	RETURN 2; -- Receive Joule Milestone Complete Notification
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTertiaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     01/30/2017 
-- Release:     7.5
-- Description: Retrieves the Tertiary Group WorkBreakdownStructure Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTertiaryGroupWorkBreakdownStructureLabelSubSystemResourceTextName]()
RETURNS NVARCHAR(100)
AS 
BEGIN
	RETURN 'Tertiary Group WorkBreakdownStructure Label';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOptInPreferenceChoiceId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     02/24/2014
-- Release:     5.0
-- Description: Retrieves the Opt-in preference choice value id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetOptInPreferenceChoiceId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- Opt-in
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetApprovePermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/19/2013
-- Release:     4.4
-- Description: Retrieves the Approve permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetApprovePermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 14; -- Approve
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFiscalYearForFiscalPeriodId]'
GO
-- =============================================================
-- Author:      Ed Putkonen
-- Created:     06/20/2018
-- Release:     9.1  
-- Description: Returns fiscal year from fiscal period
--
-- mm/dd/yy  Name      Release  Description
--
-- =============================================================

CREATE FUNCTION [G2].[fnGetFiscalYearForFiscalPeriodId]
(
	@PeriodId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @fy INT;

	SELECT @fy = per.FiscalYear
	FROM G2.Period per
	WHERE per.id = @PeriodId;

	RETURN @fy;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationProcessStatusParmKey]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     12/1/2015
-- Release:     6.4
-- Description: Get the Parm Key for Budget Formulation Process Status
--   period.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationProcessStatusParmKey] (
)
RETURNS NVarChar(100)
AS
BEGIN
	return 'Budget Formulation Process Status';
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInternationalMO99WorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     05/29/2016
-- Release:     7.1
-- Description: Retrieves the International MO-99 work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
 CREATE FUNCTION [G2].[fnGetInternationalMO99WorkTypeId] 
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 6;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name LIKE 'International Mo-99%';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- International MO-99 / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetActivitySiteFinancialOfficerRoleId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     06/25/2018
-- Release:     9.1
-- Description: Support Activity Funding
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetActivitySiteFinancialOfficerRoleId]()
RETURNS INT
AS
BEGIN
	RETURN 214;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetActivityHQFinancialOfficerRoleId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     06/25/2018
-- Release:     9.1
-- Description: Support Activity Funding
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetActivityHQFinancialOfficerRoleId]()
RETURNS INT
AS
BEGIN
	RETURN 215;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetTravelerRoleId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     4/4/2014
-- Release:     5.0
-- Description: Retrieves the id of Traveler.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetTravelerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 69; -- Traveler
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetMO99ScorecardExclusionWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     05/31/2015
-- Release:     7.1
-- Description: Returns the Mo-99: Scorecard Exclusion workbreakdownstructure attribute type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetMO99ScorecardExclusionWorkBreakdownStructureAttributeTypeId] ()
RETURNS int
AS
BEGIN
  RETURN 28; -- Mo-99: Scorecard Exclusion
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCostSubmissionLateDayParmKey]'
GO


-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/14/2014
-- Release:     5.0
-- Description: Retrieves the Cost Submission Late Day parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCostSubmissionLateDayParmKey] ()
RETURNS VARCHAR(50)
AS
BEGIN
	RETURN 'Cost Submission Late Day';
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPlanningProcessStatus]'
GO

CREATE FUNCTION [G2].[fnGetPlanningProcessStatus]
(
)
RETURNS
nvarchar(100)
AS
BEGIN
	DECLARE @status nvarchar(100)

	SELECT @status = CONVERT(nvarchar(100), value)
	FROM G2.G2Parameters gp
	WHERE gp.ParmKey='Planning Process Status';

	RETURN @status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetGeneralSystemStakeholderRoleId]'
GO


-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     6/29/2017
-- Release:     8.1
-- Description: Retrieves the id of General System Stakeholder.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetGeneralSystemStakeholderRoleId] ( )
RETURNS INT
AS
    BEGIN
        RETURN 159; -- General: System Stakeholder
    END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAssetManagementChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     08/08/2017 
-- Release:     8.2
-- Description: Returns the Asset Management Change Request Type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAssetManagementChangeRequestTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 39; --Asset Management
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA23USHPRRWorkTypeCategoryId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     04/17/2017
-- Release:     8.0
-- Description: Retrieves the USHPRR work type category id.
--
-- mm/dd/yy  Name      Release    Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA23USHPRRWorkTypeCategoryId] ()
RETURNS int
AS
BEGIN
	RETURN 17; -- USHPRR / Work Type Category
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA23ConvertWorkTypeCategoryId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     04/05/2017
-- Release:     8.0
-- Description: Retrieves the Convert work type category id.
--
-- mm/dd/yy  Name      Release    Description
-- 04/11/17  a.smith     8.0 PROD Updated id to 13 from 20 (20 was deleted)
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA23ConvertWorkTypeCategoryId] ()
RETURNS int
AS
BEGIN
	RETURN 13; -- Convert / Work Type Category
END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnSubSystemLevelFilterTypeXrefCheckSubSystemMatch]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/09/2014 
-- Release:     5.3
-- Description: Makes sure that SubSystem in SubSystemResourceText matches
--				the SubSystem in SubSystemLevel.  
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnSubSystemLevelFilterTypeXrefCheckSubSystemMatch] (
@SubSystemLevelFilterTypeXrefId int
)
RETURNS bit
AS
BEGIN
	DECLARE @InvalidInd bit;

	SELECT @InvalidInd = CASE WHEN ssl.SubSystemId = ssrt.SubSystemId THEN 0 ELSE 1 END
	FROM G2.FilterTypeConfiguration sslftx
	INNER JOIN G2.SubSystemLevel ssl ON sslftx.SubSystemLevelId = ssl.Id
	INNER JOIN G2.SubSystemResourceText ssrt ON sslftx.SubSystemResourceTextId = ssrt.Id
	WHERE sslftx.Id = @SubSystemLevelFilterTypeXrefId;

  RETURN @InvalidInd; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProgramAdministratorRoleId]'
GO

-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/17/2014
-- Release:     5.3
-- Description: Retrieves the Schedule Program Administrator Role
--              for a given SubSystem
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleProgramAdministratorRoleId] (@SubSystemId INT)
RETURNS INT
AS
BEGIN

	/* Debug Declarations */
		--DECLARE @SubSystemId INT = 3;
	/* End Debug Declarations */

		DECLARE @scheduleProgramAdministratorRoleId INT;

		SET @scheduleProgramAdministratorRoleId = (SELECT CASE WHEN @SubSystemId = 1 THEN 8
															   WHEN @SubSystemId = 3 THEN 111
															   ELSE NULL
														   END );

		RETURN @scheduleProgramAdministratorRoleId;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNFWAPeriodAdvancementDayParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the NFWA Period Advancement Day
--   parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNFWAPeriodAdvancementDayParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'NFWA Period Advancement Day';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetMinimumNFWAPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the Minimum NFWA Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetMinimumNFWAPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Minimum NFWA Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetMinimumLocalAFPPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the Minimum Local AFP Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetMinimumLocalAFPPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Minimum Local AFP Period';
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetLocalAFPPeriodAdvancementDayParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the Local AFP Period Advancement Day
--   parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetLocalAFPPeriodAdvancementDayParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Local AFP Period Advancement Day';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentNFWAPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the Current NFWA Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentNFWAPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current NFWA Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentLocalAFPPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     03/18/2014
-- Release:     5.0
-- Description: Retrieves the Current Local AFP Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentLocalAFPPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Local AFP Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectSustainabilityDomesticWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect Sustainability Domestic work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectSustainabilityDomesticWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect Sustainability Domestic';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Sustainability Domestic / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNotVisibleInfrastructurePlanningFieldConfigurationTypeId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     08/12/2016 
-- Release:     7.2
-- Description: Returns the Not Visible InfrastructurePlanningFieldConfigurationType Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNotVisibleInfrastructurePlanningFieldConfigurationTypeId]()
RETURNS int
AS
BEGIN
  RETURN 1; --Not Visible
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInfrastructurePlanningPropertyTypeWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     03/05/2018 
-- Release:     8.5
-- Description: Returns the Infrastructure Planning Property WorkBreakdownStructure Attribute Type Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetInfrastructurePlanningPropertyTypeWorkBreakdownStructureAttributeTypeId]()
RETURNS int
AS
BEGIN
  RETURN 32; --InfrastucturePlanningPropertyType
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnSubSystemLevelFilterTypeCheck]'
GO


-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     09/09/2014 
-- Release:     5.3
-- Description: Makes sure that a Filter Type is only used once per
--				SubSystem.  
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnSubSystemLevelFilterTypeCheck] (
@FilterTypeId int,
@SubSystemLevelId int
)
RETURNS bit
AS
BEGIN
	DECLARE @InvalidInd bit;

	SELECT @InvalidInd = CASE WHEN COUNT(*) > 1 THEN 1 ELSE 0 END
	FROM G2.SubSystemLevel ssl
	INNER JOIN G2.SubSystemLevel ssl2 ON ssl.SubSystemId = ssl2.SubSystemId
	INNER JOIN G2.FilterTypeConfiguration sslftx ON ssl2.Id = sslftx.SubSystemLevelId
	WHERE ssl.Id = @SubSystemLevelId
	AND sslftx.FilterTypeId = @FilterTypeId
	GROUP BY sslftx.FilterTypeId

  RETURN @InvalidInd; 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetProtectSustainabilityInternationalWorkTypeId]'
GO
-- =============================================================
-- Author:      Adela E. Smith
-- Created:     08/28/2015
-- Release:     6.2
-- Description: Retrieves the Protect Sustainability International work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetProtectSustainabilityInternationalWorkTypeId]
(
	@SubSystemId INT
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 5;
	/* Debug Declarations */
	DECLARE @WorkTypeId INT;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Protect Sustainability International';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Protect Sustainability International / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveInternationalWorkTypeId]'
GO


-- =============================================================
-- Author:      Byron Roland
-- Created:     8/28/15
-- Release:     6.2
-- Description: Retrieves the Remove International work type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveInternationalWorkTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId int;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Remove International';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove RAD International / Work Type
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTaskPrimaryGroupLabelSubSystemResourceTextName]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/3/2014
-- Release:     5.3
-- Description: Retrieves the key for the Task Primary Group
--   Label in the G2.SubSystemResourceText table.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTaskPrimaryGroupLabelSubSystemResourceTextName]()
RETURNS nvarchar(100)
AS 
BEGIN
	RETURN 'Task Primary Group Label';
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSpendPlanConfigurationTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/06/2017 
-- Release:     7.5
-- Description: Returns the SpendPlan ConfigurationType Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSpendPlanConfigurationTypeId]()
RETURNS int
AS
BEGIN
  RETURN 9; --SpendPlan
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPerformanceReportingChangeRequestPermissionObjectId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     03/28/2016
-- Release:     7.0
-- Description: Retrieves the Performance Reporting Change Request permission
--   object id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPerformanceReportingChangeRequestPermissionObjectId] ()
RETURNS INT
AS
BEGIN
	RETURN 16; -- Performance Reporting Change Request
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetStandardTertiaryFilterTypeId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     01/30/2017 
-- Release:     7.5
-- Description: Returns the Filter Type Id for the Tertiary Primary Filter.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetStandardTertiaryFilterTypeId]()
RETURNS INT
AS
BEGIN
  RETURN 5; --Standard Tertiary Filter
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFirstChildSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     9/22/2016
-- Release:     7.3
-- Description: Gets the first child subsystem level Id for a subsystem
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetFirstChildSubSystemLevelId]
(
	@SubSystemId INT 
)
RETURNS INT
AS
BEGIN
	DECLARE @parentSubSystemLevelId INT, @childSubSystemLevelId INT;

	-- gets top level parent
	SELECT @parentSubSystemLevelId = sslh.ChildSubSystemLevelId
	FROM G2.SubSystemLevelHierarchy sslh
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = sslh.ChildSubSystemLevelId
	WHERE ssl.SubSystemId = @SubSystemId
	AND sslh.ParentSubSystemLevelId IS NULL;

	--gets first child
	SELECT @childSubSystemLevelId = sslh.ChildSubSystemLevelId FROM G2.SubSystemLevelHierarchy sslh
	INNER JOIN G2.SubSystemLevel ssl ON ssl.Id = sslh.ChildSubSystemLevelId
	WHERE ssl.SubSystemId = @SubSystemId
	AND sslh.ParentSubSystemLevelId = @parentSubSystemLevelId;

	RETURN @childSubSystemLevelId;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetAssociateAssistantDeputyAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/11/2014
-- Release:     5.0
-- Description: Retrieves the Associate Assistant Deputy Administrator role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetAssociateAssistantDeputyAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 15; -- Associate Assistant Deputy Administrator
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetRemoveDomesticWorkTypeId]'
GO



-- =============================================================
-- Author:      Byron Roland
-- Created:     8/28/15
-- Release:     6.2
-- Description: Retrieves the Remove Domestic work type id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetRemoveDomesticWorkTypeId]
(
	@SubSystemId int
)
RETURNS int
AS
BEGIN
	/* Debug Declarations */
	--DECLARE @SubSystemId INT = 1;
	/* Debug Declarations */
	DECLARE @WorkTypeId int;

	SELECT @WorkTypeId = wt.Id
	FROM G2.WorkType wt
	INNER JOIN G2.WorkTypeGroup wtg ON wt.WorkTypeGroupId = wtg.Id
	WHERE wtg.SubSystemId = @SubSystemId
	AND wt.Name = 'Remove Domestic';

	/* Debug Declarations */
	--SELECT @WorkTypeId;
	/* Debug Declarations */

	RETURN @WorkTypeId; -- Remove RAD International / Work Type
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnBuildDashboardProjectSubfunctionStr]'
GO

CREATE FUNCTION [G2].[fnBuildDashboardProjectSubfunctionStr]
(
)
RETURNS
varchar(50)
AS
BEGIN
	DECLARE @retVal varchar(50);
	DECLARE @count int;
	DECLARE @id int;
	SET @retVal = NULL

	DECLARE @tempDashboardPSF AS TABLE (id int);

	INSERT INTO @tempDashboardPSF 
		SELECT id FROM WorkType WHERE DashboardFilterInd = 1

	SELECT @count = COUNT(id) FROM @tempDashboardPSF
	WHILE @count > 0
	BEGIN
		SELECT TOP 1 @id=id FROM @tempDashboardPSF	ORDER BY id
		IF @retVal IS NULL
			SET @retVal = CONVERT(varchar, @id)
		ELSE
			SET @retVal = @retVal + '|' + CONVERT(varchar, @id)

		DELETE FROM @tempDashboardPSF WHERE id = @id

		SELECT @count = COUNT(id) FROM @tempDashboardPSF
	END

	RETURN @retVal
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAnnouncementsPermissionObjectId]'
GO

-- =============================================================
-- Author:      Mark Brown
-- Created:     2/18/2016
-- Release:     6.5
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAnnouncementsPermissionObjectId]()
RETURNS INT
AS
BEGIN
	RETURN 45;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetAnnouncementFeatureTypeId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     2/18/2016
-- Release:     6.5	
-- Description: 
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetAnnouncementFeatureTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 1;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnMonthBegin]'
GO
-- =============================================
-- Author:		Chris Luttrell
-- Create date: 8/23/06
-- Description:	Returns the first second of the first day of the month based on the date passed in
-- =============================================
CREATE FUNCTION [G2].[fnMonthBegin] 
(

	@theDay datetime = null
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	set @theDay = dateadd(m, datediff(m,0,@theDay),0)


	RETURN @theDay

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentBaselineThruPeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/4/2014
-- Release:     5.3
-- Description: Retrieves the Current Baseline Thru Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentBaselineThruPeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Baseline Thru Period';
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCurrentBaselinePeriodParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/4/2014
-- Release:     5.3
-- Description: Retrieves the Current Baseline Period parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCurrentBaselinePeriodParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Current Baseline Period';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBaselinePeriodAdvancementDayParmKey]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/4/2014
-- Release:     5.3
-- Description: Retrieves the Baseline Period Advancement Day
--   parmkey.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBaselinePeriodAdvancementDayParmKey] ()
RETURNS varchar(50)
AS
BEGIN
	RETURN 'Baseline Period Advancement Day';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetQAStaffRoleId]'
GO
-- =============================================================
-- Author:      Byron Roland
-- Created:     6/19/18
-- Release:     9.1
-- Description: Retrieves the QA Staff RoleId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetQAStaffRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 71; -- QA Staff
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Administrator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPAAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 133;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDeleteConcurrenceChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/18/2014
-- Release:     5.0
-- Description: Retrieves the Delete Concurrence change request
--   action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDeleteConcurrenceChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 15; -- Delete Concurrence
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetShipmentNumberWorkBreakdownStructureAttributeTypeId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     10/5/2015
-- Release:     5.3
-- Description: Returns WorkBreakdownStructureAttributeType Id for Shipment Number.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetShipmentNumberWorkBreakdownStructureAttributeTypeId]()
RETURNS INT
AS
BEGIN
    RETURN 10;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConcurChangeRequestActionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Concur change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetConcurChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 20; -- Concur
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleSubProgramManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2015
-- Release:     6.3.1
-- Description: Retrieves the Schedule: Sub Program Manager (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleSubProgramManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 105; -- Schedule: Sub Program Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleSubOfficeDirectorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2015
-- Release:     6.3.1
-- Description: Retrieves the Schedule: Sub-Office Director (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleSubOfficeDirectorPrimaryRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 123; -- Schedule: Sub-Office Director (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProjectStaffRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     02/18/2016
-- Release:     5.3
-- Description: Get the Schedule Project Staff role id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleProjectStaffRoleId]()
RETURNS INT
AS
BEGIN
    RETURN 166;
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanSubProgramManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2015
-- Release:     6.3.1
-- Description: Retrieves the FinPlan: Sub Program Manager (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanSubProgramManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 84; -- FinPlan: Sub Program Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanSubOfficeDirectorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     11/19/2015
-- Release:     6.3.1
-- Description: Retrieves the FinPlan: Sub-Office Director (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanSubOfficeDirectorPrimaryRoleId] ()
RETURNS INT
AS
BEGIN
	RETURN 119; -- Schedule: Sub-Office Director (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetResetChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Reset change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetResetChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 14; -- Reset
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNonConcurChangeRequestActionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     2/21/2014
-- Release:     5.0
-- Description: Retrieves the Non-Concur change request action id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNonConcurChangeRequestActionId] ()
RETURNS int
AS
BEGIN
	RETURN 24; -- Non-Concur
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSubPortfolioSubSystemLevelId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     08/09/2017 
-- Release:     8.2
-- Description: Returns the subsystemlevel id for Sub-Portfolio
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetSubPortfolioSubSystemLevelId]
(
	@SubSystemId INT
)
RETURNS INT
AS
BEGIN
  RETURN (SELECT ssl.Id
			FROM G2.SubSystemLevel ssl
			WHERE ssl.Label = 'Sub-Portfolio'
			AND ssl.SubSystemId = @SubSystemId); --Sub-Portfolio
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryProjectManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     04/08/2016
-- Release:     7.0
-- Description: Retrieves the id of the Inventory Project Manager (Secondary) role.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryProjectManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 175; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryProjectManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     04/08/2016
-- Release:     7.0
-- Description: Retrieves the id of the Inventory Project Manager (Primary) role.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryProjectManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 174; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryPortfolioManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     04/08/2016
-- Release:     7.0
-- Description: Retrieves the id of the Inventory Portfolio Manager (Secondary) role.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryPortfolioManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 173; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryPortfolioManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     04/08/2016
-- Release:     7.0
-- Description: Retrieves the id of the Inventory Portfolio Manager (Primary) role.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryPortfolioManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 172; 
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/08/2014
-- Release:     5.0
-- Description: Retrieves the id of the Inventory Administrator role.
--
-- mm/dd/yy  Name        Release  Description
-- 08/07/15  Strickland  6.2.1    Update for new roles
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 141; 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetTechnicalDirectorRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the Technical Director role ID
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetTechnicalDirectorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 10; -- Technical Director
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetHQProjectControlsRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the HQ Project Controls role ID
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetHQProjectControlsRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- HQ Project Controls
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetHQProgramDirectionPOCRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the HQ Program Direction POC Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetHQProgramDirectionPOCRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 50; -- HQ Program Direction POC
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFunctionalOfficeManagerRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the Functional Office Manager Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetFunctionalOfficeManagerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 53; -- FOM
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFieldOfficeManagerRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the FOM Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetFieldOfficeManagerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 51; -- FOM
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetDeputyAssociateAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the Deputy Associate Administrator Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetDeputyAssociateAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 56; -- DAA
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetBudgetOfficerRoleId]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     6/13/2014
-- Release:     4.4
-- Description: Retrieves the id of Budget Officer
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetBudgetOfficerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 3; -- General User
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetAssociatePrincipalDeputyAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the APDA Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetAssociatePrincipalDeputyAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 60; -- APDA
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetAssociateAdministratorRoleId]'
GO
-- =============================================================
-- Author:      Karl Allen
-- Created:     5/30/2014
-- Release:     5.1
-- Description: Retrieves the Associate Administrator Role Id
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetAssociateAdministratorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 55; -- AA
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFundingRequestProcessCreationStatusParmKey]'
GO
-- =============================================================
-- Author:      Nathan Coffey
-- Created:     11/11/2016
-- Release:     7.4
-- Description: Returns the Funding Request Process Creation Status ParmKey
--
-- mm/dd/yy  Name     Release  Description
-- 04/18/17  JDR      8.0      Modified to facilitate renaming of Funding Request objects
-- =============================================================
CREATE FUNCTION [G2].[fnGetFundingRequestProcessCreationStatusParmKey]()
RETURNS
VARCHAR(50)
AS
BEGIN
	RETURN 'Funding Request Creation Status';
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnConvert_Lat_Lon_Degrees_to_Decimal]'
GO

CREATE FUNCTION [G2].[fnConvert_Lat_Lon_Degrees_to_Decimal]
(
	@Latitude nvarchar(250),
	@Longitude nvarchar(250)
)
RETURNS
@Conversion table
(
	NewLatitude decimal(18,10),
	NewLongitude decimal(18,10)
)
AS
BEGIN
	--Declaring local variables
	DECLARE @latdeg nvarchar(250), @latmin nvarchar(250), @latsec nvarchar(250);
	DECLARE @londeg nvarchar(250), @lonmin nvarchar(250), @lonsec nvarchar(250);
	DECLARE @finlat decimal(18,10), @finlon decimal(18,10);
	DECLARE @latposneg int, @lonposneg int;
	
	--Parsing the degrees
	SET @latdeg = RTRIM(LTRIM(SUBSTRING(@Latitude, 1, CHARINDEX('�', @Latitude) - 1)));
	SET @londeg = RTRIM(LTRIM(SUBSTRING(@Longitude, 1, CHARINDEX('�', @Longitude) - 1)));

	--Parsing the minutes
	SET @latmin = RTRIM(LTRIM(SUBSTRING(@Latitude, CHARINDEX('�', @Latitude) + 1, (CHARINDEX('''', @Latitude) - CHARINDEX('�', @Latitude)) - 1)));
	SET @lonmin = RTRIM(LTRIM(SUBSTRING(@Longitude, CHARINDEX('�', @Longitude) + 1, (CHARINDEX('''', @Longitude) - CHARINDEX('�', @Longitude)) - 1)));

	--Parsing the seconds
	SET @latsec = RTRIM(LTRIM(SUBSTRING(@Latitude, CHARINDEX('''', @Latitude) + 1, (CHARINDEX('"', @Latitude) - CHARINDEX('''', @Latitude)) - 1)));
	SET @lonsec = RTRIM(LTRIM(SUBSTRING(@Longitude, CHARINDEX('''', @Longitude) + 1, (CHARINDEX('"', @Longitude) - CHARINDEX('''', @Longitude)) - 1)));

	--Determining if the latitutde/longitude should be positive or negative
	--	South -> Negative, North -> Positive
	--	West  -> Negative, East  -> Positive
	SET @latposneg = CASE WHEN @Latitude LIKE '%S' THEN -1 ELSE 1 END;
	SET @lonposneg = CASE WHEN @Longitude LIKE '%W' THEN -1 ELSE 1 END;

	--Converting the degrees, minutes and seconds to a decimal
	--	60 minutes in 1 degree, 60 seconds in 1 minute
	SET @finlat = (CONVERT(decimal(18,10), @latdeg) + (CONVERT(decimal(18,10), @latmin) + CONVERT(decimal(18,10), @latsec) / 60) / 60) * @latposneg
	SET @finlon = (CONVERT(decimal(18,10), @londeg) + (CONVERT(decimal(18,10), @lonmin) + CONVERT(decimal(18,10), @lonsec) / 60) / 60) * @lonposneg

	INSERT INTO @Conversion (NewLatitude, NewLongitude) VALUES (@finlat, @finlon)

	RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetActiveAndCancelledWorkBreakdownStructureStatusStateId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Retrieves the Active and Cancelled
--   WorkBreakdownStructureStatusStateId.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetActiveAndCancelledWorkBreakdownStructureStatusStateId]()
RETURNS int
AS
BEGIN
	RETURN 3; -- Active and Cancelled
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetActiveAndOpenRequestWorkBreakdownStructureStatusStateId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Retrieves the Active and Open Request
--   WorkBreakdownStructureStatusStateId.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetActiveAndOpenRequestWorkBreakdownStructureStatusStateId]()
RETURNS int
AS
BEGIN
	RETURN 5; -- Active and Open Request
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetActiveWorkBreakdownStructureStatusStateId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Retrieves the Active
--   WorkBreakdownStructureStatusStateId.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetActiveWorkBreakdownStructureStatusStateId]()
RETURNS int
AS
BEGIN
	RETURN 1; -- Active
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationIndirectsChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     11/20/2015
-- Release:     6.4
-- Description: Retrieves the Budget Formulation Indirects
--   Change Request Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationIndirectsChangeRequestTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 33; -- Budget Formulation Indirects
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetBudgetFormulationOtherDirectFundsChangeRequestTypeId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     12/1/2016
-- Release:     7.4
-- Description: Retrieves the Budget Formulation Other Direct Costs
--   Change Request Type Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetBudgetFormulationOtherDirectFundsChangeRequestTypeId] ()
RETURNS INT
AS
BEGIN
	RETURN 38; -- Budget Formulation Other Direct Funds
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCompetitiveChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/15/2018 
-- Release:     8.5
-- Description: Returns the Competitive ChangeRequestTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCompetitiveChangeRequestTypeId]()
RETURNS int
AS
BEGIN
  RETURN 40; --Competitive
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetContaminantsImpactQuestionnaireQuestionId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     10/11/2017 
-- Release:     8.3
-- Description: Returns the contaminants impact ERI questionnaire question id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION  [G2].[fnGetContaminantsImpactQuestionnaireQuestionId]()
RETURNS INT
AS
BEGIN
  RETURN 114; --'What is the potential impact of contaminants?' 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetConversionWorkPriortoOct2015WorkTypeId]'
GO

-- =============================================================
-- Author:      Adela E. Smith
-- Created:     10/27/2015
-- Release:     6.3
-- Description: Retrieves the Convert work type id.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetConversionWorkPriortoOct2015WorkTypeId] ()
RETURNS int
AS
BEGIN
	RETURN 45; -- Conversion Work Prior to Oct 2015 / Work Type
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetCountryFilterPermissionObjectId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     2/10/2017
-- Release:     7.5
-- Description: Returns the country filter permission object
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetCountryFilterPermissionObjectId]()
RETURNS INT
AS
BEGIN
    RETURN 186;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetDirectedChangeRequestTypeId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     02/15/2018 
-- Release:     8.5
-- Description: Returns the Directed ChangeRequestTypeId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetDirectedChangeRequestTypeId]()
RETURNS int
AS
BEGIN
  RETURN 41; --Directed
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetFirstWordfromString]'
GO

-- =============================================
-- Author:		Diedre Cantrell
-- Create date: 03/09/2012
-- Description:	Extract first word from string 
-- =============================================
CREATE FUNCTION [G2].[fnGetFirstWordfromString]
(
	@String NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @Result NVARCHAR(MAX);

	SELECT @Result = LTRIM(LEFT(@String,PATINDEX('% %',LTRIM(@String+' '))))					

	RETURN @Result

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetInactiveWorkBreakdownStructureStatusStateId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Retrieves the Inactive
--   WorkBreakdownStructureStatusStateId.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetInactiveWorkBreakdownStructureStatusStateId]()
RETURNS int
AS
BEGIN
	RETURN 2; -- Inactive
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA193SubSystemId]'
GO
-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     08/04/2015 
-- Release:     6.2
-- Description: Function to get the NA-193 SubSystemId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetNA193SubSystemId]()
RETURNS int
AS
BEGIN
  RETURN 7; --NA-193 CBI
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetNA80SubSystemId]'
GO
-- =============================================================
-- Author:      Christopher Luttrell (C5\Chris.Luttrell)
-- Created:     08/23/2017
-- Release:     8.2
-- Description: Function to get the NA-80 SubSystemId
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE   FUNCTION [G2].[fnGetNA80SubSystemId]()
RETURNS int
AS
BEGIN
  RETURN 9; --NA-80
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetOpenRequestWorkBreakdownStructureStatusStateId]'
GO
-- =============================================================
-- Author:      Sara O'Neal
-- Created:     09/18/2014
-- Release:     5.3
-- Description: Retrieves the Open Request
--   WorkBreakdownStructureStatusStateId.
--
-- mm/dd/yy  Name        Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetOpenRequestWorkBreakdownStructureStatusStateId]()
RETURNS int
AS
BEGIN
	RETURN 4; -- Open Request
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetPlanningAndCoordinationMilestoneTemplateCategoryId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/3/2014
-- Release:     5.0
-- Description: Retrieves the Planning & Coordination Milestone Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetPlanningAndCoordinationMilestoneTemplateCategoryId]()
RETURNS int
AS
BEGIN
	RETURN 1; -- Planning & Coordination
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetReceiveReportSubscriptionPermissionId]'
GO
-- =============================================================
-- Author:      Diedre Cantrell
-- Created:     11/3/2014
-- Release:     5.3
-- Description: Retrieves the Receive Report Subscription PermissionId.
--
-- mm/dd/yy  Name      Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetReceiveReportSubscriptionPermissionId] ()
RETURNS INT
AS
BEGIN
	RETURN 66; /*Receive Report Subscription*/
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSiteLevelSupportConcludedMilestoneId]'
GO
-- =============================================================
-- Author:      Mike Holm
-- Created:     08/27/2018 
-- Release:     9.2
-- Description: Returns the Milestone ID for Site Level Support Concluded
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSiteLevelSupportConcludedMilestoneId] ( )
RETURNS INT
AS
    BEGIN
        RETURN 1300; --Site Level Support Concluded
    END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSiteLevelTransitionMilestoneId]'
GO
-- =============================================================
-- Author:      Mike Holm
-- Created:     08/27/2018 
-- Release:     9.2
-- Description: Returns the Milestone ID for Site Level Transition
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSiteLevelTransitionMilestoneId] ( )
RETURNS INT
AS
    BEGIN
        RETURN 1301; --Site Level Transition
    END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetSpendPlanChangeRequestTypeGroupId]'
GO

-- =============================================================
-- Author:      Jocelyn Borgers
-- Created:     03/13/2014 
-- Release:     5.0
-- Description: Returns the Spend Plan change request type group Id
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetSpendPlanChangeRequestTypeGroupId] ()
RETURNS int
AS
BEGIN
  RETURN 9; --Spend Plan
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnGetTotalScoreTypeId]'
GO
-- =============================================================
-- Author:      Nicole Harris
-- Created:     08/12/2016
-- Release:     7.2
-- Description: Retrieves total score - score type id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [G2].[fnGetTotalScoreTypeId]()
RETURNS INT
AS
BEGIN
	RETURN 21; -- Total Score - score type id.
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [G2].[fnRptLessThan20WorkingDays]'
GO



-- =============================================
-- Author:		Diedre Cantrell (per Article)
-- Create date: 4/9/2010
-- Description:	Returns a 'Y' if difference in number of working days based 
--     on the Start and End dates <= 20 days
-- =============================================
CREATE FUNCTION [G2].[fnRptLessThan20WorkingDays] 
      ( 
         @StartDate DATETIME, 
         @EndDate   DATETIME = NULL --@EndDate replaced by @StartDate when DEFAULTed 
        ) 
 
RETURNS NVARCHAR(1) 
 
AS 

  BEGIN 

--DECLARE @StartDate DATETIME, @EndDate DATETIME;
--SET @StartDate = '4/1/2010';
--SET @EndDate = '4/27/2010';


        DECLARE @Swap DATETIME ;
        DECLARE @NumDays INT;
        DECLARE @Answer NVARCHAR(1);

        IF @StartDate IS NULL 
        RETURN NULL 
 
        
         IF @EndDate IS NULL 
            SELECT @EndDate = @StartDate 
 
        /* Strip the time element from both dates (just to be safe) by converting 
              to whole days and back to a date.  Usually faster than CONVERT. 
              0 is a date (01/01/1900 00:00:00.000) */
         SELECT @StartDate = DATEADD(dd,DATEDIFF(dd,0,@StartDate),0), 
           @EndDate   = DATEADD(dd,DATEDIFF(dd,0,@EndDate)  ,0) 
 
        /* If the inputs are in the wrong order, reverse them */
             IF @StartDate > @EndDate 
                SELECT @Swap      = @EndDate, 
                       @EndDate   = @StartDate, 
                       @StartDate = @Swap 
 
       /* Calculate and return the number of workdays */

     
                SELECT @NumDays = 
              --Start with total number of days including weekends 
                (DATEDIFF(dd,@StartDate,@EndDate)+1) 
 
              --Subtact 2 days for each full weekend 
               -(DATEDIFF(wk,@StartDate,@EndDate)*2) 
 
              --If StartDate is a Sunday, Subtract 1 
               -(CASE WHEN DATENAME(dw,@StartDate) = 'Sunday' 
                      THEN 1 
                      ELSE 0 
                  END) 
 
              --If EndDate is a Saturday, Subtract 1 
               -(CASE WHEN DATENAME(dw,@EndDate) = 'Saturday' 
                      THEN 1 
                      ELSE 0 
                  END) 
                  
              SET @Answer = (SELECT CASE	WHEN @NumDays < 20 
										THEN 'Y'
										ELSE 'N'
								END Answer)
              
                --SELECT @Answer ; 
              RETURN @Answer;
    END 


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCancelPermissionId]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     6/4/2013
-- Release:     4.2
-- Description: Retrieves the Cancel permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCancelPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 63; -- Cancel
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetClaimPermissionId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     9/20/2013
-- Release:     4.4
-- Description: Retrieves the Claim permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetClaimPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 40; -- Claim
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCostsSiteFinancialOfficerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     02/26/2016
-- Release:     6.5
-- Description: Returns the value for the Costs: Site Financial Officer (Primary)
--              Role Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCostsSiteFinancialOfficerPrimaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 93;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetCostsSiteFinancialOfficerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     02/26/2016
-- Release:     6.5
-- Description: Returns the value for the Costs: Site Financial Officer (Secondary)
--              Role Id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetCostsSiteFinancialOfficerSecondaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 94;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetDeputyOfficeDirectorRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     2/24/2015
-- Release:     5.5
-- Description: Retrieves the Deputy Office Director role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetDeputyOfficeDirectorRoleId] ()
RETURNS int
AS
BEGIN

	RETURN 36; -- Deputy Office Director
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetDeputyTechnicalDirectorRoleId]'
GO
-- =============================================================
-- Author:      Adela Smith
-- Created:     8/13/2015
-- Release:     6.2
-- Description: Retrieves the Deputy Technical Director role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetDeputyTechnicalDirectorRoleId] ()
RETURNS int
AS
BEGIN

	RETURN 37; -- Deputy Office Director
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanExecutiveManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/11/2016
-- Release:     7.3
-- Description: Retrieve the role id for FinPlan Executive Manager Primary.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanExecutiveManagerPrimaryRoleId]()
RETURNS int
AS
BEGIN
	RETURN 88;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanOfficeDirectorPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Office Director (Primary)
--              Role Id. This is NOT a subsystem-specific function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanOfficeDirectorPrimaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 117;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanOfficeDirectorSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Office Director (Primary)
--              Role Id. This is NOT a subsystem-specific function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanOfficeDirectorSecondaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 118;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanProgramManagerPrimaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Office Director (Primary)
--              Role Id. This is NOT a subsystem-specific function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanProgramManagerPrimaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 86;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetFinPlanProgramManagerSecondaryRoleId]'
GO

-- =============================================================
-- Author:      Adela Smith
-- Created:     08/12/2015
-- Release:     6.2
-- Description: Returns the value for the FinPlan: Office Director (Primary)
--              Role Id. This is NOT a subsystem-specific function.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetFinPlanProgramManagerSecondaryRoleId]()
RETURNS int
AS
BEGIN
	
	RETURN 87;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetIDDCoordinatorRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the IDD Coordinator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetIDDCoordinatorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 28; -- IDD Coordinator
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryEditRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/08/2014
-- Release:     5.0
-- Description: Retrieves the id of the Inventory Edit role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryEditRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 17; -- Inventory Edit
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryQARoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the Inventory QA role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryQARoleId] ()
RETURNS int
AS
BEGIN
	RETURN 27; -- Inventory QA
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInventoryReadRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/08/2014
-- Release:     5.0
-- Description: Retrieves the id of the Inventory Read role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInventoryReadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 16; -- Inventory Read
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetInvitePermissionId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     9/30/2015
-- Release:     6.3
-- Description: Retrieves the Invite permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetInvitePermissionId] ()
RETURNS INT
AS
BEGIN
	RETURN 64; -- Invite
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetLabManagementPOCRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/14/2014
-- Release:     5.0
-- Description: Retrieves the id of the Lab Management POC role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetLabManagementPOCRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 24; -- Lab Management POC
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetLabSPALeadRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the Lab SPA Lead role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetLabSPALeadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 59; -- Lab SPA Lead
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetOverrideCreatedAsCheckPermissionId]'
GO
-- =============================================================
-- Author:      Mark Brown
-- Created:     
-- Release:     4.6
-- Description: Retrieves the Override CreatedAs Check permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetOverrideCreatedAsCheckPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 48; -- Override CreatedAs Check 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPersonAdminPermissionObjectId]'
GO

-- =============================================================
-- Author:      Karl Allen
-- Created:     6/8/2016
-- Release:     7.1
-- Description: Retrieves the Person Admin Permission Object Id
--   object id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetPersonAdminPermissionObjectId] ()
RETURNS int
AS
BEGIN
	RETURN 32; -- Person Admin
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPlanningHQRoleId]'
GO


-- =============================================================
-- Author:      Karl Allen
-- Created:     12/8/2014
-- Release:     6.4
-- Description: Retrieves the Planning: HQ role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPlanningHQRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 163; -- Planning: HQ
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPlanningImportPermissionObjectId]'
GO


-- =============================================================
-- Author:      Sara O'Neal
-- Created:     7/23/2013
-- Release:     4.3
-- Description: Retrieves the Planning Import permission object
--   id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetPlanningImportPermissionObjectId] ()
RETURNS int
AS
BEGIN
	RETURN 68; -- Planning Import
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetPlanningSiteModifierRoleId]'
GO


-- =============================================================
-- Author:      Karl Allen
-- Created:     12/8/2014
-- Release:     6.4
-- Description: Retrieves the Planning: Site Modifier role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetPlanningSiteModifierRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 162; -- Planning: Site Modifier
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProjectControlsRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     3/11/2014
-- Release:     5.0
-- Description: Retrieves the HQ Project Controls role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProjectControlsRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 8; -- HQ Project Controls
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProtectTechnicalLeadDomesticRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the Protect Technical Lead Domestic role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProtectTechnicalLeadDomesticRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 41; -- Protect Technical Lead Domestic
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetProtectTechnicalLeadInternationalRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the Protect Technical Lead International role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetProtectTechnicalLeadInternationalRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 20; -- Protect Technical Lead International
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetQueueEntitiesPermissionId]'
GO

-- =============================================================
-- Author:      Nevada Williford
-- Created:     10/1/2015
-- Release:     6.3
-- Description: Retrieves the Queue Entities permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetQueueEntitiesPermissionId] ()
RETURNS INT
AS
BEGIN
	RETURN 12; -- Queue Entities
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetRMSCoordinatorRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the RMS Coordinator role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetRMSCoordinatorRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 38; -- RMS Coordinator
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetRegionOfficerRoleId]'
GO
-- =============================================================
-- Author:      Nevada Williford
-- Created:     04/03/2014
-- Release:     5.0
-- Description: Retrieves the id of the Region Officer role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetRegionOfficerRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 22; -- Region Officer
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPACostSharingAdministratorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Cost Sharing Administrator (Primary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPACostSharingAdministratorPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 138;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPACostSharingAdministratorSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA Cost Sharing Administrator (Secondary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPACostSharingAdministratorSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 139;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPANRCLicenseAdministratorPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA NRC License Administrator (Primary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPANRCLicenseAdministratorPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 134;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPANRCLicenseAdministratorSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     08/07/2015
-- Release:     6.3
-- Description: Retrieves the id of the SPA NRC License Administrator (Secondary) role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetSPANRCLicenseAdministratorSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 135;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetSPAReadRoleId]'
GO
-- =============================================================
-- Author:      Mike Strickland
-- Created:     10/08/2014
-- Release:     5.0
-- Description: Retrieves the id of the SPA Read role.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
create FUNCTION [Security].[fnGetSPAReadRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 18; -- SPA Read
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleExecutivePrimaryRoleId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/11/2016
-- Release:     7.3
-- Description: Retrieves the Schedule: Executive (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleExecutivePrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 109; -- Schedule: Executive (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleExecutiveSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/11/2016
-- Release:     7.3
-- Description: Retrieves the Schedule: Executive (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleExecutiveSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 110; -- Schedule: Executive (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProgramManagerPrimaryRoleId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/11/2016
-- Release:     7.3
-- Description: Retrieves the Schedule: Program Manager (Primary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
Create FUNCTION [Security].[fnGetScheduleProgramManagerPrimaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 107; -- Schedule: Program Manager (Primary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetScheduleProgramManagerSecondaryRoleId]'
GO
-- =============================================================
-- Author:      Gabriel Hanas
-- Created:     10/11/2016
-- Release:     7.3
-- Description: Retrieves the Schedule: Program Manager (Secondary) role id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetScheduleProgramManagerSecondaryRoleId] ()
RETURNS int
AS
BEGIN
	RETURN 108; -- Schedule: Program Manager (Secondary)
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Security].[fnGetVoidPermissionId]'
GO

-- =============================================================
-- Author:      Sara O'Neal
-- Created:     3/6/2014
-- Release:     5.0
-- Description: Retrieves the void permission id.
--
-- mm/dd/yy  Name     Release  Description
-- =============================================================
CREATE FUNCTION [Security].[fnGetVoidPermissionId] ()
RETURNS int
AS
BEGIN
	RETURN 29; -- Void
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetAdvanceChangeRequestActionId]'
GO
GRANT EXECUTE ON  [G2].[fnGetAdvanceChangeRequestActionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetBudgetFormulationPlanningYearParmKey]'
GO
GRANT EXECUTE ON  [G2].[fnGetBudgetFormulationPlanningYearParmKey] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetBudgetFormulationProcessStatusParmKey]'
GO
GRANT EXECUTE ON  [G2].[fnGetBudgetFormulationProcessStatusParmKey] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetCancelledWorkBreakdownStructureStatusId]'
GO
GRANT EXECUTE ON  [G2].[fnGetCancelledWorkBreakdownStructureStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetCurrentFiscalYear]'
GO
GRANT EXECUTE ON  [G2].[fnGetCurrentFiscalYear] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetDefaultSubSystemPreferenceChoiceId]'
GO
GRANT EXECUTE ON  [G2].[fnGetDefaultSubSystemPreferenceChoiceId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetFinalizedChangeRequestDetailStatusId]'
GO
GRANT EXECUTE ON  [G2].[fnGetFinalizedChangeRequestDetailStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetGTRIProgram]'
GO
GRANT EXECUTE ON  [G2].[fnGetGTRIProgram] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetInProcessChangeRequestDetailStatusId]'
GO
GRANT EXECUTE ON  [G2].[fnGetInProcessChangeRequestDetailStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetNonResponseChangeRequestDetailConcurrenceStatusId]'
GO
GRANT EXECUTE ON  [G2].[fnGetNonResponseChangeRequestDetailConcurrenceStatusId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetPassBackChangeRequestActionId]'
GO
GRANT EXECUTE ON  [G2].[fnGetPassBackChangeRequestActionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetProjectCanceledMilestoneId]'
GO
GRANT EXECUTE ON  [G2].[fnGetProjectCanceledMilestoneId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetScopeMilestoneDateSpread]'
GO
GRANT SELECT ON  [G2].[fnGetScopeMilestoneDateSpread] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetSecondaryFilterSubSystemLevelId]'
GO
GRANT EXECUTE ON  [G2].[fnGetSecondaryFilterSubSystemLevelId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetSubmitChangeRequestActionId]'
GO
GRANT EXECUTE ON  [G2].[fnGetSubmitChangeRequestActionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnGetVoidedChangeRequestDetailStatusId]'
GO
GRANT EXECUTE ON  [G2].[fnGetVoidedChangeRequestDetailStatusId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [G2].[fnRptLessThan20WorkingDays]'
GO
GRANT EXECUTE ON  [G2].[fnRptLessThan20WorkingDays] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetConcurPermissionId]'
GO
GRANT EXECUTE ON  [Security].[fnGetConcurPermissionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetFireProtectionPermissionObjectId]'
GO
GRANT EXECUTE ON  [Security].[fnGetFireProtectionPermissionObjectId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetLabBudgetPOCRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetLabBudgetPOCRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetLabManagementPOCRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetLabManagementPOCRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetProcessPermissionId]'
GO
GRANT EXECUTE ON  [Security].[fnGetProcessPermissionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetRankPermissionId]'
GO
GRANT EXECUTE ON  [Security].[fnGetRankPermissionId] TO [G2AppUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAAdministratorRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAAdministratorRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPACostSharingAdministratorPrimaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPACostSharingAdministratorPrimaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPACostSharingAdministratorSecondaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPACostSharingAdministratorSecondaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAIDDCoordinatorPrimaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAIDDCoordinatorPrimaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAIDDCoordinatorSecondaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAIDDCoordinatorSecondaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAInventoryQARoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAInventoryQARoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPALabLeadRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPALabLeadRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPANRCLicenseAdministratorPrimaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPANRCLicenseAdministratorPrimaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPANRCLicenseAdministratorSecondaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPANRCLicenseAdministratorSecondaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAPortfolioManagerPrimaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAPortfolioManagerPrimaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAPortfolioManagerSecondaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAPortfolioManagerSecondaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAProtectTechnicalLeadRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAProtectTechnicalLeadRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPARMSCoordinatorRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPARMSCoordinatorRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPAReadRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPAReadRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPASubProgramManagerPrimaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPASubProgramManagerPrimaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPASubProgramManagerSecondaryRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPASubProgramManagerSecondaryRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPATeamLeadRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPATeamLeadRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSPATeamMemberRoleId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSPATeamMemberRoleId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetShowInInboxPermissionId]'
GO
GRANT EXECUTE ON  [Security].[fnGetShowInInboxPermissionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [Security].[fnGetSubmitPermissionId]'
GO
GRANT EXECUTE ON  [Security].[fnGetSubmitPermissionId] TO [G2Read]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
